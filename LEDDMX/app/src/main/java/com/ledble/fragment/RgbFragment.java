package com.ledble.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledble.activity.MainActivity;
import com.leddmx.R;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.bean.MyColor;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.view.MyColorPickerImageView.OnTouchPixListener;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView;
import com.ledble.view.MyColorPickerImageView4RGB;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import butterknife.Bind;

/**
 * 彩色
 * 
 * @author ftl
 *
 */
public class RgbFragment extends LedBleFragment {

	private SegmentedRadioGroup segmentedRadioGroup;
//	
//	
	@Bind(R.id.relativeTab1) View relativeTab1;// 色环
	@Bind(R.id.relativeTab2) View relativeTab2;// 模式
	@Bind(R.id.relativeTab3) View relativeTab3;// 自定义
//	@Bind(R.id.listViewModel) ListView listViewModel;
//	@Bind(R.id.textViewCurretModel) TextView textViewCurretModel;
	
	
	private MainActivity mActivity;
	private View mContentView;
	private View menuView;
	
	
	// RGB色环
	private int rgbOnOffStatus = 0;
	@Bind(R.id.imageViewOnOff) ImageView imageViewOnOff;
	@Bind(R.id.tvRGB) TextView textViewRGB;
	@Bind(R.id.imageViewPicker) MyColorPickerImageView4RGB imageViewPicker;
	@Bind(R.id.blackWiteSelectView) BlackWiteSelectView blackWiteSelectView;
	@Bind(R.id.tvBrightness) TextView tvBrightness;
	

	// 亮度
	private int ctOnOffStatus = 0;
	@Bind(R.id.seekBarRedBrightNess) SeekBar seekBarRedBrightNess;
	@Bind(R.id.tvBrightness1) TextView tvBrightness1;
	@Bind(R.id.seekBarGreenBrightNess) SeekBar seekBarGreenBrightNess;
	@Bind(R.id.tvBrightness2) TextView tvBrightness2;
	@Bind(R.id.seekBarBlueBrightNess) SeekBar seekBarBlueBrightNess;
	@Bind(R.id.tvBrightness3) TextView tvBrightness3;
	@Bind(R.id.imageViewOnOffBrightness) ImageView imageViewOnOffBrightness;
	
	
	// 单色
	private int dimOnOffStatus = 0;
	@Bind(R.id.textViewBrightNessDim) TextView textViewBrightNessDim;
	@Bind(R.id.pikerImageViewDim) MyColorPickerImageView pikerImageViewDim;
	@Bind(R.id.imageViewOnOffDim) ImageView imageViewOnOffDim;
	
	
	
	// 弹窗选色界面
	private MyColorPickerImageView4RGB imageViewPicker2;
	private BlackWiteSelectView blackWiteSelectView2;
	private TextView textViewRingBrightSC;
	private SeekBar seekBarSpeedBarSC;
	private TextView textViewSpeedSC;
	private SeekBar seekBarBrightBarSC;
	private TextView textViewBrightSC;
	
	private int currentTab = 1;// 1：色环，2：模式
	private SharedPreferences sp;
	private SegmentedRadioGroup srgCover;
	private TextView tvCoverModel;
	private TextView textRGB;
	private LinearLayout llRing;
	private LinearLayout llCover;
	private ListView lvCover;
	
	private int brightnessValue;
	private ModelAdapter maAdapter;
	private String diyViewTag;
	private int currentSelecColorFromPicker;
	private ColorTextView actionView;
	private PopupWindow mPopupWindow;
	
	private Button buttonSelectColorConfirm;// 确认
	
	
	
	
	private static final int COLOR_DEFALUT = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_rgb, container, false); 
		menuView = inflater.inflate(R.layout.activity_select_color, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		mActivity = (MainActivity) getActivity();
		
		segmentedRadioGroup = mActivity.getSegmentRgb();			

		
		segmentedRadioGroup.check(R.id.rbRgbOne);
		segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbRgbOne == checkedId) {
					relativeTab1.setVisibility(View.VISIBLE);
					relativeTab2.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
				} else if ((R.id.rbRgbTwo == checkedId)) {
					relativeTab2.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
				} else if ((R.id.rbRgbThree == checkedId)) {
					relativeTab3.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab2.setVisibility(View.GONE);
				}
			}
		});
		
		
		//********************************色环*******************************//
		
		textViewRGB.setText(mActivity.getString(R.string.r_g_b, 0, 0, 0));
		this.imageViewPicker.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int[] colors = Tool.getRGB(color);
				blackWiteSelectView.setStartColor(color);
				updateRgbText(colors);
				textViewRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
			}
		});
		
		this.blackWiteSelectView.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
				int p = progress;
				if (progress <= 0) {
					p = 1;
				} else if (progress >= 100) {
					p = 100;
				}
				mActivity.setBrightNess((p*32)/100);
				tvBrightness.setText(mActivity.getResources().getString(R.string.brightness_set, p));

			}
		});
		
		this.imageViewOnOff.setOnClickListener(new OnClickListener() { // 开关灯

			@Override
			public void onClick(View v) {
				switch (rgbOnOffStatus) {
				case 0:
					imageViewOnOff.setImageResource(R.drawable.off_btn);
					mActivity.close();
					rgbOnOffStatus = 1;
					break;
				case 1:
					imageViewOnOff.setImageResource(R.drawable.on_btn);
					mActivity.open();
					rgbOnOffStatus = 0;
					break;

				default:
					break;
				}
			}
		});
		
		
		//*********************************** 亮度 ************************************//
		
		this.seekBarRedBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				tvBrightness1.setText(Integer.toString(progress));
				mActivity.setSmartBrightness(progress, 1);
				
			}
		});
		this.seekBarGreenBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				tvBrightness2.setText(Integer.toString(progress));
				mActivity.setSmartBrightness(progress, 2);

			}
		});
		this.seekBarBlueBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				tvBrightness3.setText(Integer.toString(progress));
				mActivity.setSmartBrightness(progress, 3);

			}
		});
		
		this.imageViewOnOffBrightness.setOnClickListener(new OnClickListener() { // 开关灯
			@Override
			public void onClick(View v) {
				switch (ctOnOffStatus) {
				case 0:
					imageViewOnOffBrightness.setImageResource(R.drawable.off_btn);
					mActivity.close();
					ctOnOffStatus = 1;
					break;
				case 1:
					imageViewOnOffBrightness.setImageResource(R.drawable.on_btn);
					mActivity.open();
					ctOnOffStatus = 0;
					break;
				default:
					break;
				}
			}
		});
		
		
		
		//************************************* 单色 ******************************//
		
		this.pikerImageViewDim.setInnerCircle(0.25f);
		this.pikerImageViewDim.setOnTouchPixListener(new OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int cool = (int) ((angle / 360) * 100);
				String percentColor = cool + "%";
				textViewBrightNessDim.setText(mActivity.getResources().getString(R.string.brightness) + ":" + percentColor);
				mActivity.setDim((cool*32)/100);
			}
		});
		
		this.imageViewOnOffDim.setOnClickListener(new OnClickListener() { // 开关灯
			@Override
			public void onClick(View v) {
				switch (dimOnOffStatus) {
				case 0:
					imageViewOnOffDim.setImageResource(R.drawable.off_btn);
					mActivity.close();
					dimOnOffStatus = 1;
					break;
				case 1:
					imageViewOnOffDim.setImageResource(R.drawable.on_btn);
					mActivity.open();
					dimOnOffStatus = 0;
					break;
				default:
					break;
				}
			}
		});
		
		
		
		
		//*********************************** 弹窗选色界面 ************************************//						
		
		textViewRingBrightSC = (TextView) menuView.findViewById(R.id.tvRingBrightnessSC);
		
		seekBarBrightBarSC = (SeekBar) menuView.findViewById(R.id.seekBarBrightNess);
		textViewBrightSC = (TextView) menuView.findViewById(R.id.textViewBrightNess);
		seekBarBrightBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (0 == progress) {
					seekBar.setProgress(1);
					mActivity.setBrightNess(1);
					textViewBrightSC.setText(mActivity.getResources().getString(R.string.brightness_set, 1));
					SharePersistent.saveBrightData(mActivity, diyViewTag + "bright", diyViewTag + "bright", 1);
				} else {
					mActivity.setBrightNess((progress*32)/100);
					textViewBrightSC.setText(mActivity.getResources().getString(R.string.brightness_set, progress));
					SharePersistent.saveBrightData(mActivity, diyViewTag + "bright", diyViewTag + "bright", progress);
				}
			}
		});
		
		seekBarSpeedBarSC = (SeekBar) menuView.findViewById(R.id.seekBarSpeed);
		textViewSpeedSC = (TextView) menuView.findViewById(R.id.textViewSpeed);
		seekBarSpeedBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					mActivity.setSpeed(1);
					textViewSpeedSC.setText(mActivity.getResources().getString(R.string.speed_set, 1));
					SharePersistent.saveBrightData(mActivity, diyViewTag+"speed", diyViewTag+"speed", 1);

				} else {
					mActivity.setSpeed(progress);
					textViewSpeedSC.setText(mActivity.getResources().getString(R.string.speed_set, progress));
					SharePersistent.saveBrightData(mActivity, diyViewTag+"speed", diyViewTag+"speed", progress);
				}
			}
		});
		
		sp = mActivity.getSharedPreferences(Constant.SPF_DIY, Context.MODE_PRIVATE);
		textRGB = (TextView) menuView.findViewById(R.id.tvRGB);
		
		srgCover = (SegmentedRadioGroup) menuView.findViewById(R.id.srgCover);
		srgCover.check(R.id.rbRing);
		srgCover.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbRing == checkedId) {
					currentTab = 1;
					llRing.setVisibility(View.VISIBLE);
					llCover.setVisibility(View.GONE);
				} else if ((R.id.rbModle == checkedId)) {
					currentTab = 2;
					llCover.setVisibility(View.VISIBLE);
					llRing.setVisibility(View.GONE);
//					llCover.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);  
					
				}
			}
		});
		
		lvCover = (ListView) menuView.findViewById(R.id.lvCover);
		lvCover.setAdapter(buildModel());
		lvCover.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				maAdapter.setIndex(position);
				maAdapter.notifyDataSetChanged();
				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
				String value = abean.getValue();
				tvCoverModel.setText(mActivity.getResources().getString(R.string.current_mode_format, abean.getLabel()));
				tvCoverModel.setTag(value);
				currentSelecColorFromPicker = Integer.parseInt(abean.getValue());
				mActivity.setSPIModel(Integer.parseInt(value));
			}
		});
		
		
		llRing = (LinearLayout) menuView.findViewById(R.id.llRing);
		llCover = (LinearLayout) menuView.findViewById(R.id.llCover);
		tvCoverModel = (TextView) menuView.findViewById(R.id.tvCoverModel);
		imageViewPicker2 = (MyColorPickerImageView4RGB) menuView.findViewById(R.id.imageViewPicker2);
		blackWiteSelectView2 = (BlackWiteSelectView) menuView.findViewById(R.id.blackWiteSelectView2);
		buttonSelectColorConfirm = (Button) menuView.findViewById(R.id.buttonSelectColorConfirm); // 通过另外一个布局对象的findViewById获取其中的控件
		

//		this.seekBarSpeedBar.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
//			@Override
//			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//				if (progress == 0) {
//					seekBar.setProgress(1);
//					mActivity.setSpeed(1);
//					textViewSpeed.setText(mActivity.getResources().getString(R.string.speed_set, 1));
//
//				} else {
//					mActivity.setSpeed(progress);
//					textViewSpeed.setText(mActivity.getResources().getString(R.string.speed_set, progress));
//
//				}
//			}
//		});
		




//		this.seekBarBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
//			@Override
//			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//				if (0 == progress) {
//					seekBar.setProgress(1);
//					textViewBrightness.setText(mActivity.getResources().getString(R.string.brightness_set, 1));
//					mActivity.setBrightNess(1);
//
//				} else {
//					textViewBrightness.setText(mActivity.getResources().getString(R.string.brightness_set, progress));
//					mActivity.setBrightNess(progress);
//
//				}
//			}
//		});
//		

//


//		this.listViewModel.setAdapter(buildModel());
//		this.listViewModel.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				maAdapter.setIndex(position);
//				maAdapter.notifyDataSetChanged();
//				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
//				String value = abean.getValue();
//				textViewCurretModel
//						.setText(mActivity.getResources().getString(R.string.current_mode_format, abean.getLabel()));
//				textViewCurretModel.setTag(value);
//				mActivity.setRegMode(Integer.parseInt(value));
//
//			}
//		});


//		changeRadioGroup.check(R.id.changeButtonOne);
//		changeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				if (R.id.changeButtonOne == checkedId) {
//					style = 0;
//				} else if ((R.id.changeButtonTwo == checkedId)) {
//					style = 1;
//				} else if ((R.id.changeButtonThree == checkedId)) {
//					style = 2;
//				} else if ((R.id.changeButtonFour == checkedId)) {
//					style = 3;
//				}
//			}
//		});

//		tgbtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mActivity.isLightOpen = isChecked;
//				if (isChecked) {
//					mActivity.open();
//				} else {
//					mActivity.close();
//				}
//
//			}
//		});
		


//		buttonRunButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				putDataBack(getSelectColor());
//			}
//		});

		

		

		initSingColorView();
		initColorBlock();
		initColorSelecterView();
	}

	@Override
	public void initView() {
//		tgbtn.setChecked(mActivity.isLightOpen);
//		seekBarBrightness.setProgress(mActivity.brightness);// 设置亮度
//		seekBarSpeedBar.setProgress(mActivity.speed);
	}

	@Override
	public void initEvent() {

	}

	// private void pauseMusicAndVolum() {
	// musicFragment.pauseMusic();
	// musicFragment.pauseVolum();
	// }

	private void putDataBack(ArrayList<MyColor> colors) {
//		mActivity.setDiy(colors, style);

	}

//	private ArrayList<MyColor> getSelectColor() {
//
//		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
//		if (!ListUtiles.isEmpty(colorTextViews)) {
//			for (ColorTextView ctx : colorTextViews) {
//				if (COLOR_DEFALUT != ctx.getColor()) {
//					int[] rgb = Tool.getRGB(ctx.getColor());
//					colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
//				}
//			}
//		}
//		return colorList;
//	}

	/**
	 * 初始化单色View的点击事件
	 */
	private void initSingColorView() {
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
//		int[] colors = { 0xFF0000, 0x00FF00,  0x0000FF, 0xFFFFFF,  0xFFFF00,  0xff00ff };

//		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		String tagStart = "viewColor";
		View.OnClickListener click = new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.layout_scale));
				int color = (Integer) v.getTag();
				updateRgbText(Tool.getRGB(color));
				blackWiteSelectView.setStartColor(color);
				imageViewPicker.move2Ege(rmap.get(color));
				int[] colors = Tool.getRGB(color);
				textViewRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
			}
		};
		ArrayList<View> views = new ArrayList<View>();
		for (int i = 1; i <= 6; i++) {
			View view = mContentView.findViewWithTag(tagStart + i);
			view.setOnClickListener(click);
			/*if (i==3) {
				Toast.makeText(mActivity, "tag:" + colors[i], Toast.LENGTH_SHORT).show();
			}*/
			view.setTag(colors[i - 1]);
			views.add(view);
		}

	}

	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
//		View blocks = mContentView.findViewById(R.id.linearLayoutViewBlocks);
		// 16个自定义view
//		colorTextViews = new ArrayList<ColorTextView>();
//		for (int i = 1; i <= 16; i++) {
//			final ColorTextView tv = (ColorTextView) blocks.findViewWithTag((String.valueOf("labelColor") + i));
//			String tag = (String) tv.getTag();
//			int color = sp.getInt(tag, COLOR_DEFALUT);
//			if (color != COLOR_DEFALUT) {
//				int radius = 10;
//				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
//				// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
//				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
//				// 组合圆角矩形和ShapeDrawable
//				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
//				// 设置形状的颜色
//				shapeDrawable.getPaint().setColor(color);
//				// 设置绘制方式为填充
//				shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
//				// 将当前选择得颜色设置到触发颜色编辑器得View上
//				tv.setBackgroundDrawable(shapeDrawable);
//				tv.setColor(color);
//				tv.setText("");
//			}
//			tv.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {// 点击弹出颜色选择框
//					v.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.layout_scale));
//					int color = tv.getColor();
//					if (color == COLOR_DEFALUT) {
//						showColorCover((ColorTextView) v, false);
//					}
//				}
//			});
//			tv.setOnLongClickListener(new OnLongClickListener() {
//				@Override
//				public boolean onLongClick(View v) {
//					ColorTextView cv = (ColorTextView) v;
//					cv.setColor(COLOR_DEFALUT);
//					String tag = (String) tv.getTag();
//					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
//					// 长按删除颜色
//					cv.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.block_shap_color));
//					cv.setText("+");
//					return true;
//				}
//			});
//			colorTextViews.add(tv);
//		}

		View diy = mContentView.findViewById(R.id.linarLayoutColorCile);
		for (int i = 1; i <= 6; i++) {
			final ColorTextView tv = (ColorTextView) diy.findViewWithTag((String.valueOf("diyColor") + i));
			String tag = (String) tv.getTag();
			int color = sp.getInt(tag, COLOR_DEFALUT);

			if (color != COLOR_DEFALUT) {
				if (color < 128) {
					int radius = 10;
					float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
					// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
					RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
					// 组合圆角矩形和ShapeDrawable
					ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
					// 设置形状的颜色
					shapeDrawable.getPaint().setColor(color);
					// 设置绘制方式为填充
					shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
					// 将当前选择得颜色设置到触发颜色编辑器得View上
					tv.setBackgroundDrawable(shapeDrawable);
					tv.setColor(color);
				} else {
					Drawable image = getImage(color + "");
					tv.setBackgroundDrawable(image);
					tv.setColor(color);
				}
				tv.setText("");
			}

			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.layout_scale));
					int color = tv.getColor();
					
					diyViewTag = (String) tv.getTag();
					
					if (color != COLOR_DEFALUT) {
						if (color < 128) {
							
							updateRgbText(Tool.getRGB(color));
//							int  bright = SharePersistent.getBrightData(mActivity, Constant.BRIGHT_TYPE, Constant.BRIGHT_KEY);
							int  bright = SharePersistent.getBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright");
							if (0 == bright) {
								mActivity.setBrightNess(100); //默认 100
							}else {
								mActivity.setBrightNess(bright);  //取出亮度，并发送
							}
							
						} else {
							mActivity.setSPIModel(color);
							
							int  bright = SharePersistent.getBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright");
							int  speed = SharePersistent.getBrightData(mActivity, diyViewTag+"speed", diyViewTag+"speed");
							if (0 == bright) {
								mActivity.setBrightNess(100);
							}else {
								mActivity.setBrightNess(bright);
							}
							
							if (0 == speed) {
								mActivity.setSpeed(85);
							}else {
								mActivity.setSpeed(speed);
							}
						}
					} else {
//						Toast.makeText(mActivity, "showColorCover", Toast.LENGTH_SHORT).show();
//						showColorCover((ColorTextView) v, true);
						showColorCover((ColorTextView) v, true);
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(COLOR_DEFALUT);
					String tag = (String) tv.getTag();
					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
		}
	}

	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 * @param hasModel
	 */
	public void showColorCover(ColorTextView actionView, final boolean hasBrightNess) {
		// 数据初始化
		this.actionView = actionView;
		currentSelecColorFromPicker = COLOR_DEFALUT;
		srgCover.check(R.id.rbRing);
		tvCoverModel.setText(mActivity.getResources().getString(R.string.current_mode));
		maAdapter.setIndex(COLOR_DEFALUT);
		maAdapter.notifyDataSetChanged();
		textRGB.setText(mActivity.getString(R.string.r_g_b, 0, 0, 0));

		if (hasBrightNess) {
			srgCover.setVisibility(View.GONE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.VISIBLE);
		} else {
			srgCover.setVisibility(View.INVISIBLE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.GONE);
			textViewRingBrightSC.setVisibility(View.GONE);
		}

		mPopupWindow = new PopupWindow(menuView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
		mPopupWindow.showAtLocation(mContentView, Gravity.BOTTOM, 0, 0);
//		mPopupWindow.setAnimationStyle(R.style.animTranslate);

	}

	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		mPopupWindow.dismiss(); // 隐藏
	}

	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {

		imageViewPicker2.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView2.setStartColor(color);
				currentSelecColorFromPicker = color;

				int[] colors = Tool.getRGB(color);
				updateRgbText(colors);
				textRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
//				SharePersistent.saveBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright", 100);
				SharePersistent.saveBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright", brightnessValue);
			}
		});

		blackWiteSelectView2.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
//				currentSelecColorFromPicker = color;

				int p = progress;
				if (progress <= 0) {
					p = 1;
				}
				if (progress >= 100) {
					p = 100;
				}
				brightnessValue = p;
				textViewRingBrightSC.setText(mActivity.getResources().getString(R.string.brightness_set, p));
				SharePersistent.saveBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright", p);
				mActivity.setBrightNess((p*32)/100);
			}
		});

		View viewColors = menuView.findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
//		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView2.setStartColor(selectColor);
					imageViewPicker2.move2Ege(rmap.get(selectColor));
					updateRgbText(Tool.getRGB(selectColor));
					
					int[] colors = Tool.getRGB(selectColor);
					textRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				}
			});
			viewCsArrayLis.add(vc);
		}

		buttonSelectColorConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (currentSelecColorFromPicker != COLOR_DEFALUT) {
					if (currentTab == 1) {
						int radius = 10;
						float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
						// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
						// 就会根据形状来绘制。
						RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
						// 组合圆角矩形和ShapeDrawable
						ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
						// 设置形状的颜色
						shapeDrawable.getPaint().setColor(currentSelecColorFromPicker);
						// 设置绘制方式为填充
						shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						sp.edit().putInt(tag, currentSelecColorFromPicker).commit();
						// 将当前选择得颜色设置到触发颜色编辑器得View上
						actionView.setBackgroundDrawable(shapeDrawable);
					} else {
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						if (null != tvCoverModel) {
							if (null != tvCoverModel.getTag().toString()) {
								String value = tvCoverModel.getTag().toString();
								sp.edit().putInt(tag, Integer.parseInt(value)).commit();
								Drawable image = getImage(value);
								actionView.setBackgroundDrawable(image);
							}
						}
					}
					actionView.setText("");
				}
				
				hideColorCover();
			}
		});
	}

	public Drawable getImage(String value) {
		int resID = mActivity.getResources().getIdentifier("img_" + value, "drawable", "com.ledble");
		return mActivity.getResources().getDrawable(resID);
	}

	public void updateRgbText(int rgb[]) {
		((TextView) mContentView.findViewById(R.id.textViewR)).setText("R:" + (rgb[0]));
		((TextView) mContentView.findViewById(R.id.textViewG)).setText("G:" + (rgb[1]));
		((TextView) mContentView.findViewById(R.id.textViewB)).setText("B:" + (rgb[2]));
		try {
			mActivity.setRgb(rgb[0], rgb[1], rgb[2]);
		} catch (Exception e) {
			e.printStackTrace();
			Tool.ToastShow(mActivity, "错误。。。");
		}
	}

	private ModelAdapter buildModel() {
		String[] ary = mActivity.getResources().getStringArray(R.array.rgb_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(mActivity, abs);
		return maAdapter;
	}

}
