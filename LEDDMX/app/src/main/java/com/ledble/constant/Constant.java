package com.ledble.constant;

public class Constant {

	public static final int MUSIC_STYLE_POP = 0;
	public static final int MUSIC_STYLE_SOFT = 1;
	public static final int MUSIC_STYLE_ROCK = 2;
	
	public static final String MODLE_TYPE = "MODLE_TYPE";
	public static final String MODLE_VALUE = "MODLE_VALUE";
	public static final String SPF_DIY = "DIY";
	public static final String DYNAMIC_DIY = "DYNAMIC_DIY";
	public static final String IMAGE_VALUE = "IMAGE_VALUE";
	
	
	public static final String BRIGHT_TYPE = "BRIGHT_TYPE";
	public static final String BRIGHT_KEY = "BRIGHT_KEY";
	
	public static final String TIMER_TAG = "timer_tag";
	
	public static final String RGB_SORT_TYPE = "RGB_SORT_TYPE";
	public static final String CHIP_TYPE = "CHIP_TYPE";
	public static final String PIX_VALUE = "PIX_VALUE";
	
	public static final String RGB_SORT_TYPE_KEY = "RGB_SORT_TYPE_KEY";
	public static final String CHIP_TYPE_KEY = "CHIP_TYPE_KEY";
	public static final String PIX_VALUE_KEY = "PIX_VALUE_KEY";
	
	public static final String PIX_SPACERCODE_KEY = "PIX_SPACERCODE_KEY";
	public static final String PIX_SPACERCODE_TYPE = "PIX_SPACERCODE_TYPE";
	
	public static final String PIX_STARTCODE_KEY = "PIX_STARTCODE_KEY";
	public static final String PIX_STARTCODE_TYPE = "PIX_STARTCODE_TYPE";


}
