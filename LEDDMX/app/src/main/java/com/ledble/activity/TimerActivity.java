package com.ledble.activity;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.leddmx.R;
import com.umeng.analytics.MobclickAgent;

public class TimerActivity extends BaseActivity {

    private LinearLayout linearLayoutTimerOn;
    private LinearLayout linearLayoutTimerOff;
    private TextView textViewOnTime;

    private TextView textViewOffTime;
    private TextView textViewModelText;
    private ToggleButton toggleButtonOn;
    private ToggleButton toggleButtonOff;

    private boolean isClick = false;
    private final int INT_TIMER_ON = 111;
    private final int INT_TIMER_OFF = 222;
    private MainActivity mActivity;

    private String tag = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initView();
    }

    @Override
    public void initView() {
        setContentView(R.layout.fragment_timer);
        mActivity = MainActivity.getMainActivity();

        textViewOnTime = findTextViewById(R.id.textViewOnTime);
        textViewOffTime = findTextViewById(R.id.textViewOffTime);
        textViewModelText = findTextViewById(R.id.textViewModelText);
        toggleButtonOn =  findViewById(R.id.toggleOn);
        toggleButtonOff = findViewById(R.id.toggleOff);

        for (int i = 0; i < 2; i++) {
            if (i == 0) {
                tag = "TimerOn";
                String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName() + tag);

                if (null != timeData && timeData.length > 3) {

                    int hour = Integer.parseInt(timeData[0]);
                    int minute = Integer.parseInt(timeData[1]);
                    int model = Integer.parseInt(timeData[2]);
                    String modelText = timeData[3];

                    if (hour >= 0 && minute >= 0) {
                        textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":"
                                + NumberHelper.LeftPad_Tow_Zero(minute));
                    }
                    if (model >= 0) {
                        textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on)
                                + ":" + modelText);
                    } else {
                        textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on));
                    }

                    toggleButtonOn.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOn"));

                }
            } else {
                tag = "TimerOff";
                String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName() + tag);
                if (null != timeData && timeData.length > 1) {
                    int hour = Integer.parseInt(timeData[0]);
                    int minute = Integer.parseInt(timeData[1]);

                    if (hour >= 0 && minute >= 0) {
                        textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":"
                                + NumberHelper.LeftPad_Tow_Zero(minute));
                    }

                    toggleButtonOff.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOff"));
                }
            }
        }


        Button buttonBackButton = (Button) findButtonById(R.id.buttonBack);
        buttonBackButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        linearLayoutTimerOn = (LinearLayout) findViewById(R.id.linearLayoutTimerOn);
        linearLayoutTimerOn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "TimerOn";
                gotoTimerSettting(v, INT_TIMER_ON);
                SharePersistent.savePerference(mActivity, "Timer", tag);
            }
        });
        linearLayoutTimerOff = (LinearLayout) findViewById(R.id.linearLayoutTimerOff);
        linearLayoutTimerOff.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                tag = "TimerOff";
                gotoTimerSettting(v, INT_TIMER_OFF);
                SharePersistent.savePerference(mActivity, "Timer", tag);
            }
        });


        toggleButtonOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharePersistent.saveBoolean(mActivity, mActivity.getGroupName() + "toggleButtonOn", toggleButtonOn.isChecked());

                tag = "TimerOn";
                String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName() + tag);
                if (toggleButtonOn.isChecked()) {
                    int hour = Integer.parseInt(timeData[0]);
                    int minute = Integer.parseInt(timeData[1]);
                    int model = Integer.parseInt(timeData[2]);
                    if (hour >= 0 && minute >= 0) {
                        mActivity.timerOn(hour, minute, model);
                    }

                } else {
                    mActivity.turnOnOffTimerOn(0);
                }
            }
        });

        toggleButtonOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // textViewOffTime //保存定时关的时间
                SharePersistent.saveBoolean(mActivity, mActivity.getGroupName() + "toggleButtonOff", toggleButtonOff.isChecked());
                tag = "TimerOff";
                String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName() + tag);
                if (toggleButtonOff.isChecked()) {
                    int hour = Integer.parseInt(timeData[0]);
                    int minute = Integer.parseInt(timeData[1]);
                    mActivity.timerOff(hour, minute);
                } else {
                    mActivity.turnOnOffTimerOff(0);
                }
            }
        });
    }

    private void gotoTimerSettting(View v, int id) {
        isClick = true;
        Intent intent = new Intent(mActivity, TimerSettingActivity.class);
        mActivity.startActivityForResult(intent, id);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();

        if (isClick) {
            String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName() + tag);
            if (tag.equalsIgnoreCase("TimerOn")) {
                int hour = Integer.parseInt(timeData[0]);
                int minute = Integer.parseInt(timeData[1]);
                int model = Integer.parseInt(timeData[2]);
                String modelText = timeData[3];

                if (hour >= 0 && minute >= 0) {
                    textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
                }

                textViewOnTime.setTag(modelText);
                if (model >= 0) {
                    textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on) + ":" + modelText);
                } else {
                    textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on));
                }

                textViewModelText.setTag(model);
                if (toggleButtonOn.isChecked()) {
                    mActivity.timerOn(hour, minute, model);
                }

            } else if (tag.equalsIgnoreCase("TimerOff")) {

                int hour = Integer.parseInt(timeData[0]);
                int minute = Integer.parseInt(timeData[1]);
                textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
                if (toggleButtonOff.isChecked()) {
                    mActivity.timerOff(hour, minute);
                }

            }
        }
    }
}
