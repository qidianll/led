package com.ledble.activity;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baidu.android.pushservice.PushConstants;
import com.baidu.android.pushservice.PushManager;
import com.leddmx.R;
import com.ledble.http.AdvertBean;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledble.utils.Utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

public class LoadingActivity extends Activity {
	
	private String TAG = "LoadingActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//去掉标题栏
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//设置全屏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_loading);
		
        // 启动百度push
        PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY,
               Utils.getMetaValue(LoadingActivity.this, "api_key"));
		
		String url = "http://120.79.182.171/xpy-server/advert/queryNewAdvert";
		Map<String, String> params = new HashMap<String, String>();
		params.put("appId", "1");
		HttpUtil.getInstance().getSourceData(this, url, params, new HttpCallBack() {
			
			@Override
			public void onSuccess(String result) {
				ResponseBean<AdvertBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<AdvertBean>>() {});
				Log.v(TAG, "responseBean:" + responseBean.toString());
				Intent intent = null;
				if (responseBean != null && "000000".equals(responseBean.getReturnCode())) {
					intent = new Intent(LoadingActivity.this, AdvertActivity.class);
					AdvertBean advertBean = (AdvertBean) responseBean.getContent();
					intent.putExtra("advertBean", advertBean);
				} else {
					intent = new Intent(LoadingActivity.this, MainActivity.class);
				}

				startActivity(intent);
				finish();
			}
			
			@Override
			public void onException(String message) {
				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		});
	}

}
