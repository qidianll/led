package com.ledble.activity;

import com.leddmx.R;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class CustomActivity extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvContent = (TextView) findViewById(R.id.tvContent);
        tvTitle.setText(getIntent().getStringExtra("title"));
        tvContent.setText(getIntent().getStringExtra("description"));
    }
    
}
