package com.ledble.activity;

//import android.R.string;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import com.common.uitl.SharePersistent;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

//import java.lang.reflect.Array;
import java.util.ArrayList;
//import java.util.Arrays;

import com.common.BaseActivity;
//import com.common.uitl.Constant;
import com.common.uitl.StringUtils;
import com.common.adapter.ChipSelectAdapter;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.common.adapter.RGBSelectAdapter;
import com.ledble.bean.AdapterBean;
//import com.ledble.db.GroupDeviceDao;
import com.leddmx.R;

public class CodeActivity extends BaseActivity {

	private Button buttonChipselect;
	private Context mContext;
	private Button backButton;

	private ListView mLeftLv/*, mRightLv*/;

	// private MainActivity mActivity;
	// private ModelAdapter maAdapter;
	private RGBSelectAdapter rgbAdapter;
	private ChipSelectAdapter chipAdapter;
	private Button setSpacercodeBtn;
	private Button setStartcodeBtn;
	private Button spacercodeNum;
	private Button startcodeNum;
//	private Button pixNumber;
//	private TextView chipTvResult;
	private TextView rgbTvResult;
	private TextView spacercodeTvResult;
	private TextView startcodeTvResult;
//	private Button chipselectOKBtn;
	private SharedPreferences sp;
	private int chipType;
	private int spacerCode;
	private int startCode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_code);
		// mActivity = (MainActivity) getActivity();

		mLeftLv = (ListView) findViewById(R.id.lv_left);
//		mRightLv = (ListView) findViewById(R.id.lv_right);

		mLeftLv.setAdapter(buildRGBModel());
		mLeftLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				rgbAdapter.setIndex(position);
				rgbAdapter.notifyDataSetChanged();
				
				AdapterBean abean = ((AdapterBean) rgbAdapter.getItem(position));
				chipType = Integer.parseInt(abean.getValue());
				Toast.makeText(getApplicationContext(), abean.getLabel(), Toast.LENGTH_SHORT).show();
				rgbTvResult.setText(abean.getLabel());

				// saveData(Constant.RGB_SORT_TYPE, Constant.RGB_SORT_TYPE_KEY,
				// abean.getLabel(), position); // 保存选择的RGB排序类型
				SharePersistent.saveConfigData(mContext, Constant.CHIP_TYPE, Constant.CHIP_TYPE_KEY,
						abean.getLabel(), position);// 保存选择的RGB排序类型
			}
		});

		// 返回按钮
		backButton = findButtonById(R.id.buttonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(chipType, (byte) (bannerPix >> 8), (byte) bannerPix,bannerSort);
				finish();
			}
		});
		
		// ok按钮
		this.buttonChipselect = findButtonById(R.id.button_chipselect_ok);
		this.buttonChipselect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (chipType <= 0) {
					Toast.makeText(mContext, R.string.chiptypevaluetip, Toast.LENGTH_SHORT).show();
					return;
				}
				if (startCode < 0) {
					Toast.makeText(mContext, R.string.startcodevaluetip, Toast.LENGTH_SHORT).show();
					return;
				}
				if (spacerCode <= 0) {
					Toast.makeText(mContext, R.string.spacercodevaluetip, Toast.LENGTH_SHORT).show();
					return;
				}
				NetConnectBle.getInstance().configCode(chipType, startCode, spacerCode);
				finish();
			}
		});
		
		// 设置Startcode按钮
		setStartcodeBtn = findButtonById(R.id.btn_startcode_set);
		setStartcodeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue(0, Constant.PIX_STARTCODE_TYPE, Constant.PIX_STARTCODE_KEY); // 起始码 0
			}
		});
		startcodeNum = findButtonById(R.id.startcodeNum);
		startcodeNum.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue(0, Constant.PIX_STARTCODE_TYPE, Constant.PIX_STARTCODE_KEY); // 起始码 0
			}
		});
		

		// 设置Spacercode按钮
		setSpacercodeBtn = findButtonById(R.id.btn_spacercode_set);
		setSpacercodeBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue(1, Constant.PIX_SPACERCODE_TYPE, Constant.PIX_SPACERCODE_KEY); //间隔码  1  
			}
		});
		spacercodeNum = findButtonById(R.id.spacercodeNum);
		spacercodeNum.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue(1, Constant.PIX_SPACERCODE_TYPE, Constant.PIX_SPACERCODE_KEY); //间隔码  1  
			}
		});
		
		
		
//		pixNumber = findButtonById(R.id.tv_pix_num);

		// 选择结果 展示
//		chipTvResult = findTextViewById(R.id.tv_chip_result);
		rgbTvResult = findTextViewById(R.id.tv_rgb_result);
		spacercodeTvResult = findTextViewById(R.id.tv_spacercode_result);
		startcodeTvResult = findTextViewById(R.id.tv_startcode_result);
		

		// 取出RGB值并赋值
		if (mLeftLv != null && rgbTvResult != null) {

			// String value = getData(Constant.RGB_SORT_TYPE,
			// Constant.RGB_SORT_TYPE_KEY);
			String value = SharePersistent.getConfigData(mContext, Constant.CHIP_TYPE, Constant.CHIP_TYPE_KEY);
			if (value == null || value.length() <= 0) {// 判断取出的数据是否非空
				chipType = 2;
				rgbTvResult.setText("UCS512C");
				rgbAdapter.setIndex(0);
				rgbAdapter.notifyDataSetChanged();
				mLeftLv.setSelection(0);
			}else {
				String[] tempData = null;
				tempData = value.split(",");
				rgbTvResult.setText(tempData[0]);
		
				rgbAdapter.setIndex(Integer.parseInt(tempData[1]));
				rgbAdapter.notifyDataSetChanged();
				mLeftLv.setSelection(Integer.parseInt(tempData[1]));
				chipType = Integer.parseInt(tempData[1]);
			}
			
		}
		
		

		// 取出spacercode值并赋值
		if (spacercodeTvResult != null) {

			// String value = getData(Constant.RGB_SORT_TYPE,
			// Constant.RGB_SORT_TYPE_KEY);
			String value = SharePersistent.getConfigData(mContext, Constant.PIX_SPACERCODE_TYPE, Constant.PIX_SPACERCODE_KEY);
			if (value == null || value.length() <= 0) { // 判断取出的数据是否非空
				spacerCode = 3;
				spacercodeTvResult.setText("3");
			}else {
				String[] tempData = null;
				tempData = value.split(",");
				spacercodeTvResult.setText(tempData[0]);
				spacercodeNum.setText(tempData[0]);
				spacerCode = Integer.parseInt(tempData[0]);
			}			
			
		}

		// 取出startcode值并赋值
		if (startcodeTvResult != null) {
			// String value = getData(Constant.PIX_VALUE,
			// Constant.PIX_VALUE_KEY);
			String value = SharePersistent.getConfigData(mContext, Constant.PIX_STARTCODE_TYPE, Constant.PIX_STARTCODE_KEY);
			if (value == null || value.length() <= 0) { // 判断取出的数据是否非空
				startCode = 0;
				startcodeTvResult.setText("0");
			}else {
				String[] tempData = null;
				tempData = value.split(",");
				startcodeTvResult.setText(tempData[0]);
				startcodeNum.setText(tempData[0]);
				startCode = Integer.parseInt(tempData[0]);
			}
			
		}

		// 芯片选择OK按钮
//		chipselectOKBtn = findButtonById(R.id.btn_chipselect_ok);
//		chipselectOKBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix,
//						bannerSort);
//				finish();
//			}
//		});
	}
	

	private RGBSelectAdapter buildRGBModel() {
		String[] ary = mContext.getResources().getStringArray(R.array.chip_model);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		rgbAdapter = new RGBSelectAdapter(mContext, abs);
		return rgbAdapter;
	}

	private ChipSelectAdapter buildChipModel() {
		String[] ary = mContext.getResources().getStringArray(R.array.light_banner);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		chipAdapter = new ChipSelectAdapter(mContext, abs);
		return chipAdapter;
	}

	// 设置像素值
	private void setPixValue(final int status, String pixType, String pixKey) {

		final EditText editText = new EditText(mContext);
		new AlertDialog.Builder(mContext).setTitle(R.string.btn_pix_number).setIcon(android.R.drawable.ic_dialog_info)
				.setView(editText).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						String gnameString = editText.getText().toString();
						
						if (gnameString.length() <= 0 && 0 == status) { //起始码
							Toast.makeText(mContext, R.string.startcodevaluetip, Toast.LENGTH_SHORT).show();
							return;
						}
						if (gnameString.length() <= 0 && 1 == status) { //间隔码
							Toast.makeText(mContext, R.string.spacercodevaluetip, Toast.LENGTH_SHORT).show();
							return;
						}
						
						int value = Integer.parseInt(gnameString);
						
						if (value < 0) {
							Toast.makeText(mContext, R.string.valuetip, Toast.LENGTH_SHORT).show();
							return;
						}
						
						String valueStr = String.valueOf(value);
						if (!StringUtils.isEmpty(gnameString)) {						
							
							if (1 == status) { //间隔码
								spacercodeTvResult.setText(valueStr);
								spacercodeNum.setText(valueStr);
								spacerCode = Integer.parseInt(valueStr);
								SharePersistent.saveConfigData(mContext, Constant.PIX_SPACERCODE_TYPE, Constant.PIX_SPACERCODE_KEY,
										valueStr, 0);// 保存间隔码值
							}else {	//起始码
								startcodeTvResult.setText(valueStr);
								startcodeNum.setText(valueStr);
								startCode = Integer.parseInt(valueStr);
								SharePersistent.saveConfigData(mContext, Constant.PIX_STARTCODE_TYPE, Constant.PIX_STARTCODE_KEY,
										valueStr, 0);// 保存起始码值
							}
							
						}
					}
				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

}

