package com.ledble.activity;

import com.afled.R;
import com.ledble.http.AdvertBean;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class AdvertActivity extends Activity {
	
	private String TAG = "AdvertActivity";
	
	private ImageView ivAdvertImage;
	private TextView tvText;
	
	private AdvertBean advertBean;
    //计时器
    private CountDownTimer timer;
    
    private String ADVERT_KEY = "advertKey";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_advert);
		ivAdvertImage = (ImageView) findViewById(R.id.ivAdvertImage);
		tvText = (TextView) findViewById(R.id.tvText);
		
		advertBean = (AdvertBean) getIntent().getSerializableExtra("advertBean");
		if (advertBean != null) {
			SharedPreferences sf = getSharedPreferences("Advert", Context.MODE_PRIVATE);
			int advertValue = sf.getInt(ADVERT_KEY, 0);
			int advertId = advertBean.getAdvertId();
			Log.d(TAG, "advertKey:" + advertValue);
			Log.d(TAG, "advertId:" + advertId);
			if (advertId > advertValue) {
				Editor editor = sf.edit();
				editor.putInt(ADVERT_KEY, advertId);
				editor.commit();
			} else {
				gotoMainPage();
			}
		} else {
			gotoMainPage();
		}
		
		timer = new CountDownTimer(6 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            	Log.d(TAG, "onTick  " + millisUntilFinished / 1000);
            	tvText.setText(millisUntilFinished / 1000 + "S");
            }

            @Override
            public void onFinish() {
            	gotoMainPage();
            }
        };

		if (advertBean != null) {
			ImageLoader.getInstance().displayImage(advertBean.getImageVisitUrl(), ivAdvertImage);
			ivAdvertImage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					/*String url = advertBean.getAdvertUrl();
					if (!"".equals(url) && (url.startsWith("http://") || url.startsWith("https://"))) {
						Intent intent = new Intent(Intent.ACTION_VIEW);
						Uri uri = Uri.parse(url);
						intent.setData(uri);
						startActivity(intent);
					}*/
				}
			});
		}
		
		findViewById(R.id.llSkip).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				gotoMainPage();
			}
		});;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume");
		if (timer != null) {
			// 开始计时
			timer.start();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (timer != null) {
			timer.cancel();
		}
	}
	
	private void gotoMainPage() {
		Intent intent = new Intent(AdvertActivity.this, MainActivity.class);
		startActivity(intent);
		finish();
	}

}
