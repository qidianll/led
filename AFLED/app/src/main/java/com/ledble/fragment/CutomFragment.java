package com.ledble.fragment;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import butterknife.Bind;
import android.widget.TextView;

import com.afled.R;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.ListUtiles;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;

import com.ledble.activity.MainActivity;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.MyColor;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView4RGB;
/**
 * 单色
 * @author ftl
 *
 */
public class CutomFragment extends LedBleFragment {
	
	@Bind(R.id.changeButton) SegmentedRadioGroup changeRadioGroup;  // 渐变,拖尾，流水，追光
	@Bind(R.id.imageViewOnOff) ImageView imageViewOnOff;
	
	@Bind(R.id.seekBarSpeedCustom) SeekBar seekBarSpeedCustom;
	@Bind(R.id.textViewSpeedCustom) TextView textViewSpeedCustom;
	@Bind(R.id.seekBarBrightCustom) SeekBar seekBarBrightCustom;
	@Bind(R.id.textViewBrightCustom) TextView textViewBrightCustom;
	
	@Bind(R.id.button1) Button buttonRunButton;// 运行

	private View mContentView;
	private MainActivity mActivity;

	private int offOnBtnState;
	private int style = 0;
	private ArrayList<ColorTextView> colorTextViews;
	private SharedPreferences sp;
	private String diyViewTag ;
	private ColorTextView actionView;
	private int currentSelecColorFromPicker;

	private int currentTab = 1;// 1：色环，2：模式
	private View menuView;
	private PopupWindow mPopupWindow;
	private SegmentedRadioGroup srgCover;
	private TextView textRGB;
	private LinearLayout llRing;
	private LinearLayout llCover;
	private TextView tvCoverModel;
	private ListView lvCover;
	private MyColorPickerImageView4RGB imageViewPicker2;
	private BlackWiteSelectView blackWiteSelectView2;
	private TextView textViewRingBrightSC;
	private SeekBar seekBarSpeedBarSC;
	private TextView textViewSpeedSC;
	private SeekBar seekBarBrightBarSC;
	private TextView textViewBrightSC;
	private Button buttonSelectColorConfirm;// 确认
//	
	private static final int COLOR_DEFALUT = 0;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_custom, container, false);
		menuView = inflater.inflate(R.layout.activity_select_color, container, false);
		return mContentView;
	}

	@Override
	public void initData() {		
	}

	@Override
	public void initView() {
		
		mActivity = (MainActivity) getActivity();
		
		sp = mActivity.getSharedPreferences(Constant.SPF_DIY, Context.MODE_PRIVATE);
		changeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				
				if (R.id.changeButtonOne == checkedId) {
					style = 0;			
				} else if ((R.id.changeButtonTwo == checkedId)) {
					style = 1;	
				} else if ((R.id.changeButtonThree == checkedId)) {
					style = 2;
				} else if ((R.id.changeButtonFour == checkedId)) {
					style = 3;
				}				
				
			}
		});
		
		
		this.seekBarSpeedCustom.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@SuppressLint({"StringFormatInvalid", "StringFormatMatches"})
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					mActivity.setSpeed(1);
					textViewSpeedCustom.setText(mActivity.getResources().getString(R.string.speed_set, 1));

				} else {
					mActivity.setSpeed(progress);
					textViewSpeedCustom.setText(mActivity.getResources().getString(R.string.speed_set, progress));

				}
			}
		});

		this.seekBarBrightCustom.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@SuppressLint({"StringFormatInvalid", "StringFormatMatches"})
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					mActivity.setBrightNess(1);
					textViewBrightCustom.setText(mActivity.getResources().getString(R.string.brightness_set, 1));

				} else {
					mActivity.setBrightNess((progress*32)/100);
					textViewBrightCustom.setText(mActivity.getResources().getString(R.string.brightness_set, progress));

				}
			}
		});
			
	
		
		buttonRunButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mActivity.setDiy(getSelectColor(), style);
			}
		});
		
		
		imageViewOnOff.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (offOnBtnState == 0) {
					imageViewOnOff.setBackgroundResource(R.drawable.on_btn);
					offOnBtnState = 1;
					mActivity.open();

				} else {
					imageViewOnOff.setBackgroundResource(R.drawable.off_btn);
					offOnBtnState = 0;
					mActivity.close();
				}
			}
		});
			

		buttonSelectColorConfirm = (Button) menuView.findViewById(R.id.buttonSelectColorConfirm); // 通过另外一个布局对象的findViewById获取其中的控件
		textRGB = (TextView) menuView.findViewById(R.id.tvRGB);
		srgCover = (SegmentedRadioGroup) menuView.findViewById(R.id.srgCover);
		llRing = (LinearLayout) menuView.findViewById(R.id.llRing);
		llCover = (LinearLayout) menuView.findViewById(R.id.llCover);
		tvCoverModel = (TextView) menuView.findViewById(R.id.tvCoverModel);
		lvCover = (ListView) menuView.findViewById(R.id.lvCover);
		imageViewPicker2 = (MyColorPickerImageView4RGB) menuView.findViewById(R.id.imageViewPicker2);
		blackWiteSelectView2 = (BlackWiteSelectView) menuView.findViewById(R.id.blackWiteSelectView2);
		seekBarSpeedBarSC = (SeekBar) menuView.findViewById(R.id.seekBarSpeed);
		textViewSpeedSC = (TextView) menuView.findViewById(R.id.textViewSpeed);
		seekBarBrightBarSC = (SeekBar) menuView.findViewById(R.id.seekBarBrightNess);
		textViewBrightSC = (TextView) menuView.findViewById(R.id.textViewBrightNess);
		textViewRingBrightSC = (TextView) menuView.findViewById(R.id.tvRingBrightnessSC);
		
		
		initColorBlock();
		initColorSelecterView();
		
	}

	@Override
	public void initEvent() {

	}
	
	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
		View blocks = mContentView.findViewById(R.id.linearLayoutViewBlocks);
		// 16个自定义view
		colorTextViews = new ArrayList<ColorTextView>();
		for (int i = 1; i <= 16; i++) {
			final ColorTextView tv = (ColorTextView) blocks.findViewWithTag((String.valueOf("labelColor") + i));
			String tag = (String) tv.getTag();
			int color = sp.getInt(tag, COLOR_DEFALUT);
			if (color != COLOR_DEFALUT) {
				int radius = 10;
				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
				// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
				// 组合圆角矩形和ShapeDrawable
				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
				// 设置形状的颜色
				shapeDrawable.getPaint().setColor(color);
				// 设置绘制方式为填充
				shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
				// 将当前选择得颜色设置到触发颜色编辑器得View上
				tv.setBackgroundDrawable(shapeDrawable);
				tv.setColor(color);
				tv.setText("");
			}
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.layout_scale));
					int color = tv.getColor();
					if (color == COLOR_DEFALUT) {
						showColorCover((ColorTextView) v, false);
					}else {
						updateRgbText(Tool.getRGB(color), true);
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(COLOR_DEFALUT);
					String tag = (String) tv.getTag();
					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
				colorTextViews.add(tv);
		}

	}
	
	public Drawable getImage(String value) {
		int resID = mActivity.getResources().getIdentifier("img_" + value, "drawable", "com.ledble");
		return mActivity.getResources().getDrawable(resID);
	}
	
	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 * @param
	 */
	@SuppressLint({"StringFormatInvalid", "StringFormatMatches"})
	public void showColorCover(ColorTextView actionView, final boolean hasBrightNess) {
		// 数据初始化
		this.actionView = actionView;
		currentSelecColorFromPicker = COLOR_DEFALUT;
		srgCover.check(R.id.rbRing);
		tvCoverModel.setText(mActivity.getResources().getString(R.string.current_mode));
//		maAdapter.setIndex(COLOR_DEFALUT);
//		maAdapter.notifyDataSetChanged();
		textRGB.setText(mActivity.getString(R.string.r_g_b, 0, 0, 0));

		if (hasBrightNess) {
			srgCover.setVisibility(View.INVISIBLE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.VISIBLE);
		} else {
			srgCover.setVisibility(View.INVISIBLE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.GONE);
			textViewRingBrightSC.setVisibility(View.GONE);
		}

		mPopupWindow = new PopupWindow(menuView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
		mPopupWindow.showAtLocation(mContentView, Gravity.BOTTOM, 0, 0);

	}
	
	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {

		imageViewPicker2.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@SuppressLint("StringFormatMatches")
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView2.setStartColor(color);
				currentSelecColorFromPicker = color;

				int[] colors = Tool.getRGB(color);
				updateRgbText(colors, false);
				textRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				SharePersistent.saveBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright", 32);
			}
		});

		blackWiteSelectView2.setOnSelectColor(new OnSelectColor() {
			@SuppressLint("StringFormatMatches")
			@Override
			public void onColorSelect(int color, int progress) {
//				currentSelecColorFromPicker = color;
				mActivity.pauseMusicAndVolum(true);
				int p = progress;
				if (progress<=0) {
					p = -3;
				}else if (progress >= 100) {
					p = 100;
				}
				if (p > 95) {
			        return;
			    }
			    
				p = (p + 3)/3;
				textViewRingBrightSC.setText(mActivity.getResources().getString(R.string.brightness_set, p));
				SharePersistent.saveBrightData(mActivity, diyViewTag+"bright", diyViewTag+"bright", p);
				mActivity.setBrightNess(p);
			}
		});

		View viewColors = menuView.findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@SuppressLint("StringFormatMatches")
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView2.setStartColor(selectColor);
					imageViewPicker2.move2Ege(rmap.get(selectColor));
					updateRgbText(Tool.getRGB(selectColor), true);
					int[] colors = Tool.getRGB(selectColor);
					textRGB.setText(mActivity.getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				}
			});
			viewCsArrayLis.add(vc);
		}

		buttonSelectColorConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (currentSelecColorFromPicker != COLOR_DEFALUT) {
					if (currentTab == 1) {
						int radius = 10;
						float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
						// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
						// 就会根据形状来绘制。
						RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
						// 组合圆角矩形和ShapeDrawable
						ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
						// 设置形状的颜色
						shapeDrawable.getPaint().setColor(currentSelecColorFromPicker);
						// 设置绘制方式为填充
						shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						sp.edit().putInt(tag, currentSelecColorFromPicker).commit();
						// 将当前选择得颜色设置到触发颜色编辑器得View上
						actionView.setBackgroundDrawable(shapeDrawable);
					} else {
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						if (null != tvCoverModel) {
							if (null != tvCoverModel.getTag().toString()) {
								String value = tvCoverModel.getTag().toString();
								sp.edit().putInt(tag, Integer.parseInt(value)).commit();
								Drawable image = getImage(value);
								actionView.setBackgroundDrawable(image);
							}
						}
					}
					actionView.setText("");
				}
				
				hideColorCover();
			}
		});
	}
	
	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		mPopupWindow.dismiss(); // 隐藏
	}
	
	@SuppressLint("StringFormatMatches")
	public void updateRgbText(int rgb[], boolean isClick) {
		try {
			mActivity.pauseMusicAndVolum(true);
			textRGB.setText(mActivity.getString(R.string.r_g_b, rgb[0], rgb[1], rgb[2]));
			mActivity.setRgb(rgb[0], rgb[1], rgb[2]);
		} catch (Exception e) {
			e.printStackTrace();
			Tool.ToastShow(mActivity, "错误。。。");
		}
	}
	
	private ArrayList<MyColor> getSelectColor() {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		if (!ListUtiles.isEmpty(colorTextViews)) {
			for (ColorTextView ctx : colorTextViews) {
				if (COLOR_DEFALUT != ctx.getColor()) {
					int[] rgb = Tool.getRGB(ctx.getColor());
					colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
				}
			}
		}
		return colorList;
	}

}
