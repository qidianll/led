// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.wifi;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RgbFragment_WiFi$$ViewBinder<T extends com.ledble.fragment.wifi.RgbFragment_WiFi> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231201, "field 'relativeTab1'");
    target.relativeTab1 = view;
    view = finder.findRequiredView(source, 2131231202, "field 'relativeTab2'");
    target.relativeTab2 = view;
    view = finder.findRequiredView(source, 2131231203, "field 'relativeTab3'");
    target.relativeTab3 = view;
    view = finder.findRequiredView(source, 2131230935, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230935, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131231666, "field 'textViewRGB'");
    target.textViewRGB = finder.castView(view, 2131231666, "field 'textViewRGB'");
    view = finder.findRequiredView(source, 2131230939, "field 'imageViewPicker'");
    target.imageViewPicker = finder.castView(view, 2131230939, "field 'imageViewPicker'");
    view = finder.findRequiredView(source, 2131230766, "field 'blackWiteSelectView'");
    target.blackWiteSelectView = finder.castView(view, 2131230766, "field 'blackWiteSelectView'");
    view = finder.findRequiredView(source, 2131231583, "field 'tvBrightness'");
    target.tvBrightness = finder.castView(view, 2131231583, "field 'tvBrightness'");
    view = finder.findRequiredView(source, 2131231374, "field 'textViewWarmCool'");
    target.textViewWarmCool = finder.castView(view, 2131231374, "field 'textViewWarmCool'");
    view = finder.findRequiredView(source, 2131231160, "field 'pikerImageView'");
    target.pikerImageView = finder.castView(view, 2131231160, "field 'pikerImageView'");
    view = finder.findRequiredView(source, 2131231252, "field 'seekBarBrightNess'");
    target.seekBarBrightNess = finder.castView(view, 2131231252, "field 'seekBarBrightNess'");
    view = finder.findRequiredView(source, 2131231341, "field 'textViewBrightNess'");
    target.textViewBrightNess = finder.castView(view, 2131231341, "field 'textViewBrightNess'");
    view = finder.findRequiredView(source, 2131230937, "field 'imageViewOnOffCT'");
    target.imageViewOnOffCT = finder.castView(view, 2131230937, "field 'imageViewOnOffCT'");
    view = finder.findRequiredView(source, 2131231343, "field 'textViewBrightNessDim'");
    target.textViewBrightNessDim = finder.castView(view, 2131231343, "field 'textViewBrightNessDim'");
    view = finder.findRequiredView(source, 2131231161, "field 'pikerImageViewDim'");
    target.pikerImageViewDim = finder.castView(view, 2131231161, "field 'pikerImageViewDim'");
    view = finder.findRequiredView(source, 2131230938, "field 'imageViewOnOffDim'");
    target.imageViewOnOffDim = finder.castView(view, 2131230938, "field 'imageViewOnOffDim'");
  }

  @Override public void unbind(T target) {
    target.relativeTab1 = null;
    target.relativeTab2 = null;
    target.relativeTab3 = null;
    target.imageViewOnOff = null;
    target.textViewRGB = null;
    target.imageViewPicker = null;
    target.blackWiteSelectView = null;
    target.tvBrightness = null;
    target.textViewWarmCool = null;
    target.pikerImageView = null;
    target.seekBarBrightNess = null;
    target.textViewBrightNess = null;
    target.imageViewOnOffCT = null;
    target.textViewBrightNessDim = null;
    target.pikerImageViewDim = null;
    target.imageViewOnOffDim = null;
  }
}
