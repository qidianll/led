// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.smart;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class SceneFragment_Smart$$ViewBinder<T extends com.ledble.fragment.smart.SceneFragment_Smart> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231202, "field 'relativeTab2'");
    target.relativeTab2 = view;
    view = finder.findRequiredView(source, 2131230954, "field 'imageViewSceneSunrise'");
    target.imageViewSceneSunrise = finder.castView(view, 2131230954, "field 'imageViewSceneSunrise'");
    view = finder.findRequiredView(source, 2131230955, "field 'imageViewSceneSunset'");
    target.imageViewSceneSunset = finder.castView(view, 2131230955, "field 'imageViewSceneSunset'");
    view = finder.findRequiredView(source, 2131230948, "field 'imageViewSceneColorful'");
    target.imageViewSceneColorful = finder.castView(view, 2131230948, "field 'imageViewSceneColorful'");
    view = finder.findRequiredView(source, 2131230946, "field 'imageViewSceneAfternoontea'");
    target.imageViewSceneAfternoontea = finder.castView(view, 2131230946, "field 'imageViewSceneAfternoontea'");
    view = finder.findRequiredView(source, 2131230949, "field 'imageViewSceneDrivemidge'");
    target.imageViewSceneDrivemidge = finder.castView(view, 2131230949, "field 'imageViewSceneDrivemidge'");
    view = finder.findRequiredView(source, 2131230957, "field 'imageViewSceneYoga'");
    target.imageViewSceneYoga = finder.castView(view, 2131230957, "field 'imageViewSceneYoga'");
    view = finder.findRequiredView(source, 2131230950, "field 'imageViewSceneParty'");
    target.imageViewSceneParty = finder.castView(view, 2131230950, "field 'imageViewSceneParty'");
    view = finder.findRequiredView(source, 2131230956, "field 'imageViewSceneTropical'");
    target.imageViewSceneTropical = finder.castView(view, 2131230956, "field 'imageViewSceneTropical'");
    view = finder.findRequiredView(source, 2131230953, "field 'imageViewSceneSea'");
    target.imageViewSceneSea = finder.castView(view, 2131230953, "field 'imageViewSceneSea'");
    view = finder.findRequiredView(source, 2131230951, "field 'imageViewSceneReading'");
    target.imageViewSceneReading = finder.castView(view, 2131230951, "field 'imageViewSceneReading'");
    view = finder.findRequiredView(source, 2131230947, "field 'imageViewSceneCandlelightdinner'");
    target.imageViewSceneCandlelightdinner = finder.castView(view, 2131230947, "field 'imageViewSceneCandlelightdinner'");
  }

  @Override public void unbind(T target) {
    target.relativeTab2 = null;
    target.imageViewSceneSunrise = null;
    target.imageViewSceneSunset = null;
    target.imageViewSceneColorful = null;
    target.imageViewSceneAfternoontea = null;
    target.imageViewSceneDrivemidge = null;
    target.imageViewSceneYoga = null;
    target.imageViewSceneParty = null;
    target.imageViewSceneTropical = null;
    target.imageViewSceneSea = null;
    target.imageViewSceneReading = null;
    target.imageViewSceneCandlelightdinner = null;
  }
}
