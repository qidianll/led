// Generated code from Butter Knife. Do not modify!
package com.start;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class HomePageFragment$$ViewBinder<T extends com.start.HomePageFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231314, "field 'srlComment'");
    target.srlComment = finder.castView(view, 2131231314, "field 'srlComment'");
    view = finder.findRequiredView(source, 2131231135, "field 'lvComment'");
    target.lvComment = finder.castView(view, 2131231135, "field 'lvComment'");
  }

  @Override public void unbind(T target) {
    target.srlComment = null;
    target.lvComment = null;
  }
}
