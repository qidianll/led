// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.wifi;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TimerFragment_WiFi$$ViewBinder<T extends com.ledble.fragment.wifi.TimerFragment_WiFi> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231015, "field 'linearGroups'");
    target.linearGroups = finder.castView(view, 2131231015, "field 'linearGroups'");
  }

  @Override public void unbind(T target) {
    target.linearGroups = null;
  }
}
