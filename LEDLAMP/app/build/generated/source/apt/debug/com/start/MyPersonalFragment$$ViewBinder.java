// Generated code from Butter Knife. Do not modify!
package com.start;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MyPersonalFragment$$ViewBinder<T extends com.start.MyPersonalFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231721, "field 'tvUserName'");
    target.tvUserName = finder.castView(view, 2131231721, "field 'tvUserName'");
    view = finder.findRequiredView(source, 2131231613, "field 'tvEmail'");
    target.tvEmail = finder.castView(view, 2131231613, "field 'tvEmail'");
    view = finder.findRequiredView(source, 2131230779, "field 'btnUpdate'");
    target.btnUpdate = finder.castView(view, 2131230779, "field 'btnUpdate'");
    view = finder.findRequiredView(source, 2131230774, "field 'btnLogout'");
    target.btnLogout = finder.castView(view, 2131230774, "field 'btnLogout'");
  }

  @Override public void unbind(T target) {
    target.tvUserName = null;
    target.tvEmail = null;
    target.btnUpdate = null;
    target.btnLogout = null;
  }
}
