// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.ble;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ModeFragment$$ViewBinder<T extends com.ledble.fragment.ble.ModeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231079, "field 'listViewModel'");
    target.listViewModel = finder.castView(view, 2131231079, "field 'listViewModel'");
    view = finder.findRequiredView(source, 2131231346, "field 'textViewCurretModel'");
    target.textViewCurretModel = finder.castView(view, 2131231346, "field 'textViewCurretModel'");
    view = finder.findRequiredView(source, 2131230935, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230935, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131231270, "field 'seekBarMode'");
    target.seekBarMode = finder.castView(view, 2131231270, "field 'seekBarMode'");
    view = finder.findRequiredView(source, 2131231354, "field 'textViewMode'");
    target.textViewMode = finder.castView(view, 2131231354, "field 'textViewMode'");
    view = finder.findRequiredView(source, 2131231279, "field 'seekBarSpeedBar'");
    target.seekBarSpeedBar = finder.castView(view, 2131231279, "field 'seekBarSpeedBar'");
    view = finder.findRequiredView(source, 2131231367, "field 'textViewSpeed'");
    target.textViewSpeed = finder.castView(view, 2131231367, "field 'textViewSpeed'");
    view = finder.findRequiredView(source, 2131231252, "field 'seekBarBrightness'");
    target.seekBarBrightness = finder.castView(view, 2131231252, "field 'seekBarBrightness'");
    view = finder.findRequiredView(source, 2131231341, "field 'textViewBrightness'");
    target.textViewBrightness = finder.castView(view, 2131231341, "field 'textViewBrightness'");
  }

  @Override public void unbind(T target) {
    target.listViewModel = null;
    target.textViewCurretModel = null;
    target.imageViewOnOff = null;
    target.seekBarMode = null;
    target.textViewMode = null;
    target.seekBarSpeedBar = null;
    target.textViewSpeed = null;
    target.seekBarBrightness = null;
    target.textViewBrightness = null;
  }
}
