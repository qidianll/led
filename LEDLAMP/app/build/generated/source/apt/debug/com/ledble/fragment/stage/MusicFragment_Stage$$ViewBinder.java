// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.stage;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MusicFragment_Stage$$ViewBinder<T extends com.ledble.fragment.stage.MusicFragment_Stage> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230944, "field 'imageViewPre'");
    target.imageViewPre = finder.castView(view, 2131230944, "field 'imageViewPre'");
    view = finder.findRequiredView(source, 2131230941, "field 'imageViewPlay'");
    target.imageViewPlay = finder.castView(view, 2131230941, "field 'imageViewPlay'");
    view = finder.findRequiredView(source, 2131230934, "field 'imageViewNext'");
    target.imageViewNext = finder.castView(view, 2131230934, "field 'imageViewNext'");
    view = finder.findRequiredView(source, 2131231338, "field 'textViewAutoAjust'");
    target.textViewAutoAjust = finder.castView(view, 2131231338, "field 'textViewAutoAjust'");
    view = finder.findRequiredView(source, 2131231610, "field 'tvCurrentTime'");
    target.tvCurrentTime = finder.castView(view, 2131231610, "field 'tvCurrentTime'");
    view = finder.findRequiredView(source, 2131231720, "field 'tvTotalTime'");
    target.tvTotalTime = finder.castView(view, 2131231720, "field 'tvTotalTime'");
    view = finder.findRequiredView(source, 2131231122, "field 'llDecibel'");
    target.llDecibel = finder.castView(view, 2131231122, "field 'llDecibel'");
    view = finder.findRequiredView(source, 2131231266, "field 'seekBarDecibel'");
    target.seekBarDecibel = finder.castView(view, 2131231266, "field 'seekBarDecibel'");
    view = finder.findRequiredView(source, 2131231612, "field 'tvDecibelValue'");
    target.tvDecibelValue = finder.castView(view, 2131231612, "field 'tvDecibelValue'");
    view = finder.findRequiredView(source, 2131231118, "field 'llBottom'");
    target.llBottom = finder.castView(view, 2131231118, "field 'llBottom'");
    view = finder.findRequiredView(source, 2131231271, "field 'seekBarMusic'");
    target.seekBarMusic = finder.castView(view, 2131231271, "field 'seekBarMusic'");
    view = finder.findRequiredView(source, 2131230945, "field 'imageViewRotate'");
    target.imageViewRotate = finder.castView(view, 2131230945, "field 'imageViewRotate'");
    view = finder.findRequiredView(source, 2131230943, "field 'imageViewPlayType'");
    target.imageViewPlayType = finder.castView(view, 2131230943, "field 'imageViewPlayType'");
    view = finder.findRequiredView(source, 2131230793, "field 'buttonMusicLib'");
    target.buttonMusicLib = finder.castView(view, 2131230793, "field 'buttonMusicLib'");
    view = finder.findRequiredView(source, 2131231278, "field 'seekBarRhythm'");
    target.seekBarRhythm = finder.castView(view, 2131231278, "field 'seekBarRhythm'");
    view = finder.findRequiredView(source, 2131231787, "field 'volumCircleBar'");
    target.volumCircleBar = finder.castView(view, 2131231787, "field 'volumCircleBar'");
    view = finder.findRequiredView(source, 2131231673, "field 'tvRhythm'");
    target.tvRhythm = finder.castView(view, 2131231673, "field 'tvRhythm'");
    view = finder.findRequiredView(source, 2131231744, "field 'tvrhythmValue'");
    target.tvrhythmValue = finder.castView(view, 2131231744, "field 'tvrhythmValue'");
  }

  @Override public void unbind(T target) {
    target.imageViewPre = null;
    target.imageViewPlay = null;
    target.imageViewNext = null;
    target.textViewAutoAjust = null;
    target.tvCurrentTime = null;
    target.tvTotalTime = null;
    target.llDecibel = null;
    target.seekBarDecibel = null;
    target.tvDecibelValue = null;
    target.llBottom = null;
    target.seekBarMusic = null;
    target.imageViewRotate = null;
    target.imageViewPlayType = null;
    target.buttonMusicLib = null;
    target.seekBarRhythm = null;
    target.volumCircleBar = null;
    target.tvRhythm = null;
    target.tvrhythmValue = null;
  }
}
