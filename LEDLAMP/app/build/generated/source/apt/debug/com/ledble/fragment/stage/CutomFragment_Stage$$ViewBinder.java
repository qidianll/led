// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.stage;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CutomFragment_Stage$$ViewBinder<T extends com.ledble.fragment.stage.CutomFragment_Stage> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230808, "field 'changeRadioGroup'");
    target.changeRadioGroup = finder.castView(view, 2131230808, "field 'changeRadioGroup'");
    view = finder.findRequiredView(source, 2131230935, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230935, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131231281, "field 'seekBarSpeedCustom'");
    target.seekBarSpeedCustom = finder.castView(view, 2131231281, "field 'seekBarSpeedCustom'");
    view = finder.findRequiredView(source, 2131231369, "field 'textViewSpeedCustom'");
    target.textViewSpeedCustom = finder.castView(view, 2131231369, "field 'textViewSpeedCustom'");
    view = finder.findRequiredView(source, 2131231251, "field 'seekBarBrightCustom'");
    target.seekBarBrightCustom = finder.castView(view, 2131231251, "field 'seekBarBrightCustom'");
    view = finder.findRequiredView(source, 2131231340, "field 'textViewBrightCustom'");
    target.textViewBrightCustom = finder.castView(view, 2131231340, "field 'textViewBrightCustom'");
    view = finder.findRequiredView(source, 2131230783, "field 'buttonRunButton'");
    target.buttonRunButton = finder.castView(view, 2131230783, "field 'buttonRunButton'");
  }

  @Override public void unbind(T target) {
    target.changeRadioGroup = null;
    target.imageViewOnOff = null;
    target.seekBarSpeedCustom = null;
    target.textViewSpeedCustom = null;
    target.seekBarBrightCustom = null;
    target.textViewBrightCustom = null;
    target.buttonRunButton = null;
  }
}
