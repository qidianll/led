// Generated code from Butter Knife. Do not modify!
package com.ledble.activity.ble;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity_STAGE$$ViewBinder<T extends com.ledble.activity.ble.MainActivity_STAGE> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231287, "field 'segmentDm'");
    target.segmentDm = finder.castView(view, 2131231287, "field 'segmentDm'");
    view = finder.findRequiredView(source, 2131231286, "field 'segmentCt'");
    target.segmentCt = finder.castView(view, 2131231286, "field 'segmentCt'");
    view = finder.findRequiredView(source, 2131231289, "field 'segmentRgb'");
    target.segmentRgb = finder.castView(view, 2131231289, "field 'segmentRgb'");
    view = finder.findRequiredView(source, 2131231355, "field 'textViewModeTitle'");
    target.textViewModeTitle = finder.castView(view, 2131231355, "field 'textViewModeTitle'");
    view = finder.findRequiredView(source, 2131231348, "field 'textViewCustomTitle'");
    target.textViewCustomTitle = finder.castView(view, 2131231348, "field 'textViewCustomTitle'");
    view = finder.findRequiredView(source, 2131231344, "field 'textViewBrightnessTitle'");
    target.textViewBrightnessTitle = finder.castView(view, 2131231344, "field 'textViewBrightnessTitle'");
    view = finder.findRequiredView(source, 2131231221, "field 'rlTimerAddButton'");
    target.rlTimerAddButton = finder.castView(view, 2131231221, "field 'rlTimerAddButton'");
    view = finder.findRequiredView(source, 2131231378, "field 'timerAddButton'");
    target.timerAddButton = finder.castView(view, 2131231378, "field 'timerAddButton'");
    view = finder.findRequiredView(source, 2131230977, "field 'ivLeftMenu'");
    target.ivLeftMenu = finder.castView(view, 2131230977, "field 'ivLeftMenu'");
    view = finder.findRequiredView(source, 2131231345, "field 'textViewConnectCount'");
    target.textViewConnectCount = finder.castView(view, 2131231345, "field 'textViewConnectCount'");
    view = finder.findRequiredView(source, 2131230984, "field 'ivRightMenu'");
    target.ivRightMenu = finder.castView(view, 2131230984, "field 'ivRightMenu'");
    view = finder.findRequiredView(source, 2131230990, "field 'ivType'");
    target.ivType = finder.castView(view, 2131230990, "field 'ivType'");
    view = finder.findRequiredView(source, 2131231211, "field 'rgBottom'");
    target.rgBottom = finder.castView(view, 2131231211, "field 'rgBottom'");
    view = finder.findRequiredView(source, 2131231174, "field 'rbFirst'");
    target.rbFirst = finder.castView(view, 2131231174, "field 'rbFirst'");
    view = finder.findRequiredView(source, 2131231185, "field 'rbSecond'");
    target.rbSecond = finder.castView(view, 2131231185, "field 'rbSecond'");
    view = finder.findRequiredView(source, 2131231186, "field 'rbThrid'");
    target.rbThrid = finder.castView(view, 2131231186, "field 'rbThrid'");
    view = finder.findRequiredView(source, 2131231175, "field 'rbFourth'");
    target.rbFourth = finder.castView(view, 2131231175, "field 'rbFourth'");
    view = finder.findRequiredView(source, 2131231173, "field 'rbFifth'");
    target.rbFifth = finder.castView(view, 2131231173, "field 'rbFifth'");
    view = finder.findRequiredView(source, 2131230757, "field 'backLinearLayout'");
    target.backLinearLayout = finder.castView(view, 2131230757, "field 'backLinearLayout'");
    view = finder.findRequiredView(source, 2131230758, "field 'backTextView'");
    target.backTextView = finder.castView(view, 2131230758, "field 'backTextView'");
    view = finder.findRequiredView(source, 2131231155, "field 'onOffButton'");
    target.onOffButton = finder.castView(view, 2131231155, "field 'onOffButton'");
    view = finder.findRequiredView(source, 2131231125, "field 'avtivity_main'");
    target.avtivity_main = finder.castView(view, 2131231125, "field 'avtivity_main'");
    view = finder.findRequiredView(source, 2131231141, "field 'left_menu'");
    target.left_menu = finder.castView(view, 2131231141, "field 'left_menu'");
    view = finder.findRequiredView(source, 2131231216, "field 'right_menu'");
    target.right_menu = finder.castView(view, 2131231216, "field 'right_menu'");
    view = finder.findRequiredView(source, 2131231064, "field 'TopItem'");
    target.TopItem = finder.castView(view, 2131231064, "field 'TopItem'");
    view = finder.findRequiredView(source, 2131231294, "field 'shakeView'");
    target.shakeView = finder.castView(view, 2131231294, "field 'shakeView'");
  }

  @Override public void unbind(T target) {
    target.segmentDm = null;
    target.segmentCt = null;
    target.segmentRgb = null;
    target.textViewModeTitle = null;
    target.textViewCustomTitle = null;
    target.textViewBrightnessTitle = null;
    target.rlTimerAddButton = null;
    target.timerAddButton = null;
    target.ivLeftMenu = null;
    target.textViewConnectCount = null;
    target.ivRightMenu = null;
    target.ivType = null;
    target.rgBottom = null;
    target.rbFirst = null;
    target.rbSecond = null;
    target.rbThrid = null;
    target.rbFourth = null;
    target.rbFifth = null;
    target.backLinearLayout = null;
    target.backTextView = null;
    target.onOffButton = null;
    target.avtivity_main = null;
    target.left_menu = null;
    target.right_menu = null;
    target.TopItem = null;
    target.shakeView = null;
  }
}
