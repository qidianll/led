// Generated code from Butter Knife. Do not modify!
package com.start;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class PublishCommentFragment$$ViewBinder<T extends com.start.PublishCommentFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230898, "field 'etTitle'");
    target.etTitle = finder.castView(view, 2131230898, "field 'etTitle'");
    view = finder.findRequiredView(source, 2131230896, "field 'etContent'");
    target.etContent = finder.castView(view, 2131230896, "field 'etContent'");
    view = finder.findRequiredView(source, 2131230776, "field 'btnPublish'");
    target.btnPublish = finder.castView(view, 2131230776, "field 'btnPublish'");
    view = finder.findRequiredView(source, 2131230914, "field 'gvPhoto'");
    target.gvPhoto = finder.castView(view, 2131230914, "field 'gvPhoto'");
  }

  @Override public void unbind(T target) {
    target.etTitle = null;
    target.etContent = null;
    target.btnPublish = null;
    target.gvPhoto = null;
  }
}
