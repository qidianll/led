// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.spi;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CutomFragment_Spi$$ViewBinder<T extends com.ledble.fragment.spi.CutomFragment_Spi> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230808, "field 'changeRadioGroup'");
    target.changeRadioGroup = finder.castView(view, 2131230808, "field 'changeRadioGroup'");
    view = finder.findRequiredView(source, 2131230815, "field 'changeButtonLine2'");
    target.changeButtonLine2 = finder.castView(view, 2131230815, "field 'changeButtonLine2'");
    view = finder.findRequiredView(source, 2131230816, "field 'changeButtonLine3'");
    target.changeButtonLine3 = finder.castView(view, 2131230816, "field 'changeButtonLine3'");
    view = finder.findRequiredView(source, 2131231079, "field 'listViewModel'");
    target.listViewModel = finder.castView(view, 2131231079, "field 'listViewModel'");
    view = finder.findRequiredView(source, 2131230927, "field 'imageViewDirection'");
    target.imageViewDirection = finder.castView(view, 2131230927, "field 'imageViewDirection'");
    view = finder.findRequiredView(source, 2131231231, "field 'buttonRunButton'");
    target.buttonRunButton = finder.castView(view, 2131231231, "field 'buttonRunButton'");
    view = finder.findRequiredView(source, 2131231253, "field 'seekBarBrightNess'");
    target.seekBarBrightNess = finder.castView(view, 2131231253, "field 'seekBarBrightNess'");
    view = finder.findRequiredView(source, 2131231342, "field 'textViewBrightNess'");
    target.textViewBrightNess = finder.castView(view, 2131231342, "field 'textViewBrightNess'");
    view = finder.findRequiredView(source, 2131231280, "field 'seekBarSpeed'");
    target.seekBarSpeed = finder.castView(view, 2131231280, "field 'seekBarSpeed'");
    view = finder.findRequiredView(source, 2131231368, "field 'textViewSpeed'");
    target.textViewSpeed = finder.castView(view, 2131231368, "field 'textViewSpeed'");
    view = finder.findRequiredView(source, 2131230935, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230935, "field 'imageViewOnOff'");
  }

  @Override public void unbind(T target) {
    target.changeRadioGroup = null;
    target.changeButtonLine2 = null;
    target.changeButtonLine3 = null;
    target.listViewModel = null;
    target.imageViewDirection = null;
    target.buttonRunButton = null;
    target.seekBarBrightNess = null;
    target.textViewBrightNess = null;
    target.seekBarSpeed = null;
    target.textViewSpeed = null;
    target.imageViewOnOff = null;
  }
}
