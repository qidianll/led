// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment.wifi;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BrightFragment_WiFi$$ViewBinder<T extends com.ledble.fragment.wifi.BrightFragment_WiFi> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231023, "field 'linearLayoutTab'");
    target.linearLayoutTab = finder.castView(view, 2131231023, "field 'linearLayoutTab'");
    view = finder.findRequiredView(source, 2131231275, "field 'seekBarRedBrightNess'");
    target.seekBarRedBrightNess = finder.castView(view, 2131231275, "field 'seekBarRedBrightNess'");
    view = finder.findRequiredView(source, 2131231584, "field 'tvBrightness1'");
    target.tvBrightness1 = finder.castView(view, 2131231584, "field 'tvBrightness1'");
    view = finder.findRequiredView(source, 2131231267, "field 'seekBarGreenBrightNess'");
    target.seekBarGreenBrightNess = finder.castView(view, 2131231267, "field 'seekBarGreenBrightNess'");
    view = finder.findRequiredView(source, 2131231592, "field 'tvBrightness2'");
    target.tvBrightness2 = finder.castView(view, 2131231592, "field 'tvBrightness2'");
    view = finder.findRequiredView(source, 2131231248, "field 'seekBarBlueBrightNess'");
    target.seekBarBlueBrightNess = finder.castView(view, 2131231248, "field 'seekBarBlueBrightNess'");
    view = finder.findRequiredView(source, 2131231593, "field 'tvBrightness3'");
    target.tvBrightness3 = finder.castView(view, 2131231593, "field 'tvBrightness3'");
    view = finder.findRequiredView(source, 2131231282, "field 'seekBarWhiteBrightNess'");
    target.seekBarWhiteBrightNess = finder.castView(view, 2131231282, "field 'seekBarWhiteBrightNess'");
    view = finder.findRequiredView(source, 2131231594, "field 'tvBrightness4'");
    target.tvBrightness4 = finder.castView(view, 2131231594, "field 'tvBrightness4'");
    view = finder.findRequiredView(source, 2131231285, "field 'seekBarYellowBrightNess'");
    target.seekBarYellowBrightNess = finder.castView(view, 2131231285, "field 'seekBarYellowBrightNess'");
    view = finder.findRequiredView(source, 2131231595, "field 'tvBrightness5'");
    target.tvBrightness5 = finder.castView(view, 2131231595, "field 'tvBrightness5'");
    view = finder.findRequiredView(source, 2131231272, "field 'seekBarPinkBrightNess'");
    target.seekBarPinkBrightNess = finder.castView(view, 2131231272, "field 'seekBarPinkBrightNess'");
    view = finder.findRequiredView(source, 2131231596, "field 'tvBrightness6'");
    target.tvBrightness6 = finder.castView(view, 2131231596, "field 'tvBrightness6'");
    view = finder.findRequiredView(source, 2131231263, "field 'seekBarCrystalBrightNess'");
    target.seekBarCrystalBrightNess = finder.castView(view, 2131231263, "field 'seekBarCrystalBrightNess'");
    view = finder.findRequiredView(source, 2131231597, "field 'tvBrightness7'");
    target.tvBrightness7 = finder.castView(view, 2131231597, "field 'tvBrightness7'");
    view = finder.findRequiredView(source, 2131231254, "field 'seekBarCH1BrightNess'");
    target.seekBarCH1BrightNess = finder.castView(view, 2131231254, "field 'seekBarCH1BrightNess'");
    view = finder.findRequiredView(source, 2131231598, "field 'tvBrightness8'");
    target.tvBrightness8 = finder.castView(view, 2131231598, "field 'tvBrightness8'");
    view = finder.findRequiredView(source, 2131231255, "field 'seekBarCH2BrightNess'");
    target.seekBarCH2BrightNess = finder.castView(view, 2131231255, "field 'seekBarCH2BrightNess'");
    view = finder.findRequiredView(source, 2131231599, "field 'tvBrightness9'");
    target.tvBrightness9 = finder.castView(view, 2131231599, "field 'tvBrightness9'");
    view = finder.findRequiredView(source, 2131231256, "field 'seekBarCH3BrightNess'");
    target.seekBarCH3BrightNess = finder.castView(view, 2131231256, "field 'seekBarCH3BrightNess'");
    view = finder.findRequiredView(source, 2131231585, "field 'tvBrightness10'");
    target.tvBrightness10 = finder.castView(view, 2131231585, "field 'tvBrightness10'");
    view = finder.findRequiredView(source, 2131231257, "field 'seekBarCH4BrightNess'");
    target.seekBarCH4BrightNess = finder.castView(view, 2131231257, "field 'seekBarCH4BrightNess'");
    view = finder.findRequiredView(source, 2131231586, "field 'tvBrightness11'");
    target.tvBrightness11 = finder.castView(view, 2131231586, "field 'tvBrightness11'");
    view = finder.findRequiredView(source, 2131231258, "field 'seekBarCH5BrightNess'");
    target.seekBarCH5BrightNess = finder.castView(view, 2131231258, "field 'seekBarCH5BrightNess'");
    view = finder.findRequiredView(source, 2131231587, "field 'tvBrightness12'");
    target.tvBrightness12 = finder.castView(view, 2131231587, "field 'tvBrightness12'");
    view = finder.findRequiredView(source, 2131231259, "field 'seekBarCH6BrightNess'");
    target.seekBarCH6BrightNess = finder.castView(view, 2131231259, "field 'seekBarCH6BrightNess'");
    view = finder.findRequiredView(source, 2131231588, "field 'tvBrightness13'");
    target.tvBrightness13 = finder.castView(view, 2131231588, "field 'tvBrightness13'");
    view = finder.findRequiredView(source, 2131231260, "field 'seekBarCH7BrightNess'");
    target.seekBarCH7BrightNess = finder.castView(view, 2131231260, "field 'seekBarCH7BrightNess'");
    view = finder.findRequiredView(source, 2131231589, "field 'tvBrightness14'");
    target.tvBrightness14 = finder.castView(view, 2131231589, "field 'tvBrightness14'");
    view = finder.findRequiredView(source, 2131231261, "field 'seekBarCH8BrightNess'");
    target.seekBarCH8BrightNess = finder.castView(view, 2131231261, "field 'seekBarCH8BrightNess'");
    view = finder.findRequiredView(source, 2131231590, "field 'tvBrightness15'");
    target.tvBrightness15 = finder.castView(view, 2131231590, "field 'tvBrightness15'");
    view = finder.findRequiredView(source, 2131231262, "field 'seekBarCH9BrightNess'");
    target.seekBarCH9BrightNess = finder.castView(view, 2131231262, "field 'seekBarCH9BrightNess'");
    view = finder.findRequiredView(source, 2131231591, "field 'tvBrightness16'");
    target.tvBrightness16 = finder.castView(view, 2131231591, "field 'tvBrightness16'");
  }

  @Override public void unbind(T target) {
    target.linearLayoutTab = null;
    target.seekBarRedBrightNess = null;
    target.tvBrightness1 = null;
    target.seekBarGreenBrightNess = null;
    target.tvBrightness2 = null;
    target.seekBarBlueBrightNess = null;
    target.tvBrightness3 = null;
    target.seekBarWhiteBrightNess = null;
    target.tvBrightness4 = null;
    target.seekBarYellowBrightNess = null;
    target.tvBrightness5 = null;
    target.seekBarPinkBrightNess = null;
    target.tvBrightness6 = null;
    target.seekBarCrystalBrightNess = null;
    target.tvBrightness7 = null;
    target.seekBarCH1BrightNess = null;
    target.tvBrightness8 = null;
    target.seekBarCH2BrightNess = null;
    target.tvBrightness9 = null;
    target.seekBarCH3BrightNess = null;
    target.tvBrightness10 = null;
    target.seekBarCH4BrightNess = null;
    target.tvBrightness11 = null;
    target.seekBarCH5BrightNess = null;
    target.tvBrightness12 = null;
    target.seekBarCH6BrightNess = null;
    target.tvBrightness13 = null;
    target.seekBarCH7BrightNess = null;
    target.tvBrightness14 = null;
    target.seekBarCH8BrightNess = null;
    target.tvBrightness15 = null;
    target.seekBarCH9BrightNess = null;
    target.tvBrightness16 = null;
  }
}
