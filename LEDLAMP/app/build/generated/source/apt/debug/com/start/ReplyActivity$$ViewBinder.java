// Generated code from Butter Knife. Do not modify!
package com.start;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ReplyActivity$$ViewBinder<T extends com.start.ReplyActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230968, "field 'ivBack'");
    target.ivBack = finder.castView(view, 2131230968, "field 'ivBack'");
    view = finder.findRequiredView(source, 2131231719, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131231719, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131231137, "field 'lvReply'");
    target.lvReply = finder.castView(view, 2131231137, "field 'lvReply'");
    view = finder.findRequiredView(source, 2131230778, "field 'btnReply'");
    target.btnReply = finder.castView(view, 2131230778, "field 'btnReply'");
  }

  @Override public void unbind(T target) {
    target.ivBack = null;
    target.tvTitle = null;
    target.lvReply = null;
    target.btnReply = null;
  }
}
