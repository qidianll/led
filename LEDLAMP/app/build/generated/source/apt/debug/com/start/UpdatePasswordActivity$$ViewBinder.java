// Generated code from Butter Knife. Do not modify!
package com.start;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class UpdatePasswordActivity$$ViewBinder<T extends com.start.UpdatePasswordActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230968, "field 'ivBack'");
    target.ivBack = finder.castView(view, 2131230968, "field 'ivBack'");
    view = finder.findRequiredView(source, 2131231719, "field 'tvTitle'");
    target.tvTitle = finder.castView(view, 2131231719, "field 'tvTitle'");
    view = finder.findRequiredView(source, 2131230850, "field 'clearEtOld'");
    target.clearEtOld = finder.castView(view, 2131230850, "field 'clearEtOld'");
    view = finder.findRequiredView(source, 2131230849, "field 'clearEtNew'");
    target.clearEtNew = finder.castView(view, 2131230849, "field 'clearEtNew'");
    view = finder.findRequiredView(source, 2131230779, "field 'btnUpdate'");
    target.btnUpdate = finder.castView(view, 2131230779, "field 'btnUpdate'");
  }

  @Override public void unbind(T target) {
    target.ivBack = null;
    target.tvTitle = null;
    target.clearEtOld = null;
    target.clearEtNew = null;
    target.btnUpdate = null;
  }
}
