package com.login;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class PasswordUtil {
	//密匙(自己定义的一个字符串)
	private static final String PASSWORD_CRYPT_KEY = "com.xpy_@_123_456";
	private final static String DES = "DES";
	
	public byte[] encrypt(byte[] src, byte[] key) throws Exception {
		// DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成加密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.ENCRYPT_MODE, securekey, sr);
		// 现在，获取数据并加密
		// 正式执行加密操作
		return cipher.doFinal(src);
	}
	public byte[] decrypt(byte[] src, byte[] key) throws Exception {
		// DES算法要求有一个可信任的随机数源
		SecureRandom sr = new SecureRandom();
		// 从原始密匙数据创建一个DESKeySpec对象
		DESKeySpec dks = new DESKeySpec(key);
		// 创建一个密匙工厂，然后用它把DESKeySpec对象转换成
		// 一个SecretKey对象
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(DES);
		SecretKey securekey = keyFactory.generateSecret(dks);
		// Cipher对象实际完成解密操作
		Cipher cipher = Cipher.getInstance(DES);
		// 用密匙初始化Cipher对象
		cipher.init(Cipher.DECRYPT_MODE, securekey, sr);
		// 现在，获取数据并解密
		// 正式执行解密操作
		return cipher.doFinal(src);
	}
	
	/**
	 * 二进制转字符串
	 * @param b
	 * @return
	 */
	public String byte2hex(byte[] b) {
		String hs = "";
		String stmp = "";
		for (int n = 0; n < b.length; n++) {
			stmp = (java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length() == 1)
				hs = hs + "0" + stmp;
			else
				hs = hs + stmp;
		}
		return hs.toUpperCase();
	}
	public byte[] hex2byte(byte[] b) {
		if ((b.length % 2) != 0)
			throw new IllegalArgumentException("长度不是偶数");
		byte[] b2 = new byte[b.length / 2];
		for (int n = 0; n < b.length; n += 2) {
			String item = new String(b, n, 2);
			b2[n / 2] = (byte) Integer.parseInt(item, 16);
		}
		return b2;
	}
	/**
	 * 字符转码
	 * 将ISO-8859-1编码传递过来的字符转换成GBK字符
	 * @param s 要转换的字符串
	 * @return 以GBK编码返回字符符串
	 */
	public static String ISO2GBK(String s) {
		if (s == null || s.length() == 0)
			return "";
		boolean isISO = false;
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c < 255 && c > 127) {
				isISO = true;
				break;
			}
		}
		if (!isISO)
			return s;
		try {
			byte[] bytes = s.getBytes("ISO-8859-1");
			return new String(bytes, "GBK");
		} catch (Exception e) {

			return s;
		}
	}
	
	/**
	 * 字符替换
	 * @author helidong
	 */
	public String conversion(String str){
		if(str != null){
			return str.replaceAll("@", "%");
		}
		return str;
	}
	/**
	 * 密码解密
	 * 要解密直接调用这个方法即可
	 * @param data
	 * @return
	 * @throws Exception
	 */
	public synchronized final String deEncrypt(String data) {
		try {
			return new String(decrypt(hex2byte(data.getBytes()),PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 密码加密
	 * 要加密直接调用这个方法即可
	 * @param source
	 * @return
	 * @throws Exception
	 */
	public synchronized final String encrypt(String source) {
		try {
			return byte2hex(encrypt(source.getBytes(), PASSWORD_CRYPT_KEY.getBytes()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 密码加密解密测试
	 * @param args
	 */
	public static void main(String args[]) {
		//原值
		String source = "123456";
		//测试加密
		String pwd = new PasswordUtil().encrypt(source);
		System.out.println("原值："+source);
		System.out.println("加密后的密码：" + pwd);
		System.out.println("加密后的密码长度："+pwd.length()+"位");
		//解密后的值
		String target = new PasswordUtil().deEncrypt(pwd);
		System.out.println("解密后的密码：" + target);
	}
}
