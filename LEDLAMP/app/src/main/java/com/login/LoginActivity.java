package com.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.base.LedBleActivity;

import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledlamp.R;
import com.start.CommentActivity;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends LedBleActivity implements OnClickListener {
    private ImageView ivBack;
    private TextView tvTitle;
    private Button btnLogin;
    private TextView tvRegister;
    EditText accountet, passwordet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ivBack = (ImageView) findViewById(R.id.ivBack);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        accountet = findViewById(R.id.et_account);
        passwordet = findViewById(R.id.et_password);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        tvRegister = (TextView) findViewById(R.id.tvRegister);

        tvTitle.setText(getString(R.string.login));

        ivBack.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
        tvRegister.setOnClickListener(this);

		if (!"".equals(getBaseApp().getUserToken())) {
			gotoCommentPage();
		}
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btnLogin:
                // 获取用户输入信息
                String account = accountet.getText().toString().trim();
                String password = passwordet.getText().toString().trim();
                if (TextUtils.isEmpty(account)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.input_account), Toast.LENGTH_SHORT).show();
                    return;
                } else if (TextUtils.isEmpty(password)) {
                    Toast.makeText(LoginActivity.this, getString(R.string.input_password), Toast.LENGTH_SHORT).show();
                    return;
                }

                //密码加密
                password = new PasswordUtil().encrypt(password);
                Map<String, String> params = new HashMap<String, String>();
                params.put("userName", account);
                params.put("password", password);
                HttpUtil.getInstance().getSourceData(true, LoginActivity.this, Constant.loginByApp, params, new HttpCallBack() {

                    @Override
                    public void onSuccess(String result) {

                        ResponseBean<UserBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<UserBean>>() {
                        });
                        if (responseBean != null) {
                            if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
								UserBean userBean = responseBean.getContent();
								if(userBean != null && userBean.getToken() != null) {
									getBaseApp().setUserBean(userBean);
								}
                                gotoCommentPage();
                            } else {
                                Toast.makeText(LoginActivity.this, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override
                    public void onException(String message) {
                        Toast.makeText(LoginActivity.this, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                    }
                });
                break;
            case R.id.tvRegister:
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void gotoCommentPage() {
        Intent intent = new Intent(LoginActivity.this, CommentActivity.class);
        startActivity(intent);
        finish();
    }
}