package com.login;

import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ledlamp.R;


public class ClearEditText extends LinearLayout {

	private ImageView ivClearImage;// 删除图标
	private ImageView ivPrompt;// 提示图片
	private EditText etClearContent;// 编辑框

	public ClearEditText(Context context) {
		super(context);
	}

	public ClearEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (isInEditMode())
			return;

		View view = LayoutInflater.from(context).inflate(R.layout.layout_clearet, this, false);
		addView(view);
		
		ivClearImage = (ImageView) findViewById(R.id.ivClearImage);
		ivPrompt = (ImageView) findViewById(R.id.ivPrompt);
		etClearContent = (EditText) findViewById(R.id.etClearContent);
		
		ivClearImage.setVisibility(View.INVISIBLE);
		ivClearImage.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				etClearContent.setText("");
				ivClearImage.setVisibility(View.INVISIBLE);
			}
		});

		etClearContent.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				EditText accountV = (EditText) v;
				if (!hasFocus) {
					ivClearImage.setVisibility(View.INVISIBLE);
				} else {
					if (!TextUtils.isEmpty(accountV.getText())) {
						ivClearImage.setVisibility(View.VISIBLE);
					}

				}
			}
		});
		etClearContent.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
				if (!TextUtils.isEmpty(s))
					ivClearImage.setVisibility(View.VISIBLE);
				else
					ivClearImage.setVisibility(View.INVISIBLE);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

			}

			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});

	}
    public void showIvPrompt(){
    	ivPrompt.setVisibility(View.VISIBLE);
    }
	public void init(int imageResId, String hintText) {
		imageResId = R.drawable.cleareditext_delete;
		ivClearImage.setImageResource(imageResId);
		etClearContent.setHint(hintText);
	}
	public void init(int imageResId, String hintText, int PromptId) {
		imageResId = R.drawable.cleareditext_delete;
		ivClearImage.setImageResource(imageResId);
		etClearContent.setHint(hintText);
		ivPrompt.setImageResource(PromptId);
	}
	public ImageView getIvPrompt() {
		return ivPrompt;
	}
	
	public void setIvPrompt(ImageView ivPrompt) {
		this.ivPrompt = ivPrompt;
	}

	public void setMaxLength(int max) {
		InputFilter[] filters = { new InputFilter.LengthFilter(max) };
		etClearContent.setFilters(filters);

	}

	public EditText getEtClearContent() {
		return etClearContent;
	}

	public void setEtClearContent(EditText etClearContent) {
		this.etClearContent = etClearContent;
	}
	
	public String getText(){
		return this.etClearContent.getText().toString();
	}

}
