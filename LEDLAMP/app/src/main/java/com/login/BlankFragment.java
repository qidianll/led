package com.login;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ledble.activity.home.FragmentTabActivity;
import com.ledble.base.LedBleActivity;
import com.ledble.base.LedBleApplication;
import com.ledlamp.R;
import com.start.CommentActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends Fragment {
    View mView;

    public BlankFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_blank, container, false);

        if (!"".equals(LedBleApplication.getApp().getUserToken())) {
            gotoCommentPage();
        }

        mView.findViewById(R.id.iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(getActivity(), LoginActivity.class);
                startActivity(mIntent);
            }
        });
        return mView;
    }

    private void gotoCommentPage() {
        Intent intent = new Intent(getActivity(), CommentActivity.class);
        startActivity(intent);
    }
}
