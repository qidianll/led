package com.login;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

import com.ledlamp.R;

import java.util.Timer;
import java.util.TimerTask;

public class TimeCountUtil {
	
	Activity activity;
	TextView btGetCheckcode;
	int time;

	public TimeCountUtil(Activity activity, TextView btGetCheckcode, int time) {
		this.activity = activity;
		this.btGetCheckcode = btGetCheckcode;
		this.time = time;
	}

	private Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			btGetCheckcode.setText(msg.arg1 + " " + activity.getString(R.string.second));
			btGetCheckcode.setClickable(false);
			countTime();

		};
	};
	private Timer timer = new Timer();;

	public void countTime() {
		btGetCheckcode.setText(time + " " + activity.getString(R.string.second));
		timer.schedule(new TimerTask() {
			@Override
			public void run() { 
				time--;
				Message message = mHandler.obtainMessage();
				message.arg1 = time;
				mHandler.sendMessage(message);
		       
		}}, 1000);
		if (time == 0) {
			timer.cancel();
			btGetCheckcode.setText(activity.getString(R.string.get_checkcode));
			btGetCheckcode.setClickable(true);
		}
	}
}
