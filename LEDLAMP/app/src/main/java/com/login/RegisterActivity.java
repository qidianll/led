package com.login;

import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.base.LedBleActivity;
import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledble.utils.Utils;
import com.ledlamp.R;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends LedBleActivity implements OnClickListener {
	private ImageView ivBack;
	private TextView tvTitle;
	private EditText clearEtName,clearEtCheckMode,clearEtCheckCode;//账号
	private EditText clearEtPassWord;// 密码
	private TextView tvCheckcode;// 获取验证码按钮
	private Button btnRegister;
	private static final String TAG = "RegisterActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		ivBack = (ImageView) findViewById(R.id.ivBack);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		clearEtName = (EditText) findViewById(R.id.clearEtName);
		clearEtPassWord = (EditText) findViewById(R.id.clearEtPassWord);
		clearEtCheckMode = findViewById(R.id.clearEtCheckMode);
		clearEtCheckCode = findViewById(R.id.clearEtCheckCode);
		tvCheckcode = (TextView) findViewById(R.id.tvCheckcode);
		btnRegister = (Button) findViewById(R.id.btnRegister);

		tvTitle.setText(getString(R.string.register));

		ivBack.setOnClickListener(this);
		tvCheckcode.setOnClickListener(this);
		btnRegister.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		String userName = clearEtName.getText().toString();
		String password = clearEtPassWord.getText().toString();
		String checkMode = clearEtCheckMode.getText().toString().trim();
		String checkcode = clearEtCheckCode.getText().toString().trim();
		Log.e(TAG, "onClick: "+"Name :"+userName+"word:"+password+"checkMode:"+checkMode+"chcode:"+checkcode );
		switch (v.getId()) {
			case R.id.ivBack:
				finish();
				break;
			case R.id.tvCheckcode:
				if (TextUtils.isEmpty(userName)) {
					Toast.makeText(RegisterActivity.this, getString(R.string.input_account), Toast.LENGTH_SHORT).show();
					return;
				} else if (!Utils.isEmail(checkMode) && !Utils.isMobileNo(checkMode)) {
					Toast.makeText(RegisterActivity.this, getString(R.string.account_format_error), Toast.LENGTH_SHORT).show();
					return;
				} else {
					Map<String, String> params = new HashMap<String, String>();
					String safetyCode = new PasswordUtil().encrypt(checkMode);
					String type = Utils.isEmail(checkMode) ? "email" : "phone";
					params.put("userName", userName);
					params.put("checkMode", checkMode);
					params.put("safetyCode", safetyCode);
					params.put("type", type);

					HttpUtil.getInstance().getSourceData(true, RegisterActivity.this, Constant.getVerifyCodeByApp, params, new HttpCallBack() {

						@Override
						public void onSuccess(String result) {
							ResponseBean<UserBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<UserBean>>() {});
							if (responseBean != null) {
								if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
									TimeCountUtil time = new TimeCountUtil(RegisterActivity.this, tvCheckcode, 60);
									time.countTime();
									Toast.makeText(RegisterActivity.this, getString(R.string.send_checkcode), Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(RegisterActivity.this, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
								}
							}
						}

						@Override
						public void onException(String message) {
							Toast.makeText(RegisterActivity.this, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
						}
					});
				}
				break;
			case R.id.btnRegister:
				if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(password) || TextUtils.isEmpty(checkMode) || TextUtils.isEmpty(checkcode)) {
					Toast.makeText(RegisterActivity.this, getString(R.string.input_not_null), Toast.LENGTH_SHORT).show();
					return;
				} else if (userName.length() < 6 || password.length() < 6 || checkcode.length() < 6) {
					Toast.makeText(RegisterActivity.this, getString(R.string.input_length_error), Toast.LENGTH_SHORT).show();
					return;
				}

				//密码加密
				password = new PasswordUtil().encrypt(password);
				Map<String, String> params = new HashMap<String, String>();
				params.put("userName", userName);
				params.put("password", password);
				params.put("checkMode", checkMode);
				params.put("verifyCode", checkcode);

				HttpUtil.getInstance().getSourceData(true, RegisterActivity.this, Constant.registerByApp, params, new HttpCallBack() {

					@Override
					public void onSuccess(String result) {
						ResponseBean<UserBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<UserBean>>() {});
						if (responseBean != null) {
							if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
								Toast.makeText(RegisterActivity.this, getString(R.string.register_success), Toast.LENGTH_SHORT).show();
								finish();
							} else {
								Toast.makeText(RegisterActivity.this, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
							}
						}
					}

					@Override
					public void onException(String message) {
						Toast.makeText(RegisterActivity.this, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
					}
				});
				break;
			default:
				break;
		}
	}
}
