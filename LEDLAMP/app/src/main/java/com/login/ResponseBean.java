package com.login;

public class ResponseBean<T> {
	
	private String returnCode;
	private String returnDesc;
	private T content;
	
	public String getReturnCode() {
		return returnCode;
	}
	public void setReturnCode(String returnCode) {
		this.returnCode = returnCode;
	}
	public String getReturnDesc() {
		return returnDesc;
	}
	public void setReturnDesc(String returnDesc) {
		this.returnDesc = returnDesc;
	}
	public T getContent() {
		return content;
	}
	public void setContent(T content) {
		this.content = content;
	}
	
    @Override
    public String toString() {
        return "ResponseBean{" +
                "returnCode='" + returnCode + '\'' +
                ", returnDesc='" + returnDesc + '\'' +
                ", content=" + content +
                '}';
    }
}
