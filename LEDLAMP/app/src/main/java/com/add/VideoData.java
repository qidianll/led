package com.add;

import com.ledble.fragment.home.VideoFragment;

import java.util.ArrayList;
import java.util.List;

public class VideoData {

    public static List<VideoBean> sVideoList;

    static {
//        String[] videoUrls = ;
//                {
//                "http://www.eywedu.com.cn/sanzijing/UploadFiles_2038/szj-01.mp4",
//                "http://flv3.bn.netease.com/videolib3/1709/15/XUeFJ9893/SD/XUeFJ9893-mobile.mp4",
//                "http://flv3.bn.netease.com/videolib3/1709/16/BdXrp2108/SD/BdXrp2108-mobile.mp4"
//        };
//                = AppContext.getInstance().getResources().getStringArray(R.array.video_url);
        String[] videoTitles = {"LED BLE", "LED DMX","LED SMART","LED STRIP","LED SPI","LED STAGE","LED LIGHT"};
//                AppContext.getInstance().getResources().getStringArray(R.array.video_title);
        sVideoList = new ArrayList<>();
        for (int i = 0; i < VideoFragment.list.size(); i++) {
            sVideoList.add(new VideoBean((String) VideoFragment.list.get(i), videoTitles[i]));
        }
    }

    public static List<VideoBean> getVideoList() {

        return sVideoList;
    }

    public static VideoBean getVideo() {

        return sVideoList.get(0);
    }
}
