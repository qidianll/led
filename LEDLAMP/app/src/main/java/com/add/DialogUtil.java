package com.add;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.ledlamp.R;


/**
 * 对话框工具类
 *
 * @author ftl
 */
public class DialogUtil {
	
	// 进度对话框
	private static Dialog dialog;

	public static MyProgressDialog showWithProgress(Context context, String title,
                                                    String message, boolean isCancelable) {
		return showProgress(context, title, message, isCancelable, null);
	}

	public static MyProgressDialog showCancelableProgress(Context context, String title,
                                                          String message, DialogInterface.OnCancelListener cancelListener) {
		return showProgress(context, title, message, true, cancelListener);
	}
	
	public static MyProgressDialog showNoCancelProgress(Context context, String title,
                                                        String message) {
		return showProgress(context, title, message, false, null);
	}
	
	public static Dialog showProgress(Context context) {
		Dialog mDialog = new Dialog(context, R.style.dialog_styles);
		View view = View.inflate(context, R.layout.layout_progress_dialog, null);
		mDialog.setContentView(view);
		mDialog.setCanceledOnTouchOutside(false);
		mDialog.show();
		dialog = mDialog;
		return mDialog;
	}

	public static MyProgressDialog showProgress(Context context, String title,
                                                String message, boolean isCancelable,
                                                DialogInterface.OnCancelListener cancelListener) {
		MyProgressDialog progress = new MyProgressDialog(context, title, message);
		progress.setCancelable(isCancelable);
		progress.setCanceledOnTouchOutside(false);
		if (isCancelable && cancelListener != null) {
			progress.setOnCancelListener(cancelListener);
		}
		progress.show();
		dialog = progress;
		return progress;
	}
	
	/**
	 * 显示自定义View
	 *  
	 * @param context
	 * @param view
	 * @return
	 */
	public static AlertDialog getCustomDialog(Context context, View view, String title,
                                              String sureBtnName, String cancelBtnName,
                                              DialogInterface.OnClickListener positiveListener,
                                              DialogInterface.OnClickListener negativeListener) {
		AlertDialog.Builder builder = getBuilder(context);
		builder.setTitle(title);
		builder.setView(view);
		builder.setPositiveButton(sureBtnName, positiveListener);
		builder.setNegativeButton(cancelBtnName, negativeListener);
		return builder.show();
	}
	
	/**
	 * 关闭对话框
	 */
	public static void closeWithProgress() {
		if (dialog != null) {
			dialog.dismiss();
			dialog = null;
		}
	}

	public static AlertDialog.Builder getBuilder(Context context) {
		return new AlertDialog.Builder(context);
	}
}
