package com.add;

public class VideoBean {
	private String mVideoUrl;
	private String mVideoTitle;

	private int videoId;
	private String describe;//视频描述
	private String videoUrl;//视频存储地址
	private String videoVisitUrl;//视频访问地址
	private String uploadTime;//上传时间
	private int appId;//app编号归属

	public VideoBean(){}

	public VideoBean(int videoId, String describe, String videoUrl,
					 String videoVisitUrl, String uploadTime, int appId) {
		super();
		this.videoId = videoId;
		this.describe = describe;
		this.videoUrl = videoUrl;
		this.videoVisitUrl = videoVisitUrl;
		this.uploadTime = uploadTime;
		this.appId = appId;
	}

	public int getVideoId() {
		return videoId;
	}

	public void setVideoId(int videoId) {
		this.videoId = videoId;
	}

	public String getDescribe() {
		return describe;
	}

	public void setDescribe(String describe) {
		this.describe = describe;
	}

	public void setVideoUrl(String videoUrl) {
		this.videoUrl = videoUrl;
	}

	public String getVideoVisitUrl() {
		return videoVisitUrl;
	}

	public void setVideoVisitUrl(String videoVisitUrl) {
		this.videoVisitUrl = videoVisitUrl;
	}

	public String getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public VideoBean(String videoUrl, String videoTitle) {

		mVideoUrl = videoUrl;
		mVideoTitle = videoTitle;
	}

	public String getVideoUrl() {

		return mVideoUrl;
	}

	public String getVideoTitle() {

		return mVideoTitle;
	}
	
}
