package com.ledble.net;

public abstract class BaseConnection {

	public BaseConnection() {

	}
	public abstract void sendSearchOnlineDevices() throws Exception;
	
	public abstract void  sendRouteCommand(String ssid, String pwd) throws Exception;
	
	public abstract void sendRouteSSID() throws Exception;

	public abstract void sendRoutePassword() throws Exception;

	public abstract void sendSSID(String ssid) throws Exception;

	public abstract void sendPassword(String pwd) throws Exception;

	public abstract void open() throws Exception;

	public abstract void close() throws Exception;

	public abstract void setRgb(int r, int g, int b) throws Exception;

	public abstract void setRgbMode(int model) throws Exception;

	public abstract void setSpeed(int speed) throws Exception;

	public abstract void setBrightness(int brightness) throws Exception;

	// 0x7e 0x05 0x03 0x0 0x02 0xff 0xff 0x0 0xef
	public abstract void setColorWarmModel(int model) throws Exception;

	// 0x7e 0x05 0x03 0x0 0x01 0xff 0xff 0x00 0xef
	public abstract void setDimModel(int model) throws Exception;

	public abstract void setDim(int dim) throws Exception;

	// 0x7e 0x07 0x06 0x00 0x00 0x00 0x00 0x0 0xef
	public abstract void setMusic(int brightness, int r, int g, int b) throws Exception;

	// 0x7e 0x06 0x05 0x02 0x00 0x00 0xff 0x08 0xef
	public abstract void setColorWarm(int warm, int cool) throws Exception;

	// 设置SPI模式
	public abstract void setSPIBrightness(int brightness) throws Exception;

	// 设置SPI速度
	public abstract void setSPISpeed(int speed) throws Exception;

	// 设置 SPI开关
	public abstract void turnOnSPI(int off_on) throws Exception;

	// 配置SPI
	public abstract void configSPI(int bannerType, byte lengthH, byte lengthL, int bannerSort) throws Exception;

	// 暂停
	public abstract void pauseSPI(int pauseBit) throws Exception;

	public abstract boolean isOnLine();

	// public abstract void trunOff() ;

	public abstract void write(int[] data) throws Exception;

	public abstract void writeByte(byte[] data) throws Exception;

	public abstract void closeSocket();

	public abstract boolean connect();

}
