package com.ledble.net.wifi.callback;

public interface ConnectDeviceListener {
	public void onConnect(boolean isSuccess);
}
