package com.ledble.net;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.text.format.Time;

import com.common.uitl.LogUtil;
import com.common.uitl.NumberHelper;
import com.common.uitl.Tool;
import com.ledble.bean.MyColor;

public class WifiConenction extends BaseConnection {

	public static final String LOCALHOST = "192.168.2.3";
	public static final int PORT = 5000;
//	public static final int SERVER_PORT = 25000;
	public static String tag = "WifiConenction";
	// public static final String DEFAULT_SSID = "R2Wifi";

	private Socket socket;
	private InputStream inputStream;
	private OutputStream outStream;
	
	private DataInputStream reader;
	
	private ArrayList<String> ips;
	private boolean isUDP;
	
	private int value;
	
	// ��������ȡ������
	private InputStreamReader inputStreamReader;
	private BufferedReader bufferedReader;

	
	private static WifiConenction netConnect;

	public WifiConenction(Context context) {

	}
	
	public WifiConenction() {
//		setAddress = new HashSet<String>();
//		 ips = (ArrayList<String>) SharePersistent.getObjectValue(context, CommonConstant.ONLINE_IPS);
	}

	
	public static WifiConenction getInstance() {
		if (netConnect == null) {
			netConnect = new WifiConenction();
		}
		return netConnect;
	}


	public WifiConenction(Context context, boolean isUDP, ArrayList<String> onlineIps) {
//		this.isUDP = isUDP;
		this.isUDP = isUDP;
		this.ips = onlineIps;
		// ips = (ArrayList<String>) SharePersistent.getObjectValue(context,
		// CommonConstant.ONLINE_IPS);
	}

	@Override
	public void open() throws Exception {
//		int code[] = new int[] { 0x7e, 0x04, 0x04, 0x01, 0xff, 0xff, 0xff, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x04, 0x01, 0xff, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	@Override
	public void close() throws Exception {
//		int code[] = new int[] { 0x7e, 0x04, 0x04, 0x00, 0xff, 0xff, 0xff, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x04, 0x00, 0xff, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	public void setRgb(int r, int g, int b) throws Exception {
//		int code[] = new int[] { 0x7e, 0x07, 0x05, 0x03, r, g, b, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x05, 0x03, r, g, b, 0xff, 0xef };
		write(code);
	}

	public void setRgbMode(int model) throws Exception {
//		int code[] = new int[] { 0x7e, 0x05, 0x03, model, 0x03, 0xff, 0xff, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x03, model, 0x03, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	public void setSpeed(int speed) throws Exception {
//		int code[] = new int[] { 0x7e, 0x04, 0x02, speed, 0xff, 0xff, 0xff, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x02, speed, 0xff, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	public void setBrightness(int brightness) throws Exception {
//		int code[] = new int[] { 0x7e, 0x04, 0x01, brightness, 0xff, 0xff, 0xff, 0x00, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x01, brightness, 0xff, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}
	
	public void setDiy(ArrayList<MyColor> colors, int style){
		try {
//			int code1[] = new int[] { 0x7e, 0x05, 0x0e, style, 0x03, 0xff, 0xff, 0x00, 0xef };
//			write(code1);
			for(int i = 0; i < colors.size(); i++){
				int r = colors.get(i).r;
				int g = colors.get(i).g;
				int b = colors.get(i).b;

//				final int code2[] = new int[] { 0x7e, 0x07, 0x10, 0x03, r, g, b, 0x00, 0xef };
				final int code2[] = new int[] { 0x7e, 0xff, 0x06, style, r, g, b, colors.size(), 0xef };				
				
				write(code2);
				
				Thread.sleep(150);
			}
//			final int code3[] = new int[] { 0x7e, 0x05, 0x0f, (byte)style, 0x03, 0xff, 0xff, 0x00, 0xef };
//			new Handler().postDelayed(new Runnable() {
//				
//				@Override
//				public void run() {
//					try {
//						write(code3);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}, 150);
		} catch (Exception e) {
			
		}
	}

	

	// 0x7e 0x05 0x03 0x0 0x02 0xff 0xff 0x0 0xef
	public void setColorWarmModel(int model) throws Exception {
//		int code[] = new int[] { 0x7e, 0x05, 0x03, model, 0x02, 0xff, 0xff, 0x0, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x03, model, 0x02, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	// 0x7e 0x05 0x03 0x0 0x01 0xff 0xff 0x00 0xef
	public void setDimModel(int model) throws Exception {
//		int code[] = new int[] { 0x7e, 0x05, 0x03, model, 0x01, 0xff, 0xff, 0x0, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x03, model, 0x01, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	public void setDim(int dim) throws Exception {
//		int code[] = new int[] { 0x7e, 0x05, 0x05, 0x01, dim, 0xff, 0xff, 0x08, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x05, 0x01, dim, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}

	// 0x7e 0x07 0x06 0x00 0x00 0x00 0x00 0x0 0xef
	public void setMusic(int brightness, int r, int g, int b) throws Exception {
		int code[] = new int[] { 0x7e, 0x07, 0x06, brightness, 0x00, 0x00, 0x00, 0x00, 0xef };
		write(code);
	}

	// 0x7e 0x06 0x05 0x02 0x00 0x00 0xff 0x08 0xef
	public void setColorWarm(int warm, int cool) throws Exception {
//		int code[] = new int[] { 0x7e, 0x06, 0x05, 0x02, warm, cool, 0xff, 0x08, 0xef };
		int code[] = new int[] { 0x7e, 0xff, 0x05, 0x02, warm, cool, 0xff, 0xff, 0xef };
		write(code);
	}

	// ����SPI����==============
	public void setSPIBrightness(int brightness) throws Exception {
		int code[] = new int[] { 0x7B, 0x04, 0x01, brightness, 0xff, 0xff, 0xff, 0x00, 0xBf };
		LogUtil.i(tag, "setSPIBrightness");
		write(code);
	}

	// ����SPI�ٶ�
	public void setSPISpeed(int speed) throws Exception {
		int code[] = new int[] { 0x7B, 0x04, 0x02, speed, 0xff, 0xff, 0xff, 0x00, 0xBf };
		LogUtil.i(tag, "setSPISpeed");
		write(code);
	}

	public void setSPIModel(int model) throws Exception {
		int code[] = new int[] { 0x7B, 0x05, 0x03, model, 0x03, 0xff, 0xff, 0x00, 0xBf };
		LogUtil.i(tag, "setSPIModel");
		write(code);
	}

	// ���� SPI����
	public void turnOnSPI(int off_on) throws Exception {
		int code[] = new int[] { 0x7B, 0x04, 0x04, off_on, 0xff, 0xff, 0xff, 0x00, 0xBf };
		LogUtil.i(tag, "turnOnSPI");
		write(code);
	}

	// ����SPI
	public void configSPI(int bannerType, byte lengthH, byte lengthL, int bannerSort) throws Exception {
		int code[] = new int[] { 0x7B, 0x04, 0x05, bannerType, lengthH, lengthL, bannerSort, 0x00, 0xBF };
		LogUtil.i(tag, "configSPI");
		write(code);
	}

	// ��ͣ
	public void pauseSPI(int pauseBit) throws Exception {
		int code[] = new int[] { 0x7B, 0x04, 0x06, pauseBit, 0xff, 0xff, 0xff, 0x00, 0xBF };
		LogUtil.i(tag, "pauseSPI");
		write(code);
	}

	// ��������ָ��
	public void sendOffline() throws Exception {
		int code[] = new int[] { 0x7e, 0x07, 0x0b, 0xff, 0xff, 0xff, 0xff, 0xff, 0xef };
		LogUtil.i(tag, "ssid");
		write(code);
	}

	// ����·��SSIDָ��
	@Override
	public void sendRouteSSID() throws Exception {
		int code[] = new int[] { 0x7e, 0x07, 0x07, 0xff, 0xff, 0xff, 0xff, 0xff, 0xef };
		LogUtil.i(tag, "ssid");
		write(code);
	}

	// ����·������ָ��
	@Override
	public void sendRoutePassword() throws Exception {
		int code[] = new int[] { 0x7e, 0x07, 0x08, 0xff, 0xff, 0xff, 0xff, 0xff, 0xef };
		LogUtil.i(tag, "password");
		write(code);
	}

	// ����ssid
	@Override
	public void sendSSID(String ssid) throws Exception {
		String content = ssid + "\n";
		writeByte(content.getBytes("utf-8"));
	}

	// ����pwd
	@Override
	public void sendPassword(String pwd) throws Exception {
		String content = pwd + "\n";
		writeByte(content.getBytes("utf-8"));

	}
	
	// 10·���� PWM
	public void setSmartBrightness(int brightness, int mode) throws Exception {
		int code[] = new int[] { 0x7e, 0xff, 0x07, brightness, mode, 0xff, 0xff, 0xff, 0xef };
		write(code);
	}
	
	// 颜色通道设置
	public void SetCHN(int r, int g , int b, int w, int y, int p) throws Exception {
		int code[] = new int[] { 0x7a, 0x01, r, g, b, w, y, p, 0xaf };
		write(code);
	}
	
	// 查询
	public void setSmartCheck(int model) throws Exception {
//		int code[] = new int[] { 0x7e, 0xff, 0x09, model, 0xff, 0xff, 0xff, 0xff, 0xef };
//		write(code);			
				
		
	}
	
	// 温度电池查询
	public ArrayList<String> setTBCheck(int model) throws Exception  {
//		 byte code[] = new byte[] { 0x7e, (byte)0xff, 0x09, (byte)model, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xff, (byte)0xef };
		 int code[] = new int[] { 0x7e, 0xff, 0x09, model, 0xff, 0xff, 0xff, 0xff, 0xef };
		 write(code);
		 
//		 SocketRecv(code); 
		 			
//		try {
//			if (socket != null) {
//				if (socket.isConnected()) {
//
//					String decStr = new String();
//					
//					ArrayList<String> decList = new ArrayList<String>();
//					
//					for (int i = 0; i < 9; i++) {
//
//						int value = 0;
//						
//						int available = inputStream.available();// 可读取多少字节内容
//		                
////		                if(available == 0){// 没有可读取内容则退出
////		                    
////		                    if(available == 0){
////		                        break;
////		                    }
////		                }else {
//		                	value = inputStream.read();
////						}
//
//						if (value > 0) {
//							if (i == 3 || i == 4) {
////								decStr += Integer.toHexString(value);
//								decStr += Integer.toString(value);
//								decList.add(Integer.toString(value));
//							}
//						}
//					}
////					return decStr;
//					inputStream.close();
//					return decList;
//					
//				}
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}		
//		
//		inputStream.close(); 			 			
		return null;

	}
	
	// 温度电池查询
	public ArrayList<String> TBCheckRecv() throws Exception {

		try {
			if (socket != null) {
				if (socket.isConnected()) {

					String decStr = new String();

					ArrayList<String> decList = new ArrayList<String>();

					for (int i = 0; i < 9; i++) {

						int value = 0;

						int available = inputStream.available();// 可读取多少字节内容

						value = inputStream.read();


						if (value > 0) {
							if (i == 3 || i == 4) {
								decStr += Integer.toString(value);
								decList.add(Integer.toString(value));
							}
						}
					}
					return decList;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;

	}
	
	private int SocketRecv(byte code[]) {
		try {
			if (inputStream != null) {
				return inputStream.read(code);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	
	// 
	public String receiveDataReturned(String hexStr) throws Exception {

//		return sendContext("xcmd_req::cmd=info" + "\r\n");
		
		InetAddress address = InetAddress.getByName(LOCALHOST);
        int port = 25000;
        String dataStr = "xcmd_req::cmd=encrypt,key=1,data="+hexStr+"\r\n";
        byte[] data = dataStr.getBytes();
        DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
        DatagramSocket udpSocket = new DatagramSocket();
        udpSocket.send(packet);      

        byte[] data2 = new byte[1024];
        DatagramPacket packet2 = new DatagramPacket(data2, data2.length);
        udpSocket.setSoTimeout(2000); 
        udpSocket.receive(packet2);

        String reply = new String(data2, 0, packet2.getLength());
        udpSocket.close();
        
        return reply;

	}

	@Override
	public void sendRouteCommand(String ssid, String pwd) throws Exception {
		sendRouteSSID();
		Thread.sleep(300);
		sendSSID(ssid);
		Thread.sleep(300);
		sendRoutePassword();
		Thread.sleep(300);
		sendPassword(pwd);
	}

	// �������������豸�㲥ָ��
	public void sendSearchOnlineDevices() throws Exception {
		// int code[] = new int[] { 0x7e, 0x07, 0x09, 0xff, 0xff, 0xff, 0xff,
		// 0xff, 0xef };
		// sendSearchBroadcast(code);
	}

	// -----------------------��ʱ�������

	private static int computeTime(int hour, int minute) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		Calendar ca = Calendar.getInstance();
		Date nowDate = ca.getTime();
		String ymd = sdf2.format(nowDate);

		String then = ymd + " " + NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute);
		Date thenDate = sdf.parse(then);
		int second = (int) ((thenDate.getTime() - nowDate.getTime()) / 1000);
		if (second < 0) {
			second = second + 24 * 60 * 60;
		}
		return second;
	}

	/**
	 * ��/�ر� ��>��ʱ���� ���� onOrOffΪ1Ϊ����Ϊ0Ϊ��
	 * 
	 * @param onOrOff
	 */
	public void turnOnOffTimerOn( int tomgdao, int hour, int minute, int model ) {
		try {
			
			Time t=new Time(); // 
			t.setToNow();
			
			// int code[] = new int[] { 0x7e, 0x01, 0x0d, minuteH, minuteL,
			// 0x01, model, distanceSecond, 0xef };
			int code[] = new int[] { 0x7e, tomgdao, 0x08, t.hour, t.minute, hour, minute, model, 0xef };
			write(code);
		} catch (Exception e) {

		}

	}
	
	/**
	 * ��/�رգ�>��ʱ�ص� ����
	 * 
	 * @param onOrOff
	 */
	public void turnOnOrOffTimerOff(int tomgdao, int hour, int minute, int model) {
		try {
			
			Time t=new Time(); // 
			t.setToNow();
			
			int code[] = new int[] { 0x7e, tomgdao, 0x08, t.hour, t.minute, hour, minute, model, 0xef };
			write(code);
		} catch (Exception e) {

		}
	}

	/**
	 * ��ʱ����
	 * 
	 * @param hour
	 * @param minute
	 * @param model
	 */
	public void timerOn(int hour, int minute, int model) {
		try {
			int second = computeTime(hour, minute);
			int distanceMinute = second / 60;
			int distanceSecond = second % 60;
			byte minuteH = (byte) (distanceMinute >> 8);
			byte minuteL = (byte) (distanceMinute);
			int code[] = new int[] { 0x7e, 0x01, 0x0d, minuteH, minuteL, 0x01, model, distanceSecond, 0xef };
			write(code);
		} catch (Exception e) {
		}
	}

	/**
	 * ��ʱ�ص�
	 * 
	 * @param hour
	 * @param minute
	 */
	public void timerOff(int hour, int minute) {
		try {
			int second = computeTime(hour, minute);
			int distanceMinute = second / 60;
			int distanceSecond = second % 60;
			byte minuteH = (byte) (distanceMinute >> 8);
			byte minuteL = (byte) (distanceMinute);
			int code[] = new int[] { 0x7e, 0x01, 0x0d, minuteH, minuteL, 0x00, 0xff, distanceSecond, 0xef };
			write(code);
		} catch (Exception e) {
		}
	}


	public boolean isOnLine() {
		if (isUDP)
			return true;
		if (null != socket && !socket.isClosed()) {
			return true;
		}
		return false;
	}

	private boolean isMusic = false;

	public void isMusic(boolean isMusic){
		this.isMusic = isMusic;
	}

	@Override
	public void write(final int[] data) throws Exception {
		if (isUDP) {
			if (ips != null && !ips.isEmpty()) {
				if (isMusic) {
					for (final String ip : ips) {
						sendBroadcast(data, ip);
					}
				} else {
					new Thread(new Runnable() {
						@Override
						public void run() {
							for (final String ip : ips) {
								sendBroadcast(data, ip);
							}
						}
					}).start();
				}

			}
		} else {
			new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						ByteArrayOutputStream byo = new ByteArrayOutputStream();
						for (int i = 0; i < data.length; i++) {
							byo.write(Tool.int2bytearray(data[i]));
						}
						byte[] array = byo.toByteArray();
						if (array != null && outStream != null) {
							outStream.write(array);
							outStream.flush();							
					
						}
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}).start();
		}
	}

	public void writeByte(byte[] data) throws Exception {
		outStream.write(data);
		outStream.flush();
	}

	public void closeSocket() {
		if (!isUDP) {
			try {
				if (null != socket) {
					// socket.shutdownOutput();
					socket.close();
					socket = null;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public boolean connect() {
		if (isUDP) {
			return true;
		} else {
			closeSocket();
			try {
				InetAddress address = InetAddress.getByName(LOCALHOST);
				socket = new Socket(address, PORT);
				inputStream = socket.getInputStream();
				outStream = socket.getOutputStream();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	public void sendBroadcast(int[] data, String ip) {
		DatagramSocket udpSocket = null;
		try {
			udpSocket = new DatagramSocket();
			// Ŀ��ip��ַ
			InetAddress addr = InetAddress.getByName(ip);
			// SocketAddress addr = new InetSocketAddress(ip, PORT);
			ByteArrayOutputStream bs = new ByteArrayOutputStream();

			for (int i = 0; i < data.length; i++) {
				bs.write(Tool.int2bytearray(data[i]));
			}
			byte[] b = bs.toByteArray();
			DatagramPacket p = new DatagramPacket(b, b.length, addr, PORT);
			udpSocket.send(p);

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (udpSocket != null) {
				udpSocket.close();
				udpSocket = null;
			}
		}

	}

    
			
}
