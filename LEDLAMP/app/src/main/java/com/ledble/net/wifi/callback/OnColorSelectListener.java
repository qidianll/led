package com.ledble.net.wifi.callback;

public interface OnColorSelectListener {

	public void onColorSelect(int color);
}
