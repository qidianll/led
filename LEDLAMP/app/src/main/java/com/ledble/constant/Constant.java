package com.ledble.constant;

import android.os.Environment;

public class Constant {

	public static final int MUSIC_STYLE_POP = 0;
	public static final int MUSIC_STYLE_SOFT = 1;
	public static final int MUSIC_STYLE_ROCK = 2;


	public static final String MODLE_TYPE = "MODLE_TYPE";
	public static final String MODLE_VALUE = "MODLE_VALUE";
	public static final String USER_TOKEN = "user_token";
	public static final String USER_NAME = "user_name";
	public static final String USER_CHECK_MODE = "user_checkMode";
	public static final String USER_INFO = "user_info";

	public static final String BLE_RGB_DIY = "BLE_RGB_DIY";
	public static final String DMX_RGB_DIY = "DMX_RGB_DIY";
	public static final String SMART_RGB_DIY = "SMART_RGB_DIY";
	public static final String STRIP_RGB_DIY = "STRIP_RGB_DIY";
	public static final String SPI_RGB_DIY = "SPI_RGB_DIY";
	public static final String STAGE_RGB_DIY = "STAGE_RGB_DIY";
	public static final String LIGHT_RGB_DIY = "LIGHT_RGB_DIY";
	
	public static final String BLE_CUSTOM_DIY = "BLE_CUSTOM_DIY";
	public static final String DMX_CUSTOM_DIY = "DMX_CUSTOM_DIY";
	public static final String SMART_CUSTOM_DIY = "SMART_CUSTOM_DIY";
	public static final String STRIP_CUSTOM_DIY = "STRIP_CUSTOM_DIY";
	public static final String SPI_CUSTOM_DIY = "SPI_CUSTOM_DIY";
	public static final String STAGE_CUSTOM_DIY = "STAGE_CUSTOM_DIY";
	public static final String LIGHT_CUSTOM_DIY = "LIGHT_CUSTOM_DIY";
	
	public static final String SPI_MODE_DIY = "SPI_MODE_DIY";
	public static final String DMX_MODE_DIY = "DMX_MODE_DIY";
	
	public static final String BLE_MUSIC_DIY = "BLE_MUSIC_DIY";
	public static final String DMX_MUSIC_DIY = "DMX_MUSIC_DIY";
	public static final String SMART_MUSIC_DIY = "SMART_MUSIC_DIY";
	
	public static final String BLE_DYNAMIC_DIY = "BLE_DYNAMIC_DIY";
	public static final String DMX_DYNAMIC_DIY = "DMX_DYNAMIC_DIY";
	public static final String SMART_DYNAMIC_DIY = "SMART_DYNAMIC_DIY";


	public static final String IMAGE_VALUE = "IMAGE_VALUE";
	
	public static final String SPF_DIY = "DIY";
	
	
	public static final String BRIGHT_TYPE = "BRIGHT_TYPE";
	public static final String BRIGHT_KEY = "BRIGHT_KEY";
	
	public static final String TIMER_TAG = "timer_tag";
    public static final String SUCCESS_CODE = "000000";
	
	public static final String RGB_SORT_TYPE_SPI = "RGB_SORT_TYPE_SPI";
	public static final String CHIP_TYPE_SPI = "CHIP_TYPE_SPI";
	public static final String PIX_VALUE_SPI = "PIX_VALUE_SPI";
	
	public static final String RGB_SORT_TYPE_KEY_SPI = "RGB_SORT_TYPE_KEY_SPI";
	public static final String CHIP_TYPE_KEY_SPI = "CHIP_TYPE_KEY_SPI";
	public static final String PIX_VALUE_KEY_SPI = "PIX_VALUE_KEY_SPI";
	
	public static final String RGB_SORT_TYPE = "RGB_SORT_TYPE";
	public static final String CHIP_TYPE = "CHIP_TYPE";
	public static final String PIX_VALUE = "PIX_VALUE";
	
	public static final String RGB_SORT_TYPE_KEY = "RGB_SORT_TYPE_KEY";
	public static final String CHIP_TYPE_KEY = "CHIP_TYPE_KEY";
	public static final String PIX_VALUE_KEY = "PIX_VALUE_KEY";
	
	public static final String PIX_SPACERCODE_KEY = "PIX_SPACERCODE_KEY";
	public static final String PIX_SPACERCODE_TYPE = "PIX_SPACERCODE_TYPE";
	
	public static final String PIX_STARTCODE_KEY = "PIX_STARTCODE_KEY";
	public static final String PIX_STARTCODE_TYPE = "PIX_STARTCODE_TYPE";
	
	public static final String Activity = "Activity";
	public static final String TimingQueryActivity = "TimingQueryActivity";
	public static final String CurrentQueryActivity = "CurrentQueryActivity";

    public static final String prdDomainPort = "http://120.79.182.171/";
    public static final String prdDomainPort2 = "http://120.78.240.163/";
    public static final String devDomainPort = "http://192.168.43.24:8080/";
    public static final String domainPort = prdDomainPort2;

    public static final String queryNewAdvert = domainPort + "xpy-server/advert/queryNewAdvert";
    public static final String loginByApp = domainPort + "xpy-server/user/loginByApp";
    public static final String updatePswdByApp = domainPort + "xpy-server/user/updatePswdByApp";
    public static final String getVerifyCodeByApp = domainPort + "xpy-server/user/getVerifyCodeByApp";
    public static final String registerByApp = domainPort + "xpy-server/user/registerByApp";
    public static final String queryCommentByApp = domainPort + "xpy-server/comment/queryCommentByApp";
    public static final String queryReplyByApp = domainPort + "xpy-server/comment/queryReplyByApp";
    public static final String publishCommentByApp = domainPort + "xpy-server/comment/publishCommentByApp";
    public static final String publishReplyByApp = domainPort + "xpy-server/comment/publishReplyByApp";
    public static final String queryNewVideo = domainPort + "xpy-server/video/queryNewVideo";

	public static final String PHOTO_IMAGE_PATH = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + "LED BLE/";
	public static final String UPLOAD_PHOTO_PATH = PHOTO_IMAGE_PATH + "upload/";
	public static final String NODATA_CODE = "000001";
}
