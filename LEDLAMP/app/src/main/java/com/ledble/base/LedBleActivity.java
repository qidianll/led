package com.ledble.base;

import java.util.List;

import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;

import com.ledble.constant.Constant;
import com.login.UserBean;

import butterknife.ButterKnife;

public class LedBleActivity extends AppCompatActivity {
	
	private LedBleApplication baseApp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		baseApp = (LedBleApplication) getApplication();
	}
	
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		ButterKnife.bind(this);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if (fragments != null && fragments.size() > 0) {
			for (Fragment fragment : fragments) {
				fragment.onActivityResult(requestCode, resultCode, data);
			}
		}
	}

	public LedBleApplication getBaseApp() {
		return baseApp;
	}
	
	//app 杩涘叆鍚庡彴淇濇寔杩愯鐘舵��
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
			moveTaskToBack(true); //ture 鍦ㄤ换浣旳ctivity涓寜涓嬭繑鍥為敭閮介��鍑哄苟杩涘叆鍚庡彴杩愯锛� false 鍙湁鍦ㄦ牴Activity涓寜涓嬭繑鍥為敭鎵嶄細閫�鍚戝悗鍙拌繍琛屻��
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		moveTaskToBack(true);
	}
	
	protected void onDestroy() {
		super.onDestroy();
		ButterKnife.unbind(this);
	}

	public void onSensorChanged(SensorEvent sensorEvent) {
		// TODO Auto-generated method stub
		
	}



}
