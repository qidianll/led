package com.ledble.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.content.Context;
import android.content.SharedPreferences;


import com.ledble.bean.Mp3;
import com.ledble.constant.Constant;
import com.login.UserBean;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;

public class LedBleApplication extends Application {
	
	private ArrayList<BluetoothDevice> bleDevices;
	private Set<BluetoothDevice> tempDevices;
	private HashMap<String, BluetoothGatt> hashMapGatt;
	public static LedBleApplication app;
	public static final String tag = "ble";
	private ArrayList<Mp3> mp3s;
	private SharedPreferences sf;
	
	@Override
	public void onCreate() {
		super.onCreate();
		//CrashHandler.getInstance().init(this);
		bleDevices = new ArrayList<BluetoothDevice>();
		hashMapGatt = new HashMap<String, BluetoothGatt>();
		mp3s = new ArrayList<Mp3>();
		sf = getSharedPreferences(Constant.USER_INFO, Context.MODE_PRIVATE);
		app = this;
		
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .threadPriority(Thread.NORM_PRIORITY - 2)
                .denyCacheImageMultipleSizesInMemory()
                .tasksProcessingOrder(QueueProcessingType.LIFO)
                .writeDebugLogs() // Remove for release app
                .build();
		ImageLoader.getInstance().init(config);
		
	}
	
	public static LedBleApplication getApp() {
		return app;
	}

	public ArrayList<Mp3> getMp3s() {
		return mp3s;
	}

	public void setMp3s(ArrayList<Mp3> mp3s) {
		this.mp3s = mp3s;
	}
	public HashMap<String, BluetoothGatt> getBleGattMap() {
		return hashMapGatt;
	}

	public void clearBleGatMap() {
		if (hashMapGatt.size() > 0) {
//			hashMapGatt.clear();
			
			for (Iterator<Map.Entry<String, BluetoothGatt>> it = hashMapGatt.entrySet().iterator(); it.hasNext();){
			    Map.Entry<String, BluetoothGatt> item = it.next();
			    //... todo with item
			    it.remove();
			}

		}
	}

	public ArrayList<BluetoothDevice> getBleDevices() {
		return bleDevices;
	}
	
	public void clearBleDevices() {
		if (bleDevices.size() > 0) {
			bleDevices.clear();
		}	
	}

	/**
	 * 删除断开的Device
	 * @param address
	 */
	public void removeDisconnectDevice(String address) {
		
		for (int i = 0, isize = bleDevices.size(); i < isize; i++) {
			BluetoothDevice tempDev = bleDevices.get(i);
			if (address.equalsIgnoreCase(tempDev.getAddress())) {
				   bleDevices.remove(i);
				   break;
			}
		}
		
	}
	
	/**
	 * 删除全部的Device
	 * @param address
	 */
	public void removeAllDevice() {
		
		if (bleDevices.size() > 0) {
			bleDevices.clear();
		}
		
	}

	public Set<BluetoothDevice> getTempDevices() {
		return tempDevices;
	}

	public void setTempDevices(Set<BluetoothDevice> tempDevices) {
		this.tempDevices = tempDevices;
	}
	
	public void exit() {
		
	}

    public String getUserToken() {

		return sf.getString(Constant.USER_TOKEN, "");
    }

	public void setUserBean(UserBean userBean) {
		SharedPreferences.Editor editor = sf.edit();
		editor.putString(Constant.USER_NAME, userBean != null ? userBean.getUserName() : "");
		editor.putString(Constant.USER_CHECK_MODE, userBean != null ? userBean.getCheckMode() : "");
		editor.putString(Constant.USER_TOKEN, userBean != null ? userBean.getToken() : "");
		editor.commit();
	}

	public String getUserName() {
		return sf.getString(Constant.USER_NAME, "");
	}

	public String getCheckMode() {
		return sf.getString(Constant.USER_CHECK_MODE, "");
	}

}
