package com.ledble.view.tdialog.listener;


import com.ledble.view.tdialog.TDialog;
import com.ledble.view.tdialog.base.BindViewHolder;

import android.view.View;

public interface OnViewClickListener {
    void onViewClick(BindViewHolder viewHolder, View view, TDialog tDialog);
}
