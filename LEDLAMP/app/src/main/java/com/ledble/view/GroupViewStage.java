package com.ledble.view;

import java.util.ArrayList;

import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.wifi.MainActivity_WiFi;
import com.ledble.base.LedBleApplication;
import com.ledble.db.stage.GroupDeviceStage;
import com.ledble.db.wifi.GroupDeviceWiFi;
import com.ledlamp.R;

import android.content.Context;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

public class GroupViewStage {

	private String groupName;
	private View groupView;
	private ToggleButton toggleButton;
	private ArrayList<GroupDeviceStage> groupDevices;
	private TextView textViewMode;
	private TextView gnameTextView;

	private ToggleButton imageViewLight;
	private ImageView imageViewControll;
	private View viewTopLine;
	private View viewBottomLine;
	private SlideSwitch slideSwitch;
	private int connect;
	private boolean isAllon;

	
	public GroupViewStage(Context context, String groupName,boolean isAllon) {
		this.isAllon=isAllon;
		this.groupName = groupName;
		this.groupView = View.inflate(context, R.layout.item_my_group_wifi, null);
		this.groupView.setTag(groupName);

		this.imageViewControll = (ImageView) groupView.findViewById(R.id.imageViewControll);
		this.viewTopLine = groupView.findViewById(R.id.viewTopLine);
		this.viewBottomLine = groupView.findViewById(R.id.viewBottomLine);

		this.gnameTextView = (TextView) groupView.findViewById(R.id.textViewGroupName);
		textViewMode = (TextView) groupView.findViewById(R.id.textViewTotal);
		toggleButton = (ToggleButton) groupView.findViewById(R.id.toggleButton);
		
		if(isAllon==false){
			setCloseDisable();
		}
		this.gnameTextView.setText(groupName);
		
		this.imageViewLight = (ToggleButton) groupView.findViewById(R.id.imageViewLight);


		toggleButton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					
					if (MainActivity_WiFi.getMainActivity() != null) {
						MainActivity_WiFi.getMainActivity().close();
					}
					if (MainActivity_BLE.getMainActivity() != null) {
						MainActivity_BLE.getMainActivity().close();
					}
//					NetConnectBle.getInstanceByGroup(GroupView.this.groupName).turnOn();
				} else {
					
					if (MainActivity_WiFi.getMainActivity() != null) {
						MainActivity_WiFi.getMainActivity().close();
					}
					if (MainActivity_BLE.getMainActivity() != null) {
						MainActivity_BLE.getMainActivity().close();
					}
//					NetConnectBle.getInstanceByGroup(GroupView.this.groupName).turnOff();
				}
				juedg();
			}
		});
		
		slideSwitch = (SlideSwitch) groupView.findViewById(R.id.slideSwitch);
		slideSwitch.setState(false);
	}

	
	public ToggleButton getToggleButton() {
		return toggleButton;
	}


	public void hideTopLine() {
		viewTopLine.setVisibility(View.VISIBLE);
	}

	public void hideBottomLine() {
		viewBottomLine.setVisibility(View.VISIBLE);
	}

	public boolean isTurnOn() {
		return toggleButton.isChecked();
	}

	public int getConnect() {
		return connect;
	}

	public ImageView getImageViewControll() {
		return imageViewControll;
	}

	public void setImageViewControll(ImageView imageViewControll) {
		this.imageViewControll = imageViewControll;
	}

	public ArrayList<GroupDeviceStage> getGroupDevices() {
		return groupDevices;
	}

	public void setGroupDevices(ArrayList<GroupDeviceStage> groupDevices) {
		this.groupDevices = groupDevices;
		// String
		// tot=App.getApp().getResources().getString(R.string.conenct_device,(ListUtiles.isEmpty(groupDevices)?0:this.groupDevices.size()),0);
		// setConnect(tot);
		setConnectCount(0);
	}

	public void setConnect(String connected) {
		String connectdDevice = LedBleApplication.getApp().getResources().getString(R.string.connect_count, connected);
		this.textViewMode.setText(connectdDevice);
	}

	public void setConnectCount(int count) {
		this.connect = count;
		String connectdDevice = LedBleApplication.getApp().getResources().getString(R.string.connect_count, count);
		this.textViewMode.setText(connectdDevice);
		juedg();
	}

	public void turnOn() {
		toggleButton.setChecked(true);
		
		if (MainActivity_WiFi.getMainActivity() != null) {
			MainActivity_WiFi.getMainActivity().close();
		}
		if (MainActivity_BLE.getMainActivity() != null) {
			MainActivity_BLE.getMainActivity().close();
		}
//		NetConnectBle.getInstanceByGroup(groupName).turnOn();
	}

	public void juedg() {
		if (toggleButton.isChecked() && connect > 0) {
			this.imageViewControll.setVisibility(View.VISIBLE);
		} else {
			this.imageViewControll.setVisibility(View.INVISIBLE);
		}
	}

	public void setCloseDisable(){
		turnOff();
		toggleButton.setChecked(false);
	}
	public void turnOff() {
		toggleButton.setChecked(false);
		
		if (MainActivity_WiFi.getMainActivity() != null) {
			MainActivity_WiFi.getMainActivity().close();
		}
		if (MainActivity_BLE.getMainActivity() != null) {
			MainActivity_BLE.getMainActivity().close();
		}
//		NetConnectBle.getInstanceByGroup(groupName).turnOff();
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
		this.gnameTextView.setText(groupName);
	}
	
	public void setWeekName(String weekStr) {

//		this.gnameTextView.setText(weekStr);
		this.textViewMode.setText(weekStr);
	}
	
	public void setModeName(String modeStr) {

		this.textViewMode.setText(modeStr);
	}

	public View getGroupView() {
		return groupView;
	}

	public void setGroupView(View groupView) {
		this.groupView = groupView;
	}

	public ToggleButton getImageViewLight() {
		return imageViewLight;
	}

	public SlideSwitch getSlideSwitch() {
		return slideSwitch;
	}


	public void setSlideSwitch(SlideSwitch slideSwitch) {
		this.slideSwitch = slideSwitch;
	}

}
