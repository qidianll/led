package com.ledble.view.tdialog.listener;

import com.ledble.view.tdialog.base.BindViewHolder;

public interface OnBindViewListener {
    void bindView(BindViewHolder viewHolder);
}
