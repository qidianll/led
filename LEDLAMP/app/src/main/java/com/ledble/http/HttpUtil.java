package com.ledble.http;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.util.Log;

import com.add.DialogUtil;

public class HttpUtil {
	
	private String result = "";
	private String message = "";
	private static HttpUtil httpUtil = null;
	
	private HttpUtil() {} 
	 
	public static HttpUtil getInstance() {
		if (httpUtil == null) {
			synchronized (HttpUtil.class) {
				if (httpUtil == null) {
					httpUtil = new HttpUtil();
				}
			}
		}
		return httpUtil;
	}

	public interface HttpCallBack {
		void onSuccess(String result);

		void onException(String message);
	}

	public void uploadFile(final boolean showDialog, final Activity activity, final String url, final Map<String, String> params,
						   final Map<String, File> files, final HttpCallBack httpCallBack) {
		if (showDialog) {
			DialogUtil.showProgress(activity);
		}

		new Thread(new Runnable() {

			public void run() {
				String BOUNDARY = java.util.UUID.randomUUID().toString();
				String PREFIX = "--", LINEND = "\r\n";
				String MULTIPART_FROM_DATA = "multipart/form-data";
				String CHARSET = "UTF-8";

				try {
					URL uri = new URL(url);
					HttpURLConnection conn = (HttpURLConnection) uri.openConnection();
					conn.setReadTimeout(10 * 1000);
					conn.setDoInput(true);
					conn.setDoOutput(true);
					conn.setUseCaches(false);
					conn.setRequestMethod("POST");
					conn.setRequestProperty("connection", "keep-alive");
					conn.setRequestProperty("Charsert", "UTF-8");
					conn.setRequestProperty("Content-Type", MULTIPART_FROM_DATA + ";boundary=" + BOUNDARY);

					StringBuilder sb = new StringBuilder();
					for (Map.Entry<String, String> entry : params.entrySet()) {
						sb.append(PREFIX);
						sb.append(BOUNDARY);
						sb.append(LINEND);
						sb.append("Content-Disposition: form-data; name=\"" + entry.getKey() + "\"" + LINEND);
						sb.append("Content-Type: text/plain; charset=" + CHARSET + LINEND);
						sb.append("Content-Transfer-Encoding: 8bit" + LINEND);
						sb.append(LINEND);
						sb.append(entry.getValue());
						sb.append(LINEND);
					}
					DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
					outStream.write(sb.toString().getBytes());
					if (files != null) {
						for (Map.Entry<String, File> file : files.entrySet()) {
							StringBuilder sb1 = new StringBuilder();
							sb1.append(PREFIX);
							sb1.append(BOUNDARY);
							sb1.append(LINEND);
							System.out.println("key:"+file.getKey());
							System.out.println("name:"+file.getValue().getName());
							sb1.append("Content-Disposition: form-data; name=\"" + file.getKey() + "\"" + "; filename=\""
									+ file.getValue().getName() + "\"" + LINEND);
							sb1.append("Content-Type: application/octet-stream; charset=" + CHARSET + LINEND);
							sb1.append(LINEND);
							outStream.write(sb1.toString().getBytes());

							InputStream is = new FileInputStream(file.getValue());
							byte[] buffer = new byte[1024];
							int len = 0;
							while ((len = is.read(buffer)) != -1) {
								outStream.write(buffer, 0, len);
							}
							outStream.write(LINEND.getBytes());
							is.close();
						}
					}
					byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINEND).getBytes();
					outStream.write(end_data);
					outStream.flush();

					int res = conn.getResponseCode();
					InputStream in = conn.getInputStream();
					StringBuilder result = new StringBuilder();
					if (res == 200) {
						int ch;
						while ((ch = in.read()) != -1) {
							result.append((char) ch);
						}
						final String resultStr = result.toString();
						if (httpCallBack != null && resultStr != null) {
							activity.runOnUiThread(new Runnable() {

								public void run() {
									if (showDialog) {
										DialogUtil.closeWithProgress();
									}
									httpCallBack.onSuccess(resultStr);
								}
							});
						}
					}
					outStream.close();
					conn.disconnect();
				} catch (Exception e) {
					if (httpCallBack != null) {
						message = "ErrorMsg:" + e.getMessage();
						activity.runOnUiThread(new Runnable() {

							public void run() {
								if (showDialog) {
									DialogUtil.closeWithProgress();
								}
								httpCallBack.onException(message);
							}
						});

					}
				}
			}
		}).start();
	}

	/**
	 * 获取数据
	 * @param showDialog
	 * @param activity
	 * @param url
	 * @param map
	 * @param httpCallBack
	 */
	public void getSourceData(final boolean showDialog, final Activity activity, final String url, final Map<String, String> params,
							  final HttpCallBack httpCallBack) {
		if (showDialog) {
			DialogUtil.showProgress(activity);
		}

		new Thread(new Runnable() {

			public void run() {
				String resultStr = null;
				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is
				// established.
				int timeoutConnection = 10000;
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT)
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

				SchemeRegistry schemeRegistry = new SchemeRegistry();
				try {
					KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
					keyStore.load(null, null);
					schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				} catch (Exception e) {
					e.printStackTrace();
				}

				ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(httpParameters, schemeRegistry);
				DefaultHttpClient httpclient = new DefaultHttpClient(cm, httpParameters);

				HttpProtocolParams.setUseExpectContinue(httpclient.getParams(), false);
				HttpPost httpPost = new HttpPost(url);

				// 设置参数
				List<NameValuePair> nvps = new ArrayList<NameValuePair>();
				for (Iterator<String> iter = params.keySet().iterator(); iter.hasNext();) {
					String name = iter.next();
					String value = params.get(name);
					nvps.add(new BasicNameValuePair(name, value));
				}

				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
					ResponseHandler<String> res1 = new BasicResponseHandler();
					resultStr = httpclient.execute(httpPost, res1);
					Log.v("HttpUtil", resultStr);

				} catch (Exception e) {
					if (httpCallBack != null && e != null) {
						message = "ErrorMsg:" + e.getMessage();
						Log.v("HttpUtil", message);
						activity.runOnUiThread(new Runnable() {

							public void run() {
								if (showDialog) {
									DialogUtil.closeWithProgress();
								}
								httpCallBack.onException(message);
							}
						});

					}
				}
				httpclient.getConnectionManager().shutdown();
				if (httpCallBack != null && resultStr != null) {
					result = resultStr;
					activity.runOnUiThread(new Runnable() {

						public void run() {
							if (showDialog) {
								DialogUtil.closeWithProgress();
							}
							httpCallBack.onSuccess(result);
						}
					});
				}
			}
		}).start();
	}

//	public interface HttpCallBack {
//		void onSuccess(String result);
//		void onException(String message);
//	}

	/**
	 * 获取数据
	 * @param activity
	 * @param url      
	 * @param map    
	 * @param httpCallBack
	 */
	public void getSourceData(final Activity activity, final String url, final Map<String, String> params, 
			final HttpCallBack httpCallBack) {
		new Thread(new Runnable() {
			
			public void run() {
				String resultStr = null;
				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is established.
				int timeoutConnection = 10000;
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT) 
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			    
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				try {
					KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
					keyStore.load(null, null);
					schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
		        ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(httpParameters, schemeRegistry);
		        DefaultHttpClient httpclient = new DefaultHttpClient(cm, httpParameters);
		        
				HttpProtocolParams.setUseExpectContinue(httpclient.getParams(), false);
			    HttpPost httpPost = new HttpPost(url);
		 
			    //设置参数  
	            List<NameValuePair> nvps = new ArrayList<NameValuePair>();   
	            for (Iterator<String> iter = params.keySet().iterator(); iter.hasNext();) {  
	                String name = iter.next();  
	                String value = params.get(name);  
	                nvps.add(new BasicNameValuePair(name, value));  
	            }
	 
		    	try {
					httpPost.setEntity(new UrlEncodedFormEntity(nvps,HTTP.UTF_8));
					ResponseHandler <String> res1 = new BasicResponseHandler();
					resultStr = httpclient.execute(httpPost,res1);
					Log.v("HttpUtil",resultStr);
			        
				} catch(Exception e) {
					if (httpCallBack != null && e != null) {
						message = "ErrorMsg:"+ e.getMessage();
						Log.v("HttpUtil",message);
						activity.runOnUiThread(new Runnable() {
				     
				            public void run() {
				            	httpCallBack.onException(message);
				            }
				        });
						
					}
				}
		    	httpclient.getConnectionManager().shutdown();
		    	if (httpCallBack != null && resultStr != null) {
		    		result = resultStr;
		    		activity.runOnUiThread(new Runnable() {
					     
			            public void run() {
			            	httpCallBack.onSuccess(result);
			            }
			        });
		    	}
			}
		}).start();
	 }
}
