package com.ledble.http;

import java.io.Serializable;

public class AdvertBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	private int advertId;
    private String describe;//广告描述
    private String advertUrl ;//点击广告后的链接
    private String imageUrl;//推广图片存储地址
    private String imageVisitUrl;//推广图片访问地址
    private String uploadTime;//上传时间
    private int appId;//app编号归属
    
    public AdvertBean(){}
    
	public AdvertBean(int advertId, String describe, String advertUrl, String imageUrl, 
			String imageVisitUrl, String uploadTime, int appId) {
		super();
		this.advertId = advertId;
		this.describe = describe;
		this.advertUrl = advertUrl;
		this.imageUrl = imageUrl;
		this.imageVisitUrl = imageVisitUrl;
		this.uploadTime = uploadTime;
		this.appId = appId;
	}
	
	public int getAdvertId() {
		return advertId;
	}

	public void setAdvertId(int advertId) {
		this.advertId = advertId;
	}

	public String getDescribe() {
		return describe;
	}
	public void setDescribe(String describe) {
		this.describe = describe;
	}
	public String getAdvertUrl() {
		return advertUrl;
	}
	public void setAdvertUrl(String advertUrl) {
		this.advertUrl = advertUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
    
	public String getImageVisitUrl() {
		return imageVisitUrl;
	}

	public void setImageVisitUrl(String imageVisitUrl) {
		this.imageVisitUrl = imageVisitUrl;
	}

	public String getUploadTime() {
		return uploadTime;
	}

	public void setUploadTime(String uploadTime) {
		this.uploadTime = uploadTime;
	}

	public int getAppId() {
		return appId;
	}

	public void setAppId(int appId) {
		this.appId = appId;
	}
	
  
}
