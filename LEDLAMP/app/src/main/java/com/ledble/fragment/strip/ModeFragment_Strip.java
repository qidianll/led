package com.ledble.fragment.strip;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import butterknife.Bind;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.ble.MainActivity_Strip;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.constant.CommonConstant;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView4RGB;

/**
 * 色温
 * 
 * @author ftl
 *
 */
public class ModeFragment_Strip extends LedBleFragment {
	
	@Bind(R.id.listViewModel) ListView listViewModel;
	@Bind(R.id.textViewCurretModel) TextView textViewCurretModel;
	
//	@Bind(R.id.imageViewPlayMode) Button buttonPlay;
	@Bind(R.id.imageViewOnOff) Button imageViewOnOff;
	
	@Bind(R.id.seekBarMode) SeekBar seekBarMode;
	@Bind(R.id.textViewMode) TextView textViewMode;
	@Bind(R.id.seekBarSpeed) SeekBar seekBarSpeedBar;
	@Bind(R.id.textViewSpeed) TextView textViewSpeed;
	@Bind(R.id.seekBarBrightNess) SeekBar seekBarBrightness;
	@Bind(R.id.textViewBrightNess) TextView textViewBrightness;
	
	private ModelAdapter maAdapter;

	private RelativeLayout mRlModeTop;
	private Button buttonPlay;
	
	private View mContentView;
//	private MainActivity_BLE mActivity;
	private MainActivity_Strip mActivity;
	
	private int offOnBtnState;
	private int playBtnState = 1;
	
	
	
	private PopupWindow mPopupWindow;
	private View menuView;
	private int currentTab = 2;// 1：色环，2：模式
	private String diyViewTag ;
	private SharedPreferences sharedPreferences;
	private ArrayList<ColorTextView> colorTextViews;
	private static final int COLOR_DEFALUT = 0;
	private ColorTextView actionView;
	private int currentSelecColorFromPicker;
	private SegmentedRadioGroup srgCover;
	private TextView tvCoverModel;
	private TextView textRGB;
	private LinearLayout llRing;
	private LinearLayout llCover;
	private ListView lvCover;
	private MyColorPickerImageView4RGB imageViewPicker2;
	private BlackWiteSelectView blackWiteSelectView2;
	private SeekBar seekBarModeSC;
	private TextView textViewModeSC;
	private TextView textViewRingBrightSC;
	private SeekBar seekBarSpeedBarSC;
	private TextView textViewSpeedSC;
	private SeekBar seekBarBrightBarSC;
	private TextView textViewBrightSC;
	private Button buttonSelectColorConfirm;// 确认
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_mode, container, false);
		menuView = inflater.inflate(R.layout.activity_select_color_spi, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		

		if (null != MainActivity_Strip.getMainActivity()) {
			mActivity = MainActivity_Strip.getMainActivity();
			if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
				mRlModeTop = MainActivity_Strip.getMainActivity().getRLModeTop();
			}
		}
	}

	@Override
	public void initView() {

		final int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff, 0xff00ffff, 0xffff00ff };
		
		if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			sharedPreferences = getActivity().getSharedPreferences(Constant.SPI_MODE_DIY, Context.MODE_PRIVATE);
		}

	 
		if (null != MainActivity_Strip.getMainActivity() && 
				MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			
			this.listViewModel.setAdapter(buildBLEModel());
			this.seekBarMode.setMax(30);
			
		} else if (null != MainActivity_Strip.getMainActivity() && 
				MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			
			this.listViewModel.setAdapter(buildSPIModel());
			this.seekBarMode.setMax(70);
			
		}
		
		this.listViewModel.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				
				maAdapter.setIndex(position);
				maAdapter.notifyDataSetChanged();
				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
				String value = abean.getValue().replaceAll(" ","");//去掉空格;
				textViewCurretModel
						.setText(getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));
				textViewCurretModel.setTag(value);
								
				if (null != mActivity) {  
					mActivity.pauseMusicAndVolum(true);
					mActivity.setRegMode(Integer.parseInt(value));
				}

			}
		});
		
		
		// 开始/暂停
		if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {

			buttonPlay = mRlModeTop.findViewById(R.id.imageViewPlayMode);
			buttonPlay.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {

					if (playBtnState == 0) {
						buttonPlay.setBackgroundResource(R.drawable.bg_play_pause);
						playBtnState = 1;
						if (null != mActivity) {
							mActivity.pauseMusicAndVolum(true);
							mActivity.setSPIPause(playBtnState);
						}

					} else {
						buttonPlay.setBackgroundResource(R.drawable.bg_play);
						playBtnState = 0;
						if (null != mActivity) {
							mActivity.pauseMusicAndVolum(true);
							mActivity.setSPIPause(playBtnState);
						}
					}
				}
			});
		}
		
		
		this.seekBarMode.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				if ((null != MainActivity_Strip.getMainActivity() && 
					MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP))) {
					
					if (progress >= 0 && progress <= 28) {
												
						AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
						String style = abean.getValue().replaceAll(" ","");//去掉空格;	
						
						if (null != mActivity) {
							mActivity.pauseMusicAndVolum(true);
							mActivity.setRegMode(Integer.parseInt(style));
						}
						
						textViewMode.setText(getActivity().getResources().getString(R.string.mode_set, progress+1));	
						
						listViewModel.setSelection(progress);
						maAdapter.notifyDataSetInvalidated();//通知adapter数据有变化					
						textViewCurretModel.setText(getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));	
					}
				}else if (null != MainActivity_Strip.getMainActivity() && 
						MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
					
					if (progress >= 0 && progress <= 70) {
						mActivity.pauseMusicAndVolum(true);
						if (progress == 0) {
							mActivity.setRegMode(255);
						} else {
							AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
							String style = abean.getValue().replaceAll(" ", "");//去掉空格;
							mActivity.setRegMode(Integer.parseInt(style));
						}

						textViewMode.setText(getActivity().getResources().getString(R.string.mode_set, progress));

						listViewModel.setSelection(progress);
						maAdapter.notifyDataSetInvalidated();// 通知adapter数据有变化
						AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
						textViewCurretModel.setText(
									getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));
					}
				}
								
			}
		});
		
		this.seekBarSpeedBar.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 100) {
					
					if (null != mActivity) {
						mActivity.pauseMusicAndVolum(true);
						mActivity.setSpeed(progress);
					}
					textViewSpeed.setText(getActivity().getResources().getString(R.string.speed_set, progress));

				}
			}
		});
		
		this.seekBarBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress > 0 && progress <= 100) {
					if (null != mActivity) {
						mActivity.pauseMusicAndVolum(true);
						mActivity.setBrightNess(progress);
//						mActivity.setBrightNess((progress*32)/100);
					}
					textViewBrightness.setText(getActivity().getResources().getString(R.string.brightness_set, progress));					

				}
			}
		});
		
		
		
		
		
		// *********************************** 弹窗选色界面  ************************************//
		
		
		lvCover = (ListView) menuView.findViewById(R.id.lvCover);
//		lvCover.setAdapter(buildSPIModel());
		lvCover.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				maAdapter.setIndex(position);
				maAdapter.notifyDataSetChanged();
				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
				String value = abean.getValue().replaceAll(" ","");//去掉空格;
				tvCoverModel.setText(getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));
				tvCoverModel.setTag(value);
				
				String[] datas = abean.getLabel().split(":");
				currentSelecColorFromPicker = Integer.parseInt(datas[0].replaceAll(" ", ""));//去掉空格
				
				if (null != mActivity) {
					mActivity.setRegMode(Integer.parseInt(value));
				}				
				
//				Toast.makeText(mActivity, ""+currentSelecColorFromPicker, Toast.LENGTH_SHORT).show();
			}
		});	
		

		
		seekBarModeSC = (SeekBar) menuView.findViewById(R.id.seekBarMode);
		textViewModeSC = (TextView) menuView.findViewById(R.id.textViewMode);
		seekBarModeSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				if (null != MainActivity_Strip.getMainActivity()
						&& MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {

					if (progress >= 0 && progress <= 70) {
						mActivity.pauseMusicAndVolum(true);
						if (progress == 0) {
							mActivity.setRegMode(255);
							currentSelecColorFromPicker = 255;
						} else {
							AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
							String style = abean.getValue().replaceAll(" ","");//去掉空格
							mActivity.setRegMode(Integer.parseInt(style));
							currentSelecColorFromPicker = Integer.parseInt(style);
						}

						textViewModeSC.setText(getActivity().getResources().getString(R.string.mode_set, progress));

						lvCover.setSelection(progress);
						maAdapter.notifyDataSetInvalidated();// 通知adapter数据有变化
						AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
						tvCoverModel.setText(
								getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));
					}
				}
			}
		});
		if (null != MainActivity_Strip.getMainActivity() && 
				MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			
			lvCover.setAdapter(buildBLEModel());
			seekBarModeSC.setMax(30);
			
		} else if (null != MainActivity_Strip.getMainActivity() && 
				MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			
			lvCover.setAdapter(buildSPIModel());
//			seekBarModeSC.setMax(71);
			
		}
		

		seekBarBrightBarSC = (SeekBar) menuView.findViewById(R.id.seekBarBrightNess);
		textViewBrightSC = (TextView) menuView.findViewById(R.id.textViewBrightNess);
		seekBarBrightBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (0 == progress) {
					if (null != mActivity) {
						mActivity.setBrightNess(1);
					}

					textViewBrightSC.setText(getActivity().getResources().getString(R.string.brightness_set, 1));

					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag + "modebright" + CommonConstant.LED_SPI,
								diyViewTag + "bright" + CommonConstant.LED_SPI, 1);
					}
				} else {
					if (null != mActivity) {
						mActivity.setBrightNess(progress);						
					}

					textViewBrightSC.setText(getActivity().getResources().getString(R.string.brightness_set, progress));

					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag + "modebright" + CommonConstant.LED_SPI,
								diyViewTag + "bright" + CommonConstant.LED_SPI, progress);
					}
				}
			}
		});

		seekBarSpeedBarSC = (SeekBar) menuView.findViewById(R.id.seekBarSpeed);
		textViewSpeedSC = (TextView) menuView.findViewById(R.id.textViewSpeed);
		seekBarSpeedBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					if (null != mActivity) {
						mActivity.setSpeed(1);
					}

					textViewSpeedSC.setText(getActivity().getResources().getString(R.string.speed_set, 1));

					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag + "modespeed" + CommonConstant.LED_SPI,
								diyViewTag + "modespeed" + CommonConstant.LED_SPI, 1);
					}

				} else {
					if (null != mActivity) {
						mActivity.setSpeed(progress);
					}

					textViewSpeedSC.setText(getActivity().getResources().getString(R.string.speed_set, progress));
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag + "modespeed" + CommonConstant.LED_SPI,
								diyViewTag + "modespeed" + CommonConstant.LED_SPI, progress);
					}
				}
			}
		});
		
		
		if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {

			buttonSelectColorConfirm = (Button) menuView.findViewById(R.id.buttonSelectColorConfirm); // 通过另外一个布局对象的findViewById获取其中的控件
			textRGB = (TextView) menuView.findViewById(R.id.tvRGB);
			srgCover = (SegmentedRadioGroup) menuView.findViewById(R.id.srgCover);
			llRing = (LinearLayout) menuView.findViewById(R.id.llRing);
			llCover = (LinearLayout) menuView.findViewById(R.id.llCover);
			tvCoverModel = (TextView) menuView.findViewById(R.id.tvCoverModel);
			imageViewPicker2 = (MyColorPickerImageView4RGB) menuView.findViewById(R.id.imageViewPicker2);
			blackWiteSelectView2 = (BlackWiteSelectView) menuView.findViewById(R.id.blackWiteSelectView2);
			seekBarSpeedBarSC = (SeekBar) menuView.findViewById(R.id.seekBarSpeed);
			textViewSpeedSC = (TextView) menuView.findViewById(R.id.textViewSpeed);
			seekBarBrightBarSC = (SeekBar) menuView.findViewById(R.id.seekBarBrightNess);
			textViewBrightSC = (TextView) menuView.findViewById(R.id.textViewBrightNess);
			textViewRingBrightSC = (TextView) menuView.findViewById(R.id.tvRingBrightnessSC);

			
			
			initColorBlock();
			initColorSelecterView();
		}
		
	}

	@Override
	public void initEvent() {

	}
	
	
	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
//		View blocks = mRlModeTop.findViewById(R.id.linearLayoutViewBlocks);
		// 3个自定义view
		colorTextViews = new ArrayList<ColorTextView>();
		for (int i = 1; i <= 3; i++) {
			final ColorTextView tv = (ColorTextView) mRlModeTop.findViewWithTag((String.valueOf("diyColor") + i));
			String tag = (String) tv.getTag()+CommonConstant.LED_SPI;
			int color = sharedPreferences.getInt(tag, COLOR_DEFALUT);
			if (color != COLOR_DEFALUT) {
				int radius = 10;
				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
				// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
				// 组合圆角矩形和ShapeDrawable
				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
				// 设置形状的颜色
				shapeDrawable.getPaint().setColor(color);
				// 设置绘制方式为填充
				shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
				// 将当前选择得颜色设置到触发颜色编辑器得View上
//				tv.setBackgroundDrawable(shapeDrawable);
				tv.setColor(color);
				if (color == 255) {
					tv.setText("Auto");
				}else {
					tv.setText(""+color);
				}				
			}
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.layout_scale));
					int color = tv.getColor();
					
					diyViewTag = (String) tv.getTag();
					
					if (color == COLOR_DEFALUT) {
						showColorCover((ColorTextView) v, true);
					}else {
//						updateRgbText(Tool.getRGB(color), true);
						
						mActivity.setRegMode(color);
						listViewModel.setSelection(color);
						maAdapter.notifyDataSetInvalidated();
						
						String[] ary = getActivity().getResources().getStringArray(R.array.spi_model);
						String valueStr = ary[color];
						String label[] = valueStr.split(",");							
						
						mActivity.setRegMode(Integer.parseInt(label[1].replaceAll(" ","")));
						
											
						final int bright = SharePersistent.getBrightData(getActivity(),
								diyViewTag + "modebright" + CommonConstant.LED_SPI,
								diyViewTag + "modebright" + CommonConstant.LED_SPI);

						final int speed = SharePersistent.getBrightData(getActivity(),
								diyViewTag + "modespeed" + CommonConstant.LED_SPI,
								diyViewTag + "modespeed" + CommonConstant.LED_SPI);						
											
						if (null != mActivity) {
														
							if (0 == bright) {
								
								final Handler handler = new Handler();
								Runnable runnable = new Runnable() {
									@Override
									public void run() {
										mActivity.setBrightNess(100);
										handler.removeCallbacksAndMessages(null);
									}
								};
								handler.postDelayed(runnable, 100);									
								
							}else {
								
								final Handler handler = new Handler();
								Runnable runnable = new Runnable() {
									@Override
									public void run() {
										mActivity.setBrightNess(bright);
										handler.removeCallbacksAndMessages(null);
									}
								};
								handler.postDelayed(runnable, 100);	
							}
							
							if (0 == speed) {
								final Handler handler = new Handler();
								Runnable runnable = new Runnable() {
									@Override
									public void run() {
										mActivity.setSpeed(85);
										handler.removeCallbacksAndMessages(null);
									}
								};
								handler.postDelayed(runnable, 200);	
								
							}else {
								final Handler handler = new Handler();
								Runnable runnable = new Runnable() {
									@Override
									public void run() {
										mActivity.setSpeed(speed);
										handler.removeCallbacksAndMessages(null);
									}
								};
								handler.postDelayed(runnable, 200);										
							}
							
						}
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(COLOR_DEFALUT);
					String tag = (String) tv.getTag()+CommonConstant.LED_SPI;
					sharedPreferences.edit().putInt(tag, COLOR_DEFALUT).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
			
//			if ( i <= 3 || (i >= 5 && i <= 7)) {
				colorTextViews.add(tv);
//			}			
		}

	}
	
	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 * @param hasModel
	 */
	public void showColorCover(ColorTextView actionView, final boolean hasBrightNess) {
		// 数据初始化
		this.actionView = actionView;
		currentSelecColorFromPicker = COLOR_DEFALUT;
		srgCover.check(R.id.rbRing);
		tvCoverModel.setText(getActivity().getResources().getString(R.string.current_mode));
		maAdapter.setIndex(COLOR_DEFALUT);
		maAdapter.notifyDataSetChanged();
		textRGB.setText(getActivity().getString(R.string.r_g_b, 0, 0, 0));

		if (hasBrightNess) {
//			if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_BLE)) {
//				srgCover.setVisibility(View.VISIBLE);
//			}else {
				srgCover.setVisibility(View.GONE);
//			}
			llRing.setVisibility(View.GONE);
			llCover.setVisibility(View.VISIBLE);
			blackWiteSelectView2.setVisibility(View.GONE);
		} else {
			srgCover.setVisibility(View.INVISIBLE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.GONE);
			textViewRingBrightSC.setVisibility(View.GONE);
		}
		
//		if (hasBrightNess) {
//			srgCover.setVisibility(View.VISIBLE);
//			llRing.setVisibility(View.VISIBLE);
//			llCover.setVisibility(View.VISIBLE);
//			blackWiteSelectView2.setVisibility(View.VISIBLE);
//		} else {
//			srgCover.setVisibility(View.INVISIBLE);
//			llRing.setVisibility(View.VISIBLE);
//			llCover.setVisibility(View.GONE);
//			blackWiteSelectView2.setVisibility(View.GONE);
//			textViewRingBrightSC.setVisibility(View.GONE);
//		}

		mPopupWindow = new PopupWindow(menuView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
		mPopupWindow.showAtLocation(mContentView, Gravity.BOTTOM, 0, 0);

	}
	
	
	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {

		imageViewPicker2.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView2.setStartColor(color);
				currentSelecColorFromPicker = color;

				int[] colors = Tool.getRGB(color);
				updateRgbText(colors, false);
				textRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright", diyViewTag+"bright", 32);
			}
		});

		blackWiteSelectView2.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
//				currentSelecColorFromPicker = color;
				mActivity.pauseMusicAndVolum(true);
				int p = progress;
				if (progress<=0) {
					p = 0;
				}else if (progress >= 100) {
					p = 100;
				}
				
				textViewRingBrightSC.setText(getActivity().getResources().getString(R.string.brightness_set, p));
				SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright", diyViewTag+"bright", p);

				if (null != mActivity) {
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						mActivity.setBrightNess((p*32)/100);
					}else {
						mActivity.setBrightNess(p);
					}
				}
			}
		});

		View viewColors = menuView.findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
//		int[] colors = { 0xFF0000, 0x00FF00,  0x0000FF, 0xFFFFFF,  0xFFFF00,  0xff00ff };

//		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView2.setStartColor(selectColor);
					imageViewPicker2.move2Ege(rmap.get(selectColor));
					updateRgbText(Tool.getRGB(selectColor), true);
					int[] colors = Tool.getRGB(selectColor);
					textRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				}
			});
			viewCsArrayLis.add(vc);
		}

		buttonSelectColorConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (currentSelecColorFromPicker != COLOR_DEFALUT) {
					if (currentTab == 1) {
						int radius = 10;
						float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
						// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
						// 就会根据形状来绘制。
						RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
						// 组合圆角矩形和ShapeDrawable
						ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
						// 设置形状的颜色
						shapeDrawable.getPaint().setColor(currentSelecColorFromPicker);
						// 设置绘制方式为填充
						shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag()+CommonConstant.LED_SPI;
						sharedPreferences.edit().putInt(tag, currentSelecColorFromPicker).commit();
						// 将当前选择得颜色设置到触发颜色编辑器得View上
						actionView.setBackgroundDrawable(shapeDrawable);
					} else {
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag()+CommonConstant.LED_SPI;
						if (null != tvCoverModel) {
							if (null != tvCoverModel.getTag().toString()) {							
								
								sharedPreferences.edit().putInt(tag, currentSelecColorFromPicker).commit();
//								Drawable image = getImage(value);
//								actionView.setBackgroundDrawable(image);
								
								if (currentSelecColorFromPicker == 255) {
									actionView.setText("Auto");
									actionView.setText(""+currentSelecColorFromPicker);
								}else {
									actionView.setText(""+currentSelecColorFromPicker);
								}
							}
						}
					}
//					actionView.setText("");
				}
				
				hideColorCover();
			}
		});
	}
	
	public Drawable getImage(String value) {
		int resID = getActivity().getResources().getIdentifier("img_" + value, "drawable", "com.ledlamp");
		return getActivity().getResources().getDrawable(resID);
	}
	
	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		mPopupWindow.dismiss(); // 隐藏
	}

	public void updateRgbText(int rgb[], boolean isClick) {
		try {
			mActivity.pauseMusicAndVolum(true);
			textRGB.setText(getActivity().getString(R.string.r_g_b, rgb[0], rgb[1], rgb[2]));
			
			if (null != mActivity) {
				mActivity.setRgb(rgb[0], rgb[1], rgb[2], isClick);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Tool.ToastShow(getActivity(), "错误。。。");
		}
	}
	

	
	private ModelAdapter buildBLEModel() {
		String[] ary = getActivity().getResources().getStringArray(R.array.ble_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1].replaceAll(" ",""));
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(getActivity(), abs);
		return maAdapter;
	}
	
	private ModelAdapter buildSPIModel() {
		String[] ary = getActivity().getResources().getStringArray(R.array.spi_model);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1].replaceAll(" ",""));
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(getActivity(), abs);
		return maAdapter;
	}

}
