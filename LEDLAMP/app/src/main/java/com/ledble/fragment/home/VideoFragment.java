package com.ledble.fragment.home;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.add.VideoAdapter;
import com.add.VideoBean;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.ResponseBean;
import com.ledlamp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.ittiger.player.PlayerManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class VideoFragment extends Fragment {
    private static final String TAG = "VideoFragment";
    View mView;
    RecyclerView mRecyclerView;

    public static List list;

    public VideoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_video, container, false);

        mRecyclerView = mView.findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        list = new ArrayList();
        Map<String, String> params = new HashMap<String, String>();
        params.put("appId", "1");
        HttpUtil.getInstance().getSourceData(true, getActivity(), Constant.queryNewVideo, params, new HttpUtil.HttpCallBack() {
            @Override
            public void onSuccess(String result) {
//                String url = result.substring(result.indexOf("Url") + 6, result.indexOf("returnCode") - 4);
                ResponseBean<VideoBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<VideoBean>>() {
                });
                if (responseBean != null) {
                    if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
                        VideoBean videoBean = responseBean.getContent();
                        if (videoBean != null && videoBean.getVideoVisitUrl() != null) {
                            String url = videoBean.getVideoVisitUrl();
                            for (int i = 0; i < 7; i++) {
                                list.add(url);
                            }
                            mRecyclerView.setAdapter(new VideoAdapter(getContext()));
                        }
                    } else {
                        Toast.makeText(getActivity(), responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onException(String message) {
                Toast.makeText(getActivity(), getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
            }
        });
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PlayerManager.getInstance().stop();
    }
}
