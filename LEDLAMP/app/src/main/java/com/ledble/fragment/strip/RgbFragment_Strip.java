package com.ledble.fragment.strip;

import java.util.ArrayList;
import java.util.HashMap;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.ble.MainActivity_Strip;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.constant.CommonConstant;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.view.MyColorPickerImageView.OnTouchPixListener;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView;
import com.ledble.view.MyColorPickerImageView4RGB;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

/**
 * 彩色
 * 
 * @author ftl
 *
 */
public class RgbFragment_Strip extends LedBleFragment {

	private SegmentedRadioGroup segmentedRadioGroup;
	
	
	@Bind(R.id.relativeTab1) View relativeTab1;// 色环
	@Bind(R.id.relativeTab2) View relativeTab2;// 色溫
	@Bind(R.id.relativeTab3) View relativeTab3;// 单色
	@Bind(R.id.relativeTabBN) View relativeTabBN;// 亮度
	
	
//	private MainActivity_BLE mActivity;
	private MainActivity_Strip mActivity;

	private View mContentView;
	private View menuView;
	
	
	// RGB色环
	private int rgbOnOffStatus = 0;
	@Bind(R.id.imageViewOnOff) ImageView imageViewOnOff;
	@Bind(R.id.tvRGB) TextView textViewRGB;
	@Bind(R.id.imageViewPicker) MyColorPickerImageView4RGB imageViewPicker;
	@Bind(R.id.blackWiteSelectView) BlackWiteSelectView blackWiteSelectView;
	@Bind(R.id.tvBrightness) TextView tvBrightness;
	

	// 色温
//	private int ctOnOffStatus = 0;
	@Bind(R.id.textViewWarmCool) TextView textViewWarmCool;
	@Bind(R.id.pikerImageView) MyColorPickerImageView pikerImageView;
	@Bind(R.id.seekBarBrightNess) SeekBar seekBarBrightNess;
	@Bind(R.id.textViewBrightNess) TextView textViewBrightNess;
//	@Bind(R.id.imageViewOnOffCT) ImageView imageViewOnOffCT;
	
	// 亮度
//	private int ctOnOffStatus = 0;
	@Bind(R.id.seekBarRedBrightNess) SeekBar seekBarRedBrightNess;
	@Bind(R.id.tvBrightness1) TextView tvBrightness1;
	@Bind(R.id.seekBarGreenBrightNess) SeekBar seekBarGreenBrightNess;
	@Bind(R.id.tvBrightness2) TextView tvBrightness2;
	@Bind(R.id.seekBarBlueBrightNess) SeekBar seekBarBlueBrightNess;
	@Bind(R.id.tvBrightness3) TextView tvBrightness3;
	@Bind(R.id.imageViewOnOffBrightness) ImageView imageViewOnOffBrightness;
	
	
	// 单色
	private int dimOnOffStatus = 0;
	@Bind(R.id.textViewBrightNessDim) TextView textViewBrightNessDim;
	@Bind(R.id.pikerImageViewDim) MyColorPickerImageView pikerImageViewDim;
	@Bind(R.id.imageViewOnOffDim) ImageView imageViewOnOffDim;
	
	
	
	// 弹窗选色界面
	private MyColorPickerImageView4RGB imageViewPicker2;
	private BlackWiteSelectView blackWiteSelectView2;
	private TextView textViewRingBrightSC;
	private SeekBar seekBarSpeedBarSC;
	private TextView textViewSpeedSC;
	private SeekBar seekBarBrightBarSC;
	private TextView textViewBrightSC;
	
	private int currentTab = 1;// 1：色环，2：模式
	private SharedPreferences sp;
	private SegmentedRadioGroup srgCover;
	private TextView tvCoverModel;
	private TextView textRGB;
	private LinearLayout llRing;
	private LinearLayout llCover;
	private ListView lvCover;
	
	private int brightnessValue;
	private ModelAdapter maAdapter;
	private String diyViewTag;
	private int currentSelecColorFromPicker;
	private ColorTextView actionView;
	private PopupWindow mPopupWindow;
	
	private Button buttonSelectColorConfirm;// 确认
	
	
	
	
	private static final int COLOR_DEFALUT = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_rgb, container, false); 
		menuView = inflater.inflate(R.layout.activity_select_color, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		

		if (null != MainActivity_Strip.getMainActivity()) {
			mActivity = MainActivity_Strip.getMainActivity();
			segmentedRadioGroup = mActivity.getSegmentRgb();

		}
		
					

//		if (null != MainActivity_BLE.getMainActivity()) {
//			if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_BLE)) {
//				getActivity().findViewById(R.id.rbRgbThree).setVisibility(View.GONE);
//			}else if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
//				getActivity().findViewById(R.id.rbRgbTwo).setVisibility(View.GONE);
//			}
//			else if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
//				getActivity().findViewById(R.id.rbRgbThree).setVisibility(View.GONE);
//			} 
//			else if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
//				getActivity().findViewById(R.id.rbRgbTwo).setVisibility(View.GONE);
//			} 
//			else if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SMART)) {
//				getActivity().findViewById(R.id.rbRgbTwo).setVisibility(View.GONE);
//				getActivity().findViewById(R.id.rbRgbThree).setVisibility(View.GONE);
//			}
//			
//			if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
//				getActivity().findViewById(R.id.rbRgbThree).setVisibility(View.GONE);
//			}
//			if (MainActivity_BLE.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
//				getActivity().findViewById(R.id.rbRgbTwo).setVisibility(View.GONE);
//			}
//		}
		
		if (null != MainActivity_Strip.getMainActivity()) {	
			if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
				getActivity().findViewById(R.id.rbRgbThree).setVisibility(View.GONE);
			}
			if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
				getActivity().findViewById(R.id.rbRgbTwo).setVisibility(View.GONE);
			}
		}
		
//		if (null != segmentedRadioGroup) {
//			Toast.makeText(mActivityStrip, "segmentedRadioGroup 不为空", Toast.LENGTH_SHORT).show();
//		}else {
//			Toast.makeText(mActivityStrip, "segmentedRadioGroup 为空", Toast.LENGTH_SHORT).show();
//		}
		
		segmentedRadioGroup.check(R.id.rbRgbOne);
		segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbRgbOne == checkedId) {
					relativeTab1.setVisibility(View.VISIBLE);
					relativeTab2.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
					relativeTabBN.setVisibility(View.GONE);
				} else if ((R.id.rbRgbTwo == checkedId)) {
					relativeTab2.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
					relativeTabBN.setVisibility(View.GONE);
				} else if ((R.id.rbRgbThree == checkedId)) {
					relativeTab3.setVisibility(View.GONE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab2.setVisibility(View.GONE);
					relativeTabBN.setVisibility(View.VISIBLE);
				} else if ((R.id.rbRgbFour == checkedId)) {
					relativeTab3.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab2.setVisibility(View.GONE);
					relativeTabBN.setVisibility(View.GONE);
				}
			}
		});
		
		
		//********************************色环*******************************//
		
		textViewRGB.setText(getActivity().getString(R.string.r_g_b, 0, 0, 0));
		this.imageViewPicker.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int[] colors = Tool.getRGB(color);
				blackWiteSelectView.setStartColor(color);
				updateRgbText(colors);
				textViewRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
			}
		});
		
		this.blackWiteSelectView.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
				int p = progress;
				if (progress <= 0) {
					p = 1;
				} else if (progress >= 100) {
					p = 100;
				}
				tvBrightness.setText(getActivity().getResources().getString(R.string.brightness_set, p));
				
				if (null != mActivity) {
//					mActivity.setBrightNess((p*32)/100);
					mActivity.setBrightNess(p);
				}
//				else if (null != mActivityStrip) {
//					mActivityStrip.setBrightNess((p*32)/100);					
//				}
			}
		});
		
//		this.imageViewOnOff.setOnClickListener(new OnClickListener() { // 开关灯
//
//			@Override
//			public void onClick(View v) {
//				switch (rgbOnOffStatus) {
//				case 0:
//					imageViewOnOff.setImageResource(R.drawable.off_btn);					
//										
//					if (null != mActivity) {
//						mActivity.close();
//					}else if (null != mActivityStrip) {
//						mActivityStrip.turnOff();					
//					}
//					rgbOnOffStatus = 1;
//					break;
//				case 1:
//					imageViewOnOff.setImageResource(R.drawable.on_btn);					
//					
//					if (null != mActivity) {
//						mActivity.open();
//					}else if (null != mActivityStrip) {
//						mActivityStrip.turnOn();					
//					}
//					rgbOnOffStatus = 0;
//					break;
//
//				default:
//					break;
//				}
//			}
//		});
//		
//		
		//*********************************** 色温 ************************************//
		
		this.pikerImageView.setInnerCircle(0.459f);
		this.pikerImageView.setOnTouchPixListener(new OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int cool = (int) ((angle / 360) * 100);
				int warm = 100 - cool;
				String warmText = getActivity().getString(R.string.cool_warm, warm, cool);
				textViewWarmCool.setText(warmText);
				
				if (null != mActivity) {
					mActivity.setCT(warm, 100 - warm);
				}
			}
		});
		
		this.seekBarBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if(0==progress){
					String brightNess = getActivity().getResources().getString(R.string.brightness_set, String.valueOf(1));
					textViewBrightNess.setText(brightNess);
					seekBar.setProgress(1);
					
					if (null != mActivity) {
						mActivity.setBrightNess(1);
					}
				}else {
					String brightNess = getActivity().getResources().getString(R.string.brightness_set, String.valueOf(progress));
					textViewBrightNess.setText(brightNess);

					if (null != mActivity) {
						mActivity.setBrightNess(progress);
					}
				}
			}
		});
		

		
		
		//*********************************** 亮度 ************************************//
		
		this.seekBarRedBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {			
				
				if (progress >= 0 && progress <= 100) {
					tvBrightness1.setText(Integer.toString(progress));
					
					if (null != mActivity) {
						mActivity.setSmartBrightness(1, progress);
					}
				}								
			}
		});
		this.seekBarGreenBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				if (progress >= 0 && progress <= 100) {
					tvBrightness2.setText(Integer.toString(progress));
					
					if (null != mActivity) {
						mActivity.setSmartBrightness(2, progress);
					}
				}				
			}
		});
		this.seekBarBlueBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				if (progress >= 0 && progress <= 100) {
					tvBrightness3.setText(Integer.toString(progress));
					
					if (null != mActivity) {
						mActivity.setSmartBrightness(3, progress);
					}
				}								
			}
		});

		
		
		
		//************************************* 单色 ******************************//
		
		this.pikerImageViewDim.setInnerCircle(0.25f);
		this.pikerImageViewDim.setOnTouchPixListener(new OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int cool = (int) ((angle / 360) * 100);
				String percentColor = cool + "%";
				textViewBrightNessDim.setText(getActivity().getResources().getString(R.string.brightness) + ":" + percentColor);
				
				if (null != mActivity) {
//					mActivity.setDim((cool*32)/100);
					mActivity.setDim(cool);
				}
			}
		});
		

		
		
		
		
		//*********************************** 弹窗选色界面 ************************************//						
		
		textViewRingBrightSC = (TextView) menuView.findViewById(R.id.tvRingBrightnessSC);
		
		seekBarBrightBarSC = (SeekBar) menuView.findViewById(R.id.seekBarBrightNess);
		textViewBrightSC = (TextView) menuView.findViewById(R.id.textViewBrightNess);
		seekBarBrightBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (0 == progress) {
					if (null != mActivity) {
						mActivity.setBrightNess(1);
					}
					
					textViewBrightSC.setText(getActivity().getResources().getString(R.string.brightness_set, 1));
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP, 1);
					}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI, 1);
					}
					
				} else {
					if (null != mActivity) {
//						mActivity.setBrightNess((progress*32)/100);
						mActivity.setBrightNess(progress);
					}					
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP, progress);
					}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI, progress);
					}
				}
			}
		});
		
		seekBarSpeedBarSC = (SeekBar) menuView.findViewById(R.id.seekBarSpeed);
		textViewSpeedSC = (TextView) menuView.findViewById(R.id.textViewSpeed);
		seekBarSpeedBarSC.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					if (null != mActivity) {
						mActivity.setSpeed(1);
					}
					
					textViewSpeedSC.setText(getActivity().getResources().getString(R.string.speed_set, 1));
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_STRIP, diyViewTag+"speed"+CommonConstant.LED_STRIP, 1);
					}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_SPI, diyViewTag+"speed"+CommonConstant.LED_SPI, 1);
					}

				} else {
					if (null != mActivity) {
						mActivity.setSpeed(progress);
					}
					
					textViewSpeedSC.setText(getActivity().getResources().getString(R.string.speed_set, progress));
					
					if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_STRIP, diyViewTag+"speed"+CommonConstant.LED_STRIP, progress);
					}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						SharePersistent.saveBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_SPI, diyViewTag+"speed"+CommonConstant.LED_SPI, progress);
					}
				}
			}
		});
		
//		sp = getActivity().getSharedPreferences(Constant.SPF_DIY, Context.MODE_PRIVATE);
		if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			sp = getActivity().getSharedPreferences(Constant.STRIP_RGB_DIY, Context.MODE_PRIVATE);
		}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			sp = getActivity().getSharedPreferences(Constant.SPI_RGB_DIY, Context.MODE_PRIVATE);
		}
		
		textRGB = (TextView) menuView.findViewById(R.id.tvRGB);
		
		srgCover = (SegmentedRadioGroup) menuView.findViewById(R.id.srgCover);
		srgCover.check(R.id.rbRing);
		srgCover.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbRing == checkedId) {
					currentTab = 1;
					llRing.setVisibility(View.VISIBLE);
					llCover.setVisibility(View.GONE);
				} else if ((R.id.rbModle == checkedId)) {
					currentTab = 2;
					llCover.setVisibility(View.VISIBLE);
					llRing.setVisibility(View.GONE);
//					llCover.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);  
					
				}
			}
		});
		
		lvCover = (ListView) menuView.findViewById(R.id.lvCover);
		lvCover.setAdapter(buildModel());
		lvCover.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				maAdapter.setIndex(position);
				maAdapter.notifyDataSetChanged();
				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
				String value = abean.getValue().replaceAll(" ","");//去掉空格;
				tvCoverModel.setText(getActivity().getResources().getString(R.string.current_mode_format, abean.getLabel()));
				tvCoverModel.setTag(value);
				currentSelecColorFromPicker = Integer.parseInt(value);
				if (null != mActivity) {
					mActivity.setSPIModel(Integer.parseInt(value));
				}
				
			}
		});
		
		
		llRing = (LinearLayout) menuView.findViewById(R.id.llRing);
		llCover = (LinearLayout) menuView.findViewById(R.id.llCover);
		tvCoverModel = (TextView) menuView.findViewById(R.id.tvCoverModel);
		imageViewPicker2 = (MyColorPickerImageView4RGB) menuView.findViewById(R.id.imageViewPicker2);
		blackWiteSelectView2 = (BlackWiteSelectView) menuView.findViewById(R.id.blackWiteSelectView2);
		buttonSelectColorConfirm = (Button) menuView.findViewById(R.id.buttonSelectColorConfirm); // 通过另外一个布局对象的findViewById获取其中的控件
		

		

		

		initSingColorView();
		initColorBlock();
		initColorSelecterView();
	}

	@Override
	public void initView() {
//		tgbtn.setChecked(mActivity.isLightOpen);
//		seekBarBrightness.setProgress(mActivity.brightness);// 设置亮度
//		seekBarSpeedBar.setProgress(mActivity.speed);
	}

	@Override
	public void initEvent() {

	}

	// private void pauseMusicAndVolum() {
	// musicFragment.pauseMusic();
	// musicFragment.pauseVolum();
	// }

//	private void putDataBack(ArrayList<MyColor> colors) {
//		mActivity.setDiy(colors, style);
//
//	}

//	private ArrayList<MyColor> getSelectColor() {
//
//		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
//		if (!ListUtiles.isEmpty(colorTextViews)) {
//			for (ColorTextView ctx : colorTextViews) {
//				if (COLOR_DEFALUT != ctx.getColor()) {
//					int[] rgb = Tool.getRGB(ctx.getColor());
//					colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
//				}
//			}
//		}
//		return colorList;
//	}

	/**
	 * 初始化单色View的点击事件
	 */
	private void initSingColorView() {
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
//		int[] colors = { 0xFF0000, 0x00FF00,  0x0000FF, 0xFFFFFF,  0xFFFF00,  0xff00ff };

//		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		String tagStart = "viewColor";
		View.OnClickListener click = new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.layout_scale));
				int color = (Integer) v.getTag();
				updateRgbText(Tool.getRGB(color));
				blackWiteSelectView.setStartColor(color);
				imageViewPicker.move2Ege(rmap.get(color));
				int[] colors = Tool.getRGB(color);
				textViewRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
			}
		};
		ArrayList<View> views = new ArrayList<View>();
		for (int i = 1; i <= 6; i++) {
			View view = mContentView.findViewWithTag(tagStart + i);
			view.setOnClickListener(click);
			/*if (i==3) {
				Toast.makeText(mActivity, "tag:" + colors[i], Toast.LENGTH_SHORT).show();
			}*/
			view.setTag(colors[i - 1]);
			views.add(view);
		}

	}

	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
//		View blocks = mContentView.findViewById(R.id.linearLayoutViewBlocks);
		// 16个自定义view
//		colorTextViews = new ArrayList<ColorTextView>();
//		for (int i = 1; i <= 16; i++) {
//			final ColorTextView tv = (ColorTextView) blocks.findViewWithTag((String.valueOf("labelColor") + i));
//			String tag = (String) tv.getTag();
//			int color = sp.getInt(tag, COLOR_DEFALUT);
//			if (color != COLOR_DEFALUT) {
//				int radius = 10;
//				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
//				// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
//				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
//				// 组合圆角矩形和ShapeDrawable
//				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
//				// 设置形状的颜色
//				shapeDrawable.getPaint().setColor(color);
//				// 设置绘制方式为填充
//				shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
//				// 将当前选择得颜色设置到触发颜色编辑器得View上
//				tv.setBackgroundDrawable(shapeDrawable);
//				tv.setColor(color);
//				tv.setText("");
//			}
//			tv.setOnClickListener(new OnClickListener() {
//				@Override
//				public void onClick(View v) {// 点击弹出颜色选择框
//					v.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.layout_scale));
//					int color = tv.getColor();
//					if (color == COLOR_DEFALUT) {
//						showColorCover((ColorTextView) v, false);
//					}
//				}
//			});
//			tv.setOnLongClickListener(new OnLongClickListener() {
//				@Override
//				public boolean onLongClick(View v) {
//					ColorTextView cv = (ColorTextView) v;
//					cv.setColor(COLOR_DEFALUT);
//					String tag = (String) tv.getTag();
//					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
//					// 长按删除颜色
//					cv.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.block_shap_color));
//					cv.setText("+");
//					return true;
//				}
//			});
//			colorTextViews.add(tv);
//		}

		View diy = mContentView.findViewById(R.id.linarLayoutColorCile);
		for (int i = 1; i <= 6; i++) {
			final ColorTextView tv = (ColorTextView) diy.findViewWithTag((String.valueOf("diyColor") + i));
			String tag = (String) tv.getTag();
			int color = sp.getInt(tag, COLOR_DEFALUT);

			if (color != COLOR_DEFALUT) {
				if (color < 128) {
					int radius = 10;
					float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
					// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
					RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
					// 组合圆角矩形和ShapeDrawable
					ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
					// 设置形状的颜色
					shapeDrawable.getPaint().setColor(color);
					// 设置绘制方式为填充
					shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
					// 将当前选择得颜色设置到触发颜色编辑器得View上
					tv.setBackgroundDrawable(shapeDrawable);
					tv.setColor(color);
				} else {
					Drawable image = getImage(color + "");
					tv.setBackgroundDrawable(image);
					tv.setColor(color);
				}
				tv.setText("");
			}

			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.layout_scale));
					int color = tv.getColor();
					
					diyViewTag = (String) tv.getTag();
					
					if (color != COLOR_DEFALUT) {
						if (color < 128) {
							
							int rgb[] = Tool.getRGB(color);
							
							int  bright = 0;
							if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
								bright = SharePersistent.getBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP);
							}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
								bright = SharePersistent.getBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI);
							}
							final int bright2 = bright;
							
							if (null != mActivity) {
								
								mActivity.setRgb(rgb[0], rgb[1], rgb[2], true);
								
								if (0 == bright) {
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setBrightNess(100); //默认 100
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 100);	
									
								}else {
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setBrightNess(bright2);  //取出亮度，并发送
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 100);										
								}
								
							}
							

						} else {							
														
							int  bright = 0;
							if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
								bright = SharePersistent.getBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP);
							}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
								bright = SharePersistent.getBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI);
							}
							final int bright2 = bright;
							
							int  speed = 0;
							if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
								speed = SharePersistent.getBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_STRIP, diyViewTag+"speed"+CommonConstant.LED_STRIP);
							}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
								speed = SharePersistent.getBrightData(getActivity(), diyViewTag+"speed"+CommonConstant.LED_SPI, diyViewTag+"speed"+CommonConstant.LED_SPI);
							}
							final int speed2 = speed;
							
//							Toast.makeText(mActivity, "bright="+bright2, Toast.LENGTH_SHORT).show();
//							Toast.makeText(mActivity, "speed="+speed2, Toast.LENGTH_SHORT).show();
							
							if (null != mActivity) {
								
								mActivity.setRegMode(color);
								
								if (0 == bright) {
									
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setBrightNess(100);
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 100);									
									
								}else {
									
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setBrightNess(bright2);
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 100);	
								}
								
								if (0 == speed) {
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setSpeed(85);
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 200);	
									
								}else {
									final Handler handler = new Handler();
									Runnable runnable = new Runnable() {
										@Override
										public void run() {
											mActivity.setSpeed(speed2);
											handler.removeCallbacksAndMessages(null);
										}
									};
									handler.postDelayed(runnable, 200);										
								}
								
							}
							
							
						}
					} else {
//						Toast.makeText(mActivity, "showColorCover", Toast.LENGTH_SHORT).show();
						showColorCover((ColorTextView) v, true);
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(COLOR_DEFALUT);
					String tag = (String) tv.getTag();
					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
		}
	}

	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 * @param hasModel
	 */
	public void showColorCover(ColorTextView actionView, final boolean hasBrightNess) {
		// 数据初始化
		this.actionView = actionView;
		currentSelecColorFromPicker = COLOR_DEFALUT;
		srgCover.check(R.id.rbRing);
		tvCoverModel.setText(getActivity().getResources().getString(R.string.current_mode));
		maAdapter.setIndex(COLOR_DEFALUT);
		maAdapter.notifyDataSetChanged();
		textRGB.setText(getActivity().getString(R.string.r_g_b, 0, 0, 0));

		if (hasBrightNess) {
			if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
				srgCover.setVisibility(View.VISIBLE);
			}else {
				srgCover.setVisibility(View.GONE);
			}
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.VISIBLE);
		} else {
			srgCover.setVisibility(View.INVISIBLE);
			llRing.setVisibility(View.VISIBLE);
			llCover.setVisibility(View.GONE);
			blackWiteSelectView2.setVisibility(View.GONE);
			textViewRingBrightSC.setVisibility(View.GONE);
		}

		mPopupWindow = new PopupWindow(menuView, WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT, true);
		mPopupWindow.showAtLocation(mContentView, Gravity.BOTTOM, 0, 0);
//		mPopupWindow.setAnimationStyle(R.style.animTranslate);

	}

	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		mPopupWindow.dismiss(); // 隐藏
	}

	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {

		imageViewPicker2.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView2.setStartColor(color);
				currentSelecColorFromPicker = color;

				int[] colors = Tool.getRGB(color);
				updateRgbText(colors);
				textRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				
				if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
					SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP, brightnessValue);
				}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
					SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI, brightnessValue);
				}
			}
		});

		blackWiteSelectView2.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
//				currentSelecColorFromPicker = color;

				int p = progress;
				if (progress <= 0) {
					p = 1;
				}
				if (progress >= 100) {
					p = 100;
				}
				brightnessValue = p;
				textViewRingBrightSC.setText(getActivity().getResources().getString(R.string.brightness_set, p));
				
				if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
					SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_STRIP, diyViewTag+"bright"+CommonConstant.LED_STRIP, p);
				}else if (MainActivity_Strip.getSceneBean().getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
					SharePersistent.saveBrightData(getActivity(), diyViewTag+"bright"+CommonConstant.LED_SPI, diyViewTag+"bright"+CommonConstant.LED_SPI, p);
				}
				
				if (null != mActivity) {
//					mActivity.setBrightNess((p*32)/100);
					mActivity.setBrightNess(p);
				}
			}
		});

		View viewColors = menuView.findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
//		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView2.setStartColor(selectColor);
					imageViewPicker2.move2Ege(rmap.get(selectColor));
					updateRgbText(Tool.getRGB(selectColor));
					
					int[] colors = Tool.getRGB(selectColor);
					textRGB.setText(getActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				}
			});
			viewCsArrayLis.add(vc);
		}

		buttonSelectColorConfirm.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				if (currentSelecColorFromPicker != COLOR_DEFALUT) {
					if (currentTab == 1) {
						int radius = 10;
						float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
						// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
						// 就会根据形状来绘制。
						RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null);
						// 组合圆角矩形和ShapeDrawable
						ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape);
						// 设置形状的颜色
						shapeDrawable.getPaint().setColor(currentSelecColorFromPicker);
						// 设置绘制方式为填充
						shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						sp.edit().putInt(tag, currentSelecColorFromPicker).commit();
						// 将当前选择得颜色设置到触发颜色编辑器得View上
						actionView.setBackgroundDrawable(shapeDrawable);
					} else {
						actionView.setColor(currentSelecColorFromPicker);
						// 保存颜色值
						String tag = (String) actionView.getTag();
						if (null != tvCoverModel) {
							if (null != tvCoverModel.getTag().toString()) {
								String value = tvCoverModel.getTag().toString();
								sp.edit().putInt(tag, Integer.parseInt(value)).commit();
								Drawable image = getImage(value);
								actionView.setBackgroundDrawable(image);
							}
						}
					}
					actionView.setText("");
				}
				
				hideColorCover();
			}
		});
	}

	public Drawable getImage(String value) {
		int resID = getActivity().getResources().getIdentifier("img_" + value, "drawable", "com.ledlamp");
		return getActivity().getResources().getDrawable(resID);
	}

	public void updateRgbText(int rgb[]) {
		try {			
			mActivity.setRgb(rgb[0], rgb[1], rgb[2], false);						
		} catch (Exception e) {
			e.printStackTrace();
			Tool.ToastShow(getActivity(), "错误。。。");
		}
	}

	private ModelAdapter buildModel() {
		String[] ary = getActivity().getResources().getStringArray(R.array.ble_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(getActivity(), abs);
		return maAdapter;
	}

}
