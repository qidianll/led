package com.ledble.fragment.stage;

import java.util.ArrayList;
import java.util.Random;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaRecorder;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore.Audio.Media;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.DrawTool;
import com.common.uitl.ListUtiles;
import com.common.uitl.SharePersistent;
import com.common.uitl.StringUtils;
import com.common.uitl.Tool;
import com.common.view.ScrollForeverTextView;
import com.common.view.Segment;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.activity.EditColorActivity;
import com.ledble.activity.MusicLibActivity;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.ble.MainActivity_STAGE;
import com.ledble.activity.wifi.MainActivity_WiFi;
import com.ledble.base.LedBleApplication;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.Mp3;
import com.ledble.bean.MyColor;
import com.ledble.view.VolumCircleBar;

/**
 * 闊充箰
 * 
 * @author ftl
 *
 */
public class MusicFragment_Stage extends LedBleFragment implements Runnable {

//	@Bind(R.id.segmentStyle) Segment segment;
	@Bind(R.id.imageViewPre) ImageView imageViewPre;
	@Bind(R.id.imageViewPlay) ImageView imageViewPlay;
	@Bind(R.id.imageViewNext) ImageView imageViewNext;
	@Bind(R.id.textViewAutoAjust) ScrollForeverTextView textViewAutoAjust;
	@Bind(R.id.tvCurrentTime) TextView tvCurrentTime;
	@Bind(R.id.tvTotalTime) TextView tvTotalTime;
	@Bind(R.id.llDecibel) LinearLayout llDecibel;
	@Bind(R.id.seekBarDecibel) SeekBar seekBarDecibel;
	@Bind(R.id.tvDecibelValue) TextView tvDecibelValue;
	@Bind(R.id.llBottom) LinearLayout llBottom;
	@Bind(R.id.seekBarMusic) SeekBar seekBarMusic;
	@Bind(R.id.imageViewRotate) ImageView imageViewRotate;
	@Bind(R.id.imageViewPlayType) ImageView imageViewPlayType;
	@Bind(R.id.buttonMusicLib) Button buttonMusicLib;
	@Bind(R.id.seekBarRhythm) SeekBar seekBarRhythm;
	@Bind(R.id.volumCircleBar) VolumCircleBar volumCircleBar;
	@Bind(R.id.tvRhythm) TextView tvRhythm;
	@Bind(R.id.tvrhythmValue) TextView tvrhythmValue;
//	@Bind(R.id.tvJumpView) TextView tvJumpView;

	private static final int INT_GO_SELECT_MUSIC = 100;
	private static final int INT_UPDATE_PROGRESS = 101;
	private static final int INT_EDIT_COLOR = 102;
	private static final int INT_UPDATE_RECORD = 103;
	private static int SAMPLE_RATE_IN_HZ = 8000;

	private ObjectAnimator mCircleAnimator;
	private SegmentedRadioGroup segmentedRadioGroup;
	private MainActivity_STAGE mActivity;
	private View mContentView;
	private Animation animationRotate;
	private Animation animationScale;
	
	private MediaPlayer mediaPlayer;
	private Visualizer mVisualizer;
	private VisualizerView mVisualizerView;
	private Mp3 currentMp3;
	
	private boolean isSending = true;
	private boolean isLoopAll = false;
	private boolean isLoopOne = true;
	private boolean isRandom = false;
	
	private int musicMode = 1;
	private int microMode = 0;

	private boolean isClickMicro = false; // 是否点击麦克风
	private volatile float microRecordValue; // 麦克风value
	
	private Handler mstartMicroHandler;
	private Runnable mstartMicroRunnable;

	private ArrayList<MyColor> colors;// colors 集合
	private volatile int chnnelValue;// 
	private boolean isStartTimer = false;
	
//	private int peridTime = 50;
	private int peridTime = 150;
	private int fy = 0;

	private Thread sendMusicThread;
	private Random random = new Random();
	private AudioRecord ar;
	private int bs;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_music, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		bs = AudioRecord.getMinBufferSize(SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT);
		
		ar = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT, bs);	
		
		//
		// mAudioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
		// SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_OUT_MONO,
		// AudioFormat.ENCODING_PCM_16BIT, bs,
		// AudioTrack.MODE_STREAM);

		mActivity = (MainActivity_STAGE) getActivity();
//		segmentedRadioGroup = mActivity.getSegmentMusic();
		
		mCircleAnimator = ObjectAnimator.ofFloat(imageViewRotate, "rotation", 0.0f, 360.0f);
		mCircleAnimator.setDuration(8000);
		mCircleAnimator.setInterpolator(new LinearInterpolator());
		mCircleAnimator.setRepeatCount(-1);
		mCircleAnimator.setRepeatMode(ObjectAnimator.RESTART);
		
		mVisualizerView = new VisualizerView(mActivity);  
        
	}
	
	public void requestMicroPermissionsSucess() {
		bs = AudioRecord.getMinBufferSize(SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT);
		
//		ar = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
//				AudioFormat.ENCODING_PCM_16BIT, bs);
		ar = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT, bs);
	}

	@Override
	public void initView() {
		new Thread(this).start();
		
	}
	
	private ArrayList<MyColor> getRandomColors() {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		colorList.add(new MyColor(getRandIntColor(), getRandIntColor(), getRandIntColor()));

		return colorList;
	}

	@SuppressLint("NewApi")
	@Override
	public void initEvent() {
		this.imageViewPre.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playAnimationPress(v);
				playPre();
			}
		});
		this.imageViewPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playAnimationPress(v);
				playPause();
			}
		});
		this.imageViewNext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				playAnimationPress(v);
				palayNext();
			}
		});
		this.seekBarDecibel.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				mActivity.setSpeed(progress);
				SharePersistent.savePerference(activity, "decibel", progress);
				tvDecibelValue.setText(String.valueOf(progress));
			}
		});
		this.seekBarDecibel.setProgress(SharePersistent.getInt(activity, "decibel"));
		this.animationScale = AnimationUtils.loadAnimation(activity, R.anim.layout_scale);
		this.animationRotate = AnimationUtils.loadAnimation(activity, R.anim.rotate_frover);
		this.animationRotate.setInterpolator(new LinearInterpolator());
		this.animationRotate.setRepeatCount(-1);
		this.animationRotate.setAnimationListener(new com.common.adapter.AnimationListenerAdapter() {
		});
		this.imageViewRotate.setOnClickListener(new OnClickListener() { // 中间旋转大图

			@Override
			public void onClick(View v) {
				switch (musicMode) {
				case 0:
					imageViewRotate.setImageResource(R.drawable.music_gradualchange);	//渐变			
					musicMode = 1;
					break;
				case 1:
					imageViewRotate.setImageResource(R.drawable.music_jump);	//跳变
					musicMode = 2;
					break;
				case 2:
					imageViewRotate.setImageResource(R.drawable.music_stroboflash);	//频闪
					musicMode = 3;
					break;
				case 3:
					imageViewRotate.setImageResource(R.drawable.music_nooutput);	//无输出
					musicMode = 0;
					break;

				default:
					break;
				}
				
				try {
					if (null != mediaPlayer && mediaPlayer.isPlaying()) {
						sendMusicMode();
					}					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		this.imageViewPlayType.setOnClickListener(new OnClickListener() { //播放 循环模式

			@Override
			public void onClick(View v) {
				if (isLoopAll) {
					imageViewPlayType.setImageResource(R.drawable.playtype_loopall);// 顺序播放
					Toast.makeText(mActivity, R.string.SequentialPlay, Toast.LENGTH_SHORT).show();
					isLoopAll = false;
					isLoopOne = true;
				} else if (isLoopOne) {
					imageViewPlayType.setImageResource(R.drawable.playtype_loopone); //单曲循环
					Toast.makeText(mActivity, R.string.SinglePlay, Toast.LENGTH_SHORT).show();
					isLoopOne = false;
					isRandom = true;
				} else if (isRandom) {
					imageViewPlayType.setImageResource(R.drawable.playtype_random); //随机播放
					Toast.makeText(mActivity, R.string.RandomPlay, Toast.LENGTH_SHORT).show();
					isRandom = false;
					isLoopAll = true;
				}
//				isSending = !isSending;
			}
		});
		this.buttonMusicLib.setOnClickListener(new OnClickListener() { //歌曲列表
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, MusicLibActivity.class);
				mActivity.startActivityForResult(intent, INT_GO_SELECT_MUSIC);
			}
		});

		mContentView.findViewById(R.id.imageViewEdit).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, EditColorActivity.class);
				mActivity.startActivityForResult(intent, INT_EDIT_COLOR);
			}
		});
		this.seekBarRhythm.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvrhythmValue.setText(String.valueOf(progress));
				mActivity.setSpeed(progress);
				SharePersistent.savePerference(mActivity, "rhythm", progress);
			}
		});
		this.seekBarRhythm.setProgress(SharePersistent.getInt(mActivity, "rhythm"));
		this.volumCircleBar.setVisibility(View.GONE);
		this.volumCircleBar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				switch (microMode) {
				case 0:
					volumCircleBar.toggleRecord(0);
					volumCircleBar.setBackgroundResource(R.drawable.micro_jump);
					microMode = 1;
					break;
				case 1:
					volumCircleBar.setBackgroundResource(R.drawable.micro_stroboflash);
					microMode = 2;
					break;
				case 2:
					volumCircleBar.setBackgroundResource(R.drawable.micro_gradualchange);
					microMode = 0;
					break;

				default:
					break;
				}

				try {
					if (isClickMicro = true) {
						sendMicroMode();
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

//		segmentedRadioGroup.check(R.id.rbMusicOne);
//		segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				if (R.id.rbMusicOne == checkedId) {
//
//					tvRhythm.setVisibility(View.VISIBLE);
//					seekBarRhythm.setVisibility(View.VISIBLE);
//					tvrhythmValue.setVisibility(View.VISIBLE);
//					imageViewRotate.setVisibility(View.VISIBLE);
//					volumCircleBar.setVisibility(View.GONE);
//					llDecibel.setVisibility(View.GONE);
//					llBottom.setVisibility(View.VISIBLE);
//					
//					try {
//						if (null != mediaPlayer && mediaPlayer.isPlaying()) {
//							sendMusicMode();
//						}
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					
//					isClickMicro = false;
//					
//					pauseVolum(true);
//				} else {
//																											
//					// 麦克风
//					if (mActivity.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
//						// 申请麦克风授权
//						mActivity.requestPermissions(new String[] { Manifest.permission.RECORD_AUDIO }, 111);
////						requestMicroPermissionsSucess();
////						checkMicroAuthorization();
//						return;
//						
//					}else {
//										
////						bs = AudioRecord.getMinBufferSize(SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
////								AudioFormat.ENCODING_PCM_16BIT);
////						
////						ar = new AudioRecord(MediaRecorder.AudioSource.DEFAULT, SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
////								AudioFormat.ENCODING_PCM_16BIT, bs);																								
//						
//						tvRhythm.setVisibility(View.GONE);
//						seekBarRhythm.setVisibility(View.GONE);
//						tvrhythmValue.setVisibility(View.GONE);
//						imageViewRotate.setVisibility(View.GONE);
//						volumCircleBar.setVisibility(View.VISIBLE);
//						llDecibel.setVisibility(View.VISIBLE);
//						llBottom.setVisibility(View.GONE);
//						
//						// 初始化					
//						volumCircleBar.setBackgroundResource(R.drawable.micro_gradualchange);
//						Toast.makeText(mActivity, R.string.microphone_start, Toast.LENGTH_SHORT).show();				
//						microMode = 0;
//						
//						volumCircleBar.toggleRecord(0);
//						ar.startRecording();
//						isClickMicro = true;
//						
//						startRecord();
//						
//						try {
//							sendMicroMode();
//							pauseMusic();
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}		
//					}
//					
//							
//				}
//			}
//		});
//		
		seekBarMusic.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				if (null == currentMp3) {
					Tool.ToastShow(mActivity, R.string.chose_list);
					seekBar.setProgress(0);
					return;
				}
				if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
					System.out.println("progress-->" + seekBar.getProgress());
					int duration = currentMp3.getDuration();
					int current = (int) seekBar.getProgress() * duration / 100;
					System.out.println("current-->" + current);
					mediaPlayer.seekTo(current);
				}
			}
		});
	}
	
	
	public void startRecord() {
		if (null == mstartMicroHandler) {
			mstartMicroHandler = new Handler(); 
		}
		if (null == mstartMicroRunnable) {
				
			mstartMicroRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					mstartMicroHandler.postDelayed(mstartMicroRunnable, 50);// 打开定时器，执行操作
					
					volumCircleBar.updateVolumRate(microRecordValue);
				}
			};
		}	
		mstartMicroHandler.postDelayed(mstartMicroRunnable, 500);// 打开定时器，执行操作
	}
	
	
	/**
	 * 打开权限提醒
	 * @param context
	 */

	private void checkMicroAuthorization() {
			
		
		final TextView editText = new TextView(mActivity);
		editText.setText(getResources().getString(R.string.micro_permission));
		new AlertDialog.Builder(mActivity).setTitle(R.string.authorization_title).setView(editText)
				.setIcon(R.drawable.ic_launcher)
				.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						openGPSSettings();
					}
				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}
	
	/**
	 * 跳转GPS设置
	 */
	private void openGPSSettings() {
		
		Intent intent = new Intent();                                
		intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
		Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
		intent.setData(uri);
		startActivity(intent);


//		Intent intent = new Intent(Media.RECORD_SOUND_ACTION);
//        startActivityForResult(intent, 222);  
	}
	

	@Override
	public void onResume() {
		super.onResume();
		// 璁剧疆闊充箰
		sendMusicData();
	}	

	private void sendMusicData() {
		if (null == sendMusicThread || isStartTimer == false) {
			sendMusicThread = new Thread(new Runnable() {
				@Override
				public void run() {
					isStartTimer = true;
					while (true) {
						try {
							if (chnnelValue > 0 && (isSending && null != mediaPlayer && mediaPlayer.isPlaying())) {
								sendMusicValue(getRandColor(), chnnelValue);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						Tool.delay(peridTime - fy);
						if (mActivity.isFinishing()) {
							break;
						}
					}
				}
			});
			sendMusicThread.start();
		}
	}


	/*
	 * 随机颜色
	 */
	private MyColor getRandColor() {
		if (null == colors || colors.size() == 0) {
			return null;
		}

		int r = random.nextInt(colors.size());
		MyColor color = colors.get(r);
		return color;
	}

	private void playAnimationPress(View v) {
		v.startAnimation(animationScale);
	}

	private Handler mhanHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int what = msg.what;
			switch (what) {
			case INT_UPDATE_PROGRESS:
				updatePlayProgress(currentMp3);
				if (mediaPlayer != null) {
					int current = (int) (mediaPlayer.getCurrentPosition());
					int musicTime = (int) current / 1000;
//					String time = "";
					String time = new String("");
					if (musicTime < 60) {
						if (musicTime < 10) {
							time = "0:0" + musicTime;
						} else {
							time = "0:" + musicTime;
						}
					} else {
//						String minute = "";
						String minute = new String("");
						if (musicTime % 60 < 10) {
							minute = "0" + musicTime % 60;
						} else {
							minute = "" + musicTime % 60;
						}
						time = musicTime / 60 + ":" + minute;
					}
					tvCurrentTime.setText(time);
			
				}
				break;
			case INT_UPDATE_RECORD:
				// int pre = (Integer) msg.obj;
				double pre = (Double) msg.obj;
				int dB = (int) (10 * Math.log10(pre));
//				int value = dB - 10;
				int value = dB - 35;

				float precent = (float) value / 100;
//				float precent = (float) value / 100;

				if (precent > 0) {
//					volumCircleBar.updateVolumRate(precent);
				}
							
				microRecordValue = precent;
				
//				tvDecibelValue.setText(String.valueOf(precent));
				
				sendVolumValue(getRandColor(), value);
			}
		}
	};

	private void updatePlayProgress(Mp3 currentMp3) {
		if (null != mediaPlayer && mediaPlayer.isPlaying()) {
			int duration = currentMp3.getDuration();
			int current = (int) (mediaPlayer.getCurrentPosition());
			if (duration != 0 && (current < duration)) {
				int percent = (int) (current * 100 / duration);
				seekBarMusic.setProgress(percent);
			}
		}
	}

	public void palayNext() {
		if (null == currentMp3) {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				startPlay(0);
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		} else {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				int currentIndex = findCurrentIndex(currentMp3);
				if (currentIndex != -1) {
					if (currentIndex < (LedBleApplication.getApp().getMp3s().size() - 1)) {
						startPlay(++currentIndex);
					} else if (currentIndex == (LedBleApplication.getApp().getMp3s().size() - 1)) {
						startPlay(0);
					}
				} else {
					startPlay(0);
				}
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		}
	}

	public void playPre() {
		if (null == currentMp3) {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				startPlay(0);
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		} else {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				int currentIndex = findCurrentIndex(currentMp3);
				if (currentIndex != -1) {
					if (currentIndex > 0) {
						startPlay(--currentIndex);
					} else if (currentIndex == 0) {
						startPlay(LedBleApplication.getApp().getMp3s().size() - 1);
					}
				} else {
					startPlay(0);
				}
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		}
	}

	/**
	 * 暂停音乐
	 */
	public void pauseMusic() {
		if (isSending) {
			if (mediaPlayer != null && mediaPlayer.isPlaying()) {
				// 鏆傚仠
				mediaPlayer.pause();
				imageViewPlay.setImageResource(R.drawable.bg_play);
				mCircleAnimator.pause();
			}
		}
	}

	/**
	 * 暂停麦克风
	 */
	public void pauseVolum(boolean status) {
		if (status) {
			if (volumCircleBar != null && volumCircleBar.recordMode() != 3) {
				volumCircleBar.toggleRecord(3);
			}
			if (mstartMicroHandler != null) {
				mstartMicroHandler.removeCallbacks(mstartMicroRunnable);
				mstartMicroHandler = null;
			}
		}else {			
			
			if (volumCircleBar != null) {
				volumCircleBar.toggleRecord(0);
	
				startRecord();
				
				if (isClickMicro) {
					try {
						sendMicroMode();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}				
			}
		}
	}

	public void playPause() {

		if (ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
			Tool.ToastShow(mActivity, R.string.chose_list);
			return;
		}

		if (null == mediaPlayer) {
			startPlay(0);
		} else {
			if (mediaPlayer.isPlaying()) {
				// 鏆傚仠
				mediaPlayer.pause();
				imageViewPlay.setImageResource(R.drawable.bg_play);
				stopRotate(); // 鏆傚仠鏃嬭浆鍔ㄧ敾
			} else {
				// 鎾斁
				if (null != currentMp3) {
					imageViewPlay.setImageResource(R.drawable.bg_play_pause);
					mediaPlayer.start(); 
					resumeRotate();  // 缁х画鏃嬭浆鍔ㄧ敾
					try {
						sendMusicMode();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
						startPlay(0);
					} else {
						Tool.ToastShow(mActivity, R.string.chose_list);
						return;
					}
				}
				
			}

		}

	}

	boolean isSettingBoolean = false;

	public void playMp3(final Mp3 mp3) {
		try {
			currentMp3 = mp3;
			setTitles(mp3);
			setAbulmImage(imageViewRotate, mp3);
			if (null == mediaPlayer) {
				mediaPlayer = new MediaPlayer();
			}
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.reset();
			mediaPlayer.setDataSource(mp3.getUrl());
			if (!isSettingBoolean) {
				isSettingBoolean = true;
				mVisualizer = new Visualizer(mediaPlayer.getAudioSessionId());
				mVisualizer.setCaptureSize(256);
			}
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					if (mVisualizer != null) {
						mVisualizer.setEnabled(false);
					}
					try {
						ArrayList<Mp3> list = LedBleApplication.getApp().getMp3s();
						int index = findCurrentIndex(mp3);
						
						if (!isLoopOne && isRandom) { //鍗曟洸寰幆
							mediaPlayer.start();  
							mediaPlayer.setLooping(true);
//							playMp3(list.get(index));
							return;
						}else if (!isRandom && isLoopAll) { //闅忔満鎾斁
							int playIndex = getRandIntPlayIndex(list.size());
							playMp3(list.get(playIndex));
							return;
						}
						
						if (-1 != index && !ListUtiles.isEmpty(list)) {
							if (index == (list.size() - 1)) {
								playMp3(list.get(0));
								return;
							}
							if (index <= (list.size() - 2)) {
								playMp3(list.get(index + 1));
								return;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mediaPlayer.prepare();
			mediaPlayer.start();
			startRotate();	//缁х画鏃嬭浆鍔ㄧ敾
			if (mVisualizer != null) {
				mVisualizer.setEnabled(false);
			}
			
			sendMusicMode();		//鍙戦�侀煶涔愭ā寮�
			

			mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
				public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
//					updateVisualizer(bytes);
					mVisualizerView.updateVisualizer(bytes);
				}

				public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
					mVisualizerView.updateVisualizer(fft);
//					updateVisualizer(fft);
				}
			}, Visualizer.getMaxCaptureRate() / 2, false, true);
			if (mVisualizer != null) {
				mVisualizer.setEnabled(true);
			}
			new Thread(this).start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void sendMusicMode() throws InterruptedException {
		switch (musicMode) {
		case 0:		//鏃犺緭鍑�
			mActivity.setRgb(255, 255, 255);
			mActivity.setBrightNess(100);
			break;
		case 1:		//娓愬彉
//			Toast.makeText(mActivity, "娓愬彉", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 3);	
			Thread.sleep(100);
			mActivity.setSpeed(80);
			break;
		case 2:		//璺冲彉
//			Toast.makeText(mActivity, "璺冲彉", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 0);
			Thread.sleep(100);
			mActivity.setSpeed(60);
			break;
		case 3:		//棰戦棯
//			Toast.makeText(mActivity, "棰戦棯", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 2);
			Thread.sleep(100);
			mActivity.setSpeed(98);
		
			break;

		default:
			break;
		}
		
		
	}
	
	public void sendMicroMode() throws InterruptedException {
		switch (microMode) {
		case 0:		//渐变
//			Toast.makeText(mActivity, "渐变", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 3);
			Thread.sleep(100);
			mActivity.setSpeed(80);
			break;
		case 1:		//跳变
//			Toast.makeText(mActivity, "跳变", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 0);
			Thread.sleep(100);
			mActivity.setSpeed(60);
			break;
		case 2:		//频闪
//			Toast.makeText(mActivity, "频闪", Toast.LENGTH_SHORT).show();
			mActivity.setDiy(getSelectColor(), 2);
			Thread.sleep(100);
			mActivity.setSpeed(98);
		
			break;

		default:
			break;
		}
	}
	
	private ArrayList<MyColor> getSelectColor() {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		
		if (null == colors || colors.size() == 0) {
			for (int i = 0; i < 7; i++) {
				colorList.add(new MyColor(getRandIntColor(), getRandIntColor(), getRandIntColor()));
			}
		} else {
			for (int i = 0; i < colors.size(); i++) {
				
				colorList.add(colors.get(i));
			}
//			Toast.makeText(mActivity, ""+colorList.size(), Toast.LENGTH_SHORT).show();
		}
	
		return colorList;
	}

	/**
	 **聽寮�鍚姩鐢宦犅� 聽
	 */
	public void startRotate() {
		
		mCircleAnimator.start();
	}
	
	/**
	 **聽缁х画鍔ㄧ敾聽聽 聽
	 */
	public void resumeRotate() {
//		imageViewRotate.clearAnimation();
//		imageViewRotate.onAnimationStart();
		mCircleAnimator.resume();
	}

	/**
	 **聽鏆傚仠鍔ㄧ敾聽聽 聽
	 */
	public void stopRotate() {
//		imageViewRotate.clearAnimation();
//		imageViewRotate.onAnimationStart();
		mCircleAnimator.pause();
	}

	/**
	 * 璁剧疆title鐜板疄鍐呭
	 * 
	 * @param mp3
	 */
	private void setTitles(Mp3 mp3) {
		String artist = mp3.getArtist();
		if (StringUtils.isEmpty(artist)) {
			textViewAutoAjust.setText(mp3.getTitle());
		} else {
			String title = getActivity().getResources().getString(R.string.ablum_title, "<<" + mp3.getTitle() + ">>",
					mp3.getArtist(), mp3.getAlbum());
			textViewAutoAjust.setText(title);
		}
	}

	/**
	 * 鑾峰彇骞朵笖涓撹緫涓撹緫鍥剧墖
	 * 
	 * @param imageViewImg
	 * @param mp3
	 */
	@SuppressLint("NewApi")
	private void setAbulmImage(ImageView imageViewImg, Mp3 mp3) {
		try {
			MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
			mediaMetadataRetriever.setDataSource(mp3.getUrl());
			byte[] data = mediaMetadataRetriever.getEmbeddedPicture();
			if (data != null) {
				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
				bitmap = DrawTool.toRoundBitmap(bitmap);
				// imageViewRotate.setImageBitmap(bitmap); // associated cover
				// art in//
			} else {
//				imageViewRotate.setImageResource(R.drawable.bg_special);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			imageViewRotate.setImageResource(R.z.bg_special);
		}
	}

	
	
	// *************************************音乐*************************************************//
	private void sendMusicValue(MyColor color, int chnnel) {
		if (musicMode == 0) {
			return;
		}

		if (isSending && null != mediaPlayer && mediaPlayer.isPlaying()) {

			mActivity.setBrightNess(chnnel);
			
		}

	}

	//*************************************麦克风*************************************************//
	private void sendVolumValue(MyColor color, int volumValue) {
		
		if (/*volumCircleBar != null && volumCircleBar.recordMode() != 3 &&*/ isClickMicro == true) {
			
			mActivity.setBrightNess(volumValue);
			
		}
		
	}



	Random rand = new Random();

	private int getRandIntColor() {
		int color = rand.nextInt(255);
		return color;
	}
	
	private int getRandIntPlayIndex(int size) {
		int index = rand.nextInt(size);
		return index;
	}


	private int findCurrentIndex(Mp3 mp3) {
		ArrayList<Mp3> list = LedBleApplication.getApp().getMp3s();
		if (ListUtiles.isEmpty(list)) {
			return -1;
		}
		for (int i = 0, lsize = list.size(); i < lsize; i++) {
			if (mp3.equals(list.get(i))) {
				return i;
			}
		}
		return -1;
	}

	public int avgData(byte[] fft) {
		int sum = 0;
		for (int i = 1; i < fft.length; i++) {
			sum += fft[i];
		}
		int ave = sum / (fft.length - 1);
		ave = (int) (Math.abs((float) ave / 127) * 100);
		return ave;
		// LogUtil.i(App.tag, "consult:" + sum / fft.length);
	}

	private void startPlay(int index) {
		if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())
				&& LedBleApplication.getApp().getMp3s().size() > index) {
			try {
				playMp3(LedBleApplication.getApp().getMp3s().get(index));
				imageViewPlay.setImageResource(R.drawable.bg_play_pause);
				int musicTime = mediaPlayer.getDuration() / 1000;
				String minute = "";
				if (musicTime % 60 < 10) {
					minute = "0" + musicTime % 60;
				} else {
					minute = "" + musicTime % 60;
				}
				String time = musicTime / 60 + ":" + minute;
				tvCurrentTime.setText("0:00");
				tvTotalTime.setText(time);
				// this.currentPlayIndex = index;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INT_GO_SELECT_MUSIC && resultCode == Activity.RESULT_OK) {
			startPlay(0);
		}
		if (requestCode == INT_EDIT_COLOR && resultCode == Activity.RESULT_OK && null != data) {
			colors = (ArrayList<MyColor>) data.getSerializableExtra("color");
			try {
				
				if (!isClickMicro) {
					sendMusicMode();
				}else {
					sendMicroMode();
				}
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		
		}
	}

	@Override
	public void run() {
		try {
			// ar.startRecording();
			// 鐢ㄤ簬璇诲彇鐨� buffer
			short[] buffer = new short[bs];
			while (true) {
				if (isSending && null != mediaPlayer && mediaPlayer.isPlaying()) {
					mhanHandler.sendEmptyMessage(INT_UPDATE_PROGRESS);
				}

				if (volumCircleBar != null && volumCircleBar.recordMode() != 3) {
					int r = ar.read(buffer, 0, bs);
					long v = 0;
					// 灏� buffer 鍐呭鍙栧嚭锛岃繘琛屽钩鏂瑰拰杩愮畻
					for (int i = 0; i < buffer.length; i++) {
						// 杩欓噷娌℃湁鍋氳繍绠楃殑浼樺寲锛屼负浜嗘洿鍔犳竻鏅扮殑灞曠ず浠ｇ爜
						v += buffer[i] * buffer[i];
					}

					// int value = (int)(v / r);
					double value = v / (double) r;
					// 骞虫柟鍜岄櫎浠ユ暟鎹�婚暱搴︼紝寰楀埌闊抽噺澶у皬銆傚彲浠ヨ幏鍙栫櫧鍣０鍊硷紝鐒跺悗瀵瑰疄闄呴噰鏍疯繘琛屾爣鍑嗗寲銆�
					// 濡傛灉鎯冲埄鐢ㄨ繖涓暟鍊艰繘琛屾搷浣滐紝寤鸿鐢� sendMessage 灏嗗叾鎶涘嚭锛屽湪 Handler 閲岃繘琛屽鐞嗐��
					// double dB = 10*Math.log10(v / (long) r);
					Message msg = new Message();
					msg.what = INT_UPDATE_RECORD;
					msg.obj = value;
					
//					Toast.makeText(mActivity, "value = "+value, Toast.LENGTH_SHORT).show();
					
					mhanHandler.sendMessage(msg);
					
				}
				if (!isClickMicro) {
					Thread.sleep(peridTime - fy);
				}
//				Tool.delay(peridTime - fy);
				if (activity.isFinishing()) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	 /** 
     * A simple class that draws waveform data received from a 
     * {@link Visualizer.OnDataCaptureListener#onWaveFormDataCapture } 
     */  
    class VisualizerView extends View  
    {  
        private byte[] mBytes;  
        private float[] mPoints;  
        private Rect mRect = new Rect();  
  
//        private Paint mForePaint = new Paint();  
        private int mSpectrumNum = 48;  
        private boolean mFirst = true;  
  
        public VisualizerView(Context context)  
        {  
            super(context);  
            init();  
        }  
  
        private void init()  
        {  
            mBytes = null;  
  
        }  
  
        public void updateVisualizer(byte[] fft)  
        {      
              
            byte[] model = new byte[fft.length / 2 + 1];  
  
            model[0] = (byte) Math.abs(fft[0]);  
            for (int i = 2, j = 1; j < mSpectrumNum;)  
            {  
                model[j] = (byte) Math.hypot(fft[i], fft[i + 1]);  
                i += 2;  
                j++;  
            }  
            mBytes = model;  
            
            
            if (mBytes == null)  
            {  
                return;  
            }  
  
            if (mPoints == null || mPoints.length < mBytes.length * 4)  
            {  
                mPoints = new float[mBytes.length * 4];  
            }  
  
            mRect.set(0, 0, getWidth(), getHeight());  
  
              
            //缁樺埗棰戣氨  
            final int baseX = mRect.width()/mSpectrumNum;  
            final int height = mRect.height();  
  
            for (int i = 0; i < mSpectrumNum ; i++)  
            {  
                if (mBytes[i] < 0)  
                {  
                    mBytes[i] = 127;  
                }  
                  
                final int xi = baseX*i + baseX/2;  
                  
                mPoints[i * 4] = xi;  
                mPoints[i * 4 + 1] = height;  
                  
                mPoints[i * 4 + 2] = xi;  
                mPoints[i * 4 + 3] = height - mBytes[i];  
                
//                double value = Math.pow((int)(1 - mPoints[i * 4 + 3]), 5);
                
//                chnnelValue = (int)(1 - mPoints[i * 4 + 2]);
                chnnelValue = (int)(1 - mPoints[i * 4 + 3]);
                
                chnnelValue = chnnelValue*chnnelValue;
                
                if (chnnelValue <= 1) {
                	chnnelValue = 1;
				}
                 

            }
        }           
    }  
}

