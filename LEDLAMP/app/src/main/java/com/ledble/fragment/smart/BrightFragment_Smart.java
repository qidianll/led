package com.ledble.fragment.smart;

import java.util.ArrayList;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import butterknife.Bind;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.Segment;
import com.common.view.SegmentedRadioGroup;
import com.common.view.TextViewBorder;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.constant.CommonConstant;
import com.ledble.view.MyColorPickerImageView;
import com.ledble.view.MyColorPickerImageView.OnTouchPixListener;
import com.ledlamp.R;


/**
 * 色温
 * 
 * @author ftl
 *
 */
public class BrightFragment_Smart extends LedBleFragment {
	
	@Bind(R.id.relativeLayoutTab1) View relativeLayoutTab1;
	@Bind(R.id.relativeLayoutTab2) View relativeLayoutTab2;
	@Bind(R.id.relativeLayoutTab3) View relativeLayoutTab3;

	@Bind(R.id.seekBarRedBrightNess) SeekBar seekBarRedBrightNess;
	@Bind(R.id.tvBrightness1) TextView tvBrightness1;
	@Bind(R.id.seekBarGreenBrightNess) SeekBar seekBarGreenBrightNess;
	@Bind(R.id.tvBrightness2) TextView tvBrightness2;
	@Bind(R.id.seekBarBlueBrightNess) SeekBar seekBarBlueBrightNess;
	@Bind(R.id.tvBrightness3) TextView tvBrightness3;
	@Bind(R.id.seekBarWhiteBrightNess) SeekBar seekBarWhiteBrightNess;
	@Bind(R.id.tvBrightness4) TextView tvBrightness4;
	@Bind(R.id.seekBarCrystalBrightNess) SeekBar seekBarCrystalBrightNess;
	@Bind(R.id.tvBrightness5) TextView tvBrightness5;
	@Bind(R.id.seekBarPinkBrightNess) SeekBar seekBarPinkBrightNess;
	@Bind(R.id.tvBrightness6) TextView tvBrightness6;
	
	@Bind(R.id.seekBarRedBrightNess2) SeekBar seekBarRedBrightNess2;
	@Bind(R.id.tvRedBrightNess2) TextView tvRedBrightNess2;
	@Bind(R.id.seekBarGreenBrightNess2) SeekBar seekBarGreenBrightNess2;
	@Bind(R.id.tvGreenBrightNess2) TextView tvGreenBrightNess2;
	@Bind(R.id.seekBarBlueBrightNess2) SeekBar seekBarBlueBrightNess2;
	@Bind(R.id.tvBlueBrightNess2) TextView tvBlueBrightNess2;
	@Bind(R.id.seekBarWhiteBrightNess2) SeekBar seekBarWhiteBrightNess2;
	@Bind(R.id.tvWhiteBrightNess2) TextView tvWhiteBrightNess2;
	@Bind(R.id.seekBarCrystalBrightNess2) SeekBar seekBarCrystalBrightNess2;
	@Bind(R.id.tvCrystalBrightNess2) TextView tvCrystalBrightNess2;
	@Bind(R.id.seekBarPinkBrightNess2) SeekBar seekBarPinkBrightNess2;
	@Bind(R.id.tvPinkBrightNess2) TextView tvPinkBrightNess2;
	
	@Bind(R.id.seekBarRedBrightNess3) SeekBar seekBarRedBrightNess3;
	@Bind(R.id.tvRedBrightNess3) TextView tvRedBrightNess3;
	@Bind(R.id.seekBarGreenBrightNess3) SeekBar seekBarGreenBrightNess3;
	@Bind(R.id.tvGreenBrightNess3) TextView tvGreenBrightNess3;
	@Bind(R.id.seekBarBlueBrightNess3) SeekBar seekBarBlueBrightNess3;
	@Bind(R.id.tvBlueBrightNess3) TextView tvBlueBrightNess3;
	@Bind(R.id.seekBarWhiteBrightNess3) SeekBar seekBarWhiteBrightNess3;
	@Bind(R.id.tvWhiteBrightNess3) TextView tvWhiteBrightNess3;
	@Bind(R.id.seekBarCrystalBrightNess3) SeekBar seekBarCrystalBrightNess3;
	@Bind(R.id.tvCrystalBrightNess3) TextView tvCrystalBrightNess3;
	@Bind(R.id.seekBarPinkBrightNess3) SeekBar seekBarPinkBrightNess3;
	@Bind(R.id.tvPinkBrightNess3) TextView tvPinkBrightNess3;


	private View mContentView;
	private MainActivity_BLE mActivity;
	
	private boolean cansend = false;
	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_brightness_smart, container, false);
		
		return mContentView;
	}

	@Override
	public void initData() {
		mActivity = (MainActivity_BLE) getActivity();
//		refresh();
		setActive();
	}

	@Override
	public void initView() {

	}
	
	/**
	 * 
	 */
	protected void refresh() {
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				mHandler.postDelayed(this, 20);// 打开定时器，执行操作
				cansend = true;					
			}
		};
		mHandler.postDelayed(mRunnable, 10);// 打开定时器，执行操作

	}
	
	public void setActive() {
		
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");	
		
//		Toast.makeText(mActivity, ""+datas[0], Toast.LENGTH_SHORT).show();
		
		if (datas[0].equalsIgnoreCase(RGB)) {
			this.relativeLayoutTab1.setVisibility(View.VISIBLE);
			this.relativeLayoutTab2.setVisibility(View.GONE);
			this.relativeLayoutTab3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBW)) {
			this.relativeLayoutTab1.setVisibility(View.GONE);
			this.relativeLayoutTab2.setVisibility(View.VISIBLE);
			this.relativeLayoutTab3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			this.relativeLayoutTab1.setVisibility(View.GONE);
			this.relativeLayoutTab2.setVisibility(View.GONE);
			this.relativeLayoutTab3.setVisibility(View.VISIBLE);
		}
		initViews();
	}

	@Override
	public void initEvent() {

	}
	
	public void initViews() {
		
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");	
		
		if (datas[0].equalsIgnoreCase(RGB)) {

			this.seekBarRedBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
					
					tvBrightness1.setText(Integer.toString(progress));	
					setSmartBrightness(progress, 0);
				}
			});

			this.seekBarGreenBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvBrightness2.setText(Integer.toString(progress));
					setSmartBrightness(progress, 1);
				}
			});

			this.seekBarBlueBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvBrightness3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 2);
				}
			});


		}else if (datas[0].equalsIgnoreCase(RGBW)) {

			this.seekBarRedBrightNess2.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvRedBrightNess2.setText(Integer.toString(progress));
					setSmartBrightness(progress, 0);
				}
			});
			
			this.seekBarGreenBrightNess2.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvGreenBrightNess2.setText(Integer.toString(progress));
					setSmartBrightness(progress, 1);
				}
			});
			
			this.seekBarBlueBrightNess2.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvBlueBrightNess2.setText(Integer.toString(progress));
					setSmartBrightness(progress, 2);
				}
			});
			
			this.seekBarWhiteBrightNess2.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvWhiteBrightNess2.setText(Integer.toString(progress));
					setSmartBrightness(progress, 3);
				}
			});

			
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			
			this.seekBarRedBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvRedBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 0);
				}
			});
			
			this.seekBarGreenBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvGreenBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 1);
				}
			});
			
			this.seekBarBlueBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvBlueBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 2);
				}
			});
			
			this.seekBarWhiteBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvWhiteBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 3);
				}
			});
			
			this.seekBarCrystalBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvCrystalBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 4);
				}
			});
			
			this.seekBarPinkBrightNess3.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
				@Override
				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

					tvPinkBrightNess3.setText(Integer.toString(progress));
					setSmartBrightness(progress, 5);
				}
			});
			
		}

	}

	private void setSmartBrightness(int progress, int mode) {
//		if (cansend) {
			mActivity.setSmartBrightness(mode, progress);
			cansend = false;
//		}		
	}

}
