package com.ledble.fragment.wifi;

import java.util.ArrayList;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import butterknife.Bind;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.Tool;
import com.common.view.Segment;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.wifi.MainActivity_WiFi;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.view.MyColorPickerImageView;
import com.ledble.view.MyColorPickerImageView.OnTouchPixListener;

/**
 * 色温
 * 
 * @author ftl
 *
 */
public class ModeFragment_WiFi extends LedBleFragment {
	
	@Bind(R.id.listViewModel) ListView listViewModel;
	@Bind(R.id.textViewCurretModel) TextView textViewCurretModel;
	
	@Bind(R.id.imageViewPlayMode) Button buttonPlay;
	@Bind(R.id.imageViewOnOff) Button imageViewOnOff;
	
	@Bind(R.id.seekBarMode) SeekBar seekBarMode;
	@Bind(R.id.textViewMode) TextView textViewMode;
	@Bind(R.id.seekBarSpeed) SeekBar seekBarSpeedBar;
	@Bind(R.id.textViewSpeed) TextView textViewSpeed;
	@Bind(R.id.seekBarBrightNess) SeekBar seekBarBrightness;
	@Bind(R.id.textViewBrightNess) TextView textViewBrightness;
	
	private ModelAdapter maAdapter;

	private View mContentView;
	private MainActivity_WiFi mActivity;
	
	private int offOnBtnState;
	private int playBtnState = 1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_mode_wifi, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		mActivity = (MainActivity_WiFi) getActivity();
	}

	@Override
	public void initView() {

		final int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff, 0xff00ffff, 0xffff00ff };

		this.listViewModel.setAdapter(buildModel());
		this.listViewModel.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				mActivity.pauseMusicAndVolum(true);
				maAdapter.setIndex(position);
				maAdapter.notifyDataSetChanged();
				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
				String value = abean.getValue();
				textViewCurretModel
						.setText(mActivity.getResources().getString(R.string.current_mode_format, abean.getLabel()));
				textViewCurretModel.setTag(value);
				mActivity.setRegMode(position+1);

			}
		});
		
		// 开始/暂停
		buttonPlay.bringToFront();
		buttonPlay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (playBtnState == 0) {
					buttonPlay.setBackgroundResource(R.drawable.bg_play_pause);
					playBtnState = 1;
//					mActivity.setSPIPause(playBtnState);
				} else {
					buttonPlay.setBackgroundResource(R.drawable.bg_play);
					playBtnState = 0;
//					mActivity.setSPIPause(playBtnState);
				}
			}
		});
		
		imageViewOnOff.bringToFront();
		imageViewOnOff.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (offOnBtnState == 0) {
					imageViewOnOff.setBackgroundResource(R.drawable.on_btn);
					offOnBtnState = 1;
					mActivity.open();

				} else {
					imageViewOnOff.setBackgroundResource(R.drawable.off_btn);
					offOnBtnState = 0;
					mActivity.close();
				}
			}
		});
		
		
		this.seekBarMode.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 209) {
					mActivity.pauseMusicAndVolum(true);
//					if (progress == 0) {
//						mActivity.setRegMode(255);
//					}else  {
					AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
					String style = abean.getValue();
					mActivity.setRegMode(Integer.parseInt(style));
//					}			
					
					textViewMode.setText(mActivity.getResources().getString(R.string.mode_set, progress+1));	
					
					listViewModel.setSelection(progress);
					maAdapter.notifyDataSetInvalidated();//通知adapter数据有变化
					
//					AdapterBean abean = ((AdapterBean) maAdapter.getItem(progress));
					textViewCurretModel.setText(mActivity.getResources().getString(R.string.current_mode_format, abean.getLabel()));	
				}
			}
		});
		
		this.seekBarSpeedBar.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 100) {
					mActivity.pauseMusicAndVolum(true);
					mActivity.setSpeed(progress);
					textViewSpeed.setText(mActivity.getResources().getString(R.string.speed_set, progress));

				}
			}
		});
		
		this.seekBarBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress > 0 && progress <= 100) {
					mActivity.pauseMusicAndVolum(true);
					textViewBrightness.setText(mActivity.getResources().getString(R.string.brightness_set, progress));
					mActivity.setBrightNess(progress);

				}
			}
		});
	}

	@Override
	public void initEvent() {

	}

	
	private ModelAdapter buildModel() {
		String[] array = mActivity.getResources().getStringArray(R.array.ble_mode);
//		String[] ary = mActivity.getResources().getStringArray(R.array.timer_mode);
				
		ArrayList<String> totalArray = new ArrayList<String>();//必须初始化 
		for (String lable : array) {	
			totalArray.add(lable);
		}
	
		for (int i = 30; i <= 210; i++) 
		{ 
			totalArray.add("MODE"+i+","+i);
		} 
		
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : totalArray) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(mActivity, abs);
		return maAdapter;
	}

}
