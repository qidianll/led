package com.ledble.fragment.smart;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;
import butterknife.Bind;

import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.base.LedBleFragment;
import com.ledlamp.R;

/**
 * 彩色
 * 
 * @author ftl
 *
 */
public class SceneFragment_Smart extends LedBleFragment {
	
	@Bind(R.id.relativeTab2) View relativeTab2;// 模式
	@Bind(R.id.imageViewSceneSunrise) ImageView imageViewSceneSunrise;
	@Bind(R.id.imageViewSceneSunset) ImageView imageViewSceneSunset;
	@Bind(R.id.imageViewSceneColorful) ImageView imageViewSceneColorful;
	@Bind(R.id.imageViewSceneAfternoontea) ImageView imageViewSceneAfternoontea;
	@Bind(R.id.imageViewSceneDrivemidge) ImageView imageViewSceneDrivemidge;
	@Bind(R.id.imageViewSceneYoga) ImageView imageViewSceneYoga;
	@Bind(R.id.imageViewSceneParty) ImageView imageViewSceneParty;
	@Bind(R.id.imageViewSceneTropical) ImageView imageViewSceneTropical;
	@Bind(R.id.imageViewSceneSea) ImageView imageViewSceneSea;
	@Bind(R.id.imageViewSceneReading) ImageView imageViewSceneReading;
	@Bind(R.id.imageViewSceneCandlelightdinner) ImageView imageViewSceneCandlelightdinner;

	private MainActivity_BLE mActivity;	
	private int style = 0;


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_scene_smart, container, false);
	}

	@Override
	public void initData() {
		
		mActivity = (MainActivity_BLE) getActivity();
			
		// --------------- 情景模式 -------------------
		// 日出模式
		imageViewSceneSunrise.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(255, 100, 2);
				mActivity.setRegMode(1);
				Toast.makeText(mActivity, getString(R.string.sunrise_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 日落模式
		imageViewSceneSunset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				MainActivity.getMainActivity().setRgb(255, 17, 13);
				mActivity.setRegMode(2);
				Toast.makeText(mActivity, getString(R.string.sunset_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 多彩模式
		imageViewSceneColorful.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(214, 177, 255);
				mActivity.setRegMode(3);
				Toast.makeText(mActivity, getString(R.string.lightning_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 下午茶模式
		imageViewSceneAfternoontea.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(210, 115, 255);
				mActivity.setRegMode(4);
				Toast.makeText(mActivity, getString(R.string.thunderstorm_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 驱蚊模式
		imageViewSceneDrivemidge.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(255, 80, 12);
				mActivity.setRegMode(5);
				Toast.makeText(mActivity, getString(R.string.sunshine_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 瑜伽模式
		imageViewSceneYoga.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(46, 230, 255);
				mActivity.setRegMode(6);
				Toast.makeText(mActivity, getString(R.string.marine_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 聚会模式
		imageViewSceneParty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(220, 14, 255);
				mActivity.setRegMode(7);
				Toast.makeText(mActivity, getString(R.string.coral_artistic_conception), Toast.LENGTH_SHORT).show();
			}
		});

		// 热带雨林
		imageViewSceneTropical.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(55, 255, 27);
				mActivity.setRegMode(8);
				Toast.makeText(mActivity, getString(R.string.aquatic_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 海边模式
		imageViewSceneSea.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(3, 13, 255);
				mActivity.setRegMode(9);
				Toast.makeText(mActivity, getString(R.string.sleep_mode), Toast.LENGTH_SHORT).show();
			}	
		});

		// 阅读模式
		imageViewSceneReading.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(255, 214, 147);
				mActivity.setRegMode(10);
				Toast.makeText(mActivity, getString(R.string.deep_sea_mode), Toast.LENGTH_SHORT).show();
			}
		});

		// 烛光晚餐
		imageViewSceneCandlelightdinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
//				MainActivity.getMainActivity().setRgb(252, 97, 2);
				mActivity.setRegMode(11);
				Toast.makeText(mActivity, getString(R.string.colorful_mode), Toast.LENGTH_SHORT).show();
			}
		});

	}

	@Override
	public void initView() {
//		tgbtn.setChecked(mActivity.isLightOpen);
//		seekBarBrightness.setProgress(mActivity.brightness);// 设置亮度
//		seekBarSpeedBar.setProgress(mActivity.speed);
	}

	@Override
	public void initEvent() {

	}

}
