package com.ledble.fragment.stage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import butterknife.Bind;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.ledble.activity.ble.MainActivity_STAGE;
import com.ledble.activity.wifi.MainActivity_WiFi;
import com.ledble.base.LedBleFragment;
import com.ledlamp.R;

/**
 * 色温
 * 
 * @author ftl
 *
 */
public class BrightFragment_Stage extends LedBleFragment {
	
//	@Bind(R.id.toggleButtonOnoff) ToggleButton toggleButtonOnoff;
	
	@Bind(R.id.linearLayoutTab) LinearLayout linearLayoutTab;

	@Bind(R.id.seekBarRedBrightNess) SeekBar seekBarRedBrightNess;
	@Bind(R.id.tvBrightness1) TextView tvBrightness1;

	@Bind(R.id.seekBarGreenBrightNess) SeekBar seekBarGreenBrightNess;
	@Bind(R.id.tvBrightness2) TextView tvBrightness2;

	@Bind(R.id.seekBarBlueBrightNess) SeekBar seekBarBlueBrightNess;
	@Bind(R.id.tvBrightness3) TextView tvBrightness3;

	@Bind(R.id.seekBarWhiteBrightNess) SeekBar seekBarWhiteBrightNess;
	@Bind(R.id.tvBrightness4) TextView tvBrightness4;
	
	@Bind(R.id.seekBarYellowBrightNess) SeekBar seekBarYellowBrightNess;
	@Bind(R.id.tvBrightness5) TextView tvBrightness5;

	@Bind(R.id.seekBarPinkBrightNess) SeekBar seekBarPinkBrightNess;
	@Bind(R.id.tvBrightness6) TextView tvBrightness6;
	
	@Bind(R.id.seekBarCrystalBrightNess) SeekBar seekBarCrystalBrightNess;
	@Bind(R.id.tvBrightness7) TextView tvBrightness7;
	
	@Bind(R.id.seekBarCH1BrightNess) SeekBar seekBarCH1BrightNess;
	@Bind(R.id.tvBrightness8) TextView tvBrightness8;
	
	@Bind(R.id.seekBarCH2BrightNess) SeekBar seekBarCH2BrightNess;
	@Bind(R.id.tvBrightness9) TextView tvBrightness9;
	
	@Bind(R.id.seekBarCH3BrightNess) SeekBar seekBarCH3BrightNess;
	@Bind(R.id.tvBrightness10) TextView tvBrightness10;
	
	@Bind(R.id.seekBarCH4BrightNess) SeekBar seekBarCH4BrightNess;
	@Bind(R.id.tvBrightness11) TextView tvBrightness11;
	
	@Bind(R.id.seekBarCH5BrightNess) SeekBar seekBarCH5BrightNess;
	@Bind(R.id.tvBrightness12) TextView tvBrightness12;
	
	@Bind(R.id.seekBarCH6BrightNess) SeekBar seekBarCH6BrightNess;
	@Bind(R.id.tvBrightness13) TextView tvBrightness13;
	
	@Bind(R.id.seekBarCH7BrightNess) SeekBar seekBarCH7BrightNess;
	@Bind(R.id.tvBrightness14) TextView tvBrightness14;
	
	@Bind(R.id.seekBarCH8BrightNess) SeekBar seekBarCH8BrightNess;
	@Bind(R.id.tvBrightness15) TextView tvBrightness15;
	
	@Bind(R.id.seekBarCH9BrightNess) SeekBar seekBarCH9BrightNess;
	@Bind(R.id.tvBrightness16) TextView tvBrightness16;

//	private ToggleButton tgbtn;

	private View mContentView;
	private MainActivity_STAGE mActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_brightness_wifi, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		mActivity = (MainActivity_STAGE) getActivity();
	}

	@Override
	public void initView() {

		final int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff, 0xff00ffff, 0xffff00ff };
		
//		toggleButtonOnoff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (isChecked) {
//					mActivity.open();
//				} else {
//					mActivity.close();
//				}
//			}
//
//		});

		this.seekBarRedBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness1.setText(Integer.toString(progress));
				setSmartBrightness(progress, 1);
//				updateColor(colors[0]);
//				tgbtn.setBackgroundColor(colors[0]);
				
			}
		});
		this.seekBarGreenBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness2.setText(Integer.toString(progress));
				setSmartBrightness(progress, 2);
//				updateColor(colors[1]);
//				tgbtn.setBackgroundColor(colors[1]);
			}
		});
		this.seekBarBlueBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness3.setText(Integer.toString(progress));
				setSmartBrightness(progress, 3);
//				updateColor(colors[2]);
//				tgbtn.setBackgroundColor(colors[2]);
			}
		});
		this.seekBarWhiteBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness4.setText(Integer.toString(progress));
				setSmartBrightness(progress, 4);
//				updateColor(colors[3]);
//				tgbtn.setBackgroundColor(colors[3]);
			}
		});
		this.seekBarYellowBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness5.setText(Integer.toString(progress));
				setSmartBrightness(progress, 5);
//				updateColor(colors[3]);
//				tgbtn.setBackgroundColor(colors[4]);
			}
		});
		this.seekBarPinkBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness6.setText(Integer.toString(progress));
				setSmartBrightness(progress, 6);
//				updateColor(colors[5]);
//				tgbtn.setBackgroundColor(colors[5]);
			}
		});
		this.seekBarCrystalBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness7.setText(Integer.toString(progress));
				setSmartBrightness(progress, 7);
//				updateColor(colors[4]);
//				tgbtn.setBackgroundColor(colors[6]);
			}
		});
		this.seekBarCH1BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness8.setText(Integer.toString(progress));
				setSmartBrightness(progress, 8);
//				updateColor(colors[4]);
//				tgbtn.setBackgroundColor(colors[7]);
			}
		});
		this.seekBarCH2BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness9.setText(Integer.toString(progress));
				setSmartBrightness(progress, 9);
//				updateColor(colors[4]);
//				tgbtn.setBackgroundColor(colors[8]);
			}
		});
		this.seekBarCH3BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness10.setText(Integer.toString(progress));
				setSmartBrightness(progress, 10);
			}
		});
		this.seekBarCH4BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness11.setText(Integer.toString(progress));
				setSmartBrightness(progress, 11);
			}
		});
		this.seekBarCH5BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness12.setText(Integer.toString(progress));
				setSmartBrightness(progress, 12);
			}
		});
		this.seekBarCH6BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness13.setText(Integer.toString(progress));
				setSmartBrightness(progress, 13);
			}
		});
		this.seekBarCH7BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness14.setText(Integer.toString(progress));
				setSmartBrightness(progress, 14);
			}
		});
		this.seekBarCH8BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness15.setText(Integer.toString(progress));
				setSmartBrightness(progress, 15);
			}
		});
		this.seekBarCH9BrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				tvBrightness16.setText(Integer.toString(progress));
				setSmartBrightness(progress, 16);
			}
		});

	}

	@Override
	public void initEvent() {

	}

	public void updateColor(int color) {
		// tgbtn.setChecked(mActivity.isLightOpen);
		// seekBarRedBrightNess.setProgress(mActivity.brightness);
		// seekBar2.setProgress(mActivity.brightness);
		int rgb[] = Tool.getRGB(color);
		mActivity.setRgb(rgb[0], rgb[1], rgb[2]);
	}

	private void setSmartBrightness(int progress, int mode) {
		mActivity.setSmartBrightness(progress, mode);

	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		for (int i = 1; i <= 10; i++) {
			final SeekBar tv = (SeekBar) linearLayoutTab.findViewWithTag((String.valueOf("SeekBar") + i));
			
			tv.setBackgroundResource(R.drawable.slider_bg_gray);		
			tv.setBackgroundResource(R.drawable.slider_bg_gray);
			tv.setBackgroundResource(R.drawable.slider_bg_gray);
			tv.setBackgroundResource(R.drawable.slider_bg_gray);
			tv.setBackgroundResource(R.drawable.slider_bg_gray);
			tv.setBackgroundResource(R.drawable.slider_bg_gray);
			
		}
		
		int rValue = SharePersistent.getInt(getActivity(), "CH_R_STAGE");
		int gValue = SharePersistent.getInt(getActivity(), "CH_G_STAGE");
		int bValue = SharePersistent.getInt(getActivity(), "CH_B_STAGE");
		int wValue = SharePersistent.getInt(getActivity(), "CH_W_STAGE");
		int yValue = SharePersistent.getInt(getActivity(), "CH_Y_STAGE");
		int pValue = SharePersistent.getInt(getActivity(), "CH_P_STAGE");
		
		for (int i = 1; i <= 10; i++) {
			final SeekBar tv = (SeekBar) linearLayoutTab.findViewWithTag((String.valueOf("SeekBar") + i));
			
			if (rValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_red);
			}
			if (gValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_green);
			}
			if (bValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_blue);
			}
			if (wValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_white);
			}
			if (yValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_crystal);
			}
			if (pValue == i) {
				tv.setBackgroundResource(R.drawable.slider_bg_pink);
			}
		
		}	
		
	}

}
