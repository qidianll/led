package com.ledble.fragment.wifi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import butterknife.Bind;

import com.common.uitl.ListUtiles;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.uitl.StringUtils;
import com.common.uitl.Tool;
import com.ledble.activity.wifi.MainActivity_WiFi;
//import com.ledble.view.CustomAlertDialog;
import com.ledble.base.LedBleFragment;
import com.ledble.db.wifi.GroupDeviceDaoWiFi;
import com.ledble.db.wifi.GroupDeviceWiFi;
import com.ledble.db.wifi.GroupWiFi;
import com.ledble.net.NetConnectBle;
import com.ledble.view.ActionSheet;
import com.ledble.view.ActionSheet.ActionSheetListener;
import com.ledble.view.ActionSheet.Item;
import com.ledble.view.GroupView;
import com.ledble.view.GroupViewWiFi;
import com.ledble.view.SlideSwitch;
import com.ledble.view.SlideSwitch.SlideListener;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.ledlamp.R;
//import com.umeng.analytics.MobclickAgent;


/**
 * 定时
 * @author ftl
 *
 */
public class TimerFragment_WiFi extends LedBleFragment implements ActionSheetListener  {
	
	@Bind(R.id.linearLayoutDefineGroups) LinearLayout linearGroups;
	
	private MainActivity_WiFi mActivity;
	

	private boolean isAllOn = true;
	private int onOffStatus = 1;
	private int switchStatus = 1;
	private Map<String, SlideSwitch> map = new HashMap<>();
	private ArrayList<GroupViewWiFi> arrayListGroupViews;
	
	private String KEY_ONOFF = "onOffStatus";
	private String KEY_SWITCH = "switchStatus";
	
	private int hour /*= 12*/;
	private int minute /*= 30*/;
	private int model /*= 10*/;
	
	private int MonValue ;
	private int TueValue ;
	private int WedValue ;
	private int ThuValue ;
	private int FriValue ;
	private int SatValue ;
	private int SunValue ;
	
		
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_timer_wifi, container, false);
	}

	@Override
	public void initData() {
		
		Time t=new Time(); // 
		t.setToNow();
		hour = t.hour;    // 0-23
		minute = t.minute;    // 0-23
		
		mActivity = (MainActivity_WiFi) getActivity();	
		
		arrayListGroupViews = new ArrayList<GroupViewWiFi>();

		initGroup(isAllOn);
	}

	@Override
	public void initView() {
		
		
	}

	@Override
	public void initEvent() {
		
	}
	
	/**
	 * 首次初始化组视图
	 */
	private void initGroup(boolean isAllOn) {
		GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
		ArrayList<GroupWiFi> groups = gDao.getAllgroup();
		
//		for (Group group : groups) {			
//			String weekStr = SharePersistent.getWeekData(mActivity, group.getGroupName());
//			boolean switchStatus = SharePersistent.getBoolean(mActivity, group.getGroupName()+"switchStatus");
//			int onOffStatus = SharePersistent.getInt(mActivity, group.getGroupName()+"onOffStatus");
//			addGroupViewFromInit(group.getGroupName(), group.getIsOn(), isAllOn, weekStr, switchStatus, onOffStatus);
//		}
		
		if (groups.size() > 0 ) {
			for (int i = 1; i <= groups.size(); i++) {
				GroupWiFi group = groups.get(i-1);		
				String weekStr = SharePersistent.getWeekData(mActivity, group.getGroupName());
				boolean switchStatus = SharePersistent.getBoolean(mActivity, group.getGroupName()+"switchStatus");
//				int onOffStatus = SharePersistent.getInt(mActivity, group.getGroupName()+"onOffStatus");
				addGroupViewFromInit(group.getGroupName(), group.getIsOn(), isAllOn, weekStr, switchStatus, i);
			}
		}
		
	}
	
	private void addGroupViewFromInit(final String groupname, final String ison, final boolean isAllOn, final String weekStr, final boolean switchSta, final int tongdao) {
		// 添加组视图
		final GroupViewWiFi groupView = new GroupViewWiFi(mActivity, groupname, isAllOn);

		GroupDeviceDaoWiFi groupDeviceDao = new GroupDeviceDaoWiFi(mActivity);
		final ArrayList<GroupDeviceWiFi> groupDevices = groupDeviceDao.getDevicesByGroup(groupname);// 相同组的所有设备
		if (!ListUtiles.isEmpty(groupDevices)) {
			groupView.setGroupDevices(groupDevices);
		}
		groupView.setWeekName(weekStr);
//		groupView.setModeName(weekStr);
		final ToggleButton imageViewLight = groupView.getImageViewLight();
		final SlideSwitch slideSwitch = groupView.getSlideSwitch();
		map.put(groupname, slideSwitch);
		slideSwitch.setChecked(switchSta);
		slideSwitch.setSlideListener(new SlideListener() {

			@Override
			public void open() {
//				changeStatus(groupname);
				switchStatus = 1;
				mActivity.turnOnOffTimerOn(tongdao, getHourData(groupname), getMinuteData(groupname), getModeData(groupname));
				SharePersistent.saveBoolean(mActivity, groupname+KEY_SWITCH, true);
			}

			@Override
			public void close() {
				switchStatus = 0;
				mActivity.turnOnOffTimerOff(tongdao, getHourData(groupname), getMinuteData(groupname), 0);
				SharePersistent.saveBoolean(mActivity, groupname+KEY_SWITCH, false);
			}
			
		});
		
//		if (onOff == 1) {
//			imageViewLight.setChecked(true);
//			imageViewLight.setBackgroundResource(R.drawable.light_on);
//		}else if (onOff == 0) {
//			imageViewLight.setChecked(false);
//			imageViewLight.setBackgroundResource(R.drawable.light_off);
//		} 
//		imageViewLight.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (isChecked) {
//					SharePersistent.saveInt(mActivity, groupname+KEY_ONOFF, 1);
//					imageViewLight.setBackgroundResource(R.drawable.light_on);
//				} else {
//					SharePersistent.saveInt(mActivity, groupname+KEY_ONOFF, 0);
//					imageViewLight.setBackgroundResource(R.drawable.light_off);
//				}
//			}
//		});


		groupView.getGroupView().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				initWeekData(groupname);
//				showCustomMessage(getResources().getString(R.string.editTimer), false, groupname);
				
//				if (groupView.isTurnOn()) {
//					if (groupView.getConnect() > 0) {
//						// gotoMain(groupName, groupView);
//					} else {
//						Tool.ToastShow(mActivity, R.string.edit_group_please);
//						showActionSheet(groupname);
//					}
//				} else {
//					showActionSheet(groupname);
//				}
			}
		});

		linearGroups.addView(groupView.getGroupView());
		groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				showDeleteDialog(groupname);
				return true;
			}
		});

		if ("y".equalsIgnoreCase(ison) && isAllOn) {
			groupView.turnOn();
		} else {
			groupView.turnOff();
		}
		arrayListGroupViews.add(groupView);
	}
	
	
	public void addTimer() {
//		Toast.makeText(mActivity, "添加定时", Toast.LENGTH_SHORT).show();
		
		GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
		ArrayList<GroupWiFi> groups = gDao.getAllgroup();
		
		if (groups.size() > 0 && groups.size() == 15) {
			Toast.makeText(mActivity, getString(R.string.supported), Toast.LENGTH_SHORT).show();
			return;
		}
		
		ResetWeekData();
		showCustomMessage(getString(R.string.addTimer), true, "");
//		showCustomMessage("Add Timer", true, "");
		
	}
	
	
	private void addGroupByName(String timeStr, String weekStr) {
		try {
			// 添加组数据库
			GroupDeviceDaoWiFi groupDeviceDao = new GroupDeviceDaoWiFi(mActivity);
			groupDeviceDao.addGroup(timeStr);
			addGroupView(timeStr, weekStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void addGroupView(final String timeStr, String weekStr) {
		// 添加组视图
		final GroupViewWiFi groupView = new GroupViewWiFi(mActivity, timeStr, isAllOn);
		final SlideSwitch slideSwitch = groupView.getSlideSwitch();
		final ToggleButton imageViewLight = groupView.getImageViewLight();
		groupView.setWeekName(weekStr);
		linearGroups.addView(groupView.getGroupView());
		map.put(timeStr, slideSwitch);	
		
		GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
		final ArrayList<GroupWiFi> groups = gDao.getAllgroup();
		slideSwitch.setSlideListener(new SlideListener() {			
			@Override
			public void open() {
				switchStatus = 1;
//				Toast.makeText(mActivity, ""+groups.size(), Toast.LENGTH_SHORT).show();
				for (int i = 0; i < groups.size(); i++) {
					GroupWiFi group = groups.get(i);
					if (group.getGroupName().equalsIgnoreCase(timeStr)) {
//						Toast.makeText(mActivity, ""+groups, Toast.LENGTH_SHORT).show();
						mActivity.turnOnOffTimerOn(i+1, getHourData(timeStr), getMinuteData(timeStr), getModeData(timeStr));
					}
				}
				
				SharePersistent.saveBoolean(mActivity, timeStr+KEY_SWITCH, true);
			}

			@Override
			public void close() {
				switchStatus = 0;
//				Toast.makeText(mActivity, ""+groups.size(), Toast.LENGTH_SHORT).show();
				for (int i = 0; i < groups.size(); i++) {
					GroupWiFi group = groups.get(i);
					if (group.getGroupName().equalsIgnoreCase(timeStr)) {
						mActivity.turnOnOffTimerOff(i+1, getHourData(timeStr), getMinuteData(timeStr), 0);
					}
				}				
				
				SharePersistent.saveBoolean(mActivity, timeStr+KEY_SWITCH, false);
			}
		});
		
		imageViewLight.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					SharePersistent.saveInt(mActivity, timeStr+KEY_ONOFF, 1);
					imageViewLight.setBackgroundResource(R.drawable.light_on);
				} else {
					SharePersistent.saveInt(mActivity, timeStr+KEY_ONOFF, 0);
					imageViewLight.setBackgroundResource(R.drawable.light_off);
				}
			}
		});
		
		groupView.getGroupView().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

//				showCustomMessage(getResources().getString(R.string.editTimer), false, timeStr);
				
//				if (groupView.isTurnOn()) {
//					if (groupView.getConnect() > 0) {
//						// gotoMain(groupName, groupView);
//					} else {
//						Tool.ToastShow(mActivity, R.string.edit_group_please);
//						showActionSheet(timeStr);
//					}
//				} else {
//					showActionSheet(timeStr);
//				}
			}
		});
		groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				showDeleteDialog(timeStr);
				return true;
			}
		});

		arrayListGroupViews.add(groupView);
	}
	
	
	public void showActionSheet(String groupname) {

		NetConnectBle.getInstanceByGroup(groupname);

		if (groupname.equalsIgnoreCase("")) {
			Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct,
					R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
			Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white,
					getResources().getString(R.string.text_cancel));

			ActionSheet.createBuilder(mActivity, mActivity.getFragmentManager()).setCancelItem(cancelItem).setmOtherItems(item1)
					.setGroupName(groupname).setCancelableOnTouchOutside(true).setListener(this).show();
		} else {
			Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct,
					R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
			Item item2 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct,
					R.color.colorPrimary, R.color.white, getResources().getString(R.string.add_device));
			Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white,
					getResources().getString(R.string.text_cancel));

			ActionSheet.createBuilder(mActivity, mActivity.getFragmentManager()).setCancelItem(cancelItem)
					.setmOtherItems(item1, item2).setGroupName(groupname).setCancelableOnTouchOutside(true)
					.setListener(this).show();
		}
	}
	
	
	private void showDeleteDialog(final String groupName) {

		Dialog alertDialog = new AlertDialog.Builder(mActivity).setTitle(getResources().getString(R.string.tips))
				.setMessage(getResources().getString(R.string.delete_group, groupName))
				.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						try {
							GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
							gDao.deleteGroup(groupName);							
//							gDao.delteByGroup(groupName);
							linearGroups.removeView(linearGroups.findViewWithTag(groupName));
							map.remove(groupName);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}

				}).create();

		alertDialog.show();

	}
	
	
	
	private void showCustomMessage(String pTitle, final boolean isAdd, final String timeStr) {
		
		final Dialog lDialog = new Dialog(mActivity,
				android.R.style.Theme_Translucent_NoTitleBar);
		lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		lDialog.setContentView(R.layout.dialogview_wifi_timer);
		((TextView) lDialog.findViewById(R.id.dialog_title)).setText(pTitle);
		
		WheelView listViewH = (WheelView) lDialog.findViewById(R.id.listViewH);
		String[] modelH = new String[24];
		for (int i = 0; i < 24; i++) {
			modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		WheelModelAdapter wheelAdapterH = new WheelModelAdapter(mActivity, modelH);
		listViewH.setViewAdapter(wheelAdapterH);
		listViewH.setCurrentItem(hour);
		listViewH.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				hour = newValue;
			}
		});

		WheelView listViewM = (WheelView) lDialog.findViewById(R.id.listViewM);
		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		WheelModelAdapter wheelAdapterM = new WheelModelAdapter(mActivity, modelM);
		listViewM.setViewAdapter(wheelAdapterM);
		listViewM.setCurrentItem(minute);
		// this.listViewModel.setCyclic(true);
		listViewM.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				minute = newValue;
			}
		});
		
//		String[] timerSwitch = getResources().getStringArray(R.array.timer_mode);
//		String[] timerSwitch = getResources().getStringArray(R.array.rgb_mode);
//		final String[] switchStrings = new String[timerSwitch.length];
//		for (int i = 0; i < timerSwitch.length; i++) {
//			String[] datas = timerSwitch[i].split(",");
//			switchStrings[i] = datas[0];
//		}
//		
		String[] array = mActivity.getResources().getStringArray(R.array.ble_mode);
//		String[] ary = mActivity.getResources().getStringArray(R.array.timer_mode);
				
		ArrayList<String> totalArray = new ArrayList<String>();//必须初始化 
		for (String lable : array) {	
			totalArray.add(lable);
		}
	
		for (int i = 30; i <= 211; i++) 
		{ 
			totalArray.add("MODE"+i+","+i);
		} 
		
		final String[] switchStrings = new String[totalArray.size()];
		for (int i = 0; i < totalArray.size(); i++) {
			String[] datas = totalArray.get(i).split(",");
			switchStrings[i] = datas[0];
		}
		
		WheelView listViewSwitch = (WheelView) lDialog.findViewById(R.id.listViewStatus);
		WheelModelAdapter wheelAdapterSwitch = new WheelModelAdapter(mActivity, switchStrings);
		listViewSwitch.setViewAdapter(wheelAdapterSwitch);
		listViewSwitch.setCurrentItem(0);
		listViewSwitch.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				model = newValue;
			}
		});
		
		String[] timerModel = getResources().getStringArray(R.array.week_model);

		final String[] modelStrings = new String[timerModel.length];
		for (int i = 0; i < timerModel.length; i++) {
			String[] datas = timerModel[i].split(",");
			modelStrings[i] = datas[0];
		}
		WheelView listViewMon = (WheelView) lDialog.findViewById(R.id.listViewMon);
		WheelModelAdapter wheelAdapterMon = new WheelModelAdapter(mActivity, modelStrings);
		listViewMon.setViewAdapter(wheelAdapterMon);
		listViewMon.setCurrentItem(MonValue);
		listViewMon.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				MonValue = newValue;

			}
		});
		
		WheelView listViewTue = (WheelView) lDialog.findViewById(R.id.listViewTue);
		WheelModelAdapter wheelAdapterTue = new WheelModelAdapter(mActivity, modelStrings);
		listViewTue.setViewAdapter(wheelAdapterTue);
		listViewTue.setCurrentItem(TueValue);
		listViewTue.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				TueValue = newValue;
			}
		});
		
		WheelView listViewWed = (WheelView) lDialog.findViewById(R.id.listViewWed);
		WheelModelAdapter wheelAdapterWed = new WheelModelAdapter(mActivity, modelStrings);
		listViewWed.setViewAdapter(wheelAdapterWed);
		listViewWed.setCurrentItem(WedValue);
		listViewWed.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				WedValue = newValue;
			}
		});
		
		WheelView listViewThu = (WheelView) lDialog.findViewById(R.id.listViewThu);
		WheelModelAdapter wheelAdapterThu = new WheelModelAdapter(mActivity, modelStrings);
		listViewThu.setViewAdapter(wheelAdapterThu);
		listViewThu.setCurrentItem(ThuValue);
		listViewThu.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				ThuValue = newValue;
			}
		});
		
		WheelView listViewFri = (WheelView) lDialog.findViewById(R.id.listViewFri);
		WheelModelAdapter wheelAdapterFri = new WheelModelAdapter(mActivity, modelStrings);
		listViewFri.setViewAdapter(wheelAdapterFri);
		listViewFri.setCurrentItem(FriValue);
		listViewFri.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				FriValue = newValue;
			}
		});
		
		WheelView listViewSat = (WheelView) lDialog.findViewById(R.id.listViewSat);
		WheelModelAdapter wheelAdapterSat = new WheelModelAdapter(mActivity, modelStrings);
		listViewSat.setViewAdapter(wheelAdapterSat);
		listViewSat.setCurrentItem(SatValue);
		listViewSat.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				SatValue = newValue;
			}
		});
		
		WheelView listViewSun = (WheelView) lDialog.findViewById(R.id.listViewSun);
		WheelModelAdapter wheelAdapterSun = new WheelModelAdapter(mActivity, modelStrings);
		listViewSun.setViewAdapter(wheelAdapterSun);
		listViewSun.setCurrentItem(SunValue);
		listViewSun.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
		
				SunValue = newValue;
			}
		});
		

		((Button) lDialog.findViewById(R.id.ok))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						lDialog.dismiss();
						
						String timeString = NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute);
						
						if (isAdd) {
							
							GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
							ArrayList<GroupWiFi> groups = gDao.getAllgroup();
							for (GroupWiFi group : groups) {
								if (group.getGroupName().equalsIgnoreCase(timeString)) { //不能添加相同组名的组
									Tool.ToastShow(mActivity, R.string.groupname_cannot_same);
									return;
								}
							}
							
							if (!StringUtils.isEmpty(timeString)) {								
//								SharePersistent.saveWeekData(mActivity, timeString, getWeekString());
								SharePersistent.saveWeekData(mActivity, timeString, getModeString());
								SharePersistent.saveInt(mActivity, timeString+"mode", model);
								SharePersistent.saveInt(mActivity, timeString+"hour", hour);
								SharePersistent.saveInt(mActivity, timeString+"minute", minute);
//								SharePersistent.saveTimerData(mActivity, timeString, 
//										MonValue, TueValue, WedValue, ThuValue, FriValue, SatValue, SunValue);
								addGroupByName(timeString, getModeString());
							}
							
						}else {
							
							GroupDeviceDaoWiFi gDao = new GroupDeviceDaoWiFi(mActivity);
							ArrayList<GroupWiFi> groups = gDao.getAllgroup();
							for (GroupWiFi group : groups) {
								if (group.getGroupName().equalsIgnoreCase(timeStr)) { //不能添加相同组名的组
//									Tool.ToastShow(mActivity, R.string.groupname_cannot_same);
									Tool.ToastShow(mActivity, timeString);
									group.setGroupName(timeString);	
									try {
										gDao.updateGroupStatus(groups);
									} catch (Exception e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
//									group.setOldGroupName(timeString);	
//									return;
								}
							}
							try {
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
												
						
					}
				});
		
		((Button) lDialog.findViewById(R.id.cancel))
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						lDialog.dismiss();
//						ResetWeekData(); //重置数据
					}
				});
		lDialog.show();
	}
	
	public String getWeekString() {
		ArrayList<String> menus = new ArrayList<String>();
		String timeString = "";
		if (MonValue == 1) {
			menus.add(getResources().getString(R.string.Mon));
		} 
		if (TueValue == 1) {
			menus.add(getResources().getString(R.string.Tue));
		} 
		if (WedValue == 1) {
			menus.add(getResources().getString(R.string.Wed));
		} 
		if (ThuValue == 1) {
			menus.add(getResources().getString(R.string.Thu));
		}
		if (FriValue == 1) {
			menus.add(getResources().getString(R.string.Fri));
		}
		if (SatValue == 1) {
			menus.add(getResources().getString(R.string.Sat));
		}
		if (SunValue == 1) {
			menus.add(getResources().getString(R.string.Sun));
		}
		
		for (int i = 0; i < menus.size(); i++) {
			timeString += " "+menus.get(i);
		}
		Tool.ToastShow(mActivity, timeString+"menus.size="+menus.size());
		return timeString;
	}
	
	public String getModeString() {
				
		String[] array = mActivity.getResources().getStringArray(R.array.ble_mode);
//		String[] ary = mActivity.getResources().getStringArray(R.array.timer_mode);
				
		ArrayList<String> totalArray = new ArrayList<String>();//必须初始化 
		for (String lable : array) {	
			totalArray.add(lable);
		}
	
		for (int i = 30; i <= 211; i++) 
		{ 
			totalArray.add("MODE"+i+","+i);
		} 
		
		final String[] switchStrings = new String[totalArray.size()];
		for (int i = 0; i < totalArray.size(); i++) {
			String[] datas = totalArray.get(i).split(",");
			switchStrings[i] = datas[0];
		}
		
//		String[] timerSwitch = getResources().getStringArray(R.array.timer_mode);
//		final String[] switchStrings = new String[timerSwitch.length];
//		for (int i = 0; i < timerSwitch.length; i++) {
//			String[] datas = timerSwitch[i].split(",");
//			switchStrings[i] = datas[0];
//		}
		
		String modeString = switchStrings[model];
	
//		Tool.ToastShow(mActivity, "modeString="+modeString);
		return modeString;
	}
	
	public void initWeekData(String timeStr) {
		
		int[] timeData = SharePersistent.getWiFiTimerData(mActivity, timeStr);
		
		MonValue = timeData[0];
		TueValue = timeData[1];
		WedValue = timeData[2];
		ThuValue = timeData[3];
		FriValue = timeData[4];
		SatValue = timeData[5];
		SunValue = timeData[6];
	}
	
	public int getModeData(String timeStr) {
		
		int modeValue = SharePersistent.getInt(mActivity, timeStr+"mode");
		
		return modeValue+1;
	}
	
	public int getHourData(String timeStr) {
		
		int hour = SharePersistent.getInt(mActivity, timeStr+"hour");
		
		return hour;
	}

	public int getMinuteData(String timeStr) {
	
		int minute = SharePersistent.getInt(mActivity, timeStr+"minute");
	
		return minute;
	}
	
	public void ResetWeekData() {
		MonValue = 0;
		TueValue = 0;
		WedValue = 0;
		ThuValue = 0;
		FriValue = 0;
		SatValue = 0;
		SunValue = 0;
	}
	

	@Override
	public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onOtherButtonClick(ActionSheet actionSheet, int index, String groupName) {
		// TODO Auto-generated method stub
		
	}

	
}
