package com.ledble.fragment.smart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import java.text.ParseException;

import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.view.SegmentedRadioGroup;
import com.ledble.activity.TimerSettingActivity;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.smart.TimerSettingActivity_Smart;
import com.ledble.base.LedBleFragment;
import com.ledble.constant.CommonConstant;
import com.ledlamp.R;

/**
 * 定时
 * @author ftl
 *
 */
public class TimerFragment_Smart extends LedBleFragment {
	
	@Bind(R.id.linearLayoutTimer1) LinearLayout linearLayoutTimer1;
	@Bind(R.id.linearLayoutTimerLine1) LinearLayout linearLayoutTimerLine1;
	@Bind(R.id.linearLayoutTimerLine2) LinearLayout linearLayoutTimerLine2;
	@Bind(R.id.linearLayoutTimerLine3) LinearLayout linearLayoutTimerLine3;
	@Bind(R.id.linearLayoutTimerLine4) LinearLayout linearLayoutTimerLine4;
	@Bind(R.id.linearLayoutTimerLine5) LinearLayout linearLayoutTimerLine5;
	@Bind(R.id.linearLayoutTimerLine6) LinearLayout linearLayoutTimerLine6;
	@Bind(R.id.linearLayoutTimerLine7) LinearLayout linearLayoutTimerLine7;
	@Bind(R.id.linearLayoutTimerLine8) LinearLayout linearLayoutTimerLine8;
	@Bind(R.id.linearLayoutTimerLine9) LinearLayout linearLayoutTimerLine9;
	@Bind(R.id.linearLayoutTimerLine10) LinearLayout linearLayoutTimerLine10;
//	@Bind(R.id.linearLayoutTimer) LinearLayout linearLayoutTimer;
	@Bind(R.id.changeButton1) SegmentedRadioGroup changeRadioGroup1;
	@Bind(R.id.tvOK1) TextView tvOK1;
	
	@Bind(R.id.linearLayoutTimer2) LinearLayout linearLayoutTimer2;
	@Bind(R.id.linearLayoutTimerLine21) LinearLayout linearLayoutTimerLine21;
	@Bind(R.id.linearLayoutTimerLine22) LinearLayout linearLayoutTimerLine22;
	@Bind(R.id.linearLayoutTimerLine23) LinearLayout linearLayoutTimerLine23;
	@Bind(R.id.linearLayoutTimerLine24) LinearLayout linearLayoutTimerLine24;
	@Bind(R.id.linearLayoutTimerLine25) LinearLayout linearLayoutTimerLine25;
	@Bind(R.id.linearLayoutTimerLine26) LinearLayout linearLayoutTimerLine26;
	@Bind(R.id.linearLayoutTimerLine27) LinearLayout linearLayoutTimerLine27;
	@Bind(R.id.linearLayoutTimerLine28) LinearLayout linearLayoutTimerLine28;
	@Bind(R.id.linearLayoutTimerLine29) LinearLayout linearLayoutTimerLine29;
	@Bind(R.id.linearLayoutTimerLine210) LinearLayout linearLayoutTimerLine210;
	@Bind(R.id.changeButton2) SegmentedRadioGroup changeRadioGroup2;
	@Bind(R.id.tvOK2) TextView tvOK2;
	
	@Bind(R.id.linearLayoutTimer3) LinearLayout linearLayoutTimer3;
	@Bind(R.id.linearLayoutTimerLine31) LinearLayout linearLayoutTimerLine31;
	@Bind(R.id.linearLayoutTimerLine32) LinearLayout linearLayoutTimerLine32;
	@Bind(R.id.linearLayoutTimerLine33) LinearLayout linearLayoutTimerLine33;
	@Bind(R.id.linearLayoutTimerLine34) LinearLayout linearLayoutTimerLine34;
	@Bind(R.id.linearLayoutTimerLine35) LinearLayout linearLayoutTimerLine35;
	@Bind(R.id.linearLayoutTimerLine36) LinearLayout linearLayoutTimerLine36;
	@Bind(R.id.linearLayoutTimerLine37) LinearLayout linearLayoutTimerLine37;
	@Bind(R.id.linearLayoutTimerLine38) LinearLayout linearLayoutTimerLine38;
	@Bind(R.id.linearLayoutTimerLine39) LinearLayout linearLayoutTimerLine39;
	@Bind(R.id.linearLayoutTimerLine310) LinearLayout linearLayoutTimerLine310;
//	@Bind(R.id.linearLayoutTimer) LinearLayout linearLayoutTimer;
	@Bind(R.id.changeButton3) SegmentedRadioGroup changeRadioGroup3;
	@Bind(R.id.tvOK3) TextView tvOK3;
	
	
	private LinearLayout linearLayoutTemp;
	
	private int style1 = 0;
	private int style2 = 0;
	private int style3 = 0;
	private int linearLayoutTag;
	private MainActivity_BLE mActivity;
	private int INT_TIMER_ID;
//	private int INT_GO_COLORMODE = 112; //颜色模式选择
	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";
	
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_timer_smart, container, false);
	}

	@Override
	public void initData() {
		mActivity = (MainActivity_BLE) getActivity();	
		
		boolean first = SharePersistent.getBoolean(mActivity, "FIRST");
		if (!first) {
			for (int i = 1; i <= 10; i++) {
				if (1 == i) {
					saveTimerData(i, 9, 0, 0, 0, 0, 0, 0,  0);
				}else if (2 == i) {
					saveTimerData(i, 10, 0, 10, 10, 0, 0, 0,  0);
				}else if (3 == i) {
					saveTimerData(i, 12, 0, 80, 85, 85, 85, 85,  85);
				}else if (4 == i) {
					saveTimerData(i, 13, 0, 80, 100, 100, 100, 100,  100);
				}else if (5 == i) {
					saveTimerData(i, 14, 0, 100, 100, 100, 100, 100,  100);
				}else if (6 == i) {
					saveTimerData(i, 16, 0, 100, 100, 100, 60, 60,  60);
				}else if (7 == i) {
					saveTimerData(i, 18, 0, 100, 100, 80, 30, 30,  30);
				}else if (8 == i) {
					saveTimerData(i, 20, 0, 90, 80, 60, 10, 10,  10);
				}else if (9 == i) {
					saveTimerData(i, 22, 0, 70, 70, 50, 10, 10,  10);
				}else if (10 == i) {
					saveTimerData(i, 23, 0, 0, 0, 0, 0, 0,  0);
				}			
			}
		}
		
		for (int i = 1; i <= 10; i++) {
			int[] array = SharePersistent.getTimerData(mActivity, String.valueOf(i), String.valueOf(i));
			
			final String[] timerModel = getResources().getStringArray(R.array.select_mode);
			String[] datas = timerModel[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");			
			if (datas[0].equalsIgnoreCase(RGB)) {
				updataUI((LinearLayout) linearLayoutTimer1.findViewWithTag((String.valueOf("linearLayoutTimerLine"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);

			}else if (datas[0].equalsIgnoreCase(RGBW)) {
				updataUI((LinearLayout) linearLayoutTimer2.findViewWithTag((String.valueOf("linearLayoutTimerLine2"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);

			}else if (datas[0].equalsIgnoreCase(RGBWC)) {

			}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
				updataUI((LinearLayout) linearLayoutTimer3.findViewWithTag((String.valueOf("linearLayoutTimerLine3"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);
			}						
		}
	}

	@Override
	public void initView() {	
		initViews();
		SharePersistent.saveBoolean(mActivity, "FIRST", true);
	}

	@Override
	public void initEvent() {
		
	}
	
	private void initViews() {
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");			
		if (datas[0].equalsIgnoreCase(RGB)) {
			linearLayoutTimerLine1.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 1;
					linearLayoutTemp = linearLayoutTimerLine1;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine2.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 2;
					linearLayoutTemp = linearLayoutTimerLine2;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine3.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 3;
					linearLayoutTemp = linearLayoutTimerLine3;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine4.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 4;
					linearLayoutTemp = linearLayoutTimerLine4;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine5.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 5;
					linearLayoutTemp = linearLayoutTimerLine5;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine6.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 6;
					linearLayoutTemp = linearLayoutTimerLine6;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine7.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 7;
					linearLayoutTemp = linearLayoutTimerLine7;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine8.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 8;
					linearLayoutTemp = linearLayoutTimerLine8;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine9.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 9;
					linearLayoutTemp = linearLayoutTimerLine9;
					gotoTimerSettting(v, 9, SharePersistent.getTimerData(mActivity, String.valueOf(9), String.valueOf(9)));
				}
			});
			
			linearLayoutTimerLine10.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 10;
					linearLayoutTemp = linearLayoutTimerLine10;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			changeRadioGroup1.check(R.id.changeButtonOne1);
			changeRadioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (R.id.changeButtonOne1 == checkedId) {
						style1 = 0;
					} else if ((R.id.changeButtonTwo1 == checkedId)) {
						style1 = 1;
					}
				}
			});
			
			tvOK1.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					Toast.makeText(mActivity, ""+style1, Toast.LENGTH_SHORT).show();
					
					mActivity.setTimerFirData(style1);

					for (int i = 1; i <= 10; i++) {
						int[] array = SharePersistent.getTimerData(mActivity, String.valueOf(i), String.valueOf(i));				
						
						try {						
//							Toast.makeText(mActivity, str0+" "+str1+" "+str2+" "+str3+" "+str4+" "+str5+" "+str6+" "+str7, Toast.LENGTH_SHORT).show();
							Thread.sleep(200);
							mActivity.setTimerSecData(array);
//							Toast.makeText(mActivity, ""+array[0]+":"+array[1]+" "+array[2]+" "+array[3]+" "+array[4]+" "+array[5]+" "+array[6]+" "+array[7], Toast.LENGTH_SHORT).show();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					Toast.makeText(mActivity, getString(R.string.sent_success), Toast.LENGTH_SHORT).show();
				}
			});
			
		}else if (datas[0].equalsIgnoreCase(RGBW)) {			
			linearLayoutTimerLine21.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 1;
					linearLayoutTemp = linearLayoutTimerLine21;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine22.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 2;
					linearLayoutTemp = linearLayoutTimerLine22;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine23.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 3;
					linearLayoutTemp = linearLayoutTimerLine23;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine24.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 4;
					linearLayoutTemp = linearLayoutTimerLine24;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine25.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 5;
					linearLayoutTemp = linearLayoutTimerLine25;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine26.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 6;
					linearLayoutTemp = linearLayoutTimerLine26;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine27.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 7;
					linearLayoutTemp = linearLayoutTimerLine27;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine28.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 8;
					linearLayoutTemp = linearLayoutTimerLine28;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine29.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 9;
					linearLayoutTemp = linearLayoutTimerLine29;
					gotoTimerSettting(v, 9, SharePersistent.getTimerData(mActivity, String.valueOf(9), String.valueOf(9)));
				}
			});
			
			linearLayoutTimerLine210.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 10;
					linearLayoutTemp = linearLayoutTimerLine210;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			changeRadioGroup2.check(R.id.changeButtonOne2);
			changeRadioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (R.id.changeButtonOne2 == checkedId) {
						style2 = 0;
					} else if ((R.id.changeButtonTwo2 == checkedId)) {
						style2 = 1;
					}
				}
			});
			
			tvOK2.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					mActivity.setTimerFirData(style2);
//					Toast.makeText(mActivity, getString(R.string.sent_success), Toast.LENGTH_SHORT).show();
					for (int i = 1; i <= 10; i++) {
						int[] array = SharePersistent.getTimerData(mActivity, String.valueOf(i), String.valueOf(i));
						
						try {						
//							Toast.makeText(mActivity, ""+array, Toast.LENGTH_SHORT).show();
							Thread.sleep(200);
							mActivity.setTimerSecData(array);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					Toast.makeText(mActivity, getString(R.string.sent_success), Toast.LENGTH_SHORT).show();
				}
			});
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {
			
		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			linearLayoutTimerLine31.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 1;
					linearLayoutTemp = linearLayoutTimerLine31;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine32.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 2;
					linearLayoutTemp = linearLayoutTimerLine32;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine33.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 3;
					linearLayoutTemp = linearLayoutTimerLine33;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine34.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 4;
					linearLayoutTemp = linearLayoutTimerLine34;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine35.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 5;
					linearLayoutTemp = linearLayoutTimerLine35;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine36.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 6;
					linearLayoutTemp = linearLayoutTimerLine36;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine37.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 7;
					linearLayoutTemp = linearLayoutTimerLine37;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine38.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 8;
					linearLayoutTemp = linearLayoutTimerLine38;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			linearLayoutTimerLine39.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 9;
					linearLayoutTemp = linearLayoutTimerLine39;
					gotoTimerSettting(v, 9, SharePersistent.getTimerData(mActivity, String.valueOf(9), String.valueOf(9)));
				}
			});
			
			linearLayoutTimerLine310.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					linearLayoutTag = 10;
					linearLayoutTemp = linearLayoutTimerLine310;
					gotoTimerSettting(v, linearLayoutTag, SharePersistent.getTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag)));
				}
			});
			
			changeRadioGroup3.check(R.id.changeButtonOne3);
			changeRadioGroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(RadioGroup group, int checkedId) {
					if (R.id.changeButtonOne3 == checkedId) {
						style3 = 0;
					} else if ((R.id.changeButtonTwo3 == checkedId)) {
						style3 = 1;
					}
				}
			});
			
			tvOK3.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					mActivity.setTimerFirData(style3);
//					Toast.makeText(mActivity, getString(R.string.sent_success), Toast.LENGTH_SHORT).show();
					for (int i = 1; i <= 10; i++) {
						int[] array = SharePersistent.getTimerData(mActivity, String.valueOf(i), String.valueOf(i));
//						Toast.makeText(mActivity, ""+array[0]+":"+array[1]+" "+array[2]+" "+array[3]+" "+array[4]+" "+array[5]+" "+array[6]+" "+array[7], Toast.LENGTH_SHORT).show();
						try {												
							Thread.sleep(200);
							mActivity.setTimerSecData(array);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					Toast.makeText(mActivity, getString(R.string.sent_success), Toast.LENGTH_SHORT).show();
				}
			});
		}
	}
	
	public void setActive() {
		
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");	
		
//		Toast.makeText(mActivity, ""+datas[0], Toast.LENGTH_SHORT).show();
		if (datas[0].equalsIgnoreCase(RGB)) {
			this.linearLayoutTimer1.setVisibility(View.VISIBLE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBW)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.VISIBLE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.VISIBLE);
		}
		initViews();
		
		for (int i = 1; i <= 10; i++) {
			int[] array = SharePersistent.getTimerData(mActivity, String.valueOf(i), String.valueOf(i));
			
			final String[] timerModel1 = getResources().getStringArray(R.array.select_mode);
			String[] datas1 = timerModel1[SharePersistent.getInt(mActivity, CommonConstant.SELECT_MODE_SMART)].split(",");			
			if (datas1[0].equalsIgnoreCase(RGB)) {
				updataUI((LinearLayout) linearLayoutTimer1.findViewWithTag((String.valueOf("linearLayoutTimerLine"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);

			}else if (datas1[0].equalsIgnoreCase(RGBW)) {
				updataUI((LinearLayout) linearLayoutTimer2.findViewWithTag((String.valueOf("linearLayoutTimerLine2"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);

			}else if (datas1[0].equalsIgnoreCase(RGBWC)) {

			}else if (datas1[0].equalsIgnoreCase(RGBWCP)) {
				updataUI((LinearLayout) linearLayoutTimer3.findViewWithTag((String.valueOf("linearLayoutTimerLine3"+i))), array[0], array[1], array[2], array[3], array[4], array[5], array[6], array[7]);
			}						
		}
	}
	
	private void gotoTimerSettting(View v, int id, int[] data) {
		INT_TIMER_ID = id;
		Intent intent = new Intent(getActivity(), TimerSettingActivity_Smart.class);
		intent.putExtra("tag", id);
		intent.putExtra("data", data);
		getActivity().startActivityForResult(intent, id);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INT_TIMER_ID && resultCode == Activity.RESULT_OK) {
			int hour = data.getIntExtra("hour", -1);
			int minute = data.getIntExtra("minite", -1);
			int redValue = data.getIntExtra("redValue", -1);
			int greenValue = data.getIntExtra("greenValue", -1);
			int lightblueValue = data.getIntExtra("lightblueValue", -1);
			int whiteValue = data.getIntExtra("whiteValue", -1);
			int crystalValue = data.getIntExtra("crystalValue", -1);			
			int pinkValue = data.getIntExtra("pinkValue", -1);
			
			
			updataUI(linearLayoutTemp, hour, minute, redValue, greenValue, lightblueValue, whiteValue, crystalValue,  pinkValue);
			saveTimerData(linearLayoutTag, hour, minute, redValue, greenValue, lightblueValue, whiteValue, crystalValue,  pinkValue);
			return;
		}
	}
	
	public void updataUI(LinearLayout linearLayout, int hour, int minute, int redValue , int greenValue, int lightblueValue, int whiteValue, int crystalValue, int pinkValue) {
		
		final TextView tvTime = (TextView) linearLayout.findViewWithTag((String.valueOf("time")));
		tvTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
		
		final TextView tvRed = (TextView) linearLayout.findViewWithTag((String.valueOf("red")));
		tvRed.setText(String.valueOf(redValue));
		
		final TextView tvGreen = (TextView) linearLayout.findViewWithTag((String.valueOf("green")));
		tvGreen.setText(String.valueOf(greenValue));
		
		final TextView tvLightblue = (TextView) linearLayout.findViewWithTag((String.valueOf("lightblue")));
		tvLightblue.setText(String.valueOf(lightblueValue));
		
		final TextView tvWhite = (TextView) linearLayout.findViewWithTag((String.valueOf("white")));
		tvWhite.setText(String.valueOf(whiteValue));	
		
		final TextView tvCrystal = (TextView) linearLayout.findViewWithTag((String.valueOf("crystal")));
		tvCrystal.setText(String.valueOf(crystalValue));
		
		final TextView tvPink = (TextView) linearLayout.findViewWithTag((String.valueOf("pink")));
		tvPink.setText(String.valueOf(pinkValue));
		
	}
	
	public void saveTimerData(int linearLayoutTag, int hour, int minute, int redValue , int greenValue, int lightblueValue, int whiteValue, int crystalValue, int pinkValue) {
		
		SharePersistent.saveTimerData(mActivity, String.valueOf(linearLayoutTag), String.valueOf(linearLayoutTag), hour, minute, redValue, greenValue, lightblueValue, whiteValue, crystalValue,  pinkValue);

	}

}
