package com.ledble.db.wifi;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.common.uitl.LogUtil;

public class GroupDeviceDaoWiFi {
	private SQLiteHelperWiFi sqLiteHelper;

	public GroupDeviceDaoWiFi(Context context) {
		sqLiteHelper = new SQLiteHelperWiFi(context);
	}

	public ArrayList<GroupWiFi> getAllgroup() {
		ArrayList<GroupWiFi> groups = new ArrayList<GroupWiFi>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupWiFi.GROUP_TAB_WIFI, new String[] {});
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupWiFi group = new GroupWiFi();
			group.setGroupName((cursor.getString(cursor.getColumnIndex(GroupWiFi.GROUP_NAME_WIFI))));
			group.setIsOn((cursor.getString(cursor.getColumnIndex(GroupWiFi.GROUP_ISON_WIFI))));
			groups.add(group);
		}
		sdb.close();
		return groups;
	}

	/**
	 * 更新组信息
	 * @param groupList
	 */
	public void updateGroupStatus(ArrayList<GroupWiFi> groupList) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		for (int i = 0, isize = groupList.size(); i < isize; i++) {
			GroupWiFi group = groupList.get(i);
			
			ContentValues values = new ContentValues();
			values.put(GroupWiFi.GROUP_ISON_WIFI, group.getIsOn());
			
			sdb.update(GroupWiFi.GROUP_TAB_WIFI, values, GroupWiFi.GROUP_NAME_WIFI + " =?", new String[] { group.getGroupName() });
		}
		sdb.close();
	}

	public ArrayList<GroupDeviceWiFi> getAllGroupDevices() {
		ArrayList<GroupDeviceWiFi> groupDevices = new ArrayList<GroupDeviceWiFi>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI, new String[] {});
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupDeviceWiFi groupDevice = new GroupDeviceWiFi();
			groupDevice.setAddress(cursor.getString(cursor.getColumnIndex(GroupDeviceWiFi.ADDRESSNUM_WIFI)));
			groupDevice.setGroupName(cursor.getString(cursor.getColumnIndex(GroupDeviceWiFi.GROUPNUM_WIFI)));
			groupDevices.add(groupDevice);
		}
		sdb.close();
		return groupDevices;
	}

	public void delteByGroup(String groupName) {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		sdb.execSQL("DELETE FROM " + GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI + " WHERE " + GroupDeviceWiFi.GROUPNUM_WIFI + " = ? ", new Object[] { groupName });
		sdb.close();
	}

	public ArrayList<GroupDeviceWiFi> getDevicesByGroup(String groupName) {
		ArrayList<GroupDeviceWiFi> groupDevices = new ArrayList<GroupDeviceWiFi>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI + " where " + GroupDeviceWiFi.GROUPNUM_WIFI + " =?",
				new String[] { groupName });
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupDeviceWiFi groupDevice = new GroupDeviceWiFi();
			groupDevice.setAddress(cursor.getString(cursor.getColumnIndex(GroupDeviceWiFi.ADDRESSNUM_WIFI)));
			groupDevice.setGroupName(cursor.getString(cursor.getColumnIndex(GroupDeviceWiFi.GROUPNUM_WIFI)));
			groupDevices.add(groupDevice);
		}
		sdb.close();
		return groupDevices;
	}

	public void addGroup(String groupName) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			ContentValues values = new ContentValues();
			values.put(GroupWiFi.GROUP_NAME_WIFI, groupName);
			sdb.insert(GroupWiFi.GROUP_TAB_WIFI, null, values);
			sdb.close();
		}
	}

	public void save2Group(ArrayList<GroupDeviceWiFi> groupDevice) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			for (GroupDeviceWiFi gd : groupDevice) {
				ContentValues values = new ContentValues();
				values.put(GroupDeviceWiFi.ADDRESSNUM_WIFI, gd.getAddress());
				values.put(GroupDeviceWiFi.GROUPNUM_WIFI, gd.getGroupName());
				sdb.insert(GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI, null, values);
			}
			sdb.close();
		}
	}

	public void deleteGroup(String groupName) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			sdb.execSQL("DELETE FROM " + GroupWiFi.GROUP_TAB_WIFI + " WHERE " + GroupWiFi.GROUP_NAME_WIFI + " = ?", new Object[] { groupName });
			sdb.close();
		}
	}

	public void deleteGroupDevice(ArrayList<GroupDeviceWiFi> groupDevice) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			for (GroupDeviceWiFi gd : groupDevice) {
				sdb.execSQL("DELETE FROM " + GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI + " WHERE " + GroupDeviceWiFi.ADDRESSNUM_WIFI + " = ? & "
						+ GroupDeviceWiFi.GROUPNUM_WIFI + " = ? ", new Object[] { gd.getAddress(), gd.getGroupName() });
			}
			sdb.close();
		}
	}

}
