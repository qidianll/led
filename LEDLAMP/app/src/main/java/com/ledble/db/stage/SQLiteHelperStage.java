package com.ledble.db.stage;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelperStage extends SQLiteOpenHelper {
	
	private static final String dbname_stage="groupstage.db";
	private static final int version_stage=1;
	
	public SQLiteHelperStage(Context context) {
		super(context, dbname_stage, null, version_stage);
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		//组名
		db.execSQL("CREATE TABLE IF NOT EXISTS " + GroupStage.GROUP_TAB_STAGE + " ( " +
				GroupStage.GROUP_NAME_STAGE + " varchar primary key,"+
				GroupStage.GROUP_ISON_STAGE + " varchar"+
				" ) ");
		
		
		//组名和附属的组
		db.execSQL("CREATE TABLE IF NOT EXISTS " + GroupDeviceStage.GROUP_CONTENT_TAB_STAGE + " ( " +
				GroupDeviceStage.ADDRESSNUM_STAGE + " varchar,"+ 
				GroupDeviceStage.GROUPNUM_STAGE + " varchar" +" ) ");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}


}
