package com.ledble.db.stage;

import com.common.bean.IBeanInterface;

public class GroupDeviceStage implements IBeanInterface{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//==========
	public final static String GROUP_CONTENT_TAB_STAGE="group_content_name_stage";//组
	public final static String ADDRESSNUM_STAGE= "address_stage";//地址
	public final static String GROUPNUM_STAGE = "groupName_stage";//所在组名

	private String address;
	private String groupName;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
