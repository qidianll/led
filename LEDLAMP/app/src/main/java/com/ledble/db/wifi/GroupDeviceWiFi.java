package com.ledble.db.wifi;

import com.common.bean.IBeanInterface;

public class GroupDeviceWiFi implements IBeanInterface{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//==========
	public final static String GROUP_CONTENT_TAB_WIFI="group_content_name";//组
	public final static String ADDRESSNUM_WIFI= "address_wifi";//地址
	public final static String GROUPNUM_WIFI = "groupName_wifi";//所在组名

	private String address;
	private String groupName;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
