package com.ledble.db.stage;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.common.uitl.LogUtil;

public class GroupDeviceDaoStage {
	private SQLiteHelperStage sqLiteHelper;

	public GroupDeviceDaoStage(Context context) {
		sqLiteHelper = new SQLiteHelperStage(context);
	}

	public ArrayList<GroupStage> getAllgroup() {
		ArrayList<GroupStage> groups = new ArrayList<GroupStage>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupStage.GROUP_TAB_STAGE, new String[] {});
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupStage group = new GroupStage();
			group.setGroupName((cursor.getString(cursor.getColumnIndex(GroupStage.GROUP_NAME_STAGE))));
			group.setIsOn((cursor.getString(cursor.getColumnIndex(GroupStage.GROUP_ISON_STAGE))));
			groups.add(group);
		}
		sdb.close();
		return groups;
	}

	/**
	 * 更新组信息
	 * @param groupList
	 */
	public void updateGroupStatus(ArrayList<GroupStage> groupList) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		for (int i = 0, isize = groupList.size(); i < isize; i++) {
			GroupStage group = groupList.get(i);
			
			ContentValues values = new ContentValues();
			values.put(GroupStage.GROUP_ISON_STAGE, group.getIsOn());
			
			sdb.update(GroupStage.GROUP_TAB_STAGE, values, GroupStage.GROUP_NAME_STAGE + " =?", new String[] { group.getGroupName() });
		}
		sdb.close();
	}

	public ArrayList<GroupDeviceStage> getAllGroupDevices() {
		ArrayList<GroupDeviceStage> groupDevices = new ArrayList<GroupDeviceStage>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupDeviceStage.GROUP_CONTENT_TAB_STAGE, new String[] {});
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupDeviceStage groupDevice = new GroupDeviceStage();
			groupDevice.setAddress(cursor.getString(cursor.getColumnIndex(GroupDeviceStage.ADDRESSNUM_STAGE)));
			groupDevice.setGroupName(cursor.getString(cursor.getColumnIndex(GroupDeviceStage.GROUPNUM_STAGE)));
			groupDevices.add(groupDevice);
		}
		sdb.close();
		return groupDevices;
	}

	public void delteByGroup(String groupName) {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		sdb.execSQL("DELETE FROM " + GroupDeviceStage.GROUP_CONTENT_TAB_STAGE + " WHERE " + GroupDeviceStage.GROUPNUM_STAGE + " = ? ", new Object[] { groupName });
		sdb.close();
	}

	public ArrayList<GroupDeviceStage> getDevicesByGroup(String groupName) {
		ArrayList<GroupDeviceStage> groupDevices = new ArrayList<GroupDeviceStage>();
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		Cursor cursor = sdb.rawQuery("select * from " + GroupDeviceStage.GROUP_CONTENT_TAB_STAGE + " where " + GroupDeviceStage.GROUPNUM_STAGE + " =?",
				new String[] { groupName });
		// cursor.moveToFirst();
		while (cursor.moveToNext()) {
			GroupDeviceStage groupDevice = new GroupDeviceStage();
			groupDevice.setAddress(cursor.getString(cursor.getColumnIndex(GroupDeviceStage.ADDRESSNUM_STAGE)));
			groupDevice.setGroupName(cursor.getString(cursor.getColumnIndex(GroupDeviceStage.GROUPNUM_STAGE)));
			groupDevices.add(groupDevice);
		}
		sdb.close();
		return groupDevices;
	}

	public void addGroup(String groupName) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			ContentValues values = new ContentValues();
			values.put(GroupStage.GROUP_NAME_STAGE, groupName);
			sdb.insert(GroupStage.GROUP_TAB_STAGE, null, values);
			sdb.close();
		}
	}

	public void save2Group(ArrayList<GroupDeviceStage> groupDevice) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			for (GroupDeviceStage gd : groupDevice) {
				ContentValues values = new ContentValues();
				values.put(GroupDeviceStage.ADDRESSNUM_STAGE, gd.getAddress());
				values.put(GroupDeviceStage.GROUPNUM_STAGE, gd.getGroupName());
				sdb.insert(GroupDeviceStage.GROUP_CONTENT_TAB_STAGE, null, values);
			}
			sdb.close();
		}
	}

	public void deleteGroup(String groupName) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			sdb.execSQL("DELETE FROM " + GroupStage.GROUP_TAB_STAGE + " WHERE " + GroupStage.GROUP_NAME_STAGE + " = ?", new Object[] { groupName });
			sdb.close();
		}
	}

	public void deleteGroupDevice(ArrayList<GroupDeviceStage> groupDevice) throws Exception {
		SQLiteDatabase sdb = sqLiteHelper.getWritableDatabase();
		if (null != sdb && sdb.isOpen()) {
			for (GroupDeviceStage gd : groupDevice) {
				sdb.execSQL("DELETE FROM " + GroupDeviceStage.GROUP_CONTENT_TAB_STAGE + " WHERE " + GroupDeviceStage.ADDRESSNUM_STAGE + " = ? & "
						+ GroupDeviceStage.GROUPNUM_STAGE + " = ? ", new Object[] { gd.getAddress(), gd.getGroupName() });
			}
			sdb.close();
		}
	}

}
