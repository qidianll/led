package com.ledble.db.wifi;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelperWiFi extends SQLiteOpenHelper {
	
	private static final String dbname_wifi="groupwifi.db";
	private static final int version_wifi=1;
	
	public SQLiteHelperWiFi(Context context) {
		super(context, dbname_wifi, null, version_wifi);
	}
	

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		//组名
		db.execSQL("CREATE TABLE IF NOT EXISTS " + GroupWiFi.GROUP_TAB_WIFI + " ( " +
				GroupWiFi.GROUP_NAME_WIFI + " varchar primary key,"+
				GroupWiFi.GROUP_ISON_WIFI + " varchar"+
				" ) ");
		
		
		//组名和附属的组
		db.execSQL("CREATE TABLE IF NOT EXISTS " + GroupDeviceWiFi.GROUP_CONTENT_TAB_WIFI + " ( " +
				GroupDeviceWiFi.ADDRESSNUM_WIFI + " varchar,"+ 
				GroupDeviceWiFi.GROUPNUM_WIFI + " varchar" +" ) ");
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}


}
