package com.ledble.db.stage;

public class GroupStage {

//	public final static String GROUP_TAB_WIFI = "group_sw";
//	public final static String GROUP_NAME_WIFI = "group_namew";
	
	public final static String GROUP_TAB_STAGE = "group_ss";
	public final static String GROUP_NAME_STAGE = "group_names";
	
//	public final static String GROUP_TAB = "group_s";
//	public final static String GROUP_NAME = "group_name";
	public final static String GROUP_ISON_STAGE = "is_on_stage";
	private String groupName;
	private String isOn;

	public GroupStage() {
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getIsOn() {
		return isOn;
	}

	public void setIsOn(String isOn) {
		this.isOn = isOn;
	}

}
