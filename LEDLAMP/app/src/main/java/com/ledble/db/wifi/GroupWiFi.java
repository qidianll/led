package com.ledble.db.wifi;

public class GroupWiFi {

	public final static String GROUP_TAB_WIFI = "group_sw";
	public final static String GROUP_NAME_WIFI = "group_namew";
	
//	public final static String GROUP_TAB = "group_s";
//	public final static String GROUP_NAME = "group_name";
	public final static String GROUP_ISON_WIFI = "is_on_wifi";
	private String groupName;
	private String isOn;

	public GroupWiFi() {
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getIsOn() {
		return isOn;
	}

	public void setIsOn(String isOn) {
		this.isOn = isOn;
	}

}
