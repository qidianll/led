package com.ledble.activity.wifi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import com.common.net.NetResult;
import com.common.task.BaseTask;
import com.common.task.NetCallBack;
import com.common.uitl.SharePersistent;
import com.common.uitl.StringUtils;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.activity.ChipSelectActivity;
import com.ledble.activity.CodeActivity;
import com.ledble.activity.OprationManualActivity;
import com.ledble.activity.TimerActivity;
import com.ledble.base.LedBleActivity;
import com.ledble.bean.MyColor;
import com.ledble.bean.SceneBean;
import com.ledble.constant.Constant;
import com.ledble.fragment.wifi.BrightFragment_WiFi;
import com.ledble.fragment.wifi.CutomFragment_WiFi;
import com.ledble.fragment.wifi.ModeFragment_WiFi;
import com.ledble.fragment.wifi.MusicFragment_WiFi;
import com.ledble.fragment.wifi.RgbFragment_WiFi;
import com.ledble.fragment.wifi.TimerFragment_WiFi;
import com.ledble.net.NetConnectBle;
import com.ledble.net.NetExceptionInterface;
import com.ledble.net.WifiConenction;
import com.ledble.net.wifi.callback.ConnectDeviceListener;
import com.ledble.utils.ManageFragment;

@SuppressLint("NewApi")
public class MainActivity_WiFi extends LedBleActivity implements NetExceptionInterface, SensorEventListener  {

	@Bind(R.id.segmentDm) SegmentedRadioGroup segmentDm;
	@Bind(R.id.segmentCt) SegmentedRadioGroup segmentCt;
	@Bind(R.id.segmentRgb) SegmentedRadioGroup segmentRgb;
//	@Bind(R.id.segmentMusic) SegmentedRadioGroup segmentMusic;
	@Bind(R.id.textViewModeTitle) TextView textViewModeTitle;
	@Bind(R.id.textViewCustomTitle) TextView textViewCustomTitle;
	@Bind(R.id.textViewBrightnessTitle) TextView textViewBrightnessTitle;
	@Bind(R.id.rlTimerAddButton) RelativeLayout rlTimerAddButton;
	@Bind(R.id.timerAddButton) ImageView timerAddButton;

	@Bind(R.id.backLinearLayout) LinearLayout backLinearLayout;
	@Bind(R.id.backTextView) TextView backTextView;
	@Bind(R.id.ivLeftMenu) ImageView ivLeftMenu;
	@Bind(R.id.textViewConnectCount) TextView textViewConnectCount;
	@Bind(R.id.ivRightMenu) ImageView ivRightMenu;
	@Bind(R.id.onOffButton) Button onOffButton;
	@Bind(R.id.ivType) ImageView ivType;
	@Bind(R.id.rgBottom) RadioGroup rgBottom;
	@Bind(R.id.rbFirst) RadioButton rbFirst;
	@Bind(R.id.rbSecond) RadioButton rbSecond;
	@Bind(R.id.rbThrid) RadioButton rbThrid;
	@Bind(R.id.rbFourth) RadioButton rbFourth;
	@Bind(R.id.rbFifth) RadioButton rbFifth;

	@Bind(R.id.llMenu) LinearLayout avtivity_main;
	@Bind(R.id.menu_content_layout) LinearLayout left_menu;
	@Bind(R.id.right_menu_frame) ScrollView right_menu;
	@Bind(R.id.linearLayoutTopItem) LinearLayout TopItem;
	@Bind(R.id.shakeView) RelativeLayout shakeView;

	private int currentIndex;
	private FragmentManager fragmentManager;
	private List<Fragment> fragmentList = new ArrayList<Fragment>();
	private DrawerLayout mDrawerLayout;

	public boolean isLightOpen = true;
	public int speed = 1;
	private String speedKey = "speedkey";
	public int brightness = 1;
	private String brightnessKey = "brightnesskey";
	private String groupName = "";
	private SharedPreferences sp;
	private Editor editor;


	private static MainActivity_WiFi mActivity;
	private TimerFragment_WiFi timerFragment;

	// ====wifi==相关
	private WifiManager wifiManager;
	private WifiConenction wifiConenction;
	List<ScanResult> scanResults;
	private BroadcastReceiver receiver;
	private BaseTask baseTask;

	// right_menu
	private TextView channelsetTV;
	private TextView operationguideTV, resetTV, changePicTV, dynamicTV, shakeTV, timesetTV, fanTV;
	private ImageView gradientIV, breathIV, jumpIV, strobeIV;
	private TextView timerheckTV, TBCheckTV;
	private int STORAGE_CODE = 112;

	private Bitmap bm;
	private ImageView imageView = null; // app背景 
	private final int TAKE_PICTURE = 0; //
	private final int CHOOSE_PICTURE = 1;
	
	private boolean islegal = true; //是否合法


	private int INT_GO_LIST = 111;

	private boolean isAllOn = true;
	public final String ILLEGAL_DEVICE_NAME = "ELK-BLED";  //非法设备
	private boolean isShowIllegal = false;


    private boolean isUDP = false;
    private final String encrypteKey = "1234567890abcdef"; //加密key
    public boolean isFirstOpen = true;
    private String randStr;
    private Random rand;
    
    public static SceneBean sceneBean;

	
	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main_wifi);

		mActivity = this;
		
		if (Build.VERSION.SDK_INT >= 21) {
			View decorView = getWindow().getDecorView();
			int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
			decorView.setSystemUiVisibility(option);
			getWindow().setStatusBarColor(Color.TRANSPARENT); //透明 状态栏		
		}	
		
		Intent intent = getIntent();
		sceneBean = (SceneBean) intent.getSerializableExtra("scene");

		initFragment();
		initSlidingMenu();
		initView();
		

		if (getImagePath() != "") { // 显示保存的皮肤
			showImage(getImagePath());
		}
		
	}
	
	public MainActivity_WiFi() {
		mActivity = this;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.e("onkeydown", "in");

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			MainActivity_WiFi.this.finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}

	public static MainActivity_WiFi getMainActivity() {
		return mActivity;
	}
	
	public static SceneBean getSceneBean() {
		return sceneBean;
	}
	

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// super.onSaveInstanceState(outState);
	}

	@SuppressLint("NewApi")
	private void initView() {

		wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifiConenction = new WifiConenction(getApplicationContext(), false, null);
//		wifiConenction = new WifiConenction(getApplicationContext(), true, null);
		

		//右菜单  各个标签项
		// 通道设置
		channelsetTV = (TextView) mActivity.findViewById(R.id.tv_channel_set);
		channelsetTV.setOnClickListener(new MyOnClickListener());

		changePicTV = (TextView) mActivity.findViewById(R.id.change_under_pic_tv);
		changePicTV.setOnClickListener(new MyOnClickListener());
		resetTV = (TextView) mActivity.findViewById(R.id.reset_tv);
		resetTV.setOnClickListener(new MyOnClickListener());
		shakeTV = (TextView) mActivity.findViewById(R.id.shake_tv);
		shakeTV.setOnClickListener(new MyOnClickListener());
		dynamicTV = (TextView) mActivity.findViewById(R.id.dynamic_tv);
		dynamicTV.setOnClickListener(new MyOnClickListener());

		// 右菜单 律动各项
		gradientIV = (ImageView) mActivity.findViewById(R.id.dynamic_gradient_iv);
		gradientIV.setOnClickListener(new MyOnClickListener());
		breathIV = (ImageView) mActivity.findViewById(R.id.dynamic_breath_iv);
		breathIV.setOnClickListener(new MyOnClickListener());
		jumpIV = (ImageView) mActivity.findViewById(R.id.dynamic_jump_iv);
		jumpIV.setOnClickListener(new MyOnClickListener());
		strobeIV = (ImageView) mActivity.findViewById(R.id.dynamic_strobe_iv);
		strobeIV.setOnClickListener(new MyOnClickListener());

		timesetTV = (TextView) mActivity.findViewById(R.id.other_set_tv);
		timesetTV.setOnClickListener(new MyOnClickListener());
		fanTV = (TextView) mActivity.findViewById(R.id.fan_rotational_temperature_tv);
		fanTV.setOnClickListener(new MyOnClickListener());

		timerheckTV = (TextView) mActivity.findViewById(R.id.tv_timer_check);
		timerheckTV.setOnClickListener(new MyOnClickListener());
		TBCheckTV = (TextView) mActivity.findViewById(R.id.tv_tb_check);
		TBCheckTV.setOnClickListener(new MyOnClickListener());

		operationguideTV = (TextView) mActivity.findViewById(R.id.operation_guide_tv);
		operationguideTV.setOnClickListener(new MyOnClickListener());
		
		
		

		
		//左侧   菜单
		ivLeftMenu.setVisibility(View.GONE);
		textViewConnectCount.setVisibility(View.GONE);
		backLinearLayout = (LinearLayout) findViewById(R.id.backLinearLayout);
		backLinearLayout.setOnClickListener(new MyOnClickListener());
		backTextView = (TextView) findViewById(R.id.backTextView);
		backLinearLayout.setOnClickListener(new MyOnClickListener());
		
		onOffButton = (Button) findViewById(R.id.onOffButton);
		onOffButton.setOnClickListener(new MyOnClickListener());

		imageView = (ImageView) mActivity.findViewById(R.id.activity_main_imageview); //app背景图
		TopItem.setOnClickListener(new MyOnClickListener());

		
		
		// 初始tab项
		sp = getSharedPreferences(Constant.MODLE_TYPE, Context.MODE_PRIVATE);
		editor = sp.edit();
//		currentIndex = sp.getInt(Constant.MODLE_VALUE, 2);
		currentIndex = 0;

		// 添加定时
		timerAddButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (timerFragment != null) {
					timerFragment.addTimer();
				}
			}
		});
		

		ivType.setImageResource(R.drawable.tab_dim_check);
		ivType.setOnClickListener(new MyOnClickListener());
		ivLeftMenu.setOnClickListener(new MyOnClickListener());
		ivRightMenu.setOnClickListener(new MyOnClickListener());

		// buttonAllOff.setOnClickListener(new MyOnClickListener());
		// buttonAllOn.setOnClickListener(new MyOnClickListener());
		// buttonAddGroup.setOnClickListener(new MyOnClickListener());
		

		rgBottom.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbFirst:
					currentIndex = 0;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.VISIBLE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					rlTimerAddButton.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
//					rbFirst.setVisibility(View.VISIBLE);
//					rbSecond.setVisibility(View.VISIBLE);
//					rbThrid.setVisibility(View.VISIBLE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					pauseMusicAndVolum(true);
					break;
				case R.id.rbSecond:
					currentIndex = 1;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.VISIBLE);
					textViewCustomTitle.setVisibility(View.GONE);
					rlTimerAddButton.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					pauseMusicAndVolum(true);
					break;
				case R.id.rbThrid:
					currentIndex = 2;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.VISIBLE);
					rlTimerAddButton.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					editor.commit();
					pauseMusicAndVolum(true);
					break;
				case R.id.rbFourth:
					currentIndex = 3;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.VISIBLE);
					textViewModeTitle.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					rlTimerAddButton.setVisibility(View.GONE);
					ivType.setVisibility(View.INVISIBLE);
					pauseMusicAndVolum(false);
					break;
				case R.id.rbFifth:
					currentIndex = 4;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					rlTimerAddButton.setVisibility(View.VISIBLE);
					ivType.setVisibility(View.INVISIBLE);
					pauseMusicAndVolum(true);
					break;
				}
				ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
			}
		});

		if (currentIndex == 0) {
			ivType.setImageResource(R.drawable.tab_dim_check);
			rgBottom.check(R.id.rbFirst);
		} else if (currentIndex == 1) {
			ivType.setImageResource(R.drawable.tab_ct_check);
			rgBottom.check(R.id.rbSecond);
		} else if (currentIndex == 2) {
			ivType.setImageResource(R.drawable.tab_reg_check);
			rgBottom.check(R.id.rbThrid);
		}
		
		brightness = SharePersistent.getInt(this, brightnessKey);
		speed = SharePersistent.getInt(this, speedKey);


//		if (mBluetoothAdapter.isEnabled()) {
//			initBleScanList(isAllOn);
//		}
//		initBleScanList(isAllOn);
	}

	/**
	 * 初始化滑动菜单
	 */
	private void initSlidingMenu() {

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setScrimColor(Color.TRANSPARENT);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	
	}

	/**
	 * 初始化fragment
	 */
	public void initFragment() {

		fragmentList.add(new RgbFragment_WiFi());
		fragmentList.add(new ModeFragment_WiFi());
		fragmentList.add(new CutomFragment_WiFi());
		fragmentList.add(new BrightFragment_WiFi());
		
		timerFragment = new TimerFragment_WiFi();
		fragmentList.add(timerFragment);

		// 添加Fragment
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		for (int i = 0; i < fragmentList.size(); i++) {
			transaction.add(R.id.flContent, fragmentList.get(i), fragmentList.get(i).getClass().getSimpleName());
		}
		transaction.commit();
		ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
	}

	public void pauseMusicAndVolum(boolean status) {
//		musicFragment.pauseMusic();
//		musicFragment.pauseVolum(status);
	}

	public class MyOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			
			case R.id.backLinearLayout: // 返回
//				Toast.makeText(mActivity, "back", Toast.LENGTH_SHORT).show();
				finish();
				break;
			case R.id.backTextView: // 返回
//				Toast.makeText(mActivity, "back", Toast.LENGTH_SHORT).show();
				finish();
				break;
				
			case R.id.onOffButton: // 
				if (isLightOpen) {
					mActivity.close();
					onOffButton.setBackgroundResource(R.drawable.off_btn);
					isLightOpen = false;
				}else {
					mActivity.open();
					onOffButton.setBackgroundResource(R.drawable.on_btn);
					isLightOpen = true;
				}
				break;
			
			case R.id.ivLeftMenu:
				mDrawerLayout.openDrawer(left_menu);
				break;
			case R.id.ivRightMenu:
				mDrawerLayout.openDrawer(right_menu);
				break;
			case R.id.linearLayoutTopItem:
//				showActionSheet("");
				break;
			case R.id.ivType:
				if (currentIndex == 0) {
					ivType.setImageResource(R.drawable.tab_ct_check);
					rgBottom.check(R.id.rbSecond);
				} else if (currentIndex == 1) {
					ivType.setImageResource(R.drawable.tab_reg_check);
					rgBottom.check(R.id.rbThrid);
				} else if (currentIndex == 2) {
					ivType.setImageResource(R.drawable.tab_dim_check);
					rgBottom.check(R.id.rbFirst);
				}
				break;

			case R.id.buttonAllOff:
				// Toast.makeText(mActivity, "全关", Toast.LENGTH_SHORT).show();
//				allOff();			
				break;
			case R.id.buttonAllOn:
//				allOn();			
				break;
			case R.id.buttonAddGroup:
//				addGroupMessage();
				
				break;
			case R.id.ivRefresh:// 刷新
//				refreshDevices(true);
				break;
				
			case R.id.set_tv:// 设置
				Intent chipSelectIntent = new Intent(mActivity, ChipSelectActivity.class);
				startActivity(chipSelectIntent);
				break;
			case R.id.dynamic_gradient_iv: // 律动  渐变
//				setDynamicModel(128);
				break;
			case R.id.dynamic_breath_iv: // 渐变	呼吸
//				setDynamicModel(129);
				break;
			case R.id.dynamic_jump_iv: // 律动	跳变
//				setDynamicModel(130);
				break;
			case R.id.dynamic_strobe_iv: // 律动 	频闪
//				setDynamicModel(131);
				break;
				
			case R.id.rgb_sort_tv: // RGB 排序
				startActivity(new Intent(mActivity, CodeActivity.class));
				break;
			case R.id.timer_tv: //定时器
				startActivity(new Intent(mActivity, TimerActivity.class));
				break;
			case R.id.tv_channel_set: //颜色通道设置
				startActivity(new Intent(mActivity, ChannelSetActivity_Wifi.class));
				break;
			case R.id.tv_tb_check: //温度电池查询
				startActivity(new Intent(mActivity, TBQueryActivity.class));
				break;


			case R.id.operation_guide_tv: // 操作指南
				Intent intent = new Intent(mActivity, OprationManualActivity.class);
				startActivity(intent);
				break;
			case R.id.change_under_pic_tv: // 更换皮肤
				showPicturePicker(MainActivity_WiFi.this);
				break;
			case R.id.reset_tv: // 一键还原
				Drawable drawable = getResources().getDrawable(R.drawable.bg_all);
				imageView.setImageDrawable(drawable);

				String imagePath = getImagePath();
				imagePath = "";
				saveImagePathToSharedPreferences(imagePath);// 保存图片

				break;
			}
			
		}
	}
	

	// =============================================行为事件=====================================================// 
	

	// =============================================更换皮肤=====================================================//
	/**
	 * 更换皮肤相关 : 保存图片地址
	 */
	private void saveImagePathToSharedPreferences(String imagePath) {

		SharedPreferences sharedPreferences = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putString(Constant.IMAGE_VALUE, imagePath);
		editor.commit();
	}

	/**
	 * 更换皮肤相关 : 打开相册
	 */
	public void showPicturePicker(Context context) {

		if (Build.VERSION.SDK_INT >= 23) {			
			if (mActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // 申请授权
				mActivity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_CODE);					

            } else {
            	Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
        		openAlbumIntent.setType("image/*");
        		startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
            }			
		}else {
			Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
			openAlbumIntent.setType("image/*");
			startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
		}
		
//		Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
//		openAlbumIntent.setType("image/*");
//		startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
	}

	/**
	 * 更换皮肤相关 : 获取保存过的图片路径
	 */
	private String getImagePath() {
		sp = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
		editor = sp.edit();
		String imagePath = sp.getString(Constant.IMAGE_VALUE, "");

		return imagePath;
	}

	/**
	 * 更换皮肤相关 : 加载图片
	 */
	private void showImage(String imaePath) {

		if (null != bm && !bm.isRecycled()) {
			bm.recycle();
			bm = null;
			System.gc();
		}
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize=2;//图片高宽度都为原来的二分之一，即图片大小为原来的大小的四分之一  
		options.inTempStorage = new byte[5*1024]; //设置16MB的临时存储空间（不过作用还没看出来，待验证
		Bitmap bitMap = BitmapFactory.decodeFile(imaePath, options);
		imageView.setImageBitmap(bitMap);
		
	}
	
	/**
     * 根据图片的Uri获取图片的绝对路径(已经适配多种API)
     * @return 如果Uri对应的图片存在,那么返回该图片的绝对路径,否则返回null
     */
    public static String getRealPathFromUri(Context context, Uri uri) {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion < 11) {
            // SDK < Api11
            return getRealPathFromUri_BelowApi11(context, uri);
        }
        if (sdkVersion < 19) {
            // SDK > 11 && SDK < 19
            return getRealPathFromUri_Api11To18(context, uri);
        }
        // SDK > 19
        return getRealPathFromUri_AboveApi19(context, uri);
    }

    /**
     * 适配api19以上,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_AboveApi19(Context context, Uri uri) {
        String filePath = null;
        String wholeID = DocumentsContract.getDocumentId(uri);

        // 使用':'分割
        String id = wholeID.split(":")[1];

        String[] projection = { MediaStore.Images.Media.DATA };
        String selection = MediaStore.Images.Media._ID + "=?";
        String[] selectionArgs = { id };

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, null);
        int columnIndex = cursor.getColumnIndex(projection[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    /**
     * 适配api11-api18,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_Api11To18(Context context, Uri uri) {
        String filePath = null;
        String[] projection = { MediaStore.Images.Media.DATA };

        CursorLoader loader = new CursorLoader(context, uri, projection, null,
                null, null);
        Cursor cursor = loader.loadInBackground();

        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }

    /**
     * 适配api11以下(不包括api11),根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_BelowApi11(Context context, Uri uri) {
        String filePath = null;
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection,
                null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }
	
	

	// =============================================蓝牙操作=====================================================//
	

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		if (resultCode == Activity.RESULT_OK && requestCode == INT_GO_LIST) {
			
		} else {
			if (resultCode == RESULT_OK) {
				switch (requestCode) {
				case TAKE_PICTURE:
					break;
				case CHOOSE_PICTURE: // 更换皮肤
					ContentResolver resolver = getContentResolver();
					// 照片的原始资源地址
					if (null != data) {
						Uri originalUri = data.getData();
						if (null != originalUri) {
							try {
								
								if (null != bm && !bm.isRecycled()) { //回收图片资源
									bm.recycle();
									bm = null;
									System.gc();
								}
								
								// 使用ContentProvider通过URI获取原始图片		
								bm = MediaStore.Images.Media.getBitmap(resolver, originalUri);
								if (bm != null) {

									String img_path = getRealPathFromUri(mActivity, originalUri);// 这是本机的图片路径
									showImage(img_path);

									saveImagePathToSharedPreferences(img_path);// 保存图片
								}
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}				
					}
					
					
					break;

				default:
					break;
				}
			}

		}

	}
	

	// =========================================业务模式=================================================
	
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public boolean open() {		
		try {
			if (null != wifiConenction && wifiConenction.isOnLine()) {
				wifiConenction.open();
				return true;
			} else {
				onException(new Exception());
			}
		} catch (Exception e) {
			onException(e);
		}
		return false;
	}

	public boolean close() {
		try {
			if (null != wifiConenction && wifiConenction.isOnLine()) {
				wifiConenction.close();
				return true;
			} else {
				onException(new Exception());
			}
		} catch (Exception e) {
			onException(e);
		}
		return false;
	}
	
	public void turnOnOffTimerOn(int tomgdao, int hour, int minute, int model ) {
		if (null != wifiConenction) {
			if (islegal) {
				wifiConenction.turnOnOffTimerOn(tomgdao, hour, minute, model );
			}			
		}

	}
	
	public void turnOnOffTimerOff( int tomgdao, int hour, int minute, int model ) {
		if (null != wifiConenction) {
			if (islegal) {
				wifiConenction.turnOnOrOffTimerOff(tomgdao, hour, minute, model );
			}		
		}
	}

	public void setRgb(int r, int g, int b) {

		try {
			if (islegal) {
				wifiConenction.setRgb(r, g, b);
			}		
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setDim(int dim) {

		try {
			if (islegal) {
				wifiConenction.setDim(dim);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setDimModel(int dimModel) {

		try {
			if (islegal) {
				wifiConenction.setDimModel(dimModel);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setRegMode(int model) {

		try {
			if (islegal) {
				wifiConenction.setRgbMode(model);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setSpeed(int speed) {

		try {
			if (islegal) {
				wifiConenction.setSpeed(speed);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}
	
	public void setDiy(ArrayList<MyColor> colors, int style){

		try {
			if (islegal) {
				wifiConenction.setDiy(colors, style);
			}		
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setBrightNess(int brightness) {

		try {
			if (islegal) {
				wifiConenction.setBrightness(brightness);
			}			
		} catch (Exception e) {
			onException(e);
		}

	}

	public void setCT(int warm, int cool) {

		try {
			if (islegal) {
				wifiConenction.setColorWarm(warm, cool);
			}		
		} catch (Exception e) {
			onException(e);
		}
	}

	public void setCTModel(int model) {

		try {
			if (islegal) {
				wifiConenction.setColorWarmModel(model);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}
	
	public void setSmartBrightness(int brightness, int mode) {
		try {
			if (islegal) {
				wifiConenction.setSmartBrightness(brightness, mode);
			}		
		} catch (Exception e) {
			onException(e);
		}
	}
	
	// 查询
	public void setSmartCheck(int model) {
		try {
			if (islegal) {
				wifiConenction.setSmartCheck(model);
			}			
		} catch (Exception e) {
			onException(e);
		}
	}
	
	// 温度电池查询
	public ArrayList<String> setTBCheck(int model) {
		try {
			if (islegal && wifiConenction.isOnLine()) {
				return wifiConenction.setTBCheck(model);
			}
		} catch (Exception e) {			
			onException(e);
		}
		return null;
	}
	
	// 温度电池接收查询数据
	public ArrayList<String> TBCheckRecv() {
		try {
			if (islegal && wifiConenction.isOnLine()) {
				return wifiConenction.TBCheckRecv();
			}
		} catch (Exception e) {
			onException(e);
		}
		return null;
	}
	
	// 颜色通道设置
	public void SetCHN(int r, int g , int b, int w, int y, int p) {
		try {
			if (islegal && wifiConenction.isOnLine()) {
				wifiConenction.SetCHN(r, g, b, w, y, p);
			}		
		} catch (Exception e) {
			onException(e);
		}
	}

	@Override
	public void onException(Exception e) {

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		if (isBackground(mActivity)) { //app进入后台停止扫描
//			Toast.makeText(mActivity, "当前为后台", Toast.LENGTH_SHORT).show();
		}else {
			
//			Toast.makeText(mActivity, "当前为前台", Toast.LENGTH_SHORT).show();
		}
	}
	
	private ArrayList<String> ips;

	@Override
	protected void onResume() {
		super.onResume();
//		MobclickAgent.onResume(this);

		// ====检查并修复连接========
		
		checkConnect();

		if (ips == null || ips.isEmpty()) {

			// ======wifi状态改变=
			registerWifiChangeBoradCast();

		}
	}
	

	@Override
	protected void onPause() {
		super.onPause();
//		MobclickAgent.onPause(this);
		unregisterWifiChangeReceiver();
	}

	/**
	 * 检查并修复连接
	 */
	private void checkConnect() {

		conenctDevice(new ConnectDeviceListener() {
			@Override
			public void onConnect(boolean isSuccess) {
				if (!isSuccess) {
					showWifiListDialogAnimation();

				}
			}
		});
	}

	@Override
	public void onBackPressed() {
//		if (dialogView.getVisibility() == View.VISIBLE) {
//			hideWifiList();
//		} else {
//			closeSocket();// 必须手动关闭
//			finish();
//		}
//
//		return;
	}

//	private void closeSocket() {
//		if (null != wifiConenction) {
//			wifiConenction.closeSocket();
//		}
//	}

	/**
	 * 扫描WIFI
	 */
	public void scanWifi() {
		OpenWifi();
		wifiManager.startScan();
//		showProgressDialog();
	}


	private void registerWifiChangeBoradCast() {
		receiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// WIFI可用时连接到指定热点
				if (wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
					// unregisterWifiChangeReceiver();
//					handler.sendEmptyMessage(0);
				}
			}
		};

		IntentFilter filter = new IntentFilter(WifiManager.WIFI_STATE_CHANGED_ACTION);
		registerReceiver(receiver, filter);

	}

	private void unregisterWifiChangeReceiver() {
		if (receiver != null) {
			unregisterReceiver(receiver);
		}
	}

	public void showWifiListDialogAnimation() {
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
//		if (dialogView.getVisibility() == View.VISIBLE) {
//			return;
//		}

	}

	@Deprecated
	public void showWifiListDialogAnimation(final List<ScanResult> results) {
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
//		if (dialogView.getVisibility() == View.VISIBLE) {
//			return;
//		}
//		
//		dialogView.startAnimation(slideBottomIn);

	}


	public String[] getString(List<ScanResult> wifiList) {
		ArrayList<String> listStr = new ArrayList<String>();
		for (int i = 0; i < wifiList.size(); i++) {
			String SSID = wifiList.get(i).SSID;
			if (!StringUtils.isEmpty(SSID)) {
				listStr.add(SSID);
			}
		}
		return listStr.toArray(new String[0]);
	}

	/**
	 * 自动搜索处理WIFI扫描结果
	 * 
	 * @param results
	 */

	public void conenctDevice(final ConnectDeviceListener connectDeviceListener) {
		if (null != baseTask && !baseTask.isCancelled()) {
			baseTask.cancel(true);
		}
		baseTask = new BaseTask(new NetCallBack() {
			@Override
			public void onPreCall() {
//				showProgressDialogWithTask(baseTask);
			}

			@Override
			public void onFinish(NetResult result) {
//				hideProgressDialogWithTask();
				if (!isUDP) {
					if (null != result) {
									
						try {
							//验证模块合法性
//							verifyModuleValidity();
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if (isFirstOpen) {
							Tool.ToastShow(mActivity, R.string.connect_success);
							isFirstOpen = false;
						}
						
						// 连接成功后，打开灯，并且设置亮度最大
//						open();
						// setBrightNess(100);					
						

					} else {
						
						Tool.ToastShow(mActivity, R.string.can_not_connect);
						
					}
				}
				if (null != connectDeviceListener) {
					connectDeviceListener.onConnect((null == result) ? false : true);
				}
			}

			@Override
			public NetResult onDoInBack(HashMap<String, String> paramMap) {
				try {
					if (wifiConenction == null) {
						wifiConenction = new WifiConenction(mActivity, isUDP, ips);
					}

					if (wifiConenction.connect()) {
//						LogUtil.i(App.tag, "连接成功！");
						return new NetResult();
					} else {
//						LogUtil.e(App.tag, "连接失败！");
					}
				} catch (Exception e) {
//					LogUtil.e(App.tag, "连接失败！");
					e.printStackTrace();
				}
				return null;
			}
		});
		baseTask.execute(new HashMap<String, String>());

	}
	
	/**
	 * 验证模块合法性
	 * @throws Exception 
	 */
	public void verifyModuleValidity() throws Exception {
		
		// 1、发送验证，获取返回值
		String string = sendVerification();
//		Toast.makeText(mActivity, "返回值："+string, Toast.LENGTH_SHORT).show();
		
		// 2、计算本地加密后的值
		String string1 = calculateEncryptedValue();
//		Toast.makeText(mActivity, "本地值："+string1, Toast.LENGTH_SHORT).show();
		
		// 3、判断 返回值 跟 本地计算的值是否一致 ，一致就证明模块合法，不一致就是非法模块
		if (string.equalsIgnoreCase(string1)) {
			islegal = true;
			Toast.makeText(mActivity, "合法模块", Toast.LENGTH_SHORT).show();
		}else {
			islegal = false;
			Toast.makeText(mActivity, "非法模块", Toast.LENGTH_SHORT).show();
			return;
		}
		
	}
	
	/**
	 * 发送验证，获取返回值 
	 * @throws IOException 
	 */
	public String sendVerification() throws IOException {
		rand = new Random();
		int randInt = rand.nextInt(1000);
		randStr = String.valueOf(randInt);
		String ascii = convertStringToHex(randStr);
		
		String string = receiveDataReturned(ascii); // 查询 带返回
		String[] strArray = string.split("[=]");
		String string1 = strArray[2];
		String[] strArray1 = string1.split("[\r]");
		String string2 = strArray1[0];	
	
		return string2;
	}
	
	// 接收数据返回
	public String receiveDataReturned(String hexStr) throws IOException {

		InetAddress address = InetAddress.getByName("192.168.2.3");
		int port = 25000;
		String dataStr = "xcmd_req::cmd=encrypt,key=1,data=" + hexStr + "\r\n";
		byte[] data = dataStr.getBytes();
		DatagramPacket packet = new DatagramPacket(data, data.length, address, port);
		DatagramSocket udpSocket = new DatagramSocket();
		udpSocket.send(packet);

		byte[] data2 = new byte[1024];
		DatagramPacket packet2 = new DatagramPacket(data2, data2.length);
		udpSocket.setSoTimeout(2000);
		udpSocket.receive(packet2);

		String reply = new String(data2, 0, packet2.getLength());
		udpSocket.close();
		return reply;
	}
	
	/**
	 * 计算本地加密后的值 
	 */
	public String calculateEncryptedValue() throws Exception {		
 
        byte[] raw = encrypteKey.getBytes();
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        IvParameterSpec iv = new IvParameterSpec(encrypteKey.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

        byte[] cipherText = cipher.doFinal(randStr.getBytes());
        return BytetohexString(cipherText);
	}
	
	public String convertStringToHex(String str) {

		char[] chars = str.toCharArray();
		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}

		return hex.toString();

	}
	
	// 字节数组按照一定格式转换拼装成字符串用于打印显示
	private static String BytetohexString(byte[] b) {
		int len = b.length;
		StringBuilder sb = new StringBuilder(b.length * (2 + 1));
		Formatter formatter = new Formatter(sb);

		for (int i = 0; i < len; i++) {
			if (i < len - 1)
				formatter.format("%02X", b[i]);
			else
				formatter.format("%02X", b[i]);

		}
		formatter.close();

		return sb.toString();
	}

	/**
	 * 打开WIFI
	 */
	public void OpenWifi() {
		if (!wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(true);
		}
	}

	/**
	 * 关闭WIFI
	 */
	public void CloseWifi() {
		if (wifiManager.isWifiEnabled()) {
			wifiManager.setWifiEnabled(false);
		}
	}

	/**
	 * 判断app是否进入后台
	 */
	public static boolean isBackground(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
					
					return true;
				} else {			
					return false;
				}
			}
		}
		return false;
	}

	public SegmentedRadioGroup getSegmentDm() {
		return segmentDm;
	}

	public SegmentedRadioGroup getSegmentCt() {
		return segmentCt;
	}

	public SegmentedRadioGroup getSegmentRgb() {
		return segmentRgb;
	}

//	public SegmentedRadioGroup getSegmentMusic() {
//		return segmentMusic;
//	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

}
