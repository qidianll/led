package com.ledble.activity.wifi;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import com.common.BaseActivity;
import com.ledlamp.R;

public class TBQueryActivity extends BaseActivity {

	private TextView tvTemperature;
	private TextView tvBattery;
	
	private ArrayList<String> dataList;
	
	private boolean canSend = true;
	
//	private boolean 
	
	public static final String LOCALHOST = "192.168.2.3";

	private final int MSG_START_CONNECT = 10000; // 开始连接
	private final int CHECK_NOW = 10001;         // 正在查询

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
//		mActivity = (MainActivity) getActivity();
		
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_tb_query);
		
		this.tvTemperature = (TextView) findViewById(R.id.tvTemperature);
		this.tvBattery = (TextView) findViewById(R.id.tvBattery);
		
		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.tvQuery: {										
										
					
					new Thread(new Runnable() {
						@Override
						public void run() {
							
							conectHandler.sendEmptyMessage(CHECK_NOW);// 正在查询
							
							if (null != MainActivity_WiFi.getMainActivity()) {
								MainActivity_WiFi.getMainActivity().setTBCheck(1); // 查询 带返回							
								if (canSend) {
									canSend = false;								
									dataList = MainActivity_WiFi.getMainActivity().TBCheckRecv(); // 查询 带返回							
									conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了								
								}
							}																																		
							
						}
					}).start();
																			
				}
					break;
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.buttonSave: {
//					putDataback();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
		findTextViewById(R.id.tvQuery).setOnClickListener(clickListener);

	}
	
	private Handler conectHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case CHECK_NOW:// 正在查询	
				
				Toast.makeText(getApplicationContext(), ""+getString(R.string.currentquery), Toast.LENGTH_SHORT).show();											
//				refresh();
				
				break;
			case MSG_START_CONNECT:// 可以开始连接了											
				
				if (dataList != null) {
					if (dataList.size() >= 2) {				
						tvTemperature.setText(dataList.get(0));
						tvBattery.setText(dataList.get(1));					
						
						Toast.makeText(getApplicationContext(), "查询结果："+dataList, Toast.LENGTH_SHORT).show();
						
						canSend = true;
					}
				}				
				
				break;

			default:
				break;
			}
		};
	};
	
	
	/**
	 *  RGB间隔发送
	 */
	protected void refresh() {
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				mHandler.postDelayed(this, 3000);// 延时时长
				canSend = true;
			}
		};
		mHandler.postDelayed(mRunnable, 3000);// 打开定时器，执行操作		

	}
	
}
