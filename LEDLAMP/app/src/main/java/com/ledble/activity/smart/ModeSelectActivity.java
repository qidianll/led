package com.ledble.activity.smart;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.view.TextViewBorder;
import com.ledble.constant.CommonConstant;
import com.ledble.net.NetConnectBle;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.ledlamp.R;
import com.umeng.analytics.MobclickAgent;

public class ModeSelectActivity extends BaseActivity {
	
//	private WheelView listViewH;
//	private WheelView listViewM;
	
	private TextViewBorder textViewModel;
	private WheelView listViewModel;

//	private WheelModelAdapter wheelAdapterH;
//	private WheelModelAdapter wheelAdapterM;
	private WheelModelAdapter wheelAdapterModel;

//	private String tag = "";
//	private View linearLayoutContainer;

//	private int hour /*= 12*/;
//	private int minite /*= 30*/;
	private int model /*= 10*/;
	
	
//	private String modelText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_rgb_sort);
		

		this.textViewModel = (TextViewBorder) findViewById(R.id.textViewModel);
		
		this.listViewModel = (WheelView) findViewById(R.id.listViewModel);
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		final String[] modelStrings = new String[timerModel.length];
		for (int i = 0; i < timerModel.length; i++) {
			String[] datas = timerModel[i].split(",");
			modelStrings[i] = datas[0];
		}
		
		wheelAdapterModel = new WheelModelAdapter(this, modelStrings);
		this.listViewModel.setViewAdapter(wheelAdapterModel);
		
		this.listViewModel.setCurrentItem(SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART));
		String[] datas = timerModel[SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART)].split(",");
		textViewModel.setText(getResources().getString(R.string.rgb_sort_set, datas[0]));
		
		this.listViewModel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = timerModel[newValue].split(",");
				model = Integer.parseInt(datas[1]);
				textViewModel.setText(getResources().getString(R.string.rgb_sort_set, datas[0]));
				SharePersistent.savePerference(getApplicationContext(), CommonConstant.SELECT_MODE_SMART, model-1);
				
				Toast.makeText(getApplicationContext(), ""+datas[0], Toast.LENGTH_SHORT).show();
//				modelText = datas[0];
//				textViewSpeed.setText(mActivity.getResources().getString(R.string.speed_set, 1));
			}
		});
		
		TextView textViewOKButton = (TextView) findViewById(R.id.textViewOKButton);
		textViewOKButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				putDataback();
//				finish();
				
//				Intent data = new Intent();
//				data.putExtra("group", "group");
//				setResult(Activity.RESULT_OK, data);
				setResult(Activity.RESULT_OK);
				finish();
			}
		});
		
		Button buttonCancell = (Button) findViewById(R.id.buttonCancell);
		buttonCancell.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				putDataback();
//				finish();
			}
		});

	}

	private void putDataback() {

		Intent intent = new Intent();
		intent.putExtra("hour", "hour");
		setResult(Activity.RESULT_OK, intent);
//		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onPause(this);
	}
}
