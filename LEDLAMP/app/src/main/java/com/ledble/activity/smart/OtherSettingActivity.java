package com.ledble.activity.smart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.ledble.net.NetConnectBle;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.ledlamp.R;

public class OtherSettingActivity extends BaseActivity {

	private WheelView listViewH;
	private WheelView listViewM;
	private WheelView listViewS;
	private LinearLayout llTextViewBorder;
//	private WheelView listViewLightblue;
	private WheelView listViewRed;
	private TextView tvFan;
	private TextView textViewOKButton;

	private WheelModelAdapter wheelAdapterH;
	private WheelModelAdapter wheelAdapterM;
	private WheelModelAdapter wheelAdapterS;
	private WheelModelAdapter wheelAdapterModel;
	
	
	private String tag = "";
	private View linearLayoutContainer;

	private int hour ;
	private int minute ;
	private int second ;
	private int model /*= 10*/;
	
	private int whiteValue;
	private int lightblueValue ;
	private int redValue ;
	private int greenValue;
	private int pinkValue ;
	private int crystalValue ;
	
	private String modelText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_other_setting);
	
		if (null != getIntent()) {
			tag = getIntent().getStringExtra("tag");
		} else {
			finish();
		}

		
		Time t=new Time(); // or Time t=new Time("GMT+8"); 鍔犱笂Time Zone璧勬枡  
		t.setToNow(); // 鍙栧緱绯荤粺鏃堕棿銆? 
//		int year = t.year;  
//		int month = t.month;  
//		int date = t.monthDay;  
		hour = t.hour;   
		minute = t.minute;   
		second = t.second;
		
		this.listViewH = (WheelView) findViewById(R.id.listViewH);
		this.listViewM = (WheelView) findViewById(R.id.listViewM);
		this.listViewS = (WheelView) findViewById(R.id.listViewS);
		this.llTextViewBorder = (LinearLayout) findViewById(R.id.llTextViewBorder);
		
		this.listViewRed = (WheelView) findViewById(R.id.listViewModel1);
		this.tvFan = (TextView) findViewById(R.id.fan_rotational_temperature_tv);
		
		
		// ================
		
		String[] modelH = new String[24];
		for (int i = 0; i < 24; i++) {
			modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterH = new WheelModelAdapter(this, modelH);
		this.listViewH.setViewAdapter(wheelAdapterH);
		this.listViewH.setCurrentItem(hour);
		this.listViewH.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				hour = newValue;
//				NetConnectBle.getInstance().setSmartTimeSet(hour, minute, second);
			}
		});

		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterM = new WheelModelAdapter(this, modelM);
		this.listViewM.setViewAdapter(wheelAdapterM);
		this.listViewM.setCurrentItem(minute);
		// this.listViewModel.setCyclic(true);
		this.listViewM.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				minute = newValue;
//				NetConnectBle.getInstance().setSmartTimeSet(hour, minute, second);
			}
		});
		
		String[] modelS = new String[60];
		for (int i = 0; i < 60; i++) {
			modelS[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterS = new WheelModelAdapter(this, modelS);
		this.listViewS.setViewAdapter(wheelAdapterS);
		this.listViewS.setCurrentItem(second);
		// this.listViewModel.setCyclic(true);
		this.listViewS.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				second = newValue;
//				NetConnectBle.getInstance().setSmartTimeSet(hour, minute, second);
			}
		});


		
		final String[] timerModel = new String[101];
		for (int i = 0; i <= 100; i++) {
			timerModel[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterModel = new WheelModelAdapter(this, timerModel);

		this.listViewRed.setViewAdapter(wheelAdapterModel);
		this.listViewRed.setCurrentItem(0);
		this.listViewRed.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				redValue = newValue;
//				NetConnectBle.getInstance().setSmartFanSet(redValue);
			}
		});

		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.buttonSave: {
					putDataback();
				}
				case R.id.textViewOKButton: {
					if ("time".equalsIgnoreCase(tag) ) {
						NetConnectBle.getInstance().setSmartTimeSet(hour, minute, second);
					}else if ("fan".equalsIgnoreCase(tag)) {
						NetConnectBle.getInstance().setSmartFanSet(redValue);
					}
					finish();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
		findViewById(R.id.textViewOKButton).setOnClickListener(clickListener);

		
		this.linearLayoutContainer = findViewById(R.id.linearLayoutContainer);
		if ("time".equalsIgnoreCase(tag)) {
			this.listViewRed.setVisibility(View.INVISIBLE);
			this.tvFan.setVisibility(View.INVISIBLE);
		}else if ("fan".equalsIgnoreCase(tag)) {
			this.listViewH.setVisibility(View.INVISIBLE);
			this.listViewM.setVisibility(View.INVISIBLE);
			this.listViewS.setVisibility(View.INVISIBLE);
			this.llTextViewBorder.setVisibility(View.INVISIBLE);
		}
		

	}

	private void putDataback() {

		Intent intent = new Intent();
		intent.putExtra("hour", hour);
		intent.putExtra("minite", minute);
		intent.putExtra("model", model);
		intent.putExtra("modelText", modelText);
		intent.putExtra("whiteValue", whiteValue);
		intent.putExtra("lightblueValue", lightblueValue);
		intent.putExtra("redValue", redValue);
		intent.putExtra("greenValue", greenValue);
		intent.putExtra("pinkValue", pinkValue);
		intent.putExtra("crystalValue", crystalValue);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onResume(this);
	}
}
