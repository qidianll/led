package com.ledble.activity;

import android.R.integer;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.view.TextViewBorder;
import com.ledlamp.R;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.net.NetConnectBle;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.umeng.analytics.MobclickAgent;

public class PairCodeActivity extends BaseActivity {
	
//	private WheelView listViewH;
//	private WheelView listViewM;
	
	private TextViewBorder textViewModel;
	private WheelView listViewModel;

//	private WheelModelAdapter wheelAdapterH;
//	private WheelModelAdapter wheelAdapterM;
	private WheelModelAdapter wheelAdapterModel;

//	private String tag = "";
//	private View linearLayoutContainer;

//	private int hour /*= 12*/;
//	private int minite /*= 30*/;
	private int model /*= 10*/;
	
	
//	private String modelText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_pair_code);
		

		this.textViewModel = (TextViewBorder) findViewById(R.id.textViewModel);
		
		this.listViewModel = (WheelView) findViewById(R.id.listViewModel);
//		final String[] timerModel = getResources().getStringArray(R.array.rgb_sort_ble);
//		final String[] modelStrings = new String[timerModel.length];
//		for (int i = 0; i < timerModel.length; i++) {
//			String[] datas = timerModel[i].split(",");
//			modelStrings[i] = datas[0];
//		}
		
		String[] modelStrings = new String[256];
		for (int i = 0; i < 256; i++) {
			modelStrings[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}

		
		wheelAdapterModel = new WheelModelAdapter(this, modelStrings);
		this.listViewModel.setViewAdapter(wheelAdapterModel);
		
		this.listViewModel.setCurrentItem(SharePersistent.getInt(getApplicationContext(), "PAIR-CODE-BLE"));
//		String[] datas = timerModel[SharePersistent.getInt(getApplicationContext(), "RGB-SORT")].split(",");
//		textViewModel.setText(getResources().getString(R.string.rgb_sort_set, datas[0]));
		
		this.listViewModel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
//				String[] datas = timerModel[newValue].split(",");
				model = newValue;
				textViewModel.setText(getResources().getString(R.string.pair_code_set, newValue));
				SharePersistent.savePerference(getApplicationContext(), "PAIR-CODE-BLE", model-1);
//				modelText = datas[0];
//				textViewSpeed.setText(mActivity.getResources().getString(R.string.speed_set, 1));
			}
		});

		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.textViewOKButton: {
					
					MainActivity_BLE.getMainActivity().setPairCode(model);
//					finish();
					putDataback();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findTextViewById(R.id.textViewOKButton).setOnClickListener(clickListener);
	}

	private void putDataback() {

		Intent intent = new Intent();
//		intent.putExtra("hour", hour);
//		intent.putExtra("minite", minite);
//		intent.putExtra("model", model);
//		intent.putExtra("modelText", modelText);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}
}
