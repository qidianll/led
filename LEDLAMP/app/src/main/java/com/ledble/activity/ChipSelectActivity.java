package com.ledble.activity;

//import android.R.string;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import com.common.uitl.SharePersistent;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import butterknife.Bind;

//import java.lang.reflect.Array;
import java.util.ArrayList;
//import java.util.Arrays;

import com.common.BaseActivity;
//import com.common.uitl.Constant;
import com.common.uitl.StringUtils;
import com.common.adapter.ChipSelectAdapter;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.common.adapter.RGBSelectAdapter;
import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.ble.MainActivity_Strip;
import com.ledble.bean.AdapterBean;
//import com.ledble.db.GroupDeviceDao;
import com.ledlamp.R;

public class ChipSelectActivity extends BaseActivity {

	private Button buttonChipselect;
	private Context mContext;
	private Button backButton;

	private ListView mLeftLv/*, mRightLv*/;

	// private MainActivity mActivity;
	// private ModelAdapter maAdapter;
	private RGBSelectAdapter rgbAdapter;
	private ChipSelectAdapter chipAdapter;
	private Button setPixBtn;
	private Button pixNumber;
//	private TextView chipTvResult;
	private TextView rgbTvResult;
	private TextView pixTvResult;
//	private Button chipselectOKBtn;
	private SharedPreferences sp;
	private int bannerType = 4;
	private int bannerSort;
	private int bannerPix;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_chip_select);
		// mActivity = (MainActivity) getActivity();

		mLeftLv = (ListView) findViewById(R.id.lv_left);
//		mRightLv = (ListView) findViewById(R.id.lv_right);

//		mRightLv.setAdapter(buildChipModel());
//		mRightLv.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				chipAdapter.setIndex(position);
//				chipAdapter.notifyDataSetChanged();
//				bannerType = position;
//				AdapterBean abean = ((AdapterBean) chipAdapter.getItem(position));
//				Toast.makeText(getApplicationContext(),
//						mContext.getResources().getString(R.string.current_chip_format, abean.getLabel()),
//						Toast.LENGTH_SHORT).show();
//				chipTvResult.setText(abean.getLabel());
//
//				// saveData(Constant.CHIP_TYPE, Constant.CHIP_TYPE_KEY,
//				// abean.getLabel(), position); // 保存选择的chip类型
//				SharePersistent.saveConfigData(mContext, Constant.CHIP_TYPE, Constant.CHIP_TYPE_KEY, abean.getLabel(),
//						position);// 保存选择的chip类型
//			}
//		});

		mLeftLv.setAdapter(buildRGBModel());
		mLeftLv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				rgbAdapter.setIndex(position);
				rgbAdapter.notifyDataSetChanged();
				
				AdapterBean abean = ((AdapterBean) rgbAdapter.getItem(position));
//				 String value = abean.getValue();
				bannerSort = Integer.parseInt(abean.getValue().replaceAll(" ",""));
				Toast.makeText(getApplicationContext(), mContext.getResources().getString(R.string.current_rgb_format, abean.getLabel()), Toast.LENGTH_SHORT).show();
				rgbTvResult.setText(abean.getLabel());

				// saveData(Constant.RGB_SORT_TYPE, Constant.RGB_SORT_TYPE_KEY,
				// abean.getLabel(), position); // 保存选择的RGB排序类型
				if (null != MainActivity_BLE.getMainActivity()) {
					SharePersistent.saveConfigData(mContext, Constant.RGB_SORT_TYPE, Constant.RGB_SORT_TYPE_KEY,
							abean.getLabel(), position);// 保存选择的RGB排序类型
				}else if (null != MainActivity_Strip.getMainActivity()) {
					SharePersistent.saveConfigData(mContext, Constant.RGB_SORT_TYPE_SPI, Constant.RGB_SORT_TYPE_KEY_SPI,
							abean.getLabel(), position);// 保存选择的RGB排序类型
				} 
				
			}
		});

		// 返回按钮
		backButton = findButtonById(R.id.buttonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix,bannerSort);
				finish();
			}
		});
		
		// ok按钮
		this.buttonChipselect = findButtonById(R.id.button_chipselect_ok);
		this.buttonChipselect.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (null != MainActivity_BLE.getMainActivity()) {
					
					NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix, bannerSort);
					
				}else if (null != MainActivity_Strip.getMainActivity()) {
					MainActivity_Strip.getMainActivity().setConfigSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix, bannerSort);
				}
				
				finish();
			}
		});

		// 设置像素点按钮
		setPixBtn = findButtonById(R.id.btn_pix_set);
		setPixBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue();
			}
		});
		pixNumber = findButtonById(R.id.tv_pix_num);
		pixNumber.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setPixValue();
			}
		});

		// 选择结果 展示
//		chipTvResult = findTextViewById(R.id.tv_chip_result);
		rgbTvResult = findTextViewById(R.id.tv_rgb_result);
		pixTvResult = findTextViewById(R.id.tv_pix_result);

		// 取出RGB值并赋值
		if (mLeftLv != null && rgbTvResult != null) {

			String value = null;
			if (null != MainActivity_BLE.getMainActivity()) {
				value = SharePersistent.getConfigData(mContext, Constant.RGB_SORT_TYPE_SPI, Constant.RGB_SORT_TYPE_KEY_SPI);
			}else if (null != MainActivity_Strip.getMainActivity()) {
				value = SharePersistent.getConfigData(mContext, Constant.RGB_SORT_TYPE_SPI, Constant.RGB_SORT_TYPE_KEY_SPI);
			}
			
			if (value == null || value.length() <= 0) {// 判断取出的数据是否非空
				bannerSort = 3;
				pixNumber.setText("200");
				pixTvResult.setText("200");
				rgbAdapter.setIndex(2);
				rgbAdapter.notifyDataSetChanged();
				mLeftLv.setSelection(2);
				rgbTvResult.setText("GRB");
//				return;
			}else {
				String[] tempData = null;
				tempData = value.split(",");
				rgbTvResult.setText(tempData[0]);
				bannerSort = Integer.parseInt(tempData[1]) + 1;
		
				rgbAdapter.setIndex(Integer.parseInt(tempData[1]));
				rgbAdapter.notifyDataSetChanged();
				mLeftLv.setSelection(Integer.parseInt(tempData[1]));
			}
						
		}
		
		

		// 取出Chip值并赋值
//		if (mRightLv != null && chipTvResult != null) {
//
//			// String value = getData(Constant.RGB_SORT_TYPE,
//			// Constant.RGB_SORT_TYPE_KEY);
//			String value = SharePersistent.getConfigData(mContext, Constant.CHIP_TYPE, Constant.CHIP_TYPE_KEY);
//			if (value == null || value.length() <= 0) { // 判断取出的数据是否非空
//				return;
//			}
//
//			String[] tempData = null;
//			tempData = value.split(",");
//			chipTvResult.setText(tempData[0]);
//			bannerType = Integer.parseInt(tempData[1]);
//			chipAdapter.setIndex(Integer.parseInt(tempData[1]));
//			chipAdapter.notifyDataSetChanged();
//			mRightLv.setSelection(Integer.parseInt(tempData[1]));
//
//		}

		// 取出Pix值并赋值
		if (pixTvResult != null) {
			// String value = getData(Constant.PIX_VALUE,
			// Constant.PIX_VALUE_KEY);
			
			String value = null;
			if (null != MainActivity_BLE.getMainActivity()) {
				
				value = SharePersistent.getConfigData(mContext, Constant.PIX_VALUE, Constant.PIX_VALUE_KEY);
								
			}else if (null != MainActivity_Strip.getMainActivity()) {
				
				value = SharePersistent.getConfigData(mContext, Constant.PIX_VALUE_SPI, Constant.PIX_VALUE_KEY_SPI);
				
			}

			if (value == null || value.length() <= 0) { // 判断取出的数据是否非空
				bannerPix = 200;
//				return;
			}else {
				String[] tempData = null;
				tempData = value.split(",");
				pixNumber.setText(tempData[0]);
				pixTvResult.setText(tempData[0]);
				bannerPix = Integer.parseInt(tempData[0]);
			}
			
		}

		// 芯片选择OK按钮
//		chipselectOKBtn = findButtonById(R.id.btn_chipselect_ok);
//		chipselectOKBtn.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix,
//						bannerSort);
//				finish();
//			}
//		});
	}

	private RGBSelectAdapter buildRGBModel() {
		String[] ary = mContext.getResources().getStringArray(R.array.ble_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		rgbAdapter = new RGBSelectAdapter(mContext, abs);
		return rgbAdapter;
	}

	private ChipSelectAdapter buildChipModel() {
		String[] ary = mContext.getResources().getStringArray(R.array.light_banner);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		chipAdapter = new ChipSelectAdapter(mContext, abs);
		return chipAdapter;
	}

	// 设置像素值
	private void setPixValue() {

		final EditText editText = new EditText(mContext);
		new AlertDialog.Builder(mContext).setTitle(R.string.btn_pix_number).setIcon(android.R.drawable.ic_dialog_info)
				.setView(editText).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						String gnameString = editText.getText().toString();
						int value = Integer.parseInt(gnameString);
						
						if (value <= 0) {
							Toast.makeText(mContext, R.string.valuetip, Toast.LENGTH_SHORT).show();
							return;
						}
						
						String valueStr = String.valueOf(value);
						if (!StringUtils.isEmpty(gnameString)) {
							// addGroupByName(gnameString);
							bannerPix = Integer.parseInt(valueStr);
							pixNumber.setText(valueStr);
							pixTvResult.setText(valueStr);

							// saveData(Constant.PIX_VALUE,
							// Constant.PIX_VALUE_KEY, gnameString, 0); // 保存像素值
							
							
							if (null != MainActivity_BLE.getMainActivity()) {
								
								SharePersistent.saveConfigData(mContext, Constant.PIX_VALUE, Constant.PIX_VALUE_KEY,
										valueStr, 0);// 保存像素值
												
							}else if (null != MainActivity_Strip.getMainActivity()) {
								
								SharePersistent.saveConfigData(mContext, Constant.PIX_VALUE_SPI, Constant.PIX_VALUE_KEY_SPI,
										valueStr, 0);// 保存像素值
								
							}
						}
					}
				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}

}
