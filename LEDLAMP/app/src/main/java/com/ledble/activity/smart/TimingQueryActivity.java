package com.ledble.activity.smart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import java.util.ArrayList;
import java.util.List;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.view.SegmentedRadioGroup;
import com.ledble.constant.CommonConstant;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.ledlamp.R;


public class TimingQueryActivity extends BaseActivity {
	
	private LinearLayout linearLayoutTimer1;
	@Bind(R.id.linearLayoutTimerLine1) LinearLayout linearLayoutTimerLine1;
	@Bind(R.id.linearLayoutTimerLine2) LinearLayout linearLayoutTimerLine2;
	@Bind(R.id.linearLayoutTimerLine3) LinearLayout linearLayoutTimerLine3;
	@Bind(R.id.linearLayoutTimerLine4) LinearLayout linearLayoutTimerLine4;
	@Bind(R.id.linearLayoutTimerLine5) LinearLayout linearLayoutTimerLine5;
	@Bind(R.id.linearLayoutTimerLine6) LinearLayout linearLayoutTimerLine6;
	@Bind(R.id.linearLayoutTimerLine7) LinearLayout linearLayoutTimerLine7;
	@Bind(R.id.linearLayoutTimerLine8) LinearLayout linearLayoutTimerLine8;
	@Bind(R.id.linearLayoutTimerLine9) LinearLayout linearLayoutTimerLine9;
	@Bind(R.id.linearLayoutTimerLine10) LinearLayout linearLayoutTimerLine10;
//	@Bind(R.id.linearLayoutTimer) LinearLayout linearLayoutTimer;
	@Bind(R.id.changeButton1) SegmentedRadioGroup changeRadioGroup1;
	@Bind(R.id.tvOK1) TextView tvOK1;
	
	private LinearLayout linearLayoutTimer2;
	@Bind(R.id.linearLayoutTimerLine21) LinearLayout linearLayoutTimerLine21;
	@Bind(R.id.linearLayoutTimerLine22) LinearLayout linearLayoutTimerLine22;
	@Bind(R.id.linearLayoutTimerLine23) LinearLayout linearLayoutTimerLine23;
	@Bind(R.id.linearLayoutTimerLine24) LinearLayout linearLayoutTimerLine24;
	@Bind(R.id.linearLayoutTimerLine25) LinearLayout linearLayoutTimerLine25;
	@Bind(R.id.linearLayoutTimerLine26) LinearLayout linearLayoutTimerLine26;
	@Bind(R.id.linearLayoutTimerLine27) LinearLayout linearLayoutTimerLine27;
	@Bind(R.id.linearLayoutTimerLine28) LinearLayout linearLayoutTimerLine28;
	@Bind(R.id.linearLayoutTimerLine29) LinearLayout linearLayoutTimerLine29;
	@Bind(R.id.linearLayoutTimerLine210) LinearLayout linearLayoutTimerLine210;
	@Bind(R.id.changeButton2) SegmentedRadioGroup changeRadioGroup2;
	@Bind(R.id.tvOK2) TextView tvOK2;
	
	private LinearLayout linearLayoutTimer3;
	@Bind(R.id.linearLayoutTimerLine31) LinearLayout linearLayoutTimerLine31;
	@Bind(R.id.linearLayoutTimerLine32) LinearLayout linearLayoutTimerLine32;
	@Bind(R.id.linearLayoutTimerLine33) LinearLayout linearLayoutTimerLine33;
	@Bind(R.id.linearLayoutTimerLine34) LinearLayout linearLayoutTimerLine34;
	@Bind(R.id.linearLayoutTimerLine35) LinearLayout linearLayoutTimerLine35;
	@Bind(R.id.linearLayoutTimerLine36) LinearLayout linearLayoutTimerLine36;
	@Bind(R.id.linearLayoutTimerLine37) LinearLayout linearLayoutTimerLine37;
	@Bind(R.id.linearLayoutTimerLine38) LinearLayout linearLayoutTimerLine38;
	@Bind(R.id.linearLayoutTimerLine39) LinearLayout linearLayoutTimerLine39;
	@Bind(R.id.linearLayoutTimerLine310) LinearLayout linearLayoutTimerLine310;
//	@Bind(R.id.linearLayoutTimer) LinearLayout linearLayoutTimer;
	@Bind(R.id.changeButton3) SegmentedRadioGroup changeRadioGroup3;
	@Bind(R.id.tvOK3) TextView tvOK3;
	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";
	
	public boolean isCanSend = true;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
		setActive();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_timing_query);
		
		this.linearLayoutTimer1 = findLinearLayout(R.id.linearLayoutTimer1);
		this.linearLayoutTimer2 = findLinearLayout(R.id.linearLayoutTimer2);
		this.linearLayoutTimer3 = findLinearLayout(R.id.linearLayoutTimer3);

		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.tvQuery: {
					
					if (isCanSend) {
						NetConnectBle.getInstance().setSmartCheck(1);
						Toast.makeText(getApplicationContext(), getString(R.string.timingquery), Toast.LENGTH_SHORT).show();
						timingQuery();
						isCanSend = false;
					}
								
									
				}
					break;
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.buttonSave: {
					putDataback();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
		findTextViewById(R.id.tvQuery).setOnClickListener(clickListener);
		
	}
	
	public void setActive() {
		
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART)].split(",");	
		
//		Toast.makeText(mActivity, ""+datas[0], Toast.LENGTH_SHORT).show();
		if (datas[0].equalsIgnoreCase(RGB)) {
			this.linearLayoutTimer1.setVisibility(View.VISIBLE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBW)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.VISIBLE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.VISIBLE);
		}
//		initViews();
	}
	
	private void timingQuery() {
		
		final Handler handler = new Handler(); // 
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				
				String data = SharePersistent.getPerference(getApplicationContext(), Constant.TimingQueryActivity);
//				Toast.makeText(getApplicationContext(), "data = "+data, Toast.LENGTH_SHORT).show();

				
				if (data.length() <= 0) {
					return;
				}
				
				String[] strArray = data.split("AF");
				String[] dataArray = strArray[1].split("7B");
				
				if (dataArray.length <= 1) {
					return;
				}
				
				
				final String[] timerModel = getResources().getStringArray(R.array.select_mode);
				String[] datas = timerModel[SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART)].split(",");
				
				if (datas[0].equalsIgnoreCase(RGB)) {
					for (int i = 1; i <= dataArray.length; i++) {		
						
						if (i == 1) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime1);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line1);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line1);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line1);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line1);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));						

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line1);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line1);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));												
							
						} 
						else if (i == 2) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime2);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line2);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line2);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line2);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line2);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line2);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line2);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));						
							
						}
						else if (i == 3) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime3);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line3);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line3);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line3);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));											

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line3);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line3);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line3);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
				
						} 
						else if (i == 4) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime4);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line4);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line4);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line4);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
						
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line4);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line4);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line4);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 5) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime5);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line5);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line5);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line5);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line5);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line5);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line5);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 6) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime6);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line6);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line6);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line6);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line6);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line6);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line6);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 7) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime7);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line7);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line7);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line7);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line7);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line7);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line7);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 8) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime8);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line8);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line8);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line8);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
												
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line8);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line8);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line8);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 9) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime9);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line9);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line9);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line9);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line9);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line9);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line9);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 10) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime10);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line10);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line10);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line10);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line10);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line10);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line10);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
					}
					
				}else if (datas[0].equalsIgnoreCase(RGBW)) {
					for (int i = 1; i <= dataArray.length; i++) {		
						
						if (i == 1) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime21);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line21);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line21);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line21);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line21);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));						

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line21);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line21);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));												
							
						} 
						else if (i == 2) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime22);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line22);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line22);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line22);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line22);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line22);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line22);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));						
							
						}
						else if (i == 3) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime23);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line23);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line23);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line23);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));											

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line23);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line23);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line23);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
				
						} 
						else if (i == 4) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime24);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line24);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line24);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line24);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
						
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line24);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line24);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line24);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 5) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime25);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line25);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line25);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line25);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line25);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line25);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line25);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 6) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime26);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line26);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line26);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line26);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line26);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line26);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line26);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 7) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime27);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line27);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line27);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line27);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line27);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line27);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line27);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 8) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime28);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line28);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line28);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line28);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
												
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line28);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line28);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line28);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 9) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime29);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line29);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line29);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line29);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line29);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line29);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line29);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 10) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime210);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line210);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line210);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line10);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line210);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line210);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line210);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
					}
				}else if (datas[0].equalsIgnoreCase(RGBWC)) {
					
				}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
					
					for (int i = 1; i <= dataArray.length; i++) {		
						
						if (i == 1) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime31);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line31);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line31);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line31);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line31);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));						

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line31);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line31);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));												
							
						} 
						else if (i == 2) {
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime32);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line32);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line32);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line32);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line32);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line32);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line32);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));						
							
						}
						else if (i == 3) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime33);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line33);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line33);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line33);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));											

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line33);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line33);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line33);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
				
						} 
						else if (i == 4) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime34);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line34);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line34);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line34);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
						
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line34);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line34);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line34);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 5) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime35);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line35);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line35);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line35);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));												

							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line35);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line35);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line35);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 6) {
							
							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime36);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line36);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line36);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line36);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line36);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line36);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line36);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 7) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime37);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line37);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line37);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line37);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line37);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line37);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line37);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						} 
						else if (i == 8) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime38);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line38);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line38);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line38);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
												
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line38);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line38);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line38);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 9) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime39);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line39);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line39);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line39);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));						
							
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line39);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line39);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line39);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
						else if (i == 10) {

							String[] value = dataArray[i].split("[ ]");
							
							List<String> list = new ArrayList<String>();
							for (int j = 0; j < value.length; j++) {
								if (!value[j].equalsIgnoreCase("")) {
									list.add(value[j]);
								}
							}
							
							TextView tvTime1 = (TextView)findTextViewById(R.id.tvTime310);
							tvTime1.setText(NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(0), 16)) + ":"
									+ NumberHelper.LeftPad_Tow_Zero(Integer.valueOf(list.get(1), 16)));						

							TextView tvRed1 = (TextView)findTextViewById(R.id.tv1Line310);
							tvRed1.setText(String.valueOf(Integer.valueOf(list.get(2), 16)));

							TextView tvGreen1 = (TextView)findTextViewById(R.id.tv2Line310);
							tvGreen1.setText(String.valueOf(Integer.valueOf(list.get(3), 16)));

							TextView tvLightblue1 = (TextView)findTextViewById(R.id.tv3Line310);
							tvLightblue1.setText(String.valueOf(Integer.valueOf(list.get(4), 16)));
													
							TextView tvWhite1 = (TextView)findTextViewById(R.id.tv4Line310);
							tvWhite1.setText(String.valueOf(Integer.valueOf(list.get(5), 16)));

							TextView tvCrystal1 = (TextView)findTextViewById(R.id.tv5Line310);
							tvCrystal1.setText(String.valueOf(Integer.valueOf(list.get(6), 16)));

							TextView tvPink1 = (TextView)findTextViewById(R.id.tv6Line310);
							tvPink1.setText(String.valueOf(Integer.valueOf(list.get(7), 16)));
							
						}
					}	
				}
											
				
				SharePersistent.savePerference(getApplicationContext(), Constant.TimingQueryActivity, "");
				isCanSend = true;																	
				handler.removeCallbacks(this);// 关闭定时器处理		
				
						
				
			}
		};
		handler.postDelayed(runnable, 1000);// 打开定时器，2秒后执行runnable  
		
		
	}

	private void putDataback() {

		Intent intent = new Intent();
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onResume(this);
	}
}
