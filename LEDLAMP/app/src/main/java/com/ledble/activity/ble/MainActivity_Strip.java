package com.ledble.activity.ble;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import com.common.uitl.ListUtiles;
import com.common.uitl.LogUtil;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledlamp.R;
import com.ledble.base.LedBleActivity;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.MyColor;
import com.ledble.bean.SceneBean;
import com.ledble.constant.CommonConstant;
import com.ledble.constant.Constant;
import com.ledble.db.GroupDevice;
import com.ledble.fragment.ble.MusicFragment;
import com.ledble.fragment.spi.CutomFragment_Spi;
import com.ledble.fragment.strip.CutomFragment_Strip;
import com.ledble.fragment.strip.ModeFragment_Strip;
import com.ledble.fragment.strip.RgbFragment_Strip;
import com.ledble.net.NetExceptionInterface;
import com.ledble.service.BluetoothLeServiceSingle;
import com.ledble.service.MyServiceConenction;
import com.ledble.utils.ManageFragment;
import com.ledble.view.ActionSheet;
import com.ledble.view.ActionSheet.ActionSheetListener;
import com.ledble.view.ColorTextView;
import com.ledble.view.GroupView;
import com.ledble.view.SlideSwitch;
import com.umeng.analytics.MobclickAgent;
import com.ledble.activity.ChipSelectActivity;
import com.ledble.activity.RgbSortActivity;
import com.ledble.activity.smart.OtherSettingActivity;

@SuppressLint("NewApi")
public class MainActivity_Strip extends LedBleActivity implements NetExceptionInterface, SensorEventListener, ActionSheetListener {

	@Bind(R.id.segmentDm) SegmentedRadioGroup segmentDm;
	@Bind(R.id.segmentCt) SegmentedRadioGroup segmentCt;
	@Bind(R.id.segmentRgb) SegmentedRadioGroup segmentRgb;
	@Bind(R.id.segmentMusic) SegmentedRadioGroup segmentMusic;
	@Bind(R.id.textViewModeTitle) TextView textViewModeTitle;
	@Bind(R.id.rlModeTop) RelativeLayout rlModeTop;
	@Bind(R.id.textViewCustomTitle) TextView textViewCustomTitle;
	@Bind(R.id.textViewSceneTitle) TextView textViewSceneTitle;
	@Bind(R.id.textViewBrightnessTitle) TextView textViewBrightnessTitle;
	@Bind(R.id.textViewTimerTitle) TextView textViewTimerTitle;

	@Bind(R.id.ivLeftMenu) ImageView ivLeftMenu;
	@Bind(R.id.textViewConnectCount) TextView textViewConnectCount;
	@Bind(R.id.ivRightMenu) ImageView ivRightMenu;
	@Bind(R.id.ivType) ImageView ivType;
	@Bind(R.id.rgBottom) RadioGroup rgBottom;
	@Bind(R.id.rbFirst) RadioButton rbFirst;
	@Bind(R.id.rbSecond) RadioButton rbSecond;
	@Bind(R.id.rbThrid) RadioButton rbThrid;
	@Bind(R.id.rbFourth) RadioButton rbFourth;
	@Bind(R.id.rbFifth) RadioButton rbFifth;
	@Bind(R.id.rbScene) RadioButton rbScene;
	@Bind(R.id.rbBrightness) RadioButton rbBrightness;

	@Bind(R.id.backLinearLayout) LinearLayout backLinearLayout;
	@Bind(R.id.backTextView) TextView backTextView;
	@Bind(R.id.onOffButton) Button onOffButton;
	@Bind(R.id.llMenu) LinearLayout avtivity_main;
	@Bind(R.id.menu_content_layout) LinearLayout left_menu;
	@Bind(R.id.right_menu_frame) ScrollView right_menu;
	@Bind(R.id.linearLayoutTopItem) LinearLayout TopItem;
	@Bind(R.id.shakeView) RelativeLayout shakeView;
	

	private int currentIndex;
	private FragmentManager fragmentManager;
	private List<Fragment> fragmentList = new ArrayList<Fragment>();
	private DrawerLayout mDrawerLayout;

	public boolean isLightOpen = true;
	public int speed = 1;
	private String speedKey = "speedkey";
	public int brightness = 1;
	private String brightnessKey = "brightnesskey";
	private String groupName = "";
	private SharedPreferences sp;
	private Editor editor;
	private MusicFragment musicFragment;
	private BluetoothLeServiceSingle mBluetoothLeService;
	private MyServiceConenction myServiceConenction;
	private LinearLayout linearGroups;

	private static MainActivity_Strip mActivity;

	// left_menu
	private ArrayList<BluetoothDevice> bleDevicesArray;
	private BluetoothManager bluetoothManager;
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothLeScanner bleScanner;
	private View refreshView;
	private Button buttonAddGroup, buttonAllOn, buttonAllOff;

	// right_menu
	private RelativeLayout rl_item_ble, rl_item_dmx, rl_item_smart, rl_item_timer, rl_item_strip, rl_item_spi;
	private TextView operationguideTV, resetTV, changePicTV, dynamicTV, shakeTV, timesetTV, fanTV;;
	private TextView rgbsortTV, timer_tv, pair_code_tv, setTV, codeTV;
	private ImageView gradientIV, breathIV, jumpIV, strobeIV;
	private ImageView shakeColorIV, shakeNoneIV, shakeModelIV;
	private TextView timerheckTV, currentDataCheckTV, modeSelectTV;
	private Random random = new Random();
	private SensorManager sensorManager;
	private SoundPool soundPool;
	private int soundID;
	private int shakeStyle = 1;
	private int STORAGE_CODE = 112;

	private Bitmap bm;
	private ImageView imageView = null; // app背景 
	private final int TAKE_PICTURE = 0; //
	private final int CHOOSE_PICTURE = 1;

	private final int MSG_START_CONNECT = 10000;// 开始连接
	private TextView textViewAllDeviceIndicater;// 显示所有设备
	private final int REQUEST_ENABLE_BT = 1;
	private int INT_GO_LIST = 111;
	private boolean isInitGroup = false;
	private boolean isAllOn = true;
	public final String ILLEGAL_DEVICE_NAME = "ELK-BLED";  //非法设备
	private boolean isShowIllegal = false;

	private volatile HashMap<String, Boolean> hashMapLock;// service连接锁
	private volatile HashMap<String, Boolean> hashMapConnect;
	private Map<String, SlideSwitch> map = new HashMap<>();
	private ArrayList<GroupView> arrayListGroupViews;

	private Dialog lDialog;
	
	private int MICRO_CODE = 111;
    private int LOCATION_CODE = 110;
    private ScanCallback scanCallback;
    private int GPS_REQUEST_CODE = 10;
    private boolean isGPSOpen = false; 
    private int OPEN_BLE = 333;

    public static SceneBean sceneBean;

    
 // *************广播消息**************//
// 	public final static UUID UUID_LOST_SERVICE = UUID.fromString("0000fff0-0000-1000-8000-00805f9b34fb");
 	public final static UUID UUID_LOST_SERVICE = UUID.fromString("00000000-3c00-0005-0220-FCFBFA002400");
 	private BluetoothLeAdvertiser mBluetoothLeAdvertiser; // BLE广播
 	private AdvertiseSettings.Builder mSettingsbuilder; // 
 	private AdvertiseData.Builder mDataBuilder;
 	
 	private Handler mstopSlideBroadcastHandler;
	private Runnable mstopSlideBroadcastRunnable;
	private Handler mstopBroadcastHandler;
	private Runnable mstopBroadcastRunnable;
 	
	private int count = 0;
	private int customCount = 0;
	
 	private int bGroupNum = 255; // 组号
	private long bMACADDR1 = 0; // mac地址1
	private long bMACADDR2 = 0; // mac地址2
	private long bMACADDR3 = 0; // mac地址3
	// private int bCRC = 0; //crc 值

	private int bTmp1 = 0;
	private int bTmp2 = 0;
	private int bTmp3 = 0;
	private int bTmp4 = 0;
	private int bTmp5 = 0;
	private int bTmp6 = 0;
	private int bTmp7 = 0;
	private int bTmp8 = 0;
	private int bTmp9 = 0;
	private int bTmp10 = 0;

	
	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.M)
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main);

		mActivity = this;
		
		if (Build.VERSION.SDK_INT >= 21) {
			View decorView = getWindow().getDecorView();
			int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
			decorView.setSystemUiVisibility(option);
			getWindow().setStatusBarColor(Color.TRANSPARENT); //透明 状态栏
			
		}
		
		Intent intent = getIntent();
		sceneBean = (SceneBean) intent.getSerializableExtra("scene");
//		
		initScanCallback(); // 初始化  ScanCallback	
//
		initFragment();
		initSlidingMenu();
		initView();
		
//		refresh();

//		if (getImagePath() != "") { // 显示保存的皮肤
//			showImage(getImagePath());
//		}
		
	}
	
	public MainActivity_Strip() {
		mActivity = this;
	}

	public static MainActivity_Strip getMainActivity() {
		return mActivity;
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.e("onkeydown", "in");

		if (keyCode == KeyEvent.KEYCODE_BACK) {
			MainActivity_Strip.this.finish();
			return true;
		} else {
			return super.onKeyDown(keyCode, event);
		}
	}
	
	public static SceneBean getSceneBean() {
		return sceneBean;
	}
	
	/**
	 * 
	 */
	protected void refresh() {
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				
				turnOnBluetooth();	
				
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 800);// 打开定时器，执行操作

	}
	
	/**
	 * 强制开启当前 Android 设备的 Bluetooth 
	 * 
	 * @return true：强制打开 Bluetooth　成功　false：强制打开 Bluetooth 失败
	 */
	public void turnOnBluetooth() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter != null) {		
			
			//提示是否打开  带弹窗提醒
	        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	        mActivity.startActivityForResult(intent, OPEN_BLE);

		}

	}
	
	private void sendClickBroadcast() {			
		
		clickBroadcast(false);
			
	}
	
	/**
	 * 刷新广播
	 */
	protected void clickBroadcast(boolean showToast) {	
		
		if (mstopBroadcastHandler != null) {
			stopBLEBroadcast(); //关广播
			mstopBroadcastHandler.removeCallbacks(mstopBroadcastRunnable);
			mstopBroadcastHandler = null;
		}
		
//		new Thread(new Runnable() {
//			@Override
//			public void run() {

				startBLEBroadcast(); //开广播
			
//			}
//		}).start();
		
		if (null == mstopBroadcastHandler) {
			mstopBroadcastHandler = new Handler(); // 开启 定时搜索 定时器			
		}
		if (null == mstopBroadcastRunnable) {
			mstopBroadcastRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					
//					new Thread(new Runnable() {
//						@Override
//						public void run() {
					
							stopBLEBroadcast(); //关广播
						
//						}
//					}).start();					
				}
			};
		}	
		mstopBroadcastHandler.postDelayed(mstopBroadcastRunnable, 1000);// 打开定时器，执行操作

	}
	
	private void sendSlideBroadcast() {
		
		startBLEBroadcast(); //开广播		
		
		if (null == mstopSlideBroadcastHandler) {
			mstopSlideBroadcastHandler = new Handler(); // 开启 定时搜索 定时器			
		}
		if (null == mstopSlideBroadcastRunnable) {
			mstopSlideBroadcastRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码

					stopBLEBroadcast(); //关广播	
					
				}
			};
		}	
//		mstopSlideBroadcastHandler.postDelayed(mstopSlideBroadcastRunnable, 300);// 打开定时器，执行操作		
		mstopSlideBroadcastHandler.postDelayed(mstopSlideBroadcastRunnable, 300);// 打开定时器，执行操作	
	}
	
	// ============启动BLE蓝牙广播(广告) =================================================================================
		public void startBLEBroadcast() {
			
			if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
				
				final byte[] userBroadcastData = { 0x02, 0x15, 0x58, 0x50, 0x59, 0x02, (byte)bGroupNum, (byte)bTmp1, (byte)bTmp2, (byte)bTmp3, 
						(byte)bTmp4, (byte)bTmp5, (byte)bTmp6, (byte)bTmp7, (byte)bTmp8, (byte)bTmp9, (byte)bTmp10 };
				if (null == mBluetoothLeAdvertiser) {

					mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
					
					new Thread(new Runnable() {
						@Override
						public void run() {

							mBluetoothLeAdvertiser.startAdvertising(createAdvSettings(true, 0), createAdvertiseData(userBroadcastData), mAdvertiseCallback);
//							mBluetoothLeAdvertiser.startAdvertising(createAdvSettings(true, 100), createAdvertiseData(userBroadcastData), mAdvertiseCallback);

						}
					}).start();				
					
				}
			}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
				
				final byte[] userBroadcastData = { 0x02, 0x15, 0x58, 0x50, 0x59, 0x01, (byte)bGroupNum, (byte)bTmp1, (byte)bTmp2, (byte)bTmp3, 
						(byte)bTmp4, (byte)bTmp5, (byte)bTmp6, (byte)bTmp7, (byte)bTmp8, (byte)bTmp9, (byte)bTmp10 };
				if (null == mBluetoothLeAdvertiser) {

					mBluetoothLeAdvertiser = mBluetoothAdapter.getBluetoothLeAdvertiser();
					
					new Thread(new Runnable() {
						@Override
						public void run() {

							mBluetoothLeAdvertiser.startAdvertising(createAdvSettings(true, 0), createAdvertiseData(userBroadcastData), mAdvertiseCallback);
//							mBluetoothLeAdvertiser.startAdvertising(createAdvSettings(true, 100), createAdvertiseData(userBroadcastData), mAdvertiseCallback);

						}
					}).start();				
					
				}
			}
									
		}
		
		private void stopBLEBroadcast() {
	    	
	    	if (null != mBluetoothLeAdvertiser) {
	    		mBluetoothLeAdvertiser.stopAdvertising(mAdvertiseCallback);   		
	    		mBluetoothLeAdvertiser = null;
	    		
				if (mstopSlideBroadcastHandler != null) {
					mstopSlideBroadcastHandler.removeCallbacks(mstopSlideBroadcastRunnable);
					mstopSlideBroadcastHandler = null;
				}
			}
		}
		
		//广播设置(必须)
		public AdvertiseSettings createAdvSettings(boolean connectable, int timeoutMillis) {		
			if (mSettingsbuilder == null) {
				mSettingsbuilder = new AdvertiseSettings.Builder();
			}        
	        mSettingsbuilder.setAdvertiseMode(AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY);
	        mSettingsbuilder.setConnectable(connectable); //设置是否可以连接。 广播分为可连接广播和不可连接广播，一般不可连接广播应用在iBeacon设备上，这样APP无法连接上iBeacon设备
	        mSettingsbuilder.setTimeout(timeoutMillis); //设置广播的最长时间,设为0时，代表无时间限制会一直广播
	        mSettingsbuilder.setTxPowerLevel(AdvertiseSettings.ADVERTISE_TX_POWER_HIGH); //发射功率级别: 极低,低,中,高
	        AdvertiseSettings mAdvertiseSettings = mSettingsbuilder.build();
	        return mAdvertiseSettings;
	    }
		
		//广播数据(必须，广播启动就会发送)
		public AdvertiseData createAdvertiseData( byte[] userBroadcastData) {
			if (mDataBuilder == null) {
				mDataBuilder = new AdvertiseData.Builder();
			}
	        mDataBuilder.addManufacturerData(0x4C, userBroadcastData); //设备厂商数据，自定义
//	        mDataBuilder.setIncludeDeviceName(false); // 是否包含设备名称
//	        mDataBuilder.addServiceData(ParcelUuid.fromString("0000fff0-0000-1000-8000-00805f9b34fb"), new byte[]{1,2});
//	        mDataBuilder.addServiceData(new ParcelUuid(UUID_SERVICE), new byte[]{0x64,0x12});
//	        mDataBuilder.setIncludeTxPowerLevel(false); //广播不包含电源信息
//	        mDataBuilder.addServiceUuid(new ParcelUuid(UUID_LOST_SERVICE)); //服务UUID
	        AdvertiseData mAdvertiseData = mDataBuilder.build();
	        return mAdvertiseData;
	    }
		
		//扫描响应数据(可选，当客户端扫描时才发送)
		public AdvertiseData createAdvertiseScanResponseData(byte[] data) {
			AdvertiseData.Builder mDataBuilder = new AdvertiseData.Builder();
//			mDataBuilder.addManufacturerData(0xAC, data); //设备厂商数据，自定义
//			mDataBuilder.addServiceData( ParcelUuid.fromString("0000ae8f-0000-1000-8000-00805f9b34fb"), 
//	        		new byte[]{0x64,0x12/**/ /*(byte)bOnoff, (byte)bAction, (byte)bMode, 
//	        	(byte)bSpeed, (byte)bBrightness, (byte)bR, (byte)bG, (byte)bB /**/});
//			mDataBuilder.addServiceUuid(new ParcelUuid(UUID_SERVICE)); //服务UUID
//			mDataBuilder.setIncludeTxPowerLevel(false); //广播不包含电源信息
			AdvertiseData mAdvertiseData = mDataBuilder.build();
			return mAdvertiseData;
		}
		
		
	    private AdvertiseCallback mAdvertiseCallback = new AdvertiseCallback() {
	        @Override
	        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
	            super.onStartSuccess(settingsInEffect);

//	            if (isFirstShow) {
//	            	Toast.makeText(mActivity, "开启广播成功", Toast.LENGTH_SHORT).show();
//				}
	        }

	        @Override
	        public void onStartFailure(int errorCode) {
	            super.onStartFailure(errorCode);
	            stopBLEBroadcast(); //关广播
	            Toast.makeText(mActivity, "开启广播失败 errorCode："+ errorCode, Toast.LENGTH_SHORT).show();
	        }
	    };

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// super.onSaveInstanceState(outState);
	}

	@SuppressLint("NewApi")
	private void initView() {
//
////		mActivity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		
		mActivity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		// 是否支持蓝牙
		if (!mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(mActivity, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
			Tool.exitApp();
		}
		
		bluetoothManager = (BluetoothManager) mActivity.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
		if (Build.VERSION.SDK_INT >= 23) {//安卓6.0以上的方案
			bleScanner = mBluetoothAdapter.getBluetoothLeScanner();//用过单例的方式获取实例
        }
		
		// 不能获得蓝牙设备支持
		if (mBluetoothAdapter == null) {
			Tool.ToastShow(mActivity, R.string.ble_not_supported);
			Tool.exitApp();
		}


		hashMapLock = new HashMap<String, Boolean>();
		hashMapConnect = new HashMap<String, Boolean>();
		arrayListGroupViews = new ArrayList<GroupView>();
		bleDevicesArray = new ArrayList<BluetoothDevice>();
		
		
		// 左侧 菜单
		imageView = (ImageView) mActivity.findViewById(R.id.activity_main_imageview); // app背景图
		linearGroups = (LinearLayout) mActivity.findViewById(R.id.linearLayoutDefineGroups);
		textViewAllDeviceIndicater = (TextView) mActivity.findViewById(R.id.textViewAllDeviceIndicater);
		TopItem.setOnClickListener(new MyOnClickListener());

		buttonAllOff = (Button) mActivity.findViewById(R.id.buttonAllOff);
		buttonAllOff.setOnClickListener(new MyOnClickListener());

		buttonAllOn = (Button) mActivity.findViewById(R.id.buttonAllOn);
		buttonAllOn.setOnClickListener(new MyOnClickListener());

		buttonAddGroup = (Button) mActivity.findViewById(R.id.buttonAddGroup);
		buttonAddGroup.setOnClickListener(new MyOnClickListener());

		refreshView = (View) mActivity.findViewById(R.id.ivRefresh);
		refreshView.setOnClickListener(new MyOnClickListener());
				
		
		// 右菜单 各个标签项
		rl_item_ble = (RelativeLayout) mActivity.findViewById(R.id.rl_item_ble);
		rl_item_dmx = (RelativeLayout) mActivity.findViewById(R.id.rl_item_dmx);
		rl_item_smart = (RelativeLayout) mActivity.findViewById(R.id.rl_item_smart);
		rl_item_timer = (RelativeLayout) mActivity.findViewById(R.id.rl_item_timer);
		rl_item_strip = (RelativeLayout) mActivity.findViewById(R.id.rl_item_strip);
		rl_item_spi = (RelativeLayout) mActivity.findViewById(R.id.rl_item_spi);
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE)) {
			rl_item_dmx.setVisibility(View.GONE);
			rl_item_smart.setVisibility(View.GONE);
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
			rl_item_ble.setVisibility(View.GONE);
			rl_item_smart.setVisibility(View.GONE);
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SMART)) {
			rl_item_ble.setVisibility(View.GONE);
			rl_item_dmx.setVisibility(View.GONE);
			rl_item_timer.setVisibility(View.GONE);
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			rl_item_ble.setVisibility(View.GONE);
			rl_item_dmx.setVisibility(View.GONE);
			rl_item_smart.setVisibility(View.GONE);
			rl_item_timer.setVisibility(View.GONE);
			rl_item_strip.setVisibility(View.VISIBLE);
			rl_item_spi.setVisibility(View.GONE);
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			rl_item_ble.setVisibility(View.GONE);
			rl_item_dmx.setVisibility(View.GONE);
			rl_item_smart.setVisibility(View.GONE);
			rl_item_timer.setVisibility(View.GONE);
			rl_item_strip.setVisibility(View.GONE);
			rl_item_spi.setVisibility(View.VISIBLE);
		}
		
		backLinearLayout.setOnClickListener(new MyOnClickListener());
		backTextView.setOnClickListener(new MyOnClickListener());
		onOffButton.setOnClickListener(new MyOnClickListener());

		changePicTV = (TextView) mActivity.findViewById(R.id.change_under_pic_tv);
		changePicTV.setOnClickListener(new MyOnClickListener());
		resetTV = (TextView) mActivity.findViewById(R.id.reset_tv);
		resetTV.setOnClickListener(new MyOnClickListener());
		shakeTV = (TextView) mActivity.findViewById(R.id.shake_tv);
		shakeTV.setOnClickListener(new MyOnClickListener());
		
		setTV = (TextView) mActivity.findViewById(R.id.set_tv);
		setTV.setOnClickListener(new MyOnClickListener());
		
		mActivity.findViewById(R.id.tv_set_spi).setOnClickListener(new MyOnClickListener());
		
		codeTV = (TextView) mActivity.findViewById(R.id.code_tv);
		codeTV.setOnClickListener(new MyOnClickListener());
		
			
		dynamicTV = (TextView) mActivity.findViewById(R.id.dynamic_tv);
		dynamicTV.setOnClickListener(new MyOnClickListener());

		// 右菜单 律动各项
		gradientIV = (ImageView) mActivity.findViewById(R.id.dynamic_gradient_iv);
		gradientIV.setOnClickListener(new MyOnClickListener());
		breathIV = (ImageView) mActivity.findViewById(R.id.dynamic_breath_iv);
		breathIV.setOnClickListener(new MyOnClickListener());
		jumpIV = (ImageView) mActivity.findViewById(R.id.dynamic_jump_iv);
		jumpIV.setOnClickListener(new MyOnClickListener());
		strobeIV = (ImageView) mActivity.findViewById(R.id.dynamic_strobe_iv);
		strobeIV.setOnClickListener(new MyOnClickListener());

		operationguideTV = (TextView) mActivity.findViewById(R.id.operation_guide_tv);
		operationguideTV.setOnClickListener(new MyOnClickListener());

		// RGB 排序
		rgbsortTV = (TextView) mActivity.findViewById(R.id.rgb_sort_tv_strip);
		rgbsortTV.setOnClickListener(new MyOnClickListener());

		// 定时器
		timer_tv = (TextView) mActivity.findViewById(R.id.timer_tv);
		timer_tv.setOnClickListener(new MyOnClickListener());

		// 对码
		pair_code_tv = (TextView) mActivity.findViewById(R.id.pair_code_tv);
		pair_code_tv.setOnClickListener(new MyOnClickListener());
		
		
		// 时间设置&风扇转动温度设置
		timesetTV = (TextView) mActivity.findViewById(R.id.time_set_tv);
		timesetTV.setOnClickListener(new MyOnClickListener());
		fanTV = (TextView) mActivity.findViewById(R.id.fan_rotational_temperature_tv);
		fanTV.setOnClickListener(new MyOnClickListener());
		// 定时查询 & 当前数据查询
		timerheckTV = (TextView) mActivity.findViewById(R.id.timer_check_tv);
		timerheckTV.setOnClickListener(new MyOnClickListener());
		currentDataCheckTV = (TextView) mActivity.findViewById(R.id.currentquery_tv);
		currentDataCheckTV.setOnClickListener(new MyOnClickListener());		
		// 模式选择
		modeSelectTV = (TextView) mActivity.findViewById(R.id.mode_select_tv);
		modeSelectTV.setOnClickListener(new MyOnClickListener());
		

		// 右菜单 摇一摇
		shakeColorIV = (ImageView) mActivity.findViewById(R.id.shake_one_iv);
		shakeColorIV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				view.setBackgroundResource(R.drawable.bg_in_press);
				shakeNoneIV.setBackgroundResource(R.drawable.bg_shake_green);
				shakeModelIV.setBackgroundResource(R.drawable.bg_shake_green);
				shakeView.setBackgroundResource(R.drawable.bg_shake_green);
				shakeStyle = 0;
			}
		});

		shakeNoneIV = (ImageView) mActivity.findViewById(R.id.shake_two_iv);
		shakeNoneIV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				view.setBackgroundResource(R.drawable.bg_in_press);
				shakeColorIV.setBackgroundResource(R.drawable.bg_shake_lightgray);
				shakeModelIV.setBackgroundResource(R.drawable.bg_shake_lightgray);
				shakeView.setBackgroundResource(R.drawable.bg_shake_lightgray);
				shakeStyle = 1;
			}
		});

		shakeModelIV = (ImageView) mActivity.findViewById(R.id.shake_three_iv);
		shakeModelIV.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				view.setBackgroundResource(R.drawable.bg_in_press);
				shakeColorIV.setBackgroundResource(R.drawable.bg_shake_orange);
				shakeNoneIV.setBackgroundResource(R.drawable.bg_shake_orange);
				shakeView.setBackgroundResource(R.drawable.bg_shake_orange);
				shakeStyle = 2;
			}
		});

		shakeNoneIV.performClick();// 默认第一个点击
			
			

		
		// 摇一摇
		sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { /* 新版 */
			AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_GAME)
					.setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
			soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
		} else {
			soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 1);
		}
		soundID = soundPool.load(this, R.raw.dang, 1);
		
		// 初始tab项
		sp = getSharedPreferences(Constant.MODLE_TYPE, Context.MODE_PRIVATE);
		editor = sp.edit();
//		currentIndex = sp.getInt(Constant.MODLE_VALUE, 2);
		currentIndex = 0;


//		// ====连接后台service
//		myServiceConenction = new MyServiceConenction();
//		myServiceConenction.setServiceConnectListener(new ServiceConnectListener() {
//			@Override
//			public void onConnect(ComponentName name, IBinder service, BluetoothLeServiceSingle bLeService) {
//				mBluetoothLeService = bLeService;// 获取连接实例
//				// leftMenuFragment.setService(mBluetoothLeService);
//				
//				if (!mBluetoothLeService.initialize()) {
//					Log.e(LedBleApplication.tag, "Unable to initialize Bluetooth");
//				} else {
//					Log.e(LedBleApplication.tag, "Initialize Bluetooth");
//				}
//			}
//
//			@Override
//			public void onDisConnect(ComponentName name) {
//
//			}
//		});
//
//		Intent gattServiceIntent = new Intent(this, BluetoothLeServiceSingle.class);
//		bindService(gattServiceIntent, myServiceConenction, Activity.BIND_AUTO_CREATE);

//		ivType.setImageResource(R.drawable.tab_dim_check);
//		ivType.setOnClickListener(new MyOnClickListener());
		ivLeftMenu.setOnClickListener(new MyOnClickListener());
		ivRightMenu.setOnClickListener(new MyOnClickListener());
//
//		// buttonAllOff.setOnClickListener(new MyOnClickListener());
//		// buttonAllOn.setOnClickListener(new MyOnClickListener());
//		// buttonAddGroup.setOnClickListener(new MyOnClickListener());
//		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP) ||
			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			ivLeftMenu.setVisibility(View.GONE);
			textViewConnectCount.setVisibility(View.GONE);
		}

		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE) || 
			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX) || 
			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP) || 
			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			rbScene.setVisibility(View.GONE);
			rbBrightness.setVisibility(View.GONE);
			rbFourth.setVisibility(View.GONE);
			rbFifth.setVisibility(View.GONE);
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SMART)) {
			rbSecond.setVisibility(View.GONE);
			rbFourth.setVisibility(View.GONE);
		}
		
		rgBottom.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbFirst:
					currentIndex = 0;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.VISIBLE);
					segmentMusic.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					rlModeTop.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewTimerTitle.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE) || 
						sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						pauseMusicAndVolum(true);
					}
					
					break;
				case R.id.rbSecond:
					currentIndex = 1;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.GONE);								
					textViewCustomTitle.setVisibility(View.GONE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewTimerTitle.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
						textViewModeTitle.setVisibility(View.VISIBLE);		
						rlModeTop.setVisibility(View.GONE);
					}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
						textViewModeTitle.setVisibility(View.GONE);		
						rlModeTop.setVisibility(View.VISIBLE);
					}
					break;

				case R.id.rbBrightness:
					currentIndex = 2;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					rlModeTop.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.VISIBLE);
					textViewTimerTitle.setVisibility(View.GONE);
					ivType.setVisibility(View.INVISIBLE);
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE)
							|| sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						pauseMusicAndVolum(true);
					}
					break;
				case R.id.rbThrid:
					currentIndex = 2;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					rlModeTop.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.VISIBLE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewTimerTitle.setVisibility(View.GONE);
					ivType.setVisibility(View.GONE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE)
							|| sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						pauseMusicAndVolum(true);
					}
					
					break;
				case R.id.rbFourth:
					currentIndex = 3;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.VISIBLE);
					textViewModeTitle.setVisibility(View.GONE);
					rlModeTop.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewTimerTitle.setVisibility(View.GONE);
					ivType.setVisibility(View.INVISIBLE);
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE)
							|| sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						pauseMusicAndVolum(false);
					}
					
					break;
				case R.id.rbFifth:
					currentIndex = 4;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.GONE);
					textViewModeTitle.setVisibility(View.GONE);
					rlModeTop.setVisibility(View.GONE);
					textViewCustomTitle.setVisibility(View.GONE);
					textViewSceneTitle.setVisibility(View.GONE);
					textViewBrightnessTitle.setVisibility(View.GONE);
					textViewTimerTitle.setVisibility(View.VISIBLE);
					ivType.setVisibility(View.INVISIBLE);
					if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE)
							|| sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)) {
						pauseMusicAndVolum(true);
					}
					break;

				}
				ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
			}
		});

		if (currentIndex == 0) {
			ivType.setImageResource(R.drawable.tab_dim_check);
			rgBottom.check(R.id.rbFirst);
		} else if (currentIndex == 1) {
			ivType.setImageResource(R.drawable.tab_ct_check);
			rgBottom.check(R.id.rbSecond);
		} else if (currentIndex == 2) {
			ivType.setImageResource(R.drawable.tab_reg_check);
			rgBottom.check(R.id.rbThrid);
		}
		
//		brightness = SharePersistent.getInt(this, brightnessKey);
//		speed = SharePersistent.getInt(this, speedKey);


//		if (mBluetoothAdapter.isEnabled()) {
////			initBleScanList(isAllOn);
//		}
	}

	/**
	 * 初始化滑动菜单
	 */
	private void initSlidingMenu() {

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setScrimColor(Color.TRANSPARENT);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	
	}

	/**
	 * 初始化fragment
	 */
	public void initFragment() {

		fragmentList.add(new RgbFragment_Strip());
		fragmentList.add(new ModeFragment_Strip());		
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			fragmentList.add(new CutomFragment_Spi());
		}else {
			fragmentList.add(new CutomFragment_Strip());
		}
		
		
//		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_BLE) || 
//			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_DMX)/* || 
//			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP) || 
//			sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)*/) {
//			musicFragment = new MusicFragment();
//			fragmentList.add(musicFragment);
//		}
		
		// 添加Fragment
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		for (int i = 0; i < fragmentList.size(); i++) {
			transaction.add(R.id.flContent, fragmentList.get(i), fragmentList.get(i).getClass().getSimpleName());
		}
		transaction.commit();
		ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
	}

	public void pauseMusicAndVolum(boolean status) {
//		musicFragment.pauseMusic();
//		musicFragment.pauseVolum(status);
	}
	
	private void gotoTimerSettting(String tag, int id) {
		Intent intent = new Intent(mActivity, OtherSettingActivity.class);
		intent.putExtra("tag", tag);
		mActivity.startActivityForResult(intent, id);
	}

	public class MyOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			
			case R.id.backLinearLayout: // 返回
//				Toast.makeText(mActivity, "back", Toast.LENGTH_SHORT).show();
				finish();
				break;
			case R.id.backTextView: // 返回
//				Toast.makeText(mActivity, "back", Toast.LENGTH_SHORT).show();
				finish();
				break;
				
			case R.id.onOffButton: // 
				if (isLightOpen) {					
					mActivity.turnOff();
					onOffButton.setBackgroundResource(R.drawable.off_btn);
					isLightOpen = false;
				}else {
					onOffButton.setBackgroundResource(R.drawable.on_btn);
					mActivity.turnOn();
					isLightOpen = true;
				}
				break;
			
			case R.id.ivLeftMenu:
				mDrawerLayout.openDrawer(left_menu);
				break;
			case R.id.ivRightMenu:
//				if (sceneBean.getName().equalsIgnoreCase(LED_BLE)) {
					mDrawerLayout.openDrawer(right_menu);
//				}else if (sceneBean.getName().equalsIgnoreCase(LED_DMX)) {
//					mDrawerLayout.openDrawer(right_menu_dmx);
//				}
				
				break;
//			case R.id.linearLayoutTopItem:
//				showActionSheet("");
//				break;
//			case R.id.ivType:
//				if (currentIndex == 0) {
//					ivType.setImageResource(R.drawable.tab_ct_check);
//					rgBottom.check(R.id.rbSecond);
//				} else if (currentIndex == 1) {
//					ivType.setImageResource(R.drawable.tab_reg_check);
//					rgBottom.check(R.id.rbThrid);
//				} else if (currentIndex == 2) {
//					ivType.setImageResource(R.drawable.tab_dim_check);
//					rgBottom.check(R.id.rbFirst);
//				}
//				break;
//
//			case R.id.buttonAllOff:
//				// Toast.makeText(mActivity, "全关", Toast.LENGTH_SHORT).show();
//				allOff();			
//				break;
//			case R.id.buttonAllOn:
//				allOn();			
//				break;
//			case R.id.buttonAddGroup:
//				addGroupMessage();
//				
//				break;
//			case R.id.ivRefresh:// 刷新
//				refreshDevices(true);
//				break;
//				
//				
//			case R.id.time_set_tv:// 时间设置
//				gotoTimerSettting("time", 11);
//				break;
//			case R.id.fan_rotational_temperature_tv:// 风扇
//				gotoTimerSettting("fan", 12);
//				break;
//			case R.id.timer_check_tv:	// 定时查询	
//				startActivity(new Intent(mActivity, TimingQueryActivity.class));
//				SharePersistent.savePerference(mActivity, Constant.TimingQueryActivity, "");
//				SharePersistent.savePerference(mActivity, Constant.Activity, Constant.TimingQueryActivity);
//				break;
//			case R.id.currentquery_tv:	// 当前数据查询
//				startActivity(new Intent(mActivity, CurrentQueryActivity.class));
//				SharePersistent.savePerference(mActivity, Constant.Activity, Constant.CurrentQueryActivity);
//				break;
//			case R.id.mode_select_tv:	// 模式选择
//				startActivity(new Intent(mActivity, ModeSelectActivity.class));
//				break;
//				
//				
			case R.id.tv_set_spi:// 设置
				startActivity(new Intent(mActivity, ChipSelectActivity.class));
				break;
//			case R.id.code_tv:// 编码
//				startActivity(new Intent(mActivity, CodeActivity.class));
//				break;
//				
//			case R.id.dynamic_tv:// 律动
//				startActivity(new Intent(mActivity, DynamicColorActivity.class));
//				break;
//			case R.id.dynamic_gradient_iv: // 律动  渐变
//				setDynamicModel(128);
//				break;
//			case R.id.dynamic_breath_iv: // 渐变	呼吸
//				setDynamicModel(129);
//				break;
//			case R.id.dynamic_jump_iv: // 律动	跳变
//				setDynamicModel(130);
//				break;
//			case R.id.dynamic_strobe_iv: // 律动 	频闪
//				setDynamicModel(131);
//				break;
//				
			case R.id.rgb_sort_tv_strip: // RGB 排序
				startActivity(new Intent(mActivity, RgbSortActivity.class));
//				startActivity(new Intent(mActivity, CodeActivity.class));
				break;
//			case R.id.timer_tv: //定时器
//				startActivity(new Intent(mActivity, TimerActivity.class));
//				break;
//			case R.id.pair_code_tv: //对码 
//				startActivity(new Intent(mActivity, PairCodeActivity.class));
//				break;
//
//
//			case R.id.operation_guide_tv: // 操作指南
//				Intent intent = new Intent(mActivity, OprationManualActivity.class);
//				startActivity(intent);
//				break;
//			case R.id.change_under_pic_tv: // 更换皮肤
//				showPicturePicker(MainActivity_Strip.this);
//				break;
//			case R.id.reset_tv: // 一键还原
//				Drawable drawable = getResources().getDrawable(R.drawable.bg_all);
//				imageView.setImageDrawable(drawable);
//
//				String imagePath = getImagePath();
//				imagePath = "";
//				saveImagePathToSharedPreferences(imagePath);// 保存图片
//
//				break;
			}
			
		}
	}
	
	
//	private void showCustomMessage() {
//		
//		if (lDialog == null ) {
//			lDialog = new Dialog(mActivity, android.R.style.Theme_Translucent_NoTitleBar);
//			lDialog.requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//			lDialog.setContentView(R.layout.dialogview);	
//			
//			ImageView iv_route = (ImageView) lDialog.findViewById(R.id.imageViewWait);  
//			RotateAnimation mAnim = new RotateAnimation(0, 360, Animation.RESTART, 0.5f, Animation.RESTART, 0.5f);  
//		    mAnim.setDuration(2000);  
//		    mAnim.setRepeatCount(Animation.INFINITE);// 设置重复次数，这里是无限  
//		    mAnim.setRepeatMode(Animation.RESTART);// 设置重复模式  
//		    mAnim.setStartTime(Animation.START_ON_FIRST_FRAME);  
//		    // 匀速转动的代码  
//		    LinearInterpolator lin = new LinearInterpolator();  
//		    mAnim.setInterpolator(lin);  
//		    iv_route.startAnimation(mAnim);  
//
//			lDialog.show();
//		}
////		lDialog.show();
//	}
//	
//	public void lDialogDismiss() {
//		lDialog.dismiss();
//		lDialog = null;
//	}
//
//	// =============================================行为事件=====================================================// 
//	
//	/**
//	 * 扫描设备
//	 */
//	public void initBleScanList(boolean isAllon) {
//		if (!mBluetoothAdapter.isEnabled()) {
//			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
//		}
//
//		if (isInitGroup == false) {
//			initGroup(isAllon);
//			isInitGroup = true;
//		}
//		
//		scanLeDevice(true);
//	}
//	
//	/**
//	 * enable 暂时无用途
//	 * 
//	 * @param enable
//	 */
//	private void scanLeDevice(final boolean enable) {
//		
//		refreshDevices(true);
//				
//	}
//	
//
//	/**
//	 * 刷新
//	 */
//	protected void refreshDevices(final boolean showToast) {
//		
//		if (Build.VERSION.SDK_INT >= 23) {
//			if (!checkGPSIsOpen()) {
//				checkAuthorization();
//				return;
//			}				
//		}
//		
//		startLeScan(showToast); //开始扫描
//		
//		final Handler handler = new Handler(); // 开启 定时搜索 定时器
//		Runnable runnable = new Runnable() {
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				// 在此处添加执行的代码
//				stopLeScan(showToast);
//			}
//		};
//		handler.postDelayed(runnable, 6000);// 打开定时器，执行操作
//
//	}
//	
//	private void disConnectAll() {
//		String connected = getResources().getString(R.string.conenct_device, 0, 0);
//		textViewAllDeviceIndicater.setText(connected);
//
////		if (LedBleApplication.getApp().getBleDevices().size() > 0) {
////			for (int i = 0; i < LedBleApplication.getApp().getBleDevices().size(); i++) {
////				LedBleApplication.getApp().getBleDevices().remove(i);
////			}
////		}	
//		
//	}
//	
//	/**
//	 * 检查定位权限是否开启
//	 */
//	protected void showCheckSelfPermission() {
//        //安卓6.0以上
//		if (Build.VERSION.SDK_INT >= 23) {
//			if (mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED) {
////				Toast.makeText(mActivity, getString(R.string.targeting_permission), Toast.LENGTH_SHORT).show();
//			}
//		}
//	}
//
//	
//	/**
//	 * 开始扫描
//	 */
//	protected void startLeScan(boolean show) {	
//		
////		LogUtil.i(LedBleApplication.tag, "============================== 开始扫描：=============================" );
//		if (show) {
//			showCustomMessage();
//		}
//
////		Toast.makeText(mActivity, "开始扫描", Toast.LENGTH_SHORT).show();	
//		
//		if (Build.VERSION.SDK_INT < 23) {//安卓6.0以下的方案
//			mBluetoothAdapter.startLeScan(mLeScanCallback);
//        } else {//安卓7.0及以上的方案
//            
//            if (mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
//                    != PackageManager.PERMISSION_GRANTED) {
//                // 申请定位授权
//            	mActivity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_CODE);;
//            } else {
//                bleScanner.startScan(scanCallback);
//            }
//        }
//	}
//	
//	/**
//	 * 停止扫描
//	 */
//	protected void stopLeScan(boolean show) {
//		
////		LogUtil.i(LedBleApplication.tag, "============================== 停止扫描：=============================" );
//		if (show) {
//			lDialogDismiss(show);
//			isGPSOpen = false;
//		}
////		Toast.makeText(mActivity, "停止扫描", Toast.LENGTH_SHORT).show();
//		
//		if (Build.VERSION.SDK_INT < 23) {
//            //安卓6.0及以下版本BLE操作的代码
//			mBluetoothAdapter.stopLeScan(mLeScanCallback);
//         } else {
//        	//安卓7.0及以上版本BLE操作的代码
//             bleScanner.stopScan(scanCallback);
//         }
//             
//	}
//	
//	public void lDialogDismiss(boolean show) {		
//		if (lDialog != null) {
//			lDialog.dismiss();
//			lDialog = null;
//		}	
//	}
//	
//	/**
//	 * 检测GPS是否打开
//	 *
//	 * @return
//	 */
//	private boolean checkGPSIsOpen() {
//		LocationManager locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);  
//        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）  
//        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);  
//        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）  
//        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);  
//        if (gps || network) {  
//            return true;  
//        }  
//        return false; 
//	}
//	
//	/**
//	 * 打开权限提醒
//	 * @param context
//	 */
//
//	private void checkAuthorization () {
//
//		final TextView editText = new TextView(mActivity);
//		editText.setText(getResources().getString(R.string.authorization_alert));
//		new AlertDialog.Builder(mActivity).setTitle(R.string.authorization_title).setView(editText).setIcon(R.drawable.ic_launcher)
//				.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {	
//						openGPSSettings();
//					}
//				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//					}
//				}).show();
//	}
//	
//	/**
//	 * 跳转GPS设置
//	 */
//	private void openGPSSettings() {
////	    if (checkGPSIsOpen()) {
////	        initLocation(); //自己写的定位方法
////	    } else {
//	        //没有打开则弹出对话框
//	        new AlertDialog.Builder(this)
//	                //.setTitle(R.string.authorization_title)
//	                .setMessage(R.string.gpsNotifyMsg)
//	                // 拒绝, 退出应用
//	                .setNegativeButton(R.string.text_cancel,
//	                        new DialogInterface.OnClickListener() {
//	                            @Override
//	                            public void onClick(DialogInterface dialog, int which) {
////	                                finish();
//	                            }
//	                        })
//
//	                .setPositiveButton(R.string.action_settings,
//	                        new DialogInterface.OnClickListener() {
//	                            @Override
//	                            public void onClick(DialogInterface dialog, int which) {
//	                                //跳转GPS设置界面
//	                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//	                                startActivityForResult(intent, GPS_REQUEST_CODE);
//	                                isGPSOpen = true;                               
//	                            }
//	                        })
//
//	                .setCancelable(false)
//	                .show();
//
////	    }
//	}
//	
//	//region Request Permissions
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
//			@NonNull int[] grantResults) {
//
//		switch (requestCode) {
//		case 110:
//			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//				if (Build.VERSION.SDK_INT >= 23) {
//					refreshDevices(true);
//					// Toast.makeText(mActivity, "定位权限打开成功, 开始扫描",
//					// Toast.LENGTH_SHORT).show();
//					
//					if (mActivity.checkSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
//						// 申请麦克风授权
//						mActivity.requestPermissions(new String[] { Manifest.permission.RECORD_AUDIO }, MICRO_CODE);
//					}
//					
//				}
//			} else {
//				// lDialogDismiss(false);
//			}
//
//			break;
//		case 111: {
//			if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//				Toast.makeText(mActivity, getString(R.string.micro_authorized), Toast.LENGTH_SHORT).show();				
//				musicFragment.requestMicroPermissionsSucess();
////				musicFragment.startRecord();
//			}else {
////				Toast.makeText(mActivity, "拒绝麦克风权限", Toast.LENGTH_SHORT).show();
//			}
//		}
//			break;
//		}
//	}
//    
    protected void initScanCallback() {
		if (Build.VERSION.SDK_INT >= 23) {
			scanCallback = new ScanCallback() {
		        @Override
		        @SuppressLint("NewApi")
		        public void onScanResult(int callbackType, ScanResult result) {
//		            super.onScanResult(callbackType, result);
		            BluetoothDevice device = result.getDevice();
		            int rssi = result.getRssi();//获取rssi
		            //这里写你自己的逻辑
		    
//		            Toast.makeText(mActivity, "deviceName= "+device.getName(), Toast.LENGTH_SHORT).show(); 
		            if (!bleDevicesArray.contains(device) && device.getName() != null) {
						String name = device.getName();			

						if (name.startsWith(BluetoothLeServiceSingle.NAME_START_ELK)
							|| name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)) {
							
							bleDevicesArray.add(device);
							updateNewFindDevice();						
							
							LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
									+ LedBleApplication.getApp().getBleDevices().size());								
							conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
							
						}
					}
		        }
		    };
        }
	}
	

//	/**
//	 * 添加组
//	 */
//	private void addGroupMessage() {
//
//		final EditText editText = new EditText(mActivity);
//		new AlertDialog.Builder(mActivity).setTitle(R.string.please_input).setIcon(android.R.drawable.ic_dialog_info)
//				.setView(editText).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//						String gnameString = editText.getText().toString();
//						
//						GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
//						ArrayList<Group> groups = gDao.getAllgroup();
//						for (Group group : groups) {
//							if (group.getGroupName().equalsIgnoreCase(gnameString)) { //不能添加相同组名的组
//								Tool.ToastShow(mActivity, R.string.groupname_cannot_same);
//								return;
//							}
//						}
//						
//						if (!StringUtils.isEmpty(gnameString)) {
//							addGroupByName(gnameString);
//						}
//					}
//				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//					}
//				}).show();
//	}
//
//	private void addGroupByName(String groupName) {
//		try {
//			// 添加组数据库
//			GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
//			groupDeviceDao.addGroup(groupName);
//			addGroupView(groupName);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	private void addGroupView(final String groupname) {
//		// 添加组视图
//		final GroupView groupView = new GroupView(mActivity, groupname, isAllOn);
//		final SlideSwitch slideSwitch = groupView.getSlideSwitch();
//		linearGroups.addView(groupView.getGroupView());
//		map.put(groupname, slideSwitch);
//		slideSwitch.setStateNoListener(false);
//		slideSwitch.setSlideListener(new SlideListener() {
//
//			@Override
//			public void open() {
//				changeStatus(groupname);
//			}
//
//			@Override
//			public void close() {
//				slideSwitch.setStateNoListener(true);
//			}
//		});
//		groupView.getGroupView().setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
////				groupName = groupname;
//				if (groupView.isTurnOn()) {
//					if (groupView.getConnect() > 0) {
//						// gotoMain(groupName, groupView);
//					} else {
//						Tool.ToastShow(mActivity, R.string.edit_group_please);
////						gotoEdit(groupName);
//						showActionSheet(groupname);
//					}
//				} else {
////					gotoEdit(groupName);
//					showActionSheet(groupname);
//				}
//			}
//		});
//		groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
//			@Override
//			public boolean onLongClick(View v) {
//				showDeleteDialog(groupname);
//				return true;
//			}
//		});
//
//		arrayListGroupViews.add(groupView);
//	}
//
//	private void changeStatus(String groupName) {
//		setGroupName(groupName);
//		for (Iterator<String> i = map.keySet().iterator(); i.hasNext();) {
//			String key = i.next();
//			if (!groupName.equals(key)) {
//				map.get(key).setStateNoListener(false);
//			}
//		}
//	}
//
//	/**
//	 * 编辑组
//	 * 
//	 * @param groupName
//	 */
//	private void gotoEdit(final String groupName) {
//		Intent intent = new Intent(mActivity, DeviceListActivity.class);
//		intent.putExtra("group", groupName);
//		GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
//		ArrayList<GroupDevice> devices = groupDeviceDao.getDevicesByGroup(groupName);
//		if (!ListUtiles.isEmpty(devices)) {
//			intent.putExtra("devices", devices);
//		}
//		startActivityForResult(intent, INT_GO_LIST);
//	}
	

	// =============================================更换皮肤=====================================================//
	/**
	 * 更换皮肤相关 : 保存图片地址
	 */
	private void saveImagePathToSharedPreferences(String imagePath) {

		SharedPreferences sharedPreferences = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
		Editor editor = sharedPreferences.edit();
		editor.putString(Constant.IMAGE_VALUE, imagePath);
		editor.commit();
	}

	/**
	 * 更换皮肤相关 : 打开相册
	 */
	public void showPicturePicker(Context context) {

		if (Build.VERSION.SDK_INT >= 23) {			
			if (mActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // 申请授权
				mActivity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_CODE);					

            } else {
            	Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
        		openAlbumIntent.setType("image/*");
        		startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
            }			
		}else {
			Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
			openAlbumIntent.setType("image/*");
			startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
		}
	}

	/**
	 * 更换皮肤相关 : 获取保存过的图片路径
	 */
	private String getImagePath() {
		sp = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
		editor = sp.edit();
		String imagePath = sp.getString(Constant.IMAGE_VALUE, "");

		return imagePath;
	}

	/**
	 * 更换皮肤相关 : 加载图片
	 */
	private void showImage(String imaePath) {

		if (null != bm && !bm.isRecycled()) {
			bm.recycle();
			bm = null;
			System.gc();
		}
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize=2;//图片高宽度都为原来的二分之一，即图片大小为原来的大小的四分之一  
		options.inTempStorage = new byte[5*1024]; //设置16MB的临时存储空间（不过作用还没看出来，待验证
		Bitmap bitMap = BitmapFactory.decodeFile(imaePath, options);
		imageView.setImageBitmap(bitMap);
		
	}
	
	/**
     * 根据图片的Uri获取图片的绝对路径(已经适配多种API)
     * @return 如果Uri对应的图片存在,那么返回该图片的绝对路径,否则返回null
     */
    public static String getRealPathFromUri(Context context, Uri uri) {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion < 11) {
            // SDK < Api11
            return getRealPathFromUri_BelowApi11(context, uri);
        }
        if (sdkVersion < 19) {
            // SDK > 11 && SDK < 19
            return getRealPathFromUri_Api11To18(context, uri);
        }
        // SDK > 19
        return getRealPathFromUri_AboveApi19(context, uri);
    }

    /**
     * 适配api19以上,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_AboveApi19(Context context, Uri uri) {
        String filePath = null;
        String wholeID = DocumentsContract.getDocumentId(uri);

        // 使用':'分割
        String id = wholeID.split(":")[1];

        String[] projection = { MediaStore.Images.Media.DATA };
        String selection = MediaStore.Images.Media._ID + "=?";
        String[] selectionArgs = { id };

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, null);
        int columnIndex = cursor.getColumnIndex(projection[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    /**
     * 适配api11-api18,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_Api11To18(Context context, Uri uri) {
        String filePath = null;
        String[] projection = { MediaStore.Images.Media.DATA };

        CursorLoader loader = new CursorLoader(context, uri, projection, null,
                null, null);
        Cursor cursor = loader.loadInBackground();

        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }

    /**
     * 适配api11以下(不包括api11),根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_BelowApi11(Context context, Uri uri) {
        String filePath = null;
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection,
                null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }
	
	

	// =============================================蓝牙操作=====================================================//

	/**
	 * 创建广播过滤器
	 * 
	 * @return
	 */
	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_CONNECTED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE);
		return intentFilter;
	}

	

	/**
//	 * 成功扫描设备
//	 */
//	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
//
//		@Override
//		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
//
//			mActivity.runOnUiThread(new Runnable() {
//				@Override
//				public void run() {
//					if (device != null) {
//						if (!LedBleApplication.getApp().getBleDevices().contains(device) && device.getName() != null) {
//							String name = device.getName();			
//
//							if (name.startsWith(BluetoothLeServiceSingle.NAME_START_ELK)
//								|| name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)) {
//								
////								if (name.equalsIgnoreCase(ILLEGAL_DEVICE_NAME)) {
////									if (!isShowIllegal) {
////										Toast.makeText(mActivity, R.string.Illegal_equipment, Toast.LENGTH_SHORT).show();
////										isShowIllegal = true;
////									}									
////									return;
////								}
//								
//								LedBleApplication.getApp().getBleDevices().add(device);
//								updateNewFindDevice();						
//								
//								LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
//										+ LedBleApplication.getApp().getBleDevices().size());								
//								conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
//								
//							}
//						}
//					}
//				}
//			});
//		}
//	};


	/**
	 * 更新发现新的设备
	 */
	private void updateNewFindDevice() {
		String connected = getResources().getString(R.string.conenct_device,
				bleDevicesArray.size(), hashMapConnect.size());
		textViewAllDeviceIndicater.setText(connected);
		
	}

	/**
	 * 更新连接设备数
	 */
	private void updateDevieConnect() {

		for (int i = 0, isize = arrayListGroupViews.size(); i < isize; i++) {
			GroupView gv = arrayListGroupViews.get(i);
			ArrayList<GroupDevice> gdes = gv.getGroupDevices();
			if (!ListUtiles.isEmpty(gdes)) {
				int connectCount = 0;
				for (GroupDevice groupDevice : gdes) {
					String address = groupDevice.getAddress();
					if (hashMapConnect.containsKey(address) && hashMapConnect.get(address)) {
						connectCount++;
					}
				}
				gv.setConnectCount(connectCount);
			}
		}
		
		final String connected = getResources().getString(R.string.conenct_device,
				bleDevicesArray.size(), hashMapConnect.size());
		textViewAllDeviceIndicater.setText(connected);
		
		if (groupName.equalsIgnoreCase("")) {
			textViewConnectCount.setText(Integer.toString(hashMapConnect.size()));
		}
	}

	private boolean booleanCanStart = false;
	private Handler conectHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case MSG_START_CONNECT:// 可以开始连接了
				if (booleanCanStart == false) {
					booleanCanStart = true;
					// updateNewFindDevice();
					Tool.delay(100); //connect之前 延时300ms
					startConnectDevices();
				}
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 开始连接设备，设备的连接是异步的，必须等到一个设备连接成功后(发现service)才能连接新的设备
	 */
	private void startConnectDevices() {
		System.out.println("mBluetoothLeService:" + mBluetoothLeService);
		final int delayTime =  /*50*/ 300;
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true && null != mBluetoothLeService) {
					ArrayList<BluetoothDevice> bldevices = bleDevicesArray;
					try {
						for (BluetoothDevice bluetoothDevice : bldevices) {
							final String address = bluetoothDevice.getAddress();
							final String name = bluetoothDevice.getName();
							if (!LedBleApplication.getApp().getBleGattMap().containsKey(address)
									&& null != mBluetoothLeService) {// 如果不存在，可以连接设备了

//								Tool.delay(delayTime); //connect之前 延时300ms
								
								mBluetoothLeService.connect(address, name);
								hashMapLock.put(address, false);// 上锁
								
								while (true) {// 如果已经解锁那就可以进行下一次连接了
									Tool.delay(delayTime);
									if (hashMapLock.get(address)) {
										break;
									}
								}
							}
							Tool.delay(delayTime);
						}

					} catch (Exception e) {
						e.printStackTrace();// 防止出现并发修改异常
					}
					Tool.delay(delayTime);
				}
			}
		}).start();

	}
	

	/**
	 * 广播监听回调
	 */
	private BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			System.out.println("接收廣播action-->" + action);
			if (BluetoothLeServiceSingle.ACTION_GATT_CONNECTED.equals(action)) {// 连接到设备
				String address = intent.getStringExtra("address");
				hashMapConnect.put(address, true);
				updateDevieConnect();
				LogUtil.i(LedBleApplication.tag, "connect:" + address);		

			} else if (BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED.equals(action)) {// 设断开备

				String address = intent.getStringExtra("address");
//				Toast.makeText(mActivity, "address = "+address, Toast.LENGTH_SHORT).show();
				
				hashMapConnect.put(address,false);//删除连接计数

				hashMapConnect.remove(address);
				LedBleApplication.getApp().removeDisconnectDevice(address);// 删除断开的device
				LedBleApplication.getApp().getBleGattMap().remove(address);// 删除断开的blgatt
				bleDevicesArray.remove(address);// 删除断开的blgatt
				
				
				for (BluetoothDevice dev : bleDevicesArray) {
					if (!hashMapConnect.containsKey(dev.getAddress())) {
						bleDevicesArray.remove(address);// 删除断开的blgatt
					}
				}
				
				hashMapLock.remove(address);// 删除锁
				updateDevieConnect();
				
				
				
				LogUtil.i(LedBleApplication.tag, "disconnect:" + address + " connected devices:"
						+ LedBleApplication.getApp().getBleDevices().size());
				

			} else if (BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {// 发现service,就可以获取Characteristic
				// 发现之后依次连接
				String address = intent.getStringExtra("address");
				BluetoothGatt blgat = mBluetoothLeService.getBluetoothGatt();
				// 控制连接要求的设备
				// BluetoothGattService service32 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid32));
				// BluetoothGattService service33 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid33));
				// BluetoothGattService service34 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid34));
				// if (service32 != null || service33 != null || service34 !=
				// null) {
				LedBleApplication.getApp().getBleGattMap().put(address, blgat);
				hashMapLock.put(address, true);// 解锁
				LogUtil.i(LedBleApplication.tag, "发现service" + intent.getStringExtra("address"));
				// }
			} else if (BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE.equals(action)) {// 读取到数据，不做处理，本应用不需要读取数据
				// displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
			}
		}
	};

//	/**
//	 * 首次初始化组视图
//	 */
//	private void initGroup(boolean isAllOn) {
//		GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
//		ArrayList<Group> groups = gDao.getAllgroup();
//		for (Group group : groups) {
//			addGroupViewFromInit(group.getGroupName(), group.getIsOn(), isAllOn);
//		}
//	}
//
//	private void addGroupViewFromInit(final String groupname, final String ison, final boolean isAllOn) {
//		// 添加组视图
//		final GroupView groupView = new GroupView(mActivity, groupname, isAllOn);
//
//		GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
//		final ArrayList<GroupDevice> groupDevices = groupDeviceDao.getDevicesByGroup(groupname);// 相同组的所有设备
//		if (!ListUtiles.isEmpty(groupDevices)) {
//			groupView.setGroupDevices(groupDevices);
//		}
//
//		final SlideSwitch slideSwitch = groupView.getSlideSwitch();
//		map.put(groupname, slideSwitch);
//		slideSwitch.setStateNoListener(false);
//		slideSwitch.setSlideListener(new SlideListener() {
//
//			@Override
//			public void open() {
//				changeStatus(groupname);
//			}
//
//			@Override
//			public void close() {
//				slideSwitch.setStateNoListener(true);
//			}
//		});
//
//		groupView.getGroupView().setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
////				groupName = groupname;
//				if (groupView.isTurnOn()) {
//					if (groupView.getConnect() > 0) {
//						// gotoMain(groupName, groupView);
//					} else {
//						Tool.ToastShow(mActivity, R.string.edit_group_please);
////						gotoEdit(groupName);
//						showActionSheet(groupname);
//					}
//				} else {
////					gotoEdit(groupName);
//					showActionSheet(groupname);
//				}
//			}
//		});
//
//		linearGroups.addView(groupView.getGroupView());
//		groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
//			@Override
//			public boolean onLongClick(View v) {
//				showDeleteDialog(groupname);
//				return true;
//			}
//		});
//
//		if ("y".equalsIgnoreCase(ison) && isAllOn) {
//			groupView.turnOn();
//		} else {
//			groupView.turnOff();
//		}
//		arrayListGroupViews.add(groupView);
//	}
//
//	private void showDeleteDialog(final String groupName) {
//
//		Dialog alertDialog = new AlertDialog.Builder(mActivity).setTitle(getResources().getString(R.string.tips))
//				.setMessage(getResources().getString(R.string.delete_group, groupName))
//				.setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//						try {
//							GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
//							gDao.deleteGroup(groupName);
//							gDao.delteByGroup(groupName);
//							linearGroups.removeView(linearGroups.findViewWithTag(groupName));
//							map.remove(groupName);
//						} catch (Exception e) {
//							e.printStackTrace();
//						}
//					}
//				}).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						dialog.dismiss();
//					}
//
//				}).create();
//
//		alertDialog.show();
//
//	}
//
//	/**
//	 * 保存设备到某个组
//	 * 
//	 * @param groupName
//	 * @param selectSet
//	 * @throws Exception
//	 */
//	private void save2GroupByGroupName(String groupName, Set<BluetoothDevice> selectSet) throws Exception {
//		GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
//		// 删除原来组的设备，重新添加新的设备
//		groupDeviceDao.delteByGroup(groupName);// 删除原有的设备
//		ArrayList<GroupDevice> groupDevices = new ArrayList<GroupDevice>();
//		for (BluetoothDevice bluetoothDevice : selectSet) {
//			GroupDevice object = new GroupDevice();
//			object.setAddress(bluetoothDevice.getAddress());
//			object.setGroupName(groupName);
//			groupDevices.add(object);
//		}
//		groupDeviceDao.save2Group(groupDevices);
//	}
//
//	/**
//	 * 计算组中已经连接的设备
//	 * 
//	 * @param groupDevices
//	 * @return
//	 */
//	private int findConnectCount(ArrayList<GroupDevice> groupDevices) {
//		if (ListUtiles.isEmpty(groupDevices)) {
//			return 0;
//		}
//		int connectCount = 0;
//		for (GroupDevice groupDevice : groupDevices) {
//			if (hashMapConnect.containsKey(groupDevice.getAddress()) && hashMapConnect.get(groupDevice.getAddress())) {
//				connectCount++;
//			}
//		}
//		return connectCount;
//	}
//	
//	public void showActionSheet(String groupname) {
//		
//		NetConnectBle.getInstanceByGroup(groupname);
//		
//		if (groupname.equalsIgnoreCase("")) {
//			Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
//			Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white, getResources().getString(R.string.text_cancel));
//
//			ActionSheet.createBuilder(this, this.getFragmentManager()).setCancelItem(cancelItem)
//					.setmOtherItems(item1).setGroupName(groupname).setCancelableOnTouchOutside(true).setListener(this).show();
//		}else {
//			Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
//			Item item2 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.add_device));
//			Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white, getResources().getString(R.string.text_cancel));
//
//			ActionSheet.createBuilder(this, this.getFragmentManager()).setCancelItem(cancelItem)
//					.setmOtherItems(item1, item2).setGroupName(groupname).setCancelableOnTouchOutside(true).setListener(this).show();
//		}
//	}
//
//	@Override
//	public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
//		// TODO Auto-generated method stub
////		Toast.makeText(getApplicationContext(), "dismissed isCancle = " + isCancel, Toast.LENGTH_SHORT).show();
//	}
//
//	@Override
//	public void onOtherButtonClick(ActionSheet actionSheet, int index, String groupName) {
//		// TODO Auto-generated method stub
//		switch (index) {
//		case 0:
////			Toast.makeText(getApplicationContext(), "控制的组 = "+groupName, Toast.LENGTH_SHORT).show();
//			if (hashMapConnect.size() > 0) {
//				this.groupName = groupName;
//				if (groupName.equalsIgnoreCase("")) {
//					textViewConnectCount.setText(Integer.toString(hashMapConnect.size()));
//				}else {
//					textViewConnectCount.setText(Integer.toString(NetConnectBle.getInstance().getDevicesByGroup().size()));
//				}
//			}else {
//				Toast.makeText(getApplicationContext(), R.string.no_device_found, Toast.LENGTH_SHORT).show();
//			}
//			
//			break;
//		case 1:
//			gotoEdit(groupName);
//			break;
//
//		default:
//			break;
//		}
//	}
//	
//
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && requestCode == INT_GO_LIST) {
			try {
//				String grop = data.getStringExtra("group");
//				save2GroupByGroupName(grop, LedBleApplication.getApp().getTempDevices());// 保存新的组到数据库
//				// ==========
//				GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
//				ArrayList<GroupDevice> list = groupDeviceDao.getDevicesByGroup(grop);
//
//				for (GroupView groupView : arrayListGroupViews) {
//					if (grop.equals(groupView.getGroupName())) {
//						// 设置已经连接的设备数量
//						groupView.setGroupDevices(list);
//						int count = findConnectCount(list);
//						LogUtil.i(LedBleApplication.tag, "count:" + count);
//						groupView.setConnectCount(count);
//						break;
//					}
//				}

			} catch (Exception e) {
				e.printStackTrace();
				Tool.ToastShow(mActivity, "保存失败！");
			}
		} else if (resultCode == Activity.RESULT_OK && requestCode == OPEN_BLE) {

			if (Build.VERSION.SDK_INT >= 23) {// 安卓6.0以上的方案
				if (mBluetoothAdapter != null) {
					bleScanner = mBluetoothAdapter.getBluetoothLeScanner();// 用过单例的方式获取实例
				}
//				initScanCallback(); // 初始化 ScanCallback
			}else {
				mBluetoothAdapter = bluetoothManager.getAdapter();
			}			
			
			if (mBluetoothAdapter.isEnabled()) {
//				refreshDevices(false);
			}

		} else {
			if (resultCode == RESULT_OK) {
				switch (requestCode) {
				case TAKE_PICTURE:
					break;
				case CHOOSE_PICTURE: // 更换皮肤
					ContentResolver resolver = getContentResolver();
					// 照片的原始资源地址
					if (null != data) {
						Uri originalUri = data.getData();
						if (null != originalUri) {
							try {
								
								if (null != bm && !bm.isRecycled()) { //回收图片资源
									bm.recycle();
									bm = null;
									System.gc();
								}
								
								// 使用ContentProvider通过URI获取原始图片		
								bm = MediaStore.Images.Media.getBitmap(resolver, originalUri);
								if (bm != null) {

									String img_path = getRealPathFromUri(mActivity, originalUri);// 这是本机的图片路径
									showImage(img_path);

									saveImagePathToSharedPreferences(img_path);// 保存图片
								}
							} catch (FileNotFoundException e) {
								e.printStackTrace();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}				
					}
					
					
					break;

				default:
					break;
				}
			}

		}

	}
	
	@Override
	public void onSensorChanged(SensorEvent sensorEvent) {
		int sensorType = sensorEvent.sensor.getType();
		float[] values = sensorEvent.values;

		if (sensorType == Sensor.TYPE_ACCELEROMETER) {
			if ((Math.abs(values[0]) > 19) || Math.abs(values[1]) > 19 || Math.abs(values[2]) > 19) {

				switch (shakeStyle) {
				case 0:
					soundPool.play(soundID, 1, 1, 0, 0, 1);
//					setRgb(random.nextInt(255)+ 1, random.nextInt(255)+ 1, random.nextInt(255)+ 1);
					setRgb(random.nextInt(255)+ 1, random.nextInt(255)+ 1, random.nextInt(255)+ 1, false);
					break;
				case 1:

					break;
				case 2:
					soundPool.play(soundID, 1, 1, 0, 0, 1);
//					setSPIModel(random.nextInt(156 - 135 + 1) + 135);
					setRegMode(random.nextInt(156 - 135 + 1) + 135);
					break;

				default:
					break;
				}
			}
		}
	}

//	public void setService(BluetoothLeServiceSingle service) {
//		this.mBluetoothLeService = service;
//	}
//
//	// =========================================业务模式=================================================
//	
//	public String getGroupName() {
//		return groupName;
//	}
//
//	public void setGroupName(String groupName) {
//		this.groupName = groupName;
//	}
//
//	public boolean open() {
////		showCheckSelfPermission();
//		NetConnectBle.getInstanceByGroup(groupName).turnOn(sceneBean);
//		return false;
//	}
//
//	public boolean close() {
////		showCheckSelfPermission();
//		NetConnectBle.getInstanceByGroup(groupName).turnOff(sceneBean);
//		return false;
//	}
//	
//	/**
//	 * 全开
//	 */
//	private void allOn() {
////		showCheckSelfPermission();
//		NetConnectBle.getInstanceByGroup("").turnOn(sceneBean);
//	}
//
//	/**
//	 * 全关
//	 */
//	private void allOff() {
////		showCheckSelfPermission();
//		NetConnectBle.getInstanceByGroup("").turnOff(sceneBean);
//	}
//
////	public void setRgb(int r, int g, int b) {
//////		showCheckSelfPermission();
////		NetConnectBle.getInstanceByGroup(groupName).setRgb(r, g, b, sceneBean);
////	}
//
	public void setSPIModel(int model) {
//		NetConnectBle.getInstanceByGroup(groupName).setSPIModel(model);
	}
//	
//	// 律动 模式
//	public void setDynamicModel(int model) {
////		showCheckSelfPermission();
//		NetConnectBle.getInstanceByGroup(groupName).setDynamicModel(model);
//	}
//	
	public void setRgbSort(int bannerSort) {
		
//		NetConnectBle.getInstanceByGroup(groupName).SetRgbSort(rgbSort);

		pauseMusicAndVolum(true);
		
		bTmp1 = 0x7e;
		bTmp2 = 0x04;
		bTmp3 = 0x08;
		bTmp4 = bannerSort;
		bTmp5 = 0xff;
		bTmp6 = 0xff;
		bTmp7 = 0xff;
		bTmp8 = 0x00;
		bTmp9 = 0xef;
		bTmp10 = 0x01;
		
		sendClickBroadcast();
	}
//	
//	// 对码
//	public void setPairCode(int rgbSort) {
//		// if (isLightOpen == false) {
//		// return;
//		// }
////		this.brightness = brightness;// 设置亮度参数
//		NetConnectBle.getInstanceByGroup(groupName).SetPairCode(rgbSort);
//
//	}
//	
//	public void setTimerFirData( int style) {
//		NetConnectBle.getInstanceByGroup(groupName).setTimerFirData(style);;
//	}
//	
//	public void setTimerSecData(int[] data) {
//		NetConnectBle.getInstanceByGroup(groupName).setTimerSecData(data);
//	}
//	
	public void turnOn() {
		pauseMusicAndVolum(true);
		
		bTmp2 = 0x04;
		bTmp3 = 0x04;
		bTmp4 = 0x01;
		bTmp5 = 0xff;
		bTmp6 = 0xff;
		bTmp7 = 0xff;
		bTmp8 = 0x00;
		bTmp8 = 0x00;
		bTmp10 = 0x01;
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;						
			bTmp9 = 0xef;			
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;									
			bTmp9 = 0xbf;
		} 						
		
		if (count == 0) {
			sendClickBroadcast();							
		}
	}
	
	public void turnOff() {
		pauseMusicAndVolum(true);
		
		bTmp2 = 0x04;
		bTmp3 = 0x04;
		bTmp4 = 0x00;
		bTmp5 = 0xff;
		bTmp6 = 0xff;
		bTmp7 = 0xff;
		bTmp8 = 0x00;
		bTmp8 = 0x00;
		bTmp10 = 0x01;
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;						
			bTmp9 = 0xef;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;									
			bTmp9 = 0xbf;
		} 
		
		if (count == 0) {
			sendClickBroadcast();							
		}
	}
	
//	public void turnOnOffTimerOn(int onOrOff) {
//		NetConnectBle.getInstanceByGroup(groupName).turnOnOffTimerOn(onOrOff, sceneBean);
//	}
//
//	public void timerOn(int hour, int minute, int model) {
//		NetConnectBle.getInstanceByGroup(groupName).timerOn(hour, minute, model, sceneBean);
//	}
//
//	public void turnOnOffTimerOff(int onOrOff) {
//		NetConnectBle.getInstanceByGroup(groupName).turnOnOrOffTimerOff(onOrOff, sceneBean);
//	}
//
//	public void timerOff(int hour, int minute) {
//		NetConnectBle.getInstanceByGroup(groupName).timerOff(hour, minute, sceneBean);
//	}
//
	public void setRgb(int r, int g, int b, boolean isClick) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x07;
			bTmp3 = 0x05;
			bTmp4 = 0x03;
			bTmp5 = r;
			bTmp6 = g;
			bTmp7 = b;
			bTmp8 = 0x00;
			bTmp9 = 0xef;
			bTmp10 = 0x01;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x07;
			bTmp4 = r;
			bTmp5 = g;
			bTmp6 = b;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 
				
		if (isClick) {
			sendClickBroadcast();
		}else {
//			if (canSend) {
				sendSlideBroadcast();
//			}
		}
	}

	public void setDim(int dim) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x05;
			bTmp3 = 0x05;
			bTmp4 = 0x01;
			bTmp5 = dim;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x08;
			bTmp9 = 0xef;
			bTmp10 = 0x01;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x09;
			bTmp4 = dim;
			bTmp5 = 0x00;
			bTmp6 = 0x00;
			bTmp7 = 0x00;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 		
		
		sendSlideBroadcast();
			
	}

//	public void setDimModel(int dimModel) { 
//		pauseMusicAndVolum(true);
//		
//		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
//			
//		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
//			
//		} 
//		bTmp1 = 0x7b;
//		bTmp2 = 0x04;
//		bTmp3 = 0x09;
//		bTmp4 = dimModel;
//		bTmp5 = 0x00;
//		bTmp6 = 0x00;
//		bTmp7 = 0x00;
//		bTmp8 = 0x00;
//		bTmp9 = 0xbf;
//		bTmp10 = 0x01;
//		sendClickBroadcast();
//	}
//
	public void setRegMode(int model) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x05;
			bTmp3 = 0x03;
			bTmp4 = model;
			bTmp5 = 0x03;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xef;
			bTmp10 = 0x01;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x03;
			bTmp4 = model;
			bTmp5 = 0x03;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 		
		
//		if (count == 0) {
			sendClickBroadcast();							
//		}
	}
	
	public void setBrightNess(int brightness) {	
		pauseMusicAndVolum(true);
		
		bTmp2 = 0x04;
		bTmp3 = 0x01;
		bTmp4 = brightness;
		bTmp5 = 0xff;
		bTmp6 = 0xff;
		bTmp7 = 0xff;
		bTmp8 = 0x00;
		bTmp10 = 0x01;
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp9 = 0xef;			
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;			
			bTmp9 = 0xbf;
		} 		
		
		sendSlideBroadcast();
	}

	public void setSpeed(int speed) {
		pauseMusicAndVolum(true);
		
		bTmp2 = 0x04;
		bTmp3 = 0x02;
		bTmp4 = speed;
		bTmp5 = 0xff;
		bTmp6 = 0xff;
		bTmp7 = 0xff;
		bTmp8 = 0x00;
		bTmp10 = 0x01;
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;	
			bTmp9 = 0xef;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;			
			bTmp9 = 0xbf;			
		} 		
		
		sendSlideBroadcast();
	}

	public void setDiy(final ArrayList<ColorTextView> colorTextViews, final ArrayList<MyColor> colors, final int style) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x04;
			bTmp3 = 0x09;
			bTmp4 = style;
			bTmp8 = colors.size();
			bTmp9 = 0xef;
			
//			Toast.makeText(mActivity, "colorTextViews.size = "+colorTextViews.size(), Toast.LENGTH_SHORT).show();		
			final Handler handler = new Handler(); // 开启 定时搜索 定时器
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					handler.postDelayed(this, 1000);// 打开定时器，执行操作		
										
					if ( customCount <= colors.size()) {
												
						bTmp5 = colors.get(customCount).r;
						bTmp6 = colors.get(customCount).g;
						bTmp7 = colors.get(customCount).b;

						bTmp10 = customCount;
						if (count == 0) {
							sendClickBroadcast();
							// Toast.makeText(mActivity, ""+customCount,
							// Toast.LENGTH_SHORT).show();
						}
					}
					
					if (customCount >= colors.size()-1) {
						customCount = -1;
						handler.removeCallbacks(this);					
					}
					
					customCount++;			
				}
			};
			handler.postDelayed(runnable, 10);// 打开定时器，执行操作	
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x0e;
			bTmp4 = style;
			bTmp8 = colors.size();
			bTmp9 = 0xbf;
			
//			Toast.makeText(mActivity, "colorTextViews.size = "+colorTextViews.size(), Toast.LENGTH_SHORT).show();
			
			if (customCount < colors.size()-1 && customCount > 0) {
				return;					
			}
			
			final Handler handler = new Handler(); // 开启 定时搜索 定时器
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					handler.postDelayed(this, 1000);// 打开定时器，执行操作		
					
					if ( customCount <= colors.size()) {
						
						ColorTextView ctx = colorTextViews.get(customCount);
						if (0 != ctx.getColor()) {
							int[] rgb = Tool.getRGB(ctx.getColor());
							bTmp5 = rgb[0];
							bTmp6 = rgb[1];
							bTmp7 = rgb[2];
							
							bTmp10 = customCount;
							if (count == 0) {
								sendClickBroadcast();
//								Toast.makeText(mActivity, ""+customCount, Toast.LENGTH_SHORT).show();
							}
						}
					}
					
					if (customCount >= colors.size()-1) {
						customCount = -1;
						handler.removeCallbacks(this);					
					}
					
					customCount++;			
				}
			};
			handler.postDelayed(runnable, 10);// 打开定时器，执行操作	
		} 
			
	}
	
	public void setMusicDiy( final ArrayList<MyColor> colors, final int style) {
		
//		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
//			bTmp1 = 0x7e;
//			bTmp2 = 0x04;
//			bTmp3 = 0x09;
//			bTmp4 = style;
//			bTmp8 = colors.size();
//			bTmp9 = 0xef;
//			
////			Toast.makeText(mActivity, "colorTextViews.size = "+colorTextViews.size(), Toast.LENGTH_SHORT).show();
//			if (customCount < colors.size()-1 && customCount > 0) {
//				return;
//			}
//			
//			final Handler handler = new Handler(); // 开启 定时搜索 定时器
//			Runnable runnable = new Runnable() {
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					// 在此处添加执行的代码
//					handler.postDelayed(this, 1000);// 打开定时器，执行操作		
//					
//					if ( customCount <= colors.size()) {
//											
//						bTmp5 = colors.get(customCount).r;
//						bTmp6 = colors.get(customCount).g;
//						bTmp7 = colors.get(customCount).b;
//
//						bTmp10 = customCount;
//						sendClickBroadcast();
////						Toast.makeText(mActivity, ""+customCount, Toast.LENGTH_SHORT).show();
//					}
//					
//					if (customCount >= colors.size()-1) {
//						customCount = -1;
//						handler.removeCallbacks(this);					
//					}
//					
//					customCount++;			
//				}
//			};
//			handler.postDelayed(runnable, 10);// 打开定时器，执行操作	
//		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
//			bTmp1 = 0x7b;
//			bTmp2 = 0x04;
//			bTmp3 = 0x0e;
//			bTmp4 = style;
//			bTmp8 = colors.size();
//			bTmp9 = 0xbf;
//			
////			Toast.makeText(mActivity, "colorTextViews.size = "+colorTextViews.size(), Toast.LENGTH_SHORT).show();
//			
//			final Handler handler = new Handler(); // 开启 定时搜索 定时器
//			Runnable runnable = new Runnable() {
//				@Override
//				public void run() {
//					// TODO Auto-generated method stub
//					// 在此处添加执行的代码
//					handler.postDelayed(this, 1000);// 打开定时器，执行操作		
//					
//					if ( customCount <= colors.size()) {
//											
//						bTmp5 = colors.get(customCount).r;
//						bTmp6 = colors.get(customCount).g;
//						bTmp7 = colors.get(customCount).b;
//
//						bTmp10 = customCount;
//						sendClickBroadcast();
////						Toast.makeText(mActivity, ""+customCount, Toast.LENGTH_SHORT).show();
//					}
//					
//					if (customCount >= colors.size()-1) {
//						customCount = -1;
//						handler.removeCallbacks(this);					
//					}
//					
//					customCount++;			
//				}
//			};
//			handler.postDelayed(runnable, 10);// 打开定时器，执行操作
//		} 
			
	}

	
	public void setDirection(int direction) {	
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x10;
			bTmp4 = direction;
			bTmp5 = 0xff;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 		
		
		sendClickBroadcast();
	}

	public void setCT(int warm, int cool) {
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x06;
			bTmp3 = 0x05;
			bTmp4 = 0x02;
			bTmp5 = warm;
			bTmp6 = cool;
			bTmp7 = 0xff;
			bTmp8 = 0x08;
			bTmp9 = 0xef;
			bTmp10 = 0x01;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			
		} 
		
		
		sendSlideBroadcast();
	}
//
//	public void setCTModel(int model) {
//	
//		NetConnectBle.getInstanceByGroup(groupName).setColorWarmModel(model);
//	}
//	
	public void setSPIPause(int model) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x06;
			bTmp4 = model;
			bTmp5 = 0xff;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 
		
//		sendSlideBroadcast();
		sendClickBroadcast();
	}
	
	public void setConfigSPI(int bannerType, byte lengthH, byte lengthL, int bannerSort) {
		pauseMusicAndVolum(true);
		
		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			bTmp1 = 0x7e;
			bTmp2 = 0x04;
			bTmp3 = 0x08;
			bTmp4 = bannerSort;
			bTmp5 = 0xff;
			bTmp6 = 0xff;
			bTmp7 = 0xff;
			bTmp8 = 0x00;
			bTmp9 = 0xef;
			bTmp10 = 0x01;
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7B;
			bTmp2 = 0x04;
			bTmp3 = 0x05;
			bTmp4 = bannerType;
			bTmp5 = lengthH;
			bTmp6 = lengthL;
			bTmp7 = bannerSort;
			bTmp8 = 0x00;
			bTmp9 = 0xBF;
			bTmp10 = 0x01;
		} 
		
		
		sendClickBroadcast();
	}
	
	public void setSmartBrightness(int mode, int brightness) {
		pauseMusicAndVolum(true);

		if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_STRIP)) {
			
		}else if (sceneBean.getName().equalsIgnoreCase(CommonConstant.LED_SPI)) {
			bTmp1 = 0x7b;
			bTmp2 = 0x04;
			bTmp3 = 0x08;
			bTmp4 = mode;
			bTmp5 = brightness;
			bTmp6 = 0x00;
			bTmp7 = 0x00;
			bTmp8 = 0x00;
			bTmp9 = 0xbf;
			bTmp10 = 0x01;
		} 	
		
		sendSlideBroadcast();
	}

	@Override
	public void onException(Exception e) {

	}
//
	@Override
	public void onDestroy() {
		super.onDestroy();
		
//		if (null != conectHandler) {
//			conectHandler.removeCallbacksAndMessages(null);
//			conectHandler = null;
//		}
//		
////		unbindService(myServiceConenction);
////		unregisterReceiver(mGattUpdateReceiver);
//		if (null != myServiceConenction) {
//			mActivity.unbindService(myServiceConenction);
//		}
//		if (null != mGattUpdateReceiver) {
//			mActivity.unregisterReceiver(mGattUpdateReceiver);
//		}
//		
//		
////		if (null != sceneBean) {
////			sceneBean = null;
////		}
//		
////		hashMapConnect = null;
////		hashMapLock = null;
//		
		if (null != mActivity) {
			mActivity = null;
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		if (isBackground(mActivity)) { //app进入后台停止扫描
//			Toast.makeText(mActivity, "当前为后台", Toast.LENGTH_SHORT).show();
		}else {
			
//			Toast.makeText(mActivity, "当前为前台", Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected void onResume() { 
		super.onResume();
		MobclickAgent.onResume(mActivity);
		sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
//		Toast.makeText(mActivity, "从后台切换回前台", Toast.LENGTH_SHORT).show();
		
//		if (Build.VERSION.SDK_INT >= 23) {//安卓6.0以上的方案
//			if (checkGPSIsOpen() && isGPSOpen) {
//				refreshDevices(true);
//			}	
//        }
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(mActivity);
		
	}

	/**
	 * 判断app是否进入后台
	 */
	public static boolean isBackground(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
					
					return true;
				} else {			
					return false;
				}
			}
		}
		return false;
	}

	public SegmentedRadioGroup getSegmentDm() {
		return segmentDm;
	}

	public SegmentedRadioGroup getSegmentCt() {
		return segmentCt;
	}

	public SegmentedRadioGroup getSegmentRgb() {
		return segmentRgb;
	}

	public SegmentedRadioGroup getSegmentMusic() {
		return segmentMusic;
	}
	
	public RelativeLayout getRLModeTop() {
		return rlModeTop;
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onOtherButtonClick(ActionSheet actionSheet, int index, String groupName) {
		// TODO Auto-generated method stub
		
	}

}
