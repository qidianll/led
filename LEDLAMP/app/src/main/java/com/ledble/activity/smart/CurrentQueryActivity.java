package com.ledble.activity.smart;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.ledble.base.LedBleApplication;
import com.ledble.constant.CommonConstant;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.ledlamp.R;

public class CurrentQueryActivity extends BaseActivity {

//	private TextView tvshowQueryData;
	
	private LinearLayout linearLayoutTimer1;
	private TextView tvTemperature;
	private TextView tvTime;
	private TextView tvRed;
	private TextView tvGreen;
	private TextView tvBlue;
	private TextView tvWhite;
	private TextView tvCrystal;
	private TextView tvPink;
	
	private LinearLayout linearLayoutTimer2;
	private TextView tvTemperature2;
	private TextView tvTime2;
	private TextView tvRed2;
	private TextView tvGreen2;
	private TextView tvBlue2;
	private TextView tvWhite2;
	private TextView tvCrystal2;
	private TextView tvPink2;
	
	private LinearLayout linearLayoutTimer3;
	private TextView tvTemperature3;
	private TextView tvTime3;
	private TextView tvRed3;
	private TextView tvGreen3;
	private TextView tvBlue3;
	private TextView tvWhite3;
	private TextView tvCrystal3;
	private TextView tvPink3;

	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
		setActive();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_current_query);
		
		this.linearLayoutTimer1 = findLinearLayout(R.id.linearLayoutTimer1);
		this.linearLayoutTimer2 = findLinearLayout(R.id.linearLayoutTimer2);
		this.linearLayoutTimer3 = findLinearLayout(R.id.linearLayoutTimer3);
		
		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.tvQuery: {
					Toast.makeText(getApplicationContext(), getString(R.string.currentquery), Toast.LENGTH_SHORT).show();
					currentQuery();
				}
					break;
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.buttonSave: {
					putDataback();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
		findTextViewById(R.id.tvQuery).setOnClickListener(clickListener);
		
//		tvshowQueryData = findTextViewById(R.id.tvshowQueryData);
		tvTemperature = findTextViewById(R.id.tvTemperature);
		tvTime = findTextViewById(R.id.tvTime1);
		tvRed = findTextViewById(R.id.tvRed);
		tvGreen = findTextViewById(R.id.tvGreen);
		tvBlue = findTextViewById(R.id.tvBlue);
		tvWhite = findTextViewById(R.id.tvWhite);
		tvCrystal = findTextViewById(R.id.tvCrystal);
		tvPink = findTextViewById(R.id.tvPink);
		
		tvTemperature2 = findTextViewById(R.id.tvTemperature2);
		tvTime2 = findTextViewById(R.id.tvTime2);
		tvRed2 = findTextViewById(R.id.tvRed2);
		tvGreen2 = findTextViewById(R.id.tvGreen2);
		tvBlue2 = findTextViewById(R.id.tvBlue2);
		tvWhite2 = findTextViewById(R.id.tvWhite2);
		tvCrystal2 = findTextViewById(R.id.tvCrystal2);
		tvPink2 = findTextViewById(R.id.tvPink2);
		
		tvTemperature3 = findTextViewById(R.id.tvTemperature3);
		tvTime3 = findTextViewById(R.id.tvTime3);
		tvRed3 = findTextViewById(R.id.tvRed3);
		tvGreen3 = findTextViewById(R.id.tvGreen3);
		tvBlue3 = findTextViewById(R.id.tvBlue3);
		tvWhite3 = findTextViewById(R.id.tvWhite3);
		tvCrystal3 = findTextViewById(R.id.tvCrystal3);
		tvPink3 = findTextViewById(R.id.tvPink3);
	}
	
	public void setActive() {
		
		final String[] timerModel = getResources().getStringArray(R.array.select_mode);
		String[] datas = timerModel[SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART)].split(",");	
		
//		Toast.makeText(mActivity, ""+datas[0], Toast.LENGTH_SHORT).show();
		if (datas[0].equalsIgnoreCase(RGB)) {
			this.linearLayoutTimer1.setVisibility(View.VISIBLE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBW)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.VISIBLE);
			this.linearLayoutTimer3.setVisibility(View.GONE);
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			this.linearLayoutTimer1.setVisibility(View.GONE);
			this.linearLayoutTimer2.setVisibility(View.GONE);
			this.linearLayoutTimer3.setVisibility(View.VISIBLE);
		}
//		initViews();
	}
	
	private void currentQuery() {
		
		NetConnectBle.getInstance().setSmartCheck(2);
		
		final Handler handler = new Handler(); // 
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				
				String data = SharePersistent.getPerference(getApplicationContext(), Constant.CurrentQueryActivity);
//				Toast.makeText(getApplicationContext(), "data = "+data, Toast.LENGTH_SHORT).show();
				
				final String[] colorModel = getResources().getStringArray(R.array.select_mode);
				String[] datas = colorModel[SharePersistent.getInt(getApplicationContext(), CommonConstant.SELECT_MODE_SMART)].split(",");	
				
				if (data.length() > 0) {
					String[] strArray = data.split("[ ]"); 			
					for (int i = 0; i < strArray.length; i++) {
						
						if (datas[0].equalsIgnoreCase(RGB)) {
							if (i==2) {
								tvRed.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==3) {
								tvGreen.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==4) {
								tvBlue.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==5) {
								tvWhite.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==6) {
								tvCrystal.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==7) {
								tvPink.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==11 || i==12) {
								tvTime.setText(NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[11],16)) + ":" + NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[12],16)));
							}else if (i==14) {
								tvTemperature.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}
						}else if (datas[0].equalsIgnoreCase(RGBW)) {
							if (i==2) {
								tvRed2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==3) {
								tvGreen2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==4) {
								tvBlue2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==5) {
								tvWhite2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==6) {
								tvCrystal2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==7) {
								tvPink2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==11 || i==12) {
//								tvTime.setText(String.valueOf(Integer.parseInt(strArray[11],16))+":"+String.valueOf(Integer.parseInt(strArray[12],16)));
								tvTime2.setText(NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[11],16)) + ":" + NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[12],16)));
							}else if (i==14) {
								tvTemperature2.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}
						}else if (datas[0].equalsIgnoreCase(RGBWC)) {

						}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
							if (i==2) {
								tvRed3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==3) {
								tvGreen3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==4) {
								tvBlue3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==5) {
								tvWhite3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==6) {
								tvCrystal3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==7) {
								tvPink3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}else if (i==11 || i==12) {
//								tvTime.setText(String.valueOf(Integer.parseInt(strArray[11],16))+":"+String.valueOf(Integer.parseInt(strArray[12],16)));
								tvTime3.setText(NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[11],16)) + ":" + NumberHelper.LeftPad_Tow_Zero(Integer.parseInt(strArray[12],16)));
							}else if (i==14) {
								tvTemperature3.setText(String.valueOf(Integer.parseInt(strArray[i],16)));
							}
						}
						
						
					}
				}				
				
//				textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
				
				SharePersistent.savePerference(getApplicationContext(), Constant.CurrentQueryActivity, "");
				
				handler.removeCallbacks(this);// 关闭定时器处理
			}
		};
		handler.postDelayed(runnable, 1000);// 打开定时器，2秒后执行runnable  
	}

	private void putDataback() {

		Intent intent = new Intent();

		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onResume(this);
	}
}
