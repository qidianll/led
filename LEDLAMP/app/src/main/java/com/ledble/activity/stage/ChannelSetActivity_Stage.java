package com.ledble.activity.stage;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

//import com.baidu.android.pushservice.b.i;
import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.ledble.activity.wifi.MainActivity_WiFi;
import com.ledble.activity.ble.MainActivity_STAGE;
import com.ledble.bean.SceneBean;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.ledlamp.R;

public class ChannelSetActivity_Stage extends BaseActivity {

	private LinearLayout relativeLayout1;
	
	private LinearLayout relativeLayout2;
	
	private LinearLayout relativeLayout3;
	private WheelView listViewH3;
	private WheelView listViewM3;
	private WheelView listViewWhite3;
	private WheelView listViewLightblue3;
	private WheelView listViewRed3;
	private WheelView listViewGreen3;
	private WheelView listViewPink3;
	private WheelView listViewCrystal3;
	private WheelView listViewChannel7;
	private WheelView listViewChannel8;
	private WheelView listViewChannel9;
	private WheelView listViewChannel10;
	

	private WheelModelAdapter wheelAdapterH;
	private WheelModelAdapter wheelAdapterM;
	private WheelModelAdapter wheelAdapterModel;
	
	
	private TextView textViewID;
	
//	private String tag = "";
	private int tag;
	

	private int hour /*= 12*/;
	private int minite /*= 30*/;
	private int model /*= 10*/;
	
	private int rValue = 255;
	private int gValue = 255;
	private int bValue = 255;
	private int wValue = 255;
	private int yValue = 255;
	private int pValue = 255;
	private int tempValue ;
	private int ch8Value ;
	private int ch9Value ;
	private int ch10Value ;
	
	private String modelText = "";
	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";
	
	private ArrayList<Integer> arrayList;
	private ArrayList<Integer> arrayTempList;
	
	public static SceneBean sceneBean;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_channel_set);
		
		arrayList = new ArrayList<Integer>();
		arrayTempList = new ArrayList<Integer>();
		
		
//		if (null != MainActivity_WiFi.getMainActivity()) {
//			sceneBean = MainActivity_WiFi.getSceneBean();
//		}else if (null != MainActivity_STAGE.getMainActivity()) {
//			sceneBean = MainActivity_STAGE.getSceneBean();
//		}
		
	
//		SharePersistent.savePerference(getApplicationContext(), "RGB-SORT", 3-1);
		
		Time t=new Time(); 
		t.setToNow();  
  
		hour = t.hour;    // 0-23
		minite = t.minute;    // 0-23
		
		
		this.relativeLayout1 = (LinearLayout)findViewById(R.id.relativeLayout1);
		this.relativeLayout2 = (LinearLayout)findViewById(R.id.relativeLayout2);
		this.relativeLayout3 = (LinearLayout)findViewById(R.id.relativeLayout3);
		
		



		this.relativeLayout1.setVisibility(View.GONE);
		this.relativeLayout2.setVisibility(View.GONE);
		this.relativeLayout3.setVisibility(View.VISIBLE);

		this.listViewH3 = (WheelView) findViewById(R.id.listViewH3);
		this.listViewM3 = (WheelView) findViewById(R.id.listViewM3);
		this.listViewRed3 = (WheelView) findViewById(R.id.listViewModel31);
		this.listViewGreen3 = (WheelView) findViewById(R.id.listViewModel32);
		this.listViewLightblue3 = (WheelView) findViewById(R.id.listViewModel33);
		this.listViewWhite3 = (WheelView) findViewById(R.id.listViewModel34);
		this.listViewCrystal3 = (WheelView) findViewById(R.id.listViewModel35);
		this.listViewPink3 = (WheelView) findViewById(R.id.listViewModel36);

		this.listViewChannel7 = (WheelView) findViewById(R.id.listViewModel37);
		this.listViewChannel8 = (WheelView) findViewById(R.id.listViewModel38);
		this.listViewChannel9 = (WheelView) findViewById(R.id.listViewModel39);
		this.listViewChannel10 = (WheelView) findViewById(R.id.listViewModel310);

		// this.linearLayoutContainer3 =
		// findViewById(R.id.linearLayoutContainer3);

		// ================

		String[] modelH = new String[24];
		for (int i = 0; i < 24; i++) {
			modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterH = new WheelModelAdapter(this, modelH);
		this.listViewH3.setViewAdapter(wheelAdapterH);
		this.listViewH3.setCurrentItem(hour);
		this.listViewH3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				hour = newValue;
			}
		});

		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterM = new WheelModelAdapter(this, modelM);
		this.listViewM3.setViewAdapter(wheelAdapterM);
		this.listViewM3.setCurrentItem(minite);
		// this.listViewModel.setCyclic(true);
		this.listViewM3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				minite = newValue;
			}
		});		

		
		
		final String[] timerModel = new String[11];
		final String[] modeModel = getApplication().getResources().getStringArray(R.array.channel_set_model);
		for (int i = 0; i < modeModel.length; i++) {
			String label[] = modeModel[i].split(",");
			timerModel[i] = label[0];;
		}

		wheelAdapterModel = new WheelModelAdapter(this, timerModel);

		this.listViewRed3.setViewAdapter(wheelAdapterModel);
		this.listViewRed3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				rValue = Integer.parseInt(datas[1]);
				arrayList.add(rValue);
				
				if (rValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}								
				
				tempValue = rValue;
				if (rValue == 1 && rValue <= 10) {
//					timerModel.															
				}
			}
		});
		this.listViewGreen3.setViewAdapter(wheelAdapterModel);
		this.listViewGreen3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				gValue = Integer.parseInt(datas[1]);
				arrayList.add(rValue);
				
				if (gValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}
												
				tempValue = gValue;
			}
		});
		this.listViewLightblue3.setViewAdapter(wheelAdapterModel);
		this.listViewLightblue3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				bValue = Integer.parseInt(datas[1]);
				arrayList.add(rValue);
				
				if (bValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}								
				
				tempValue = bValue;
			}
		});
		this.listViewWhite3.setViewAdapter(wheelAdapterModel);
		this.listViewWhite3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				wValue = Integer.parseInt(datas[1]);
				arrayList.add(wValue);
				
				if (wValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}								
				
				tempValue = wValue;
			}
		});
		this.listViewPink3.setViewAdapter(wheelAdapterModel);
		this.listViewPink3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				pValue = Integer.parseInt(datas[1]);
				arrayList.add(pValue);
				
				if (wValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}			
								
				tempValue = pValue;
			}
		});
		this.listViewCrystal3.setViewAdapter(wheelAdapterModel);
		this.listViewCrystal3.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = modeModel[newValue].split(",");
				yValue = Integer.parseInt(datas[1]);
				arrayList.add(yValue);
				
				if (wValue == tempValue) {
					Toast.makeText(getApplication(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
				}							
				
				tempValue = yValue;
			}
		});
		
		
		// *********************初始化 **************************//
		
		if (null != MainActivity_STAGE.getMainActivity()) {
			
			int redValue = SharePersistent.getInt(getApplicationContext(), "CH_R_STAGE");
			if (redValue > 0 && redValue != 255) {
				this.listViewRed3.setCurrentItem(redValue);
			}
			int greenValue = SharePersistent.getInt(getApplicationContext(), "CH_G_STAGE");
			if (greenValue > 0 && greenValue != 255) {
				this.listViewGreen3.setCurrentItem(greenValue);
			}
			int blueValue = SharePersistent.getInt(getApplicationContext(), "CH_B_STAGE");
			if (blueValue > 0 && blueValue != 255) {
				this.listViewLightblue3.setCurrentItem(blueValue);
			}
			int whiteValue = SharePersistent.getInt(getApplicationContext(), "CH_W_STAGE");
			if (whiteValue > 0 && whiteValue != 255) {
				this.listViewWhite3.setCurrentItem(whiteValue);
			}
			int pinkValue = SharePersistent.getInt(getApplicationContext(), "CH_P_STAGE");
			if (pinkValue > 0 && pinkValue != 255) {
				this.listViewPink3.setCurrentItem(pinkValue);
			}
			int yellowValue = SharePersistent.getInt(getApplicationContext(), "CH_Y_STAGE");
			if (yellowValue > 0 && yellowValue != 255) {
				this.listViewCrystal3.setCurrentItem(yellowValue);
			}
			
		}
		
		

		this.listViewChannel7.setViewAdapter(wheelAdapterModel);
		this.listViewChannel7.setCurrentItem(0);
		this.listViewChannel7.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
//				ch7Value = newValue;
			}
		});
		this.listViewChannel8.setViewAdapter(wheelAdapterModel);
		this.listViewChannel8.setCurrentItem(0);
		this.listViewChannel8.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				ch8Value = newValue;
			}
		});
		this.listViewChannel9.setViewAdapter(wheelAdapterModel);
		this.listViewChannel9.setCurrentItem(0);
		this.listViewChannel9.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				ch9Value = newValue;
			}
		});
		this.listViewChannel10.setViewAdapter(wheelAdapterModel);
		this.listViewChannel10.setCurrentItem(0);
		this.listViewChannel10.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				ch10Value = newValue;
			}
		});

		View.OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				switch (v.getId()) {
				case R.id.buttonCancell: {
					finish();
				}
					break;
				case R.id.buttonSave: {
					
//					arrayTempList = arrayList;
//					
//					for (int i = 0; i < arrayList.size(); i++) {
//						
//						int value = arrayList.get(i);
//						arrayTempList.remove(i);
//						
//						if (arrayTempList.contains(value)) {
//							Toast.makeText(getApplicationContext(), getString(R.string.Values_can_be_the_same), Toast.LENGTH_SHORT).show();
//						}else {
//							MainActivity_WiFi.getMainActivity().SetCHN(rValue, gValue, bValue, wValue, yValue, pValue);
//							finish();
//						}
//					}
					
					
					
					if (null != MainActivity_STAGE.getMainActivity()) {
						
//						Toast.makeText(getApplicationContext(), "MainActivity_STAGE", Toast.LENGTH_SHORT).show();
						
						MainActivity_STAGE.getMainActivity().SetCHN(rValue, gValue, bValue, wValue, yValue, pValue);
						
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_R_STAGE", rValue);
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_G_STAGE", gValue);
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_B_STAGE", bValue);
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_W_STAGE", wValue);
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_P_STAGE", pValue);
						SharePersistent.savePerference(MainActivity_STAGE.getMainActivity(), "CH_Y_STAGE", yValue);
						
					}

					
//					MainActivity_WiFi.getMainActivity().SetCHN(rValue, gValue, bValue, wValue, yValue, pValue);
					
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_R", rValue);
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_G", gValue);
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_B", bValue);
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_W", wValue);
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_P", pValue);
//					SharePersistent.savePerference(MainActivity_WiFi.getMainActivity(), "CH_Y", yValue);
					
					finish();
					
					// putDataback();
				}
					break;
				}
			}
		};

		findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
		findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
			
//			this.textViewID = (TextView) findViewById(R.id.textViewID);
//			if (null != getIntent()) {
//				tag = getIntent().getIntExtra("tag", -1);
//				this.textViewID.setText("ID:"+String.valueOf(tag));
//				
//				int[] array = getIntent().getIntArrayExtra("data");
//				
//				if (array[0] != 0) {
//					this.listViewH3.setCurrentItem(array[0]);
//				}
//				if (array[1] != 0) {
//					this.listViewM3.setCurrentItem(array[1]);
//				}		
//				
//				this.listViewRed3.setCurrentItem(array[2]);
//				this.listViewGreen3.setCurrentItem(array[3]);
//				this.listViewLightblue3.setCurrentItem(array[4]);
//				this.listViewWhite3.setCurrentItem(array[5]);
//				this.listViewCrystal3.setCurrentItem(array[6]);
//				this.listViewPink3.setCurrentItem(array[7]);
//				
//			} else {
//				finish();
//			}
			
//		}

	}

	private void putDataback() {

		Intent intent = new Intent();
		intent.putExtra("hour", hour);
		intent.putExtra("minite", minite);
		intent.putExtra("model", model);
		intent.putExtra("modelText", modelText);
//		intent.putExtra("redValue", redValue);
//		intent.putExtra("greenValue", greenValue);
//		intent.putExtra("lightblueValue", lightblueValue);
//		intent.putExtra("whiteValue", whiteValue);
//		intent.putExtra("crystalValue", crystalValue);		
//		intent.putExtra("pinkValue", pinkValue);		
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onResume(this);
	}
}
