package com.start;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ledble.activity.ble.MainActivity_BLE;
import com.ledble.activity.home.FragmentTabActivity;
import com.ledble.base.LedBleActivity;
import com.ledble.base.LedBleFragment;
import com.ledble.utils.ManageFragment;
import com.ledble.view.TabButton;
import com.ledble.view.TabLayout;
import com.ledlamp.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;


public class CommentActivity extends LedBleActivity implements View.OnClickListener{

	@Bind(R.id.ivBack)
	ImageView ivBack;
	@Bind(R.id.tvTitle)
	TextView tvTitle;
	TabButton tabHomepage;
    TabButton tabPublish;
    TabButton tabPersonal;

	private TabLayout tabLayout;
	private android.support.v4.app.FragmentManager fragmentManager;
	private List<android.support.v4.app.Fragment> fragmentList = new ArrayList<>();
	private HomePageFragment homeFragment;
	private PublishCommentFragment publishFragment;
    private MyPersonalFragment myPersonalFragment;
	// 选项卡id
	private int[] buttonId = { R.id.tabHomepage, R.id.tabPublish, R.id.tabPersonal };
	private int currentIndex = 0;

	public final static int STORAGE_RUEST_CODE = 1;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_comment);

		tvTitle.setText(getString(R.string.forum));
		ivBack.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
                finish();
			}
		});

        tabHomepage = findViewById(R.id.tabHomepage);
        tabPublish = findViewById(R.id.tabPublish);
        tabPersonal = findViewById(R.id.tabPersonal);

        tabLayout = new TabLayout(R.color.gray, R.color.gray).addBtn(tabHomepage, tabPublish, tabPersonal);
        tabHomepage.init(R.drawable.icon_homepage_default, R.drawable.icon_homepage_select,getString(R.string.home_page), false, this);
        tabPublish.init(R.drawable.icon_message_default, R.drawable.icon_message_select, getString(R.string.publish_comment),true, this);
        tabPersonal.init(R.drawable.icon_my_default, R.drawable.icon_my_select, getString(R.string.my_personal),false, this);
        tabHomepage.getTvTabName().setTextColor(getResources().getColor(R.color.grayDark));
        tabPublish.getTvTabName().setTextColor(getResources().getColor(R.color.grayDark));
        tabPersonal.getTvTabName().setTextColor(getResources().getColor(R.color.grayDark));
        tabLayout.selectBtn(buttonId[currentIndex]);

        tabHomepage.setOnClickListener(this);
        tabPublish.setOnClickListener(this);
        tabPersonal.setOnClickListener(this);

        initFragment();
    }

    /**
     * 初始化fragment
     */
    public void initFragment() {
        homeFragment = new HomePageFragment();
        publishFragment = new PublishCommentFragment();
        myPersonalFragment = new MyPersonalFragment();
        fragmentList.add(homeFragment);
        fragmentList.add(publishFragment);
        fragmentList.add(myPersonalFragment);
        fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (int i = 0; i < fragmentList.size(); i++) {
            transaction.add(R.id.flContent, fragmentList.get(i), fragmentList.get(i).getClass().getSimpleName());
        }
        transaction.commit();
        ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tabHomepage:
                currentIndex = 0;
                break;
            case R.id.tabPublish:
                currentIndex = 1;
                break;
            case R.id.tabPersonal:
                currentIndex = 2;
                break;
        }
        showFragment();
    }

    private void showFragment() {
        ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
        tabLayout.selectBtn(buttonId[currentIndex]);
    }

    public void showHomePageFragment() {
        currentIndex = 0;
        showFragment();
        homeFragment.getCommentList(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case STORAGE_RUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    publishFragment.getPhoto();
                } else {
                    Toast.makeText(CommentActivity.this, "没有权限", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.e("onkeydown", "in");

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            CommentActivity.this.finish();
            return true;
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }
}
