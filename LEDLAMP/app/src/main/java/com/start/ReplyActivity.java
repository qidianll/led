package com.start;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.base.LedBleActivity;
import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledlamp.R;

import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import butterknife.Bind;

public class ReplyActivity extends LedBleActivity implements OnClickListener {
	
	@Bind(R.id.ivBack)
    ImageView ivBack;
	@Bind(R.id.tvTitle)
    TextView tvTitle;
//	@Bind(R.id.tvCommentTitle) TextView tvCommentTitle;
//	@Bind(R.id.tvCount) TextView tvCount;
	@Bind(R.id.lvReply)
ListView lvReply;
	@Bind(R.id.btnReply)
    Button btnReply;
	
	private View headerView;
	private TextView tvCommentTitle;
	private TextView tvCount;
	
	private View footerView;
	private LinearLayout llLoading;
	private TextView tvTips;
	private String replyId;
	private String target;
	
	private ReplyAdapter adapter;
	private List<ReplyBean> list = new ArrayList<ReplyBean>();
	private Map<String, String> params = new HashMap<String, String>();
	private ReplyBean replyBean = new ReplyBean();//帖子
	//private LinearLayoutManager layoutManager;
	private int visibleItem;
	private int pageNum = 1;
	// 是否是最后一页
	private String isLast = "0";
	// 请求是否已响应
	private boolean isRespon = false;
	
	public final static int PUBLISH_REQUEST_CODE = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reply);
		tvTitle.setText(getString(R.string.comment_detail));
		
		ivBack.setOnClickListener(this);
		btnReply.setOnClickListener(this);
		//layoutManager = new LinearLayoutManager(this);
        //layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        //lvReply.setHasFixedSize(true);
        //lvReply.setLayoutManager(layoutManager);
		headerView = getLayoutInflater().inflate(R.layout.layout_reply_header, null, false);
		tvCommentTitle = (TextView) headerView.findViewById(R.id.tvCommentTitle);
		tvCount = (TextView) headerView.findViewById(R.id.tvCount);
		
		footerView = getLayoutInflater().inflate(R.layout.layout_listview_footer, null, false);
		llLoading = (LinearLayout) footerView.findViewById(R.id.llLoading);
		tvTips = (TextView) footerView.findViewById(R.id.tvTips);
		footerView.setVisibility(View.GONE);
		
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			CommentBean commentBean = (CommentBean) bundle.getSerializable("comment");
			if (commentBean != null) {
				tvCommentTitle.setText(commentBean.getTitle());
				replyId = String.valueOf(commentBean.getId());
				target = commentBean.getAuthor();
				
				replyBean.setContent(commentBean.getContent());
				replyBean.setAuthor(commentBean.getAuthor());
				replyBean.setPublishTime(commentBean.getPublishTime());
				replyBean.setImageVisitUrl(commentBean.getImageVisitUrl());;
				list.add(replyBean);
				adapter = new ReplyAdapter(this, list);
				adapter.setFoorHost(commentBean.getAuthor());
				lvReply.addHeaderView(headerView, null, false);
				//lvReply.addFooterView(footerView, null, false);
				lvReply.setAdapter(adapter);
				
				params.put("token", getBaseApp().getUserToken());
				params.put("commentId", replyId);
				params.put("pageNum", String.valueOf(pageNum));
				params.put("pageSize", "10");
				getReplyList(true);
			}
		}
        
		lvReply.setOnScrollListener(new OnScrollListener() {

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				int lastItem = adapter.getCount();
				if (scrollState == 0) {
					// 当前可见的item和每一页的item条数相同时
					if (visibleItem == lastItem) {
						if("0".equals(isLast) && isRespon){
							params.put("pageNum", String.valueOf(++pageNum));
							getReplyList(false);
						}
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
				visibleItem = firstVisibleItem + visibleItemCount - 1;
			}

		});
	}
	
	private void getReplyList(final boolean isFirst){
		HttpUtil.getInstance().getSourceData(isFirst, this, Constant.queryReplyByApp, params, new HttpCallBack() {
			
			@Override
			public void onSuccess(String result) {
				isRespon = true;
				ResponseBean<PageBean<ReplyBean>> responseBean = JSON.parseObject(result,
						new TypeReference<ResponseBean<PageBean<ReplyBean>>>() {});
				if (responseBean != null) {
					if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
						if (isFirst) {
							list.clear();
							list.add(replyBean);
						}
						PageBean<ReplyBean> pageBean = responseBean.getContent();
						if (pageBean != null) {
							isLast = pageBean.getIsLast();
							if(pageBean.getList() != null && pageBean.getList().size() != 0){
								list.addAll(pageBean.getList());
							}
							adapter.notifyDataSetChanged();
							tvCount.setText(pageBean.getCount()+getString(R.string.reply));
						}
						if ("0".equals(isLast)) {
							footerView.setVisibility(View.VISIBLE);
							llLoading.setVisibility(View.VISIBLE);
							tvTips.setVisibility(View.GONE);
						} else {
							footerView.setVisibility(View.GONE);
						}
					} else if (Constant.NODATA_CODE.equals(responseBean.getReturnCode())) {
						tvCount.setText("0"+getString(R.string.reply));
						isLast = "1";
					} else {
						Toast.makeText(ReplyActivity.this, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
					}
				}
			}
			
			@Override
			public void onException(String message) {
				isRespon = true;
				if(pageNum > 0){
					Toast.makeText(ReplyActivity.this, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ivBack:
				finish();
				break;
			case R.id.btnReply:
				Intent intent = new Intent(ReplyActivity.this, ReplyPublishActivity.class);
				Bundle bundle = new Bundle();
				bundle.putString("replyId", replyId);
				bundle.putString("replyType", "comment");
				bundle.putString("target", target);
				intent.putExtras(bundle);
				startActivityForResult(intent, PUBLISH_REQUEST_CODE);
				break;
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && requestCode == PUBLISH_REQUEST_CODE) {
			getReplyList(true);
		}
	}
	
}
