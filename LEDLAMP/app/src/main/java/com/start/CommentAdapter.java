package com.start;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ledble.base.LedBleActivity;

import com.ledlamp.R;

import java.util.ArrayList;
import java.util.List;

public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private static final int TYPE_ITEM = 0;
	private static final int TYPE_FOOTER = 1;
	private LedBleActivity activity;
	private List<CommentBean> list = new ArrayList<CommentBean>();
	private String isLast = "0";

	public CommentAdapter(LedBleActivity activity, List<CommentBean> list) {
		this.activity = activity;
		this.list = list;
	}

	@Override
	public int getItemCount() {
		return list.size() + 1;
	}

	@Override
	public int getItemViewType(int position) {
		if (position + 1 == getItemCount()) {
			return TYPE_FOOTER;
		} else {
			return TYPE_ITEM;
		}
	}

	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
		if (holder instanceof ItemViewHolder) {
			final CommentBean t = list.get(position);
			if(t != null){
				ItemViewHolder item = (ItemViewHolder) holder;
				item.tvTitle.setText(t.getTitle());
				item.tvAuthor.setText(t.getAuthor());
				if (t.getPublishTime().contains(" ") && t.getPublishTime().split(" ").length == 2) {
					item.tvDate.setText(t.getPublishTime().split(" ")[0]);
				} else {
					item.tvDate.setText("");
				}
				
				item.llComment.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						Intent intent = new Intent(activity, ReplyActivity.class);
						Bundle bundle = new Bundle();
						bundle.putSerializable("comment", t);
						intent.putExtras(bundle);
						activity.startActivity(intent);
					}
				});
			}
		} else if(holder instanceof FooterViewHolder) {
			if ("1".equals(isLast)){
				((FooterViewHolder) holder).llLoading.setVisibility(View.GONE);
				((FooterViewHolder) holder).tvTips.setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		if (viewType == TYPE_ITEM) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, null);
			view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			return new ItemViewHolder(view);
		} else if (viewType == TYPE_FOOTER) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listview_footer, null);
			view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			return new FooterViewHolder(view);
		}
		return null;
	}


	class FooterViewHolder extends RecyclerView.ViewHolder {
		RelativeLayout rlFooter;
		LinearLayout llLoading;
		TextView tvTips;
		
		public FooterViewHolder(View view) {
			super(view);
			rlFooter = (RelativeLayout) view.findViewById(R.id.rlFooter);
			llLoading = (LinearLayout) view.findViewById(R.id.llLoading);
			tvTips = (TextView) view.findViewById(R.id.tvTips);
		}
	}

	class ItemViewHolder extends RecyclerView.ViewHolder {
		LinearLayout llComment;
		TextView tvTitle;
		TextView tvAuthor;
		TextView tvDate;

		public ItemViewHolder(View view) {
			super(view);
			llComment = (LinearLayout) view.findViewById(R.id.llComment);
			tvTitle = (TextView) view.findViewById(R.id.tvTitle);
			tvAuthor = (TextView) view.findViewById(R.id.tvAuthor);
			tvDate = (TextView) view.findViewById(R.id.tvDate);
		}
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}
}
