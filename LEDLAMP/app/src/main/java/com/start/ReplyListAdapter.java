package com.start;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ledble.base.LedBleActivity;
import com.ledlamp.R;

import java.util.ArrayList;
import java.util.List;

public class ReplyListAdapter extends BaseAdapter {

	private LedBleActivity activity;
	private String replyId;
	private List<ReplyBean> list = new ArrayList<ReplyBean>();

	public ReplyListAdapter(LedBleActivity activity, String replyId, List<ReplyBean> list) {
		this.activity = activity;
		this.replyId = replyId;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}
	
	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = View.inflate(parent.getContext(), R.layout.item_reply_list, null);
			viewHolder.llReply = (LinearLayout) convertView.findViewById(R.id.llReply);
			viewHolder.tvAuthor = (TextView) convertView.findViewById(R.id.tvAuthor);
			viewHolder.tvTarget = (TextView) convertView.findViewById(R.id.tvTarget);
			viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tvContent);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		final ReplyBean replyBean = list.get(position);
		if (replyBean != null) {
			viewHolder.tvAuthor.setText(replyBean.getAuthor());
			viewHolder.tvTarget.setText(replyBean.getTarget()+":");
			viewHolder.tvContent.setText(replyBean.getContent());
			viewHolder.llReply.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(activity, ReplyPublishActivity.class);
					Bundle bundle = new Bundle();
					bundle.putString("replyId", replyId);
					bundle.putString("replyType", "reply");
					bundle.putString("target", replyBean.getAuthor());
					intent.putExtras(bundle);
					activity.startActivityForResult(intent, ReplyActivity.PUBLISH_REQUEST_CODE);
				}
			});
		}
		return convertView;
	}

	class ViewHolder {
		LinearLayout llReply;
		TextView tvAuthor;
		TextView tvTarget;
		TextView tvContent;
	}

}
