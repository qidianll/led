package com.start;

import java.io.Serializable;

public class CommentBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String title;//评论标题
    private String content;//评论内容
    private String publishTime;//发表时间
    private String author;//作者
    private String imageVisitUrl;//图片访问地址
    
    public CommentBean(){}
    
	public CommentBean(int commentId, String title, String content, String publishTime,
                       String author, String imageVisitUrl) {
		super();
		this.id = commentId;
		this.title = title;
		this.content = content;
		this.publishTime = publishTime;
		this.author = author;
		this.imageVisitUrl = imageVisitUrl;
	}
	public int getId() {
		return id;
	}
	public void setId(int commentId) {
		this.id = commentId;
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPublishTime() {
		return publishTime;
	}
	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getImageVisitUrl() {
		return imageVisitUrl;
	}

	public void setImageVisitUrl(String imageVisitUrl) {
		this.imageVisitUrl = imageVisitUrl;
	}

}
