package com.start;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.ledble.base.LedBleActivity;
import com.ledlamp.R;
import butterknife.Bind;

public class ReplyPublishActivity extends LedBleActivity {
	
	@Bind(R.id.ivBack)
    ImageView ivBack;
	@Bind(R.id.tvTitle)
    TextView tvTitle;
	
	private android.support.v4.app.FragmentManager fragmentManager;
	private PublishCommentFragment fragment;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_reply_publish);
		tvTitle.setText(getString(R.string.reply));
		ivBack.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		fragmentManager = getSupportFragmentManager();
		fragment = new PublishCommentFragment();
		Bundle bundle = getIntent().getExtras();
		System.out.println("bundle:" + bundle);
		fragment.setArguments(bundle);
        
		android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.add(R.id.flContent, fragment, fragment.getClass().getSimpleName());
		transaction.commit();
		android.support.v4.app.FragmentTransaction t = fragmentManager.beginTransaction();
		t.show(fragment);
		t.commit();
	}

}
