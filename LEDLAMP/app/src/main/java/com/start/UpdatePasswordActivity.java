package com.start;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.base.LedBleActivity;
import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledlamp.R;
import com.login.PasswordUtil;
import com.login.UserBean;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;

public class UpdatePasswordActivity extends LedBleActivity implements OnClickListener {

	@Bind(R.id.ivBack)
    ImageView ivBack;
	@Bind(R.id.tvTitle)
    TextView tvTitle;
	@Bind(R.id.clearEtOld)
    EditText clearEtOld;
	@Bind(R.id.clearEtNew)
    EditText clearEtNew;
	@Bind(R.id.btnUpdate)
    Button btnUpdate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_update_password);
		tvTitle.setText(getString(R.string.update_password));

		ivBack.setOnClickListener(this);
		btnUpdate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.ivBack:
				finish();
				break;
			case R.id.btnUpdate:
				String pswdOld = clearEtOld.getText().toString();
				String pswdNew = clearEtNew.getText().toString();
				if (TextUtils.isEmpty(pswdOld) || TextUtils.isEmpty(pswdNew)) {
					Toast.makeText(UpdatePasswordActivity.this, getString(R.string.input_password), Toast.LENGTH_SHORT).show();
					return;
				} else if (pswdOld.length() < 6 || pswdNew.length() < 6) {
					Toast.makeText(UpdatePasswordActivity.this, getString(R.string.input_length_error), Toast.LENGTH_SHORT).show();
					return;
				}
				//密码加密
				pswdOld = new PasswordUtil().encrypt(pswdOld);
				pswdNew = new PasswordUtil().encrypt(pswdNew);
				Map<String, String> params = new HashMap<String, String>();
				params.put("token", getBaseApp().getUserToken());
				params.put("oldPassword", pswdOld);
				params.put("password", pswdNew);

				HttpUtil.getInstance().getSourceData(true, UpdatePasswordActivity.this, Constant.updatePswdByApp, params, new HttpCallBack() {
					
					@Override
					public void onSuccess(String result) {
						ResponseBean<UserBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<UserBean>>() {});
						if (responseBean != null) {
							if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
								Toast.makeText(UpdatePasswordActivity.this, getString(R.string.update_password_success), Toast.LENGTH_SHORT).show();
								finish();
							} else {
								Toast.makeText(UpdatePasswordActivity.this, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
							}
						}
					}
					
					@Override
					public void onException(String message) {
						Toast.makeText(UpdatePasswordActivity.this, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
					}
				});
				break;
			default:
				break;
		}		
	}

}
