package com.start;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.add.DialogUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

import com.ledble.base.LedBleFragment;

import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledble.utils.ImageUtils;
import com.ledble.utils.Utils;
import com.ledlamp.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

@SuppressLint("NewApi")
public class PublishCommentFragment extends LedBleFragment {
	
	@Bind(R.id.etTitle)
    EditText etTitle;
	@Bind(R.id.etContent)
    EditText etContent;
	@Bind(R.id.btnPublish)
    Button btnPublish;
	@Bind(R.id.gvPhoto)
    GridView gvPhoto;;
	
	private String url;
	private String replyId;
	private String replyType;
	private String target;
	private Map<String, String> params;
	private List<ImageInfo> imageList = new ArrayList<ImageInfo>();
	private List<File> fileList = new ArrayList<File>();
	private Map<String, File> files = new HashMap<String, File>();
	private ImageAdapter gvAdapter;
	private Uri fileUri;
	private int index = 0;
	
	private final static String TAG = "PublishCommentFragment";
	private final static int RUEST_CAMERA = 1;
	private final static int RUEST_PHOTO = 2;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_publish_comment, container, false);
	}

	@Override
	public void initData() {
		Bundle bundle = getArguments();
		if (bundle != null) {
			replyId = bundle.getString("replyId");
			replyType = bundle.getString("replyType");
			target = bundle.getString("target");
			Log.i(TAG, "replyId: " + replyId);
			Log.i(TAG, "replyType: " + replyType);
			Log.i(TAG, "target: " + target);
			if (!TextUtils.isEmpty(replyId)) {
				etTitle.setVisibility(View.GONE);
				if (!TextUtils.isEmpty(replyType) && "reply".equals(replyType)) {
					gvPhoto.setVisibility(View.GONE);
				}
			}
		}
		
		params = new HashMap<String, String>();
		params.put("token", activity.getBaseApp().getUserToken());
		ImageInfo imageInfo = new ImageInfo();
		imageList.add(imageInfo);
		gvAdapter = new ImageAdapter(imageList);
		gvPhoto.setAdapter(gvAdapter);
	}

	@Override
	public void initView() {
		gvPhoto.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
				index = position;
				if (Build.VERSION.SDK_INT >= 23) {
					if (activity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
		                    != PackageManager.PERMISSION_GRANTED) {
						 // 申请授权
						activity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, CommentActivity.STORAGE_RUEST_CODE);
					} else {
						getPhoto();
					}
				} else {
					getPhoto();
				}
//				String[] chooserString = { "拍照", "从相册选取" };
//				AlertDialog.Builder builder = new AlertDialog.Builder(activity);
//				builder.setTitle("");
//				builder.setItems(chooserString, new DialogInterface.OnClickListener() {
//
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						if (which == 1) {// 相册
//							getPhoto();
//						} else {// 相机
//							Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//							fileUri = getOutputMediaFileUri();
//							intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
//							startActivityForResult(intent, RUEST_CAMERA);
//						}
//					}
//				});
//				builder.show();
			}
		});
	}
	
	public void getPhoto() {
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, RUEST_PHOTO);
	}

	@Override
	public void initEvent() {
		btnPublish.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (TextUtils.isEmpty(replyId)) {
					url = Constant.publishCommentByApp;
					params.put("title", etTitle.getText().toString());
				} else {
					url = Constant.publishReplyByApp;
					params.put("replyId", replyId);
					params.put("replyType", replyType);
					params.put("target", target);
				}
				params.put("content", etContent.getText().toString());
				files.clear();
				for (int i = 0; i < fileList.size(); i++) {
					File file = fileList.get(i);
					files.put(file.getName(), file);
				}
				HttpUtil.getInstance().uploadFile(true, activity, url, params, files, new HttpCallBack() {
					
					@Override
					public void onSuccess(String result) {
						ResponseBean<String> responseBean = JSON.parseObject(result,
								new TypeReference<ResponseBean<String>>() {});
						if (responseBean != null) {
							if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
								Toast.makeText(activity, activity.getString(R.string.comment_publish_success), Toast.LENGTH_SHORT).show();
							} else {
								Toast.makeText(activity, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
							}
							if (!TextUtils.isEmpty(replyId)) {//回复
								activity.setResult(Activity.RESULT_OK);
								activity.finish();
							} else {//发布
								etTitle.setText("");
								etContent.setText("");
								imageList.clear();
								gvAdapter.notifyDataSetChanged();
								if (activity instanceof CommentActivity) {
									((CommentActivity) activity).showHomePageFragment();
								}
							}
						}
					}
					
					@Override
					public void onException(String message) {
						Toast.makeText(activity, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
					}
				});
			}
		});
	}
	
	@SuppressLint("SimpleDateFormat")
	public Uri getOutputMediaFileUri() {
		File mediaStorageDir = new File(Constant.PHOTO_IMAGE_PATH);
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				return null;
			}
		}
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
		return Uri.fromFile(mediaFile);
	}
	
	/**
	 * 取得返回的图片路径
	 */
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String picturePath = "";
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
			case RUEST_CAMERA:
				getActivity().sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, fileUri));
				picturePath = fileUri.toString().replaceFirst("file:///", "/").trim();
				compressPhoto(picturePath);
				break;
			case RUEST_PHOTO:
				if (data != null) {
					Uri imageUri = data.getData();
					String[] filePathColumn = { MediaStore.Images.Media.DATA };
					Cursor cursor = getActivity().getContentResolver().query(imageUri, filePathColumn, null, null, null);
					cursor.moveToFirst();
					int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
					picturePath = cursor.getString(columnIndex);
					cursor.close();
					compressPhoto(picturePath);
				}
				break;
			}
		}
	}
	
	/**
	 * 压缩图片
	 * 
	 * @param picturePath
	 */
	private void compressPhoto(String picturePath) {
		if (!TextUtils.isEmpty(picturePath)) {
			new ScalPhotoTask(picturePath).execute("");
		}
	}
	
	public class ScalPhotoTask extends AsyncTask<String, Void, File> {

		private String picturePath;

		public ScalPhotoTask(String picturePath) {
			this.picturePath = picturePath;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			DialogUtil.showProgress(activity);
		}

		@Override
		protected File doInBackground(String... params) {
			File oldFile = new File(picturePath);
			String fileName = oldFile.getName();
			Log.i(TAG, "old fileName: " + fileName);
			return ImageUtils.scalFile(oldFile, fileName);
		}

		@Override
		protected void onPostExecute(File newFile) {
			super.onPostExecute(newFile);
			DialogUtil.closeWithProgress();
			if (newFile != null) {
				ImageInfo imageInfo = new ImageInfo();
				String fileName = newFile.getName();
				Log.i(TAG, "new fileName: " + fileName);
				imageInfo.setImage(newFile.getAbsolutePath());
				imageInfo.setImgName(fileName);
				imageList.add(index, imageInfo);
				fileList.add(index, newFile);
				// 图片不小于10张,移除最后一张
				if (imageList.size() == 10) {
					imageList.remove(imageList.size() - 1);
				}
				gvAdapter.notifyDataSetChanged();
			}
		}
	}
	
	private class ImageAdapter extends BaseAdapter {
		
		private List<ImageInfo> list;

		public ImageAdapter(List<ImageInfo> list) {
			this.list = list;
		}

		@Override
		public int getCount() {
			return list.size();
		}

		@Override
		public Object getItem(int position) {
			return list.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder viewHolder = null;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = View.inflate(parent.getContext(), R.layout.item_photo_image, null);
				viewHolder.ivImage = (ImageView) convertView.findViewById(R.id.ivImage);
				viewHolder.tvImageName = (TextView) convertView.findViewById(R.id.tvImageName);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			ImageInfo imageInfo = list.get(position);
			if (position == list.size() - 1 && fileList.size() < 9) {
				viewHolder.ivImage.setImageResource(R.drawable.icon_plus);
				viewHolder.tvImageName.setText(activity.getString(R.string.click_add_image));
			} else {
				if (imageInfo != null) {
					viewHolder.ivImage.setImageBitmap(zoomImage(imageInfo.getImage()));
					viewHolder.tvImageName.setText(imageInfo.getImgName());
				}
			}
			return convertView;
		}
		
		class ViewHolder {
			ImageView ivImage;
			TextView tvImageName;
		}
	}
	
	private Bitmap zoomImage(String picturePath) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(picturePath, options);
		options.inJustDecodeBounds = false;
		int sampleSize = (int) (options.outWidth / 240);
		if (sampleSize < 1)
			sampleSize = 1;
		options.inSampleSize = sampleSize;
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		Bitmap bitmap = BitmapFactory.decodeFile(picturePath, options);
		// 获得图片的宽高
		int width = bitmap.getWidth();
		int height = bitmap.getHeight();
		// 计算缩放比例
		float scaleHeight = ((float) Utils.dip2px(96)) / height;
		float scaleWidth = scaleHeight;
		// 取得想要缩放的matrix参数
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		// 得到新的图片
		return Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
	}

}
