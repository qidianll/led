package com.start;


import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.ledble.base.LedBleFragment;
import com.ledble.constant.Constant;
import com.ledble.http.HttpUtil;
import com.ledble.http.HttpUtil.HttpCallBack;
import com.ledble.http.ResponseBean;
import com.ledlamp.R;
import com.login.UserBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

public class HomePageFragment extends LedBleFragment {

    @Bind(R.id.srlComment)
    SwipeRefreshLayout srlComment;
    @Bind(R.id.lvComment)
    RecyclerView lvComment;
    private static final String TAG = "HomePageFragment";
    private CommentAdapter adapter;
    View mView;
    private List<CommentBean> list = new ArrayList<>();
    public Map<String, String> params;
    private LinearLayoutManager layoutManager;
    private int lastVisibleItem;
    private int pageNum = 1;
    // 是否是最后一页
    private String isLast = "0";
    // 请求是否已响应
    private boolean isRespon = false;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home_page, container, false);
        return mView;
    }

    @Override
    public void initData() {
        layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        lvComment.setHasFixedSize(true);
        lvComment.setLayoutManager(layoutManager);

        adapter = new CommentAdapter(activity, list);
        lvComment.setAdapter(adapter);
        params = new HashMap<>();

        params.put("pageNum", String.valueOf(pageNum));
        params.put("pageSize", "8");
        params.put("token", activity.getBaseApp().getUserToken());
        getCommentList(true);
    }

    @Override
    public void initView() {
        srlComment.setColorSchemeResources(R.color.colorPrimary);
    }

    @Override
    public void initEvent() {
        srlComment.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                getCommentList(true);
            }
        });

        lvComment.setOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView,
                                             int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == RecyclerView.SCROLL_STATE_IDLE
                        && lastVisibleItem + 1 == adapter.getItemCount()) {
                    if ("0".equals(isLast) && isRespon) {
                        params.put("pageNum", String.valueOf(++pageNum));
                        getCommentList(false);
                    }
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                lastVisibleItem = layoutManager.findLastVisibleItemPosition();
            }

        });
    }

    public void getCommentList(final boolean isFirst) {
        HttpUtil.getInstance().getSourceData(isFirst, activity, Constant.queryCommentByApp, params, new HttpCallBack() {
            @Override
            public void onSuccess(String result) {
                Log.e(TAG, "onSuccess: " + result);
                isRespon = true;
                srlComment.setRefreshing(false);
                ResponseBean<PageBean<CommentBean>> responseBean = JSON.parseObject(result,
                        new TypeReference<ResponseBean<PageBean<CommentBean>>>() {
                        });
                if (responseBean != null) {
                    if (Constant.SUCCESS_CODE.equals(responseBean.getReturnCode())) {
                        if (isFirst) {
                            list.clear();
                        }
                        PageBean<CommentBean> pageBean = responseBean.getContent();
                        if (pageBean != null) {
                            isLast = pageBean.getIsLast();
                            if (pageBean.getList() != null && pageBean.getList().size() != 0) {
                                list.addAll(pageBean.getList());
                            }
                            adapter.setIsLast(isLast);
                            adapter.notifyDataSetChanged();
                        }
                    } else if (Constant.NODATA_CODE.equals(responseBean.getReturnCode())) {
                        Toast.makeText(activity, getString(R.string.no_more_data), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(activity, responseBean.getReturnDesc(), Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "onSuccess: " + responseBean.getReturnDesc());
                    }
                }
            }

            @Override
            public void onException(String message) {
                isRespon = true;
                srlComment.setRefreshing(false);
                if (pageNum > 0) {
                    Toast.makeText(activity, getString(R.string.request_failed), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
