package com.start;

/**
 * 图片信息
 * 
 * @author ftl
 * 
 */
public class ImageInfo {

	private String image;// 图片
	private String imgName;

	public String getImgName() {
		return imgName;
	}

	public void setImgName(String imgName) {
		this.imgName = imgName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
