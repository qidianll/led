package com.start;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.ledble.base.LedBleFragment;
import com.ledlamp.R;
import com.login.LoginActivity;

import butterknife.Bind;

public class MyPersonalFragment extends LedBleFragment {
	
	@Bind(R.id.tvUserName)
    TextView tvUserName;
	@Bind(R.id.tvEmail)
    TextView tvEmail;
	@Bind(R.id.btnUpdate)
    Button btnUpdate;
	@Bind(R.id.btnLogout)
    Button btnLogout;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_my_personal, container, false);
	}

	@Override
	public void initData() {
		tvUserName.setText(tvUserName.getText() + activity.getBaseApp().getUserName());
		tvEmail.setText(tvEmail.getText() + activity.getBaseApp().getCheckMode());
	}

	@Override
	public void initView() {
		
	}

	@Override
	public void initEvent() {
		final Intent intent = new Intent();
		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				intent.setClass(activity, UpdatePasswordActivity.class);
				startActivity(intent);
			}
		});
		btnLogout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				activity.getBaseApp().setUserBean(null);
				intent.setClass(activity, LoginActivity.class);
				startActivity(intent);
				activity.finish();
			}
		});
	}

}
