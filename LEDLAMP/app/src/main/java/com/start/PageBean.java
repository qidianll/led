package com.start;

import java.util.List;

public class PageBean<T> {
	
	//表示是否最后一页，0：不是，1：是
	private String isLast;
	//表示总记录数
	private String count;
	//集合存放每页显示的数据
	private List<T> list;
	
	public PageBean(){}
	
	public PageBean(String isLast, List<T> list) {
		super();
		this.isLast = isLast;
		this.list = list;
	}
	public String getIsLast() {
		return isLast;
	}

	public void setIsLast(String isLast) {
		this.isLast = isLast;
	}
	
	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public List<T> getList() {
		return list;
	}
	public void setList(List<T> list) {
		this.list = list;
	}
}
