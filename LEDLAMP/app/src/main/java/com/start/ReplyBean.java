package com.start;

import java.util.List;

public class ReplyBean {

	private int id;//回复id
	private String replyId;//回复评论指向的id
	private String replyType;//回复类型，comment:评论，reply:回复
    private String content;//评论内容
    private String publishTime;//发表时间
    private String author;//回复用户
    private String target;//目标用户
    private String imageVisitUrl;//图片访问地址
    private List<ReplyBean> list;//回复列表
    
    public ReplyBean(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getReplyId() {
		return replyId;
	}

	public void setReplyId(String replyId) {
		this.replyId = replyId;
	}

	public String getReplyType() {
		return replyType;
	}

	public void setReplyType(String replyType) {
		this.replyType = replyType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getPublishTime() {
		return publishTime;
	}

	public void setPublishTime(String publishTime) {
		this.publishTime = publishTime;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public String getImageVisitUrl() {
		return imageVisitUrl;
	}

	public void setImageVisitUrl(String imageVisitUrl) {
		this.imageVisitUrl = imageVisitUrl;
	}

	public List<ReplyBean> getList() {
		return list;
	}

	public void setList(List<ReplyBean> list) {
		this.list = list;
	}
    
}
