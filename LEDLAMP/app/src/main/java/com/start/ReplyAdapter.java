package com.start;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ledble.base.LedBleActivity;
import com.ledble.utils.Utils;
import com.ledlamp.R;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReplyAdapter extends BaseAdapter {

	private LedBleActivity activity;
	private List<ReplyBean> list = new ArrayList<ReplyBean>();
	private Map<String, Boolean> addFlag = new HashMap<String, Boolean>();
	private String foorHost = "";

	public ReplyAdapter(LedBleActivity activity, List<ReplyBean> list) {
		this.activity = activity;
		this.list = list;
		for (int i = 0; i < list.size(); i++) {
			addFlag.put(String.valueOf(i), false);
		}
	}
	
	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder = null;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = View.inflate(parent.getContext(), R.layout.item_reply, null);
			viewHolder.tvAuthor = (TextView) convertView.findViewById(R.id.tvAuthor);
			viewHolder.tvFloorHost = (TextView) convertView.findViewById(R.id.tvFloorHost);
			viewHolder.tvFloorNo = (TextView) convertView.findViewById(R.id.tvFloorNo);
			viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);
			viewHolder.tvContent = (TextView) convertView.findViewById(R.id.tvContent);
//			viewHolder.lvImage = (ListView) convertView.findViewById(R.id.lvImage);
			viewHolder.llImages = (LinearLayout) convertView.findViewById(R.id.llImages);
			viewHolder.ivReply = (ImageView) convertView.findViewById(R.id.ivReply);
			viewHolder.lvReply = (ListView) convertView.findViewById(R.id.lvReply);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		ReplyBean replyBean = list.get(position);
		if (replyBean != null) {
			viewHolder.tvAuthor.setText(replyBean.getAuthor());
			if (isFloorHost(replyBean.getAuthor())) {
				viewHolder.tvFloorHost.setVisibility(View.VISIBLE);
			} else {
				viewHolder.tvFloorHost.setVisibility(View.GONE);
			}
			if (position == 0) {
				viewHolder.tvFloorNo.setVisibility(View.GONE);
			} else {
				viewHolder.tvFloorNo.setVisibility(View.VISIBLE);
				viewHolder.tvFloorNo.setText(position + activity.getString(R.string.floor));
			}
			viewHolder.tvDate.setText(replyBean.getPublishTime());
			viewHolder.tvContent.setText(replyBean.getContent());
			String imageVisitUrl = replyBean.getImageVisitUrl();
			if (!TextUtils.isEmpty(imageVisitUrl)) {
				viewHolder.llImages.setVisibility(View.VISIBLE);
				List<String> list = new ArrayList<String>();
				if (imageVisitUrl.contains(",")) {
					String[] urlArray = imageVisitUrl.split(",");
					for (int i = 0; i < urlArray.length; i++) {
						list.add(urlArray[i]);
					}
				} else {
					list.add(imageVisitUrl);
				}
				boolean flag = addFlag.get(String.valueOf(position)) == null ? false : addFlag.get(String.valueOf(position));
				if(!flag) {
					for (int i = 0; i < list.size(); i++) {
						ImageView imageView = new ImageView(parent.getContext());
			            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
			            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
			            layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
			            layoutParams.topMargin = 10;
			            layoutParams.leftMargin = 25;
			            layoutParams.rightMargin = 25;
			            layoutParams.bottomMargin = 10;
			            imageView.setLayoutParams(layoutParams);
						ImageLoader.getInstance().displayImage(list.get(i), imageView, loadingListener);
						viewHolder.llImages.addView(imageView);
					}
					addFlag.put(String.valueOf(position), true);
				}
//				ImageAdapter adapter = new ImageAdapter(list);
//				viewHolder.lvImage.setAdapter(adapter);
//				adapter.notifyDataSetChanged();
			} else {
				viewHolder.llImages.setVisibility(View.GONE);
			}
			if (replyBean.getList() != null && replyBean.getList().size() != 0) {
				viewHolder.lvReply.setVisibility(View.VISIBLE);
				ReplyListAdapter adapter = new ReplyListAdapter(activity, String.valueOf(replyBean.getId()), replyBean.getList());
				viewHolder.lvReply.setAdapter(adapter);
			} else {
				viewHolder.lvReply.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	class ViewHolder {
		TextView tvAuthor;
		TextView tvFloorHost;
		TextView tvFloorNo;
		TextView tvDate;
		TextView tvContent;
		ListView lvImage;
		LinearLayout llImages;
		ImageView ivReply;
		ListView lvReply;
	}
	
	public void setFoorHost(String foorHost) {
		this.foorHost = foorHost;
	}

	public boolean isFloorHost(String author) {
		if (foorHost.equals(author)) {
			return true;
		} else {
			return false;
		}
	}
	
    private SimpleImageLoadingListener loadingListener = new SimpleImageLoadingListener(){
        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                float oldWidth = loadedImage.getWidth();
                float oldHeight = loadedImage.getHeight();
                float width = Utils.SCREEN_WIDTH;
                float height = width / oldWidth * oldHeight;
                ViewGroup.LayoutParams params = imageView.getLayoutParams();
                params.width = (int) width;
                params.height = (int) height;
                imageView.setLayoutParams(params);
            }
        }
    };
}
