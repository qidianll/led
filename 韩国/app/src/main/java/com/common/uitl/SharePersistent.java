package com.common.uitl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.List;

import com.ledble.bean.TimeRGBW;

import android.R.integer;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 轻量持久化
 * 
 */
public class SharePersistent {

	public static final String PREFS_NAME = Constant.TAG;

	public static void setObjectValue(Context context, String key, Object value) {
		try {
			SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, 0).edit();
			// 将map转换为byte[]
			ByteArrayOutputStream toByte = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(toByte);
			oos.writeObject(value);
			// 对byte[]进行Base64编码
			String payCityMapBase64 = new String(Base64Coder.encode(toByte.toByteArray()));
			// 存储
			editor.putString(key, payCityMapBase64);

			editor.commit();

		} catch (Exception ex) {
			ex.printStackTrace();

		}
	}

	public static synchronized Object getObjectValue(Context context, String key) {
		SharedPreferences pre = context.getSharedPreferences(PREFS_NAME, 0);
		String str = pre.getString(key, null);
		if (str == null)
			return null;
		byte[] base64Bytes = Base64Coder.decode(str);

		ByteArrayInputStream bais = new ByteArrayInputStream(base64Bytes);
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(bais);
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			return ois.readObject();
		} catch (OptionalDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void savePerference(Context context, String key, String value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString(key, value);
		editor.commit();
	}

	public static void savePerference(Context context, String key, int value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	public static void saveBoolean(Context context, String key, boolean value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public static String getPerference(Context context, String key) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString(key, "");
	}

	public static int getInt(Context context, String key) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getInt(key, 0);
	}
	
	public static int getDayInt(Context context, String key) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getInt(key, -1);
	}

	public static boolean getBoolean(Context context, String key) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getBoolean(key, false);
	}

	public static boolean getBoolean(Context context, String key, boolean defaultVal) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getBoolean(key, defaultVal);
	}
	
	public static void savePanoramaMode(Context context, String key, int value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	
	public static int getPanoramaMode(Context context, String key) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getInt(key, 0);
	}

	// 保存 FC亮度
	public static void saveFCBrightData(Context context, String key, int whiteValue, int redValue, int greenValue, int blueValue , int lightModeValue ) {
//		SharedPreferences sp = context.getSharedPreferences(type, Context.MODE_PRIVATE);
		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key + "whiteValue", whiteValue);
		editor.putInt(key + "redValue", redValue);
		editor.putInt(key + "greenValue", greenValue);
		editor.putInt(key + "blueValue", blueValue);
		editor.putInt(key + "lightModeValue", lightModeValue);
		editor.commit();
	}

	// 取出  FC亮度
	public static int[] getFCBrightData(Context context, String key) {
		String keyArray[] = { key + "whiteValue", key + "redValue", key + "greenValue", key + "blueValue" , key + "lightModeValue"};

		int[] FCData = new int[5];
		for (int i = 0; i < FCData.length; i++) {

			SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			FCData[i] = sp.getInt(keyArray[i], 0);
		}
		return FCData;
	}
	
	// 保存 Pama亮度
	public static void savePamaData(Context context, String key, int modeValue, int speedValue, int brightValue , int lightModeValue ) {

		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt(key + "modeValue", modeValue);
		editor.putInt(key + "speedValue", speedValue);
		editor.putInt(key + "brightValue", brightValue);
		editor.putInt(key + "lightModeValue", lightModeValue);
		editor.commit();
	}

	// 取出  Pama亮度
	public static int[] getPamaData(Context context, String key) {
		String keyArray[] = {key + "modeValue", key + "speedValue", key + "brightValue", key + "lightModeValue"};

		int[] PamaData = new int[4];
		for (int i = 0; i < PamaData.length; i++) {

			SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			PamaData[i] = sp.getInt(keyArray[i], 0);
		}
		return PamaData;
	}

	// 保存 单组的数据
	public static void saveSingleGroupData(Context context, String key, String index, String groupName,
			String deviceName, String deviceAddress) {
		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key + "index", index);
		editor.putString(key + "groupName", groupName);
		editor.putString(key + "deviceName", deviceName);
		editor.putString(key + "deviceAddress", deviceAddress);
		editor.commit();

	}

	// 取出 单组的数据
	public static String[] getSingleGroupData(Context context, String key) {

		String keyArray[] = { key + "index", key + "groupName", key + "deviceName", key + "deviceAddress" };

		String[] timeData = new String[4];
		for (int i = 0; i < timeData.length; i++) {

			SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			timeData[i] = sp.getString(keyArray[i], null);
		}
		return timeData;

	}

	// 保存 各组的数据
	public static void saveGroupData(Context context, String key, String index, String lightName, String deviceName, String groupName, 
			String deviceAddress) {
		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key + "index", index);
		editor.putString(key + "lightName", lightName);
		editor.putString(key + "deviceName", deviceName);
		editor.putString(key + "groupName", groupName);
		editor.putString(key + "deviceAddress", deviceAddress);
		editor.commit();

	}

	// 取出 各组的数据
	public static String[] getGroupData(Context context, String key) {

		String keyArray[] = { key + "index", key + "lightName", key + "deviceName", key + "groupName", key + "deviceAddress" };

		String[] timeData = new String[5];
		for (int i = 0; i < timeData.length; i++) {

			SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			timeData[i] = sp.getString(keyArray[i], "null");
		}
		return timeData;

	}

	public static void saveTimerData(Context context, String key, String hour, String minute, String redValue, String greenValue, 
			String blueValue, String whiteValue, String co2Value) {
		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(key + "hour", hour);
		editor.putString(key + "minute", minute);
		editor.putString(key + "redValue", redValue);
		editor.putString(key + "greenValue", greenValue);
		editor.putString(key + "blueValue", blueValue);
		editor.putString(key + "whiteValue", whiteValue);
		editor.putString(key + "co2Value", co2Value);

//		String mode = "";
//		if (openHour > 12) {
//			mode = "PM";
//		} else {
//			mode = "AM";
//		}
//		editor.putString(key + "mode", mode);
		editor.commit();
	}

	public static String[] getTimerData(Context context, String key, int index) {

		String keyArray[] = { key + "hour", key + "minute", key + "redValue", key + "greenValue", key + "blueValue", key + "whiteValue", key + "co2Value"};

		String[] timeData = new String[7];
		for (int i = 0; i < timeData.length; i++) {

			SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
			if (0 == index || 24 == index) {
				timeData[i] = sp.getString(keyArray[i], "0");
			}else {
				timeData[i] = sp.getString(keyArray[i], "-");
			}
			
		}
		return timeData;

	}

	public static String getTimerModeData(Context context, String key) {

		String keyMode = key + "mode";
		SharedPreferences sp = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		return sp.getString(keyMode, "AM");

	}
	
	

}
