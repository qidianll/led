package com.ledble.constant;

public class Constant {

	public static final int MUSIC_STYLE_POP = 0;
	public static final int MUSIC_STYLE_SOFT = 1;
	public static final int MUSIC_STYLE_ROCK = 2;
	
	public static final String MODLE_TYPE = "MODLE_TYPE";
	public static final String MODLE_VALUE = "MODLE_VALUE";
	public static final String SPF_DIY = "DIY";
	public static final String DYNAMIC_DIY = "DYNAMIC_DIY";
	public static final String IMAGE_VALUE = "IMAGE_VALUE";
		
	public static final String BRIGHT_TYPE = "BRIGHT_TYPE";
	public static final String BRIGHT_KEY = "BRIGHT_KEY";
	
	public static final String GROUP_NAME = "groupName";
	public static final String DEVICE_NAME = "deviceName";
	
	public static final String MODE_NAME = "Mode";
}
