package com.ledble.fragment;

import android.os.Bundle;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import butterknife.Bind;

import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.aquariumstudio.R;
import com.ledble.activity.MainActivity;
import com.ledble.base.LedBleFragment;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
/**
 * 定时
 * @author ftl
 *
 */
public class TimerFragment extends LedBleFragment {
	
	@Bind(R.id.linearLayoutTimerOn) LinearLayout linearLayoutTimerOn;
	
	@Bind(R.id.linearLayoutTimerOff) LinearLayout linearLayoutTimerOff;
	@Bind(R.id.textViewHourOnTime) TextView textViewHourOnTime;
	@Bind(R.id.textViewMinuteOnTime) TextView textViewMinuteOnTime;
	
	@Bind(R.id.textViewHourOffTime) TextView textViewHourOffTime;
	@Bind(R.id.textViewMinuteOffTime) TextView textViewMinuteOffTime;
	
	@Bind(R.id.textViewModelText) TextView textViewModelText;
	
	@Bind(R.id.toggleOn) ToggleButton toggleButtonOn;
	@Bind(R.id.toggleOff) ToggleButton toggleButtonOff;
	@Bind(R.id.tvMode) TextView tvMode;
	@Bind(R.id.listViewH) WheelView listViewH;
	@Bind(R.id.listViewM) WheelView listViewM;
//	@Bind(R.id.toggleOn) ToggleButton toggleButtonOn;
//	@Bind(R.id.toggleOff) ToggleButton toggleButtonOff;
	
//	private WheelView listViewH;
//	private WheelView listViewM;
//	private WheelView listViewModel;

	private WheelModelAdapter wheelAdapterH;
	private WheelModelAdapter wheelAdapterM;
	
	private int openHour;
	private int openMinite;
	private int closeHour;
	private int closeMinite;
	private boolean isTimerOpen = true;
	private String startkey = "start";
	private String stopkey = "stop";
	private boolean is = true;
	private final int INT_TIMER_ON = 11;
	private final int INT_TIMER_OFF = 12;
	private MainActivity mActivity;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_timer, container, false);
	}

	@Override
	public void initData() {
		mActivity = (MainActivity) getActivity();
		
//		int startData[] = SharePersistent.getTimerData(mActivity, startkey);
//		textViewHourOnTime.setText(NumberHelper.LeftPad_Tow_Zero(startData[0]));
//		textViewMinuteOnTime.setText(NumberHelper.LeftPad_Tow_Zero(startData[1]));
//		
//		int stopData[] = SharePersistent.getTimerData(mActivity, stopkey);
//		textViewHourOffTime.setText(NumberHelper.LeftPad_Tow_Zero(stopData[0]));
//		textViewMinuteOffTime.setText(NumberHelper.LeftPad_Tow_Zero(stopData[1]));
	}

	@Override
	public void initView() {
		
		Time t=new Time();
		t.setToNow();
		openHour = t.hour; 
		openMinite = t.minute; 
		
		String[] modelH = new String[24];
		for (int i = 0; i < 24; i++) {
			modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterH = new WheelModelAdapter(mActivity, modelH);
		this.listViewH.setViewAdapter(wheelAdapterH);
		this.listViewH.setCurrentItem(openHour);
		this.listViewH.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {	
				
				if (isTimerOpen) {	
					openHour = newValue;
					textViewHourOnTime.setText(NumberHelper.LeftPad_Tow_Zero(openHour));
//					SharePersistent.saveTimerData(mActivity, startkey, openHour, openMinite);
	
				}else {
					closeHour = newValue;
					textViewHourOffTime.setText(NumberHelper.LeftPad_Tow_Zero(closeHour));
//					SharePersistent.saveTimerData(mActivity, stopkey, closeHour, closeMinite);
				}
			}
		});

		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterM = new WheelModelAdapter(mActivity, modelM);
		this.listViewM.setViewAdapter(wheelAdapterM);
		this.listViewM.setCurrentItem(openMinite);
		// this.listViewModel.setCyclic(true);
		this.listViewM.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				
				if (isTimerOpen) {	
					openMinite = newValue;
					textViewMinuteOnTime.setText(NumberHelper.LeftPad_Tow_Zero(openMinite));
//					SharePersistent.saveTimerData(mActivity, startkey, openHour, openMinite);
					
				}else {
					closeMinite = newValue;
					textViewMinuteOffTime.setText(NumberHelper.LeftPad_Tow_Zero(closeMinite));
//					SharePersistent.saveTimerData(mActivity, stopkey, closeHour, closeMinite);
				}
			}
		});
		
		
		
		linearLayoutTimerOn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				gotoTimerSettting(v, INT_TIMER_ON);
			}
		});
		linearLayoutTimerOff.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gotoTimerSettting(v, INT_TIMER_OFF);
			}
		});

		toggleButtonOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if (toggleButtonOn.isChecked()) {
					mActivity.timerOn(openHour, openMinite, 1);
				} else {
					mActivity.turnOnOffTimerOn(0);
				}
			}
		});
		toggleButtonOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				
				if (toggleButtonOff.isChecked()) {
					mActivity.timerOff(closeHour, closeMinite);
				} else {
					mActivity.turnOnOffTimerOff(0);
				}
			}
		});
	}

	@Override
	public void initEvent() {
		
	}
	
	private void gotoTimerSettting(View v, int id) {
		
		if ("off".equalsIgnoreCase((String) v.getTag())) { //定时关
			tvMode.setText(getResources().getString(R.string.timer_off));
			isTimerOpen = false;
			
		}else{ //定时开
			tvMode.setText(getResources().getString(R.string.timer_on));
			isTimerOpen = true;
			
		}
		
//		Intent intent = new Intent(getActivity(), TimerSettingActivity.class);
//		intent.putExtra("tag", (String) v.getTag());
//		getActivity().startActivityForResult(intent, id);
	}

}
