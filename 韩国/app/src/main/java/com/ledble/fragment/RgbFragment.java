package com.ledble.fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.view.SegmentedRadioGroup;
import com.aquariumstudio.R;
import com.ledble.activity.GroupsActivity;
import com.ledble.activity.MainActivity;
import com.ledble.activity.ManualSelectColorActivity;
import com.ledble.activity.TimerSettingActivity;
import com.ledble.base.LedBleApplication;
import com.ledble.base.LedBleFragment;
import com.ledble.net.NetConnectBle;

/**
 * 褰╄壊
 * 
 * @author ftl
 *
 */
public class RgbFragment extends LedBleFragment {

	@Bind(R.id.relativeTab1) View relativeTab1;// 鑹茬幆
	@Bind(R.id.relativeTab2) View relativeTab2;// 妯″紡
	@Bind(R.id.relativeTab3) View relativeTab3;// 鑷畾涔�
	@Bind(R.id.rlGroupID) View groupView;// 鍒嗙粍
	@Bind(R.id.linearLayoutPanoramaMode) LinearLayout linearLayoutPanoramaMode;// 鍒嗙粍
	
	@Bind(R.id.rlOne) View rlOne;// 鍒嗙粍


	@Bind(R.id.textViewGroupName) TextView textViewGroupName;
	@Bind(R.id.textViewMode) TextView textViewMode;
	@Bind(R.id.textViewWRGB) TextView textViewWRGB;
	@Bind(R.id.segmentRgb) SegmentedRadioGroup segmentRgb;
	@Bind(R.id.seekBarWhite) SeekBar seekBarWhite;
	@Bind(R.id.seekBarRed) SeekBar seekBarRed;
	@Bind(R.id.seekBarGreen) SeekBar seekBarGreen;
	@Bind(R.id.seekBarBlue) SeekBar seekBarBlue;
	@Bind(R.id.imageViewOnOff) ImageView imageViewOnOff;

	@Bind(R.id.textViewColorMode) TextView textViewColorMode;
	@Bind(R.id.textViewPanoramaMode) TextView textViewPanoramaMode;

	@Bind(R.id.panoramaModeA) TextView textViewPanoramaModeA;
	@Bind(R.id.panoramaModeB) TextView textViewPanoramaModeB;
	@Bind(R.id.panoramaModeC) TextView textViewPanoramaModeC;
	@Bind(R.id.llPanoramaModeA) LinearLayout llPanoramaModeA;
	@Bind(R.id.llPanoramaModeC) LinearLayout llPanoramaModeC;
	
	@Bind(R.id.seekBarPanoramaSpeed) SeekBar seekBarPanoramaSpeed;
	@Bind(R.id.seekBarPanoramaLuman) SeekBar seekBarPanoramaLuman;
	
	@Bind(R.id.timerID) RelativeLayout timerView;
	@Bind(R.id.tvTimer) TextView tvTimer;
	@Bind(R.id.imageViewTimer) ImageView imageViewTimer;
	
	private boolean isTimerOpen = false;
	private final int INT_GROUP_BACK = 100;
	private final int INT_TIMER_BACK = 200;
	
	private int pamaData[];
	private int FCData[];
	private int whiteValue = 0, redValue = 0, greenValue = 0, blueValue = 0;
	private int modeValue = 0, speedValue = 0, lumanValue = 0;
	private int onOffStatus = 0;
	private int lightModeStatus = 0;
	private String panoramaModeName = "";
	private String startMode = "";
	private	String stopMode = "";
	private View mContentView;
	private MainActivity mActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_rgb, container, false);
		mActivity = (MainActivity) getActivity();

		return mContentView;
	}
	
	public TextView getTextViewGroupName() {
		return textViewGroupName;
	}

	@Override
	public void initData() {
		
	
		timerView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				
				if (isTimerOpen) {
					startActivityForResult(new Intent(getActivity(), TimerSettingActivity.class), INT_TIMER_BACK);
				}	
			}
		});
		
		imageViewTimer.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (isTimerOpen) { //关
					imageViewTimer.setBackgroundResource(R.drawable.tab_timer);
					NetConnectBle.getInstance().turnOnOffTimerOn(0);
					try {
						Thread.sleep(50);
						Toast.makeText(mActivity, "isTimerOpen = "+isTimerOpen, Toast.LENGTH_SHORT).show();
						NetConnectBle.getInstance().turnOnOrOffTimerOff(0);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}			
					
					isTimerOpen = false;
				}else {  //开
					imageViewTimer.setBackgroundResource(R.drawable.tab_timer_check);
					{ //定时相关
						
						String keystr = SharePersistent.getPerference(mActivity, "deviceName");
						
//						if (keystr.length() > 0) {
//							int timeData[] = SharePersistent.getTimerData(mActivity, keystr);
//							if (timeData != null) {
//								
//								if (timeData[0] > 12) {
//									startMode = "PM";
//								}else {
//									startMode = "AM";
//								}
//								
//								if (timeData[2] > 12) {
//									stopMode = "PM";
//								}else {
//									stopMode = "AM";
//								}
//								
////								Toast.makeText(mActivity, "start[0]= "+timeData[0]+" start[1]= "+timeData[1]+", stop[0] = "+timeData[2]+" stop[1] = "+timeData[3], Toast.LENGTH_SHORT).show();
//								Toast.makeText(mActivity, "isTimerOpen = "+isTimerOpen, Toast.LENGTH_SHORT).show();
//								NetConnectBle.getInstance().timerOn(timeData[0], timeData[1], 1);
//								try {
//									Thread.sleep(50);
//									
//									NetConnectBle.getInstance().timerOff(timeData[2], timeData[3]);
//								} catch (InterruptedException e) {
//									// TODO Auto-generated catch block
//									e.printStackTrace();
//								}
//							}//
//						}
						
					}
					
					isTimerOpen = true;
				}
			}
		});

		
		groupView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				startActivityForResult(new Intent(getActivity(), GroupsActivity.class), INT_GROUP_BACK);
			}
		});
		
		rlOne.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
//				startActivityForResult(new Intent(getActivity(), GroupsActivity.class), INT_GROUP_BACK);
				startActivityForResult(new Intent(getActivity(), ManualSelectColorActivity.class), INT_GROUP_BACK);
			}
		});

		textViewColorMode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				lightModeStatus = 1;
				relativeTab1.setVisibility(View.VISIBLE);
				relativeTab2.setVisibility(View.GONE);
				relativeTab3.setVisibility(View.GONE);
				textViewMode.setText(mActivity.getResources().getString(R.string.mode_color));
				textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
						greenValue, blueValue));
				textViewColorMode.setBackgroundResource(R.drawable.bg_common_left);
				textViewPanoramaMode.setBackgroundResource(R.drawable.bg_shake_white);
				
				FCData = SharePersistent.getFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"));
				
				if (FCData != null) {
					whiteValue = FCData[0];
					if (whiteValue > 0) {
						seekBarWhite.setProgress(whiteValue);						
					}
					
					redValue = FCData[1];
					if (redValue > 0) {
						seekBarRed.setProgress(redValue);
					}
					
					greenValue = FCData[2];
					if (greenValue > 0) {
						seekBarGreen.setProgress(greenValue);
					}
					
					blueValue = FCData[3];
					if (blueValue > 0) {
						seekBarBlue.setProgress(blueValue);
					}
				}
				
				SharePersistent.saveFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), whiteValue, redValue, greenValue, blueValue, lightModeStatus);
				SharePersistent.savePerference(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), lightModeStatus);
			}
		});

		textViewPanoramaMode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				lightModeStatus = 2;
				relativeTab3.setVisibility(View.VISIBLE);
				relativeTab1.setVisibility(View.GONE);
				relativeTab2.setVisibility(View.GONE);
				textViewMode.setText(mActivity.getResources().getString(R.string.mode_panorama));
				textViewWRGB.setText(
						panoramaModeName + mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
				textViewColorMode.setBackgroundResource(R.drawable.bg_shake_white);
				textViewPanoramaMode.setBackgroundResource(R.drawable.bg_common_right);
				
				pamaData = SharePersistent.getPamaData(mActivity,  SharePersistent.getPerference(mActivity, "deviceName"));
				if (pamaData != null) {
					speedValue = pamaData[1];
					lumanValue = pamaData[2];
					if (speedValue > 0) {
						seekBarPanoramaSpeed.setProgress(speedValue);
//						Toast.makeText(mActivity, "PanoramaSpeed", Toast.LENGTH_SHORT).show();

					}
					if (lumanValue > 0) {
						seekBarPanoramaLuman.setProgress(lumanValue);
//						Toast.makeText(mActivity, "PanoramaLuman", Toast.LENGTH_SHORT).show();
					}
					modeValue = pamaData[3];
					if (1 == pamaData[0]) {
						textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
						llPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
						textViewPanoramaModeB.setBackgroundResource(R.color.white);
						textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
					} else if (2 == pamaData[0]) {
						textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
						textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
					} else if (3 == pamaData[0]) {
						textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						textViewPanoramaModeB.setBackgroundResource(R.color.white);
						textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
						llPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
					}else {
						textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
						textViewPanoramaModeB.setBackgroundResource(R.color.white);
						textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
						llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
					} 
					SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);
					SharePersistent.savePerference(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), lightModeStatus);
				}
				
			}
		});

		segmentRgb.check(R.id.rbRgbOne);
		segmentRgb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbRgbOne == checkedId) {
					relativeTab1.setVisibility(View.VISIBLE);
					relativeTab2.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
					textViewMode.setText(mActivity.getResources().getString(R.string.mode_color));
					textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
							greenValue, blueValue));
				} else if ((R.id.rbRgbTwo == checkedId)) {
					relativeTab2.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
				} else if ((R.id.rbRgbThree == checkedId)) {
					relativeTab3.setVisibility(View.VISIBLE);
					
					relativeTab1.setVisibility(View.GONE);
					relativeTab2.setVisibility(View.GONE);
					textViewMode.setText(mActivity.getResources().getString(R.string.mode_panorama));
					textViewWRGB.setText(panoramaModeName
							+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
				}
			}
		});

		textViewWRGB.setText(
				mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue, greenValue, blueValue));
		this.seekBarWhite.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				whiteValue = progress;
				textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
						greenValue, blueValue));
				imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
				onOffStatus = 1;
				
				if (fromUser) {
					try {
						Thread.sleep(50);
						mActivity.setMetalLightBrightNess(progress, 4);
						SharePersistent.saveFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), whiteValue, redValue, greenValue, blueValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		this.seekBarRed.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				redValue = progress;
				textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
						greenValue, blueValue));
				imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
				onOffStatus = 1;
				
				if (fromUser) {
					try {
						Thread.sleep(50);
						mActivity.setMetalLightBrightNess(progress, 1);
						SharePersistent.saveFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), whiteValue, redValue, greenValue, blueValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		this.seekBarGreen.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				greenValue = progress;
				textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
						greenValue, blueValue));
				imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
				onOffStatus = 1;
				
				if (fromUser) {
					try {
						Thread.sleep(50);
						mActivity.setMetalLightBrightNess(progress, 2);
						SharePersistent.saveFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), whiteValue, redValue, greenValue, blueValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		this.seekBarBlue.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				blueValue = progress;
				textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
						greenValue, blueValue));
				imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
				onOffStatus = 1;
				
				if (fromUser) {
					try {
						Thread.sleep(50);
						mActivity.setMetalLightBrightNess(progress, 3);
						SharePersistent.saveFCBrightData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), whiteValue, redValue, greenValue, blueValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});


		textViewPanoramaModeA.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
				llPanoramaModeA.setBackgroundResource(R.color.grayLight);
				textViewPanoramaModeB.setBackgroundResource(R.color.white);
				llPanoramaModeC.setBackgroundResource(R.color.white);
				textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
				
				modeValue = 1;
				panoramaModeName = "A "+mActivity.getResources().getString(R.string.mode)+"   ";
				textViewWRGB.setText(panoramaModeName
						+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
				
				mActivity.setRegMode(1);
				
				SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);	
			}
		});
		textViewPanoramaModeB.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				llPanoramaModeA.setBackgroundResource(R.color.white);
				textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
				textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
				llPanoramaModeC.setBackgroundResource(R.color.white);
				textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
				
				modeValue = 2;
				panoramaModeName = "B "+mActivity.getResources().getString(R.string.mode)+"   ";
				textViewWRGB.setText(panoramaModeName
						+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));	
				mActivity.setRegMode(2);				
				SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);	
			}
		});
		textViewPanoramaModeC.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				llPanoramaModeA.setBackgroundResource(R.color.white);
				textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
				textViewPanoramaModeB.setBackgroundResource(R.color.white);
				textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
				llPanoramaModeC.setBackgroundResource(R.color.grayLight);
				
				modeValue = 3;
				panoramaModeName = "C "+mActivity.getResources().getString(R.string.mode)+"   ";
				textViewWRGB.setText(panoramaModeName
						+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
				mActivity.setRegMode(3);
				SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);	
			}
		});
		

		this.seekBarPanoramaSpeed.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				speedValue = progress;
				textViewWRGB.setText(
						panoramaModeName + mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
				imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
				onOffStatus = 1;
				
				if (fromUser) {
					
					try {
						Thread.sleep(50);
						mActivity.setSpeed(progress);
						SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		this.seekBarPanoramaLuman.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				lumanValue = progress;
				textViewWRGB.setText(
						panoramaModeName + mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
//				if (1 == onOffStatus) {
					imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
					onOffStatus = 1;
//				}
				
				if (fromUser) {					
					try {
						Thread.sleep(50);
						mActivity.setModeBrightNess(progress);
						SharePersistent.savePamaData(mActivity, SharePersistent.getPerference(mActivity, "deviceName"), modeValue, speedValue, lumanValue, lightModeStatus);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});

		this.imageViewOnOff.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅

			@Override
			public void onClick(View v) {
				switch (onOffStatus) {
				case 0:
					imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_on);
					mActivity.open();

					onOffStatus = 1;
					break;
				case 1:
					imageViewOnOff.setImageResource(R.drawable.bg_bottom_togle_off);
					mActivity.close();
					onOffStatus = 0;
					break;

				default:
					break;
				}
			}
		});
	}
	
	private void scanLeDevice() {	
		
		final Handler handler = new Handler(); // 开启 定时搜索 定时器
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
//				handler.postDelayed(this, 1000);// 1秒 是延时时长
				initViews();
			}
		};
		handler.postDelayed(runnable, 1000);// 打开定时器，执行操作
	}
	
	private void initViews() {
		
		String keystr = SharePersistent.getPerference(mActivity, "deviceName");
		String groupName = SharePersistent.getPerference(mActivity, "groupName");
		
//		if (keystr.length() > 0) {
//			int timeData[] = SharePersistent.getTimerData(mActivity, keystr);
//			if (timeData != null) {
//				
//				if (timeData[0] > 12) {
//					startMode = "PM";
//				}else {
//					startMode = "AM";
//				}
//				
//				if (timeData[2] > 12) {
//					stopMode = "PM";
//				}else {
//					stopMode = "AM";
//				}
//				
//
//				tvTimer.setText(startMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[0])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[1])
//					+" ~ "+stopMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[2])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[3]));
//
//				
//			}
//		}
			
		
		ArrayList<BluetoothDevice> bleDevices = LedBleApplication.getApp().getBleDevices();
		if (0 == bleDevices.size()) {
			return;
		}
		
		
		for (BluetoothDevice device : bleDevices) {

			if (/* device.getAddress().equalsIgnoreCase(groupData[3]) && */ device.getName().equalsIgnoreCase(keystr)) {

//				textViewGroupName.setText(groupName);

				lightModeStatus = SharePersistent.getInt(mActivity, keystr);
				FCData = SharePersistent.getFCBrightData(mActivity, keystr);

				if (lightModeStatus > 0 && FCData != null) {
					
					if (lightModeStatus == 1) {
						relativeTab1.setVisibility(View.VISIBLE);
						relativeTab2.setVisibility(View.GONE);
						relativeTab3.setVisibility(View.GONE);
						textViewMode.setText(mActivity.getResources().getString(R.string.mode_color));
						textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
								greenValue, blueValue));
						textViewColorMode.setBackgroundResource(R.drawable.bg_common_left);
						textViewPanoramaMode.setBackgroundResource(R.drawable.bg_shake_white);

						whiteValue = FCData[0];
						this.seekBarWhite.setProgress(whiteValue);
						redValue = FCData[1];
						this.seekBarRed.setProgress(redValue);
						greenValue = FCData[2];
						this.seekBarGreen.setProgress(greenValue);
						blueValue = FCData[3];
						this.seekBarBlue.setProgress(blueValue);
					}

				}

				pamaData = SharePersistent.getPamaData(mActivity, keystr);
				if (lightModeStatus > 0 && pamaData != null) {

					if (lightModeStatus == 2) {
						relativeTab3.setVisibility(View.VISIBLE);
						relativeTab1.setVisibility(View.GONE);
						relativeTab2.setVisibility(View.GONE);
						textViewMode.setText(mActivity.getResources().getString(R.string.mode_panorama));
						textViewWRGB.setText(panoramaModeName
								+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
						textViewColorMode.setBackgroundResource(R.drawable.bg_shake_white);
						textViewPanoramaMode.setBackgroundResource(R.drawable.bg_common_right);

						speedValue = pamaData[1];
						lumanValue = pamaData[2];

						seekBarPanoramaSpeed.setProgress(speedValue);
						seekBarPanoramaLuman.setProgress(lumanValue);

						if (1 == pamaData[0]) {
							textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
							llPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
						} else if (2 == pamaData[0]) {
							textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
						} else if (3 == pamaData[0]) {
							textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
							llPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
						} 

					}

				}

				return;
			}
		}
		
		
		
		for (BluetoothDevice device : bleDevices) {
			
			final String groupNewData[] = SharePersistent.getGroupData(mActivity, device.getAddress());
			
			SharePersistent.savePerference(mActivity, "groupName", groupNewData[3]);
			SharePersistent.savePerference(mActivity, "deviceName", groupNewData[2]);
			
			lightModeStatus = SharePersistent.getInt(mActivity, keystr);
			FCData = SharePersistent.getFCBrightData(mActivity, device.getName());
			if (lightModeStatus > 0 && FCData != null) {
				
				if (lightModeStatus == 1) {
					relativeTab1.setVisibility(View.VISIBLE);
					relativeTab2.setVisibility(View.GONE);
					relativeTab3.setVisibility(View.GONE);
					textViewMode.setText(mActivity.getResources().getString(R.string.mode_color));
					textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue, redValue,
							greenValue, blueValue));
					textViewColorMode.setBackgroundResource(R.drawable.bg_common_left);
					textViewPanoramaMode.setBackgroundResource(R.drawable.bg_shake_white);
					
					whiteValue = FCData[0];
					this.seekBarWhite.setProgress(whiteValue);
					redValue = FCData[1];
					this.seekBarRed.setProgress(redValue);
					greenValue = FCData[2];
					this.seekBarGreen.setProgress(greenValue);
					blueValue = FCData[3];
					this.seekBarBlue.setProgress(blueValue);
				}
			}
				
			pamaData = SharePersistent.getPamaData(mActivity,  device.getName());
			if (lightModeStatus > 0 && pamaData != null) {

				if (lightModeStatus == 2) {
					relativeTab3.setVisibility(View.VISIBLE);
					relativeTab1.setVisibility(View.GONE);
					relativeTab2.setVisibility(View.GONE);
					textViewMode.setText(mActivity.getResources().getString(R.string.mode_panorama));
					textViewWRGB.setText(
							panoramaModeName + mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
					textViewColorMode.setBackgroundResource(R.drawable.bg_shake_white);
					textViewPanoramaMode.setBackgroundResource(R.drawable.bg_common_right);
					
					
					speedValue = pamaData[1];
					this.seekBarPanoramaSpeed.setProgress(speedValue);

					lumanValue = pamaData[2];
					this.seekBarPanoramaLuman.setProgress(lumanValue);
							
					if (1 == pamaData[0]) {
						textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
						llPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
					}else if (2 == pamaData[0]) {
						textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
					}else if (3 == pamaData[0]) {
						textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
						llPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
					}
				}
				
			}
			
		}
	}
	
	private static int computeTime(int hour, int minute) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
		Calendar ca = Calendar.getInstance();
		Date nowDate = ca.getTime();
		String ymd = sdf2.format(nowDate);

		String then = ymd + " " + NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute);
		Date thenDate = sdf.parse(then);
		int second = (int) ((thenDate.getTime() - nowDate.getTime()) / 1000);
		if (second < 0) {
			second = second + 24 * 60 * 60;
		}
		return second;
	}

	@Override
	public void initView() {
		scanLeDevice(); //每隔1s执行
	}

	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
	}

	@Override
	public void initEvent() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if (requestCode == INT_GROUP_BACK && resultCode == Activity.RESULT_OK) { //分组相关

			String groupName = data.getStringExtra("groupName");	
			
			String keystr = SharePersistent.getPerference(mActivity, "deviceName");	
			
			String groupData[] = SharePersistent.getSingleGroupData(mActivity, "device");
			ArrayList<BluetoothDevice> bleDevices = LedBleApplication.getApp().getBleDevices();
			if (0 == bleDevices.size() || bleDevices == null) {
				return;
			}
			
			for (BluetoothDevice device : bleDevices) {

				if (device.getAddress().equalsIgnoreCase(groupData[3])) {

					if (keystr.length() > 0) {

						lightModeStatus = SharePersistent.getInt(mActivity, keystr);
						FCData = SharePersistent.getFCBrightData(mActivity, keystr);
						
						if (lightModeStatus >= 0 && FCData != null) {
							if (lightModeStatus == 0) {

								whiteValue = FCData[0];
								this.seekBarWhite.setProgress(whiteValue);
								redValue = FCData[1];
								this.seekBarRed.setProgress(redValue);
								greenValue = FCData[2];
								this.seekBarGreen.setProgress(greenValue);
								blueValue = FCData[3];
								this.seekBarBlue.setProgress(blueValue);
							}

							if (lightModeStatus == 1) {
								relativeTab1.setVisibility(View.VISIBLE);
								relativeTab2.setVisibility(View.GONE);
								relativeTab3.setVisibility(View.GONE);
								textViewMode.setText(mActivity.getResources().getString(R.string.mode_color));
								textViewWRGB.setText(mActivity.getResources().getString(R.string.w_r_g_b, whiteValue,
										redValue, greenValue, blueValue));
								textViewColorMode.setBackgroundResource(R.drawable.bg_common_left);
								textViewPanoramaMode.setBackgroundResource(R.drawable.bg_shake_white);

								whiteValue = FCData[0];
								this.seekBarWhite.setProgress(whiteValue);
								redValue = FCData[1];
								this.seekBarRed.setProgress(redValue);
								greenValue = FCData[2];
								this.seekBarGreen.setProgress(greenValue);
								blueValue = FCData[3];
								this.seekBarBlue.setProgress(blueValue);
							}
						}

						pamaData = SharePersistent.getPamaData(mActivity, keystr);
						if (lightModeStatus >= 0 && pamaData != null) {
							
							if (lightModeStatus == 0) {

								speedValue = pamaData[1];
								this.seekBarPanoramaSpeed.setProgress(speedValue);

								lumanValue = pamaData[2];
								this.seekBarPanoramaLuman.setProgress(lumanValue);

								modeValue = pamaData[0];
								if (1 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} else if (2 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} else if (3 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
								}else {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} 
							}

							if (lightModeStatus == 2) {
								relativeTab3.setVisibility(View.VISIBLE);
								relativeTab1.setVisibility(View.GONE);
								relativeTab2.setVisibility(View.GONE);
								textViewMode.setText(mActivity.getResources().getString(R.string.mode_panorama));
								textViewWRGB.setText(panoramaModeName
										+ mActivity.getResources().getString(R.string.s_l, speedValue, lumanValue));
								textViewColorMode.setBackgroundResource(R.drawable.bg_shake_white);
								textViewPanoramaMode.setBackgroundResource(R.drawable.bg_common_right);

								speedValue = pamaData[1];
								this.seekBarPanoramaSpeed.setProgress(speedValue);

								lumanValue = pamaData[2];
								this.seekBarPanoramaLuman.setProgress(lumanValue);
								
								modeValue = pamaData[0];
								if (1 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_lightgray);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} else if (2 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.grayLight);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} else if (3 == pamaData[0]) {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_lightgray);
								}else {
									textViewPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeA.setBackgroundResource(R.drawable.corner_white);
									textViewPanoramaModeB.setBackgroundResource(R.color.white);
									textViewPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
									llPanoramaModeC.setBackgroundResource(R.drawable.corner_white);
								} 
							}
						}
					}
				}
			}
			
			
			if (keystr.length() > 0) {
//				int timeData[] = SharePersistent.getTimerData(mActivity, keystr);
//				if (timeData != null) {
//					
//					if (timeData[0] > 12) {
//						startMode = "PM";
//					}else {
//						startMode = "AM";
//					}
//					
//					if (timeData[2] > 12) {
//						stopMode = "PM";
//					}else {
//						stopMode = "AM";
//					}
//					
//
//					tvTimer.setText(startMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[0])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[1])
//						+" ~ "+stopMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[2])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[3]));
//
//					
//				}
			}
			
			ArrayList<BluetoothDevice> bleDev = LedBleApplication.getApp().getConnectedBleDevices();
			if (bleDev.size() > 0) {
				textViewGroupName.setText(groupName);
			}else {
				textViewGroupName.setText(R.string.no_signal);
			}
			
			
			
		}else if ( requestCode == INT_TIMER_BACK && resultCode == Activity.RESULT_OK) { //定时相关
			
			
			String keystr = SharePersistent.getPerference(mActivity, "deviceName");
//			if (keystr.length() > 0) {
//				int timeData[] = SharePersistent.getTimerData(mActivity, keystr);
//				if (timeData != null) {
//					
//					if (timeData[0] > 12) {
//						startMode = "PM";
//					}else {
//						startMode = "AM";
//					}
//					
//					if (timeData[2] > 12) {
//						stopMode = "PM";
//					}else {
//						stopMode = "AM";
//					}
//					
//
//					tvTimer.setText(startMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[0])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[1])
//						+" ~ "+stopMode+"  "+NumberHelper.LeftPad_Tow_Zero(timeData[2])+":"+NumberHelper.LeftPad_Tow_Zero(timeData[3]));
//
//					
//				}
//			}
		}
		
	}


}
