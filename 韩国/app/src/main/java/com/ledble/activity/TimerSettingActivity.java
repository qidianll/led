package com.ledble.activity;

import android.R.string;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.aquariumstudio.R;
import com.ledble.net.NetConnectBle;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;

public class TimerSettingActivity extends BaseActivity {

	private LinearLayout linearLayoutTimerOn;
	private LinearLayout linearLayoutTimerOff;
	private LinearLayout timerStart;
	private LinearLayout timerStop;
	private TextView textViewStart;
	private TextView textViewStop;
	private TextView textViewHourOnTime;
	private TextView textViewMinuteOnTime;
	private TextView textViewHourOffTime;
	private TextView textViewMinuteOffTime;
	
	private TextView textViewOnTimeMode;
	private TextView textViewOffTimeMode;

	private ToggleButton toggleButtonOn;
	private ToggleButton toggleButtonOff;
	private TextView tvMode;

	private WheelView listViewStartH;
	private WheelView listViewStartM;
	
	private TextView textViewMoonLightModeOn;
	private TextView textViewMoonLightModeOff;
	private TextView textViewMoonLightLeft9;
	private TextView textViewMoonLightLeft6;
	private TextView textViewMoonLightLeft3;
	private TextView textViewMoonLightRight3;
	private TextView textViewMoonLightRight6;
	private TextView textViewMoonLightRight9;

	private WheelModelAdapter wheelAdapterH;
	private WheelModelAdapter wheelAdapterM;
	
	private TextView textViewOKButton;
	private TextView textViewBackButton;
	private Time time;
	
	private int openHour = 0;
	private int openMinite = 0;
	private int closeHour = 0;
	private int closeMinite = 0;
	private boolean isTimerStart = true;
	private boolean canSendData = false;
	private String startkey = "start";
	private String stopkey = "stop";
	private int onOffMode = 0;
	private int leftHourMode = 0;
	private int rightHourMode = 0;
	private final int INT_TIMER_ON = 11;
	private final int INT_TIMER_OFF = 12;

	private boolean isSelectMoonLightLeft9 = false;
	private boolean isSelectMoonLightLeft6 = false;
	private boolean isSelectMoonLightLeft3 = false;
	private boolean isSelectMoonLightRight9 = false;
	private boolean isSelectMoonLightRight6 = false;
	private boolean isSelectMoonLightRight3 = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		time = new Time();
		time.setToNow();
//		openHour = t.hour;
//		openMinite = t.minute;
		
		
		initView();
		
		
//		int timeData[] = SharePersistent.getTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "deviceName"));
		
//		openHour = timeData[0];
//		openMinite = timeData[1];
//		closeHour = timeData[2];
//		closeMinite = timeData[3];
//		if (timeData[0] > 0) {
//			textViewHourOnTime.setText(NumberHelper.LeftPad_Tow_Zero(timeData[0]));
//			if (timeData[0] > 12) {
//				textViewOnTimeMode.setText("PM");
//			}else {
//				textViewOnTimeMode.setText("AM");
//			}
//		}
//		if (timeData[1] > 0) {
//			textViewMinuteOnTime.setText(NumberHelper.LeftPad_Tow_Zero(timeData[1]));
//		}
//		if (timeData[2] > 0) {
//			textViewHourOffTime.setText(NumberHelper.LeftPad_Tow_Zero(timeData[2]));
//			if (timeData[2] > 12) {
//				textViewOffTimeMode.setText("PM");
//			}else {
//				textViewOffTimeMode.setText("AM");
//			}
//		}
//		if (timeData[3] > 0) {
//			textViewMinuteOffTime.setText(NumberHelper.LeftPad_Tow_Zero(timeData[3]));
//		}
//		nOffMode = timeData[4];
//		if
//		o (timeData[4] == 1) {
//			canSendData = false;
//			textViewMoonLightModeOn.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightModeOff.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
//			return;
//		}else if (timeData[4] == 2) {
//			canSendData = true;
//			textViewMoonLightModeOn.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightModeOff.setTextColor(getResources().getColor(R.color.black));
//		}
//		leftHourMode = timeData[5];
//		if (timeData[5] == 9) {
//			isSelectMoonLightLeft9 = true;
//			textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
//		}else if (timeData[5] == 6) {
//			isSelectMoonLightLeft6 = true;
//			textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
//		}else if (timeData[5] == 3) {
//			isSelectMoonLightLeft3 = true;
//			textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.blue));
//		}else if (timeData[5] == 0) {
//			textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
//		}
//		rightHourMode = timeData[6];
//		if (timeData[6] == 9) {
//			isSelectMoonLightRight9 = true;
//			textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
//		}else if (timeData[6] == 6) {
//			isSelectMoonLightRight6 = true;
//			textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.blue));
//			textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
//		}else if (timeData[6] == 3) {
//			isSelectMoonLightRight3 = true;
//			textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.blue));
//		}else if (timeData[6] == 0) {
//			textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
//			textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
//		}
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_timer_setting);

		linearLayoutTimerOn = (LinearLayout) findViewById(R.id.linearLayoutTimerOn);
		linearLayoutTimerOff = (LinearLayout) findViewById(R.id.linearLayoutTimerOff);
		
		textViewStart = (TextView) findViewById(R.id.textViewStart);
		textViewStop = (TextView) findViewById(R.id.textViewStop);
		
		timerStart = (LinearLayout) findViewById(R.id.timerStart);
		timerStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isTimerStart = true;
				textViewStart.setTextColor(getResources().getColor(R.color.blue));
				textViewStop.setTextColor(getResources().getColor(R.color.black));
				
//				int timeData[] = SharePersistent.getTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "deviceName"));
//				openHour = timeData[0];
//				openMinite = timeData[1];
				
				listViewStartH.setCurrentItem(openHour);
				listViewStartM.setCurrentItem(openMinite);
			}
		});
		timerStop = (LinearLayout) findViewById(R.id.timerStop);
		timerStop.setOnClickListener(new OnClickListener() {	

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isTimerStart = false;
				textViewStart.setTextColor(getResources().getColor(R.color.black));
				textViewStop.setTextColor(getResources().getColor(R.color.blue));
				
//				int timeData[] = SharePersistent.getTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "deviceName"));
//				closeHour = timeData[2];
//				closeMinite = timeData[3];
				
				listViewStartH.setCurrentItem(closeHour);
				listViewStartM.setCurrentItem(closeMinite);
			}
		});

		textViewHourOnTime = (TextView) findViewById(R.id.textViewHourOnTime);
		textViewMinuteOnTime = (TextView) findViewById(R.id.textViewMinuteOnTime);
		textViewHourOffTime = (TextView) findViewById(R.id.textViewHourOffTime);
		textViewMinuteOffTime = (TextView) findViewById(R.id.textViewMinuteOffTime);
		
		textViewOnTimeMode = (TextView) findViewById(R.id.textViewOnTimeMode);
		textViewOffTimeMode = (TextView) findViewById(R.id.textViewOffTimeMode);
		
		tvMode = (TextView) findViewById(R.id.tvMode);

		toggleButtonOn = (ToggleButton) findViewById(R.id.toggleOn);
		toggleButtonOff = (ToggleButton) findViewById(R.id.toggleOff);

		listViewStartH = (WheelView) findViewById(R.id.listViewStartH);
		listViewStartM = (WheelView) findViewById(R.id.listViewStartM);

		
		textViewMoonLightModeOn = (TextView) findViewById(R.id.textViewMoonLightModeOn);
		textViewMoonLightModeOn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NetConnectBle.getInstance().turnOnOrOffMoonLightMode(1);
				textViewMoonLightModeOn.setTextColor(getResources().getColor(R.color.blue));
				textViewMoonLightModeOff.setTextColor(getResources().getColor(R.color.black));
				onOffMode = 2;
				canSendData = true;
			}
		});
		textViewMoonLightModeOff = (TextView) findViewById(R.id.textViewMoonLightModeOff);
		textViewMoonLightModeOff.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NetConnectBle.getInstance().turnOnOrOffMoonLightMode(0);
				textViewMoonLightModeOff.setTextColor(getResources().getColor(R.color.blue));
				textViewMoonLightModeOn.setTextColor(getResources().getColor(R.color.black));
				
				textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
				textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
				textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
				textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
				textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
				textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
				
				onOffMode = 1;
				canSendData = false;
			}
		});
		textViewMoonLightLeft9 = (TextView) findViewById(R.id.textViewMoonLightLeft9);
		textViewMoonLightLeft9.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightLeft9) {
						leftHourMode = 0;
						textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(0, 0);
						isSelectMoonLightLeft9 = false;
						
					}else {
						textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(0, 9);
						leftHourMode = 9;
						isSelectMoonLightLeft9 = true;
						isSelectMoonLightLeft6 = false;
						isSelectMoonLightLeft3 = false;
					}
					
					textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
				}
				
				
			}
		});
		textViewMoonLightLeft6 = (TextView) findViewById(R.id.textViewMoonLightLeft6);
		textViewMoonLightLeft6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightLeft6) {
						leftHourMode = 0;
						textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(0, 0);
						isSelectMoonLightLeft6 = false;
					}else {
						textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(0, 6);
						leftHourMode = 6;
						isSelectMoonLightLeft6 = true;
						isSelectMoonLightLeft9 = false;
						isSelectMoonLightLeft3 = false;	
					}
					
					textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
				}
			}
		});
		textViewMoonLightLeft3 = (TextView) findViewById(R.id.textViewMoonLightLeft3);
		textViewMoonLightLeft3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightLeft3) {
						leftHourMode = 0;
						textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(0, 0);
						isSelectMoonLightLeft3 = false;
					}else {
						textViewMoonLightLeft3.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(0, 3);
						leftHourMode = 3;
						isSelectMoonLightLeft3 = true;
						isSelectMoonLightLeft6 = false;
						isSelectMoonLightLeft9 = false;
					}
					
					textViewMoonLightLeft9.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightLeft6.setTextColor(getResources().getColor(R.color.black));
				}
				
			}
		});
		textViewMoonLightRight3 = (TextView) findViewById(R.id.textViewMoonLightRight3);
		textViewMoonLightRight3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightRight3) {
						rightHourMode = 0;
						textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(1, 0);
						isSelectMoonLightRight3 = false;
					}else {
						textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(1, 3);
						rightHourMode = 3;
						isSelectMoonLightRight3 = true;
						isSelectMoonLightRight6 = false;
						isSelectMoonLightRight9 = false;
					}				
					
					textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
				}
			}
		});
		textViewMoonLightRight6 = (TextView) findViewById(R.id.textViewMoonLightRight6);
		textViewMoonLightRight6.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightRight6) {
						rightHourMode = 0;
						textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(1, 0);
						isSelectMoonLightRight6 = false;
					}else {
						textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(1, 6);
						rightHourMode = 6;
						isSelectMoonLightRight6 = true;
						isSelectMoonLightRight3 = false;
						isSelectMoonLightRight9 = false;
					}
										
					textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
				}
			}
		});
		textViewMoonLightRight9 = (TextView) findViewById(R.id.textViewMoonLightRight9);
		textViewMoonLightRight9.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if (canSendData) {
					if (isSelectMoonLightRight9) {
						rightHourMode = 0;
						textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.black));
						NetConnectBle.getInstance().setMoonLightMode(1, 0);
						isSelectMoonLightRight9 = false;
					}else {
						textViewMoonLightRight9.setTextColor(getResources().getColor(R.color.blue));
						NetConnectBle.getInstance().setMoonLightMode(1, 9);
						rightHourMode = 9;
						isSelectMoonLightRight9 = true;
						isSelectMoonLightRight3 = false;
						isSelectMoonLightRight6 = false;
					}
					
					textViewMoonLightRight3.setTextColor(getResources().getColor(R.color.black));
					textViewMoonLightRight6.setTextColor(getResources().getColor(R.color.black));
					
				}
			}
		});
		
		
		textViewOKButton = (TextView) findViewById(R.id.textViewOKButton);
		textViewOKButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				NetConnectBle.getInstance().timerOn(openHour, openMinite, 1);
				NetConnectBle.getInstance().timerOff(closeHour, closeMinite);
				
				try {
					Thread.sleep(50);
					NetConnectBle.getInstance().setMoonLightMode(0, leftHourMode);
					NetConnectBle.getInstance().setMoonLightMode(1, rightHourMode);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
//				SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "deviceName"), openHour, openMinite, closeHour, closeMinite, onOffMode, leftHourMode, rightHourMode);
				
				Intent intent = new Intent();
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});
		
		textViewBackButton = (TextView) findViewById(R.id.textViewBackButton);
		textViewBackButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
//				NetConnectBle.getInstance().timerOn(openHour, openMinite, 1);
//				NetConnectBle.getInstance().timerOff(closeHour, closeMinite);
				
				Intent intent = new Intent();
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});


		String[] modelH = new String[24];
		for (int i = 0; i < 24; i++) {
			modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterH = new WheelModelAdapter(getApplicationContext(), modelH);
		this.listViewStartH.setViewAdapter(wheelAdapterH);
		this.listViewStartH.setCurrentItem(openHour);
		this.listViewStartH.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {

				if (isTimerStart) {
					openHour = newValue;
					textViewHourOnTime.setText(NumberHelper.LeftPad_Tow_Zero(openHour));
//					NetConnectBle.getInstance().timerOn(openHour, openMinite, 1);
										
					if (newValue > 12) {
						textViewOnTimeMode.setText("PM");
//						SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);
					}else {
						textViewOnTimeMode.setText("AM");
//						SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);
					}

				} else {
					closeHour = newValue;
					textViewHourOffTime.setText(NumberHelper.LeftPad_Tow_Zero(closeHour));
					
					if (newValue > 12) {
						textViewOffTimeMode.setText("PM");
//						SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);
					}else {
						textViewOffTimeMode.setText("AM");
//						SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);
					}
				}	
			}
		});

		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterM = new WheelModelAdapter(getApplicationContext(), modelM);
		this.listViewStartM.setViewAdapter(wheelAdapterM);
		this.listViewStartM.setCurrentItem(openMinite);
		// this.listViewModel.setCyclic(true);
		this.listViewStartM.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {

				if (isTimerStart) {
					openMinite = newValue;
					textViewMinuteOnTime.setText(NumberHelper.LeftPad_Tow_Zero(openMinite));
//					SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);

				} else {
					closeMinite = newValue;
					textViewMinuteOffTime.setText(NumberHelper.LeftPad_Tow_Zero(closeMinite));
//					SharePersistent.saveTimerData(getApplicationContext(), SharePersistent.getPerference(getApplicationContext(), "groupName"), openHour, openMinite, closeHour, closeMinite);
				}
			}
		});
		
//		int startData[] = SharePersistent.getTimerData(getApplicationContext(), startkey);
//		openHour = startData[0];
//		openMinite = startData[1];
		
		listViewStartH.setCurrentItem(openHour);
		listViewStartM.setCurrentItem(openMinite);
		
		
		linearLayoutTimerOff.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				gotoTimerSettting(v, INT_TIMER_OFF);
			}
		});

		toggleButtonOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if (toggleButtonOn.isChecked()) {
					NetConnectBle.getInstance().timerOn(openHour, openMinite, 1);
				} else {
					NetConnectBle.getInstance().turnOnOffTimerOn(0);
				}
			}
		});
		toggleButtonOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if (toggleButtonOff.isChecked()) {
					NetConnectBle.getInstance().timerOff(closeHour, closeMinite);
				} else {
					NetConnectBle.getInstance().turnOnOrOffTimerOff(0);
				}
			}
		});
		
		
		
	}

	private void putDataback() {

	}

	private void gotoTimerSettting(View v, int id) {

		if ("off".equalsIgnoreCase((String) v.getTag())) { // 定时关
			

		} else { // 定时开
			tvMode.setText(getResources().getString(R.string.timer_on));
//			isTimerOpen = true;

		}

		// Intent intent = new Intent(getActivity(),
		// TimerSettingActivity.class);
		// intent.putExtra("tag", (String) v.getTag());
		// getActivity().startActivityForResult(intent, id);
	}
}
