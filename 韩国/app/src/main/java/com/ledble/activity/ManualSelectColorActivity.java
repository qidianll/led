package com.ledble.activity;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.common.BaseProgressActivity;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.ledble.view.leafchart.LeafLineChart;
import com.ledble.view.leafchart.bean.Axis;
import com.ledble.view.leafchart.bean.AxisValue;
import com.ledble.view.leafchart.bean.Line;
import com.ledble.view.leafchart.bean.PointValue;


import java.util.ArrayList;
import java.util.List;

import com.aquariumstudio.R;

@SuppressLint("NewApi")
public class ManualSelectColorActivity extends BaseProgressActivity {


    private TextView textViewRed;
    private TextView textViewGreen;
    private TextView textViewBlue;
    private TextView textViewWhite;
    private SeekBar seekBarRed;
    private SeekBar seekBarGreen;
    private SeekBar seekBarBlue;
    private SeekBar seekBarWhite;

    private int redValue = 0;
    private int greenValue = 0;
    private int blueValue = 0;
    private int whiteValue = 0;


    private boolean isRed;
    private boolean isGreen;
    private boolean isBlue;
    private boolean isWhite;

    private LeafLineChart lineChart;
    private float lineRedvalue;
    private float lineGreenvalue;
    private float lineBluevalue;
    private float lineWhitevalue;

//	private float tempLineRedvalue;
//	private float tempLineGreenvalue;
//	private float tempLineBluevalue;
//	
//	private float tempWhiteInRedvalue;
//	private float tempWhiteInGreenvalue;
//	private float tempWhiteInBluevalue;
//	
//	private float tempLine5value;
//	private float tempLine7value;
//	private float tempLine8value;
//	private float tempLine10value;
//	
//	private float tempGreenValueState1 = 40f;  // R 100, B 100, W 100
//	private float tempBlueValueState1 = 80f;  // R 100, G 100, W 100
//	private float tempRedValueState1 = 5f;  // B 100, G 100, W 100
//	private float tempLine8valueState1 = 10f;  // B 100, G 100, W 100
//	private float tempLine7valueState1 = 30f;  // B 100, G 100, W 100
//	private float tempLine5valueState1 = 30f;  // B 100, G 100, W 100


    private List<PointValue> newPointValue = new ArrayList<>();

    private int red, blue, green;
    private float r, g, b, wr, wg, wb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);


        initPointValue();
        initView();


    }

    public void initPointValue() {

        for (float i = 1; i <= 12; i++) {
            PointValue pointValue = new PointValue();
            pointValue.setX((i - 1) / 11f);
            pointValue.setY(0);
            pointValue.setLabel(String.valueOf(0));
            newPointValue.add(pointValue);
        }

    }

    @Override
    public void initView() {
        setContentView(R.layout.activity_manual_select_color);


        MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);

        // RGB 光谱

        lineChart = (LeafLineChart) findViewById(R.id.leaf_chart);

        initLineChart();


        seekBarRed = (SeekBar) findViewById(R.id.seekBarRed);
        textViewRed = findTextViewById(R.id.textViewRed);
//		seekBarRed.setProgress(SharePersistent.getInt(getApplicationContext(), "ManualSetRed"));
        this.seekBarRed.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                redValue = progress;
                textViewRed.setText("R                 " + progress);
                MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);
                SharePersistent.savePerference(getApplicationContext(), "ManualSetRed", progress);

                isRed = true;
                isGreen = false;
                isBlue = false;
                isWhite = false;
                lineRedvalue = progress;

                initLineChart();
            }
        });

        seekBarGreen = (SeekBar) findViewById(R.id.seekBarGreen);
        textViewGreen = findTextViewById(R.id.textViewGreen);
//		seekBarGreen.setProgress(SharePersistent.getInt(getApplicationContext(), "ManualSetGreen"));
        this.seekBarGreen.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                greenValue = progress;
                textViewGreen.setText("G                 " + progress);
                MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);
                SharePersistent.savePerference(getApplicationContext(), "ManualSetGreen", progress);

//				var = (int) (Math.random() * 100);
                isRed = false;
                isGreen = true;
                isBlue = false;
                isWhite = false;
                lineGreenvalue = progress;

                initLineChart();

            }
        });

        seekBarBlue = (SeekBar) findViewById(R.id.seekBarBlue);
        textViewBlue = findTextViewById(R.id.textViewBlue);
//		seekBarBlue.setProgress(SharePersistent.getInt(getApplicationContext(), "ManualSetBlue"));
        this.seekBarBlue.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                blueValue = progress;
                textViewBlue.setText("B                 " + progress);
                MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);
                SharePersistent.savePerference(getApplicationContext(), "ManualSetBlue", progress);

                isRed = false;
                isGreen = false;
                isBlue = true;
                isWhite = false;
                lineBluevalue = progress;
                initLineChart();

            }
        });

        seekBarWhite = (SeekBar) findViewById(R.id.seekBarWhite);
        textViewWhite = findTextViewById(R.id.textViewWhite);
//		seekBarWhite.setProgress(SharePersistent.getInt(getApplicationContext(), "ManualSetWhite"));
        this.seekBarWhite.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                whiteValue = progress;
                textViewWhite.setText("W                 " + progress);
                MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);
                SharePersistent.savePerference(getApplicationContext(), "ManualSetWhite", progress);


                isRed = false;
                isGreen = false;
                isBlue = false;
                isWhite = true;
                lineWhitevalue = progress;
                initLineChart();

            }
        });


        TextView textViewBack = findTextViewById(R.id.textViewBack);
        textViewBack.setOnClickListener(new OnClickListener() { // 返回
            @Override
            public void onClick(View v) {
//				Intent intent = new Intent();
//				setResult(Activity.RESULT_OK, intent);
                MainActivity.getMainActivity().setExitToNormalMode(redValue, greenValue, blueValue, whiteValue);
                finish();
            }
        });

    }


    private void initLineChart() {
        Axis axisX = new Axis(getAxisValuesX());
        axisX.setAxisColor(Color.BLACK).setTextColor(Color.WHITE).setHasLines(false);
        Axis axisY = new Axis(getAxisValuesY());
        axisY.setAxisColor(Color.BLACK).setTextColor(Color.WHITE).setHasLines(false).setShowText(true);
        lineChart.setAxisX(axisX);
        lineChart.setAxisY(axisY);
        List<Line> lines = new ArrayList<>();
        if (isRed) {
            lines.add(getRedLine());
        } else if (isGreen) {
            lines.add(getGreenLine());
        } else if (isBlue) {
            lines.add(getBlueLine());
        } else if (isWhite) {
            lines.add(getWhiteLine());
        } else {
            lines.add(getFoldLine());
        }

        lineChart.setChartData(lines);

        lineChart.showWithAnimation(/*2000*/0);

    }

    private List<AxisValue> getAxisValuesX() {
        List<AxisValue> axisValues = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            AxisValue value = new AxisValue();

            if (i == 2) {
                value.setLabel(400 + "");
            } else if (i == 5) {
                value.setLabel(500 + "");
            } else if (i == 8) {
                value.setLabel(600 + "");
            } else if (i == 11) {
                value.setLabel(700 + "");
            } else {
                value.setLabel("");
            }

//            value.setLabel(i + "");
            axisValues.add(value);
        }
        return axisValues;
    }

    private List<AxisValue> getAxisValuesY() {
        List<AxisValue> axisValues = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            AxisValue value = new AxisValue();
//            value.setLabel(String.valueOf(i * 10));

            if (i == 2) {
                value.setLabel(0.2 + "");
            } else if (i == 4) {
                value.setLabel(0.4 + "");
            } else if (i == 6) {
                value.setLabel(0.6 + "");
            } else if (i == 8) {
                value.setLabel(0.8 + "");
            } else if (i == 10) {
                value.setLabel(1.0 + "");
            } else {
                value.setLabel("");
            }

            axisValues.add(value);
        }
        return axisValues;
    }

    private Line getFoldLine() {
        List<PointValue> pointValues = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            PointValue pointValue = new PointValue();

            pointValue.setX((i - 1) / 11f);

            float var = 0;
//			if (i == 4) {
//				var = lineWhitevalue*80f/100f;
//			}else if (i == 5) {
//				var = lineWhitevalue*20f/100f;
//			}else if (i == 6) {
//				var = lineWhitevalue*40f/100f;
//			}else if (i == 7) {
//				var = lineWhitevalue*20f/100f;
//			}else if (i == 8) {
//				var = lineWhitevalue*10f/100f;
//			}else if (i == 9) {
//				var = lineWhitevalue*5f/100f;
//			}else {
//				var = 0;
//			}
//			else {
//				var = (int) (Math.random() * 100);
//				value = var;
//			}


            pointValue.setLabel(String.valueOf(var));
            pointValue.setY(var / 100f);
            pointValues.add(pointValue);
        }

        Line line = new Line(pointValues);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.BLACK)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }

    private Line getRedLine() {
        for (int i = 1; i <= newPointValue.size(); i++) {
            PointValue pointValue = newPointValue.get(i - 1);
            if (i == 9) {
                r = lineRedvalue * 90f / 100f + 2 * lineWhitevalue / 100;
//				tempLineRedvalue = var;
                pointValue.setY(r / 100f);
                pointValue.setLabel(String.valueOf(r));
            } else if (i == 8) {
                r = lineRedvalue * 20f / 100f + 4 * lineWhitevalue / 100;
//				tempLine8value = var;
                pointValue.setY(r / 100f);
                pointValue.setLabel(String.valueOf(r));
            } else if (i == 10) {
                r = lineRedvalue * 15f / 100f + 1 * lineWhitevalue / 100;
//				tempLine10value = var;
                pointValue.setY(r / 100f);
                pointValue.setLabel(String.valueOf(r));
            }
        }
        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }

    private Line getGreenLine() {
        for (int i = 1; i <= newPointValue.size(); i++) {
            PointValue pointValue = newPointValue.get( i - 1);
            if (i == 6) {
                g = lineGreenvalue * 20f / 100f + 40 * lineWhitevalue / 100;
                pointValue.setY(g / 100f);
                pointValue.setLabel(String.valueOf(g));
            } else if (i == 7) {
                g = lineGreenvalue * 10f / 100f + 40 * lineWhitevalue / 100 * 3 / 10;
                pointValue.setY(g / 100f);
                pointValue.setLabel(String.valueOf(g));
            }
        }
        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }

    private Line getBlueLine() {
        for (int i = 1; i <= newPointValue.size(); i++) {
            PointValue pointValue = newPointValue.get(i - 1);

            if (i == 4) {
                b = lineBluevalue * 20f / 100f + 80 * lineWhitevalue / 100;
//				tempLine8value = var;
                pointValue.setY(b / 100f);
                pointValue.setLabel(String.valueOf(b));
            }else if (i == 5) {
                b = lineBluevalue * 15f / 100f + 80 * lineWhitevalue / 100 * 3 / 4 / 2;
//				tempLine10value = var;
                pointValue.setY(b / 100f);
                pointValue.setLabel(String.valueOf(b));
            }
        }
        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }

    private Line getWhiteLine() {
        for (int i = 1; i <= newPointValue.size(); i++) {
            PointValue pointValue = newPointValue.get(i - 1);
            if (i == 4) { // 蓝色20,20,15
                wb = lineWhitevalue * 80f / 100f + 20f * (lineBluevalue / 100);
//					tempWhiteInBluevalue = var;
                pointValue.setY(wb / 100f);
                pointValue.setLabel(String.valueOf(wb));
            } else if (i == 5) {
                wb = lineWhitevalue * 30f / 100f + 15f * (lineBluevalue / 100);
                pointValue.setY(wb / 100f);
                pointValue.setLabel(String.valueOf(wb));
            } else if (i == 6) { // 绿色20,20,15
                wg = lineWhitevalue * (40f) / 100f + 20f * (lineGreenvalue / 100);
//					tempWhiteInGreenvalue = var;
                pointValue.setY(wg / 100f);
                pointValue.setLabel(String.valueOf(wg));
            } else if (i == 7) {
                wg = lineWhitevalue * 12f / 100f + 10f * (lineGreenvalue / 100);
                pointValue.setY(wg / 100f);
                pointValue.setLabel(String.valueOf(wg));
            } else if (i == 8) {//红色20,90,15
                wg = lineWhitevalue * 4f / 100f + 20f * (lineRedvalue / 100);
                pointValue.setY(wg / 100f);
                pointValue.setLabel(String.valueOf(wg));
            } else if (i == 9) {
                wr = lineWhitevalue * 2f / 100f + 90f * (lineRedvalue / 100);
                pointValue.setY(wr / 100f);
                pointValue.setLabel(String.valueOf(wr));
            } else if (i == 10) {
                wr = lineWhitevalue * 1f / 100f + 15f * (lineRedvalue / 100);
                pointValue.setY(wr / 100f);
                pointValue.setLabel(String.valueOf(wr));
            }
        }
        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#FF0000"));
        return line;
    }
}