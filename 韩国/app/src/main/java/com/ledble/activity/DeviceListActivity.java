package com.ledble.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.uitl.SharePersistent;
import com.aquariumstudio.R;
import com.ledble.adapter.BleSelectDeviceAdapter;
import com.ledble.base.LedBleApplication;
import com.ledble.db.GroupDevice;

public class DeviceListActivity extends BaseActivity {

	private ListView listViewDevices;
	private BleSelectDeviceAdapter bleDeviceAdapter;
	private String groupName = "";
	private int indexForGroupRow = -1;
	private ArrayList<GroupDevice> groupDevices;
	private int prePosition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_device_list);
		groupName = getIntent().getStringExtra("group");
		indexForGroupRow = getIntent().getIntExtra("indexForGroupRow", -1);
		groupDevices = (ArrayList<GroupDevice>) getIntent().getSerializableExtra("devices");

		listViewDevices = findListViewById(R.id.listViewDevices);
		bleDeviceAdapter = new BleSelectDeviceAdapter(this);
		bleDeviceAdapter.setSelected(groupDevices);
		listViewDevices.setAdapter(bleDeviceAdapter);
		
		listViewDevices.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				BluetoothDevice ble = bleDeviceAdapter.getDevice(position);
				bleDeviceAdapter.select(ble);
				bleDeviceAdapter.notifyDataSetChanged();
				
				
//				for (int j = 0; j < 11; j++) {
//					String [] groupData = SharePersistent.getGroupData(getApplicationContext(), Integer.toString(j));
//					if (groupData[1].equalsIgnoreCase(groupName)) {
//						SharePersistent.saveGroupData(getApplicationContext(), Integer.toString(indexForGroupRow), Integer.toString(indexForGroupRow), groupName, ble.getName());
//					}
//				}
			}
		});

		findViewById(R.id.textViewBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		findButtonById(R.id.buttonFireBind).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LedBleApplication.getApp().setTempDevices(bleDeviceAdapter.getSelectSet());
				Intent data = new Intent();
				data.putExtra("group", groupName);
				setResult(Activity.RESULT_OK, data);
				finish();
			}
		});

	}
}
