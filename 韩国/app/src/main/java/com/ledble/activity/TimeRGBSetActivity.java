package com.ledble.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseProgressActivity;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;

import java.util.ArrayList;
import java.util.List;

import com.aquariumstudio.R;
import com.ledble.view.leafchart.LeafLineChart;
import com.ledble.view.leafchart.bean.Axis;
import com.ledble.view.leafchart.bean.AxisValue;
import com.ledble.view.leafchart.bean.Line;
import com.ledble.view.leafchart.bean.PointValue;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;

public class TimeRGBSetActivity extends BaseProgressActivity {

	private String tag = "tag";
	private String hour = "";
	private String minunte = "";
	private TextView textViewH;
	
	private int openMinite = 0;
	private WheelModelAdapter wheelAdapterM;
	private WheelView listViewStartM;
	
	private TextView textViewRed;
	private TextView textViewGreen;
	private TextView textViewBlue;
	private TextView textViewWhite;
	private SeekBar seekBarRed;
	private SeekBar seekBarGreen;
	private SeekBar seekBarBlue;
	private SeekBar seekBarWhite;
	
	private int hourValue = -1;
	private int minuteValue = -1;
	private int redValue = -1;
	private int greenValue = -1;
	private int blueValue = -1;
	private int whiteValue = -1;
	private int co2Value = -1;
	
//	private int hourValue = 0;
//	private int minuteValue = 0;
//	private int redValue = 0;
//	private int greenValue = 0;
//	private int blueValue = 0;
//	private int whiteValue = 0;
//	private int co2Value = 0;
//	
	private int tmpHourValue = -1;
	private int tmpMinuteValue = -1;
	private int tmpRedValue = -1;
	private int tmpGreenValue = -1;
	private int tmpBlueValue = -1;
	private int tmpWhiteValue = -1;
	private int tmpCo2Value = -1;
	
	private int tmpValue = -1;
	
//	private int tmpHourValue = 0;
//	private int tmpMinuteValue = 0;
//	private int tmpRedValue = 0;
//	private int tmpGreenValue = 0;
//	private int tmpBlueValue = 0;
//	private int tmpWhiteValue = 0;
//	private int tmpCo2Value = 0;
	
	private int statusON = -1;
	private TextView textViewON;
	private TextView textViewOFF;
	
	private TextView textViewBack;
	private TextView textViewSave;
	
	
	
	// RGB 光谱
	private LeafLineChart lineChart;
	private List<PointValue> newPointValue = new ArrayList<>();
	
	private boolean isRed;
	private boolean isGreen;
	private boolean isBlue;
	private boolean isWhite;
	
	private float lineRedvalue;
	private float lineGreenvalue;
	private float lineBluevalue;
	private float lineWhitevalue;
	
	private float tempLineRedvalue;
	private float tempLineGreenvalue;
	private float tempLineBluevalue;
	
	private float tempLine5value;
	private float tempLine7value;
	private float tempLine8value;
	private float tempLine10value;
	
	private float tempGreenValueState1 = 40f;  // R 100, B 100, W 100
	private float tempBlueValueState1 = 80f;  // R 100, G 100, W 100
	private float tempRedValueState1 = 10f;  // B 100, G 100, W 100
	private float tempLine8valueState1 = 20f;  // B 100, G 100, W 100
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		
		initPointValue();
		initView();
		
	}
	
	public void initPointValue() {
		
		for (int i = 1; i <= 12; i++) {
            PointValue pointValue = new PointValue();
            pointValue.setX( (i - 1) / 11f);
            pointValue.setY(0);
            pointValue.setLabel(String.valueOf(0));           
            newPointValue.add(pointValue);
        }
		
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_timergb_set);	


		// RGB 光谱
		lineChart = (LeafLineChart) findViewById(R.id.leaf_chart);				
		
		
		listViewStartM = (WheelView) findViewById(R.id.listViewStartM);
		String[] modelM = new String[60];
		for (int i = 0; i < 60; i++) {
			modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
		}
		wheelAdapterM = new WheelModelAdapter(getApplicationContext(), modelM);
		listViewStartM.setViewAdapter(wheelAdapterM);
//		listViewStartM.setCurrentItem(SharePersistent.getInt(getApplicationContext(), "TimeSetM"));
		listViewStartM.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				minuteValue = newValue;
//				MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
//				SharePersistent.savePerference(getApplicationContext(), "TimeSetM", newValue);
			}
		});
		

		seekBarRed = (SeekBar)findViewById(R.id.seekBarRed);
		textViewRed = findTextViewById(R.id.textViewRed);
//		seekBarRed.setProgress(SharePersistent.getInt(getApplicationContext(), "TimeSetRed"));
		this.seekBarRed.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				redValue = progress;
				tmpValue = progress;
				textViewRed.setText("R                 "+progress);	
				MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
//				SharePersistent.savePerference(getApplicationContext(), "TimeSetRed", progress);
				
				isRed = true;
				isGreen = false;
				isBlue = false;
				isWhite = false;
				lineRedvalue = progress;
				
				initLineChart();
			}
		});

		seekBarGreen = (SeekBar)findViewById(R.id.seekBarGreen);
		textViewGreen = findTextViewById(R.id.textViewGreen);
//		seekBarGreen.setProgress(SharePersistent.getInt(getApplicationContext(), "TimeSetGreen"));
		this.seekBarGreen.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				greenValue = progress;
				tmpValue = progress;
				textViewGreen.setText("G                 "+progress);	
				MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
//				SharePersistent.savePerference(getApplicationContext(), "TimeSetGreen", progress);
				
				isRed = false;
				isGreen = true;
				isBlue = false;
				isWhite = false;
				lineGreenvalue = progress;
				
				initLineChart();
			}
		});

		seekBarBlue = (SeekBar)findViewById(R.id.seekBarBlue);
		textViewBlue = findTextViewById(R.id.textViewBlue);
//		seekBarBlue.setProgress(SharePersistent.getInt(getApplicationContext(), "TimeSetBlue"));
		this.seekBarBlue.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				blueValue = progress;
				tmpValue = progress;
				textViewBlue.setText("B                 "+progress);	
				MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
//				SharePersistent.savePerference(getApplicationContext(), "TimeSetBlue", progress);
				
				isRed = false;
				isGreen = false;
				isBlue = true;
				isWhite = false;
				lineBluevalue = progress;
				initLineChart();
			}
		});
		
		seekBarWhite = (SeekBar)findViewById(R.id.seekBarWhite);
		textViewWhite = findTextViewById(R.id.textViewWhite);
//		seekBarWhite.setProgress(SharePersistent.getInt(getApplicationContext(), "TimeSetWhite"));
		this.seekBarWhite.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				whiteValue = progress;
				tmpValue = progress;
				textViewWhite.setText("W                 "+progress);			
				MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);	
//				SharePersistent.savePerference(getApplicationContext(), "TimeSetWhite", progress);
				
				isRed = false;
				isGreen = false;
				isBlue = false;
				isWhite = true;
				lineWhitevalue = progress;
				initLineChart();
			}
		});
		
		
		textViewON = findTextViewById(R.id.textViewON);
		textViewON.setOnClickListener(new OnClickListener() { // ON
			@Override
			public void onClick(View v) {
				switch (statusON) {
				case -1:
					textViewON.setBackgroundResource(R.color.green);
					textViewOFF.setBackgroundResource(R.color.transparent);
					co2Value = 1;
					statusON = 1;
					MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
					break;
				case 0:
					textViewON.setBackgroundResource(R.color.green);
					textViewOFF.setBackgroundResource(R.color.transparent);
					co2Value = 1;
					statusON = 1;
					MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
					break;
				case 1:
//					textViewON.setBackgroundResource(R.color.transparent);
//					textViewOFF.setBackgroundResource(R.drawable.co2_check);
//					co2Value = 0;
//					statusON = 0;
					break;
				default:
					break;
				}
				tmpValue = statusON;
			}
		});
		
		textViewOFF = findTextViewById(R.id.textViewOFF);
		textViewOFF.setOnClickListener(new OnClickListener() { // OFF
			@Override
			public void onClick(View v) {
				switch (statusON) {
				case -1:									
					textViewON.setBackgroundResource(R.color.transparent);
					textViewOFF.setBackgroundResource(R.color.green);
					co2Value = 0;
					statusON = 0;
					MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
					break;
				case 0:
//					textViewON.setBackgroundResource(R.drawable.co2_check);
//					textViewOFF.setBackgroundResource(R.color.transparent);
//					co2Value = 0;
//					statusON = 1;
					break;
				case 1:									
					textViewON.setBackgroundResource(R.color.transparent);
					textViewOFF.setBackgroundResource(R.color.green);
					co2Value = 0;
					statusON = 0;
					MainActivity.getMainActivity().setColorCommandWithCo2(hourValue, minuteValue, redValue, greenValue, blueValue, whiteValue, co2Value);
					break;
				default:
					break;
				}
				tmpValue = statusON;
			}
		});
		
		
		textViewBack = findTextViewById(R.id.textViewBack);
		textViewBack.setOnClickListener(new OnClickListener() { // 返回
			@Override
			public void onClick(View v) {
				Intent intent = new Intent();
				
				if (tmpMinuteValue == minuteValue && tmpRedValue == redValue && tmpGreenValue == greenValue && 
						tmpBlueValue == blueValue && tmpWhiteValue == whiteValue && tmpCo2Value == co2Value) {
					setResult(Activity.RESULT_OK, intent);
					finish();
				}				
				if (tmpValue >= 0 && redValue == -1) {
					redValue = 0;
				}
				if (tmpValue >= 0 && greenValue == -1) {
					greenValue = 0;
				}
				if (tmpValue >= 0 && blueValue == -1) {
					blueValue = 0;
				}
				if (tmpValue >= 0 && whiteValue == -1) {
					whiteValue = 0;
				}
				if (tmpValue >= 0 && co2Value == -1) {
					co2Value = 0;
				}
				
				intent.putExtra("tag", tag);
				intent.putExtra("hour", hourValue);
				intent.putExtra("minute", minuteValue >= 0 ? minuteValue : 0);
				intent.putExtra("R", redValue);
				intent.putExtra("G", greenValue);
				intent.putExtra("B", blueValue);
				intent.putExtra("W", whiteValue);
				intent.putExtra("co2", co2Value);
				setResult(Activity.RESULT_OK, intent);
				
				MainActivity.getMainActivity().setExitToNormalMode(redValue, greenValue, blueValue, whiteValue);
				finish();
			}
		});
		
		
		
		//************** 初始化 数据***********************//
		
		if (null != getIntent()) {
			hour = getIntent().getStringExtra("hour");	
			minunte = getIntent().getStringExtra("minute");	
			
			textViewH = findTextViewById(R.id.textViewH);
			textViewH.setText(hour+"h");
			hourValue = Integer.valueOf(hour);
			
			listViewStartM.setCurrentItem(Integer.parseInt(minunte));
			
			if (getIntent().getStringExtra("R").equalsIgnoreCase("-") && 
				getIntent().getStringExtra("G").equalsIgnoreCase("-") && 
				getIntent().getStringExtra("B").equalsIgnoreCase("-") && 
				getIntent().getStringExtra("W").equalsIgnoreCase("-")) {
				
				textViewRed.setText("R                 "+"-");	
				textViewGreen.setText("G                 "+"-");
				textViewBlue.setText("B                 "+"-");	
				textViewWhite.setText("W                 "+"-");	
				
				return;
			}
			
//			if (getIntent().getStringExtra("R").equalsIgnoreCase("-") || getIntent().getStringExtra("R").equalsIgnoreCase("-1")) {
//				textViewRed.setText("R                 "+"-");	
//			}
//			if (getIntent().getStringExtra("G").equalsIgnoreCase("-") || getIntent().getStringExtra("G").equalsIgnoreCase("-1")) {
//				textViewGreen.setText("G                 "+"-");
//			}
//			if (getIntent().getStringExtra("B").equalsIgnoreCase("-") || getIntent().getStringExtra("B").equalsIgnoreCase("-1")) {
//				textViewBlue.setText("B                 "+"-");	
//			}
//			if (getIntent().getStringExtra("W").equalsIgnoreCase("-") || getIntent().getStringExtra("W").equalsIgnoreCase("-1")) {
//				textViewWhite.setText("W                 "+"-");	
//			}
			
			
			if (Integer.valueOf(getIntent().getStringExtra("R")) >= 0) {
				seekBarRed.setProgress(Integer.valueOf(getIntent().getStringExtra("R")));
				lineRedvalue = Float.valueOf(getIntent().getStringExtra("R"));
			}
			if (Integer.valueOf(getIntent().getStringExtra("G")) >= 0) {
				seekBarGreen.setProgress(Integer.valueOf(getIntent().getStringExtra("G")));
				lineGreenvalue = Float.valueOf(getIntent().getStringExtra("G"));
			}
			if (Integer.valueOf(getIntent().getStringExtra("B")) >= 0) {
				seekBarBlue.setProgress(Integer.valueOf(getIntent().getStringExtra("B")));
				lineBluevalue = Float.valueOf(getIntent().getStringExtra("B"));
			}
			if (Integer.valueOf(getIntent().getStringExtra("W")) >= 0) {
				seekBarWhite.setProgress(Integer.valueOf(getIntent().getStringExtra("W")));
				lineWhitevalue = Float.valueOf(getIntent().getStringExtra("W"));
			}
			
			initLineChart(); //初始化 RGB 光谱
			
			int Co2 = Integer.valueOf(getIntent().getStringExtra("Co2"));
			if (Co2 == 0) {
				statusON = 0;
				co2Value = 0;
				textViewON.setBackgroundResource(R.color.transparent);
				textViewOFF.setBackgroundResource(R.color.green);
			}else if (Co2 == 1) {
				statusON = 1;
				co2Value = 1;
				textViewON.setBackgroundResource(R.color.green);
				textViewOFF.setBackgroundResource(R.color.transparent);
			} 
			
			if (getIntent().getStringExtra("tag").equalsIgnoreCase("Aquascaping") || getIntent().getStringExtra("tag").equalsIgnoreCase("Tropical")) {
				listViewStartM.setEnabled(false);
				seekBarRed.setEnabled(false);
				seekBarGreen.setEnabled(false);
				seekBarBlue.setEnabled(false);
				seekBarWhite.setEnabled(false);
				textViewOFF.setEnabled(false);
				textViewON.setEnabled(false);
			}
			
		} else {
			finish();
		}
	}
	
	
	
	private void initLineChart() {
        Axis axisX = new Axis(getAxisValuesX());
        axisX.setAxisColor(Color.BLACK).setTextColor(Color.WHITE).setHasLines(false);
        Axis axisY = new Axis(getAxisValuesY());
        axisY.setAxisColor(Color.BLACK).setTextColor(Color.WHITE).setHasLines(false).setShowText(true);
        lineChart.setAxisX(axisX);
        lineChart.setAxisY(axisY);
        List<Line> lines = new ArrayList<>();
        if (isRed) {
        	lines.add(getRedLine());
		}else if (isGreen) {
			lines.add(getGreenLine());
		}else if (isBlue) {
			lines.add(getBlueLine());
		}else if (isWhite) {
			lines.add(getWhiteLine());
		}
        else {
//			lines.add(getFoldLine());
		}
        
        lineChart.setChartData(lines);

        lineChart.showWithAnimation(/*2000*/0);

    }

    private List<AxisValue> getAxisValuesX(){
        List<AxisValue> axisValues = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            AxisValue value = new AxisValue();

            if (i == 2) {
                value.setLabel(400 + "");
            }
            else if (i == 5) {
                value.setLabel(500 + "");
            }
            else if (i == 8) {
                value.setLabel(600 + "");
            }
            else if (i == 11) {
                value.setLabel(700 + "");
            }
            else {
                value.setLabel("");
            }

//            value.setLabel(i + "");
            axisValues.add(value);
        }
        return axisValues;
    }

    private List<AxisValue> getAxisValuesY(){
        List<AxisValue> axisValues = new ArrayList<>();
        for (int i = 0; i < 11; i++) {
            AxisValue value = new AxisValue();
//            value.setLabel(String.valueOf(i * 10));

            if (i == 2) {
                value.setLabel(0.2 + "");
            }
            else if (i == 4) {
                value.setLabel(0.4 + "");
            }
            else if (i == 6) {
                value.setLabel(0.6 + "");
            }
            else if (i == 8) {
                value.setLabel(0.8 + "");
            }
            else if (i == 10) {
                value.setLabel(1.0 + "");
            }
            else {
                value.setLabel("");
            }

            axisValues.add(value);
        }
        return axisValues;
    }

    private Line getFoldLine(){
        List<PointValue> pointValues = new ArrayList<>();
        for (int i = 1; i <= 12; i++) {
            PointValue pointValue = new PointValue();
            
			pointValue.setX( (i - 1) / 11f);
			
			float var = 0;
			
			if (i == 4) {
				var = lineBluevalue*80f/100f;
			}else if (i == 5) {
//				var = lineWhitevalue*20f/100f;
			}else if (i == 6) {
				var = lineGreenvalue*40f/100f;
			}else if (i == 7) {
//				var = lineWhitevalue*20f/100f;
			}else if (i == 8) {
//				var = lineWhitevalue*10f/100f;
			}else if (i == 9) {
				var = lineWhitevalue*5f/100f;
			}else {
				var = 0;
			}
//			else {
//				var = (int) (Math.random() * 100);
//				value = var;
//			}
				
            
            pointValue.setLabel(String.valueOf(var));
            pointValue.setY(var / 100f);
            pointValues.add(pointValue);
        }

        Line line = new Line(pointValues);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.BLACK)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(3)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }
    
    private Line getRedLine(){
        for (int i = 1; i <= newPointValue.size(); i++) {
        	
            PointValue pointValue = newPointValue.get(i-1);           
			
			float var = 0;
			
			if (100 == lineGreenvalue && 100 == lineBluevalue && 100 == lineWhitevalue) {
				
				if (i == 9) { 
					var = tempRedValueState1 + lineRedvalue*80f/100f;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 8) {
					var = tempLine8valueState1 + lineRedvalue*20f/100f;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 10) {
					var = lineRedvalue*15f/100f;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				} 
				
			}else {				
				
				if (i == 9) {
					var = lineRedvalue*80f/100f;
					tempLineRedvalue = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 8) {
					var = lineRedvalue*20f/100f;
					tempLine8value = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 10) {
					var = lineRedvalue*15f/100f;
					tempLine10value = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				} 
			}			
			

//			else {
//				var = (int) (Math.random() * 100);
//				value = var;
//			}
				           
                             
        }

        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }
    
    private Line getGreenLine(){
        for (int i = 1; i <= newPointValue.size(); i++) {
        	
        	PointValue pointValue = newPointValue.get(i-1);            
			
			float var = 0;
			
			if (100 == lineBluevalue && 100 == lineRedvalue && 100 == lineWhitevalue) {
				
				if (i == 6) { 
					var = tempGreenValueState1 + lineGreenvalue*10f/100f;
//					tempLineGreenvalue = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 7) {
//					var = tempGreenValueState1 +  lineGreenvalue*20f/100f;
////					tempLine7value = var;
//					pointValue.setY(var / 100f);  
//					pointValue.setLabel(String.valueOf(var));
				}
				
			}else {
				
				if (i == 6) { 
					var = lineGreenvalue*20f/100f;
					tempLineGreenvalue = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 7) {
					var = lineGreenvalue*10f/100f;
					tempLine7value = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}
			}
			
//			else {
//				var = (int) (Math.random() * 100);
//				value = var;
//			}
        }

        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }
    
    private Line getBlueLine(){
        for (int i = 1; i <= newPointValue.size(); i++) {

        	PointValue pointValue = newPointValue.get(i-1); 
			
        	float var = 0;      	
        	
        	if (100 == lineGreenvalue && 100 == lineRedvalue && 100 == lineWhitevalue) {
				
				if (i == 4) { 
					var = tempBlueValueState1 + lineBluevalue*20f/100f;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}
				
			}else {
				
				if (i == 4) {
					var = lineBluevalue*30f/100f;
					tempLineBluevalue = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}else if (i == 5) {
					var = lineBluevalue*10f/100f;
					tempLine5value = var;
					pointValue.setY(var / 100f);  
					pointValue.setLabel(String.valueOf(var));
				}
				
			}
        	
			
//			else {
//				var = (int) (Math.random() * 100);
//				value = var;
//			}
				
        }

        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#33B5E5"));
        return line;
    }
    
    private Line getWhiteLine(){
        for (int i = 1; i <= newPointValue.size(); i++) {

        	PointValue pointValue = newPointValue.get(i-1); 
			
			float var = 0;
			
			if (i == 4) { // 蓝色
				var = tempLineBluevalue + lineWhitevalue*(100f-30f)/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 5) {
				var = tempLine5value + lineWhitevalue*20f/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 6) { // 绿色
				var = tempLineGreenvalue + lineWhitevalue*(50f-20f)/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 7) {
				var = tempLine7value + lineWhitevalue*15f/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 8) {
				var = tempLine8value + lineWhitevalue*10f/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 9) { // 红色
				var = tempLineRedvalue + lineWhitevalue*(90f-80f)/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			else if (i == 10) {
				var = tempLine10value + lineWhitevalue*10f/100f;
				pointValue.setY(var / 100f);  
				pointValue.setLabel(String.valueOf(var));
			}
			
        }

        Line line = new Line(newPointValue);
        line.setLineColor(Color.parseColor("#33B5E5"))
                .setLineWidth(3)
                .setPointColor(Color.TRANSPARENT)
                .setLineColor(Color.TRANSPARENT)
                .setCubic(true)
                .setPointRadius(0)
                .setFill(true)
                .setFillColor(Color.parseColor("#FF0000"))
                .setHasLabels(true)
                .setLabelColor(Color.parseColor("#FF0000"));
        return line;
    }

}
