package com.ledble.activity;

import com.aquariumstudio.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class LoadingActivity extends Activity {
	
//	private String TAG = "LoadingActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//去掉标题栏
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//设置全屏
//		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_loading);
		
		final Handler handler = new Handler(); // 开启 定时搜索 定时器
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
			}
		};
		handler.postDelayed(runnable, 100);// 打开定时器，执行操作
		
	}

}
