package com.ledble.activity;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;

import com.common.uitl.LogUtil;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.aquariumstudio.R;
import com.ledble.base.LedBleActivity;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.MyColor;
import com.ledble.bean.ParameterBean;
import com.ledble.constant.Constant;
import com.ledble.fragment.CtFragment;
import com.ledble.fragment.DmFragment;
import com.ledble.fragment.RgbFragment;
import com.ledble.net.NetConnectBle;
import com.ledble.net.NetExceptionInterface;
import com.ledble.service.BluetoothLeServiceSingle;
import com.ledble.service.MyServiceConenction;
import com.ledble.service.MyServiceConenction.ServiceConnectListener;
import com.ledble.utils.ManageFragment;



@SuppressLint("NewApi")
public class MainActivity extends LedBleActivity implements NetExceptionInterface  {

	@Bind(R.id.segmentDm) SegmentedRadioGroup segmentDm;
	@Bind(R.id.segmentCt) SegmentedRadioGroup segmentCt;
	@Bind(R.id.segmentRgb) SegmentedRadioGroup segmentRgb;
	@Bind(R.id.segmentMusic) SegmentedRadioGroup segmentMusic;


	@Bind(R.id.ivType) ImageView ivType;
	@Bind(R.id.rgBottom) RadioGroup rgBottom;
	@Bind(R.id.rbFirst) RadioButton rbFirst;
	@Bind(R.id.rbSecond) RadioButton rbSecond;
	@Bind(R.id.rbThrid) RadioButton rbThrid;
	@Bind(R.id.rbFourth) RadioButton rbFourth;
	@Bind(R.id.rbFifth) RadioButton rbFifth;

	
	private int currentIndex;
	private FragmentManager fragmentManager;
	private RgbFragment rgbFragment;
	private List<Fragment> fragmentList = new ArrayList<Fragment>();
	
	private ArrayList<BluetoothGatt> mBluetoothGatts;

	private String blueAddress = "";
	private String groupName = "";
	private SharedPreferences sp;
	private Editor editor;
	
	private BluetoothLeServiceSingle mBluetoothLeService;
	private MyServiceConenction myServiceConenction;

	private static MainActivity mActivity;
	private DrawerLayout mDrawerLayout;

	// left_menu
	private BluetoothLeScanner bleScanner;
	private ScanCallback scanCallback;
	private BluetoothManager bluetoothManager;
	private static BluetoothAdapter mBluetoothAdapter;


	private final int MSG_START_CONNECT = 10000;// 开始连接
	private static final int REQUEST_ENABLE_BT = 1;
	private boolean isAllOn = true;
	

	private volatile HashMap<String, Boolean> hashMapLock;// service连接锁
	private volatile HashMap<String, Boolean> hashMapConnect;
	
	private Dialog lDialog;

	
	private int LOCATION_CODE = 110;
	private static int OPEN_BLE = 333;
	
	
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main);

		mActivity = this;
		
//		if (Build.VERSION.SDK_INT >= 21) {
//			View decorView = getWindow().getDecorView();
//			int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
//			decorView.setSystemUiVisibility(option);
//			getWindow().setStatusBarColor(Color.TRANSPARENT); //透明 状态栏
//		}
		
		initScanCallback(); // 初始化  ScanCallback

		initFragment();
		initView();
		
		refresh();
	}
	
	public MainActivity() {
		mActivity = this;
	}

	public static MainActivity getMainActivity() {
		return mActivity;
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// super.onSaveInstanceState(outState);
	}
	
	/**
	 * 
	 */
	protected void refresh() {
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				
				turnOnBluetooth();	
				
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 800);// 打开定时器，执行操作

	}
	
	/**
	 * 强制开启当前 Android 设备的 Bluetooth 
	 * 
	 * @return true：强制打开 Bluetooth　成功　false：强制打开 Bluetooth 失败
	 */
	public static void turnOnBluetooth() {
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		if (mBluetoothAdapter != null) {		
			
			//提示是否打开  带弹窗提醒
	        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	        mActivity.startActivityForResult(intent, OPEN_BLE);

		}

	}
	
	@SuppressLint("NewApi")
	protected void initScanCallback() {
		if (Build.VERSION.SDK_INT >= 23) {
			scanCallback = new ScanCallback() {
		        @Override
		        @SuppressLint("NewApi")
		        public void onScanResult(int callbackType, ScanResult result) {
//		            super.onScanResult(callbackType, result);
		            BluetoothDevice device = result.getDevice();
		            int rssi = result.getRssi();//获取rssi
		            //这里写你自己的逻辑
		    
//		            Toast.makeText(mActivity, "deviceName= "+device.getName(), Toast.LENGTH_SHORT).show(); 
		            if (!LedBleApplication.getApp().getBleDevices().contains(device) && device.getName() != null) {
						String name = device.getName();			

						if (name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)) {
							
							LedBleApplication.getApp().getBleDevices().add(device);
//							updateNewFindDevice();						
							
//							LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
//									+ LedBleApplication.getApp().getBleDevices().size());								
//							conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
							
						}
					}
		        }
		    };
        }
	}

	@SuppressLint("NewApi")
	private void initView() {

		mActivity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		
//		mActivity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		// 是否支持蓝牙
		if (!mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(mActivity, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
			Tool.exitApp();
		}
		
		bluetoothManager = (BluetoothManager) mActivity.getSystemService(Context.BLUETOOTH_SERVICE);
		mBluetoothAdapter = bluetoothManager.getAdapter();
		if (Build.VERSION.SDK_INT >= 23) {//安卓6.0以上的方案
			bleScanner = mBluetoothAdapter.getBluetoothLeScanner();//用过单例的方式获取实例
        }
		// 不能获得蓝牙设备支持
		if (mBluetoothAdapter == null) {
			Tool.ToastShow(mActivity, R.string.ble_not_supported);
			Tool.exitApp();
		}
		

		hashMapLock = new HashMap<String, Boolean>();
		hashMapConnect = new HashMap<String, Boolean>();
		mBluetoothGatts = new ArrayList<BluetoothGatt>();
		

		
		// 初始tab项
		sp = getSharedPreferences(Constant.MODLE_TYPE, Context.MODE_PRIVATE);
		editor = sp.edit();
		currentIndex = sp.getInt(Constant.MODLE_VALUE, 2);

		// ====连接后台service
		myServiceConenction = new MyServiceConenction();
		myServiceConenction.setServiceConnectListener(new ServiceConnectListener() {
			@Override
			public void onConnect(ComponentName name, IBinder service, BluetoothLeServiceSingle bLeService) {
				mBluetoothLeService = bLeService;// 获取连接实例
				
				try {
					Method localMethod = mBluetoothLeService.getBluetoothGatt().getClass().getMethod("refresh");
					if (localMethod != null) {
						localMethod.invoke(mBluetoothLeService.getBluetoothGatt());
					}
				} catch (Exception localException) {
					Log.e("refreshServices()", "An exception occured while refreshing device");
				}
				
				// leftMenuFragment.setService(mBluetoothLeService);
				if (!mBluetoothLeService.initialize()) {
					Log.e(LedBleApplication.tag, "Unable to initialize Bluetooth");
				} else {
					Log.e(LedBleApplication.tag, "Initialize Bluetooth");
				}
			}

			@Override
			public void onDisConnect(ComponentName name) {

			}
		});

		Intent gattServiceIntent = new Intent(this, BluetoothLeServiceSingle.class);
		bindService(gattServiceIntent, myServiceConenction, Activity.BIND_AUTO_CREATE);


		rgBottom.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				switch (checkedId) {
				case R.id.rbThrid:
					currentIndex = 2;
					segmentDm.setVisibility(View.GONE);
					segmentCt.setVisibility(View.GONE);
					segmentRgb.setVisibility(View.GONE);
					segmentMusic.setVisibility(View.GONE);
					rbFirst.setVisibility(View.GONE);
					rbSecond.setVisibility(View.GONE);
					rbThrid.setVisibility(View.VISIBLE);
					editor.putInt(Constant.MODLE_VALUE, currentIndex);
					editor.commit();

					break;

				}
				ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
			}
		});
		
		rgBottom.check(R.id.rbThrid);

//		initBleScanList(isAllOn);
		if (mBluetoothAdapter.isEnabled()) {
			initBleScanList(isAllOn);
		}
	}


	/**
	 * 初始化fragment
	 */
	public void initFragment() {
		fragmentList.add(new DmFragment());
		fragmentList.add(new CtFragment());
		rgbFragment = new RgbFragment();
		fragmentList.add(rgbFragment);

		// 添加Fragment
		fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		for (int i = 0; i < fragmentList.size(); i++) {
			transaction.add(R.id.flContent, fragmentList.get(i), fragmentList.get(i).getClass().getSimpleName());
		}
		transaction.commit();
		ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	}


	public class MyOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {

			case R.id.ivType:

				break;
			}
		}
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean open() {
		NetConnectBle.getInstanceByGroup(groupName).turnOn();
		return false;
	}

	public boolean close() {
		NetConnectBle.getInstanceByGroup(groupName).turnOff();
		return false;
	}

	// =============================================行为事件=====================================================//

	
	/**
	 * 扫描设备
	 */
	public void initBleScanList(boolean isAllon) {
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}

		
		scanLeDevice(true);
	}
	
	/**
	 * enable 暂时无用途
	 * 
	 * @param enable
	 */
	private void scanLeDevice(final boolean enable) {
//		Tool.ToastShow(mActivity, R.string.refresh);
		if (enable) {	
			
			final Handler handler = new Handler(); // 开启 定时搜索 定时器
			Runnable runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
//					handler.postDelayed(this, 8000);// 8秒 是延时时长
					refreshDevices();
				}
			};
			handler.postDelayed(runnable, 10);// 打开定时器，执行操作
		} 
	}
	

	/**
	 * 刷新
	 */
	protected void refreshDevices() {
		
		LedBleApplication.getApp().removeALLDevice();
		
		startLeScan(); //开始扫描
		
		final Handler handler = new Handler(); // 开启 定时搜索 定时器
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				stopLeScan();
			}
		};
		handler.postDelayed(runnable, 6000);// 打开定时器，执行操作

	}
	
	/**
	 * 开始扫描
	 */
	protected void startLeScan() {
		
//		Toast.makeText(mActivity, "开始扫描", Toast.LENGTH_SHORT).show();
		
//		LogUtil.i(LedBleApplication.tag, "============================== 开始扫描：=============================" );
//		mBluetoothAdapter.startLeScan(mLeScanCallback);
//		mBluetoothAdapter.startDiscovery();
		
		if (Build.VERSION.SDK_INT < 23) {//安卓6.0以下的方案
			mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {//安卓7.0及以上的方案
            
            if (mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // 申请定位授权
            	mActivity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_CODE);
            } else {
                bleScanner.startScan(scanCallback);
            }
        }
		
	}
	
	/**
	 * 停止扫描
	 */
	protected void stopLeScan() {
		
//		LogUtil.i(LedBleApplication.tag, "============================== 停止扫描：=============================" );
//		mBluetoothAdapter.stopLeScan(mLeScanCallback);
//		mBluetoothAdapter.cancelDiscovery();
		
		if (Build.VERSION.SDK_INT < 23) {
            //安卓6.0及以下版本BLE操作的代码
			mBluetoothAdapter.stopLeScan(mLeScanCallback);
         } else {
        	//安卓7.0及以上版本BLE操作的代码
             bleScanner.stopScan(scanCallback);
         }
		
	}
	
	
	public void showCustomMessage() {
		
		if (lDialog == null) {
			lDialog = new Dialog(mActivity, android.R.style.Theme_Translucent_NoTitleBar);
			lDialog.requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			lDialog.setContentView(R.layout.dialogview);	
			
			ImageView iv_route = (ImageView) lDialog.findViewById(R.id.imageViewWait);  
			RotateAnimation mAnim = new RotateAnimation(0, 360, Animation.RESTART, 0.5f, Animation.RESTART, 0.5f);  
		    mAnim.setDuration(2000);  
		    mAnim.setRepeatCount(Animation.INFINITE);// 设置重复次数，这里是无限  
		    mAnim.setRepeatMode(Animation.RESTART);// 设置重复模式  
		    mAnim.setStartTime(Animation.START_ON_FIRST_FRAME);  
		    // 匀速转动的代码  
		    LinearInterpolator lin = new LinearInterpolator();  
		    mAnim.setInterpolator(lin);  
		    iv_route.startAnimation(mAnim);  

			lDialog.show();
		}	
		
	}
	
	public void lDialogDismiss() {
		
		if (lDialog != null) {
			lDialog.dismiss();
			lDialog = null;
		}	
	}
	
	
	

	// =============================================蓝牙操作=====================================================//

	/**
	 * 创建广播过滤器
	 * 
	 * @return
	 */
	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_CONNECTED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED);
		intentFilter.addAction(BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE);
		intentFilter.addAction("action.connectble.GroupsActivity");
		intentFilter.addAction("action.refreshble.GroupsActivity");
		intentFilter.addAction("action.disconnectble.GroupsActivity");
		return intentFilter;
	}

	

	/**
	 * 成功扫描设备
	 */
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		@Override
		public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (device != null) {
						if (!LedBleApplication.getApp().getBleDevices().contains(device) && device.getName() != null) {
							String name = device.getName();

							if (/*name.startsWith(BluetoothLeServiceSingle.NAME_START_ELK)
									||*/ name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)
									/*|| name.startsWith(BluetoothLeServiceSingle.NAME_START_TV)
									|| name.startsWith(BluetoothLeServiceSingle.NAME_START_HEI)*/) {
								
								LedBleApplication.getApp().getBleDevices().add(device);

//								LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
//										+ LedBleApplication.getApp().getBleDevices().size());
//								conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
							}

						}
					}
				}
			});
		}
	};



	private boolean booleanCanStart = false;
	private Handler conectHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case MSG_START_CONNECT:// 可以开始连接了
				if (booleanCanStart == false) {
					booleanCanStart = true;
//					startConnectDevices();
				}
				break;

			default:
				break;
			}
		};
	};

	/**
	 * 开始连接设备，设备的连接是异步的，必须等到一个设备连接成功后(发现service)才能连接新的设备
	 */
	private void startConnectDevices(final String bleAddress) {

		System.out.println("mBluetoothLeService:" + mBluetoothLeService);

		if (null != mBluetoothLeService) {
			ArrayList<BluetoothDevice> bldevices = LedBleApplication.getApp().getBleDevices();
			try {
				for (BluetoothDevice bluetoothDevice : bldevices) {
					final String address = bluetoothDevice.getAddress();
					final String name = bluetoothDevice.getName();

					if (bleAddress.equalsIgnoreCase(address)
							&& !LedBleApplication.getApp().getBleGattMap().containsKey(address)
							&& null != mBluetoothLeService) {

						mBluetoothLeService.connect(address, name);
						hashMapLock.put(address, false);// 上锁

					}

				}

			} catch (Exception e) {
				e.printStackTrace();// 防止出现并发修改异常
			}

		}
	}
	
	/**
	 * 开始连接设备，设备的连接是异步的，必须等到一个设备连接成功后(发现service)才能连接新的设备
	 */
	private void disConnectDevices(String bleAddress) {
		
		for (BluetoothGatt bluetoothGatt : mBluetoothGatts) {
			bluetoothGatt.disconnect();
			bluetoothGatt.close();
			bluetoothGatt = null;
		}
		
	}

	/**
	 * 广播监听回调
	 */
	private BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			System.out.println("接收廣播action-->" + action);
			if (BluetoothLeServiceSingle.ACTION_GATT_CONNECTED.equals(action)) {// 连接到设备
				String address = intent.getStringExtra("address");
				hashMapConnect.put(address, true);
//				updateDevieConnect();
				LogUtil.i(LedBleApplication.tag, "connect:" + address);

				for (BluetoothDevice bluetoothDevice : LedBleApplication.getApp().getBleDevices()) {
					final String addr = bluetoothDevice.getAddress();		
					if (address.equalsIgnoreCase(addr)) {
						LedBleApplication.getApp().getConnectedBleDevices().add(bluetoothDevice);
					}
				}
				
				
				String groupData[] = SharePersistent.getSingleGroupData(mActivity, "device");
				
				if (address.equalsIgnoreCase(groupData[3])) {
					NetConnectBle.getInstanceByDeviceAddress(address);
				}				

				TextView textViewGroupName = rgbFragment.getTextViewGroupName();
				textViewGroupName.setText(SharePersistent.getPerference(mActivity, "groupName"));
				
//				Toast.makeText(mActivity, "设备已连接", Toast.LENGTH_SHORT).show();		
				

			} else if (BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED.equals(action)) {// 设断开备

//				isFirstOnApp = false;

				String address = intent.getStringExtra("address");
			
				hashMapConnect.put(address,false);//删除连接计数

				hashMapConnect.remove(address);
				
				BluetoothGatt blgat = mBluetoothLeService.getBluetoothGatt();
				blgat.disconnect();
				blgat.close();
				LedBleApplication.getApp().removeDisconnectDevice(address);// 删除断开的device			
				
				LedBleApplication.getApp().getBleGattMap().remove(address);// 删除断开的blgatt
				
				
				if (LedBleApplication.getApp().getBleDevices().size() > 0) {
					for (BluetoothDevice dev : LedBleApplication.getApp().getBleDevices()) {
						if (dev.getAddress().equalsIgnoreCase(address)) {
							LedBleApplication.getApp().getBleDevices().remove(dev);// 删除断开的blgatt
						}
					}
				}
				
				hashMapLock.remove(address);// 删除锁
//				updateDevieConnect();
				LogUtil.i(LedBleApplication.tag, "disconnect:" + address + " connected devices:"
						+ LedBleApplication.getApp().getBleDevices().size());
				mBluetoothLeService.close();
				
//				conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
				
				// refreshDevices(); //扫描设备
//				if (!isFirstOnApp) {
//					mBluetoothAdapter.startLeScan(mLeScanCallback);
//					Toast.makeText(mActivity, "设备已断开", Toast.LENGTH_SHORT).show();					
//				}

			} else if (BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {// 发现service,就可以获取Characteristic
				// 发现之后依次连接
				String address = intent.getStringExtra("address");
				BluetoothGatt blgat = mBluetoothLeService.getBluetoothGatt();
				mBluetoothGatts.add(blgat);
				// 控制连接要求的设备
				// BluetoothGattService service32 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid32));
				// BluetoothGattService service33 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid33));
				// BluetoothGattService service34 =
				// blgat.getService(UUID.fromString(NetConnectBle.serviceid34));
				// if (service32 != null || service33 != null || service34 !=
				// null) {
				LedBleApplication.getApp().getBleGattMap().put(address, blgat);
//				Toast.makeText(mActivity, "连接上设备 = "+address, Toast.LENGTH_SHORT).show();	
				hashMapLock.put(address, true);// 解锁
				LogUtil.i(LedBleApplication.tag, "发现service" + intent.getStringExtra("address"));
				// }
			} else if (BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE.equals(action)) {// 读取到数据，不做处理，本应用不需要读取数据
				
				// displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));
				
			} else if ("action.connectble.GroupsActivity".equalsIgnoreCase(action)) { // 接收  GroupsActivity 传递过来的连接通知
							
				final String bleAddress = intent.getStringExtra("bleAddress");
				

				
//				ArrayList<BluetoothDevice> bldevices = LedBleApplication.getApp().getBleDevices();
//				for (BluetoothDevice bluetoothDevice : bldevices) {
//					final String address = bluetoothDevice.getAddress();
//
//					if (LedBleApplication.getApp().getBleGattMap().containsKey(address) && blueAddress.length() > 0
//							&& address != blueAddress) {
//
////						Toast.makeText(mActivity, "disConnectDevices", Toast.LENGTH_SHORT).show();
//						disConnectDevices(blueAddress);						
//
//					}
//				}
				
				disConnectDevices(blueAddress);	
			
				
				final Handler handler = new Handler(); // 开启 定时搜索 定时器
				Runnable runnable = new Runnable() {
					@Override
					public void run() {
						// TODO Auto-generated method stub
						// 在此处添加执行的代码
						blueAddress = bleAddress;
						startConnectDevices(bleAddress);
						
					}
				};
				handler.postDelayed(runnable, 2000);// 打开定时器，执行操作
				
			} else if ("action.refreshble.GroupsActivity".equalsIgnoreCase(action)) { // 刷新蓝牙

				refreshDevices();
//				Toast.makeText(mActivity, R.string.refresh, Toast.LENGTH_SHORT).show();	
			}
			else if ("action.disconnectble.GroupsActivity".equalsIgnoreCase(action)) { // 断开蓝牙
				
				final String bleAddress = intent.getStringExtra("bleAddress");
				
//				if (blueAddress.length() > 0) {
//					if (!blueAddress.equalsIgnoreCase(bleAddress)) {
						disConnectDevices(bleAddress);
						
//						startLeScan();
//					}
//				}	
//				Toast.makeText(mActivity, "断开设备 = "+bleAddress, Toast.LENGTH_SHORT).show();
			}
			
		}
	};

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		
		 if (resultCode == Activity.RESULT_OK && requestCode == OPEN_BLE) {

			if (Build.VERSION.SDK_INT >= 23) {// 安卓6.0以上的方案
				if (mBluetoothAdapter != null) {
					bleScanner = mBluetoothAdapter.getBluetoothLeScanner();// 用过单例的方式获取实例
				}
				initScanCallback(); // 初始化 ScanCallback
			}else {
				mBluetoothAdapter = bluetoothManager.getAdapter();
			}			
			
			if (mBluetoothAdapter.isEnabled()) {
				refreshDevices();
			}

		} 

	}



	// ============业务模式===================
	

	public void turnOnOffTimerOn(int onOrOff) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).turnOnOffTimerOn(onOrOff);
	}

	public void timerOn(int hour, int minute, int model) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).timerOn(hour, minute, model);
	}

	public void turnOnOffTimerOff(int onOrOff) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).turnOnOrOffTimerOff(onOrOff);
	}

	public void timerOff(int hour, int minute) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).timerOff(hour, minute);
	}

	public void setRgb(int r, int g, int b) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setRgb(r, g, b);
	}

	public void setDim(int dim) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setDim(dim);
	}

	public void setDimModel(int dimModel) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setDimModel(dimModel);
	}

	public void setRegMode(int model) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setRgbMode(model);
	}

	public void setSpeed(int speed) {
		// if (isLightOpen == false) {
		// return;
		// }

//		SharePersistent.savePerference(this, speedKey, speed);
		NetConnectBle.getInstanceByGroup(groupName).setSpeed(speed);
	}

	public void setDiy(ArrayList<MyColor> colors, int style) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setDiy(colors, style);
	}

	public void setBrightNess(int brightness) {
		// if (isLightOpen == false) {
		// return;
		// }
//		this.brightness = brightness;// 设置亮度参数
//		SharePersistent.savePerference(this, brightnessKey, brightness);
		NetConnectBle.getInstanceByGroup(groupName).setBrightness(brightness);

	}
	
	public void setMetalLightBrightNess(int brightness, int mode) {
		// if (isLightOpen == false) {
		// return;
		// }

//		SharePersistent.savePerference(this, brightnessKey, brightness);
//		NetConnectBle.getInstanceByGroup(groupName).setMetalLightBrightNess(brightness, mode);

	}
	
	public void setModeBrightNess(int brightness) {
		// if (isLightOpen == false) {
		// return;
		// }

//		SharePersistent.savePerference(this, brightnessKey, brightness);
		NetConnectBle.getInstanceByGroup(groupName).setModeBrightNess(brightness);

	}

	public void setCT(int warm, int cool) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setColorWarm(warm, cool);
	}

	public void setCTModel(int model) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setColorWarmModel(model);
	}
	
	// 律动 模式
	public void setDynamicModel(int model) {
		// if (isLightOpen == false) {
		// return;
		// }
		NetConnectBle.getInstanceByGroup(groupName).setDynamicModel(model);
	}
	
	public void setTimeDataWithCo2(ParameterBean parameter) {
		
		if (parameter.getHour() == -1) {
			parameter.setHour(0);
		}
		if (parameter.getMinute() == -1) {
			parameter.setMinute(0);
		}
		if (parameter.getHour1() == -1) {
			parameter.setHour1(0);
		}
		if (parameter.getMinute1() == -1) {
			parameter.setMinute1(0);
		}
		
//		if (parameter.getHour() == 24) {
//			parameter.setHour(24);
//		}
//		
//		if (parameter.getHour1() == 24) {
//			parameter.setHour1(24);
//		}
		
		if (parameter.getR() == -1) {
			parameter.setR(0);
		}
		if (parameter.getG() == -1) {
			parameter.setG(0);
		}
		if (parameter.getB() == -1) {
			parameter.setB(0);
		}
		if (parameter.getW() == -1) {
			parameter.setW(0);
		}
		if (parameter.getCo2() == -1) {
			parameter.setCo2(0);
		}
		
		if (parameter.getR1() == -1) {
			parameter.setR1(0);
		}
		if (parameter.getG1() == -1) {
			parameter.setG1(0);
		}
		if (parameter.getB1() == -1) {
			parameter.setB1(0);
		}
		if (parameter.getW1() == -1) {
			parameter.setW1(0);
		}
		if (parameter.getCo21() == -1) {
			parameter.setCo21(0);
		}
		
		NetConnectBle.getInstanceByGroup(groupName).setTimeDataWithCo2(parameter);

	}
	
	public void setTimeDataWithCo2End(int flag, int year, int mon, int day, int hour, int minute, int second, int totalHour) {
		
		NetConnectBle.getInstanceByGroup(groupName).setTimeDataWithCo2End(flag, year, mon, day, hour, minute, second, totalHour);

	}
	
	public void setColorCommandWithCo2(int hour, int minute, int R, int G, int B, int W, int co2) {
		
		if (R == -1) {
			R = 0;
		}
		if (G == -1) {
			G = 0;
		}
		if (B == -1) {
			B = 0;
		}
		if (W == -1) {
			W = 0;
		}
		if (co2 == -1) {
			co2 = 0;
		}
		
		NetConnectBle.getInstanceByGroup(groupName).setColorCommandWithCo2(hour, minute, R, G, B, W, co2);

	}
	
	public void setColorCommandWithOutCo2(int R, int G, int B, int W) {
	
		NetConnectBle.getInstanceByGroup(groupName).setColorCommandWithOutCo2(R, G, B, W);

	}
	
	public void setExitToNormalMode(int R, int G, int B, int W) {
		
		if (R == -1) {
			R = 0;
		}
		if (G == -1) {
			G = 0;
		}
		if (B == -1) {
			B = 0;
		}
		if (W == -1) {
			W = 0;
		}
		
		NetConnectBle.getInstanceByGroup(groupName).setExitToNormalMode(R, G, B, W);

	}
	
	public void setReviewMode(int R, int G, int B, int W, int Co2) {

		NetConnectBle.getInstanceByGroup(groupName).setReviewMode(R, G, B, W, Co2);

	}
	
	public void sendTimeCheck(int year, int month, int day, int hour, int minute, int second) {

		NetConnectBle.getInstanceByGroup(groupName).sendTimeCheck(year, month, day, hour, minute, second);

	}

	@Override
	public void onException(Exception e) {

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		unbindService(myServiceConenction);
		unregisterReceiver(mGattUpdateReceiver);
//		mActivity.unregisterReceiver(mGattUpdateReceiver);
		hashMapConnect = null;
		hashMapLock = null;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		if (isBackground(mActivity)) { //app进入后台停止扫描
//			mBluetoothAdapter.stopLeScan(mLeScanCallback);
//			Toast.makeText(mActivity, "当前为后台", Toast.LENGTH_SHORT).show();
		}else {
			
//			Toast.makeText(mActivity, "当前为前台", Toast.LENGTH_SHORT).show();
		}
		
	}
	
	@Override
	protected void onResume() { 
		super.onResume();
	}

	/**
	 * 判断app是否进入后台
	 */
	public static boolean isBackground(Context context) {

		ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
		for (RunningAppProcessInfo appProcess : appProcesses) {
			if (appProcess.processName.equals(context.getPackageName())) {
				if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {
					
					return true;
				} else {			
					return false;
				}
			}
		}
		return false;
	}
	

	public SegmentedRadioGroup getSegmentDm() {
		return segmentDm;
	}

	public SegmentedRadioGroup getSegmentCt() {
		return segmentCt;
	}

	public SegmentedRadioGroup getSegmentRgb() {
		return segmentRgb;
	}

	public SegmentedRadioGroup getSegmentMusic() {
		return segmentMusic;
	}


}
