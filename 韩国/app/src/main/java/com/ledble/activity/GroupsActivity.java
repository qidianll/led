package com.ledble.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.uitl.SharePersistent;
import com.aquariumstudio.R;
import com.ledble.adapter.BleSelectDeviceAdapter;
import com.ledble.base.LedBleApplication;
import com.ledble.constant.Constant;
import com.ledble.db.GroupDevice;
import com.ledble.net.NetConnectBle;

public class GroupsActivity extends BaseActivity {

	private Handler handler;
	private Runnable runnable;
	
	private RelativeLayout topLogol;
	
	private Dialog searchDialog;
	private Dialog connectDialog;
	
	private TextView textViewSelectedGroupName;
	private ListView listViewDevices;
	private BleSelectDeviceAdapter bleDeviceAdapter;
	private String groupName = "";
//	private String bleAddress = "";
	private int indexForGroupRow = -1;
//	private ArrayList<GroupDevice> groupDevices;
	private TextView textViewOKButton;
	private View rLayoutBlePairing;
	
	private final int MSG_REFRESH_UI = 10000; // 刷新UI	
	private final int MSG_SRTART_ACTIVITY = 10001; // start新界面
	
	private boolean onOffStatus = false;
	private final int INT_DEVICE_BACK = 300;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();

		String groupData[] = SharePersistent.getSingleGroupData(getApplicationContext(), "device");
		ArrayList<BluetoothDevice> bleDevices = LedBleApplication.getApp().getConnectedBleDevices();
		if (bleDevices.size() > 0) {
			textViewSelectedGroupName.setText(groupData[1]);
		}else {
			textViewSelectedGroupName.setText("");
		}
		

		searchDevice();
		
		scanLeDevice(5000);
	}
	
	public void searchDevice() {
		
		showSearchDialog();
		

		Handler handler = new Handler(); // 开启 定时搜索 定时器
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				dismissSearchDialog();
			}
		};
		handler.postDelayed(runnable, 5000);// 打开定时器，执行操作
	}

	@Override
	public void initView() {
		
		setContentView(R.layout.activity_groups);
		groupName = getIntent().getStringExtra("group");
		groupName = SharePersistent.getPerference(getApplicationContext(), "groupName");
		
		indexForGroupRow = getIntent().getIntExtra("indexForGroupRow", -1);
//		groupDevices = (ArrayList<GroupDevice>) getIntent().getSerializableExtra("devices");

		topLogol = findRelativeLayout(R.id.topLogol);
		topLogol.setOnClickListener(new OnClickListener() { // 
			@Override
			public void onClick(View v) {
//				startActivityForResult(new Intent(MainActivity.getMainActivity(), DeviceActivity.class), INT_DEVICE_BACK);
//				startActivity(new Intent(getBaseContext(), DeviceActivity.class));
			}
		});
		
		// 刷新蓝牙
		rLayoutBlePairing = findViewById(R.id.blePairing);
		rLayoutBlePairing.setOnClickListener(new OnClickListener() { // 
			@Override
			public void onClick(View v) {
				
				searchDevice();				
				postRefreshAction();
				
//				startActivity(new Intent(getBaseContext(), DeviceActivity.class));
//				if (onOffStatus) {
//					MainActivity.getMainActivity().open();
//					onOffStatus = false;
//				}else {
//					MainActivity.getMainActivity().close();
//					onOffStatus = true;
//				}
			}
		});	
//		rLayoutBlePairing.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				// TODO Auto-generated method stub
//				postRefreshAction();
////				if (bleAddress.length() > 0) {
////					postDisConnectAction(SharePersistent.getPerference(getApplicationContext(), "bleAddress"));
////				}			
//			}
//		});
		
		textViewSelectedGroupName = findTextViewById(R.id.textViewSelectedGroupName);
		listViewDevices = findListViewById(R.id.device_list);
		bleDeviceAdapter = new BleSelectDeviceAdapter(this);
//		bleDeviceAdapter.setSelected(groupDevices);
		listViewDevices.setAdapter(bleDeviceAdapter);
		
		listViewDevices.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				BluetoothDevice ble = bleDeviceAdapter.getDevice(position);
				bleDeviceAdapter.select(ble);
				bleDeviceAdapter.notifyDataSetChanged();
				
				final String groupData[] = SharePersistent.getGroupData(getApplicationContext(), ble.getAddress());
				textViewSelectedGroupName.setText(groupData[3]);				
				groupName = groupData[3];

				
//				scanLeDevice(6000); //每隔6s执行 
				
				showConnectDialog();
//				MainActivity.getMainActivity().showCustomMessage();
				
//				final String bleaddr = SharePersistent.getPerference(getApplicationContext(), "bleAddress");
//				if (bleaddr.length() > 0 && !bleaddr.equalsIgnoreCase(ble.getAddress()) ) {
//					if (LedBleApplication.getApp().getConnectedBleDevices().contains(ble)) {
//						LedBleApplication.getApp().getConnectedBleDevices().remove(ble);
//					}	
//				}
				
				postConnectAction(ble.getAddress());
				
				NetConnectBle.getInstanceByDeviceAddress(ble.getAddress());

				SharePersistent.saveSingleGroupData(getApplicationContext(), "device", 
						Integer.toString(indexForGroupRow), groupData[3], ble.getName(), ble.getAddress());
				SharePersistent.savePerference(getApplicationContext(), Constant.GROUP_NAME, groupData[3]);
				SharePersistent.savePerference(getApplicationContext(), Constant.DEVICE_NAME, groupData[2]);
				SharePersistent.savePerference(getApplicationContext(), "bleAddress", ble.getAddress());
				
				startNewActivity(5000);
//				startActivity(new Intent(getBaseContext(), DeviceActivity.class));
			}
		});
		
		listViewDevices.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
				// TODO Auto-generated method stub
				final BluetoothDevice ble = bleDeviceAdapter.getDevice(position);
				bleDeviceAdapter.select(ble);			
				
				final String groupData[] = SharePersistent.getGroupData(getApplicationContext(), ble.getAddress());
				
				final EditText inputServer = new EditText(GroupsActivity.this); 
				inputServer.setText(groupData[3]);
	            AlertDialog.Builder builder = new AlertDialog.Builder(GroupsActivity.this);  
	            builder.setTitle(R.string.please_input).setIcon(android.R.drawable.ic_dialog_info).setView(inputServer)  
	                    .setNegativeButton("Cancel", null);  
	            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {  
	  
	                public void onClick(DialogInterface dialog, int which) {  
	                	
	                	String groupName = inputServer.getText().toString();
	                	
//	            		if (groupData[3] != null && groupData[3].length() > 0) {
	            			SharePersistent.saveGroupData(getApplicationContext(), ble.getAddress(), groupData[0], groupData[1], groupData[2], groupName, groupData[4]);
	            			bleDeviceAdapter.notifyDataSetChanged();
//	            		}
	                	
	                 }  
	            });  
	            builder.show();
				
				return true;
			}

		});
		
		
		textViewOKButton = (TextView) findViewById(R.id.textViewOKButton);
		textViewOKButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent();
				intent.putExtra("groupName", groupName);
				setResult(Activity.RESULT_OK, intent);
				finish();
			}
		});
		
	}
	
	private void postRefreshAction() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		   intent.setAction("action.refreshble.GroupsActivity");//设置Action
		   intent.setPackage(getPackageName());//设置包名使广播只能被app内接收者接收
//		   intent.putExtra("bleAddress", bleAddress);
		   sendBroadcast(intent);
	}
	
	private void postConnectAction(String bleAddress) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		   intent.setAction("action.connectble.GroupsActivity");//设置Action
		   intent.setPackage(getPackageName());//设置包名使广播只能被app内接收者接收
		   intent.putExtra("bleAddress", bleAddress);
		   sendBroadcast(intent);
	}
	
	private void postDisConnectAction() {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		   intent.setAction("action.disconnectble.GroupsActivity");//设置Action
		   intent.setPackage(getPackageName());//设置包名使广播只能被app内接收者接收
		   intent.putExtra("bleAddress", "");
		   sendBroadcast(intent);
	}
	
	private void scanLeDevice(final long time) {	
			
		if (handler == null) {
			handler = new Handler(); // 开启 定时搜索 定时器
		}
		if (runnable == null) {
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					handler.postDelayed(this, time);// 5秒 是延时时长
//					refreshDevices();
					conectHandler.sendEmptyMessage(MSG_REFRESH_UI);// 刷新UI	
				}
			};
			handler.postDelayed(runnable, time);// 打开定时器，执行操作
		}				
	}
	
	private void startNewActivity(final long time) {	
		
		final Handler handler = new Handler(); // 开启 定时搜索 定时器	
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				// handler.postDelayed(this, time);// 5秒 是延时时长
				// refreshDevices();
				conectHandler.sendEmptyMessage(MSG_SRTART_ACTIVITY);// 刷新UI
				handler.removeCallbacks(this);
			}
		};
		handler.postDelayed(runnable, time);// 打开定时器，执行操作
				
	}
	
	private Handler conectHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MSG_REFRESH_UI:// 刷新UI			

				refreshDevices();
				break;			
				
			case MSG_SRTART_ACTIVITY:// start Activity		

				dismissConnectDialog();
				startActivity(new Intent(getBaseContext(), DeviceActivity.class));
				break;			

			default:
				break;
			}
		};
	};
	
	/**
	 * 刷新
	 */
	protected void refreshDevices() {
		
		String groupData[] = SharePersistent.getSingleGroupData(getApplicationContext(), "device");
		ArrayList<BluetoothDevice> bleDevices = LedBleApplication.getApp().getConnectedBleDevices();
		
		if (bleDevices.size() > 0) {
			textViewSelectedGroupName.setText(groupData[1]);
		}else {
			textViewSelectedGroupName.setText("");
		}
		
		bleDeviceAdapter.notifyDataSetChanged();

	}
	
	public void showConnectDialog() {
		
		if (connectDialog == null) {
			connectDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
			connectDialog.requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			connectDialog.setContentView(R.layout.dialogview);	
			
			ImageView iv_route = (ImageView) connectDialog.findViewById(R.id.imageViewWait);  
			RotateAnimation mAnim = new RotateAnimation(0, 360, Animation.RESTART, 0.5f, Animation.RESTART, 0.5f);  
		    mAnim.setDuration(2000);  
		    mAnim.setRepeatCount(Animation.INFINITE);// 设置重复次数，这里是无限  
		    mAnim.setRepeatMode(Animation.RESTART);// 设置重复模式  
		    mAnim.setStartTime(Animation.START_ON_FIRST_FRAME);  
		    // 匀速转动的代码  
		    LinearInterpolator lin = new LinearInterpolator();  
		    mAnim.setInterpolator(lin);  
		    iv_route.startAnimation(mAnim);  

		    connectDialog.show();
		}	
		
	}
	
	public void dismissConnectDialog() {
		
		if (connectDialog != null) {
			connectDialog.dismiss();
			connectDialog = null;
		}	
	}
	
	public void showSearchDialog() {
		
		if (searchDialog == null) {
			searchDialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
			searchDialog.requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
			searchDialog.setContentView(R.layout.dialogview);	
			
			TextView textView = (TextView) searchDialog.findViewById(R.id.textViewWait);
			textView.setText(getString(R.string.msg_search));
			ImageView iv_route = (ImageView) searchDialog.findViewById(R.id.imageViewWait);  
			RotateAnimation mAnim = new RotateAnimation(0, 360, Animation.RESTART, 0.5f, Animation.RESTART, 0.5f);  
		    mAnim.setDuration(2000);  
		    mAnim.setRepeatCount(Animation.INFINITE);// 设置重复次数，这里是无限  
		    mAnim.setRepeatMode(Animation.RESTART);// 设置重复模式  
		    mAnim.setStartTime(Animation.START_ON_FIRST_FRAME);  
		    // 匀速转动的代码  
		    LinearInterpolator lin = new LinearInterpolator();  
		    mAnim.setInterpolator(lin);  
		    iv_route.startAnimation(mAnim);  

		    searchDialog.show();
		}	
		
	}
	
	public void dismissSearchDialog() {
		
		if (searchDialog != null) {
			searchDialog.dismiss();
			searchDialog = null;
		}	
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		searchDevice();
		
		postDisConnectAction();
		
		
		Handler handler = new Handler(); // 开启 定时搜索 定时器
		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				postRefreshAction();
			}
		};
		handler.postDelayed(runnable, 1000);// 打开定时器，执行操作
		
		refreshDevices();
		
		bleDeviceAdapter.notifyDataSetChanged();
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if (handler != null) {
			handler.removeCallbacks(runnable);
			handler = null;
			runnable = null;
		}
	}

}


