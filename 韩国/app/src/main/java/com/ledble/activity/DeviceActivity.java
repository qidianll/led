package com.ledble.activity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.common.BaseActivity;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.SharePersistent;
import com.aquariumstudio.R;
import com.ledble.adapter.BleSelectDeviceAdapter;
import com.ledble.adapter.TimeDataAdapter;
import com.ledble.adapter.TimeDataAdapter.OnSelectListener;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.BloodPressureBean;
import com.ledble.bean.ParameterBean;
import com.ledble.bean.TimeRGBW;
import com.ledble.constant.Constant;
import com.ledble.db.GroupDevice;
import com.ledble.net.NetConnectBle;
import com.ledble.view.ChartView;
import com.ledble.view.CustomPopWindow;
//import com.ledble.view.CustomRadioGroup;
import com.ledble.view.qpopuwindow.QPopuWindow;
import com.lwz.chart.hellocharts.listener.LineChartOnValueSelectListener;
import com.lwz.chart.hellocharts.model.Axis;
import com.lwz.chart.hellocharts.model.AxisValue;
import com.lwz.chart.hellocharts.model.Line;
import com.lwz.chart.hellocharts.model.LineChartData;
import com.lwz.chart.hellocharts.model.PointValue;
import com.lwz.chart.hellocharts.model.ValueShape;
import com.lwz.chart.hellocharts.model.Viewport;
import com.lwz.chart.hellocharts.util.ChartUtils;
import com.lwz.chart.hellocharts.util.TimeUtil;
import com.lwz.chart.hellocharts.view.LineChartView;

@SuppressLint("ResourceAsColor")
public class DeviceActivity extends BaseActivity  {
	
	private String VerString = "Ver1.0.1";
	private String infoString = "http://aquas.kr";

	private LinearLayout llCo2;
	
	private Handler handler;
	private Runnable runnable;
	private int count = 0;
//	private float k;
	private TextView textViewSelectedDeviceName;
	
	private ListView listViewDevices;
	private BleSelectDeviceAdapter bleDeviceAdapter;
	private String groupName = "";
	private ArrayList<GroupDevice> groupDevices;
	
	private TextView textViewMode;
	private SeekBar seekBarReview;
	
	private CustomPopWindow mCustomPopWindow;
	
	private final int INT_TIMER_SET = 122;
	
	private int positionTag;
	
	private int statusSUN = 1;
	private int statusMON = 1;
	private int statusTUE = 1;
	private int statusWED = 1;
	private int statusTHU = 1;
	private int statusFRI = 1;
	private int statusSAT = 1;
	private TextView textViewSUN;
	private TextView textViewMON;
	private TextView textViewTUE;
	private TextView textViewWED;
	private TextView textViewTHU;
	private TextView textViewFRI;
	private TextView textViewSAT;
	
	private String deviceName;
	private String MODE ;
	private String TAG_AQ ;
	private String TAG_TP ;
	private String TAG_UA ;
	private String TAG_UB ;

	
	private int rawX;
    private int rawY;
	
	private ListView listViewTimeData;
	private ArrayList<TimeRGBW> mp3s = new ArrayList<TimeRGBW>();
	private TimeDataAdapter timeDataAdapter;
	
	
	private List<Line> lines = new ArrayList<Line>(); // 存放线条对象的集合
	private View viewFrontLeft, viewFrontRight;
	

	
	private List<PointValue> mRedPointValues = new ArrayList<PointValue>();
	private List<PointValue> mGreenPointValues = new ArrayList<PointValue>();
	private List<PointValue> mBluePointValues = new ArrayList<PointValue>();
	private List<PointValue> mWhitePointValues = new ArrayList<PointValue>();


	private LineChartData redData = new LineChartData();	
	private LineChartData greenData = new LineChartData();  	
	private LineChartData blueData = new LineChartData(); 	
	private LineChartData whiteData = new LineChartData();
	
	private Line redLine = new Line(mRedPointValues).setColor(Color.parseColor("#e7262b"));  //折线的颜色 
	private Line greenLine = new Line(mGreenPointValues).setColor(Color.parseColor("#73fdaf"));  //折线的颜色
	private Line blueLine = new Line(mBluePointValues).setColor(Color.parseColor("#7bb9f6"));  //折线的颜
	private Line whiteLine = new Line(mWhitePointValues).setColor(Color.parseColor("#FFFFFF"));  //折线的颜色
	
	private Axis axisX = new Axis(); //X轴
	private Axis axisY = new Axis();  //Y轴 
	
	
	
	/***************************************************************************************************************/
	
	private String TAG;
	
	private int screenWidthPX;
	private int screenWidthDP;
	private int screenHeight;
	private int intervalValue;
	private int firValue;
	private boolean firSend = true;
	private boolean canSend = true;
	
	private LineChartView chart;           	//显示线条的自定义View
//    private LineChartData data;           // 折线图封装的数据类
//    private int numberOfLines = 4;        //线条的数量
//    private int maxNumberOfLines = 4;     //最大的线条数据
//    private int numberOfPoints = 12;      //点的数量

    //    private float[][] randomNumbersTab = new float[maxNumberOfLines][numberOfPoints];         //二维数组，线的数量和点的数量
    private List<BloodPressureBean> redlistBlood = new ArrayList<BloodPressureBean>();//数据
    private List<BloodPressureBean> greenlistBlood = new ArrayList<BloodPressureBean>();//数据
    private List<BloodPressureBean> bluelistBlood = new ArrayList<BloodPressureBean>();//数据
    private List<BloodPressureBean> whitelistBlood = new ArrayList<BloodPressureBean>();//数据

//    private boolean hasAxes = true;       //是否有轴，x和y轴
    private boolean hasAxesNames = true;   //是否有轴的名字
    private boolean hasLines = true;       //是否有线（点和点连接的线）
    private boolean hasPoints = true;       //是否有点（每个值的点）
    private ValueShape shape = ValueShape.CIRCLE;    //点显示的形式，圆形，正方向，菱形
    private boolean isFilled = false;                //是否是填充
    private boolean hasLabels = false;               //每个点是否有名字
    private boolean isCubic = false;                 //是否是立方的，线条是直线还是弧线
    private boolean hasLabelForSelected = false;       //每个点是否可以选择（点击效果）
    private boolean pointsHaveDifferentColor;           //线条的颜色变换
    private boolean hasGradientToTransparent = false;      //是否有梯度的透明

    private List<AxisValue> mAxisXValues = new ArrayList<AxisValue>();   //x轴方向的坐标数据
    private List<AxisValue> mAxisYValues = new ArrayList<AxisValue>();            //y轴方向的坐标数据

    private List<Float> distanceRedList = new ArrayList<Float>();
    private List<Float> distanceGreenList = new ArrayList<Float>();
    private List<Float> distanceBlueList = new ArrayList<Float>();
    private List<Float> distanceWhiteList = new ArrayList<Float>();
    private static final long timepre = 2 * 60 * 60 * 1000;//两个小时为一个时间单位,横坐标好像只能容纳12的长度，超过就不显示

    private ArrayList<TimeRGBW> tempArr = new ArrayList<TimeRGBW>();
    private ArrayList<TimeRGBW> lastDArr = new ArrayList<TimeRGBW>();
    private Time time = new Time(); // or Time t=new Time("GMT+8"); 

    

    
    
    
	// x轴坐标对应的数据
	private List<String> xValue = new ArrayList<>();
	// y轴坐标对应的数据
	private List<Integer> yValue = new ArrayList<>();
	// 折线对应的数据
	private Map<String, Integer> value = new HashMap<>();


    

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		
		time.setToNow(); // 取得系统时间
		deviceName =  SharePersistent.getPerference(DeviceActivity.this, Constant.DEVICE_NAME);
		MODE = "Mode";
		TAG_AQ = "Aquascaping";
		TAG_TP = "Tropical";
		TAG_UA = "User setting A";
		TAG_UB = "User setting B";
		
		
		TAG = SharePersistent.getPerference(DeviceActivity.this, MODE+deviceName).length() > 0 
				? SharePersistent.getPerference(DeviceActivity.this, MODE+deviceName) :TAG_UA;
		
		initView();		
		
		
		initData();
		
		
		generateRedLineValues(); // 画线
		
		sendTimeCheck(); //时间校对
		
		
		scanLeDevice(4000);
	}
	
	public void sendTimeCheck() { // 发送时间校验指令

		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR); // 
		int month = calendar.get(Calendar.MONTH); // 
		int day = calendar.get(Calendar.DAY_OF_MONTH); //
		int hour = calendar.get(Calendar.HOUR_OF_DAY); // 24小时制
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);

		MainActivity.getMainActivity().sendTimeCheck(year, month, day, hour, minute, second);
	}
	
	private void initData() {
        // Generate some random values.
        generateValues();   //设置四条线的值数据
//        generateData();    //设置数据
//        resetViewport();   //设置折线图的显示大小
    }

    /**
     * 添加数据,一天的时间和数值数据
     */
    private void generateValues() {

        //设置x轴坐标 ，显示的是时间1：00，2：00.。。。
//    	int xV[] = {0, 6, 12, 18, 23};
//    	int xV[] = {0, 6, 12, 18, 24};
        mAxisXValues.clear();
        int mClockNumberLength = 24;//显示的x轴的时间点的总数量
//        int startClock = TimeUtil.getTimeInt(listBlood.get(0).getTime(), "H");   //开始的时间的点数，这里使用到一个时间工具类
//        for (int i = 0; i < xV.length; i++) {
//        	mAxisXValues.add(new AxisValue(xV[i] / 2f).setLabel((i==24 ? 0 : xV[i]) + "h"));   	        	
//		}
        
//        for (int j = 0; j < 30; j+=5) {//循环为节点、X、Y轴添加数据  
//        	axisValuesY.add(new AxisValue(j).setValue(j));// 添加Y轴显示的刻度值  }
//        }
   
        for (int i = 0; i <= mClockNumberLength; i++) {      //mClockNumberLength                               
            
            if (i==0 || i==6 || i==12 || i==18 || i==23) {
            	mAxisXValues.add(new AxisValue(i / 2f).setLabel((i==24 ? 0 : i) + "h"));
			}

        }

        //设置y轴坐标，显示的是数值0，10，20.。。。
        mAxisYValues.clear();
        for (int i = 0; i < 11; i++) {
            int lengthY = 10 * i;
            
            if (i%2==0) {
            	mAxisYValues.add(new AxisValue(lengthY).setLabel(" " + lengthY + "%"));
            }
            
        }

    }


    private void resetViewport() {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 0;
        v.top = 100;
        v.left = 0;
        v.right = 12; //  listBlood.size() - 1//如何解释
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }
    

	
	private void scanLeDevice(final long time) {	
		
		String groupname = SharePersistent.getPerference(getApplicationContext(), Constant.GROUP_NAME);
		textViewSelectedDeviceName.setText(groupname+"   +");
		
		if (handler == null) {
			handler = new Handler(); // 开启 定时搜索 定时器
		}
		if (runnable == null) {
			runnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					handler.postDelayed(this, time);// 5秒 是延时时长
					refreshDevices();		
				}
			};
			handler.postDelayed(runnable, time);// 打开定时器，执行操作
		}				
	}
	
	/**
	 * 刷新
	 */
	protected void refreshDevices() {
		
		String groupData[] = SharePersistent.getSingleGroupData(getApplicationContext(), "device");
		ArrayList<BluetoothDevice> bleDevices = LedBleApplication.getApp().getConnectedBleDevices();			
		
		if (bleDevices.size() > 0) {
			textViewSelectedDeviceName.setText(groupData[1]+"   +");
			textViewSelectedDeviceName.setTextColor(android.graphics.Color.WHITE);
		}else {
			textViewSelectedDeviceName.setText("Not Connected"+"   +");
			textViewSelectedDeviceName.setTextColor(android.graphics.Color.RED);
		}		
	}
	
	
	
	private void postConnectAction(String bleAddress) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		intent.setAction("action.connectble.DeviceListActivity");// 设置Action
		intent.setPackage(getPackageName());// 设置包名使广播只能被app内接收者接收
		intent.putExtra("bleAddress", bleAddress);
		sendBroadcast(intent);
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_device);
		groupName = getIntent().getStringExtra("group");
		groupDevices = (ArrayList<GroupDevice>) getIntent().getSerializableExtra("devices");
		
		
		llCo2 = (LinearLayout)findViewById(R.id.llCo2);
		
		chart = (LineChartView)findViewById(R.id.line_chart);chart = (LineChartView)findViewById(R.id.line_chart);
		viewFrontLeft = findViewById(R.id.viewFrontLeft);
		viewFrontRight = findViewById(R.id.viewFrontRight);   
		
		
		WindowManager wm = (WindowManager) DeviceActivity.this.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics dm = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(dm);
        screenWidthPX = dm.widthPixels;         // 屏幕宽度（像素）
        int height = dm.heightPixels;       // 屏幕高度（像素）
        float density = dm.density;         // 屏幕密度（0.75 / 1.0 / 1.5）
        // 屏幕宽度算法:屏幕宽度（像素）/屏幕密度
        screenWidthDP = (int) (screenWidthPX / density);  // 屏幕宽度(dp)
        screenHeight = (int) (height / density);// 屏幕高度(dp)
        
		
//		lineChart = (LineChartView)findViewById(R.id.line_chart);
//		viewFront = findViewById(R.id.viewFront);
	    lines.add(redLine); 
		lines.add(greenLine);
	    lines.add(blueLine);
	    lines.add(whiteLine); 
	    
	   
//	    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) lineChart.getLayoutParams(); 
//	    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) viewFront.getLayoutParams();
//		params.setMargins(layoutParams.leftMargin+70, 0, 0, 0);// 通过自定义坐标来放置你的控件
//	    viewFront.setLayoutParams(params);
	    	 		
		
		textViewSelectedDeviceName = findTextViewById(R.id.textViewSelectedDeviceName);
		textViewSelectedDeviceName.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
//				postConnectAction(SharePersistent.getPerference(getApplicationContext(), "bleAddress"));  // 广播通知连接设备 
				if (handler != null) {
					handler.removeCallbacks(runnable);
					handler = null;
					runnable = null;
				}
				finish();
			}
		});
		
//		String devicename = SharePersistent.getPerference(getApplicationContext(), "groupName");
//		if (devicename != null && devicename.length() > 0) {
//			textViewSelectedDeviceName.setText(devicename+"   +");
//			postConnectAction(SharePersistent.getPerference(getApplicationContext(), "bleAddress"));  // 广播通知连接设备 
//			NetConnectBle.getInstanceByDeviceAddress(SharePersistent.getPerference(getApplicationContext(), "bleAddress"));
//		}

		listViewDevices = findListViewById(R.id.listViewDevices);
		bleDeviceAdapter = new BleSelectDeviceAdapter(this);
		bleDeviceAdapter.setSelected(groupDevices);
		listViewDevices.setAdapter(bleDeviceAdapter);
		
		textViewSUN = findTextViewById(R.id.textViewSUN);
		textViewSUN.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusSUN) {
				case 0:
					textViewSUN.setBackgroundResource(R.color.green);
					statusSUN = 1;
					break;
				case 1:
					textViewSUN.setBackgroundResource(R.color.transparent);
					statusSUN = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "SUN"+TAG+deviceName, statusSUN);
			}
		});
		
		
		textViewMON = findTextViewById(R.id.textViewMON);
		textViewMON.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusMON) {
				case 0:
					textViewMON.setBackgroundResource(R.color.green);
					statusMON = 1;
					break;
				case 1:
					textViewMON.setBackgroundResource(R.color.transparent);
					statusMON = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "MON"+TAG+deviceName, statusMON);
			}
		});
		
		
		textViewTUE = findTextViewById(R.id.textViewTUE);
		textViewTUE.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusTUE) {
				case 0:
					textViewTUE.setBackgroundResource(R.color.green);
					statusTUE = 1;
					break;
				case 1:
					textViewTUE.setBackgroundResource(R.color.transparent);
					statusTUE = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "TUE"+TAG+deviceName, statusTUE);
			}
		});
		
		
		textViewWED = findTextViewById(R.id.textViewWED);
		textViewWED.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusWED) {
				case 0:
					textViewWED.setBackgroundResource(R.color.green);
					statusWED = 1;
					break;
				case 1:
					textViewWED.setBackgroundResource(R.color.transparent);
					statusWED = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "WED"+TAG+deviceName, statusWED);
			}
		});
		
		
		textViewTHU = findTextViewById(R.id.textViewTHU);
		textViewTHU.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusTHU) {
				case 0:
					textViewTHU.setBackgroundResource(R.color.green);
					statusTHU = 1;
					break;
				case 1:
					textViewTHU.setBackgroundResource(R.color.transparent);
					statusTHU = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "THU"+TAG+deviceName, statusTHU);
			}
		});
		
		
		textViewFRI = findTextViewById(R.id.textViewFRI);
		textViewFRI.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusFRI) {
				case 0:
					textViewFRI.setBackgroundResource(R.color.green);
					statusFRI = 1;
					break;
				case 1:
					textViewFRI.setBackgroundResource(R.color.transparent);
					statusFRI = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "FRI"+TAG+deviceName, statusFRI);
			}
		});
		
		
		textViewSAT = findTextViewById(R.id.textViewSAT);
		textViewSAT.setOnClickListener(new OnClickListener() { // 寮�鍏崇伅
			@Override
			public void onClick(View v) {
				switch (statusSAT) {
				case 0:
					textViewSAT.setBackgroundResource(R.color.green);
					statusSAT = 1;
					break;
				case 1:
					textViewSAT.setBackgroundResource(R.color.transparent);
					statusSAT = 0;
					break;
				default:
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), "SAT"+TAG+deviceName, statusSAT);
			}
		});
		
		initDayViews(TAG);
		
		
		listViewDevices.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				BluetoothDevice ble = bleDeviceAdapter.getDevice(position);
				bleDeviceAdapter.select(ble);
				bleDeviceAdapter.notifyDataSetChanged();
			}
		});
		
	

		
	
	
		for (int i = 0; i <= 24; i++) {			
			String[] timeData = SharePersistent.getTimerData(getApplicationContext(), String.valueOf(i)+TAG+deviceName, i);
//			if (timeData[2].equalsIgnoreCase("-1")) {
//				timeData[2] = "-";
//			}
//			if (timeData[3].equalsIgnoreCase("-1")) {
//				timeData[3] = "-";
//			}
//			if (timeData[4].equalsIgnoreCase("-1")) {
//				timeData[4] = "-";
//			}
//			if (timeData[5].equalsIgnoreCase("-1")) {
//				timeData[5] = "-";
//			}
			TimeRGBW mp3 = new TimeRGBW();
			mp3.setR(timeData[2]);
			mp3.setG(timeData[3]);
			mp3.setB(timeData[4]);
			mp3.setW(timeData[5]);
			mp3.setCo2(timeData[6]);
			if (i >= 0 && i <= 9) {						
				mp3.setTime("0"+i+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : timeData[1]));													
			}else {
				if (24==i) {
					mp3.setTime("00"+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : timeData[1]));
				}else {
					mp3.setTime(i+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : timeData[1]));
				}		
			}						
			mp3s.add(mp3);
		}		
		
//		generateRedLineValues();
		
		
		

		timeDataAdapter = new TimeDataAdapter(this, mp3s);
//		timeDataAdapter.setOnSelectListener(new OnSelectListener() {
//			@Override
//			public void onSelect(int index, TimeRGBW mp3, HashSet<TimeRGBW> mp3s, boolean isCheck, BaseAdapter adapter) {
//				if (mp3s.contains(mp3)) {
//					mp3s.remove(mp3);
//				} else {
//					mp3s.add(mp3);
//				}
//				adapter.notifyDataSetChanged();
////				goBackWithData(mp3Adapter);
//			}
//		});
		listViewTimeData = findListViewById(R.id.listViewTimeData);
		listViewTimeData.setAdapter(timeDataAdapter);	
		listViewTimeData.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				try {
					TimeRGBW data = (TimeRGBW) parent.getAdapter().getItem(position);
					String label[] = data.getTime().split(":");
					if (TAG.equalsIgnoreCase(TAG_AQ) || TAG.equalsIgnoreCase(TAG_TP)) {
						return;
					}					
					
//					Toast.makeText(MainActivity.getMainActivity(), "hour= "+label[0], Toast.LENGTH_SHORT).show();
					
					positionTag = position;					
					gotoTimerSettting(label[0], label[1], data.getR(), data.getG(), data.getB(), data.getW(), data.getCo2());
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}			
		});
		listViewTimeData.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, final View view, final int position, long id) {
				// TODO Auto-generated method stub
//				Toast.makeText(MainActivity.getMainActivity(), ""+count, Toast.LENGTH_SHORT).show();
				
				final TimeRGBW data = (TimeRGBW) parent.getAdapter().getItem(position);
				
				if (data.getHour().equalsIgnoreCase("0") || data.getHour().equalsIgnoreCase("24")) {
					return false;
				}
				
				final TextView inputServer = new TextView(DeviceActivity.this); 
				inputServer.setText(data.getTime());
				inputServer.setGravity(Gravity.CENTER);
	            AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);  
	            builder.setTitle("Delete").setIcon(android.R.drawable.ic_dialog_info).setView(inputServer)  
	                    .setNegativeButton("Cancel", null);  
	            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {  	  
	                public void onClick(DialogInterface dialog, int which) {  
	                	
	                	data.setR("-");
	                	data.setG("-");
	                	data.setB("-");
	                	data.setW("-");
//	                	data.setCo2("0");
	                	data.setCo2("-");
	    				
//	    				mp3s.set(Integer.parseInt(hour), mp3);
//	                	reloadModeData(TAG);
	                	
	    				timeDataAdapter.notifyDataSetChanged();
	    				generateRedLineValues();
	    					    				
	    				saveData();	   
	    				
	    				reloadCo2View();
	                 }  
	            });  
	            builder.show();
				
				return true;
			}
		});
		

		

		

		findButtonById(R.id.buttonFireBind).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LedBleApplication.getApp().setTempDevices(bleDeviceAdapter.getSelectSet());
				Intent data = new Intent();
				data.putExtra("group", groupName);
				setResult(Activity.RESULT_OK, data);
				finish();
			}
		});
		
		textViewMode = findTextViewById(R.id.textViewMode);
		textViewMode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				PopModeWindow(v);
				showPopMenu();
			}
		});
		
		textViewMode.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub

				String tagA = SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName).length() > 0 
						? SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName) : TAG_UA;
				String tagB = SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName).length() > 0 
						? SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName) : TAG_UB;
				if (TAG.equalsIgnoreCase(TAG_UA) || TAG.equalsIgnoreCase(TAG_UB)) {
					final EditText inputServer = new EditText(DeviceActivity.this);
					inputServer.setText(TAG.equalsIgnoreCase(TAG_UA) ? tagA : tagB);
					AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);
					builder.setTitle(R.string.please_input).setIcon(android.R.drawable.ic_dialog_info)
							.setView(inputServer).setNegativeButton("Cancel", null);
					builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {

							String groupName = inputServer.getText().toString();
							if (groupName.length() > 0) {

							}
//							tag = groupName;
							textViewMode.setText(groupName+"  +");
							SharePersistent.savePerference(getApplicationContext(), TAG+deviceName, groupName);
						}
					});
					builder.show();
				}

				return true;
			}
		});
				
		String mode = SharePersistent.getPerference(DeviceActivity.this, MODE+deviceName);
//		mode = TAG_UA;
		mode = TAG;
		if (mode.equalsIgnoreCase(TAG_AQ)) {
			textViewMode.setText(TAG_AQ+" +");
			reloadModeData(TAG_AQ);
			generateRedLineValues();
			TAG = TAG_AQ;
		}else if (mode.equalsIgnoreCase(TAG_TP)) {
			textViewMode.setText(TAG_TP+" +");
			reloadModeData(TAG_TP);
			generateRedLineValues();
			TAG = TAG_TP;
		}else if (mode.equalsIgnoreCase(TAG_UA) || mode.equalsIgnoreCase("")) {
			String tagA = SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName).length() > 0 
					? SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName) : TAG_UA;		
			textViewMode.setText(tagA+" +");
			reloadModeData(TAG_UA);
			generateRedLineValues();
			TAG = TAG_UA;
		}else if (mode.equalsIgnoreCase(TAG_UB)) {
			String tagB = SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName).length() > 0 
					? SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName) : TAG_UB;
			textViewMode.setText(tagB+" +");
			reloadModeData(TAG_UB);
			generateRedLineValues();
			TAG = TAG_UB;
		}
				
		
		
		// Reset
		findViewById(R.id.textViewReset).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
	            AlertDialog.Builder builder = new AlertDialog.Builder(DeviceActivity.this);  
	            builder.setTitle("Reset").setIcon(android.R.drawable.ic_dialog_info).setView(null)  
	                    .setNegativeButton("Cancel", null);  
	            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {  	  
	                public void onClick(DialogInterface dialog, int which) {  
	                	
	                	Toast.makeText(MainActivity.getMainActivity(), "Reset", Toast.LENGTH_SHORT).show();
						
	    				if (TAG.equalsIgnoreCase(TAG_UA) || TAG.equalsIgnoreCase(TAG_UB)) {
	    					mp3s.clear();
	    					
	    					for (int i = 0; i <= 24; i++) {

	    						TimeRGBW mp3 = new TimeRGBW();
	    						if (0 == i || 24 == i) {
	    							mp3.setHour("" + i);
		    						mp3.setMinute("00");
		    						mp3.setR("0");
		    						mp3.setG("0");
		    						mp3.setB("0");
		    						mp3.setW("0");
		    						mp3.setCo2("0");
		    						mp3s.add(mp3);
		    						SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(i) + TAG+deviceName,
		    								String.valueOf(i), "0", "0", "0", "0", "0", "0");
								}else {
									mp3.setHour("" + i);
		    						mp3.setMinute("00");
		    						mp3.setR("-");
		    						mp3.setG("-");
		    						mp3.setB("-");
		    						mp3.setW("-");
		    						mp3.setCo2("-");
		    						mp3s.add(mp3);
		    						SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(i) + TAG+deviceName,
		    								String.valueOf(i), "-", "-", "-", "-", "-", "-");
								}	    						
	    					}		
	    					
	    					reloadModeData(TAG);
	    					generateRedLineValues();
	    				}
	    				
	                	
	    		        if (firSend) {
	    					firValue = viewFrontLeft.getLeft();
	    					intervalValue = (viewFrontRight.getRight()-viewFrontLeft.getLeft())/48;
	    					firSend = false;
	    				}
	    		        
	    		        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) viewFrontLeft.getLayoutParams();				
	    		        layoutParams.leftMargin = firValue;
	    		        viewFrontLeft.setLayoutParams(layoutParams);
	                 }  
	            });  
	            builder.show();
					
			}
		});
		
		// Review
		seekBarReview = (SeekBar)findViewById(R.id.seekBarReview);
		seekBarReview.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {					
//				MainActivity.getMainActivity().setColorCommandWithOutCo2(redValue, greenValue, blueValue, whiteValue);
			}
		});
		findViewById(R.id.textViewReview).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				if (firSend) {
					firValue = viewFrontLeft.getLeft();
					intervalValue = (viewFrontRight.getRight()-viewFrontLeft.getLeft())/48;
					firSend = false;
				}
				
				if (canSend) {
					canSend = false;
				}else {
					return;
				}
					int[] locationsLeft = new int[2];
					viewFrontLeft.getLocationOnScreen(locationsLeft);
				    int xl = locationsLeft[0];//获取组件当前位置的横坐标
				    int yl = locationsLeft[1];//获取组件当前位置的纵坐标		    
				    int[] locationsRight = new int[2];
					viewFrontRight.getLocationOnScreen(locationsRight);
				    int xr = locationsRight[0];//获取组件当前位置的横坐标
				    int yr = locationsRight[1];//获取组件当前位置的纵坐标
//				    Toast.makeText(MainActivity.getMainActivity(), "xl= "+xl+",  yl= " + yl, Toast.LENGTH_SHORT).show();
//				    Toast.makeText(MainActivity.getMainActivity(), "xr= "+xr+",  yr= " + yr, Toast.LENGTH_SHORT).show();
				     
				    
				    
				    
				    FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) viewFrontLeft.getLayoutParams();			
					float value = (float)(count*(intervalValue+0.548));
			        layoutParams.leftMargin = firValue + (int)(value);			    
			        viewFrontLeft.setLayoutParams(layoutParams);
	
				
				    if (mp3s.size() > 0) {
						tempArr.clear();
						lastDArr.clear();
						
						for (int i = 0; i < mp3s.size(); i++) {
							TimeRGBW timeRGBW = mp3s.get(i);
							if (!timeRGBW.getR().equalsIgnoreCase("-")) {
								tempArr.add(timeRGBW);
							}
						}
//						Toast.makeText(MainActivity.getMainActivity(), "tempArr.size() = "+tempArr.size(), Toast.LENGTH_SHORT).show();
						Review();
					}				    						    
			  						
			}
		});
		
		
		// ******************** Save-Uploading ************************//
		
		findViewById(R.id.textViewUpload).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Toast.makeText(MainActivity.getMainActivity(), "Save-Uploading", Toast.LENGTH_SHORT).show();								
				saveAndSendCMD();
			}
		});
          
		findViewById(R.id.textViewBack).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		// Manual 
        findViewById(R.id.textViewSelectColor).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(DeviceActivity.this, ManualSelectColorActivity.class));	
			}
		});	
		
		findViewById(R.id.textViewInfo).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PopInfoWindow(v);
			}
		});	
	}
	
	private void initDayViews(String tag) {
		
		if (SharePersistent.getDayInt(getApplicationContext(), "SUN"+tag+deviceName) == 1) {
			statusSUN = 1;
			textViewSUN.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "SUN"+tag+deviceName) == 0) {
			statusSUN = 0;
			textViewSUN.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "SUN"+tag+deviceName) == -1) {
			statusSUN = 1;
			textViewSUN.setBackgroundResource(R.color.green);
		}
//		Toast.makeText(this, "SUN = "+SharePersistent.getDayInt(getApplicationContext(), "SUN"+tag), Toast.LENGTH_SHORT).show();
		
		if (SharePersistent.getDayInt(getApplicationContext(), "MON"+tag+deviceName) == 1) {
			statusMON = 1;
			textViewMON.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "MON"+tag+deviceName) == 0) {
			statusMON = 0;
			textViewMON.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "MON"+tag+deviceName) == -1) {
			statusMON = 1;
			textViewMON.setBackgroundResource(R.color.green);
		}
		
		if (SharePersistent.getDayInt(getApplicationContext(), "TUE"+tag+deviceName) == 1) {
			statusTUE = 1;
			textViewTUE.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "TUE"+tag+deviceName) == 0) {
			statusTUE = 0;
			textViewTUE.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "TUE"+tag+deviceName) == -1) {
			statusTUE = 1;
			textViewTUE.setBackgroundResource(R.color.green);
		}
		
		if (SharePersistent.getDayInt(getApplicationContext(), "WED"+tag+deviceName) == 1) {
			statusWED = 1;
			textViewWED.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "WED"+tag+deviceName) == 0) {
			statusWED = 0;
			textViewWED.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "WED"+tag+deviceName) == -1) {
			statusWED = 1;
			textViewWED.setBackgroundResource(R.color.green);
		}
		
		if (SharePersistent.getDayInt(getApplicationContext(), "THU"+tag+deviceName) == 1) {
			statusTHU= 1;
			textViewTHU.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "THU"+tag+deviceName) == 0) {
			statusTHU = 0;
			textViewTHU.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "THU"+tag+deviceName) == -1) {
			statusTHU = 1;
			textViewTHU.setBackgroundResource(R.color.green);
		}
		
		if (SharePersistent.getDayInt(getApplicationContext(), "FRI"+tag+deviceName) == 1) {
			statusFRI = 1;
			textViewFRI.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "FRI"+tag+deviceName) == 0) {
			statusFRI = 0;
			textViewFRI.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "FRI"+tag+deviceName) == -1) {
			statusFRI = 1;
			textViewFRI.setBackgroundResource(R.color.green);
		}
		
		if (SharePersistent.getDayInt(getApplicationContext(), "SAT"+tag+deviceName) == 1) {
			statusSAT = 1;
			textViewSAT.setBackgroundResource(R.color.green);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "SAT"+tag+deviceName) == 0) {
			statusSAT = 0;
			textViewSAT.setBackgroundResource(R.color.transparent);
		}else if (SharePersistent.getDayInt(getApplicationContext(), "SAT"+tag+deviceName) == -1) {
			statusSAT = 1;
			textViewSAT.setBackgroundResource(R.color.green);
		}
		
	}
	
	
	private void showPopMenu() {
		View contentView = LayoutInflater.from(this).inflate(R.layout.pop_menu, null);
		final String ModeA = SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName);
		final String ModeB = SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName);
		if (ModeA != null && ModeA.length() > 0) {
			((TextView)contentView.findViewById(R.id.textViewUserSetA)).setText(ModeA);
		}
		if (ModeB != null && ModeB.length() > 0) {
			((TextView)contentView.findViewById(R.id.textViewUserSetB)).setText(ModeB);
		}
		
		// 创建并显示popWindow
		mCustomPopWindow = new CustomPopWindow.PopupWindowBuilder(this).setView(contentView).create()
				.showAsDropDown(textViewMode, 0, 20);
		mCustomPopWindow.showAsDropDown(textViewMode, 0, 20);
//		mCustomPopWindow = null;
//		Toast.makeText(this, "1", Toast.LENGTH_SHORT).show();

		// 处理popWindow 显示内容
		handleLogic(contentView);
	}
	 
	/**
	 * 处理弹出显示内容、点击事件等逻辑
	 * 
	 * @param contentView
	 */
	private void handleLogic(View contentView) {
		View.OnClickListener listener = new View.OnClickListener() {
			@Override
			public void onClick(View v) {				
					mCustomPopWindow.dissmiss();
					mCustomPopWindow = null;				
				switch (v.getId()) {
				case R.id.menu1:
					textViewMode.setText(TAG_AQ+" +");
					reloadModeData(TAG_AQ);
					generateRedLineValues();
					TAG = TAG_AQ;
					break;
				case R.id.menu2:
					textViewMode.setText(TAG_TP+" +");
					reloadModeData("Tropical");
					generateRedLineValues();
					TAG = TAG_TP;
					break;
				case R.id.menu3:
					textViewMode.setText((SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName).length() > 0 
							? SharePersistent.getPerference(getApplicationContext(), TAG_UA+deviceName) : TAG_UA)+" +");
					reloadModeData(TAG_UA);
					generateRedLineValues();
					TAG = TAG_UA;
//					tag = SharePersistent.getPerference(getApplicationContext(), TAG_UA).length() > 0 
//							? SharePersistent.getPerference(getApplicationContext(), TAG_UA) : TAG_UA;
					break;
				case R.id.menu4:
					textViewMode.setText((SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName).length() > 0 
							? SharePersistent.getPerference(getApplicationContext(), TAG_UB+deviceName) : TAG_UB)+" +");
					reloadModeData(TAG_UB);
					generateRedLineValues();
					TAG = TAG_UB;
//					tag = SharePersistent.getPerference(getApplicationContext(), TAG_UB).length() > 0 
//							? SharePersistent.getPerference(getApplicationContext(), TAG_UB) : TAG_UB;
					break;
				}
				SharePersistent.savePerference(getApplicationContext(), MODE+deviceName, TAG);
			}
		};
		contentView.findViewById(R.id.menu1).setOnClickListener(listener);
		contentView.findViewById(R.id.menu2).setOnClickListener(listener);
		contentView.findViewById(R.id.menu3).setOnClickListener(listener);
		contentView.findViewById(R.id.menu4).setOnClickListener(listener);
		contentView.findViewById(R.id.menu5).setOnClickListener(listener);
	}
	
	
	private void Review() {	
				
		final Handler handler = new Handler(); // 开启 定时搜索 定时器
		final Runnable runnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				handler.postDelayed(this, 500);// 5秒 是延时时长
				count++;
				
//				setReviewCMD();
//				updataUI();
				
				if (count == 49) {
					canSend = true;
					handler.removeCallbacks(this);
				}
				
				setReviewCMD();
				updataUI();
			}
		};
		handler.postDelayed(runnable, 100);// 打开定时器，执行操作
					
	}
	
	
	private void setReviewCMD() {
		
		if (tempArr.size() >= 2) {
			for (int i = 0; i < tempArr.size(); i++) {			
				
				if (i == 0) {
					TimeRGBW timeRGBW1 = tempArr.get(i);
					TimeRGBW timeRGBW2 = tempArr.get(i+1);
					
					float minute1 = Integer.parseInt(timeRGBW1.getHour())*60 + Integer.parseInt(timeRGBW1.getMinute());
					float minute2 = Integer.parseInt(timeRGBW2.getHour())*60 + Integer.parseInt(timeRGBW2.getMinute());
					
					if ((minute2-minute1) > 0) {
						float k1 = (count*30-minute1)/(minute2-minute1);					
						
						int r = (int)(Float.parseFloat(timeRGBW1.getR())*(1-k1) + Float.parseFloat(timeRGBW2.getR())*k1);
						int g = (int)(Float.parseFloat(timeRGBW1.getG())*(1-k1) + Float.parseFloat(timeRGBW2.getG())*k1);
						int b = (int)(Float.parseFloat(timeRGBW1.getB())*(1-k1) + Float.parseFloat(timeRGBW2.getB())*k1);
						int w = (int)(Float.parseFloat(timeRGBW1.getW())*(1-k1) + Float.parseFloat(timeRGBW2.getW())*k1);
												
						if (time.minute < 30) {	
					
							int r2 = (r*time.minute)/30;
							int g2 = (g*time.minute)/30;
							int b2 = (b*time.minute)/30;
							int w2 = (w*time.minute)/30;
							
							if (count == time.hour*2) {
								TimeRGBW data = new TimeRGBW();
								data.setR(""+r2);
								data.setG(""+g2);
								data.setB(""+b2);
								data.setW(""+w2);
								data.setCo2(timeRGBW1.getCo2());
								lastDArr.add(data);
							}
						}else {
							
							int r2 = (r*time.minute)/60;
							int g2 = (g*time.minute)/60;
							int b2 = (b*time.minute)/60;
							int w2 = (w*time.minute)/60;
							
							if (count == time.hour*2+1) {
								TimeRGBW data = new TimeRGBW();
								data.setR(""+r2);
								data.setG(""+g2);
								data.setB(""+b2);
								data.setW(""+w2);
								data.setCo2(timeRGBW1.getCo2());
								lastDArr.add(data);
							}
						}
						
						MainActivity.getMainActivity().setReviewMode(r, g, b, w, Integer.parseInt(timeRGBW1.getCo2()));
										
						if (count*30+30 > minute2) {
							if (tempArr.size()>0) {
								tempArr.remove(i);
							}							
						}
					}
				}																		
			}
		}
		
		if (count == 48) {
			MainActivity.getMainActivity().setExitToNormalMode(0, 0, 0, 0);
		}
		
		if (count == 49) {			
			
			saveAndSendCMD();
			
			// *********** 发送第49 个指令 *******************//
			
//			if (lastDArr.size() >= 0) {
//
//				TimeRGBW mode = lastDArr.get(0);
//				
//				int r = Integer.parseInt(mode.getR());
//				int g = Integer.parseInt(mode.getG());
//				int b = Integer.parseInt(mode.getB());
//				int w = Integer.parseInt(mode.getW());
//				int co2 = Integer.parseInt(mode.getCo2());
//
////				Toast.makeText(MainActivity.getMainActivity(), "lastDArr.size= " + lastDArr.size() + ", " + "r= " + r
////						+ ", " + "g= " + g + ", " + "b= " + b + ", " + "w= " + w + ", ", Toast.LENGTH_SHORT).show();
//
//				MainActivity.getMainActivity().setReviewMode(r, g, b, w, co2);
//
//			}
		}
	}
	
	private void updataUI() {
		seekBarReview.setProgress(count);
		
		FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) viewFrontLeft.getLayoutParams();	
		
		float value = (float)(count*(intervalValue+0.548));
        layoutParams.leftMargin = firValue + (int)(value);
        
//        layoutParams.leftMargin = 89 + (count*20);
        viewFrontLeft.setLayoutParams(layoutParams);
        
        	    	    
		if (count == 49) {
			Time t = new Time(); // or Time t=new Time("GMT+8"); 
			t.setToNow(); // 取得系统时间。
			if (t.minute < 30) {
				
				seekBarReview.setProgress(t.hour*2);
				layoutParams.leftMargin = firValue + (int)(t.hour*2*(intervalValue+0.548));
				viewFrontLeft.setLayoutParams(layoutParams);
			}else {
				seekBarReview.setProgress(t.hour*2+1);
				layoutParams.leftMargin = firValue + (int)((t.hour*2+1)*(intervalValue+0.548));
				viewFrontLeft.setLayoutParams(layoutParams);
			}	
			
//			MainActivity.getMainActivity().setExitToNormalMode(0, 0, 0, 0);
			count = 0;
//			firValue = 0;
		}
//		Toast.makeText(MainActivity.getMainActivity(), ""+count, Toast.LENGTH_SHORT).show();
	}
	
	public int Px2Dp(Context context, float px) { 
	    final float scale = context.getResources().getDisplayMetrics().density; 
	    return (int) (px / scale + 0.5f); 
	}
	
	
	

    
    
    
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        rawX= (int) ev.getRawX();
        rawY= (int) ev.getRawY();
        return super.dispatchTouchEvent(ev);
    }
    
    private void reloadCo2View() {
		
    	for (int i = 0; i <= 23; i++) {
			
			TimeRGBW t1 = mp3s.get(i);
			ImageView imageView1 =  (ImageView)llCo2.findViewWithTag((String.valueOf("co2ImageView") + i));			
			if ( t1.getCo2().equalsIgnoreCase("1") ) {
				imageView1.setVisibility(View.VISIBLE);
			}
						
			for (int j = i; j <= 23; j++) {
				
				TimeRGBW t2 = mp3s.get(j);	
				ImageView imageView2 =  (ImageView)llCo2.findViewWithTag((String.valueOf("co2ImageView") + j));
//				Toast.makeText(MainActivity.getMainActivity(), "j= "+j, Toast.LENGTH_SHORT).show();
				
				if ( t1.getCo2().equalsIgnoreCase("1") && t2.getCo2().equalsIgnoreCase("-")) {
					imageView2.setVisibility(View.VISIBLE);
				}
					
				else if (t1.getCo2().equalsIgnoreCase("0") && (t2.getCo2().equalsIgnoreCase("0") || t2.getCo2().equalsIgnoreCase("-"))) {
					imageView2.setVisibility(View.GONE);
				} 					
				
			}
			
		}	
		
	}
    
	private void reloadModeData(String tag) {
		mp3s.clear();
		initDayViews(tag);
//		reloadCo2View();
		
		if (tag.equalsIgnoreCase(TAG_UA) || tag.equalsIgnoreCase(TAG_UB)) {
			for (int i = 0; i <= 24; i++) {
				String[] timeData = SharePersistent.getTimerData(getApplicationContext(), String.valueOf(i)+tag+deviceName, i);
				
				TimeRGBW mp3 = new TimeRGBW();
				mp3.setHour(""+i);
				mp3.setMinute((timeData[1].equalsIgnoreCase("-") ? "00" : timeData[1]));
				mp3.setR(timeData[2]);
				mp3.setG(timeData[3]);
				mp3.setB(timeData[4]);
				mp3.setW(timeData[5]);
				mp3.setCo2(timeData[6]);
				if (i >= 0 && i <= 9) {						
					mp3.setTime("0"+i+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : (Integer.valueOf(timeData[1]) >= 10 ? timeData[1] : "0"+timeData[1])));
				}else {
					if (24==i) {
//						mp3.setTime("00"+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : (Integer.valueOf(timeData[1]) >= 10 ? timeData[1] : "0"+timeData[1])));
						mp3.setTime(i+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : (Integer.valueOf(timeData[1]) >= 10 ? timeData[1] : "0"+timeData[1])));
					}else {
						mp3.setTime(i+":"+(timeData[1].equalsIgnoreCase("-") ? "00" : (Integer.valueOf(timeData[1]) >= 10 ? timeData[1] : "0"+timeData[1])));
					}				
				}
				
				
				mp3s.add(mp3);
			}
			
			
//			TimeRGBW t111 = mp3s.get(positionTag);
//			Toast.makeText(MainActivity.getMainActivity(), "positionTag= "+positionTag+",  "+"co2= "+t111.getCo2(), Toast.LENGTH_SHORT).show();
			
			reloadCo2View();
													
			timeDataAdapter.notifyDataSetInvalidated();
			return;
		}
		
		for (int i = 0; i <= 24; i++) {
			
			TimeRGBW mp3 = new TimeRGBW();
			mp3.setTime(i+":00");
			mp3.setHour(""+i);
			mp3.setMinute("00");
//			mp3.setCo2("0");						
			
			if (i > 0 && i <= 9) {	
				mp3.setTime("0"+i+":00");
				mp3.setR("-");
				mp3.setG("-");
				mp3.setB("-");
				mp3.setW("-");
				mp3.setCo2("-");
			}
			if (i==24 || i==0) {
				mp3.setTime("0"+0+":00");
				mp3.setCo2("0");
			}
			if (i==10 || i==0 || i==24) {			
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");	
				mp3.setCo2("0");
			}else if (11 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "10" : "0");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "10" : "0");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "0" : "0");
			}else if (12 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "20" : "0");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "20" : "0");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "20" : "0");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "50" : "0");
			}else if (13 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "60" : "10");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "10");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "0");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "0");
			}else if (14 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "80" : "20");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "85" : "20");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "85" : "50");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "85" : "50");
			}else if (15 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "80" : "60");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "50");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
			}else if (16 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "65");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
			}else if (17 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "75");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "100");
			}else if (18 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "75");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "60" : "70");
			}else if (19 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "65");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "30" : "50");
			}else if (20 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "55");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "10" : "30");
			}else if (21 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "70");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "85");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "100" : "54");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "10" : "10");
			}else if (22 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "60");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "70" : "70");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "50" : "30");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "10" : "10");
			}else if (23 == i) {
				mp3.setR(tag.equalsIgnoreCase(TAG_AQ) ? "50" : "50");
				mp3.setG(tag.equalsIgnoreCase(TAG_AQ) ? "50" : "50");
				mp3.setB(tag.equalsIgnoreCase(TAG_AQ) ? "2" : "2");
				mp3.setW(tag.equalsIgnoreCase(TAG_AQ) ? "2" : "2");
			}
			if (i>=11 && i<=23) {
				mp3.setCo2(tag.equalsIgnoreCase(TAG_AQ) ? "1" : "0");				
			}
											
			mp3s.add(mp3);
		}					
		
		reloadCo2View();
		
		timeDataAdapter.notifyDataSetInvalidated();		
	}
	
	
	private void generateRedLineValues() {
		
		redlistBlood.clear();
		greenlistBlood.clear();
		bluelistBlood.clear();
		whitelistBlood.clear();
		for (int i = 0; i < mp3s.size(); i++) {
			TimeRGBW timeRGBW = mp3s.get(i);					
			if (timeRGBW.getR().equalsIgnoreCase("-")) {						

			}else {					
				redlistBlood.add(new BloodPressureBean("2017-5-1"+" "+timeRGBW.getHour()+":"+timeRGBW.getMinute()+":0", Integer.parseInt(timeRGBW.getR())));
				greenlistBlood.add(new BloodPressureBean("2017-5-1"+" "+timeRGBW.getHour()+":"+timeRGBW.getMinute()+":0", Integer.parseInt(timeRGBW.getG())));
				bluelistBlood.add(new BloodPressureBean("2017-5-1"+" "+timeRGBW.getHour()+":"+timeRGBW.getMinute()+":0", Integer.parseInt(timeRGBW.getB())));
				whitelistBlood.add(new BloodPressureBean("2017-5-1"+" "+timeRGBW.getHour()+":"+timeRGBW.getMinute()+":0", Integer.parseInt(timeRGBW.getW())));
			}														
		}


        //获取距离
		distanceRedList.clear();					
        for (int i = 0; i < redlistBlood.size(); i++) {
            long disY0 = TimeUtil.getTimeLong("yyyy-MM-dd HH", redlistBlood.get(0).getTime());//第一个小时数的距离
            long disY = TimeUtil.getTimeLong("yyyy-MM-dd HH:mm:ss", redlistBlood.get(i).getTime());
            float apart = (disY - disY0) / (float) timepre;//得到的拒Y轴的距离
            distanceRedList.add(apart);
        }
        distanceGreenList.clear();
        for (int i = 0; i < greenlistBlood.size(); i++) {
            long disY0 = TimeUtil.getTimeLong("yyyy-MM-dd HH", greenlistBlood.get(0).getTime());//第一个小时数的距离
            long disY = TimeUtil.getTimeLong("yyyy-MM-dd HH:mm:ss", greenlistBlood.get(i).getTime());
            float apart = (disY - disY0) / (float) timepre;//得到的拒Y轴的距离
            distanceGreenList.add(apart);
        }
        distanceBlueList.clear();
        for (int i = 0; i < bluelistBlood.size(); i++) {
            long disY0 = TimeUtil.getTimeLong("yyyy-MM-dd HH", bluelistBlood.get(0).getTime());//第一个小时数的距离
            long disY = TimeUtil.getTimeLong("yyyy-MM-dd HH:mm:ss", bluelistBlood.get(i).getTime());
            float apart = (disY - disY0) / (float) timepre;//得到的拒Y轴的距离
            distanceBlueList.add(apart);
        }
        distanceWhiteList.clear();
        for (int i = 0; i < whitelistBlood.size(); i++) {
            long disY0 = TimeUtil.getTimeLong("yyyy-MM-dd HH", whitelistBlood.get(0).getTime());//第一个小时数的距离
            long disY = TimeUtil.getTimeLong("yyyy-MM-dd HH:mm:ss", whitelistBlood.get(i).getTime());
            float apart = (disY - disY0) / (float) timepre;//得到的拒Y轴的距离
            distanceWhiteList.add(apart);
        }
             		
		// 把数据设置到线条上面去
        mRedPointValues.clear();
		for (int j = 0; j < redlistBlood.size(); ++j) {
			// PointValue的两个参数值，一个是距离y轴的长度距离，另一个是距离x轴长度距离
//			values.add(new PointValue(distanceList.get(j), listBlood.get(j).getValue() / 2f)); // y的值除以2，因为默认在y上显示的是0到100，0到200的数值除以2，就相当于0到100.
			mRedPointValues.add(new PointValue(distanceRedList.get(j), redlistBlood.get(j).getValue()));
		}
		mGreenPointValues.clear();
		for (int j = 0; j < greenlistBlood.size(); ++j) {
			mGreenPointValues.add(new PointValue(distanceGreenList.get(j), greenlistBlood.get(j).getValue()));
		}
		mBluePointValues.clear();
		for (int j = 0; j < bluelistBlood.size(); ++j) {
			mBluePointValues.add(new PointValue(distanceBlueList.get(j), bluelistBlood.get(j).getValue()));
		}
		mWhitePointValues.clear();
		for (int j = 0; j < whitelistBlood.size(); ++j) {
			mWhitePointValues.add(new PointValue(distanceWhiteList.get(j), whitelistBlood.get(j).getValue()));
		}

		// 设置线条的基本属性
		redLine.setColor(ChartUtils.COLOR_RED);
		redLine.setShape(shape);
		redLine.setCubic(isCubic);
		redLine.setFilled(isFilled);
		redLine.setHasLabels(hasLabels);
		redLine.setHasLabelsOnlyForSelected(hasLabelForSelected);
		redLine.setHasLines(hasLines);
		redLine.setHasPoints(hasPoints);
		redLine.setPointRadius(2);
		redLine.setStrokeWidth(1);// 线条的粗细，默认是3
		redLine.setHasLabels(false);// 曲线的数据坐标是否加上备注
		redLine.setHasLabelsOnlyForSelected(true);// 点击数据坐标提示数据（设置了这个line.setHasLabels(true);就无效）
		redLine.setPointColor(ChartUtils.COLOR_RED);
		redData.setLines(lines);
		
		greenLine.setStrokeWidth(1);//线条的粗细，默认是3
	    greenLine.setFilled(false);//是否填充曲线的面积
	    greenLine.setHasLabels(false);//曲线的数据坐标是否加上备注
	    greenLine.setHasLabelsOnlyForSelected(true);//点击数据坐标提示数据（设置了这个line.setHasLabels(true);就无效）
	    greenLine.setHasLines(true);//是否用直线显示。如果为false 则没有曲线只有点显示	
	    greenLine.setHasPoints(true);//是否显示圆点 如果为false 则没有原点只有点显示
	    greenLine.setPointRadius(2);          
	    greenData.setLines(lines);    
	    	       
	    blueLine.setShape(ValueShape.CIRCLE);//折线图上每个数据点的形状  这里是圆形 （有三种 ：ValueShape.SQUARE  ValueShape.CIRCLE  ValueShape.SQUARE）
	    blueLine.setCubic(false);//曲线是否平滑
	    blueLine.setStrokeWidth(1);//线条的粗细，默认是3
	    blueLine.setFilled(false);//是否填充曲线的面积
	    blueLine.setHasLabels(false);//曲线的数据坐标是否加上备注
	    blueLine.setHasLabelsOnlyForSelected(true);//点击数据坐标提示数据（设置了这个line.setHasLabels(true);就无效）
	    blueLine.setHasLines(true);//是否用直线显示。如果为false 则没有曲线只有点显示	
	    blueLine.setHasPoints(true);//是否显示圆点 如果为false 则没有原点只有点显示
	    blueLine.setPointRadius(2);	           
	    blueData.setLines(lines);	    
	    	       
	    whiteLine.setShape(ValueShape.CIRCLE);//折线图上每个数据点的形状  这里是圆形 （有三种 ：ValueShape.SQUARE  ValueShape.CIRCLE  ValueShape.SQUARE）
	    whiteLine.setCubic(false);//曲线是否平滑
	    whiteLine.setStrokeWidth(1);//线条的粗细，默认是3
	    whiteLine.setFilled(false);//是否填充曲线的面积
	    whiteLine.setHasLabels(false);//曲线的数据坐标是否加上备注
	    whiteLine.setHasLabelsOnlyForSelected(true);//点击数据坐标提示数据（设置了这个line.setHasLabels(true);就无效）
	    whiteLine.setHasLines(true);//是否用直线显示。如果为false 则没有曲线只有点显示	
	    whiteLine.setHasPoints(true);//是否显示圆点 如果为false 则没有原点只有点显示
	    whiteLine.setPointRadius(2);	           
	    whiteData.setLines(lines);
//		lines.add(redLine);


		if (hasAxesNames) {
//			axisX.setName("Time(h)");
//			axisY.setName("RGBW");
			axisX.setName(" ");
			axisY.setName(" ");
		}

		// 对x轴，数据和属性的设置
		axisX.setTextSize(8);// 设置字体的大小
		axisX.setHasTiltedLabels(false);// x坐标轴字体是斜的显示还是直的，true表示斜的
		axisX.setTextColor(Color.WHITE);// 设置字体颜色
		axisX.setValues(mAxisXValues); // 设置x轴各个坐标点名称
		axisX.setHasSeparationLine(true);
		axisX.setMaxLabelChars(1); // 最多几个X轴坐标，  
		axisX.setHasLines(false);// 是否显示X轴网格线  
		axisX.setHasSeparationLine(true);// 设置是否有分割线  
		axisX.setInside(false);// 设置X轴文字是否在X轴内部  


		// 对Y轴 ，数据和属性的设置
		axisY.setTextSize(8);
		axisY.setHasLines(false); // 是否显示Y轴网格线  
		axisY.setHasSeparationLine(true);
		axisY.setHasTiltedLabels(false);// true表示斜的
		axisY.setTextColor(Color.WHITE);// 设置字体颜色
		axisY.setValues(mAxisYValues); // 设置x轴各个坐标点名称
		axisY.setHasSeparationLine(true);
		axisY.setMaxLabelChars(2); // 最多几个Y轴坐标
//		axisY.setMaxLabelChars(2); // 最多几个Y轴坐标
		// axisY.setMaxLabelChars(2); //最多几个Y轴坐标，

		redData.setAxisXBottom(axisX);// x轴坐标线的文字，显示在x轴下方
		redData.setAxisYLeft(axisY); // 显示在y轴的左边
		greenData.setAxisXBottom(axisX); //x 轴在底部
	    greenData.setAxisYLeft(axisY);  //Y轴设置在左边  
	    blueData.setAxisXBottom(axisX); //x 轴在底部
	    blueData.setAxisYLeft(axisY);  //Y轴设置在左边
	    whiteData.setAxisXBottom(axisX); //x 轴在底部
	    whiteData.setAxisYLeft(axisY);  //Y轴设置在左边
		

//		redData.setBaseValue(Float.NaN);
		chart.setLineChartData(redData);
		chart.setLineChartData(greenData);
		chart.setLineChartData(blueData);
		chart.setLineChartData(whiteData);
		chart.setZoomEnabled(false);
		
		resetViewport();
	}
    
    private void PopInfoWindow(View v) {
    	QPopuWindow.getInstance(this).builder
        .bindView(v,0)
        .setPopupItemList(new String[]{VerString, infoString})
        .setPointers(rawX,rawY)
        .setOnPopuListItemClickListener(new QPopuWindow.OnPopuListItemClickListener() {
            @Override
            public void onPopuListItemClick(View anchorView, int anchorViewPosition, int position) {
//                Toast.makeText(DeviceListActivity.this, anchorViewPosition+"---->"+position, Toast.LENGTH_SHORT).show();
                if (position == 1) {
//                	startActivity(new Intent(DeviceActivity.this, OprationManualActivity.class));
				}
            }
        }).show();
	}
    
    
	private void gotoTimerSettting(String hour, String minute, String R, String G, String B, String W, String Co2) {

		if (R.equalsIgnoreCase("-")) {
			R = "-1";
		}
		if (G.equalsIgnoreCase("-")) {
			G = "-1";
		}
		if (B.equalsIgnoreCase("-")) {
			B = "-1";
		}
		if (W.equalsIgnoreCase("-")) {
			W = "-1";
		}
		if (Co2.equalsIgnoreCase("-")) {
			Co2 = "-1";
		}
		
//		Toast.makeText(MainActivity.getMainActivity(), 
//									"R= "+R+",  "+
//									"G= "+G+",  "+
//									"B= "+B+",  "+
//									"W= "+W, Toast.LENGTH_SHORT).show();

		
		Intent intent = new Intent(getApplicationContext(), TimeRGBSetActivity.class);
		intent.putExtra("tag", TAG);
		intent.putExtra("hour", hour);
		intent.putExtra("minute", minute);
		intent.putExtra("R", R);
		intent.putExtra("G", G);
		intent.putExtra("B", B);
		intent.putExtra("W", W);
		intent.putExtra("Co2", Co2);

	    DeviceActivity.this.startActivityForResult(intent, INT_TIMER_SET);
	}
	
	
	// ********************** saveAndSendCMD / 保存并发送数据 *************************** //
	
	private void saveAndSendCMD() {
		
		ArrayList<TimeRGBW> timeRGBWArray = new ArrayList<TimeRGBW>();
		ArrayList<ParameterBean> pArray = new ArrayList<ParameterBean>();
		
		for (int i = 0; i < mp3s.size(); i++) {
			TimeRGBW timeRGBW = mp3s.get(i);
			
			String time[] = timeRGBW.getTime().split(":");
			
			if ( !timeRGBW.getR().equalsIgnoreCase("-") && !timeRGBW.getG().equalsIgnoreCase("-") && 
				 !timeRGBW.getB().equalsIgnoreCase("-") && !timeRGBW.getW().equalsIgnoreCase("-")) {
				
				if (Integer.parseInt(timeRGBW.getHour()) >= 0 || Integer.parseInt(timeRGBW.getMinute()) >= 0 || Integer.parseInt(timeRGBW.getR()) >= 0 || 
						Integer.parseInt(timeRGBW.getG()) >= 0 || Integer.parseInt(timeRGBW.getB()) >= 0  || Integer.parseInt(timeRGBW.getW()) >= 0  || 
						Integer.parseInt(timeRGBW.getCo2()) >= 0) {
					timeRGBWArray.add(timeRGBW);
				}					
			}									
		}
		
		if (timeRGBWArray.size() <= 0) {
			return;
		}
		
		ParameterBean p = null;
		for (int j = 0; j < timeRGBWArray.size(); j++) {
			
			TimeRGBW timeRGBW = timeRGBWArray.get(j);	
			
//			Toast.makeText(MainActivity.getMainActivity(), "R= "+timeRGBW.getR()+",  "+"G= "+timeRGBW.getG()+",  "+"B= "+timeRGBW.getB()+",  "+"W= "+timeRGBW.getW(), Toast.LENGTH_SHORT).show();
			
			if (j%2==0) {
				p = new ParameterBean();
			}										
			
			if (j%2==0) {
				p.setHour(Integer.parseInt(timeRGBW.getHour()));
				p.setMinute(Integer.parseInt(timeRGBW.getMinute()));
				p.setR(Integer.parseInt(timeRGBW.getR()));
				p.setG(Integer.parseInt(timeRGBW.getG()));
				p.setB(Integer.parseInt(timeRGBW.getB()));
				p.setW(Integer.parseInt(timeRGBW.getW()));
				p.setCo2(Integer.parseInt(timeRGBW.getCo2()));	
				
				if (j == timeRGBWArray.size()-1) {
					p.setHour1(255);
				}
				
			}else {									
				p.setHour1(Integer.parseInt(timeRGBW.getHour()));
				p.setMinute1(Integer.parseInt(timeRGBW.getMinute()));
				p.setR1(Integer.parseInt(timeRGBW.getR()));
				p.setG1(Integer.parseInt(timeRGBW.getG()));
				p.setB1(Integer.parseInt(timeRGBW.getB()));
				p.setW1(Integer.parseInt(timeRGBW.getW()));
				p.setCo21(Integer.parseInt(timeRGBW.getCo2()));

			}	
			
			if (j%2==0) {
				pArray.add(p);
			}
		}	
		
		int  totalHour = 0;
		
		for (int i = 0; i < pArray.size(); i++) {
			ParameterBean p1 = pArray.get(i);
			if (pArray.size() == 1) {
				if (i == pArray.size()-1) {
					p1.setHour(0);
				}	
				pArray.set(i, p1);
			}else {
//				if (i == pArray.size()-1) {
//					p1.setHour(24);
//				}
//				pArray.set(i, p1);
			}
			MainActivity.getMainActivity().setTimeDataWithCo2(p1);
			
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

//			Toast.makeText(MainActivity.getMainActivity(), "H="+p1.getHour()+",  "+"M="+p1.getMinute()+",  "+"R="+p1.getR()+",  "+"G="+p1.getG()+",  "+"B="+p1.getB()+",  "+"W="+p1.getW(), Toast.LENGTH_SHORT).show();
//			Toast.makeText(MainActivity.getMainActivity(), "H1="+p1.getHour1()+",  "+"M1="+p1.getMinute1()+",  "+"R1="+p1.getR1()+",  "+"G1="+p1.getG1()+",  "+"B1="+p1.getB1()+",  "+"W1="+p1.getW1(), Toast.LENGTH_SHORT).show();

			
			if (p1.getHour() < 24) {
				totalHour += (1<<p1.getHour());
			}
			if (p1.getHour1() < 24) {
				totalHour += (1<<p1.getHour1());
			}
			 
		}
		
		int  totalDay = (statusSUN<<0) + (statusMON<<1) + (statusTUE<<2)+ 
				(statusWED<<3)+ (statusTHU<<4)+ (statusFRI<<5)+ (statusSAT<<6);
		
//		Toast.makeText(MainActivity.getMainActivity(), "7= "+statusSUN+",  "+"1= "+statusMON+",  "+"2= "+statusTUE
//				+",  "+"3= "+statusWED+",  "+"4= "+statusTUE+",  "+"5= "+statusFRI+",  "+
//				"6= "+statusSAT, Toast.LENGTH_SHORT).show();
		
		Time t = new Time(); // or Time t=new Time("GMT+8"); 加上Time Zone资料。  
		t.setToNow(); // 取得系统时间。  
		
		MainActivity.getMainActivity().setTimeDataWithCo2End(totalDay, t.year, t.month, t.monthDay, t.hour, t.minute, t.second, totalHour);
		
	}
    
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK && requestCode == INT_TIMER_SET) {
			try {												
				
//				tag = data.getStringExtra("tag");
				int hour = data.getIntExtra("hour", -1);
				int minute = data.getIntExtra("minute", -1);
				int redValue = data.getIntExtra("R", -1);
				int greenValue = data.getIntExtra("G", -1);
				int blueValue = data.getIntExtra("B", -1);
				int whiteValue = data.getIntExtra("W", -1);
				int co2Value = data.getIntExtra("co2", -1);
				
				if (minute == -1 && redValue == -1 && greenValue == -1 && blueValue == -1 && whiteValue == -1 && co2Value == -1) {
					return;
				}
//				if (/*minute == -1 &&*/ redValue >=0 || greenValue >=0 || blueValue >=0 || whiteValue >=0 ) {
////					minute = 0;
//					redValue = 0;
//					greenValue = 0;
//					blueValue = 0;
//					whiteValue = 0;
//					co2Value = 0;
//				}
//				Toast.makeText(MainActivity.getMainActivity(), "minute= "+minute, Toast.LENGTH_SHORT).show();
				
//				Toast.makeText(DeviceActivity.this, "hour = "+hour+", "+"minute = "+hour+
//						", "+"redValue = "+redValue+", "+"greenValue = "+greenValue+", "+"blueValue = "+blueValue+", "+"whiteValue = "+whiteValue+
//						", "+"co2Value = "+co2Value, Toast.LENGTH_SHORT).show();
				
				TimeRGBW mp3 = new TimeRGBW();
				if (hour >= 0 && hour <= 9) {	
					if (minute >= 0 && minute <= 9) {
						mp3.setTime("0"+hour+":"+"0"+minute);
					}else {
						mp3.setTime("0"+hour+":"+minute);
					}								
				}else {
					if (minute >= 0 && minute <= 9) {
						mp3.setTime(hour+":"+"0"+minute);
					}else {
						mp3.setTime(hour+":"+minute);
					}
				}
				

				mp3.setMinute(String.valueOf(minute));
				mp3.setR(String.valueOf(redValue));
				mp3.setG(String.valueOf(greenValue));
				mp3.setB(String.valueOf(blueValue));
				mp3.setW(String.valueOf(whiteValue));
				mp3.setCo2(String.valueOf(co2Value));
				

				if (hour == 0 || hour == 24) {
					mp3s.set(hour, mp3);
					if (hour == 0) {
						for (int i = 0; i <= 24; i++) {
							if (24 == i) {
								TimeRGBW t24 = mp3s.get(i);
								t24.setR(String.valueOf(redValue));
								t24.setG(String.valueOf(greenValue));
								t24.setB(String.valueOf(blueValue));
								t24.setW(String.valueOf(whiteValue));
								t24.setCo2(String.valueOf(co2Value));
								SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(24)+TAG+deviceName, String.valueOf(24), String.valueOf(minute), String.valueOf(redValue),
										String.valueOf(greenValue), String.valueOf(blueValue), String.valueOf(whiteValue), String.valueOf(co2Value));
							}
						}
					}else if (24==hour) {
						for (int i = 0; i <= 24; i++) {
							if (0 == i) {
								TimeRGBW t0 = mp3s.get(i);
								t0.setR(String.valueOf(redValue));
								t0.setG(String.valueOf(greenValue));
								t0.setB(String.valueOf(blueValue));
								t0.setW(String.valueOf(whiteValue));
								t0.setCo2(String.valueOf(co2Value));
								SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(0)+TAG+deviceName, String.valueOf(0), String.valueOf(minute), String.valueOf(redValue),
										String.valueOf(greenValue), String.valueOf(blueValue), String.valueOf(whiteValue), String.valueOf(co2Value));
								
//								Toast.makeText(MainActivity.getMainActivity(), ""+tag, Toast.LENGTH_SHORT).show();
							}
						}
					}						
					
				}else {
					mp3s.set(hour, mp3);					
				}
				SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(positionTag)+TAG+deviceName, String.valueOf(hour), String.valueOf(minute), String.valueOf(redValue),
						String.valueOf(greenValue), String.valueOf(blueValue), String.valueOf(whiteValue), String.valueOf(co2Value));
				
				
				
				
				
				reloadModeData(TAG);
				
				timeDataAdapter.notifyDataSetChanged();
				generateRedLineValues();		
				
				
				//**** Save Co2 *****//
//				saveCo2Data(hour);
				
				
			} catch (Exception e) {
				e.printStackTrace();		
			}
		} 
	}
	
	
	
	public void saveData() {
		for (int i = 0; i <= 24; i++) {
			
			TimeRGBW t0 = mp3s.get(i);
			SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(i)+TAG+deviceName, String.valueOf(i), String.valueOf(t0.getMinute()), String.valueOf(t0.getR()),
						String.valueOf(t0.getG()), String.valueOf(t0.getB()), String.valueOf(t0.getW()), String.valueOf(t0.getCo2()));
			
		}	
	}
	
	public void saveCo2Data(int hour) {
		
		TimeRGBW t0 = mp3s.get(hour);
		
//		Toast.makeText(DeviceActivity.this, "hour = "+hour+", "+"minute = "+t0.getMinute()+
//		", "+"R = "+t0.getR()+", "+"G = "+t0.getG()+", "+"B = "+t0.getB()+", "+"W = "+t0.getW()+
//		", "+"co2 = "+t0.getCo2(), Toast.LENGTH_SHORT).show();
		
		for (int i = 0; i <= 23; i++) {
			
			TimeRGBW t1 = mp3s.get(i);									
			if (t0.getCo2().equalsIgnoreCase("1") && t1.getCo2().equalsIgnoreCase("1") && i <= hour) {
				for (int j = i; j <= hour; j++) {
					TimeRGBW t2 = mp3s.get(j);
					t2.setCo2("1");
					SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(j)+TAG+deviceName, String.valueOf(j), 
							String.valueOf(t2.getMinute()), String.valueOf(t2.getR()),
							String.valueOf(t2.getG()), String.valueOf(t2.getB()), 
							String.valueOf(t2.getW()), String.valueOf(t2.getCo2()));
				}
			}else if (t0.getCo2().equalsIgnoreCase("0") && t1.getCo2().equalsIgnoreCase("0") && i <= hour) {
				for (int j = i; j <= hour; j++) {
					TimeRGBW t2 = mp3s.get(j);
					t2.setCo2("0");
					SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(j)+TAG+deviceName, String.valueOf(j), 
							String.valueOf(t2.getMinute()), String.valueOf(t2.getR()),
							String.valueOf(t2.getG()), String.valueOf(t2.getB()), 
							String.valueOf(t2.getW()), String.valueOf(t2.getCo2()));
				}
			}
			
			if (t0.getCo2().equalsIgnoreCase("1") && t1.getCo2().equalsIgnoreCase("1") && i >= hour) {
				for (int j = hour; j <= i; j++) {
					TimeRGBW t2 = mp3s.get(j);
					t2.setCo2("1");
					SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(j)+TAG+deviceName, String.valueOf(j), 
							String.valueOf(t2.getMinute()), String.valueOf(t2.getR()),
							String.valueOf(t2.getG()), String.valueOf(t2.getB()), 
							String.valueOf(t2.getW()), String.valueOf(t2.getCo2()));
				}
			}else if (t0.getCo2().equalsIgnoreCase("0") && t1.getCo2().equalsIgnoreCase("0") && i >= hour) {
				for (int j = hour; j <= i; j++) {
					TimeRGBW t2 = mp3s.get(j);
					t2.setCo2("0");
					SharePersistent.saveTimerData(getApplicationContext(), String.valueOf(j)+TAG+deviceName, String.valueOf(j), 
							String.valueOf(t2.getMinute()), String.valueOf(t2.getR()),
							String.valueOf(t2.getG()), String.valueOf(t2.getB()), 
							String.valueOf(t2.getW()), String.valueOf(t2.getCo2()));
				}
			}	
			
//			Toast.makeText(DeviceActivity.this, "hour = "+i+", "+"minute = "+t1.getMinute()+
//					", "+"R = "+t1.getR()+", "+"G = "+t1.getG()+", "+"B = "+t1.getB()+", "+"W = "+t1.getW()+
//					", "+"co2 = "+t1.getCo2(), Toast.LENGTH_SHORT).show();
			
		}
	}
	
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
		if (handler != null) {
			handler.removeCallbacks(runnable);
			handler = null;
			runnable = null;
		}
	}

}
