package com.ledble.bean;

import com.common.bean.IBeanInterface;

public class TimeRGBW implements IBeanInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String time;
	private String hour;
	private String minute;
	private String R;
	private String G;
	private String B;
	private String W;
	private String Co2;
	private int duration;
	private long size;

	public TimeRGBW() {
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getHour() {
		return hour;
	}

	public void setHour(String hour) {
		this.hour = hour;
	}
	
	public String getMinute() {
		return minute;
	}

	public void setMinute(String minute) {
		this.minute = minute;
	}

	public String getR() {
		return R;
	}

	public void setR(String r) {
		this.R = r;
	}

	public String getG() {
		return G;
	}

	public void setG(String g) {
		this.G = g;
	}

	public String getB() {
		return B;
	}

	public void setB(String b) {
		this.B = b;
	}
	
	public String getW() {
		return W;
	}

	public void setW(String w) {
		this.W = w;
	}
	
	public String getCo2() {
		return Co2;
	}

	public void setCo2(String co2) {
		this.Co2 = co2;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
