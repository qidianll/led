package com.ledble.bean;

import com.common.bean.IBeanInterface;

public class ParameterBean implements IBeanInterface{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String time;
	private int hour;
	private int minute;
	private int R;
	private int G;
	private int B;
	private int W;
	private int Co2;
	private int hour1;
	private int minute1;
	private int R1;
	private int G1;
	private int B1;
	private int W1;
	private int Co21;
	private int duration;
	private long size;

	public ParameterBean() {
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		this.hour = hour;
	}
	
	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		this.minute = minute;
	}

	public int getR() {
		return R;
	}

	public void setR(int r) {
		this.R = r;
	}

	public int getG() {
		return G;
	}

	public void setG(int g) {
		this.G = g;
	}

	public int getB() {
		return B;
	}

	public void setB(int b) {
		this.B = b;
	}
	
	public int getW() {
		return W;
	}

	public void setW(int w) {
		this.W = w;
	}
	
	public int getCo2() {
		return Co2;
	}

	public void setCo2(int co2) {
		this.Co2 = co2;
	}
	
	public int getHour1() {
		return hour1;
	}

	public void setHour1(int hour) {
		this.hour1 = hour;
	}
	
	public int getMinute1() {
		return minute1;
	}

	public void setMinute1(int minute) {
		this.minute1 = minute;
	}

	public int getR1() {
		return R1;
	}

	public void setR1(int r) {
		this.R1 = r;
	}

	public int getG1() {
		return G1;
	}

	public void setG1(int g) {
		this.G1 = g;
	}

	public int getB1() {
		return B1;
	}

	public void setB1(int b) {
		this.B1 = b;
	}
	
	public int getW1() {
		return W1;
	}

	public void setW1(int w) {
		this.W1 = w;
	}
	
	public int getCo21() {
		return Co21;
	}

	public void setCo21(int co2) {
		this.Co21 = co2;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
