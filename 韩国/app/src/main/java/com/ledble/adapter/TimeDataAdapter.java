package com.ledble.adapter;

import java.util.ArrayList;
import java.util.HashSet;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aquariumstudio.R;
import com.ledble.bean.TimeRGBW;

public class TimeDataAdapter extends BaseAdapter {

	private ArrayList<TimeRGBW> list;
	private HashSet<TimeRGBW> selectSet;
	private Context context;
	private int index = -1;
	private OnSelectListener onSelectListener;

	public TimeDataAdapter(Context context, ArrayList<TimeRGBW> list) {
		this.list = list;
		this.context = context;
		this.selectSet = new HashSet<TimeRGBW>();
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return list.get(position).getId();
	}

	@SuppressLint("ResourceAsColor")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder vh = null;
		if (null == convertView) {
			vh = new ViewHolder();
			convertView = View.inflate(context, R.layout.item_time, null);
			vh.textViewTime = (TextView) convertView.findViewById(R.id.textViewTime);
			vh.textViewR = (TextView) convertView.findViewById(R.id.textViewR);
			vh.textViewG = (TextView) convertView.findViewById(R.id.textViewG);
			vh.textViewB = (TextView) convertView.findViewById(R.id.textViewB);
			vh.textViewW = (TextView) convertView.findViewById(R.id.textViewW);
			vh.textViewCo2 = (ImageView) convertView.findViewById(R.id.co2Select);
			vh.llco2Select = (LinearLayout) convertView.findViewById(R.id.llco2Select);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh = (ViewHolder) convertView.getTag();
		final TimeRGBW mp3 = list.get(position);
		vh.textViewTime.setText(mp3.getTime());
		vh.textViewR.setText((mp3.getR().equalsIgnoreCase("-1") ? "0" : mp3.getR()));
		vh.textViewG.setText(mp3.getG().equalsIgnoreCase("-1") ? "0" : mp3.getG());
		vh.textViewB.setText(mp3.getB().equalsIgnoreCase("-1") ? "0" : mp3.getB());
		vh.textViewW.setText(mp3.getW().equalsIgnoreCase("-1") ? "0" : mp3.getW());
		
		
		if (Integer.valueOf(mp3.getHour()) == 1 || Integer.valueOf(mp3.getHour()) == 3 || Integer.valueOf(mp3.getHour()) == 5 || 
				Integer.valueOf(mp3.getHour()) == 7 || Integer.valueOf(mp3.getHour()) == 9 || Integer.valueOf(mp3.getHour()) == 11
				 || Integer.valueOf(mp3.getHour()) == 13 || Integer.valueOf(mp3.getHour()) == 15 || Integer.valueOf(mp3.getHour()) == 17
				 || Integer.valueOf(mp3.getHour()) == 19 || Integer.valueOf(mp3.getHour()) == 21 || Integer.valueOf(mp3.getHour()) == 23) {
			vh.textViewTime.setBackgroundResource(R.color.mainColor);
			vh.textViewR.setBackgroundResource(R.color.mainColor);
			vh.textViewG.setBackgroundResource(R.color.mainColor);
			vh.textViewB.setBackgroundResource(R.color.mainColor);
			vh.textViewW.setBackgroundResource(R.color.mainColor);
			vh.textViewCo2.setBackgroundResource(R.color.mainColor);
			vh.llco2Select.setBackgroundResource(R.color.mainColor);
		}else {
			vh.textViewTime.setBackgroundResource(R.color.minorColor);
			vh.textViewR.setBackgroundResource(R.color.minorColor);
			vh.textViewG.setBackgroundResource(R.color.minorColor);
			vh.textViewB.setBackgroundResource(R.color.minorColor);
			vh.textViewW.setBackgroundResource(R.color.minorColor);
			vh.textViewCo2.setBackgroundResource(R.color.minorColor);
			vh.llco2Select.setBackgroundResource(R.color.minorColor);
		}
		
		if (Integer.valueOf(mp3.getHour()) <= 9) {
			
			if (Integer.valueOf(mp3.getMinute()) == 0) {
//				vh.textViewTime.setText(mp3.getHour());
				vh.textViewTime.setText("0"+mp3.getHour()+":00");
			}
			
		}else {
			if (Integer.valueOf(mp3.getMinute()) == 0) {
				vh.textViewTime.setText(mp3.getHour()+":00");
			}		
		}
		
		
		if (mp3.getCo2().equalsIgnoreCase("0")) {
			vh.textViewCo2.setBackgroundResource(R.drawable.co2_normal);
		}else if (mp3.getCo2().equalsIgnoreCase("1")) {
			vh.textViewCo2.setBackgroundResource(R.drawable.co2_check);
		}else {
			vh.textViewCo2.setBackgroundResource(R.drawable.co2_none);
		}
		

//		vh.checkBox.setOnCheckedChangeListener(null);
//		if (selectSet.contains(mp3)) {
//			vh.checkBox.setChecked(true);
//		} else {
//			vh.checkBox.setChecked(false);
//		}
//
//		vh.checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				if (null != onSelectListener) {
//					onSelectListener.onSelect(position, mp3, selectSet, isChecked, TimeDataAdapter.this);
//				}
//			}
//		});
		return convertView;
	}

	public void selectAll() {
		selectSet.addAll(list);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public HashSet<TimeRGBW> getSelectSet() {
		return selectSet;
	}

	public void setSelectSet(HashSet<TimeRGBW> selectSet) {
		this.selectSet = selectSet;
	}

	public OnSelectListener getOnSelectListener() {
		return onSelectListener;
	}

	public void setOnSelectListener(OnSelectListener onSelectListener) {
		this.onSelectListener = onSelectListener;
	}

	class ViewHolder {
		TextView textViewTime;
		TextView textViewR;
		TextView textViewG;
		TextView textViewB;
		TextView textViewW;
		ImageView textViewCo2;
		LinearLayout llco2Select;
//		CheckBox checkBox;
	}

	public static interface OnSelectListener {
		public void onSelect(int index, TimeRGBW mp3, HashSet<TimeRGBW> mp3s, boolean isCheck, BaseAdapter adapter);
	}
}
