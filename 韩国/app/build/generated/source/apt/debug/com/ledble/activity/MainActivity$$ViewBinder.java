// Generated code from Butter Knife. Do not modify!
package com.ledble.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends com.ledble.activity.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230998, "field 'segmentDm'");
    target.segmentDm = finder.castView(view, 2131230998, "field 'segmentDm'");
    view = finder.findRequiredView(source, 2131230997, "field 'segmentCt'");
    target.segmentCt = finder.castView(view, 2131230997, "field 'segmentCt'");
    view = finder.findRequiredView(source, 2131231001, "field 'segmentRgb'");
    target.segmentRgb = finder.castView(view, 2131231001, "field 'segmentRgb'");
    view = finder.findRequiredView(source, 2131230999, "field 'segmentMusic'");
    target.segmentMusic = finder.castView(view, 2131230999, "field 'segmentMusic'");
    view = finder.findRequiredView(source, 2131230853, "field 'ivType'");
    target.ivType = finder.castView(view, 2131230853, "field 'ivType'");
    view = finder.findRequiredView(source, 2131230957, "field 'rgBottom'");
    target.rgBottom = finder.castView(view, 2131230957, "field 'rgBottom'");
    view = finder.findRequiredView(source, 2131230935, "field 'rbFirst'");
    target.rbFirst = finder.castView(view, 2131230935, "field 'rbFirst'");
    view = finder.findRequiredView(source, 2131230944, "field 'rbSecond'");
    target.rbSecond = finder.castView(view, 2131230944, "field 'rbSecond'");
    view = finder.findRequiredView(source, 2131230945, "field 'rbThrid'");
    target.rbThrid = finder.castView(view, 2131230945, "field 'rbThrid'");
    view = finder.findRequiredView(source, 2131230936, "field 'rbFourth'");
    target.rbFourth = finder.castView(view, 2131230936, "field 'rbFourth'");
    view = finder.findRequiredView(source, 2131230934, "field 'rbFifth'");
    target.rbFifth = finder.castView(view, 2131230934, "field 'rbFifth'");
  }

  @Override public void unbind(T target) {
    target.segmentDm = null;
    target.segmentCt = null;
    target.segmentRgb = null;
    target.segmentMusic = null;
    target.ivType = null;
    target.rgBottom = null;
    target.rbFirst = null;
    target.rbSecond = null;
    target.rbThrid = null;
    target.rbFourth = null;
    target.rbFifth = null;
  }
}
