// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RgbFragment$$ViewBinder<T extends com.ledble.fragment.RgbFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230952, "field 'relativeTab1'");
    target.relativeTab1 = view;
    view = finder.findRequiredView(source, 2131230953, "field 'relativeTab2'");
    target.relativeTab2 = view;
    view = finder.findRequiredView(source, 2131230954, "field 'relativeTab3'");
    target.relativeTab3 = view;
    view = finder.findRequiredView(source, 2131230964, "field 'groupView'");
    target.groupView = view;
    view = finder.findRequiredView(source, 2131230869, "field 'linearLayoutPanoramaMode'");
    target.linearLayoutPanoramaMode = finder.castView(view, 2131230869, "field 'linearLayoutPanoramaMode'");
    view = finder.findRequiredView(source, 2131230966, "field 'rlOne'");
    target.rlOne = view;
    view = finder.findRequiredView(source, 2131231047, "field 'textViewGroupName'");
    target.textViewGroupName = finder.castView(view, 2131231047, "field 'textViewGroupName'");
    view = finder.findRequiredView(source, 2131231057, "field 'textViewMode'");
    target.textViewMode = finder.castView(view, 2131231057, "field 'textViewMode'");
    view = finder.findRequiredView(source, 2131231100, "field 'textViewWRGB'");
    target.textViewWRGB = finder.castView(view, 2131231100, "field 'textViewWRGB'");
    view = finder.findRequiredView(source, 2131231001, "field 'segmentRgb'");
    target.segmentRgb = finder.castView(view, 2131231001, "field 'segmentRgb'");
    view = finder.findRequiredView(source, 2131230996, "field 'seekBarWhite'");
    target.seekBarWhite = finder.castView(view, 2131230996, "field 'seekBarWhite'");
    view = finder.findRequiredView(source, 2131230992, "field 'seekBarRed'");
    target.seekBarRed = finder.castView(view, 2131230992, "field 'seekBarRed'");
    view = finder.findRequiredView(source, 2131230988, "field 'seekBarGreen'");
    target.seekBarGreen = finder.castView(view, 2131230988, "field 'seekBarGreen'");
    view = finder.findRequiredView(source, 2131230985, "field 'seekBarBlue'");
    target.seekBarBlue = finder.castView(view, 2131230985, "field 'seekBarBlue'");
    view = finder.findRequiredView(source, 2131230836, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230836, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131231039, "field 'textViewColorMode'");
    target.textViewColorMode = finder.castView(view, 2131231039, "field 'textViewColorMode'");
    view = finder.findRequiredView(source, 2131231073, "field 'textViewPanoramaMode'");
    target.textViewPanoramaMode = finder.castView(view, 2131231073, "field 'textViewPanoramaMode'");
    view = finder.findRequiredView(source, 2131230921, "field 'textViewPanoramaModeA'");
    target.textViewPanoramaModeA = finder.castView(view, 2131230921, "field 'textViewPanoramaModeA'");
    view = finder.findRequiredView(source, 2131230922, "field 'textViewPanoramaModeB'");
    target.textViewPanoramaModeB = finder.castView(view, 2131230922, "field 'textViewPanoramaModeB'");
    view = finder.findRequiredView(source, 2131230923, "field 'textViewPanoramaModeC'");
    target.textViewPanoramaModeC = finder.castView(view, 2131230923, "field 'textViewPanoramaModeC'");
    view = finder.findRequiredView(source, 2131230894, "field 'llPanoramaModeA'");
    target.llPanoramaModeA = finder.castView(view, 2131230894, "field 'llPanoramaModeA'");
    view = finder.findRequiredView(source, 2131230895, "field 'llPanoramaModeC'");
    target.llPanoramaModeC = finder.castView(view, 2131230895, "field 'llPanoramaModeC'");
    view = finder.findRequiredView(source, 2131230991, "field 'seekBarPanoramaSpeed'");
    target.seekBarPanoramaSpeed = finder.castView(view, 2131230991, "field 'seekBarPanoramaSpeed'");
    view = finder.findRequiredView(source, 2131230990, "field 'seekBarPanoramaLuman'");
    target.seekBarPanoramaLuman = finder.castView(view, 2131230990, "field 'seekBarPanoramaLuman'");
    view = finder.findRequiredView(source, 2131231106, "field 'timerView'");
    target.timerView = finder.castView(view, 2131231106, "field 'timerView'");
    view = finder.findRequiredView(source, 2131231136, "field 'tvTimer'");
    target.tvTimer = finder.castView(view, 2131231136, "field 'tvTimer'");
    view = finder.findRequiredView(source, 2131230843, "field 'imageViewTimer'");
    target.imageViewTimer = finder.castView(view, 2131230843, "field 'imageViewTimer'");
  }

  @Override public void unbind(T target) {
    target.relativeTab1 = null;
    target.relativeTab2 = null;
    target.relativeTab3 = null;
    target.groupView = null;
    target.linearLayoutPanoramaMode = null;
    target.rlOne = null;
    target.textViewGroupName = null;
    target.textViewMode = null;
    target.textViewWRGB = null;
    target.segmentRgb = null;
    target.seekBarWhite = null;
    target.seekBarRed = null;
    target.seekBarGreen = null;
    target.seekBarBlue = null;
    target.imageViewOnOff = null;
    target.textViewColorMode = null;
    target.textViewPanoramaMode = null;
    target.textViewPanoramaModeA = null;
    target.textViewPanoramaModeB = null;
    target.textViewPanoramaModeC = null;
    target.llPanoramaModeA = null;
    target.llPanoramaModeC = null;
    target.seekBarPanoramaSpeed = null;
    target.seekBarPanoramaLuman = null;
    target.timerView = null;
    target.tvTimer = null;
    target.imageViewTimer = null;
  }
}
