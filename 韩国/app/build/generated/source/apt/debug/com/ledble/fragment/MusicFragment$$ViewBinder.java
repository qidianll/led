// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MusicFragment$$ViewBinder<T extends com.ledble.fragment.MusicFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131231002, "field 'segment'");
    target.segment = finder.castView(view, 2131231002, "field 'segment'");
    view = finder.findRequiredView(source, 2131230841, "field 'imageViewPre'");
    target.imageViewPre = finder.castView(view, 2131230841, "field 'imageViewPre'");
    view = finder.findRequiredView(source, 2131230839, "field 'imageViewPlay'");
    target.imageViewPlay = finder.castView(view, 2131230839, "field 'imageViewPlay'");
    view = finder.findRequiredView(source, 2131230835, "field 'imageViewNext'");
    target.imageViewNext = finder.castView(view, 2131230835, "field 'imageViewNext'");
    view = finder.findRequiredView(source, 2131230832, "field 'imageViewEdit'");
    target.imageViewEdit = finder.castView(view, 2131230832, "field 'imageViewEdit'");
    view = finder.findRequiredView(source, 2131231032, "field 'textViewAutoAjust'");
    target.textViewAutoAjust = finder.castView(view, 2131231032, "field 'textViewAutoAjust'");
    view = finder.findRequiredView(source, 2131231125, "field 'tvCurrentTime'");
    target.tvCurrentTime = finder.castView(view, 2131231125, "field 'tvCurrentTime'");
    view = finder.findRequiredView(source, 2131231138, "field 'tvTotalTime'");
    target.tvTotalTime = finder.castView(view, 2131231138, "field 'tvTotalTime'");
    view = finder.findRequiredView(source, 2131230891, "field 'llDecibel'");
    target.llDecibel = finder.castView(view, 2131230891, "field 'llDecibel'");
    view = finder.findRequiredView(source, 2131230987, "field 'seekBarDecibel'");
    target.seekBarDecibel = finder.castView(view, 2131230987, "field 'seekBarDecibel'");
    view = finder.findRequiredView(source, 2131231126, "field 'tvDecibelValue'");
    target.tvDecibelValue = finder.castView(view, 2131231126, "field 'tvDecibelValue'");
    view = finder.findRequiredView(source, 2131230888, "field 'llBottom'");
    target.llBottom = finder.castView(view, 2131230888, "field 'llBottom'");
    view = finder.findRequiredView(source, 2131230989, "field 'seekBarMusic'");
    target.seekBarMusic = finder.castView(view, 2131230989, "field 'seekBarMusic'");
    view = finder.findRequiredView(source, 2131230842, "field 'imageViewRotate'");
    target.imageViewRotate = finder.castView(view, 2131230842, "field 'imageViewRotate'");
    view = finder.findRequiredView(source, 2131230840, "field 'imageViewPlayType'");
    target.imageViewPlayType = finder.castView(view, 2131230840, "field 'imageViewPlayType'");
    view = finder.findRequiredView(source, 2131230771, "field 'buttonMusicLib'");
    target.buttonMusicLib = finder.castView(view, 2131230771, "field 'buttonMusicLib'");
    view = finder.findRequiredView(source, 2131230994, "field 'seekBarRhythm'");
    target.seekBarRhythm = finder.castView(view, 2131230994, "field 'seekBarRhythm'");
    view = finder.findRequiredView(source, 2131231131, "field 'tvRhythm'");
    target.tvRhythm = finder.castView(view, 2131231131, "field 'tvRhythm'");
    view = finder.findRequiredView(source, 2131231139, "field 'tvrhythmValue'");
    target.tvrhythmValue = finder.castView(view, 2131231139, "field 'tvrhythmValue'");
    view = finder.findRequiredView(source, 2131231161, "field 'volumCircleBar'");
    target.volumCircleBar = finder.castView(view, 2131231161, "field 'volumCircleBar'");
  }

  @Override public void unbind(T target) {
    target.segment = null;
    target.imageViewPre = null;
    target.imageViewPlay = null;
    target.imageViewNext = null;
    target.imageViewEdit = null;
    target.textViewAutoAjust = null;
    target.tvCurrentTime = null;
    target.tvTotalTime = null;
    target.llDecibel = null;
    target.seekBarDecibel = null;
    target.tvDecibelValue = null;
    target.llBottom = null;
    target.seekBarMusic = null;
    target.imageViewRotate = null;
    target.imageViewPlayType = null;
    target.buttonMusicLib = null;
    target.seekBarRhythm = null;
    target.tvRhythm = null;
    target.tvrhythmValue = null;
    target.volumCircleBar = null;
  }
}
