// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TimerFragment$$ViewBinder<T extends com.ledble.fragment.TimerFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230872, "field 'linearLayoutTimerOn'");
    target.linearLayoutTimerOn = finder.castView(view, 2131230872, "field 'linearLayoutTimerOn'");
    view = finder.findRequiredView(source, 2131230871, "field 'linearLayoutTimerOff'");
    target.linearLayoutTimerOff = finder.castView(view, 2131230871, "field 'linearLayoutTimerOff'");
    view = finder.findRequiredView(source, 2131231050, "field 'textViewHourOnTime'");
    target.textViewHourOnTime = finder.castView(view, 2131231050, "field 'textViewHourOnTime'");
    view = finder.findRequiredView(source, 2131231056, "field 'textViewMinuteOnTime'");
    target.textViewMinuteOnTime = finder.castView(view, 2131231056, "field 'textViewMinuteOnTime'");
    view = finder.findRequiredView(source, 2131231049, "field 'textViewHourOffTime'");
    target.textViewHourOffTime = finder.castView(view, 2131231049, "field 'textViewHourOffTime'");
    view = finder.findRequiredView(source, 2131231055, "field 'textViewMinuteOffTime'");
    target.textViewMinuteOffTime = finder.castView(view, 2131231055, "field 'textViewMinuteOffTime'");
    view = finder.findRequiredView(source, 2131231058, "field 'textViewModelText'");
    target.textViewModelText = finder.castView(view, 2131231058, "field 'textViewModelText'");
    view = finder.findRequiredView(source, 2131231119, "field 'toggleButtonOn'");
    target.toggleButtonOn = finder.castView(view, 2131231119, "field 'toggleButtonOn'");
    view = finder.findRequiredView(source, 2131231118, "field 'toggleButtonOff'");
    target.toggleButtonOff = finder.castView(view, 2131231118, "field 'toggleButtonOff'");
    view = finder.findRequiredView(source, 2131231128, "field 'tvMode'");
    target.tvMode = finder.castView(view, 2131231128, "field 'tvMode'");
    view = finder.findRequiredView(source, 2131230878, "field 'listViewH'");
    target.listViewH = finder.castView(view, 2131230878, "field 'listViewH'");
    view = finder.findRequiredView(source, 2131230879, "field 'listViewM'");
    target.listViewM = finder.castView(view, 2131230879, "field 'listViewM'");
  }

  @Override public void unbind(T target) {
    target.linearLayoutTimerOn = null;
    target.linearLayoutTimerOff = null;
    target.textViewHourOnTime = null;
    target.textViewMinuteOnTime = null;
    target.textViewHourOffTime = null;
    target.textViewMinuteOffTime = null;
    target.textViewModelText = null;
    target.toggleButtonOn = null;
    target.toggleButtonOff = null;
    target.tvMode = null;
    target.listViewH = null;
    target.listViewM = null;
  }
}
