package com.ledble.activity;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;

import com.common.uitl.ListUtiles;
import com.common.uitl.LogUtil;
import com.common.uitl.SharePersistent;
import com.common.uitl.StringUtils;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledsmart.R;
import com.ledble.base.LedBleActivity;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.MyColor;
import com.ledble.constant.Constant;
import com.ledble.db.Group;
import com.ledble.db.GroupDevice;
import com.ledble.db.GroupDeviceDao;
import com.ledble.fragment.BrightFragment;
import com.ledble.fragment.CtFragment;
import com.ledble.fragment.CustomFragment;
import com.ledble.fragment.DmFragment;
import com.ledble.fragment.RgbFragment;
import com.ledble.fragment.SceneFragment;
import com.ledble.fragment.TimerFragment1;
import com.ledble.net.NetConnectBle;
import com.ledble.net.NetExceptionInterface;
import com.ledble.service.BluetoothLeServiceSingle;
import com.ledble.service.MyServiceConenction;
import com.ledble.service.MyServiceConenction.ServiceConnectListener;
import com.ledble.utils.ManageFragment;
import com.ledble.view.ActionSheet;
import com.ledble.view.ActionSheet.ActionSheetListener;
import com.ledble.view.ActionSheet.Item;
import com.ledble.view.GroupView;
import com.ledble.view.SlideSwitch;
import com.ledble.view.SlideSwitch.SlideListener;
import com.ledble.activity.OprationManualActivity;

@SuppressLint("NewApi")
public class MainActivity extends LedBleActivity implements NetExceptionInterface, SensorEventListener, ActionSheetListener {

    private static final String TAG = "MainActivity";
    @Bind(R.id.segmentDm)
    SegmentedRadioGroup segmentDm;
    @Bind(R.id.segmentCt)
    SegmentedRadioGroup segmentCt;
    @Bind(R.id.segmentRgb)
    SegmentedRadioGroup segmentRgb;
    @Bind(R.id.segmentMusic)
    SegmentedRadioGroup segmentMusic;
    @Bind(R.id.brightnessToggleButton)
    ImageView brightnessToggleButton;

    @Bind(R.id.ivLeftMenu)
    ImageView ivLeftMenu;
    @Bind(R.id.textViewConnectCount)
    TextView textViewConnectCount;
    @Bind(R.id.ivRightMenu)
    ImageView ivRightMenu;
    @Bind(R.id.ivType)
    ImageView ivType;
    @Bind(R.id.rgBottom)
    RadioGroup rgBottom;
    @Bind(R.id.rbFirst)
    RadioButton rbFirst;
    @Bind(R.id.rbSecond)
    RadioButton rbSecond;
    @Bind(R.id.rbThrid)
    RadioButton rbThrid;
    @Bind(R.id.rbFourth)
    RadioButton rbFourth;
    @Bind(R.id.rbFifth)
    RadioButton rbFifth;

    @Bind(R.id.llMenu)
    LinearLayout avtivity_main;
    @Bind(R.id.menu_content_layout)
    LinearLayout left_menu;
    @Bind(R.id.right_menu_frame)
    ScrollView right_menu;
    @Bind(R.id.linearLayoutTopItem)
    LinearLayout TopItem;
    @Bind(R.id.shakeView)
    RelativeLayout shakeView;

    private int currentIndex;
    private FragmentManager fragmentManager;
    private List<Fragment> fragmentList = new ArrayList<Fragment>();
    private DrawerLayout mDrawerLayout;

    public boolean isLightOpen = true;
    private int onOffStatus = 1;
    public int speed = 1;
    private String speedKey = "speedkey";
    public int brightness = 1;
    private String brightnessKey = "brightnesskey";
    private String groupName = "";
    private GroupView groupView;
    private SharedPreferences sp;
    private Editor editor;
    private BrightFragment brightFragment;
    private TimerFragment1 timerFragment;
    private BluetoothLeServiceSingle mBluetoothLeService;
    private MyServiceConenction myServiceConenction;
    private LinearLayout linearGroups;

    private static MainActivity mActivity;

    // left_menu
    private BluetoothManager bluetoothManager;
    private static BluetoothAdapter mBluetoothAdapter;
    private BluetoothLeScanner bleScanner;
    private Handler mHandler;
    private View refreshView;
    private Button buttonAddGroup, buttonAllOn, buttonAllOff;

    // right_menu
    private TextView operationguideTV, resetTV, changePicTV, dynamicTV, shakeTV, timesetTV, fanTV;
    private ImageView gradientIV, breathIV, jumpIV, strobeIV;
    private ImageView shakeColorIV, shakeNoneIV, shakeModelIV;
    private TextView timerheckTV, currentDataCheckTV, textViewRGBSort;
    private Random random = new Random();
    private SensorManager sensorManager;
    private SoundPool soundPool;
    private int soundID;
    private int shakeStyle = 1;

    private static Bitmap bm;
    private ImageView imageView = null;
    private static final int TAKE_PICTURE = 0; //
    private static final int CHOOSE_PICTURE = 1;

    private final int MSG_START_CONNECT = 10000;// 开始连接
    private TextView textViewAllDeviceIndicater;// 显示所有设备
    private static final long SCAN_PERIOD = 3000;
    private static final int REQUEST_ENABLE_BT = 1;
    private boolean isInitGroup = false;
    private boolean isAllOn = true;

    private volatile HashMap<String, Boolean> hashMapLock;// service连接锁
    private volatile HashMap<String, Boolean> hashMapConnect;
    private Map<String, SlideSwitch> map = new HashMap<>();
    private ArrayList<GroupView> arrayListGroupViews;

    private Dialog lDialog;

    private int LOCATION_CODE = 110;
    private int STORAGE_CODE = 112;
    private ScanCallback scanCallback;

    private int INT_GO_LIST = 111; // 设备列表
    private int INT_GO_COLORMODE = 112; //颜色模式选择
    private int GPS_REQUEST_CODE = 10;
    private boolean isGPSOpen = false;
    private static int OPEN_BLE = 333;


    @SuppressLint("NewApi")
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        mActivity = this;

        if (Build.VERSION.SDK_INT >= 21) {
            View decorView = getWindow().getDecorView();
            int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
            decorView.setSystemUiVisibility(option);
            getWindow().setStatusBarColor(Color.TRANSPARENT); //透明 状态栏
        }
        initScanCallback(); //6.0以上系统
        initFragment();
        initSlidingMenu();
        initView();
        refresh();
        if (getImagePath() != "") { // 显示保存的皮肤
            showImage(getImagePath());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // super.onSaveInstanceState(outState);
    }

    /**
     *
     */
    protected void refresh() {

        final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
        Runnable mRunnable = new Runnable() {
            @Override
            public void run() {
                // TODO Auto-generated method stub
                // 在此处添加执行的代码
                turnOnBluetooth();
                mHandler.removeCallbacks(this);
            }
        };
        mHandler.postDelayed(mRunnable, 800);// 打开定时器，执行操作
    }

    /**
     * 强制开启当前 Android 设备的 Bluetooth
     *
     * @return true：强制打开 Bluetooth　成功　false：强制打开 Bluetooth 失败
     */
    public static void turnOnBluetooth() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            //提示是否打开  带弹窗提醒
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mActivity.startActivityForResult(intent, OPEN_BLE);
        }
    }

    public MainActivity() {
        mActivity = this;
    }

    public static MainActivity getMainActivity() {
        return mActivity;
    }


    /**
     * 打开权限提醒
     *
     * @param context
     */

    private void checkAuthorization() {
        final TextView editText = new TextView(mActivity);
        editText.setText(getResources().getString(R.string.authorization_alert));
        new AlertDialog.Builder(mActivity).setTitle(R.string.authorization_title).setView(editText).setIcon(R.drawable.ic_launcher)
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        openGPSSettings();
                    }
                }).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    /**
     * 检测GPS是否打开
     *
     * @return
     */
    private boolean checkGPSIsOpen() {
        LocationManager locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        // 通过GPS卫星定位，定位级别可以精确到街（通过24颗卫星定位，在室外和空旷的地方定位准确、速度快）  
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // 通过WLAN或移动网络(3G/2G)确定的位置（也称作AGPS，辅助GPS定位。主要用于在室内或遮盖物（建筑群或茂密的深林等）密集的地方定位）  
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (gps || network) {
            return true;
        }
        return false;
    }

    /**
     * 跳转GPS设置
     */
    private void openGPSSettings() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.notifyTitle)
                .setMessage(R.string.gpsNotifyMsg)
                // 拒绝, 退出应用
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        })

                .setPositiveButton(R.string.setting,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //跳转GPS设置界面
                                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivityForResult(intent, GPS_REQUEST_CODE);
                                isGPSOpen = true;
                            }
                        })

                .setCancelable(false)
                .show();
    }

    private void initScanCallback() {
        if (Build.VERSION.SDK_INT >= 23) {
            scanCallback = new ScanCallback() {
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
//		            super.onScanResult(callbackType, result);
                    BluetoothDevice device = result.getDevice();
                    int rssi = result.getRssi();//获取rssi
                    //这里写你自己的逻辑
//		            Toast.makeText(mActivity, "deviceName= "+device.getName(), Toast.LENGTH_SHORT).show(); 	            
                    if (!LedBleApplication.getApp().getBleDevices().contains(device) && device.getName() != null) {
                        String name = device.getName();

                        if (/*name.startsWith(BluetoothLeServiceSingle.NAME_START_ELK) ||*/ name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)) {

                            LedBleApplication.getApp().getBleDevices().add(device);
                            updateNewFindDevice();

                            LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
                                    + LedBleApplication.getApp().getBleDevices().size());
                            conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了
                        }
                    }
                }
            };
        }
    }

    private void initView() {
        mActivity.registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        // 是否支持蓝牙
        if (!mActivity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(mActivity, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
            Tool.exitApp();
        }

        bluetoothManager = (BluetoothManager) mActivity.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
        if (Build.VERSION.SDK_INT >= 23) {//安卓6.0以上的方案
            bleScanner = mBluetoothAdapter.getBluetoothLeScanner();//用过单例的方式获取实例
        }
        // 不能获得蓝牙设备支持
        if (mBluetoothAdapter == null) {
            Tool.ToastShow(mActivity, R.string.ble_not_supported);
            Tool.exitApp();
        }

        mHandler = new Handler();
        hashMapLock = new HashMap<String, Boolean>();
        hashMapConnect = new HashMap<String, Boolean>();
        arrayListGroupViews = new ArrayList<GroupView>();

        //右菜单  各个标签项
        changePicTV = (TextView) mActivity.findViewById(R.id.change_under_pic_tv);
        changePicTV.setOnClickListener(new MyOnClickListener());
        resetTV = (TextView) mActivity.findViewById(R.id.reset_tv);
        resetTV.setOnClickListener(new MyOnClickListener());
        shakeTV = (TextView) mActivity.findViewById(R.id.shake_tv);
        shakeTV.setOnClickListener(new MyOnClickListener());
        dynamicTV = (TextView) mActivity.findViewById(R.id.dynamic_tv);
        dynamicTV.setOnClickListener(new MyOnClickListener());

        //右菜单   律动各项
        gradientIV = (ImageView) mActivity.findViewById(R.id.dynamic_gradient_iv);
        gradientIV.setOnClickListener(new MyOnClickListener());
        breathIV = (ImageView) mActivity.findViewById(R.id.dynamic_breath_iv);
        breathIV.setOnClickListener(new MyOnClickListener());
        jumpIV = (ImageView) mActivity.findViewById(R.id.dynamic_jump_iv);
        jumpIV.setOnClickListener(new MyOnClickListener());
        strobeIV = (ImageView) mActivity.findViewById(R.id.dynamic_strobe_iv);
        strobeIV.setOnClickListener(new MyOnClickListener());

        timesetTV = (TextView) mActivity.findViewById(R.id.other_set_tv);
        timesetTV.setOnClickListener(new MyOnClickListener());
        fanTV = (TextView) mActivity.findViewById(R.id.fan_rotational_temperature_tv);
        fanTV.setOnClickListener(new MyOnClickListener());

        timerheckTV = (TextView) mActivity.findViewById(R.id.tv_timer_check);
        timerheckTV.setOnClickListener(new MyOnClickListener());
        currentDataCheckTV = (TextView) mActivity.findViewById(R.id.tv_current_data_check);
        currentDataCheckTV.setOnClickListener(new MyOnClickListener());
        textViewRGBSort = (TextView) mActivity.findViewById(R.id.textViewRGBSort);
        textViewRGBSort.setOnClickListener(new MyOnClickListener());

        operationguideTV = (TextView) mActivity.findViewById(R.id.operation_guide_tv);
        operationguideTV.setOnClickListener(new MyOnClickListener());

        // 右菜单 摇一摇
        shakeColorIV = (ImageView) mActivity.findViewById(R.id.shake_one_iv);
        shakeColorIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundResource(R.drawable.bg_in_press);
                shakeNoneIV.setBackgroundResource(R.drawable.bg_shake_green);
                shakeModelIV.setBackgroundResource(R.drawable.bg_shake_green);
                shakeView.setBackgroundResource(R.drawable.bg_shake_green);
                shakeStyle = 0;
            }
        });

        shakeNoneIV = (ImageView) mActivity.findViewById(R.id.shake_two_iv);
        shakeNoneIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundResource(R.drawable.bg_in_press);
                shakeColorIV.setBackgroundResource(R.drawable.bg_shake_lightgray);
                shakeModelIV.setBackgroundResource(R.drawable.bg_shake_lightgray);
                shakeView.setBackgroundResource(R.drawable.bg_shake_lightgray);
                shakeStyle = 1;
            }
        });

        shakeModelIV = (ImageView) mActivity.findViewById(R.id.shake_three_iv);
        shakeModelIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                view.setBackgroundResource(R.drawable.bg_in_press);
                shakeColorIV.setBackgroundResource(R.drawable.bg_shake_orange);
                shakeNoneIV.setBackgroundResource(R.drawable.bg_shake_orange);
                shakeView.setBackgroundResource(R.drawable.bg_shake_orange);
                shakeStyle = 2;
            }
        });

        shakeNoneIV.performClick();// 默认第一个点击


        //左侧   菜单
        imageView = (ImageView) mActivity.findViewById(R.id.activity_main_imageview);
        linearGroups = (LinearLayout) mActivity.findViewById(R.id.linearLayoutDefineGroups);
        textViewAllDeviceIndicater = (TextView) mActivity.findViewById(R.id.textViewAllDeviceIndicater);
        TopItem.setOnClickListener(new MyOnClickListener());

        buttonAllOff = (Button) mActivity.findViewById(R.id.buttonAllOff);
        buttonAllOff.setOnClickListener(new MyOnClickListener());

        buttonAllOn = (Button) mActivity.findViewById(R.id.buttonAllOn);
        buttonAllOn.setOnClickListener(new MyOnClickListener());

        buttonAddGroup = (Button) mActivity.findViewById(R.id.buttonAddGroup);
        buttonAddGroup.setOnClickListener(new MyOnClickListener());

        refreshView = (View) mActivity.findViewById(R.id.ivRefresh);
        refreshView.setOnClickListener(new MyOnClickListener());

        // 摇一摇
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { /* 新版 */
            AudioAttributes attributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION).build();
            soundPool = new SoundPool.Builder().setAudioAttributes(attributes).build();
        } else {
            soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 1);
        }
        soundID = soundPool.load(this, R.raw.dang, 1);

        // 初始tab项
        sp = getSharedPreferences(Constant.MODLE_TYPE, Context.MODE_PRIVATE);
        editor = sp.edit();
        currentIndex = sp.getInt(Constant.MODLE_VALUE, 2);

        // ====连接后台service
        myServiceConenction = new MyServiceConenction();
        myServiceConenction.setServiceConnectListener(new ServiceConnectListener() {
            @Override
            public void onConnect(ComponentName name, IBinder service, BluetoothLeServiceSingle bLeService) {
                mBluetoothLeService = bLeService;// 获取连接实例
                // leftMenuFragment.setService(mBluetoothLeService);
                if (!mBluetoothLeService.initialize()) {
                    Log.e(LedBleApplication.tag, "Unable to initialize Bluetooth");
                } else {
                    Log.e(LedBleApplication.tag, "Initialize Bluetooth");
                }
            }

            @Override
            public void onDisConnect(ComponentName name) {

            }
        });

        Intent gattServiceIntent = new Intent(this, BluetoothLeServiceSingle.class);
        bindService(gattServiceIntent, myServiceConenction, Activity.BIND_AUTO_CREATE);

        ivType.setImageResource(R.drawable.tab_dim_check);
        ivType.setOnClickListener(new MyOnClickListener());
        ivLeftMenu.setOnClickListener(new MyOnClickListener());
        ivRightMenu.setOnClickListener(new MyOnClickListener());

        brightnessToggleButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                switch (onOffStatus) {
                    case 0:
                        brightnessToggleButton.setImageResource(R.drawable.on_btn);
                        mActivity.open();

                        onOffStatus = 1;
                        break;
                    case 1:
                        brightnessToggleButton.setImageResource(R.drawable.off_btn);
                        mActivity.close();
                        onOffStatus = 0;
                        break;

                    default:
                        break;
                }
            }
        });

        rgBottom.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbFirst:
                        currentIndex = 0;
                        segmentDm.setVisibility(View.VISIBLE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.GONE);
                        rbFirst.setVisibility(View.VISIBLE);
                        rbSecond.setVisibility(View.GONE);
                        rbThrid.setVisibility(View.GONE);
                        editor.putInt(Constant.MODLE_VALUE, currentIndex);
                        editor.commit();
                        break;
                    case R.id.rbSecond:
                        currentIndex = 1;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.VISIBLE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.GONE);
                        rbFirst.setVisibility(View.GONE);
                        rbSecond.setVisibility(View.VISIBLE);
                        rbThrid.setVisibility(View.GONE);
                        editor.putInt(Constant.MODLE_VALUE, currentIndex);
                        editor.commit();
                        break;
                    case R.id.rbThrid:
                        currentIndex = 2;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.VISIBLE);
                        segmentMusic.setVisibility(View.GONE);
                        rbFirst.setVisibility(View.GONE);
                        rbSecond.setVisibility(View.GONE);
                        rbThrid.setVisibility(View.VISIBLE);
                        brightnessToggleButton.setVisibility(View.GONE);
                        editor.putInt(Constant.MODLE_VALUE, currentIndex);
                        editor.commit();
                        break;
                    case R.id.rbFourth:
                        currentIndex = 3;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.INVISIBLE);
                        brightnessToggleButton.setVisibility(View.VISIBLE);
                        brightFragment.setActive();
                        break;
                    case R.id.rbSix:
                        currentIndex = 4;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.GONE);
                        brightnessToggleButton.setVisibility(View.GONE);
                        timerFragment.setActive();
                        break;
                    case R.id.rbSeven:
                        currentIndex = 5;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.GONE);
                        brightnessToggleButton.setVisibility(View.GONE);
                        timerFragment.setActive();
                        break;
                    case R.id.rbFifth:
                        currentIndex = 6;
                        segmentDm.setVisibility(View.GONE);
                        segmentCt.setVisibility(View.GONE);
                        segmentRgb.setVisibility(View.GONE);
                        segmentMusic.setVisibility(View.GONE);
                        ivType.setVisibility(View.INVISIBLE);
                        brightnessToggleButton.setVisibility(View.GONE);
                        timerFragment.setActive();
                        break;

                }
                ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
            }
        });

        if (currentIndex == 0) {
            ivType.setImageResource(R.drawable.tab_dim_check);
            rgBottom.check(R.id.rbFirst);
        } else if (currentIndex == 2) {
            ivType.setImageResource(R.drawable.tab_reg_check);
            rgBottom.check(R.id.rbThrid);
        }

        brightness = SharePersistent.getInt(this, brightnessKey);
        speed = SharePersistent.getInt(this, speedKey);
        if (mBluetoothAdapter.isEnabled()) {
            initBleScanList(isAllOn);
        }
    }

    /**
     * 初始化滑动菜单
     */
    private void initSlidingMenu() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
    }

    /**
     * 初始化fragment
     */
    public void initFragment() {
        fragmentList.add(new DmFragment());
        fragmentList.add(new CtFragment());
        fragmentList.add(new RgbFragment());
        brightFragment = new BrightFragment();
        fragmentList.add(brightFragment);

        fragmentList.add(new SceneFragment());
        fragmentList.add(new CustomFragment());

        timerFragment = new TimerFragment1();
        fragmentList.add(timerFragment);
        // 添加Fragment
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        for (int i = 0; i < fragmentList.size(); i++) {
            transaction.add(R.id.flContent, fragmentList.get(i), fragmentList.get(i).getClass().getSimpleName());
        }
        transaction.commit();
        ManageFragment.showFragment(fragmentManager, fragmentList, currentIndex);
    }

    public class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivLeftMenu:
                    mDrawerLayout.openDrawer(left_menu);
                    break;
                case R.id.ivRightMenu:
                    mDrawerLayout.openDrawer(right_menu);
                    break;
                case R.id.linearLayoutTopItem:
                    showActionSheet("", null);
                    break;
                case R.id.ivType:
                    if (currentIndex == 0) {
                        ivType.setImageResource(R.drawable.tab_reg_check);
                        rgBottom.check(R.id.rbThrid);
                    } else if (currentIndex == 2) {
                        ivType.setImageResource(R.drawable.tab_dim_check);
                        rgBottom.check(R.id.rbFirst);
                    }
                    break;

                case R.id.brightnessToggleButton:
                    allOff();
                    break;
                case R.id.buttonAllOff:
                    allOff();
                    break;
                case R.id.buttonAllOn:
                    allOn();
                    break;
                case R.id.buttonAddGroup:
                    addGroupMessage(false, "");
                    break;
                case R.id.ivRefresh:// 刷新
                    refreshDevices(true);
                    break;

                case R.id.dynamic_tv:// 律动
                    startActivity(new Intent(mActivity, DynamicColorActivity.class));
                    break;
                case R.id.dynamic_gradient_iv: // 律动  渐变
                    setDynamicModel(128);
                    Toast.makeText(mActivity, R.string.gradient, Toast.LENGTH_SHORT).show();
                    break;
                case R.id.dynamic_breath_iv: // 渐变	呼吸
                    setDynamicModel(129);
                    Toast.makeText(mActivity, R.string.breathe, Toast.LENGTH_SHORT).show();
                    break;
                case R.id.dynamic_jump_iv: // 律动	跳变
                    setDynamicModel(130);
                    Toast.makeText(mActivity, R.string.jump, Toast.LENGTH_SHORT).show();
                    break;
                case R.id.dynamic_strobe_iv: // 律动 	频闪
                    setDynamicModel(131);
                    Toast.makeText(mActivity, R.string.flash, Toast.LENGTH_SHORT).show();
                    break;
                case R.id.other_set_tv:// 时间设置
                    gotoTimerSettting("time", 11);
                    break;
                case R.id.fan_rotational_temperature_tv:// 风扇
                    gotoTimerSettting("fan", 12);
                    break;
                case R.id.shake_one_iv:// 摇一摇	颜色
                    Toast.makeText(mActivity, R.string.shake_random_color, Toast.LENGTH_SHORT).show();
                    shakeStyle = 0;
                    break;
                case R.id.shake_two_iv:// 摇一摇	NONE
                    Toast.makeText(mActivity, R.string.shake_random_none, Toast.LENGTH_SHORT).show();
                    shakeStyle = 1;
                    break;
                case R.id.shake_three_iv:// 摇一摇		模式
                    Toast.makeText(mActivity, R.string.shake_random_mode, Toast.LENGTH_SHORT).show();
                    shakeStyle = 2;
                    break;
                case R.id.tv_timer_check:    // 定时查询
                    startActivity(new Intent(mActivity, TimingQueryActivity.class));
                    SharePersistent.savePerference(mActivity, Constant.TimingQueryActivity, "");
                    SharePersistent.savePerference(mActivity, Constant.Activity, Constant.TimingQueryActivity);
                    break;
                case R.id.tv_current_data_check:    // 当前数据查询
                    startActivity(new Intent(mActivity, CurrentQueryActivity.class));
                    SharePersistent.savePerference(mActivity, Constant.Activity, Constant.CurrentQueryActivity);
                    break;
                case R.id.textViewRGBSort:// RGB排序
                    Intent intent = new Intent(mActivity, RgbSortActivity.class);
                    intent.putExtra("group", "group");
                    startActivityForResult(intent, INT_GO_COLORMODE);
                    break;
                case R.id.operation_guide_tv: // 操作指南
                    startActivity(new Intent(mActivity, OprationManualActivity.class));
                    break;
                case R.id.change_under_pic_tv: // 更换皮肤
                    showPicturePicker(MainActivity.this);
                    break;
                case R.id.reset_tv: // 一键还原
                    Drawable drawable = getResources().getDrawable(R.drawable.bg_all);
                    imageView.setImageDrawable(drawable);

                    String imagePath = getImagePath();
                    imagePath = "";
                    saveImagePathToSharedPreferences(imagePath);// 保存图片

                    break;
            }
        }
    }

    private void showCustomMessage() {
        if (lDialog == null) {
            lDialog = new Dialog(mActivity, android.R.style.Theme_Translucent_NoTitleBar);
            lDialog.requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            lDialog.setContentView(R.layout.dialogview);

            ImageView iv_route = (ImageView) lDialog.findViewById(R.id.imageViewWait);
            RotateAnimation mAnim = new RotateAnimation(0, 360, Animation.RESTART, 0.5f, Animation.RESTART, 0.5f);
            mAnim.setDuration(2000);
            mAnim.setRepeatCount(Animation.INFINITE);// 设置重复次数，这里是无限
            mAnim.setRepeatMode(Animation.RESTART);// 设置重复模式
            mAnim.setStartTime(Animation.START_ON_FIRST_FRAME);
            // 匀速转动的代码
            LinearInterpolator lin = new LinearInterpolator();
            mAnim.setInterpolator(lin);
            iv_route.startAnimation(mAnim);

            lDialog.show();
        }

    }

    public void lDialogDismiss(boolean show) {
        if (show) {
            sendTimeCheck(); // 发送时间校验指令
        }

        if (lDialog != null) {
            lDialog.dismiss();
            lDialog = null;
        }
    }

    public void sendTimeCheck() { // 发送时间校验指令
        Calendar calendar = Calendar.getInstance();
//		int hour       = calendar.get(Calendar.HOUR);        // 12小时制
        int hour = calendar.get(Calendar.HOUR_OF_DAY); // 24小时制
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
//		Toast.makeText(mActivity, ""+hour+": "+minute+": "+second, Toast.LENGTH_SHORT).show();
        NetConnectBle.getInstance().setSmartTimeSet(hour, minute, second);
    }

    private void gotoTimerSettting(String tag, int id) {
        Intent intent = new Intent(mActivity, OtherSettingActivity.class);
        intent.putExtra("tag", tag);
        mActivity.startActivityForResult(intent, id);
    }

    // =============================================行为事件=====================================================//

    /**
     * 扫描设备
     */
    public void initBleScanList(boolean isAllon) {
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        if (isInitGroup == false) {
            initGroup(isAllon);
            isInitGroup = true;
        }

        scanLeDevice(true);

    }

    /**
     * enable 暂时无用途
     *
     * @param enable
     */
    private void scanLeDevice(final boolean enable) {
        refreshDevices(true);
    }

    /**
     * 刷新
     */
    protected void refreshDevices(final boolean showToast) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!checkGPSIsOpen()) {
                checkAuthorization();
                return;
            }
        }
        if (mBluetoothAdapter.isEnabled()) {
            //开始扫描
            startLeScan(showToast);
            final Handler handler = new Handler(); // 开启 定时搜索 定时器
            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    // TODO Auto-generated method stub
                    // 在此处添加执行的代码
                    stopLeScan(showToast);
                    handler.removeCallbacks(this);
                }
            };
            handler.postDelayed(runnable, 6000);// 打开定时器，执行操作
        } else {
            Toast.makeText(MainActivity.this, R.string.scan, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 开始扫描
     */
    protected void startLeScan(boolean show) {
//		LogUtil.i(LedBleApplication.tag, "============================== 开始扫描：=============================" );
        if (show) {
            showCustomMessage();
        }
        if (Build.VERSION.SDK_INT < 23) {//安卓6.0以下的方案
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {//安卓6.0及以上的方案  

            if (mActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                // 申请定位授权
                mActivity.requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_CODE);

            } else {
                bleScanner.startScan(scanCallback);
            }
        }
    }

    /**
     * 停止扫描
     */
    protected void stopLeScan(boolean show) {
//		LogUtil.i(LedBleApplication.tag, "============================== 停止扫描：=============================" );
        if (show) {
            lDialogDismiss(show);
            isGPSOpen = false;
        }

        if (Build.VERSION.SDK_INT < 23) {
            //安卓6.0及以下版本BLE操作的代码
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        } else {
            //安卓6.0以下版本BLE操作的代码
            bleScanner.stopScan(scanCallback);
        }
    }

    //region Request Permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 110:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        refreshDevices(true);
//                        Toast.makeText(mActivity, "定位权限打开成功, 开始扫描", Toast.LENGTH_SHORT).show();
                    }
                } else {
//                	lDialogDismiss(false);
                }

                break;
        }
    }

    /**
     * 添加组
     */
    private void addGroupMessage(final boolean reName, final String groupName) {

        final EditText editText = new EditText(mActivity);
        if (reName) {
            editText.setText(groupName);
        }
        new AlertDialog.Builder(mActivity).setTitle(R.string.please_input).setIcon(android.R.drawable.ic_dialog_info)
                .setView(editText).setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                String gnameString = editText.getText().toString();

                GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
                ArrayList<Group> groups = gDao.getAllgroup();

                if (reName) {
                    for (Group group : groups) { //重命名组名
                        if (group.getGroupName().equalsIgnoreCase(groupName)) {
                            group.setGroupName(gnameString);
                            group.setOldGroupName(groupName);
                            groupView.setGroupName(gnameString);
                            Tool.ToastShow(mActivity, R.string.rename_groupname_success);
                        }
                    }
                    try {
                        gDao.updateGroupStatus(groups);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return;
                } else {
                    for (Group group : groups) { //添加新组
                        if (group.getGroupName().equalsIgnoreCase(gnameString)) { // 不能添加相同组名的组
                            Tool.ToastShow(mActivity, R.string.groupname_cannot_same);
                            return;
                        }
                    }
                }

                if (!StringUtils.isEmpty(gnameString)) {
                    addGroupByName(gnameString);
                }
            }
        }).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    private void addGroupByName(String groupName) {
        try {
            // 添加组数据库
            GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
            groupDeviceDao.addGroup(groupName);
            addGroupView(groupName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addGroupView(final String groupname) {
        // 添加组视图
        final GroupView groupView = new GroupView(mActivity, groupname, isAllOn);
        final SlideSwitch slideSwitch = groupView.getSlideSwitch();
        linearGroups.addView(groupView.getGroupView());
        map.put(groupname, slideSwitch);
        slideSwitch.setStateNoListener(false);
        slideSwitch.setSlideListener(new SlideListener() {

            @Override
            public void open() {
                changeStatus(groupname);
            }

            @Override
            public void close() {
                slideSwitch.setStateNoListener(true);
            }
        });
        groupView.getGroupView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (groupView.isTurnOn()) {
                    if (groupView.getConnect() > 0) {
                    } else {
                        Tool.ToastShow(mActivity, R.string.edit_group_please);
                        showActionSheet(groupView.getGroupName(), groupView);
                    }
                } else {
                    showActionSheet(groupView.getGroupName(), groupView);
                }
            }
        });
        groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDeleteDialog(groupView.getGroupName());
                return true;
            }
        });
        arrayListGroupViews.add(groupView);
    }

    private void changeStatus(String groupName) {
        setGroupName(groupName);
        for (Iterator<String> i = map.keySet().iterator(); i.hasNext(); ) {
            String key = i.next();
            if (!groupName.equals(key)) {
                map.get(key).setStateNoListener(false);
            }
        }
    }

    /**
     * 编辑组
     *
     * @param groupName
     */
    private void gotoEdit(final String groupName) {
        Intent intent = new Intent(mActivity, DeviceListActivity.class);
        intent.putExtra("group", groupName);
        GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
        ArrayList<GroupDevice> devices = groupDeviceDao.getDevicesByGroup(groupName);
        if (!ListUtiles.isEmpty(devices)) {
            intent.putExtra("devices", devices);
        }
        startActivityForResult(intent, INT_GO_LIST);
    }

    // =============================================更换皮肤=====================================================//

    /**
     * 更换皮肤相关 : 保存图片地址
     */
    private void saveImagePathToSharedPreferences(String imagePath) {

        SharedPreferences sharedPreferences = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
        Editor editor = sharedPreferences.edit();
        editor.putString(Constant.IMAGE_VALUE, imagePath);
        editor.commit();
    }

    /**
     * 更换皮肤相关 : 打开相册
     */
    public void showPicturePicker(Context context) {

        if (Build.VERSION.SDK_INT >= 23) {
            if (mActivity.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                // 申请授权
                mActivity.requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_CODE);
//				refresh();
            } else {
//                scanMp3();
                Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
                openAlbumIntent.setType("image/*");
                startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
            }
        } else {
//			scanMp3();
            Intent openAlbumIntent = new Intent(Intent.ACTION_GET_CONTENT);
            openAlbumIntent.setType("image/*");
            startActivityForResult(openAlbumIntent, CHOOSE_PICTURE);
        }
    }

    /**
     * 更换皮肤相关 : 获取保存过的图片路径
     */
    private String getImagePath() {
        sp = getSharedPreferences(Constant.IMAGE_VALUE, Context.MODE_PRIVATE);
        editor = sp.edit();
        String imagePath = sp.getString(Constant.IMAGE_VALUE, "");

        return imagePath;
    }

    /**
     * 更换皮肤相关 : 加载图片
     */
    private void showImage(String imaePath) {
        if (null != bm && !bm.isRecycled()) {
            bm.recycle();
            bm = null;
            System.gc();
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;//图片高宽度都为原来的二分之一，即图片大小为原来的大小的四分之一  
        options.inTempStorage = new byte[5 * 1024]; //设置16MB的临时存储空间（不过作用还没看出来，待验证
        Bitmap bitMap = BitmapFactory.decodeFile(imaePath, options);
        imageView.setImageBitmap(bitMap);

    }

    /**
     * 根据图片的Uri获取图片的绝对路径(已经适配多种API)
     *
     * @return 如果Uri对应的图片存在, 那么返回该图片的绝对路径, 否则返回null
     */
    public static String getRealPathFromUri(Context context, Uri uri) {
        int sdkVersion = Build.VERSION.SDK_INT;
        if (sdkVersion < 11) {
            // SDK < Api11
            return getRealPathFromUri_BelowApi11(context, uri);
        }
        if (sdkVersion < 19) {
            // SDK > 11 && SDK < 19
            return getRealPathFromUri_Api11To18(context, uri);
        }
        // SDK > 19
        return getRealPathFromUri_AboveApi19(context, uri);
    }

    /**
     * 适配api19以上,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_AboveApi19(Context context, Uri uri) {
        String filePath = null;
        String wholeID = DocumentsContract.getDocumentId(uri);

        // 使用':'分割
        String id = wholeID.split(":")[1];
        String[] projection = {MediaStore.Images.Media.DATA};
        String selection = MediaStore.Images.Media._ID + "=?";
        String[] selectionArgs = {id};

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, projection,
                selection, selectionArgs, null);
        int columnIndex = cursor.getColumnIndex(projection[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    /**
     * 适配api11-api18,根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_Api11To18(Context context, Uri uri) {
        String filePath = null;
        String[] projection = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, uri, projection, null,
                null, null);
        Cursor cursor = loader.loadInBackground();

        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }

    /**
     * 适配api11以下(不包括api11),根据uri获取图片的绝对路径
     */
    private static String getRealPathFromUri_BelowApi11(Context context, Uri uri) {
        String filePath = null;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection,
                null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            filePath = cursor.getString(cursor.getColumnIndex(projection[0]));
            cursor.close();
        }
        return filePath;
    }

    // =============================================蓝牙操作=====================================================//

    /**
     * 创建广播过滤器
     *
     * @return
     */
    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    /**
     * 成功扫描设备
     */
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (device != null) {
                        if (!LedBleApplication.getApp().getBleDevices().contains(device) && device.getName() != null) {
                            String name = device.getName();


                            if (name.startsWith(BluetoothLeServiceSingle.NAME_START_ELK)
                                    || name.startsWith(BluetoothLeServiceSingle.NAME_START_LED)) {
                                LedBleApplication.getApp().getBleDevices().add(device);
                                // updateNewFindDevice();
                                LogUtil.i(LedBleApplication.tag, "发现新设备：" + device.getAddress() + " total:"
                                        + LedBleApplication.getApp().getBleDevices().size());
                                conectHandler.sendEmptyMessage(MSG_START_CONNECT);// 可以开始连接设备了

                            }

                        }
                    }
                }
            });
        }
    };

    /**
     * 更新发现新的设备
     */
    private void updateNewFindDevice() {
        String connected = getResources().getString(R.string.conenct_device,
                LedBleApplication.getApp().getBleDevices().size(), hashMapConnect.size());
        textViewAllDeviceIndicater.setText(connected);
        updateDevieConnect();
    }

    /**
     * 更新连接设备数
     */
    private void updateDevieConnect() {
        final String connected = getResources().getString(R.string.conenct_device,
                LedBleApplication.getApp().getBleDevices().size(), hashMapConnect.size());
        // if (hashMapConnect.size() > 0) {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                textViewAllDeviceIndicater.setText(connected);
            }
        }, 3000);
        // } else {
        textViewAllDeviceIndicater.setText(connected);
        // }

        for (int i = 0, isize = arrayListGroupViews.size(); i < isize; i++) {
            GroupView gv = arrayListGroupViews.get(i);
            ArrayList<GroupDevice> gdes = gv.getGroupDevices();
            if (!ListUtiles.isEmpty(gdes)) {
                int connectCount = 0;
                for (GroupDevice groupDevice : gdes) {
                    String address = groupDevice.getAddress();
                    if (hashMapConnect.containsKey(address) && hashMapConnect.get(address)) {
                        connectCount++;
                    }
                }
                gv.setConnectCount(connectCount);
            }
        }
        if (groupName.equalsIgnoreCase("")) {
            textViewConnectCount.setText(Integer.toString(hashMapConnect.size()));
        }
    }

    private boolean booleanCanStart = false;
    private Handler conectHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {

            switch (msg.what) {
                case MSG_START_CONNECT:// 可以开始连接了
                    if (booleanCanStart == false) {
                        booleanCanStart = true;
                        // updateNewFindDevice();
                        startConnectDevices();
                    }
                    break;

                default:
                    break;
            }
        }

        ;
    };

    /**
     * 开始连接设备，设备的连接是异步的，必须等到一个设备连接成功后(发现service)才能连接新的设备
     */
    private void startConnectDevices() {
        System.out.println("mBluetoothLeService:" + mBluetoothLeService);
        final int delayTime = 50 /* 500*/;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true && null != mBluetoothLeService) {
                    ArrayList<BluetoothDevice> bldevices = LedBleApplication.getApp().getBleDevices();
                    try {
                        for (BluetoothDevice bluetoothDevice : bldevices) {
                            final String address = bluetoothDevice.getAddress();
                            final String name = bluetoothDevice.getName();
                            if (!LedBleApplication.getApp().getBleGattMap().containsKey(address)
                                    && null != mBluetoothLeService) {// 如果不存在，可以连接设备了

                                mBluetoothLeService.connect(address, name);
                                hashMapLock.put(address, false);// 上锁

                                while (true) {// 如果已经解锁那就可以进行下一次连接了
                                    Tool.delay(delayTime);
                                    if (hashMapLock.get(address)) {
                                        break;
                                    }
                                }
                            }
                            Tool.delay(delayTime);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();// 防止出现并发修改异常
                    }
                    Tool.delay(delayTime);
                }
            }
        }).start();

    }

    /**
     * 广播监听回调
     */
    private BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            System.out.println("接收廣播action-->" + action);
            if (BluetoothLeServiceSingle.ACTION_GATT_CONNECTED.equals(action)) {// 连接到设备
                String address = intent.getStringExtra("address");
                hashMapConnect.put(address, true);
                updateDevieConnect();
                LogUtil.i(LedBleApplication.tag, "connect:" + address);
            } else if (BluetoothLeServiceSingle.ACTION_GATT_DISCONNECTED.equals(action)) {// 设断开备
                String address = intent.getStringExtra("address");
                hashMapConnect.remove(address);
                LedBleApplication.getApp().removeDisconnectDevice(address);// 删除断开的device
                LedBleApplication.getApp().getBleGattMap().remove(address);// 删除断开的blgatt
                hashMapLock.remove(address);// 删除锁
                updateDevieConnect();
                LogUtil.i(LedBleApplication.tag, "disconnect:" + address + " connected devices:"
                        + LedBleApplication.getApp().getBleDevices().size());
            } else if (BluetoothLeServiceSingle.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {// 发现service,就可以获取Characteristic
                // 发现之后依次连接
                String address = intent.getStringExtra("address");
                BluetoothGatt blgat = mBluetoothLeService.getBluetoothGatt();
                LedBleApplication.getApp().getBleGattMap().put(address, blgat);
                hashMapLock.put(address, true);// 解锁
                LogUtil.i(LedBleApplication.tag, "发现service" + intent.getStringExtra("address"));
            } else if (BluetoothLeServiceSingle.ACTION_DATA_AVAILABLE.equals(action)) {// 读取到数据，不做处理，本应用不需要读取数据
                String data = intent.getStringExtra(BluetoothLeServiceSingle.EXTRA_DATA);
                String avtivity = SharePersistent.getPerference(mActivity, Constant.Activity);
                if (avtivity.equalsIgnoreCase(Constant.CurrentQueryActivity)) {
                    String str = SharePersistent.getPerference(getApplicationContext(), Constant.CurrentQueryActivity);

                    if (str.length() > 0) {
                        SharePersistent.savePerference(mActivity, Constant.CurrentQueryActivity, str + "" + data);
                    } else {
                        SharePersistent.savePerference(mActivity, Constant.CurrentQueryActivity, data);
                    }
                } else {
                    String str = SharePersistent.getPerference(getApplicationContext(), Constant.TimingQueryActivity);
                    if (str.length() > 0) {
                        SharePersistent.savePerference(mActivity, Constant.TimingQueryActivity, str + " " + data);
                    } else {
                        SharePersistent.savePerference(mActivity, Constant.TimingQueryActivity, data);
                    }
                }

            }
        }
    };

    /**
     * 首次初始化组视图
     */
    private void initGroup(boolean isAllOn) {
        GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
        ArrayList<Group> groups = gDao.getAllgroup();
        for (Group group : groups) {
            addGroupViewFromInit(group.getGroupName(), group.getIsOn(), isAllOn);
        }
    }

    private void addGroupViewFromInit(final String groupname, final String ison, final boolean isAllOn) {
        // 添加组视图
        final GroupView groupView = new GroupView(mActivity, groupname, isAllOn);
        GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
        final ArrayList<GroupDevice> groupDevices = groupDeviceDao.getDevicesByGroup(groupname);// 相同组的所有设备
        if (!ListUtiles.isEmpty(groupDevices)) {
            groupView.setGroupDevices(groupDevices);
        }

        final SlideSwitch slideSwitch = groupView.getSlideSwitch();
        map.put(groupname, slideSwitch);
        slideSwitch.setStateNoListener(false);
        slideSwitch.setSlideListener(new SlideListener() {

            @Override
            public void open() {
                changeStatus(groupname);
            }

            @Override
            public void close() {
                slideSwitch.setStateNoListener(true);
            }
        });

        groupView.getGroupView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (groupView.isTurnOn()) {
                    if (groupView.getConnect() > 0) {
                    } else {
                        Tool.ToastShow(mActivity, R.string.edit_group_please);
                        showActionSheet(groupView.getGroupName(), groupView);
                    }
                } else {
                    showActionSheet(groupView.getGroupName(), groupView);
                }
            }
        });

        linearGroups.addView(groupView.getGroupView());
        groupView.getGroupView().setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showDeleteDialog(groupView.getGroupName());
                return true;
            }
        });

        if ("y".equalsIgnoreCase(ison) && isAllOn) {
            groupView.turnOn();
        } else {
            groupView.turnOff();
        }
        arrayListGroupViews.add(groupView);
    }

    private void showDeleteDialog(final String groupName) {
        Dialog alertDialog = new AlertDialog.Builder(mActivity).setTitle(getResources().getString(R.string.tips))
                .setMessage(getResources().getString(R.string.delete_group, groupName))
                .setPositiveButton(R.string.confirm, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        try {
                            GroupDeviceDao gDao = new GroupDeviceDao(mActivity);
                            gDao.deleteGroup(groupName);
                            gDao.delteByGroup(groupName);
                            linearGroups.removeView(linearGroups.findViewWithTag(groupName));
                            map.remove(groupName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).setNegativeButton(R.string.cancell_dialog, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                }).create();

        alertDialog.show();

    }

    /**
     * 保存设备到某个组
     *
     * @param groupName
     * @param selectSet
     * @throws Exception
     */
    private void save2GroupByGroupName(String groupName, Set<BluetoothDevice> selectSet) throws Exception {
        GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
        // 删除原来组的设备，重新添加新的设备
        groupDeviceDao.delteByGroup(groupName);// 删除原有的设备
        ArrayList<GroupDevice> groupDevices = new ArrayList<GroupDevice>();
        for (BluetoothDevice bluetoothDevice : selectSet) {
            GroupDevice object = new GroupDevice();
            object.setAddress(bluetoothDevice.getAddress());
            object.setGroupName(groupName);
            groupDevices.add(object);
        }
        groupDeviceDao.save2Group(groupDevices);
    }

    /**
     * 计算组中已经连接的设备
     *
     * @param groupDevices
     * @return
     */
    private int findConnectCount(ArrayList<GroupDevice> groupDevices) {
        if (ListUtiles.isEmpty(groupDevices)) {
            return 0;
        }
        int connectCount = 0;
        for (GroupDevice groupDevice : groupDevices) {
            if (hashMapConnect.containsKey(groupDevice.getAddress()) && hashMapConnect.get(groupDevice.getAddress())) {
                connectCount++;
            }
        }
        return connectCount;
    }

    public void showActionSheet(String groupname, GroupView groupView) {

        NetConnectBle.getInstanceByGroup(groupname);
        if (null != groupView) {
            this.groupView = groupView;
        }

        if (groupname.equalsIgnoreCase("")) {
            Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
            Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white, getResources().getString(R.string.text_cancel));

            ActionSheet.createBuilder(this, this.getFragmentManager()).setCancelItem(cancelItem)
                    .setmOtherItems(item1).setGroupName(groupname).setCancelableOnTouchOutside(true).setListener(this).show();
        } else {
            Item item1 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.control));
            Item item2 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.add_device));
            Item item3 = new Item(R.color.white, R.color.white, R.drawable.tab_ct, R.drawable.tab_ct, R.color.colorPrimary, R.color.white, getResources().getString(R.string.rename_groupname));
            Item cancelItem = new Item(R.color.white, R.color.white, 0, 0, R.color.colorPrimary, R.color.white, getResources().getString(R.string.text_cancel));

            ActionSheet.createBuilder(this, this.getFragmentManager()).setCancelItem(cancelItem)
                    .setmOtherItems(item1, item2, item3).setGroupName(groupname).setCancelableOnTouchOutside(true).setListener(this).show();
        }
    }

    @Override
    public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
        // TODO Auto-generated method stub
//		Toast.makeText(getApplicationContext(), "dismissed isCancle = " + isCancel, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onOtherButtonClick(ActionSheet actionSheet, int index, String groupName) {
        // TODO Auto-generated method stub
        switch (index) {
            case 0:
//			Toast.makeText(getApplicationContext(), "控制的组 = "+groupName, Toast.LENGTH_SHORT).show();
                if (hashMapConnect.size() > 0) {
                    this.groupName = groupName;

                    NetConnectBle.getInstance().setGroupName(groupName);

                    Log.e(TAG, "onOtherButtonClick: " + groupName);
                    if (groupName.equalsIgnoreCase("")) {
                        textViewConnectCount.setText(Integer.toString(hashMapConnect.size()));
                    } else {
                        textViewConnectCount.setText(Integer.toString(NetConnectBle.getInstance().getDevicesByGroup().size()));
                    }
//				arrayListGroupViews.add(groupView);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.no_device_found, Toast.LENGTH_SHORT).show();
                }

                break;
            case 1:
                gotoEdit(groupName);
                break;
            case 2:
                addGroupMessage(true, groupName);
                break;

            default:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == INT_GO_LIST) {
            try {
                String grop = data.getStringExtra("group");
                save2GroupByGroupName(grop, LedBleApplication.getApp().getTempDevices());// 保存新的组到数据库
                // ==========
                GroupDeviceDao groupDeviceDao = new GroupDeviceDao(mActivity);
                ArrayList<GroupDevice> list = groupDeviceDao.getDevicesByGroup(grop);

                for (GroupView groupView : arrayListGroupViews) {
                    if (grop.equals(groupView.getGroupName())) {
                        // 设置已经连接的设备数量
                        groupView.setGroupDevices(list);
                        int count = findConnectCount(list);
                        LogUtil.i(LedBleApplication.tag, "count:" + count);
                        groupView.setConnectCount(count);
                        break;
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                Tool.ToastShow(mActivity, "保存失败！");
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == INT_GO_COLORMODE) {
            try {

                brightFragment.setActive();
                timerFragment.setActive();

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (resultCode == Activity.RESULT_OK && requestCode == GPS_REQUEST_CODE) {

            refreshDevices(false);

        } else if (resultCode == Activity.RESULT_OK && requestCode == OPEN_BLE) {

            if (Build.VERSION.SDK_INT >= 23) {// 安卓6.0以上的方案
                if (mBluetoothAdapter != null) {
                    bleScanner = mBluetoothAdapter.getBluetoothLeScanner();// 用过单例的方式获取实例
                    initScanCallback(); // 初始化 ScanCallback
                }

            } else {
                mBluetoothAdapter = bluetoothManager.getAdapter();
            }

            if (mBluetoothAdapter.isEnabled()) {
                refreshDevices(true);
            }

        } else {
            if (resultCode == RESULT_OK) {
                switch (requestCode) {
                    case TAKE_PICTURE:
                        break;
                    case CHOOSE_PICTURE: // 更换皮肤
                        ContentResolver resolver = getContentResolver();
                        // 照片的原始资源地址
                        if (null != data) {
                            Uri originalUri = data.getData();
                            if (null != originalUri) {
                                try {

                                    if (null != bm && !bm.isRecycled()) { //回收图片资源
                                        bm.recycle();
                                        bm = null;
                                        System.gc();
                                    }

                                    // 使用ContentProvider通过URI获取原始图片
                                    bm = MediaStore.Images.Media.getBitmap(resolver, originalUri);
                                    if (bm != null) {

                                        String img_path = getRealPathFromUri(mActivity, originalUri);// 这是本机的图片路径
                                        showImage(img_path);

                                        saveImagePathToSharedPreferences(img_path);// 保存图片
                                    }
                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        int sensorType = sensorEvent.sensor.getType();
        float[] values = sensorEvent.values;

        if (sensorType == Sensor.TYPE_ACCELEROMETER) {
            if ((Math.abs(values[0]) > 19) || Math.abs(values[1]) > 19 || Math.abs(values[2]) > 19) {

                switch (shakeStyle) {
                    case 0:
                        soundPool.play(soundID, 1, 1, 0, 0, 1);
                        setRgb(random.nextInt(255) + 1, random.nextInt(255) + 1, random.nextInt(255) + 1);
                        break;
                    case 1:

                        break;
                    case 2:
                        soundPool.play(soundID, 1, 1, 0, 0, 1);
                        setRegMode(random.nextInt(156 - 135 + 1) + 135);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public void setService(BluetoothLeServiceSingle service) {
        this.mBluetoothLeService = service;
    }

    // =========================================业务模式=====================================================

    /**
     * 全开
     */
    private void allOn() {
        NetConnectBle.getInstanceByGroup("").turnOn();
    }

    /**
     * 全关
     */
    private void allOff() {
        NetConnectBle.getInstanceByGroup("").turnOff();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public boolean open() {
        NetConnectBle.getInstanceByGroup(groupName).turnOn();
        return false;
    }

    public boolean close() {
        NetConnectBle.getInstanceByGroup(groupName).turnOff();
        return false;
    }

    public void turnOnOffTimerOn(int onOrOff) {
        NetConnectBle.getInstanceByGroup(groupName).turnOnOffTimerOn(onOrOff);
    }

    public void timerOn(int hour, int minute, int model) {
        NetConnectBle.getInstanceByGroup(groupName).timerOn(hour, minute, model);
    }

    public void turnOnOffTimerOff(int onOrOff) {
        NetConnectBle.getInstanceByGroup(groupName).turnOnOrOffTimerOff(onOrOff);
    }

    public void timerOff(int hour, int minute) {
        NetConnectBle.getInstanceByGroup(groupName).timerOff(hour, minute);
    }

    public void setRgb(int r, int g, int b) {
        NetConnectBle.getInstanceByGroup(groupName).setRgb(r, g, b);
    }

    public void setDim(int dim) {
        NetConnectBle.getInstanceByGroup(groupName).setDim(dim);
    }

    public void setDimModel(int dimModel) {
        NetConnectBle.getInstanceByGroup(groupName).setDimModel(dimModel);
    }

    public void setRegMode(int model) {
        NetConnectBle.getInstanceByGroup(groupName).setRgbMode(model);
    }

    public void setSpeed(int speed) {
        NetConnectBle.getInstanceByGroup(groupName).setSpeed(speed);
    }

    public void setTimerFirData(int style) throws ParseException {
        NetConnectBle.getInstanceByGroup(groupName).setTimerFirData(style);
        ;
    }

    public void setTimerSecData(int[] data) {
        NetConnectBle.getInstanceByGroup(groupName).setTimerSecData(data);
    }

    public void setDiy(ArrayList<MyColor> colors, int style) {
        NetConnectBle.getInstanceByGroup(groupName).setDiy(colors, style);
    }

    public void setBrightNess(int brightness) {
//		this.brightness = brightness;// 设置亮度参数
//		SharePersistent.savePerference(this, brightnessKey, this.brightness);
        NetConnectBle.getInstanceByGroup(groupName).setBrightness(brightness);

    }

    public void setSmartBrightness(int brightness, int mode) {
//		showCheckSelfPermission();
//		this.brightness = brightness;// 设置亮度参数
        NetConnectBle.getInstanceByGroup(groupName).setSmartBrightness(brightness, mode);

    }

    public void setCT(int warm, int cool) {
        NetConnectBle.getInstanceByGroup(groupName).setColorWarm(warm, cool);
    }

    public void setCTModel(int model) {
        NetConnectBle.getInstanceByGroup(groupName).setColorWarmModel(model);
    }

    // 律动 模式
    public void setDynamicModel(int model) {
        NetConnectBle.getInstanceByGroup(groupName).setDynamicModel(model);
    }

    @Override
    public void onException(Exception e) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unbindService(myServiceConenction);
        unregisterReceiver(mGattUpdateReceiver);
//		mActivity.unregisterReceiver(mGattUpdateReceiver);
        hashMapConnect = null;
        hashMapLock = null;
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (isBackground(mActivity)) { //app进入后台停止扫描
//			mBluetoothAdapter.stopLeScan(mLeScanCallback);
//			Toast.makeText(mActivity, "当前为后台", Toast.LENGTH_SHORT).show();
        } else {

//			Toast.makeText(mActivity, "当前为前台", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    /**
     * 判断app是否进入后台
     */
    public static boolean isBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        for (RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.processName.equals(context.getPackageName())) {
                if (appProcess.importance == RunningAppProcessInfo.IMPORTANCE_BACKGROUND) {

                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public SegmentedRadioGroup getSegmentDm() {
        return segmentDm;
    }

    public SegmentedRadioGroup getSegmentCt() {
        return segmentCt;
    }

    public SegmentedRadioGroup getSegmentRgb() {
        return segmentRgb;
    }

    public SegmentedRadioGroup getSegmentMusic() {
        return segmentMusic;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }
}
