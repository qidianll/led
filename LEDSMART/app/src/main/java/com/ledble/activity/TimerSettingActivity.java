package com.ledble.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.BaseActivity;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.ledsmart.R;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;

public class TimerSettingActivity extends BaseActivity {

	private LinearLayout relativeLayout1;
	private WheelView listViewH;
	private WheelView listViewM;
	private WheelView listViewWhite;
	private WheelView listViewLightblue;
	private WheelView listViewRed;
	private WheelView listViewGreen;
	private WheelView listViewPink;
	private WheelView listViewCrystal;
	
	
	private LinearLayout relativeLayout2;
	private WheelView listViewH2;
	private WheelView listViewM2;
	private WheelView listViewWhite2;
	private WheelView listViewLightblue2;
	private WheelView listViewRed2;
	private WheelView listViewGreen2;
	private WheelView listViewPink2;
	private WheelView listViewCrystal2;
	
	private LinearLayout relativeLayout3;
	private WheelView listViewH3;
	private WheelView listViewM3;
	private WheelView listViewWhite3;
	private WheelView listViewLightblue3;
	private WheelView listViewRed3;
	private WheelView listViewGreen3;
	private WheelView listViewPink3;
	private WheelView listViewCrystal3;
	

	private WheelModelAdapter wheelAdapterH;
	private WheelModelAdapter wheelAdapterM;
	private WheelModelAdapter wheelAdapterModel;
	
	
	private TextView textViewID;
	
//	private String tag = "";
	private int tag;
	

	private int hour /*= 12*/;
	private int minite /*= 30*/;
	private int model /*= 10*/;
	
	private int whiteValue;
	private int lightblueValue ;
	private int redValue ;
	private int greenValue;
	private int pinkValue ;
	private int crystalValue ;
	
	private String modelText = "";
	
	private String RGB = "RGB";
	private String RGBW = "RGBW";
	private String RGBWC = "RGBWC";
	private String RGBWCP = "RGBWCP";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.activity_timer_setting1);
	
		
		Time t=new Time(); 
		t.setToNow();  
  
		hour = t.hour;    // 0-23
		minite = t.minute;    // 0-23
		
		
		this.relativeLayout1 = (LinearLayout)findViewById(R.id.relativeLayout1);
		this.relativeLayout2 = (LinearLayout)findViewById(R.id.relativeLayout2);
		this.relativeLayout3 = (LinearLayout)findViewById(R.id.relativeLayout3);
		
		
		final String[] colorModel = getResources().getStringArray(R.array.rgb_model);
		String[] datas = colorModel[SharePersistent.getInt(getApplicationContext(), "RGB-SORT")].split(",");	
	
		if (datas[0].equalsIgnoreCase(RGB)) {			
			this.relativeLayout1.setVisibility(View.VISIBLE);			
			this.relativeLayout2.setVisibility(View.GONE);			
			this.relativeLayout3.setVisibility(View.GONE);
			
			this.listViewH = (WheelView) findViewById(R.id.listViewH);
			this.listViewM = (WheelView) findViewById(R.id.listViewM);
			this.listViewRed = (WheelView) findViewById(R.id.listViewModel1);
			this.listViewGreen = (WheelView) findViewById(R.id.listViewModel2);
			this.listViewLightblue = (WheelView) findViewById(R.id.listViewModel3);
			this.listViewWhite = (WheelView) findViewById(R.id.listViewModel4);
			this.listViewCrystal = (WheelView) findViewById(R.id.listViewModel5);
			this.listViewPink = (WheelView) findViewById(R.id.listViewModel6);
			
//			this.linearLayoutContainer = findViewById(R.id.linearLayoutContainer);
			
			
			// ================
			
			String[] modelH = new String[24];
			for (int i = 0; i < 24; i++) {
				modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterH = new WheelModelAdapter(this, modelH);
			this.listViewH.setViewAdapter(wheelAdapterH);
			this.listViewH.setCurrentItem(hour);
			this.listViewH.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					hour = newValue;
				}
			});

			String[] modelM = new String[60];
			for (int i = 0; i < 60; i++) {
				modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterM = new WheelModelAdapter(this, modelM);
			this.listViewM.setViewAdapter(wheelAdapterM);
			this.listViewM.setCurrentItem(minite);
			// this.listViewModel.setCyclic(true);
			this.listViewM.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					minite = newValue;
				}
			});


			
			final String[] timerModel = new String[101];
			for (int i = 0; i <= 100; i++) {
				timerModel[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterModel = new WheelModelAdapter(this, timerModel);						
			this.listViewRed.setViewAdapter(wheelAdapterModel);
			this.listViewRed.setCurrentItem(0);
			this.listViewRed.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					redValue = newValue;
				}
			});
			this.listViewGreen.setViewAdapter(wheelAdapterModel);
			this.listViewGreen.setCurrentItem(0);
			this.listViewGreen.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					greenValue = newValue;
				}
			});
			this.listViewLightblue.setViewAdapter(wheelAdapterModel);
			this.listViewLightblue.setCurrentItem(0);
			this.listViewLightblue.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					lightblueValue = newValue;
				}
			});
			this.listViewWhite.setViewAdapter(wheelAdapterModel);
			this.listViewWhite.setCurrentItem(0);
			this.listViewWhite.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
			
					whiteValue = newValue;
				}
			});
			this.listViewPink.setViewAdapter(wheelAdapterModel);
			this.listViewPink.setCurrentItem(0);
			this.listViewPink.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					pinkValue = newValue;
				}
			});
			this.listViewCrystal.setViewAdapter(wheelAdapterModel);
			this.listViewCrystal.setCurrentItem(0);
			this.listViewCrystal.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					crystalValue = newValue;
				}
			});

			View.OnClickListener clickListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					switch (v.getId()) {
					case R.id.buttonCancell: {
						finish();
					}
						break;
					case R.id.buttonSave: {
						putDataback();
					}
						break;
					}
				}
			};

			findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
			findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
			
			this.textViewID = (TextView) findViewById(R.id.textViewID);
			if (null != getIntent()) {
				tag = getIntent().getIntExtra("tag", -1);
				this.textViewID.setText("ID:"+String.valueOf(tag));
				
				int[] array = getIntent().getIntArrayExtra("data");
				
//				if (array[0] == 0 && array[1] == 0) {
//					this.listViewH.setCurrentItem(hour);
//					this.listViewM.setCurrentItem(minite);
//				}else {
					this.listViewH.setCurrentItem(array[0]);
					this.listViewM.setCurrentItem(array[1]);
//				}
						
				
				this.listViewRed.setCurrentItem(array[2]);
				this.listViewGreen.setCurrentItem(array[3]);
				this.listViewLightblue.setCurrentItem(array[4]);
				this.listViewWhite.setCurrentItem(array[5]);
				this.listViewCrystal.setCurrentItem(array[6]);
				this.listViewPink.setCurrentItem(array[7]);				
				
			} else {
				finish();
			}
			
		}else if (datas[0].equalsIgnoreCase(RGBW)) {
			this.relativeLayout1.setVisibility(View.GONE);
			this.relativeLayout2.setVisibility(View.VISIBLE);
			this.relativeLayout3.setVisibility(View.GONE);
			
			this.listViewH2 = (WheelView) findViewById(R.id.listViewH2);
			this.listViewM2 = (WheelView) findViewById(R.id.listViewM2);
			this.listViewRed2 = (WheelView) findViewById(R.id.listViewModel21);
			this.listViewGreen2 = (WheelView) findViewById(R.id.listViewModel22);
			this.listViewLightblue2 = (WheelView) findViewById(R.id.listViewModel23);
			this.listViewWhite2 = (WheelView) findViewById(R.id.listViewModel24);
			this.listViewCrystal2 = (WheelView) findViewById(R.id.listViewModel25);
			this.listViewPink2 = (WheelView) findViewById(R.id.listViewModel26);
			
//			this.linearLayoutContainer2 = findViewById(R.id.linearLayoutContainer2);
			
			
			// ================
			
			String[] modelH = new String[24];
			for (int i = 0; i < 24; i++) {
				modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterH = new WheelModelAdapter(this, modelH);
			this.listViewH2.setViewAdapter(wheelAdapterH);
			this.listViewH2.setCurrentItem(hour);
			this.listViewH2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					hour = newValue;
				}
			});

			String[] modelM = new String[60];
			for (int i = 0; i < 60; i++) {
				modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterM = new WheelModelAdapter(this, modelM);
			this.listViewM2.setViewAdapter(wheelAdapterM);
			this.listViewM2.setCurrentItem(minite);
			// this.listViewModel.setCyclic(true);
			this.listViewM2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					minite = newValue;
				}
			});


			
			final String[] timerModel = new String[101];
			for (int i = 0; i <= 100; i++) {
				timerModel[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterModel = new WheelModelAdapter(this, timerModel);			
			this.listViewRed2.setViewAdapter(wheelAdapterModel);
			this.listViewRed2.setCurrentItem(0);
			this.listViewRed2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					redValue = newValue;
				}
			});
			this.listViewGreen2.setViewAdapter(wheelAdapterModel);
			this.listViewGreen2.setCurrentItem(0);
			this.listViewGreen2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					greenValue = newValue;
				}
			});
			this.listViewLightblue2.setViewAdapter(wheelAdapterModel);
			this.listViewLightblue2.setCurrentItem(0);
			this.listViewLightblue2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					lightblueValue = newValue;
				}
			});
			this.listViewWhite2.setViewAdapter(wheelAdapterModel);
			this.listViewWhite2.setCurrentItem(0);
			this.listViewWhite2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
//					String[] datas = timerModel[newValue].split(",");
//					model = Integer.parseInt(datas[1]);
//					modelText = datas[0];
			
					whiteValue = newValue;
				}
			});
			this.listViewPink2.setViewAdapter(wheelAdapterModel);
			this.listViewPink2.setCurrentItem(0);
			this.listViewPink2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					pinkValue = newValue;
				}
			});
			this.listViewCrystal2.setViewAdapter(wheelAdapterModel);
			this.listViewCrystal2.setCurrentItem(0);
			this.listViewCrystal2.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					crystalValue = newValue;
				}
			});

			View.OnClickListener clickListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					switch (v.getId()) {
					case R.id.buttonCancell: {
						finish();
					}
						break;
					case R.id.buttonSave: {
						putDataback();
					}
						break;
					}
				}
			};

			findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
			findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
			
			this.textViewID = (TextView) findViewById(R.id.textViewID);
			if (null != getIntent()) {
				tag = getIntent().getIntExtra("tag", -1);
				this.textViewID.setText("ID:"+String.valueOf(tag));
				
				int[] array = getIntent().getIntArrayExtra("data");
				
//				if (array[0] != 0) {
					this.listViewH2.setCurrentItem(array[0]);
//				}
//				if (array[1] != 0) {
					this.listViewM2.setCurrentItem(array[1]);
//				}		
				
				this.listViewRed2.setCurrentItem(array[2]);
				this.listViewGreen2.setCurrentItem(array[3]);
				this.listViewLightblue2.setCurrentItem(array[4]);
				this.listViewWhite2.setCurrentItem(array[5]);
				this.listViewCrystal2.setCurrentItem(array[6]);
				this.listViewPink2.setCurrentItem(array[7]);
				
			} else {
				finish();
			}
			
		}else if (datas[0].equalsIgnoreCase(RGBWC)) {

		}else if (datas[0].equalsIgnoreCase(RGBWCP)) {
			this.relativeLayout1.setVisibility(View.GONE);
			this.relativeLayout2.setVisibility(View.GONE);
			this.relativeLayout3.setVisibility(View.VISIBLE);
			
			this.listViewH3 = (WheelView) findViewById(R.id.listViewH3);
			this.listViewM3 = (WheelView) findViewById(R.id.listViewM3);
			this.listViewRed3 = (WheelView) findViewById(R.id.listViewModel31);
			this.listViewGreen3 = (WheelView) findViewById(R.id.listViewModel32);
			this.listViewLightblue3 = (WheelView) findViewById(R.id.listViewModel33);
			this.listViewWhite3 = (WheelView) findViewById(R.id.listViewModel34);
			this.listViewCrystal3 = (WheelView) findViewById(R.id.listViewModel35);
			this.listViewPink3 = (WheelView) findViewById(R.id.listViewModel36);
			
//			this.linearLayoutContainer3 = findViewById(R.id.linearLayoutContainer3);
			
			
			// ================
			
			String[] modelH = new String[24];
			for (int i = 0; i < 24; i++) {
				modelH[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterH = new WheelModelAdapter(this, modelH);
			this.listViewH3.setViewAdapter(wheelAdapterH);
			this.listViewH3.setCurrentItem(hour);
			this.listViewH3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					hour = newValue;
				}
			});

			String[] modelM = new String[60];
			for (int i = 0; i < 60; i++) {
				modelM[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterM = new WheelModelAdapter(this, modelM);
			this.listViewM3.setViewAdapter(wheelAdapterM);
			this.listViewM3.setCurrentItem(minite);
			// this.listViewModel.setCyclic(true);
			this.listViewM3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					minite = newValue;
				}
			});


			
			final String[] timerModel = new String[101];
			for (int i = 0; i <= 100; i++) {
				timerModel[i] = NumberHelper.LeftPad_Tow_Zero(i);
			}
			wheelAdapterModel = new WheelModelAdapter(this, timerModel);
			this.listViewWhite3.setViewAdapter(wheelAdapterModel);
			this.listViewWhite3.setCurrentItem(0);
			this.listViewWhite3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
//					String[] datas = timerModel[newValue].split(",");
//					model = Integer.parseInt(datas[1]);
//					modelText = datas[0];
			
					whiteValue = newValue;
				}
			});
			this.listViewLightblue3.setViewAdapter(wheelAdapterModel);
			this.listViewLightblue3.setCurrentItem(0);
			this.listViewLightblue3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					lightblueValue = newValue;
				}
			});
			this.listViewRed3.setViewAdapter(wheelAdapterModel);
			this.listViewRed3.setCurrentItem(0);
			this.listViewRed3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					redValue = newValue;
				}
			});
			this.listViewGreen3.setViewAdapter(wheelAdapterModel);
			this.listViewGreen3.setCurrentItem(0);
			this.listViewGreen3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					greenValue = newValue;
				}
			});
			this.listViewPink3.setViewAdapter(wheelAdapterModel);
			this.listViewPink3.setCurrentItem(0);
			this.listViewPink3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					pinkValue = newValue;
				}
			});
			this.listViewCrystal3.setViewAdapter(wheelAdapterModel);
			this.listViewCrystal3.setCurrentItem(0);
			this.listViewCrystal3.addChangingListener(new OnWheelChangedListener() {
				@Override
				public void onChanged(WheelView wheel, int oldValue, int newValue) {
					crystalValue = newValue;
				}
			});

			View.OnClickListener clickListener = new OnClickListener() {
				@Override
				public void onClick(View v) {
					switch (v.getId()) {
					case R.id.buttonCancell: {
						finish();
					}
						break;
					case R.id.buttonSave: {
						putDataback();
					}
						break;
					}
				}
			};

			findButtonById(R.id.buttonCancell).setOnClickListener(clickListener);
			findButtonById(R.id.buttonSave).setOnClickListener(clickListener);
			
			this.textViewID = (TextView) findViewById(R.id.textViewID);
			if (null != getIntent()) {
				tag = getIntent().getIntExtra("tag", -1);
				this.textViewID.setText("ID:"+String.valueOf(tag));
				
				int[] array = getIntent().getIntArrayExtra("data");
				
//				if (array[0] != 0) {
					this.listViewH3.setCurrentItem(array[0]);
//				}
//				if (array[1] != 0) {
					this.listViewM3.setCurrentItem(array[1]);
//				}		
				
				this.listViewRed3.setCurrentItem(array[2]);
				this.listViewGreen3.setCurrentItem(array[3]);
				this.listViewLightblue3.setCurrentItem(array[4]);
				this.listViewWhite3.setCurrentItem(array[5]);
				this.listViewCrystal3.setCurrentItem(array[6]);
				this.listViewPink3.setCurrentItem(array[7]);
				
			} else {
				finish();
			}
			
		}

	}

	private void putDataback() {

		Intent intent = new Intent();
		intent.putExtra("hour", hour);
		intent.putExtra("minite", minite);
		intent.putExtra("model", model);
		intent.putExtra("modelText", modelText);
		intent.putExtra("redValue", redValue);
		intent.putExtra("greenValue", greenValue);
		intent.putExtra("lightblueValue", lightblueValue);
		intent.putExtra("whiteValue", whiteValue);
		intent.putExtra("crystalValue", crystalValue);		
		intent.putExtra("pinkValue", pinkValue);		
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
//		MobclickAgent.onResume(this);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
//		MobclickAgent.onResume(this);
	}
}
