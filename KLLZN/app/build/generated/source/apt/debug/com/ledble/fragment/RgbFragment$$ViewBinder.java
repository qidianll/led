// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RgbFragment$$ViewBinder<T extends com.ledble.fragment.RgbFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296333, "field 'changeRadioGroup'");
    target.changeRadioGroup = finder.castView(view, 2131296333, "field 'changeRadioGroup'");
    view = finder.findRequiredView(source, 2131296559, "field 'relativeTab1'");
    target.relativeTab1 = view;
    view = finder.findRequiredView(source, 2131296560, "field 'relativeTab2'");
    target.relativeTab2 = view;
    view = finder.findRequiredView(source, 2131296561, "field 'relativeTab3'");
    target.relativeTab3 = view;
    view = finder.findRequiredView(source, 2131296490, "field 'listViewModel'");
    target.listViewModel = finder.castView(view, 2131296490, "field 'listViewModel'");
    view = finder.findRequiredView(source, 2131296660, "field 'textViewCurretModel'");
    target.textViewCurretModel = finder.castView(view, 2131296660, "field 'textViewCurretModel'");
    view = finder.findRequiredView(source, 2131296416, "field 'imageViewPicker'");
    target.imageViewPicker = finder.castView(view, 2131296416, "field 'imageViewPicker'");
    view = finder.findRequiredView(source, 2131296299, "field 'blackWiteSelectView'");
    target.blackWiteSelectView = finder.castView(view, 2131296299, "field 'blackWiteSelectView'");
    view = finder.findRequiredView(source, 2131296411, "field 'imageButtonOffOn'");
    target.imageButtonOffOn = finder.castView(view, 2131296411, "field 'imageButtonOffOn'");
    view = finder.findRequiredView(source, 2131296418, "field 'buttonPlay'");
    target.buttonPlay = finder.castView(view, 2131296418, "field 'buttonPlay'");
    view = finder.findRequiredView(source, 2131296415, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131296415, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131296600, "field 'seekBarSpeedBar'");
    target.seekBarSpeedBar = finder.castView(view, 2131296600, "field 'seekBarSpeedBar'");
    view = finder.findRequiredView(source, 2131296676, "field 'textViewSpeed'");
    target.textViewSpeed = finder.castView(view, 2131296676, "field 'textViewSpeed'");
    view = finder.findRequiredView(source, 2131296593, "field 'seekBarBrightNess2'");
    target.seekBarBrightNess2 = finder.castView(view, 2131296593, "field 'seekBarBrightNess2'");
    view = finder.findRequiredView(source, 2131296658, "field 'textViewBrightNess2'");
    target.textViewBrightNess2 = finder.castView(view, 2131296658, "field 'textViewBrightNess2'");
    view = finder.findRequiredView(source, 2131296601, "field 'seekBarSpeed2'");
    target.seekBarSpeed2 = finder.castView(view, 2131296601, "field 'seekBarSpeed2'");
    view = finder.findRequiredView(source, 2131296677, "field 'textViewSpeed2'");
    target.textViewSpeed2 = finder.castView(view, 2131296677, "field 'textViewSpeed2'");
    view = finder.findRequiredView(source, 2131296592, "field 'seekBarBrightness'");
    target.seekBarBrightness = finder.castView(view, 2131296592, "field 'seekBarBrightness'");
    view = finder.findRequiredView(source, 2131296657, "field 'textViewBrightness'");
    target.textViewBrightness = finder.castView(view, 2131296657, "field 'textViewBrightness'");
    view = finder.findRequiredView(source, 2131296693, "field 'tgbtn'");
    target.tgbtn = finder.castView(view, 2131296693, "field 'tgbtn'");
    view = finder.findRequiredView(source, 2131296716, "field 'tvBrightness'");
    target.tvBrightness = finder.castView(view, 2131296716, "field 'tvBrightness'");
    view = finder.findRequiredView(source, 2131296306, "field 'buttonRunButton'");
    target.buttonRunButton = finder.castView(view, 2131296306, "field 'buttonRunButton'");
  }

  @Override public void unbind(T target) {
    target.changeRadioGroup = null;
    target.relativeTab1 = null;
    target.relativeTab2 = null;
    target.relativeTab3 = null;
    target.listViewModel = null;
    target.textViewCurretModel = null;
    target.imageViewPicker = null;
    target.blackWiteSelectView = null;
    target.imageButtonOffOn = null;
    target.buttonPlay = null;
    target.imageViewOnOff = null;
    target.seekBarSpeedBar = null;
    target.textViewSpeed = null;
    target.seekBarBrightNess2 = null;
    target.textViewBrightNess2 = null;
    target.seekBarSpeed2 = null;
    target.textViewSpeed2 = null;
    target.seekBarBrightness = null;
    target.textViewBrightness = null;
    target.tgbtn = null;
    target.tvBrightness = null;
    target.buttonRunButton = null;
  }
}
