// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class TimerFragment$$ViewBinder<T extends com.ledble.fragment.TimerFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296705, "field 'toggleButtonTop'");
    target.toggleButtonTop = finder.castView(view, 2131296705, "field 'toggleButtonTop'");
    view = finder.findRequiredView(source, 2131296698, "field 'toggleButtonLeft'");
    target.toggleButtonLeft = finder.castView(view, 2131296698, "field 'toggleButtonLeft'");
    view = finder.findRequiredView(source, 2131296702, "field 'toggleButtonRight'");
    target.toggleButtonRight = finder.castView(view, 2131296702, "field 'toggleButtonRight'");
    view = finder.findRequiredView(source, 2131296694, "field 'toggleButtonBotton'");
    target.toggleButtonBotton = finder.castView(view, 2131296694, "field 'toggleButtonBotton'");
    view = finder.findRequiredView(source, 2131296674, "field 'textViewSelectedLamp'");
    target.textViewSelectedLamp = finder.castView(view, 2131296674, "field 'textViewSelectedLamp'");
    view = finder.findRequiredView(source, 2131296461, "field 'linearLayoutDynamic'");
    target.linearLayoutDynamic = finder.castView(view, 2131296461, "field 'linearLayoutDynamic'");
    view = finder.findRequiredView(source, 2131296466, "field 'linearLayoutRGB'");
    target.linearLayoutRGB = finder.castView(view, 2131296466, "field 'linearLayoutRGB'");
    view = finder.findRequiredView(source, 2131296468, "field 'linearLayoutScene'");
    target.linearLayoutScene = finder.castView(view, 2131296468, "field 'linearLayoutScene'");
    view = finder.findRequiredView(source, 2131296464, "field 'linearLayoutMusic'");
    target.linearLayoutMusic = finder.castView(view, 2131296464, "field 'linearLayoutMusic'");
    view = finder.findRequiredView(source, 2131296477, "field 'linearLayoutTop'");
    target.linearLayoutTop = finder.castView(view, 2131296477, "field 'linearLayoutTop'");
  }

  @Override public void unbind(T target) {
    target.toggleButtonTop = null;
    target.toggleButtonLeft = null;
    target.toggleButtonRight = null;
    target.toggleButtonBotton = null;
    target.textViewSelectedLamp = null;
    target.linearLayoutDynamic = null;
    target.linearLayoutRGB = null;
    target.linearLayoutScene = null;
    target.linearLayoutMusic = null;
    target.linearLayoutTop = null;
  }
}
