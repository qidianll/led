// Generated code from Butter Knife. Do not modify!
package com.ledble.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends com.ledble.activity.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296604, "field 'segmentDm'");
    target.segmentDm = finder.castView(view, 2131296604, "field 'segmentDm'");
    view = finder.findRequiredView(source, 2131296603, "field 'segmentCt'");
    target.segmentCt = finder.castView(view, 2131296603, "field 'segmentCt'");
    view = finder.findRequiredView(source, 2131296606, "field 'segmentRgb'");
    target.segmentRgb = finder.castView(view, 2131296606, "field 'segmentRgb'");
    view = finder.findRequiredView(source, 2131296605, "field 'segmentMusic'");
    target.segmentMusic = finder.castView(view, 2131296605, "field 'segmentMusic'");
    view = finder.findRequiredView(source, 2131296440, "field 'ivLeftMenu'");
    target.ivLeftMenu = finder.castView(view, 2131296440, "field 'ivLeftMenu'");
    view = finder.findRequiredView(source, 2131296659, "field 'textViewConnectCount'");
    target.textViewConnectCount = finder.castView(view, 2131296659, "field 'textViewConnectCount'");
    view = finder.findRequiredView(source, 2131296442, "field 'ivRightMenu'");
    target.ivRightMenu = finder.castView(view, 2131296442, "field 'ivRightMenu'");
    view = finder.findRequiredView(source, 2131296564, "field 'rgBottom'");
    target.rgBottom = finder.castView(view, 2131296564, "field 'rgBottom'");
    view = finder.findRequiredView(source, 2131296540, "field 'rbFirst'");
    target.rbFirst = finder.castView(view, 2131296540, "field 'rbFirst'");
    view = finder.findRequiredView(source, 2131296549, "field 'rbSecond'");
    target.rbSecond = finder.castView(view, 2131296549, "field 'rbSecond'");
    view = finder.findRequiredView(source, 2131296550, "field 'rbThrid'");
    target.rbThrid = finder.castView(view, 2131296550, "field 'rbThrid'");
    view = finder.findRequiredView(source, 2131296541, "field 'rbFourth'");
    target.rbFourth = finder.castView(view, 2131296541, "field 'rbFourth'");
    view = finder.findRequiredView(source, 2131296539, "field 'rbFifth'");
    target.rbFifth = finder.castView(view, 2131296539, "field 'rbFifth'");
    view = finder.findRequiredView(source, 2131296499, "field 'avtivity_main'");
    target.avtivity_main = finder.castView(view, 2131296499, "field 'avtivity_main'");
    view = finder.findRequiredView(source, 2131296505, "field 'left_menu'");
    target.left_menu = finder.castView(view, 2131296505, "field 'left_menu'");
    view = finder.findRequiredView(source, 2131296568, "field 'right_menu'");
    target.right_menu = finder.castView(view, 2131296568, "field 'right_menu'");
    view = finder.findRequiredView(source, 2131296478, "field 'TopItem'");
    target.TopItem = finder.castView(view, 2131296478, "field 'TopItem'");
    view = finder.findRequiredView(source, 2131296611, "field 'shakeView'");
    target.shakeView = finder.castView(view, 2131296611, "field 'shakeView'");
    view = finder.findRequiredView(source, 2131296462, "field 'linearLayoutMiddle'");
    target.linearLayoutMiddle = finder.castView(view, 2131296462, "field 'linearLayoutMiddle'");
    view = finder.findRequiredView(source, 2131296454, "field 'linearLayoutBottom'");
    target.linearLayoutBottom = finder.castView(view, 2131296454, "field 'linearLayoutBottom'");
  }

  @Override public void unbind(T target) {
    target.segmentDm = null;
    target.segmentCt = null;
    target.segmentRgb = null;
    target.segmentMusic = null;
    target.ivLeftMenu = null;
    target.textViewConnectCount = null;
    target.ivRightMenu = null;
    target.rgBottom = null;
    target.rbFirst = null;
    target.rbSecond = null;
    target.rbThrid = null;
    target.rbFourth = null;
    target.rbFifth = null;
    target.avtivity_main = null;
    target.left_menu = null;
    target.right_menu = null;
    target.TopItem = null;
    target.shakeView = null;
    target.linearLayoutMiddle = null;
    target.linearLayoutBottom = null;
  }
}
