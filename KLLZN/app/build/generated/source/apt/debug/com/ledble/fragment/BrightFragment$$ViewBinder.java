// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class BrightFragment$$ViewBinder<T extends com.ledble.fragment.BrightFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131296598, "field 'seekBarRedBrightNess'");
    target.seekBarRedBrightNess = finder.castView(view, 2131296598, "field 'seekBarRedBrightNess'");
    view = finder.findRequiredView(source, 2131296717, "field 'tvBrightness1'");
    target.tvBrightness1 = finder.castView(view, 2131296717, "field 'tvBrightness1'");
    view = finder.findRequiredView(source, 2131296595, "field 'seekBarGreenBrightNess'");
    target.seekBarGreenBrightNess = finder.castView(view, 2131296595, "field 'seekBarGreenBrightNess'");
    view = finder.findRequiredView(source, 2131296718, "field 'tvBrightness2'");
    target.tvBrightness2 = finder.castView(view, 2131296718, "field 'tvBrightness2'");
    view = finder.findRequiredView(source, 2131296590, "field 'seekBarBlueBrightNess'");
    target.seekBarBlueBrightNess = finder.castView(view, 2131296590, "field 'seekBarBlueBrightNess'");
    view = finder.findRequiredView(source, 2131296719, "field 'tvBrightness3'");
    target.tvBrightness3 = finder.castView(view, 2131296719, "field 'tvBrightness3'");
    view = finder.findRequiredView(source, 2131296415, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131296415, "field 'imageViewOnOff'");
  }

  @Override public void unbind(T target) {
    target.seekBarRedBrightNess = null;
    target.tvBrightness1 = null;
    target.seekBarGreenBrightNess = null;
    target.tvBrightness2 = null;
    target.seekBarBlueBrightNess = null;
    target.tvBrightness3 = null;
    target.imageViewOnOff = null;
  }
}
