package com.ledble.fragment;

import java.util.ArrayList;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import butterknife.Bind;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.Tool;
import com.common.view.Segment;
import com.common.view.SegmentedRadioGroup;
import com.kllzn.R;
import com.ledble.activity.MainActivity;
import com.ledble.adapter.ModelAdapter;
import com.ledble.base.LedBleFragment;
import com.ledble.bean.AdapterBean;
import com.ledble.view.MyColorPickerImageView;
import com.ledble.view.MyColorPickerImageView.OnTouchPixListener;

/**
 * 色温
 * 
 * @author ftl
 *
 */
public class BrightFragment extends LedBleFragment {

	@Bind(R.id.seekBarRedBrightNess) SeekBar seekBarRedBrightNess;
	@Bind(R.id.tvBrightness1) TextView tvBrightness1;

	@Bind(R.id.seekBarGreenBrightNess) SeekBar seekBarGreenBrightNess;
	@Bind(R.id.tvBrightness2) TextView tvBrightness2;

	@Bind(R.id.seekBarBlueBrightNess) SeekBar seekBarBlueBrightNess;
	@Bind(R.id.tvBrightness3) TextView tvBrightness3;
	@Bind(R.id.imageViewOnOff) Button imageViewOnOff;
	

	private int offOnBtnState;
	private ToggleButton tgbtn;

	private View mContentView;
	private MainActivity mActivity;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		mContentView = inflater.inflate(R.layout.fragment_brightness, container, false);
		return mContentView;
	}

	@Override
	public void initData() {
		mActivity = (MainActivity) getActivity();
	}

	@Override
	public void initView() {

		final int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff, 0xff00ffff, 0xffff00ff };

		this.seekBarRedBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//				int p = progress;
//				if (progress<=0) {
//					p = 1;
//				}else if (progress >= 100) {
//					p = 100;
//				}
//				
//				if (p > 93) {
//			        return;
//				}
//				p = (p + 3)/3;
				
				tvBrightness1.setText(Integer.toString(progress));
				setSmartBrightness(progress, 1);
//				updateColor(colors[0]);
//				tgbtn.setBackgroundColor(colors[0]);
				
			}
		});
		this.seekBarGreenBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//				int p = progress;
//				if (progress<=0) {
//					p = 1;
//				}else if (progress >= 100) {
//					p = 100;
//				}
//				
//				if (p > 93) {
//			        return;
//				}
//				p = (p + 3)/3;
				
				tvBrightness2.setText(Integer.toString(progress));
				setSmartBrightness(progress, 2);
//				updateColor(colors[1]);
//				tgbtn.setBackgroundColor(colors[1]);
			}
		});
		this.seekBarBlueBrightNess.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

//				int p = progress;
//				if (progress<=0) {
//					p = 1;
//				}else if (progress >= 100) {
//					p = 100;
//				}
//				
//				if (p > 93) {
//			        return;
//				}
//				p = (p + 3)/3;
				
				tvBrightness3.setText(Integer.toString(progress));
				setSmartBrightness(progress, 3);
//				updateColor(colors[2]);
//				tgbtn.setBackgroundColor(colors[2]);
			}
		});
		
		imageViewOnOff.bringToFront();
		imageViewOnOff.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				if (offOnBtnState == 0) {
					imageViewOnOff.setBackgroundResource(R.drawable.on_btn);
					offOnBtnState = 1;
//					mActivity.turnOn();

				} else {
					imageViewOnOff.setBackgroundResource(R.drawable.off_btn);
					offOnBtnState = 0;
//					mActivity.turnOff();
				}
			}
		});
		

		tgbtn = (ToggleButton) mContentView.findViewById(R.id.toggleButton1);
		tgbtn.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mActivity.isLightOpen = isChecked;
				if (isChecked) {
					mActivity.open();
				} else {
					mActivity.close();
				}
			}

		});
	}

	@Override
	public void initEvent() {

	}

	public void updateColor(int color) {
		// tgbtn.setChecked(mActivity.isLightOpen);
		// seekBarRedBrightNess.setProgress(mActivity.brightness);
		// seekBar2.setProgress(mActivity.brightness);
		int rgb[] = Tool.getRGB(color);
		mActivity.setRgb(rgb[0], rgb[1], rgb[2]);
	}

	private void setSmartBrightness(int progress, int mode) {
//		mActivity.setSmartBrightness(progress, mode);

	}

}
