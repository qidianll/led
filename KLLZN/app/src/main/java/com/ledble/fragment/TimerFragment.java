package com.ledble.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import butterknife.Bind;

import java.util.ArrayList;
import java.util.List;

import com.kllzn.R;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.ledble.activity.ElectricSlideDoorActivity;
import com.ledble.activity.ElectricTrailerHookActivity;
import com.ledble.activity.MainActivity;
import com.ledble.activity.ModeActivity;
import com.ledble.activity.MusicActivity;
import com.ledble.activity.MusicLibActivity;
import com.ledble.activity.RGBActivity;
import com.ledble.activity.ElectricPedalActivity;
import com.ledble.activity.SceneActivity;
import com.ledble.base.LedBleApplication;
import com.ledble.base.LedBleFragment;
import com.ledble.net.NetConnectBle;

/**
 * 定时
 * @author ftl
 *
 */
public class TimerFragment extends LedBleFragment {
	
	@Bind(R.id.toggleButtonTop) ToggleButton toggleButtonTop;
	@Bind(R.id.toggleButtonLeft) ToggleButton toggleButtonLeft;
	@Bind(R.id.toggleButtonRight) ToggleButton toggleButtonRight;
	@Bind(R.id.toggleButtonBotton) ToggleButton toggleButtonBotton;
	@Bind(R.id.textViewSelectedLamp) TextView textViewSelectedLamp;
	
	@Bind(R.id.linearLayoutDynamic) LinearLayout linearLayoutDynamic;
	@Bind(R.id.linearLayoutRGB) LinearLayout linearLayoutRGB;
	@Bind(R.id.linearLayoutScene) LinearLayout linearLayoutScene;
	@Bind(R.id.linearLayoutMusic) LinearLayout linearLayoutMusic;
	
//	@Bind(R.id.frameLayoutButtonOnOff) LinearLayout frameLayoutButtonOnOff;
//	@Bind(R.id.toggleButtonOnOff) ToggleButton toggleButtonOnOff;
//	@Bind(R.id.textViewOnOff) TextView textViewOnOff;
	
	@Bind(R.id.linearLayoutTop) LinearLayout linearLayoutTop;
	
	private ToggleButton toggleButton;

	private String position;
	
	private boolean isCheckedTop = false;
	private boolean isCheckedLeft = false;
	private boolean isCheckedRight = false;
	private boolean isCheckedBotton = false;
	
	private boolean canCheckedTop = false;
	private boolean canCheckedLeft = false;
	private boolean canCheckedRight = false;
	private boolean canCheckedBotton = false;
	
	private String topDevName = "";
	private String leftDevName = "";
	private String rightDevName = "";
	private String bottonDevName = "";
	
	private boolean frameLayoutButtonChecked = false;
//	private boolean toggleButtonChecked = false;
	
	private final int INT_TIMER_ON = 11;
	private final int INT_TIMER_OFF = 12;
	private MainActivity mActivity;
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container,  Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_timer, container, false);
	}

	@Override
	public void initData() {
		mActivity = (MainActivity) getActivity();
		
//		mActivity.setGroupNameArr(groupNameArr);
	}

	@Override
	public void initView() {

		
		linearLayoutDynamic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) { 
//				Intent chipSelectIntent = new Intent(mActivity, ModeActivity.class);
				Intent chipSelectIntent = new Intent(mActivity, ElectricPedalActivity.class);
				startActivity(chipSelectIntent);
			}
		});
		
		linearLayoutRGB.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				Intent chipSelectIntent = new Intent(mActivity, ElectricTrailerHookActivity.class);
				startActivity(chipSelectIntent);				

			}
		});
		
		linearLayoutScene.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent chipSelectIntent = new Intent(mActivity, ElectricSlideDoorActivity.class);
				startActivity(chipSelectIntent);
			}
		});
		
		linearLayoutMusic.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
//				Intent chipSelectIntent = new Intent(mActivity, MusicActivity.class);
//				startActivity(chipSelectIntent);
			}
		});
		
		
		toggleButtonTop.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {		
				
				if (canCheckedTop) {
					isCheckedTop = true;
					isCheckedLeft = false;
					isCheckedRight = false;
					isCheckedBotton = false;
					if (isChecked) {
						toggleButtonTop.setBackgroundResource(R.drawable.light_on);
						textViewSelectedLamp.setText(
								getString(R.string.selected_lamp) + getCheckTitle(getString(R.string.above)));
//						SharePersistent.savePerference(mActivity, deviceName, getString(R.string.behind));
					}else {
						toggleButtonTop.setBackgroundResource(R.drawable.light_off);
						textViewSelectedLamp.setText(getString(R.string.selected_lamp) + getCheckTitle(""));					
					}
					

					SharePersistent.saveBoolean(mActivity, topDevName+"isCheckedTop", isChecked);
					SharePersistent.saveBoolean(mActivity, topDevName+"lightStatus", isChecked);
				}
			}
		});
		
		toggleButtonLeft.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {									
				
				if (canCheckedLeft) {
					isCheckedTop = false;
					isCheckedLeft = true;
					isCheckedRight = false;
					isCheckedBotton = false;
					
					if (isChecked) {
						toggleButtonLeft.setBackgroundResource(R.drawable.light_on);
						textViewSelectedLamp.setText(
								getString(R.string.selected_lamp) + getCheckTitle(getString(R.string.front)));
					}else {
						toggleButtonLeft.setBackgroundResource(R.drawable.light_off);
						textViewSelectedLamp.setText(getString(R.string.selected_lamp) + getCheckTitle(""));
					}
					
					SharePersistent.saveBoolean(mActivity, leftDevName+"isCheckedLeft", isChecked);
					SharePersistent.saveBoolean(mActivity, leftDevName+"lightStatus", isChecked);
				}
				
			}
		});
		
		toggleButtonRight.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {			
				
				if (canCheckedRight) {
					isCheckedTop = false;
					isCheckedLeft = false;
					isCheckedRight = true;
					isCheckedBotton = false;
					if (isChecked) {
						toggleButtonRight.setBackgroundResource(R.drawable.light_on);
						textViewSelectedLamp.setText(
								getString(R.string.selected_lamp) + getCheckTitle(getString(R.string.behind)));
					}else {
						toggleButtonRight.setBackgroundResource(R.drawable.light_off);
						textViewSelectedLamp.setText(getString(R.string.selected_lamp) + getCheckTitle(""));
					}
					
					SharePersistent.saveBoolean(mActivity, rightDevName+"isCheckedRight", isChecked);
					SharePersistent.saveBoolean(mActivity, rightDevName+"lightStatus", isChecked);
				}
				
			}
		});
		
		toggleButtonBotton.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {	
				
				if (canCheckedBotton) {
					isCheckedTop = false;
					isCheckedLeft = false;
					isCheckedRight = false;
					isCheckedBotton = true;
					if (isChecked) {
						toggleButtonBotton.setBackgroundResource(R.drawable.light_on);
						textViewSelectedLamp.setText(
								getString(R.string.selected_lamp) + getCheckTitle(getString(R.string.below)));
					}else {
						toggleButtonBotton.setBackgroundResource(R.drawable.light_off);
						textViewSelectedLamp.setText(getString(R.string.selected_lamp) + getCheckTitle(""));
					}
					
					SharePersistent.saveBoolean(mActivity, bottonDevName+"isCheckedBotton", isChecked);
					SharePersistent.saveBoolean(mActivity, bottonDevName+"lightStatus", isChecked);
				}				
			}
		});
		
	}
	
	private String titleChecked = "";
	private ArrayList<String> groupNameArr = new ArrayList<String>();
//	private ArrayList<String> bottonNameArr = new ArrayList<String>();
	private String getCheckTitle(String str) {
		
		if (isCheckedTop) {
			if (!titleChecked.contains(getString(R.string.above))) { // 不包含
				titleChecked += str + " ";
				groupNameArr.add(topDevName);
			}
			if (str.equalsIgnoreCase("")) {
				if (null != groupNameArr && groupNameArr.size() > 0) {
					groupNameArr.remove(topDevName);
					titleChecked = titleChecked.replace(getString(R.string.above) + " ", str);
					return titleChecked;
				}				
			}

		} else if (isCheckedLeft) {
			if (!titleChecked.contains(getString(R.string.front))) {
				titleChecked += str + " ";
				groupNameArr.add(leftDevName);
			}
			if (str.equalsIgnoreCase("")) {
				if (null != groupNameArr && groupNameArr.size() > 0) {
					groupNameArr.remove(leftDevName);
					titleChecked = titleChecked.replace(getString(R.string.front) + " ", str);
					return titleChecked;
				}				
			}

		} else if (isCheckedRight) {
			if (!titleChecked.contains(getString(R.string.behind))) {
				titleChecked += str + " ";
				groupNameArr.add(rightDevName);
			}
			if (str.equalsIgnoreCase("")) {
				if (null != groupNameArr && groupNameArr.size() > 0) {
					groupNameArr.remove(rightDevName);
					titleChecked = titleChecked.replace(getString(R.string.behind) + " ", str);
					return titleChecked;
				}				
			}

		} else if (isCheckedBotton) {
			if (!titleChecked.contains(getString(R.string.below))) {
				titleChecked += str + " ";
				groupNameArr.add(bottonDevName);
			}
			if (str.equalsIgnoreCase("")) {
				if (null != groupNameArr && groupNameArr.size() > 0) {
					groupNameArr.remove(bottonDevName);
					titleChecked = titleChecked.replace(getString(R.string.below) + " ", str);
					return titleChecked;
				}				
			}
		}
		
//		mActivity.setGroupNameArr(groupNameArr); //设置

		return titleChecked;
	}

	@Override
	public void initEvent() {
		
	}
	
	/**
	 * 获取控制的灯
	 */
	public ToggleButton geToggleButton() {
		if (toggleButton != null) {
			return toggleButton;
		}
		return null;
	}
	
	/**
	 * 获取控制的灯
	 */
	public TextView getTextViewSelectedLamp() {
		if (textViewSelectedLamp != null) {
			return textViewSelectedLamp;
		}
		return null;
	}
	
	/**
	 * 获取控制的灯所在的LinearLayout
	 */
	public LinearLayout getLinearLayoutTop() {
		if (linearLayoutTop != null) {
			return linearLayoutTop;
		}
		return null;
	}
	
	public void setCanClickLamp(String position) {
		 this.position = position;
	}
	
	public void setCheckedTop(boolean check, String deviceName) {
		this.topDevName = deviceName;
		if (null != toggleButtonTop) {
			toggleButtonTop.setChecked(check);
		}
	}
	
	public void setCheckedLeft(boolean check, String deviceName) {
		this.leftDevName = deviceName;
		if (null != toggleButtonLeft) {
			toggleButtonLeft.setChecked(check);
		}
	}
	
	public void setCheckedRight(boolean check, String deviceName) {
		this.rightDevName = deviceName;
		if (null != toggleButtonRight) {
			toggleButtonRight.setChecked(check);
		}
	}
	
	public void setCheckedBotton(boolean check, String deviceName) {
		this.bottonDevName = deviceName;
		if (null != toggleButtonBotton) {
			toggleButtonBotton.setChecked(check);
		}
	}
	
	public void canCheckedTop(boolean canCheck) {
		this.canCheckedTop = canCheck;		
	}
	
	public void canCheckedLeft(boolean canCheck) {
		this.canCheckedLeft = canCheck;		
	}
	
	public void canCheckedRight(boolean canCheck) {
		this.canCheckedRight = canCheck;		
	}
	
	public void canCheckedBotton(boolean canCheck) {
		this.canCheckedBotton = canCheck;		
	}
	
	public void setIsCheckedTop(boolean check) {
		this.isCheckedTop = check;
	}
	
	public void setIsCheckedLeft(boolean check) {
		this.isCheckedLeft = check;
	}
	
	public void setIsCheckedRight(boolean check) {
		this.isCheckedRight = check;
	}
	
	public void setIsCheckedBotton(boolean check) {
		this.isCheckedBotton = check;
	}
	
	public void changeCheckedButton(String positionName) {
		if (null != textViewSelectedLamp) {
			textViewSelectedLamp.setText(
					getString(R.string.selected_lamp) + getCheckTitle(positionName));
		}		
	}
	
	public void setButtonOnOff(Boolean onOff) {	
		
	}
	
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		if (requestCode == INT_TIMER_ON && resultCode == Activity.RESULT_OK) {			
//			return;
//		}
//		if (requestCode == INT_TIMER_OFF && resultCode == Activity.RESULT_OK) {			
//			return;
//		}
	}

}
