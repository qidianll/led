package com.ledble.adapter;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kllzn.R;
import com.ledble.adapter.BleSelectDeviceAdapter.ViewHolder;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.AdapterBean;

public class NewDeviceAdapter extends BaseAdapter {

	private ArrayList<AdapterBean> list;
	private Context context;
	private int index = -1;

	public NewDeviceAdapter(Context context, ArrayList<AdapterBean> list) {
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return Integer.parseInt(list.get(position).getValue().trim());
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder vh = null;
		if (null == convertView) {
			convertView = View.inflate(context, R.layout.layout_listitem_device_select, null);
			vh = new ViewHolder();
			vh.deviceAddress = (TextView) convertView.findViewById(R.id.device_address);
			vh.deviceName = (TextView) convertView.findViewById(R.id.device_name);
			vh.imageViewHook = (ImageView) convertView.findViewById(R.id.imageViewHook);
			convertView.setTag(vh);
		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		
		BluetoothDevice device = LedBleApplication.getApp().getBleDevices().get(position);
		final String deviceName = device.getName();
		if (deviceName != null && deviceName.length() > 0) {
			vh.deviceName.setText(deviceName);
		} else {
			vh.deviceName.setText("unknown device");
		}
		vh.deviceAddress.setText(device.getAddress());
		
		return convertView;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	
	public BluetoothDevice getDevice(int position) {
		return LedBleApplication.getApp().getBleDevices().get(position);
	}

	class ViewHolder {
		TextView deviceName;
		TextView deviceAddress;
		ImageView imageViewHook;
	}
}
