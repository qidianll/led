package com.ledble.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.common.BaseActivity;
import com.common.adapter.AnimationListenerAdapter;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.common.uitl.ListUtiles;
import com.common.uitl.SharePersistent;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.ledble.adapter.ModelAdapter;
import com.ledble.bean.AdapterBean;
import com.ledble.bean.MyColor;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView4RGB;
import com.ledble.view.PickerView;
import com.ledble.view.PickerView.onSelectListener;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.kllzn.R;

public class ModeActivity extends TimerActivity {
	
	private MainActivity mActivity;
	
	private SegmentedRadioGroup segmentedRadioGroup;
	private View relativeTab2;// 模式
	private View relativeTab3;// 自定义
	
//	private ToggleButton buttonPlay;
//	private ToggleButton imageViewOnOff;	
	
	private PickerView listViewModel;
//	private WheelModelAdapter wheelAdapterModel;
	
//	private WheelView listViewOnOff;
//	private WheelModelAdapter wheelAdapterOnOff;
	
	private SeekBar seekBarSpeedBar;
	private TextView textViewSpeed;
	
	private SeekBar seekBarBrightness;
	private TextView textViewBrightness;
	
	private SeekBar seekBarPixelPoint;
	private TextView textViewPixelPoint;
	
	private ModelAdapter maAdapter;
	
	private Button backButton;
	private Button viewArrowLeft;
	
	private Button linearLayoutMusic;
	private Button linearLayoutCustom;
	private Button linearLayoutTimer;
	
	
	
	//**********转向流光*************//
	
	private WheelView listViewLightModel;
	private WheelModelAdapter wheelAdapterLightModel;
	
	private WheelView listViewBrakeModel; // 刹车
	private WheelModelAdapter wheelAdapteBrake;
	private WheelView listViewReverse;	// 倒车
	private WheelModelAdapter wheelAdapterReverse;
	private WheelView listViewDoublFlicker; // 双闪
	private WheelModelAdapter wheelAdapterDoublFlicker;
	
	private SegmentedRadioGroup dynamicChangeGroup;
	private Button buttonRunButton;//运行
//	private Button buttonBackButton;
	private Button buttonConfirButton;
	
	private SeekBar seekBarSpeedCustom;
	private TextView textViewSpeedCustom;
	private SeekBar seekBarBrightCustom;
	private TextView textViewBrightCustom;
	
	private TextView textRGB;
	private TextView textViewRingBrightSC;

	private View relativeTabColorCover;
	private int currentSelecColorFromPicker;
	private ColorTextView actionView;
	private ArrayList<ColorTextView> colorTextViews;
	private MyColorPickerImageView4RGB imageViewPicker ;
	private BlackWiteSelectView blackWiteSelectView ;
	private SharedPreferences sp;
	private int style = 0;
	
	private static final int COLOR_DEFALUT = 0;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//		//隐藏标题  
//	    requestWindowFeature(Window.FEATURE_NO_TITLE);        
//	    //定义全屏参数  
//	    int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;  
//	    //获得窗口对象  
//	    Window myWindow = this.getWindow();  
//	    //设置Flag标识  
//	    myWindow.setFlags(flag, flag);

		initView();
	}

	@Override
	public void initView() {
//		super.initView();
		setContentView(R.layout.activity_mode);
//		setContentView(R.layout.activity_timer_setting);
		
		mActivity = MainActivity.getMainActivity();
		 
		relativeTab2 = findViewById(R.id.relativeTab2);
		relativeTab3 = findViewById(R.id.relativeTab3);
		segmentedRadioGroup = (SegmentedRadioGroup)findViewById(R.id.segmentedRadioGroup);
		segmentedRadioGroup.check(R.id.One);
		segmentedRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				currentStatus =checkedId;
				
				if (R.id.One == checkedId) {
					relativeTab2.setVisibility(View.VISIBLE); //模式
					relativeTab3.setVisibility(View.GONE);
//					SharePersistent.savePerference(mActivity, "checkedId", 0);// 保存选择的 Tab
				} else if((R.id.Two == checkedId)) {
					relativeTab3.setVisibility(View.VISIBLE); //自定义
					relativeTab2.setVisibility(View.GONE);
//					SharePersistent.savePerference(mActivity, "checkedId", 2);// 保存选择的 Tab
				}
			}
		});
		 
	
		
		final String[] timerModel = getResources().getStringArray(R.array.color_model);
		final String[] modelStrings = new String[timerModel.length];
		for (int i = 0; i < timerModel.length; i++) {
			String[] datas = timerModel[i].split(",");
			modelStrings[i] = datas[0];
		}
		
		List<String> data = new ArrayList<String>();
		for (int i = 0; i < timerModel.length; i++)
		{
			data.add(modelStrings[i]);
		}
		this.listViewModel = (PickerView) findViewById(R.id.listViewModel);
		this.listViewModel.setData(data);
		this.listViewModel.setSelected(SharePersistent.getInt(mActivity, "MODE_ROW"));
		listViewModel.setOnSelectListener(new onSelectListener()
		{
			@Override
			public void onSelect(String text, int row)
			{
				String[] datas = text.split(":");
				if (datas.length == 2) {
					MainActivity.getMainActivity().setRegMode(Integer.parseInt(datas[0]));			
					SharePersistent.savePerference(mActivity, "MODE_ROW", Integer.parseInt(datas[0]));
				}else {
					MainActivity.getMainActivity().setRegMode(255);
					SharePersistent.savePerference(mActivity, "MODE_ROW", 0);
				}								
			}
		});	

		// 返回按钮
		backButton = findButtonById(R.id.buttonBack1);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		// 返回按钮
		viewArrowLeft = findButtonById(R.id.viewArrowLeft);
		viewArrowLeft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		
		
		this.seekBarSpeedBar = (SeekBar) findViewById(R.id.seekBarSpeed);
		this.textViewSpeed = (TextView) findTextViewById(R.id.textViewSpeed);
		this.seekBarSpeedBar.setProgress(95);
		this.textViewSpeed.setText(getResources().getString(R.string.speed_set, 95));
		this.seekBarSpeedBar.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 100) {
					for (int i = 0; i < 5; i++) {
						MainActivity.getMainActivity().setSpeed(progress);
//						NetConnectBle.getInstance().setSpeed(progress);
					}					
					textViewSpeed.setText(getResources().getString(R.string.speed_set, progress));

				}
			}
		});
		
		this.seekBarBrightness = (SeekBar) findViewById(R.id.seekBarBrightNess);
		this.textViewBrightness = (TextView) findTextViewById(R.id.textViewBrightNess);
		this.seekBarBrightness.setProgress(95);
		this.textViewBrightness.setText(getResources().getString(R.string.brightness_set, 95));
		this.seekBarBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 100) {	
					
					MainActivity.getMainActivity().setBrightNess(progress);	
					
					int p = progress;
//					p = progress*100/32;
					textViewBrightness.setText(getResources().getString(R.string.brightness_set, p));									

				}
			}
		});
		
		this.seekBarPixelPoint = (SeekBar) findViewById(R.id.seekBarPixelPoint);
		this.textViewPixelPoint = (TextView) findTextViewById(R.id.textViewPixelPoint);
		this.seekBarPixelPoint.setProgress(35);
		this.textViewPixelPoint.setText(getResources().getString(R.string.pixel_set, 35));
		this.seekBarPixelPoint.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress >= 0 && progress <= 200) {
//					for (int i = 0; i < 5; i++) {
						MainActivity.getMainActivity().setPixNub(progress);
//						NetConnectBle.getInstance().setSpeed(progress);
//					}					
					textViewPixelPoint.setText(getResources().getString(R.string.pixel_set, progress));

				}
			}
		});
		
		
		
		linearLayoutMusic = findButtonById(R.id.linearLayoutMusic);
		linearLayoutMusic.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {;
				Intent chipSelectIntent = new Intent(mActivity, MusicActivity.class);
				startActivity(chipSelectIntent);
			}
		});
		
		linearLayoutCustom = findButtonById(R.id.linearLayoutCustom);
		linearLayoutCustom.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {;
				Intent chipSelectIntent = new Intent(mActivity, CustomActivity.class);
				startActivity(chipSelectIntent);
			}
		});
		
		linearLayoutTimer = findButtonById(R.id.linearLayoutTimer);
		linearLayoutTimer.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {;
				Intent chipSelectIntent = new Intent(mActivity, TimerActivity.class);
				startActivity(chipSelectIntent);
			}
		});
		
		
		
		//**************************************安全警示***************************************************//
		
		this.sp = getSharedPreferences(Constant.LIGHT_DIY, Context.MODE_PRIVATE);
		
		final String[] lishtModel = getResources().getStringArray(R.array.turn_mode);
		final String[] LightModelStrings = new String[lishtModel.length];
		for (int i = 0; i < lishtModel.length; i++) {
			String[] datas = lishtModel[i].split(",");
			LightModelStrings[i] = datas[0];
		}
		this.listViewLightModel = (WheelView) findViewById(R.id.listViewLightModel);		
		wheelAdapterLightModel = new WheelModelAdapter(this, LightModelStrings);
		this.listViewLightModel.setViewAdapter(wheelAdapterLightModel);	
		this.listViewLightModel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = lishtModel[newValue].split(",");
				style = Integer.parseInt(datas[1]);
				
				mActivity.setLampTurnMode(getSelectColor(0), Integer.parseInt(datas[1]));
			}
		});	
		
		
		textViewRingBrightSC = (TextView)findViewById(R.id.tvRingBrightnessSC);
		
		textViewSpeedCustom = (TextView)findTextViewById(R.id.textViewSpeedCustom);
		seekBarSpeedCustom = (SeekBar)findViewById(R.id.seekBarSpeedCustom);
		this.seekBarSpeedCustom.setProgress(95);
		this.textViewSpeedCustom.setText(getResources().getString(R.string.speed_set, 95));
		this.seekBarSpeedCustom.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					mActivity.setLampTurnSpeed(1);
					textViewSpeedCustom.setText(getResources().getString(R.string.speed_set, 1));
		
				} else {
					mActivity.setLampTurnSpeed(progress);
					textViewSpeedCustom.setText(getResources().getString(R.string.speed_set, progress));
				}
			}
		});
		
		textViewBrightCustom = (TextView)findTextViewById(R.id.textViewBrightCustom);
		seekBarBrightCustom = (SeekBar)findViewById(R.id.seekBarBrightCustom);
		this.seekBarBrightCustom.setProgress(95);
		this.textViewBrightCustom.setText(getResources().getString(R.string.brightness_set, 95));
		this.seekBarBrightCustom.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					mActivity.setLampTurnBrightness(1);
					textViewBrightCustom.setText(getResources().getString(R.string.brightness_set, 1));		
				} else {
					mActivity.setLampTurnBrightness(progress);
					textViewBrightCustom.setText(getResources().getString(R.string.brightness_set, progress));
				}
			}
		});
		
		
//		buttonRunButton = findButtonById(R.id.button1);
//		buttonRunButton.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
////				putDataBack(getSelectColor());
//			}
//		});
		
		

		final String[] brakeModel = getResources().getStringArray(R.array.brake_mode);
		final String[] brakeModelStrings = new String[brakeModel.length];
		for (int i = 0; i < brakeModel.length; i++) {
			String[] datas = brakeModel[i].split(",");
			brakeModelStrings[i] = datas[0];
		}
		listViewBrakeModel = (WheelView) findViewById(R.id.listViewBrakeModel);		
		wheelAdapteBrake = new WheelModelAdapter(this, brakeModelStrings);
		listViewBrakeModel.setViewAdapter(wheelAdapteBrake);	
		listViewBrakeModel.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = brakeModel[newValue].split(",");
//				style = Integer.parseInt(datas[1]);
//				mActivity.setLampOtherMode(1, Integer.parseInt(datas[1]));
				mActivity.setBrakeMode(getSelectColor(1), Integer.parseInt(datas[1]));
			}
		});	
		
		final String[] backModel = getResources().getStringArray(R.array.back_mode);
		final String[] backModelStrings = new String[backModel.length];
		for (int i = 0; i < backModel.length; i++) {
			String[] datas = backModel[i].split(",");
			backModelStrings[i] = datas[0];
		}
		listViewReverse = (WheelView) findViewById(R.id.listViewReverse);		
		wheelAdapterReverse = new WheelModelAdapter(this, backModelStrings);
		listViewReverse.setViewAdapter(wheelAdapterReverse);	
		listViewReverse.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = backModel[newValue].split(",");
//				style = Integer.parseInt(datas[1]);
//				mActivity.setLampOtherMode(2, Integer.parseInt(datas[1]));
				mActivity.setReverseMode(getSelectColor(2), Integer.parseInt(datas[1]));
			}
		});	
		
		final String[] flashModel = getResources().getStringArray(R.array.flash_mode);
		final String[] flashModelStrings = new String[flashModel.length];
		for (int i = 0; i < flashModel.length; i++) {
			String[] datas = flashModel[i].split(",");
			flashModelStrings[i] = datas[0];
		}
		listViewDoublFlicker = (WheelView) findViewById(R.id.listViewDoublFlicker);		
		wheelAdapterDoublFlicker = new WheelModelAdapter(this, flashModelStrings);
		listViewDoublFlicker.setViewAdapter(wheelAdapterDoublFlicker);	
		listViewDoublFlicker.addChangingListener(new OnWheelChangedListener() {
			@Override
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				String[] datas = flashModel[newValue].split(",");
//				style = Integer.parseInt(datas[1]);
//				mActivity.setLampOtherMode(3, Integer.parseInt(datas[1]));
				mActivity.setFlashMode(getSelectColor(3), Integer.parseInt(datas[1]));
			}
		});	
		
		
		
		// *********************************** 选色框 ******************************************************//
		
		initColorBlock();
		initColorSelecterView();
		
	}
	
	private void putDataBack(ArrayList<MyColor> colors) {

//		Toast.makeText(mActivity, "colors.size = "+colors, Toast.LENGTH_SHORT).show();
		mActivity.setLampTurnMode(colors, style);
	}
	
	private ArrayList<MyColor> getSelectColor(int index) {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		
		if (!ListUtiles.isEmpty(colorTextViews)) {
//			for (ColorTextView ctx : colorTextViews) {
//				if (COLOR_DEFALUT != ctx.getColor()) {
//					int[] rgb = Tool.getRGB(ctx.getColor());
//					colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
//				}else {
//					colorList.add(new MyColor(0, 0, 0));
//				}
//			}		
			
			for (int i = 0; i<colorTextViews.size(); i++) {				
				ColorTextView ctx = colorTextViews.get(i);
				if (index == i) {
					if (COLOR_DEFALUT != ctx.getColor()) {
						int[] rgb = Tool.getRGB(ctx.getColor());
						colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
					}else {
						colorList.add(new MyColor(0, 0, 0));
					}
				}				
			}
		}
		
		return colorList;
	}
	
	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
//		View blocks = findViewById(R.id.linearLayoutViewBlocks);
		colorTextViews = new ArrayList<ColorTextView>();
		for (int i = 1; i <= 4/*16*/; i++) {
			final ColorTextView tv = (ColorTextView) relativeTab3.findViewWithTag((String.valueOf("editColor") + i));
			String tag = (String) tv.getTag();
			int color = sp.getInt(tag, COLOR_DEFALUT);
			if(color != COLOR_DEFALUT){
				int radius = 10;
				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
				// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null); 
				// 组合圆角矩形和ShapeDrawable
				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape); 
				// 设置形状的颜色
				shapeDrawable.getPaint().setColor(color); 
				// 设置绘制方式为填充
				shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
				// 将当前选择得颜色设置到触发颜色编辑器得View上
				tv.setBackgroundDrawable(shapeDrawable);
				tv.setColor(color);
				tv.setText("");
			}
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(ModeActivity.this, R.anim.layout_scale));
					int color = tv.getColor();
					if(color == COLOR_DEFALUT) {
						showColorCover((ColorTextView) v);
//						Toast.makeText(mActivity, "showColorCover", Toast.LENGTH_SHORT).show();
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(COLOR_DEFALUT);
					String tag = (String) tv.getTag();
					sp.edit().putInt(tag, COLOR_DEFALUT).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
			colorTextViews.add(tv);
		}

	}

	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 */
	private void showColorCover(ColorTextView actionView) {
		this.actionView = actionView;
		//数据初始化
		currentSelecColorFromPicker = COLOR_DEFALUT;
		textRGB.setText(getResources().getString(R.string.r_g_b, 0, 0, 0));
		
		Animation slideInAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_in);
		slideInAnimation.setAnimationListener(new AnimationListenerAdapter() {
			@Override
			public void onAnimationStart(Animation animation) {
				relativeTabColorCover.setVisibility(View.VISIBLE);
				blackWiteSelectView.setVisibility(View.INVISIBLE);
				textViewRingBrightSC.setVisibility(View.INVISIBLE);
			}
		});
		relativeTabColorCover.startAnimation(slideInAnimation);
	}

	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		Animation slideInAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_bottom_out);
		slideInAnimation.setAnimationListener(new AnimationListenerAdapter() {
			@Override
			public void onAnimationEnd(Animation animation) {
				relativeTabColorCover.setVisibility(View.GONE);
			}
		});
		relativeTabColorCover.startAnimation(slideInAnimation);
	}

	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {
		
		relativeTabColorCover = findViewById(R.id.relativeTabColorCover);
		textRGB = (TextView)findViewById(R.id.tvRGB);
		
		imageViewPicker = (MyColorPickerImageView4RGB) findViewById(R.id.imageViewPicker);
		blackWiteSelectView = (BlackWiteSelectView) findViewById(R.id.blackWiteSelectView);
		imageViewPicker.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView.setStartColor(color);
				currentSelecColorFromPicker = color;
				
				int[] colors = Tool.getRGB(color);
				textRGB.setText(getResources().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
//				NetConnectBle.getInstance().setRgb(colors[0], colors[1], colors[2]);
				mActivity.setRgbKLL(colors[0], colors[1], colors[2]);
			}
		});

		blackWiteSelectView.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color,int progress) {
				currentSelecColorFromPicker = color;
				int p = progress;
				if (progress <= 0) {
					p = 1;
				}
				if (progress >= 100) {
					p = 100;
				}
				textViewRingBrightSC.setText(getResources().getString(R.string.brightness_set, p));
//				NetConnectBle.getInstance().setSPIBrightness(p);
				mActivity.setBrightNess(p);
			}
		});

		View viewColors = findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
		int[] colors = { 0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffffff,  0xffffff00,  0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);
		
		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView.setStartColor(selectColor);
					imageViewPicker.move2Ege(rmap.get(selectColor));
					
					int[] colors = Tool.getRGB(selectColor);
					textRGB.setText(getResources().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
//					NetConnectBle.getInstance().setRgb(colors[0], colors[1], colors[2]);
					mActivity.setRgbKLL(colors[0], colors[1], colors[2]);
				}
			});
			viewCsArrayLis.add(vc);
		}
		buttonConfirButton = findButtonById(R.id.buttonConfirm);
		buttonConfirButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideColorCover();
				if (currentSelecColorFromPicker == COLOR_DEFALUT)
					return;
				int radius = 10;
				float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
				RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null); // 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
																						// 就会根据形状来绘制。
				ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape); // 组合圆角矩形和ShapeDrawable
				shapeDrawable.getPaint().setColor(currentSelecColorFromPicker); // 设置形状的颜色
				shapeDrawable.getPaint().setStyle(Paint.Style.FILL); // 设置绘制方式为填充

				actionView.setColor(currentSelecColorFromPicker);
				// 将当前选择得颜色设置到触发颜色编辑器得View上
				actionView.setBackgroundDrawable(shapeDrawable);
				// 保存颜色值
				String tag = (String) actionView.getTag();
				sp.edit().putInt(tag, currentSelecColorFromPicker).commit();
				actionView.setText("");
			}
		});
	}

	private ModelAdapter buildTurnModel() {
		String[] ary = getResources().getStringArray(R.array.turn_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(getApplicationContext(), abs);
		return maAdapter;
	}

}
