package com.ledble.activity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.common.BaseActivity;
import com.ledble.bean.MyColor;

import java.util.ArrayList;

import com.kllzn.R;

public class SceneActivity extends BaseActivity {

	private Button backButton;
	private ImageView imageViewSceneSunrise;
	private ImageView imageViewSceneSunset;
	private ImageView imageViewSceneColorful;
	private ImageView imageViewSceneAfternoontea;
	private ImageView imageViewSceneDrivemidge;
	private ImageView imageViewSceneYoga;
	private ImageView imageViewSceneParty;
	private ImageView imageViewSceneTropical;
	private ImageView imageViewSceneSea;
	private ImageView imageViewSceneReading;
	private ImageView imageViewSceneCandlelightdinner;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_scene);
	
		
		
		// 日出模式
		imageViewSceneSunrise = findImageViewById(R.id.imageViewSceneSunrise);
		imageViewSceneSunrise.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(82);
//					putDataBack(getSelectColor(255, 100, 2), 0, 80);  // style 0渐变  1拖尾  2流水  3追光
					MainActivity.getMainActivity().setRgb(255, 100, 2);
//				}			
			}
		});
		
		// 日落模式
		imageViewSceneSunset = findImageViewById(R.id.imageViewSceneSunset);
		imageViewSceneSunset.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(82);
//					putDataBack(getSelectColor(255, 17, 13), 0, 80);
					MainActivity.getMainActivity().setRgb(255, 17, 13);
//				}				
			}
		});

		// 多彩模式
		imageViewSceneColorful = findImageViewById(R.id.imageViewSceneColorful);
		imageViewSceneColorful.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(52);
//					putDataBack(getSelectColor(214, 177, 255), 0, 80);
					MainActivity.getMainActivity().setRgb(214, 177, 255);
//				}				
			}
		});

		// 下午茶模式
		imageViewSceneAfternoontea = findImageViewById(R.id.imageViewSceneAfternoontea);
		imageViewSceneAfternoontea.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(86);
//					putDataBack(getSelectColor(210, 115, 255), 0, 80);
					MainActivity.getMainActivity().setRgb(210, 115, 255);
//				}				
			}
		});

		// 驱蚊模式
		imageViewSceneDrivemidge = findImageViewById(R.id.imageViewSceneDrivemidge);
		imageViewSceneDrivemidge.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(38);
//					putDataBack(getSelectColor(255, 80, 12), 0, 80);
					MainActivity.getMainActivity().setRgb(255, 80, 12);
//				}			
			}
		});

		// 瑜伽模式
		imageViewSceneYoga = findImageViewById(R.id.imageViewSceneYoga);
		imageViewSceneYoga.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(85);
//					putDataBack(getSelectColor(46, 230, 255), 0, 80);
					MainActivity.getMainActivity().setRgb(46, 230, 255);
//				}			
			}
		});

		// 聚会模式
		imageViewSceneParty = findImageViewById(R.id.imageViewSceneParty);
		imageViewSceneParty.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(69);
//					putDataBack(getSelectColor(220, 14, 255), 1, 80); // style 0渐变  1拖尾  2流水  3追光
					MainActivity.getMainActivity().setRgb(220, 14, 255);
//				}				
			}
		});

		// 热带雨林
		imageViewSceneTropical = findImageViewById(R.id.imageViewSceneTropical);
		imageViewSceneTropical.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(101);
//					putDataBack(getSelectColor(55, 255, 27), 0, 80);
					MainActivity.getMainActivity().setRgb(55, 255, 27);
//				}				
			}
		});

		// 海边模式
		imageViewSceneSea = findImageViewById(R.id.imageViewSceneSea);
		imageViewSceneSea.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(165);
//					putDataBack(getSelectColor(3, 13, 255), 0, 80);
					MainActivity.getMainActivity().setRgb(3, 13, 255);
//				}				
			}
		});

		// 阅读模式
		imageViewSceneReading = findImageViewById(R.id.imageViewSceneReading);
		imageViewSceneReading.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(132);
//					putDataBack(getSelectColor(255, 214, 147), 0, 80);
					MainActivity.getMainActivity().setRgb(255, 214, 147);
//				}				
			}
		});

		// 烛光晚餐
		imageViewSceneCandlelightdinner = findImageViewById(R.id.imageViewSceneCandlelightdinner);
		imageViewSceneCandlelightdinner.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				for (int i = 0; i < 10; i++) {					
//					MainActivity.getMainActivity().setRegMode(31);
//					putDataBack(getSelectColor(252, 97, 2), 0, 80);
					MainActivity.getMainActivity().setRgb(252, 97, 2);
//				}				
			}
		});
		
		
	
		// 返回按钮
		backButton = findButtonById(R.id.buttonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	private void putDataBack(ArrayList<MyColor> colors, int style, int speed) {
		MainActivity.getMainActivity().setDiy(colors, style);
		MainActivity.getMainActivity().setSpeed(speed);
			
	}

	private ArrayList<MyColor> getSelectColor(int r, int g, int b) {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		colorList.add(new MyColor(r, g, b));
				
		return colorList;
	}


}
