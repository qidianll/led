package com.ledble.activity;

import java.util.HashMap;
import java.util.Map;

//import com.alibaba.fastjson.JSON;
//import com.alibaba.fastjson.TypeReference;
//import com.baidu.android.pushservice.PushConstants;
//import com.baidu.android.pushservice.PushManager;
import com.kllzn.R;
//import com.ledble.http.AdvertBean;
//import com.ledble.http.HttpUtil;
//import com.ledble.http.HttpUtil.HttpCallBack;
//import com.ledble.http.ResponseBean;
//import com.ledble.utils.Utils;
import com.ledble.view.VideoView.CustomVideoView;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class LoadingActivity extends Activity {
	
//	private String TAG = "LoadingActivity";
	
    private CustomVideoView videoview;
    private Button btn_start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//去掉标题栏
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//设置全屏
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_loading);
		
//        btn_start = (Button) findViewById(R.id.btn_start);
//        btn_start.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
//				startActivity(intent);
//				finish();
//			}
//		});
		
        videoview = (CustomVideoView) findViewById(R.id.videoview);
        //设置播放加载路径
        videoview.setVideoURI(Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.start));
        //播放
        videoview.start();
        //循环播放
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
//                videoview.start();
                Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
				startActivity(intent);
				finish();
            }
        });
		
		
//		final Handler handler = new Handler(); // 开启 定时搜索 定时器
//		Runnable runnable = new Runnable() {
//			@Override
//			public void run() {
//				// TODO Auto-generated method stub
//				// 在此处添加执行的代码
//				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
//				startActivity(intent);
//				finish();
//			}
//		};
//		handler.postDelayed(runnable, 2000);// 打开定时器，执行操作
		
        // 启动百度push
//        PushManager.startWork(getApplicationContext(), PushConstants.LOGIN_TYPE_API_KEY,
//               Utils.getMetaValue(LoadingActivity.this, "api_key"));
//		
//		String url = "http://120.79.182.171/xpy-server/advert/queryNewAdvert";
//		Map<String, String> params = new HashMap<String, String>();
//		params.put("appId", "1");
//		HttpUtil.getInstance().getSourceData(this, url, params, new HttpCallBack() {
//			
//			@Override
//			public void onSuccess(String result) {
//				ResponseBean<AdvertBean> responseBean = JSON.parseObject(result, new TypeReference<ResponseBean<AdvertBean>>() {});
//				Log.v(TAG, "responseBean:" + responseBean.toString());
//				Intent intent = null;
//				if (responseBean != null && "000000".equals(responseBean.getReturnCode())) {
//					intent = new Intent(LoadingActivity.this, AdvertActivity.class);
//					AdvertBean advertBean = (AdvertBean) responseBean.getContent();
//					intent.putExtra("advertBean", advertBean);
//				} else {
//					intent = new Intent(LoadingActivity.this, MainActivity.class);
//				}
//
//				startActivity(intent);
//				finish();
//			}
//			
//			@Override
//			public void onException(String message) {
//				Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
//				startActivity(intent);
//				finish();
//			}
//		});
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (videoview != null) {
			videoview.pause();
		}		
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		if (videoview != null) {
			videoview.start();
		}
	}

}
