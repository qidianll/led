package com.ledble.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
//import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.common.uitl.SharePersistent;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemClickListener;

//import java.lang.reflect.Array;
import java.util.ArrayList;
//import java.util.Arrays;

import com.common.BaseActivity;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.common.adapter.RGBSelectAdapter;
import com.ledble.bean.AdapterBean;
import com.kllzn.R;

public class ElectricPedalActivity extends BaseActivity {

	private Context mContext;
	private MainActivity mActivity;
	
	private ToggleButton toggleButtonPedal;
	private ToggleButton toggleButtonLamp;
	private ToggleButton toggleButtonOnOff;
	
	private RelativeLayout relativeLayout;
	
	private Button ArrowLeft;
	private Button ArrowRight;
	private Button backButton;
	private Button buttonLighting;

//	private ListView mLeftLv/*, mRightLv*/;

	private RGBSelectAdapter rgbAdapter;
	

	private boolean isPedalOpen = false;
	private boolean isPedalClose = true;
	private boolean isPedalLampOpen = false;
	private boolean isPedalLampClose = true;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//隐藏标题  
	    requestWindowFeature(Window.FEATURE_NO_TITLE);        
	    //定义全屏参数  
	    int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;  
	    //获得窗口对象  
	    Window myWindow = this.getWindow();  
	    //设置Flag标识  
	    myWindow.setFlags(flag, flag);
	    
		mContext = this;
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_electricpedal);
		mActivity = MainActivity.getMainActivity();

		relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayoutBG);
		
		 // 踏板
		toggleButtonPedal = (ToggleButton) findViewById(R.id.toggleButtonPedal);
//		toggleButtonPedal.setEnabled(false);
		toggleButtonPedal.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mActivity.isLightOpen=isChecked;
				if (isChecked) {
					isPedalClose = true;
					isPedalOpen = false;
//					isPedalLampOpen = false;
//					isPedalLampClose = false;
					mActivity.setElectricModel(0, 0);
					toggleButtonPedal.setBackgroundResource(R.drawable.pedal_selected);					
				} else {
					isPedalClose = false;
					isPedalOpen = true;
//					isPedalLampOpen = false;
//					isPedalLampClose = false;
					mActivity.setElectricModel(0, 1);
					toggleButtonPedal.setBackgroundResource(R.drawable.pedal);
				}
				startAnimation();
			}
		});
		
		// 踏板灯
		toggleButtonLamp = (ToggleButton) findViewById(R.id.toggleButtonLamp);
//		toggleButtonLamp.setEnabled(false);
		toggleButtonLamp.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mActivity.isLightOpen=isChecked;
				if (isChecked) {
//					isPedalClose = false;
//					isPedalOpen = false;
					
					isPedalLampOpen = false;
					isPedalLampClose = true;
					mActivity.setElectricModel(1, 0);
					if (isPedalOpen) {
						relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
					}else {
						relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
					}
					toggleButtonLamp.setBackgroundResource(R.drawable.pedal_lamp_selected);				
				} else {

					isPedalLampOpen = true;
					isPedalLampClose = false;
					mActivity.setElectricModel(1, 1);
					if (isPedalOpen) {
						relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_5);	
					}else {
						relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_0);
					}
					toggleButtonLamp.setBackgroundResource(R.drawable.pedal_lamp);
				}
//				startAnimation();
			}
		});
		
		// 踏板灯常亮开关
		toggleButtonOnOff = (ToggleButton) findViewById(R.id.toggleButtonOnOff);
		toggleButtonOnOff.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					mActivity.setElectricModel(2, 0);
					toggleButtonOnOff.setBackgroundResource(R.drawable.pedal_lamp_onoff_selected);						
				} else {
					mActivity.setElectricModel(2, 1);
					toggleButtonOnOff.setBackgroundResource(R.drawable.pedal_lamp_onoff);
				}
			}
		});

		// 返回按钮
		ArrowLeft = findButtonById(R.id.viewArrowLeft);
		ArrowLeft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
				
		// 返回按钮
		backButton = findButtonById(R.id.buttonBack1);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix, bannerSort);
				finish();
			}
		});
		
		// 灯光按钮
		buttonLighting = findButtonById(R.id.buttonLighting);
		buttonLighting.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent codeIntent = new Intent(MainActivity.getMainActivity(), ModeActivity.class);
				startActivity(codeIntent);
			}
		});
		ArrowRight = findButtonById(R.id.viewArrowRight);
		ArrowRight.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent codeIntent = new Intent(MainActivity.getMainActivity(), ModeActivity.class);
				startActivity(codeIntent);
			}
		});
	}
	
	private void startAnimation() {
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation1();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
		
	}
	
	private void animation1() {
		
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_0);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
			}
			
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_5);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
		}
//		else if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_0);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_5);
//		}
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation2();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
	}
	
	private void animation2() {
		
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_1);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_1);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_1);
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_4);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_3);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_3);
		}
//		else if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_1);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_4);
//		}
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation3();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
	}
	
	private void animation3() {
		
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_2);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_2);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_2);
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_3);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_2);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_2);
		}
//		else if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_2);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_3);
//		}
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation4();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
	}

	private void animation4() {
		
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_3);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_3);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_3);
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_2);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_1);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_1);
		}
//		else if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_3);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_2);
//		}
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation5();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
	}

	private void animation5() {
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_4);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_1);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
		}
//		else if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_4);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_1);
//		}
		
		final Handler mHandler = new Handler(); // 开启 定时搜索 定时器
		final Runnable mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				animation6();
				mHandler.removeCallbacks(this);
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作
	}
	
	private void animation6() {
		
		if (isPedalOpen) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_5);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_4);
		}else if (isPedalClose) {
			
			if (isPedalLampOpen) {
				relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_0);
			}else if (isPedalLampClose) {
				relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
			}
			
//			relativeLayout.setBackgroundResource(R.drawable.electricpedal_animation_0);
		}
		
//		if (isPedalLampOpen) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_5);
//		}else if (isPedalLampClose) {
//			relativeLayout.setBackgroundResource(R.drawable.pedallamp_animation_0);
//		}
	}

}
