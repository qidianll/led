package com.ledble.activity;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.audiofx.Visualizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;

import com.common.uitl.DrawTool;
import com.common.uitl.ListUtiles;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.SeekBar.OnSeekBarChangeListener;

import java.util.ArrayList;
import java.util.Random;

import com.common.BaseActivity;
import com.common.uitl.StringUtils;
import com.common.uitl.Tool;
import com.common.view.ScrollForeverTextView;
import com.common.view.SegmentedRadioGroup;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.ledble.view.VolumCircleBar;
import com.ledble.base.LedBleApplication;
import com.ledble.bean.Mp3;
import com.ledble.bean.MyColor;
import com.kllzn.R;

@SuppressLint("NewApi")
public class MusicActivity extends BaseActivity implements Runnable {

	private SegmentedRadioGroup segmentMusic;
	private ImageView imageViewPre;
	private ToggleButton toggleButtonPlay;
	private ImageView imageViewNext;
	private ImageView imageViewEdit;
	private ScrollForeverTextView textViewAutoAjust;
	private TextView tvCurrentTime;
	private TextView tvTotalTime;
	private LinearLayout llDecibel;
	private SeekBar seekBarDecibel;
	private TextView tvDecibelValue;
	private LinearLayout llBottom;
	private SeekBar seekBarMusic;
	private ImageView imageViewRotate;
	private ImageView imageViewPlayType;
	private Button buttonMusicLib;
	private SeekBar seekBarRhythm;
	private TextView tvRhythm;
	private TextView tvrhythmValue;
	private TextView tvDecibel;
	private VolumCircleBar volumCircleBar;
//	private ImageView imageViewMusicChange;
//	private ImageView imageViewMicroChange;
	
	private Button backButton;
	
	private static final int INT_GO_SELECT_MUSIC = 100;
	private static final int INT_UPDATE_PROGRESS = 101;
	private static final int INT_EDIT_COLOR = 102;
	private static final int INT_UPDATE_RECORD = 103;
	private static int SAMPLE_RATE_IN_HZ = 8000;
	
	private ObjectAnimator mCircleAnimator;
	
	private MainActivity mActivity;
	private Animation animationRotate;
	private Animation animationScale;
	private MediaPlayer mediaPlayer;
	private boolean isMusic = true;
	private boolean isMusicStop = false;
//	private boolean isClickMusicMode = false; //是否 选择了音乐模式
//	private boolean isClickMicroMode = false; //是否 选择了麦克风模式
	private boolean isClickMicro = false; //是否 麦克风
	private boolean canClick = false;
	private int musicMode = 1;
	private int microMode = 0;

//	private boolean isMusicSpeed = true;
//	private boolean isMicroSpeed = true;
	
	private boolean isSending = true;
	private boolean isLoopAll = false;
	private boolean isLoopOne = true;
	private boolean isRandom = false;
	private Mp3 currentMp3;
	private Visualizer mVisualizer;
	private VisualizerView mVisualizerView;
	private ArrayList<MyColor> colors;// 随机选择得颜色
	private volatile int chnnelValue;// 声道值，发送传化为亮度
	private volatile int volumValue;// 声音大小，发送传化为亮度

	private int peridTime = 10;


	private Random random = new Random();;
	private AudioRecord ar;
	private int bs;
	
	private Handler mHandler;
	private Runnable mRunnable;
	
	private Handler musicStartHandler;
	private Runnable musicStartRunnable;
	private Handler musicStopHandler;
	private Runnable musicStopRunnable;
	
	private Handler microStartHandler;
	private Runnable microStartRunnable;
	private Handler microStopHandler;
	private Runnable microStopRunnable;
	
	private Handler startHandler;
	private Runnable startRunnable;
	private int MICRO_CODE = 111;
	
	private Handler sendMusicDataHandler;
	private Runnable sendMusicDataRunnable;
	
	private Handler updateVolumRateHandler;
	private Runnable updateVolumRateRunnable;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	
		bs = AudioRecord.getMinBufferSize(SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT);
		ar = new AudioRecord(MediaRecorder.AudioSource.MIC, SAMPLE_RATE_IN_HZ, AudioFormat.CHANNEL_IN_DEFAULT,
				AudioFormat.ENCODING_PCM_16BIT, bs);		
		
		
		refresh(); // 刷新状态
		
		initView();
	}
	
	/**
	 * 
	 */
	protected void refresh() {
		
		mHandler = new Handler(); // 开启 定时搜索 定时器
		mRunnable = new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				// 在此处添加执行的代码
				mHandler.postDelayed(this, 800);// 延时时长
				canClick = true;
			}
		};
		mHandler.postDelayed(mRunnable, 100);// 打开定时器，执行操作

	}

	@SuppressLint("NewApi")
	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_music);
		new Thread(this).start();
	
		
		mActivity = MainActivity.getMainActivity();
		mVisualizerView = new VisualizerView(mActivity);

//		if (mActivity.checkSelfPermission(Manifest.permission.RECORD_AUDIO)
//                != PackageManager.PERMISSION_GRANTED) {
//        	mActivity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, MICRO_CODE);
//        }
		
		textViewAutoAjust = (ScrollForeverTextView) findViewById(R.id.textViewAutoAjust);
		tvCurrentTime = (TextView) findViewById(R.id.tvCurrentTime);
		tvTotalTime = (TextView) findViewById(R.id.tvTotalTime);
		llBottom = (LinearLayout) findViewById(R.id.llBottom);
		llDecibel = (LinearLayout) findViewById(R.id.llDecibel);
		tvrhythmValue = (TextView) findViewById(R.id.tvrhythmValue);
		tvRhythm = (TextView) findViewById(R.id.tvRhythm);
		tvDecibelValue = (TextView) findViewById(R.id.tvDecibelValue);
		tvDecibel = (TextView) findViewById(R.id.tvDecibel);
		
		this.imageViewRotate = (ImageView) findViewById(R.id.imageViewRotate);	
		mCircleAnimator = ObjectAnimator.ofFloat(imageViewRotate, "rotation", 0.0f, 360.0f);
		mCircleAnimator.setDuration(8000);
		mCircleAnimator.setInterpolator(new LinearInterpolator());
		mCircleAnimator.setRepeatCount(-1);
		mCircleAnimator.setRepeatMode(ObjectAnimator.RESTART);
		
		
		// 返回按钮
		backButton = findButtonById(R.id.buttonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix,
//						bannerSort);
				pauseMusic();
				pauseVolum();
				
//				mHandler.removeCallbacks(mRunnable);
				
				finish();
			}
		});
		
		
		this.imageViewPre = (ImageView) findViewById(R.id.imageViewPre);
		this.imageViewPre.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (canClick) {
//					playAnimationPress(v);
					playPre();
					canClick = false;
				}
			}
		});
		
		this.toggleButtonPlay = (ToggleButton) findViewById(R.id.toggleButtonPlay);
		this.toggleButtonPlay.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (canClick) {
//					playAnimationPress(v);
					playPause();
					canClick = false;
				}
			}
		});
		
		this.imageViewNext = (ImageView) findViewById(R.id.imageViewNext);
		this.imageViewNext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (canClick) {
//					playAnimationPress(v);
					palayNext();
					canClick = false;
				}
			}
		});
		
		
		this.animationScale = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.layout_scale);
		this.animationRotate = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_frover);
		this.animationRotate.setInterpolator(new LinearInterpolator());
		this.animationRotate.setRepeatCount(-1);
		this.animationRotate.setAnimationListener(new com.common.adapter.AnimationListenerAdapter() {
		});
		this.imageViewRotate.setOnClickListener(new OnClickListener() { // 中间旋转大图

			@Override
			public void onClick(View v) {
				switch (musicMode) {
				case 0:
					imageViewRotate.setBackgroundResource(R.drawable.music_gradualchange);
					musicMode = 1;
					break;
				case 1:
					imageViewRotate.setBackgroundResource(R.drawable.music_trailing);  
					musicMode = 2;
					break;
				case 2:
					imageViewRotate.setBackgroundResource(R.drawable.music_jump);
					musicMode = 3;
					break;
				case 3:
					imageViewRotate.setBackgroundResource(R.drawable.music_stroboflash);
					musicMode = 4;
					break;
				case 4:
					imageViewRotate.setBackgroundResource(R.drawable.music_nooutput);
					musicMode = 0;
					break;

				default:
					break;
				}
				
				try {
					sendMusicMode();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		this.imageViewPlayType = (ImageView) findViewById(R.id.imageViewPlayType);
		this.imageViewPlayType.setOnClickListener(new OnClickListener() { //播放模式（顺序，单曲，随机）

			@Override
			public void onClick(View v) {
				if (isLoopAll) {
					imageViewPlayType.setImageResource(R.drawable.playtype_loopall);
					Toast.makeText(getApplicationContext(), R.string.SequentialPlay, Toast.LENGTH_SHORT).show();
					isLoopAll = false;
					isLoopOne = true;
				} else if (isLoopOne) {
					imageViewPlayType.setImageResource(R.drawable.playtype_loopone);
					Toast.makeText(getApplicationContext(), R.string.SinglePlay, Toast.LENGTH_SHORT).show();
					isLoopOne = false;
					isRandom = true;
				} else if (isRandom) {
					imageViewPlayType.setImageResource(R.drawable.playtype_random);
					Toast.makeText(getApplicationContext(), R.string.RandomPlay, Toast.LENGTH_SHORT).show();
					isRandom = false;
					isLoopAll = true;
				}
//				isSending = !isSending;
			}
		});
		
		this.buttonMusicLib = (Button) findViewById(R.id.buttonMusicLib);
		this.buttonMusicLib.setOnClickListener(new OnClickListener() { //乐库
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, MusicLibActivity.class);
				startActivityForResult(intent, INT_GO_SELECT_MUSIC);
			}
		});

		this.imageViewEdit = (ImageView) findViewById(R.id.imageViewEdit);
		imageViewEdit.findViewById(R.id.imageViewEdit).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mActivity, EditColorActivity.class);
				startActivityForResult(intent, INT_EDIT_COLOR);
			}
		});
		
//		this.imageViewMusicChange = (ImageView) findViewById(R.id.imageViewMusicChange);
//		imageViewMusicChange.findViewById(R.id.imageViewMusicChange).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (isMusicSpeed) {
//					tvRhythm.setText(R.string.brightness);
//					isMusicSpeed = false;
//				}else {
//					tvRhythm.setText(R.string.speed);
//					isMusicSpeed = true;
//				}
//				
//			}
//		});
		
		this.seekBarRhythm = (SeekBar) findViewById(R.id.seekBarRhythm);
		this.seekBarRhythm.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
								
				tvrhythmValue.setText(Integer.toString(progress));
//				if (isMusicSpeed) {
//					mActivity.setSpeed(progress);
//				}else {
					int p = progress;
					if (progress<=0) {
						p = -3;
					}else if (progress >= 100) {
						p = 100;
					}
					
					if (p > 95) {
				        return;
					}
					p = (p + 3)/3;
					
					for (int i = 0; i < 5; i++) {
						mActivity.setBrightNess(p);
					}				
//				}
				
			}
		});
		
		
//		this.imageViewMicroChange= (ImageView) findViewById(R.id.imageViewMicroChange);
//		imageViewMicroChange.findViewById(R.id.imageViewMicroChange).setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				if (isMicroSpeed) {
//					tvDecibel.setText(R.string.brightness);
//					isMicroSpeed = false;
//				}else {
//					tvDecibel.setText(R.string.speed);
//					isMicroSpeed = true;
//				}
//				
//			}
//		});
		
		this.seekBarDecibel = (SeekBar) findViewById(R.id.seekBarDecibel);
		this.seekBarDecibel.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				
				tvDecibelValue.setText(Integer.toString(progress));
//				if (isMicroSpeed) {
//					mActivity.setSpeed(progress);					
//				}else {
					int p = progress;
					if (progress<=0) {
						p = -3;
					}else if (progress >= 100) {
						p = 100;
					}
					
					if (p > 95) {
				        return;
					}
					p = (p + 3)/3;
					
					for (int i = 0; i < 5; i++) {
						mActivity.setBrightNess(p);
					}
					
//				}
				
//				mActivity.setSpeed(progress);
//				tvDecibelValue.setText(String.valueOf(progress));
//				SharePersistent.savePerference(getApplicationContext(), "decibel", progress);
				
			}
		});
//		this.seekBarDecibel.setProgress(SharePersistent.getInt(getApplicationContext(), "decibel"));
		
		
		volumCircleBar = (VolumCircleBar) findViewById(R.id.volumCircleBar);
//		volumCircleBar = (VolumCircleBar) findViewById(R.id.volumCircleBar);
//		this.volumCircleBar.setVisibility(View.GONE);		
		volumCircleBar.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				switch (microMode) {
				case 0:
					volumCircleBar.toggleRecord(0);
					volumCircleBar.setBackgroundResource(R.drawable.micro_light_trailing);
					
					microMode = 1;
					break;
				case 1:
					volumCircleBar.setBackgroundResource(R.drawable.micro_light_flowwater);
					microMode = 2;
					break;
				case 2:
					volumCircleBar.setBackgroundResource(R.drawable.micro_light_chaselight);
					microMode = 3;
					break;
				case 3:
					volumCircleBar.setBackgroundResource(R.drawable.micro_light_gradient);
					microMode = 0;
					break;

				default:
					break;
				}
				
				try {
					sendMicroMode();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		segmentMusic = (SegmentedRadioGroup) findViewById(R.id.segmentMusic);
		segmentMusic.check(R.id.rbMusicOne);
		segmentMusic.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.rbMusicOne == checkedId) {
					tvRhythm.setVisibility(View.VISIBLE);
					seekBarRhythm.setVisibility(View.VISIBLE);
					tvrhythmValue.setVisibility(View.VISIBLE);
					imageViewRotate.setVisibility(View.VISIBLE);
					volumCircleBar.setVisibility(View.GONE);
					llDecibel.setVisibility(View.GONE);
					llBottom.setVisibility(View.VISIBLE);
//					imageViewMusicChange.setVisibility(View.VISIBLE);
					
//					try {
//						sendMusicMode();
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
					
					isClickMicro = false;
					
//					pauseVolum();
				} else {
					isMusic = false;					
	
//					imageViewMusicChange.setVisibility(View.INVISIBLE);
					tvrhythmValue.setVisibility(View.INVISIBLE);
					tvRhythm.setVisibility(View.INVISIBLE);
					seekBarRhythm.setVisibility(View.INVISIBLE);
//					segment.setVisibility(View.INVISIBLE);
					imageViewRotate.setVisibility(View.GONE);
					volumCircleBar.setVisibility(View.VISIBLE);
					llDecibel.setVisibility(View.VISIBLE);
					llBottom.setVisibility(View.GONE);
					
					if (Build.VERSION.SDK_INT >= 23) {			
						if (mActivity.checkSelfPermission(Manifest.permission.RECORD_AUDIO)
			                    != PackageManager.PERMISSION_GRANTED) {
			                // 申请授权							
							mActivity.requestPermissions(new String[]{Manifest.permission.RECORD_AUDIO}, MICRO_CODE);
//							startMicroRecording();
			            } else {			            				            	
							
			            	volumCircleBar.toggleRecord(0);			            											
			            	ar.startRecording();
										            			            	
			            	updateVolumRate();
			            	
			            	try {
								sendMicroMode();
								pauseMusic();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
			            }
					}else {											
						volumCircleBar.toggleRecord(0);
						ar.startRecording();
						updateVolumRate();
			
						try {
							sendMicroMode();
							pauseMusic();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}

					// 点击了 麦克风 标签，再初始化
//					volumCircleBar.toggleRecord(0);
//					ar.startRecording();					
					volumCircleBar.setBackgroundResource(R.drawable.micro_light_gradient);
					Toast.makeText(mActivity, R.string.microphone_start, Toast.LENGTH_SHORT).show();
					isClickMicro = true;					
					
				}
			}
		});
		
		this.seekBarMusic = (SeekBar) findViewById(R.id.seekBarMusic);
		seekBarMusic.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

			}

			public void onStartTrackingTouch(SeekBar seekBar) {

			}

			public void onStopTrackingTouch(SeekBar seekBar) {
				if (null == currentMp3) {
					Tool.ToastShow(MainActivity.getMainActivity(), R.string.chose_list);
					seekBar.setProgress(0);
					return;
				}
				if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
					System.out.println("progress-->" + seekBar.getProgress());
					int duration = currentMp3.getDuration();
					int current = (int) seekBar.getProgress() * duration / 100;
					System.out.println("current-->" + current);
					mediaPlayer.seekTo(current);
				}
			}
		});
		
	}
	
	public void startMicroRecording() {
			
		if (null == startHandler) {
			startHandler = new Handler(); // 开启 定时搜索 定时器
		}
		if (null == startRunnable) {
			startRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					startHandler.postDelayed(startRunnable, 2000);// 8秒 是延时时长		

					if (mActivity.checkSelfPermission(Manifest.permission.RECORD_AUDIO)
		                    == PackageManager.PERMISSION_GRANTED) {
						
						volumCircleBar.toggleRecord(0);
		            	ar.startRecording();
		            	updateVolumRate();
		            	try {
							sendMicroMode();
							pauseMusic();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					
		            	Tool.ToastShow(MusicActivity.this, "麦克风");
						startHandler.removeCallbacks(startRunnable);
					}
				}
			};
		}		
		startHandler.postDelayed(startRunnable,2000);// 打开定时器，执行操作
	}

//	@Override
//	public void onResume() {
//		super.onResume();
//		// 设置音乐
////		sendMusicData();
//	}
	
	private void sendMusicData() {	
		
		if (null == sendMusicDataHandler) {
			sendMusicDataHandler = new Handler(); // 开启 定时搜索 定时器
		} 
		if (null == sendMusicDataRunnable) {
			sendMusicDataRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
//					if (isMusicSpeed) {
//						mHandler.postDelayed(this, 50);// 1毫秒秒 是延时时长
//					}else {
					sendMusicDataHandler.postDelayed(this, 50);// 1毫秒秒 是延时时长
//					}
					
//					while (true) {
						try {
							
							if (chnnelValue > 0 && (/*musicMode!= 3 &&*/ null != mediaPlayer && mediaPlayer.isPlaying())) {
								sendMusicValue(getRandColor(), chnnelValue);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			};
		}
		
		sendMusicDataHandler.postDelayed(sendMusicDataRunnable, 10);// 打开定时器，执行操作
	}
	
	private void updateVolumRate() {
		
		if (null  == updateVolumRateHandler) {
			updateVolumRateHandler = new Handler(); // 开启 定时搜索 定时器
		}
		if (null == updateVolumRateRunnable) {
			updateVolumRateRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					updateVolumRateHandler.postDelayed(this, peridTime);// 1毫秒秒 是延时时长
//					while (true) {
						try {
							
							if (volumValue > 0 &&  null != volumCircleBar && isClickMicro == true) {
								
								float precent = (float) volumValue / 100;														
								volumCircleBar.updateVolumRate(precent);
								sendVolumValue(getRandColor(), volumValue);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
				}
			};
		}	
		updateVolumRateHandler.postDelayed(updateVolumRateRunnable, 1);// 打开定时器，执行操作
	}

	/*
	 * 随机发送颜色
	 */
	private MyColor getRandColor() {
		if (null == colors || colors.size() == 0) {
			return null;
		}

		int r = random.nextInt(colors.size());
		MyColor color = colors.get(r);
		return color;
	}

//	private void playAnimationPress(View v) {
//		v.startAnimation(animationScale);
//	}

	private Handler mhanHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			int what = msg.what;
			switch (what) {
			case INT_UPDATE_PROGRESS:
				updatePlayProgress(currentMp3);
				if (mediaPlayer != null) {
					int current = (int) (mediaPlayer.getCurrentPosition());
					int musicTime = (int) current / 1000;
					// String time = "";
					String time = new String("");
					if (musicTime < 60) {
						if (musicTime < 10) {
							time = "0:0" + musicTime;
						} else {
							time = "0:" + musicTime;
						}
					} else {
						// String minute = "";
						String minute = new String("");
						if (musicTime % 60 < 10) {
							minute = "0" + musicTime % 60;
						} else {
							minute = "" + musicTime % 60;
						}
						time = musicTime / 60 + ":" + minute;
					}
					tvCurrentTime.setText(time);
				}
				break;
			case INT_UPDATE_RECORD:				
//				// int pre = (Integer) msg.obj;
//				double pre = (Double) msg.obj;
//				int dB = (int) (10 * Math.log10(pre));
////				int value = dB;
//				int value = dB - 35;
//				
////				if (value <= 0) {
////					value = 1;
////				}
				
				
				// int pre = (Integer) msg.obj;
				double pre = (Double) msg.obj;
				int dB = (int) (10 * Math.log10(pre));
//				if (dB <= 35) {
//					dB = 36;
//				}
				int value = dB;	
//				int value = dB - 25;

//				float precent = (float) value / 100;
//				volumCircleBar.updateVolumRate(precent);
				
				if (value >= 0) {
					value = dB - 40;
				}
				
				volumValue = value;				

				break;
				
			default:
				break;
			}
		}
	};

	private void updatePlayProgress(Mp3 currentMp3) {
		if (null != mediaPlayer && mediaPlayer.isPlaying()) {
			int duration = currentMp3.getDuration();
			int current = (int) (mediaPlayer.getCurrentPosition());
			if (duration != 0 && (current < duration)) {
				int percent = (int) (current * 100 / duration);
				seekBarMusic.setProgress(percent);
			}
		}
	}

	public void palayNext() {
		if (null == currentMp3) {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				startPlay(0);
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		} else {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				int currentIndex = findCurrentIndex(currentMp3);
				if (currentIndex != -1) {
					if (currentIndex < (LedBleApplication.getApp().getMp3s().size() - 1)) {
						startPlay(++currentIndex);
					} else if (currentIndex == (LedBleApplication.getApp().getMp3s().size() - 1)) {
						startPlay(0);
					}
				} else {
					startPlay(0);
				}
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		}
	}

	public void playPre() {
		if (null == currentMp3) {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				startPlay(0);
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		} else {
			if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
				int currentIndex = findCurrentIndex(currentMp3);
				if (currentIndex != -1) {
					if (currentIndex > 0) {
						startPlay(--currentIndex);
					} else if (currentIndex == 0) {
						startPlay(LedBleApplication.getApp().getMp3s().size() - 1);
					}
				} else {
					startPlay(0);
				}
			} else {
				Tool.ToastShow(mActivity, R.string.chose_list);
			}
		}
	}

	/**
	 * 停止播放音乐
	 */
	public void pauseMusic() {
//		if (musicMode!= 3) {	
		
		if (mediaPlayer != null) {
			if (mediaPlayer.isPlaying()) {
				// 暂停
				mediaPlayer.pause();
				toggleButtonPlay.setBackgroundResource(R.drawable.bg_play);
				mCircleAnimator.pause();
		
				if (null != musicStartHandler && null != musicStartRunnable) {
					musicStartHandler.removeCallbacks(musicStartRunnable);
				} 
				if (null != musicStopHandler && null != musicStopRunnable) {
					musicStopHandler.removeCallbacks(musicStopRunnable);
				}
				if (null != mHandler && null != mRunnable) {
					mHandler.removeCallbacks(mRunnable);
				}
				if (null != sendMusicDataHandler && null != sendMusicDataRunnable) {
					sendMusicDataHandler.removeCallbacks(sendMusicDataRunnable);
				}
			}
		}
	}
	
	/**
	 * 停止麦克风
	 */
	public void pauseVolum(boolean status) {
		if (status) {
			if (volumCircleBar != null && volumCircleBar.recordMode() != 3) {
				volumCircleBar.toggleRecord(3);
			}
		}else {
			if (volumCircleBar != null) {
				volumCircleBar.toggleRecord(0);
				
//				if (isClickMicro) {
//					try {
//						sendMicroMode();
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//				}
				
			}
		}
	}
	
	/**
	 * 开始播放音乐
	 */
	public void startMusic() {
		if (musicMode!= 3 && isMusic == true ) {
			
			if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
				
				if (isMusicStop) {
					return;
				}else {
					mediaPlayer.start();
					toggleButtonPlay.setBackgroundResource(R.drawable.bg_play_pause);
					startRotate(); // 开始旋转动画
				}
			}
		}
	}

	/**
	 * 停止麦克风
	 */
	public void pauseVolum() {
		if (volumCircleBar != null && volumCircleBar.recordMode() != 3) {			
			
//			volumCircleBar.toggleRecord(4);
			if (ar != null) {
				ar.stop();
				ar = null;
			}
			
//			volumCircleBar = null;
			
			
			if (null != microStartHandler && null != microStartRunnable) {
				microStartHandler.removeCallbacks(microStartRunnable);
				
			}
			if (null != microStopHandler && null != microStopRunnable) {
				microStopHandler.removeCallbacks(microStopRunnable);
			}
			if (null != updateVolumRateHandler && null != updateVolumRateRunnable) {
				updateVolumRateHandler.removeCallbacks(updateVolumRateRunnable);
			}
			
//			volumCircleBar.toggleRecord(4);
//			ar.stop();
////			volumCircleBar = null;
//			ar = null;
		}
	}
	
	/**
	 * 开始麦克风
	 */
	public void startVolum() {
		if (microMode != 0 && isMusic == false) {
			if (volumCircleBar != null) {
				volumCircleBar.toggleRecord(0);
			}		
		}
	}

	public void playPause() {

		if (ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
			Tool.ToastShow(mActivity, R.string.chose_list);
			return;
		}

		if (null == mediaPlayer) {
			startPlay(0);
		} else {
			if (mediaPlayer.isPlaying()) {
				// 暂停
				mediaPlayer.pause();
				toggleButtonPlay.setBackgroundResource(R.drawable.bg_play);
				stopRotate(); // 暂停旋转动画
				isMusicStop = true;
			} else {
				// 播放
				if (null != currentMp3) {
					mediaPlayer.start(); 
					resumeRotate();  // 继续旋转动画
					isMusicStop = false;
					try {
						sendMusicMode();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())) {
						startPlay(0);
					} else {
						Tool.ToastShow(mActivity, R.string.chose_list);
						return;
					}
				}
				toggleButtonPlay.setBackgroundResource(R.drawable.bg_play_pause);
			}

		}

	}

	boolean isSettingBoolean = false;

	public void playMp3(final Mp3 mp3) {
		try {
			currentMp3 = mp3;
			setTitles(mp3);
			setAbulmImage(imageViewRotate, mp3);
			if (null == mediaPlayer) {							
				mediaPlayer = new MediaPlayer();				
			}
			if (mediaPlayer.isPlaying()) {
				mediaPlayer.stop();
			}
			mediaPlayer.reset();
			mediaPlayer.setDataSource(mp3.getUrl());
			if (!isSettingBoolean) {
				isSettingBoolean = true;
				mVisualizer = new Visualizer(mediaPlayer.getAudioSessionId());
				mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[0]);
				// mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);
			}
			mediaPlayer.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					if (mVisualizer != null) {
						mVisualizer.setEnabled(false);
					}
					try {
						ArrayList<Mp3> list = LedBleApplication.getApp().getMp3s();
						int index = findCurrentIndex(mp3);
						
						if (!isLoopOne && isRandom) { //单曲循环
							mediaPlayer.start();  
							mediaPlayer.setLooping(true);
//							playMp3(list.get(index));
							return;
						}else if (!isRandom && isLoopAll) { //随机播放
							int playIndex = getRandIntPlayIndex(list.size());
							playMp3(list.get(playIndex));
							return;
						}
						
						if (-1 != index && !ListUtiles.isEmpty(list)) {
							if (index == (list.size() - 1)) {
								playMp3(list.get(0));
								return;
							}
							if (index <= (list.size() - 2)) {
								playMp3(list.get(index + 1));
								return;
							}
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});

			mediaPlayer.prepare();
			mediaPlayer.start();
			startRotate();	//继续旋转动画
			if (mVisualizer != null) {
				mVisualizer.setEnabled(false);
			}
			
			sendMusicMode();		//发送音乐模式

			mVisualizer.setDataCaptureListener(new Visualizer.OnDataCaptureListener() {
				public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes, int samplingRate) {
//					updateVisualizer(bytes);
					mVisualizerView.updateVisualizer(bytes);
				}

				public void onFftDataCapture(Visualizer visualizer, byte[] fft, int samplingRate) {
//					updateVisualizer(fft);
					mVisualizerView.updateVisualizer(fft);
				}
			}, Visualizer.getMaxCaptureRate() / 2, false, true);
			if (mVisualizer != null) {
				mVisualizer.setEnabled(true);
			}
			new Thread().start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 ** 开启动画    
	 */
	public void startRotate() {
		
		mCircleAnimator.start();
	}
	
	/**
	 ** 继续动画    
	 */
	public void resumeRotate() {

		mCircleAnimator.resume();
	}

	/**
	 ** 暂停动画    
	 */
	public void stopRotate() {

		mCircleAnimator.pause();
	}

	/**
	 * 设置title现实内容
	 * 
	 * @param mp3
	 */
	private void setTitles(Mp3 mp3) {
		String artist = mp3.getArtist();
		if (StringUtils.isEmpty(artist)) {
			textViewAutoAjust.setText(mp3.getTitle());
		} else {
			String title = getResources().getString(R.string.ablum_title, "<<" + mp3.getTitle() + ">>",
					mp3.getArtist(), mp3.getAlbum());
			textViewAutoAjust.setText(title);
		}
	}

	/**
	 * 获取并且专辑专辑图片
	 * 
	 * @param imageViewImg
	 * @param mp3
	 */
	@SuppressLint("NewApi")
	private void setAbulmImage(ImageView imageViewImg, Mp3 mp3) {
		try {
			MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
			mediaMetadataRetriever.setDataSource(mp3.getUrl());
			byte[] data = mediaMetadataRetriever.getEmbeddedPicture();
			if (data != null) {
				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
				bitmap = DrawTool.toRoundBitmap(bitmap);
				// imageViewRotate.setImageBitmap(bitmap); // associated cover
				// art in//
			} else {
//				imageViewRotate.setImageResource(R.drawable.music_gradualchange);
			}
		} catch (Exception e) {
			e.printStackTrace();
//			imageViewRotate.setImageResource(R.drawable.music_gradualchange);
		}
	}

	public void sendMusicMode() throws InterruptedException {

		if (mediaPlayer != null && mediaPlayer.isPlaying()) {
			if (4 == musicMode) { // 追光
				for (int i = 0; i < 10; i++) {	
					MainActivity.getMainActivity().setRegMode(78);
				}
			}else {
				startMusicMode();
			}
		}			
	}

	public void sendMicroMode() throws InterruptedException {
		
//		if (isClickMicro) {
			if (3 == microMode) { // 追光
				for (int i = 0; i < 10; i++) {	
					MainActivity.getMainActivity().setRegMode(78);
				}
			}
			else {
				startMicroMode();
			}
//		}
		
	}
	
	private void startMusicMode() {
		
		if (null == musicStartHandler) {
			musicStartHandler = new Handler(); // 开启 定时搜索 定时器
		}
		if (null == musicStartRunnable) {
			musicStartRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					musicStartHandler.postDelayed(this, 4000);// 8秒 是延时时长
					
					// ************************ 音乐 ***************************//				
					if (mediaPlayer != null && mediaPlayer.isPlaying()) {
						if (0 == musicMode) { // 无输出
//							mActivity.setRgb(255, 255, 255);
//							mActivity.setBrightNess(100);
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(39);
							}
						}else if (1 == musicMode) { // 渐变
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(71);
							}
						}else if (2 == musicMode) { // 拖尾
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(23);
							}
						}else if (3 == musicMode) { // 流水
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(161);
							}
						}
					}			
					refreshDevices();
				}
			};
		}
		musicStartHandler.postDelayed(musicStartRunnable, 1);// 打开定时器，执行操作
	}
	
	protected void refreshDevices() {

		if (null == musicStopHandler) {
			musicStopHandler = new Handler(); // 开启 定时搜索 定时器		
		}
		if (null == musicStopRunnable) {
			musicStopRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					
					// ************************ 音乐 ***************************//	
					if (mediaPlayer != null && mediaPlayer.isPlaying()) {
						if (0 == musicMode) { // 无输出
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(40);
							}
						}else if (1 == musicMode) {
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(72);
							}
						}else if (2 == musicMode) {
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(24);
							}
						}else if (3 == musicMode) { // 流水
							for (int i = 0; i < 10; i++) {	
								MainActivity.getMainActivity().setRegMode(162);
							}
						}
					}					
				}
			};
		}
		musicStopHandler.postDelayed(musicStopRunnable, 2000);// 打开定时器，执行操作
//		stopHandler.removeCallbacks(runnable);
			
	}
	
	private void startMicroMode() {

		if (null == microStartHandler) {
			microStartHandler = new Handler(); // 开启 定时搜索 定时器
		}
		if (null == microStartRunnable) {
			microStartRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码
					microStartHandler.postDelayed(this, 4000);// 8秒 是延时时长

					// ************************ 麦克风
					// ***************************//
					
					if (isClickMicro) {
						if (volumCircleBar != null && volumCircleBar.recordMode() != 4) {
							if (0 == microMode) { // 渐变
								for (int i = 0; i < 10; i++) {
									MainActivity.getMainActivity().setRegMode(71);
								}
							} else if (1 == microMode) { // 拖尾
								for (int i = 0; i < 10; i++) {
									MainActivity.getMainActivity().setRegMode(23);
								}
							} else if (2 == microMode) { // 流水
							 	for (int i = 0; i < 10; i++) {	
									MainActivity.getMainActivity().setRegMode(161);
								}
							}
						}						
					}

					refreshMicroMode();
				}
			};
		}
		microStartHandler.postDelayed(microStartRunnable, 1);// 打开定时器，执行操作
	}
	
	protected void refreshMicroMode() {

		if (null == microStopHandler) {
			microStopHandler = new Handler(); // 开启 定时搜索 定时器	
		}
		if (null == microStopRunnable) {
			microStopRunnable = new Runnable() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					// 在此处添加执行的代码					
					
					// ************************ 麦克风 ***************************//

					if (isClickMicro) {
						if (volumCircleBar != null && volumCircleBar.recordMode() != 4) {
							if (0 == microMode) { // 渐变
								for (int i = 0; i < 10; i++) {	
									MainActivity.getMainActivity().setRegMode(72);
								}
							}else if (1 == microMode) { // 拖尾
								for (int i = 0; i < 10; i++) {	
									MainActivity.getMainActivity().setRegMode(24);
								}
							} else if (2 == microMode) { // 流水
							 	for (int i = 0; i < 10; i++) {	
									MainActivity.getMainActivity().setRegMode(162);
								}
							}
						}					
					}
				}
			};
		}
		microStopHandler.postDelayed(microStopRunnable, 2000);// 打开定时器，执行操作
			
	}
	
	private ArrayList<MyColor> getSelectColor() {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		
		if (null == colors || colors.size() == 0) {
			for (int i = 0; i < 7; i++) {
				colorList.add(new MyColor(getRandIntColor(), getRandIntColor(), getRandIntColor()));
			}
		} else {
			for (int i = 0; i < colors.size(); i++) {
				
				colorList.add(colors.get(i));
			}
//			Toast.makeText(mActivity, ""+colorList.size(), Toast.LENGTH_SHORT).show();
		}
	
		return colorList;
	}

	

	// *************************************音乐*************************************************//
	private void sendMusicValue(MyColor color, int chnnel) {
		if (musicMode == 0) {
			return;
		}

		if (isSending && null != mediaPlayer && mediaPlayer.isPlaying()) {
//			if (isMusicSpeed) {				
//				mActivity.setBrightNess(chnnel);
//			}else {	
				for (int i = 0; i < 5; i++) {
					mActivity.setSpeed(chnnel);
				}											
//			}
			
		}

	}
	
	// *************************************麦克风*************************************************//
	private void sendVolumValue(MyColor color, int volumValue) {

		if (volumValue <= 1) {
			volumValue = 1;
		}

		if (volumValue >= 60) {
			volumValue = 100;
		} else if (volumValue >= 50 && volumValue < 60) {
			volumValue = 95;
		} else if (volumValue >= 40 && volumValue < 50) {
			volumValue = 90;
		} else if (volumValue >= 15 && volumValue < 40) {
			volumValue = 85;
		} else if (volumValue >= 9 && volumValue < 15) {
			volumValue = 60;
		} else if (volumValue >= 4 && volumValue < 9) {
			volumValue = 30;
		} else if (volumValue >= 1 && volumValue < 4) {
			volumValue = 1;
		}
            	

		if (/* volumCircleBar != null && volumCircleBar.recordMode() != 3 && */ isClickMicro == true) {
//			if (isMicroSpeed) {
//				mActivity.setBrightNess(volumValue);
//			}else {				
			for (int i = 0; i < 5; i++) {
				mActivity.setSpeed(volumValue);
			}
//			}
//			tvDecibelValue.setText(Integer.toString(volumValue));
//			Toast.makeText(mActivity, "volumValue = "+volumValue, Toast.LENGTH_SHORT).show();
		}

	}
	

	Random rand = new Random();

	private int getRandIntColor() {
		int color = rand.nextInt(255);
		return color;
	}
	
	private int getRandIntPlayIndex(int size) {
		int index = rand.nextInt(size);
		return index;
	}


	private int findCurrentIndex(Mp3 mp3) {
		ArrayList<Mp3> list = LedBleApplication.getApp().getMp3s();
		if (ListUtiles.isEmpty(list)) {
			return -1;
		}
		for (int i = 0, lsize = list.size(); i < lsize; i++) {
			if (mp3.equals(list.get(i))) {
				return i;
			}
		}
		return -1;
	}

	public int avgData(byte[] fft) {
		int sum = 0;
		for (int i = 1; i < fft.length; i++) {
			sum += fft[i];
		}
		int ave = sum / (fft.length - 1);
		ave = (int) (Math.abs((float) ave / 127) * 100);
		return ave;
		// LogUtil.i(App.tag, "consult:" + sum / fft.length);
	}

	private void startPlay(int index) {
		if (!ListUtiles.isEmpty(LedBleApplication.getApp().getMp3s())
				&& LedBleApplication.getApp().getMp3s().size() > index) {
			try {
				sendMusicData();
				playMp3(LedBleApplication.getApp().getMp3s().get(index));
				toggleButtonPlay.setBackgroundResource(R.drawable.bg_play_pause);
				int musicTime = mediaPlayer.getDuration() / 1000;
				String minute = "";
				if (musicTime % 60 < 10) {
					minute = "0" + musicTime % 60;
				} else {
					minute = "" + musicTime % 60;
				}
				String time = musicTime / 60 + ":" + minute;
				tvCurrentTime.setText("0:00");
				tvTotalTime.setText(time);
				// this.currentPlayIndex = index;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == INT_GO_SELECT_MUSIC && resultCode == Activity.RESULT_OK) {
			startPlay(0);
		}
		if (requestCode == INT_EDIT_COLOR && resultCode == Activity.RESULT_OK && null != data) {
			colors = (ArrayList<MyColor>) data.getSerializableExtra("color");
			try {

				if (!isClickMicro) {
					sendMusicMode();
				} else {
					sendMicroMode();
				}

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		pauseMusic();
		pauseVolum();
		if (null != musicStartHandler) {
			musicStartHandler.removeCallbacks(musicStartRunnable);
		} 
		if (null != musicStopHandler) {
			musicStopHandler.removeCallbacks(musicStopRunnable);
		}
		if (null != microStartHandler) {
			microStartHandler.removeCallbacks(microStartRunnable);
		}
		if (null != microStopHandler) {
			microStopHandler.removeCallbacks(microStopRunnable);
		}
		
	}

	@Override
	public void run() {
		try {
			// ar.startRecording();
			// 用于读取的 buffer
			short[] buffer = new short[bs];
			while (true) {
				if (musicMode!= 3 && null != mediaPlayer && mediaPlayer.isPlaying()) {
					mhanHandler.sendEmptyMessage(INT_UPDATE_PROGRESS);
				}

				if (volumCircleBar != null && volumCircleBar.recordMode() != 3) {
					int r = ar.read(buffer, 0, bs);
					long v = 0;
					// 将 buffer 内容取出，进行平方和运算
					for (int i = 0; i < buffer.length; i++) {
						// 这里没有做运算的优化，为了更加清晰的展示代码
						v += buffer[i] * buffer[i];
					}

					// int value = (int)(v / r);
					double value = v / (double) r;
					// 平方和除以数据总长度，得到音量大小。可以获取白噪声值，然后对实际采样进行标准化。
					// 如果想利用这个数值进行操作，建议用 sendMessage 将其抛出，在 Handler 里进行处理。
					// double dB = 10*Math.log10(v / (long) r);
					Message msg = new Message();
					msg.what = INT_UPDATE_RECORD;
					msg.obj = value;
					mhanHandler.sendMessage(msg);
				}
				Tool.delay(peridTime);
				if (mActivity.isFinishing()) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static int audioSource = MediaRecorder.AudioSource.MIC;
    public static int sampleRateInHz = 44100;
    public static int channelConfig = AudioFormat.CHANNEL_IN_STEREO;
    public static int audioFormat = AudioFormat.ENCODING_PCM_16BIT;
    public static int bufferSizeInBytes = 0;
	public static boolean isHasPermission(){
        bufferSizeInBytes = 0;
        bufferSizeInBytes = AudioRecord.getMinBufferSize(sampleRateInHz, channelConfig, audioFormat);
        AudioRecord audioRecord =  new AudioRecord(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes);
        
        try{
            audioRecord.startRecording();
        }catch (IllegalStateException e){
            e.printStackTrace();
        }
        
        if (audioRecord.getRecordingState() != AudioRecord.RECORDSTATE_RECORDING) {
            return false;
        }
        audioRecord.stop();
        audioRecord.release();
        audioRecord = null;

        return true;
    }
	
	 /** 
     * A simple class that draws waveform data received from a 
     * {@link Visualizer.OnDataCaptureListener#onWaveFormDataCapture } 
     */  
    class VisualizerView extends View  
    {  
        private byte[] mBytes;  
        private float[] mPoints;  
        private Rect mRect = new Rect();  
   
        private int mSpectrumNum = 48;    
  
        public VisualizerView(Context context)  
        {  
            super(context);  
            init();  
        }  
  
        private void init()  
        {  
            mBytes = null;  
  
        }  
  
        public void updateVisualizer(byte[] fft)  
        {      
              
            byte[] model = new byte[fft.length / 2 + 1];  
  
            model[0] = (byte) Math.abs(fft[0]);  
            for (int i = 2, j = 1; j < mSpectrumNum;)  
            {  
                model[j] = (byte) Math.hypot(fft[i], fft[i + 1]);  
                i += 2;  
                j++;  
            }  
            mBytes = model;  
            
            
            if (mBytes == null)  
            {  
                return;  
            }  
  
            if (mPoints == null || mPoints.length < mBytes.length * 4)  
            {  
                mPoints = new float[mBytes.length * 4];  
            }  
  
            mRect.set(0, 0, getWidth(), getHeight());  
  
              
            //绘制频谱  
            final int baseX = mRect.width()/mSpectrumNum;  
            final int height = mRect.height();  
  
            for (int i = 0; i < mSpectrumNum ; i++)  
            {  
                if (mBytes[i] < 0)  
                {  
                    mBytes[i] = 127;  
                }  
                  
                final int xi = baseX*i + baseX/2;  
                  
                mPoints[i * 4] = xi;  
                mPoints[i * 4 + 1] = height;  
                  
                mPoints[i * 4 + 2] = xi;  
                mPoints[i * 4 + 3] = height - mBytes[i];  
                
//                double value = Math.pow((int)(1 - mPoints[i * 4 + 3]), 5);
                
//                chnnelValue = (int)(1 - mPoints[i * 4 + 2]);
                chnnelValue = (int)(1 - mPoints[i * 4 + 3]);
                
                chnnelValue = chnnelValue*chnnelValue;
                
                if (chnnelValue <= 1) {
                	chnnelValue = 1;
				}
//                tvrhythmValue.setText(Integer.toString(chnnelValue));
                
//                if (!isMusicSpeed) {
//                	if (chnnelValue >= 60) {
//                    	chnnelValue = chnnelValue + 30;
                    	if (chnnelValue >= 60) {
                    		chnnelValue = 100;
						}else if (chnnelValue >= 50 && chnnelValue < 60) {
							chnnelValue = 100;
						}
						else if (chnnelValue >= 40 && chnnelValue < 50) {
							chnnelValue = 100;
						}
						else if (chnnelValue >= 15 && chnnelValue < 40) {
							chnnelValue = 100;
						}
						else if (chnnelValue >= 9 && chnnelValue < 15) {
							chnnelValue = 90;
						}
						else if (chnnelValue >= 4 && chnnelValue < 9) {
							chnnelValue = 50;
						}
						else if (chnnelValue >= 1 && chnnelValue < 4) {
							chnnelValue = 1;
						}
                    	
//    				}
//				}
                 
//                sendMusicValue(getRandColor(), chnnelValue);
                
            }
        }           
    }

}
