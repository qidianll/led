package com.ledble.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.common.uitl.SharePersistent;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.AdapterView.OnItemClickListener;

//import java.lang.reflect.Array;
import java.util.ArrayList;
//import java.util.Arrays;

import com.common.BaseActivity;
import com.ledble.constant.Constant;
import com.ledble.net.NetConnectBle;
import com.common.adapter.RGBSelectAdapter;
import com.ledble.bean.AdapterBean;
import com.kllzn.R;

public class ElectricSlideDoorActivity extends BaseActivity {

	private Context mContext;
	private MainActivity mActivity;
	
	private ToggleButton toggleButtonSidedoorLeft;
	private ToggleButton toggleButtonSidedoorRight;
	
	private Button ArrowLeft;
	private Button backButton;
	private Button buttonLighting;

//	private ListView mLeftLv/*, mRightLv*/;

	private RGBSelectAdapter rgbAdapter;

//	private int bannerType = 4;
//	private int bannerSort;
//	private int bannerPix;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//隐藏标题  
	    requestWindowFeature(Window.FEATURE_NO_TITLE);        
	    //定义全屏参数  
	    int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;  
	    //获得窗口对象  
	    Window myWindow = this.getWindow();  
	    //设置Flag标识  
	    myWindow.setFlags(flag, flag);
	    
		mContext = this;
		initView();
	}

	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_electricslidedoor);
		mActivity = MainActivity.getMainActivity();

		
		toggleButtonSidedoorLeft = (ToggleButton) findViewById(R.id.toggleButtonSidedoorLeft);
		toggleButtonSidedoorLeft.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mActivity.isLightOpen=isChecked;
				if (isChecked) {
					mActivity.setElectricModel(5, 0);
					toggleButtonSidedoorLeft.setBackgroundResource(R.drawable.sidedoor_left_selected);					
				} else {
					mActivity.setElectricModel(5, 1);
					toggleButtonSidedoorLeft.setBackgroundResource(R.drawable.sidedoor_left);
				}
			}
		});
		
		toggleButtonSidedoorRight = (ToggleButton) findViewById(R.id.toggleButtonSidedoorRight);
		toggleButtonSidedoorRight.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				mActivity.isLightOpen=isChecked;
				if (isChecked) {
					mActivity.setElectricModel(6, 0);
					toggleButtonSidedoorRight.setBackgroundResource(R.drawable.sidedoor_right_selected);					
				} else {
					mActivity.setElectricModel(6, 1);
					toggleButtonSidedoorRight.setBackgroundResource(R.drawable.sidedoor_right);
				}
			}
		});
		

		// 返回按钮
		ArrowLeft = findButtonById(R.id.viewArrowLeft);
		ArrowLeft.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
				
		// 返回按钮
		backButton = findButtonById(R.id.buttonBack1);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
//				NetConnectBle.getInstance().configSPI(bannerType, (byte) (bannerPix >> 8), (byte) bannerPix, bannerSort);
				finish();
			}
		});
		
		// 灯光按钮
		buttonLighting = findButtonById(R.id.buttonLighting);
		buttonLighting.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent codeIntent = new Intent(MainActivity.getMainActivity(), ModeActivity.class);
				startActivity(codeIntent);
			}
		});
	}

	private RGBSelectAdapter buildRGBModel() {
		String[] ary = mContext.getResources().getStringArray(R.array.rhythm_model);
		
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		
		rgbAdapter = new RGBSelectAdapter(mContext, abs);
		return rgbAdapter;
	}

}
