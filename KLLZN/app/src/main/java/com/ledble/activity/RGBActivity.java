package com.ledble.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;

import com.common.uitl.ListUtiles;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import java.util.ArrayList;
import java.util.HashMap;

import com.common.BaseActivity;
import com.common.uitl.Tool;
import com.common.view.SegmentedRadioGroup;
import com.common.adapter.AnimationListenerAdapter;
import com.common.adapter.OnSeekBarChangeListenerAdapter;
import com.ledble.constant.Constant;
import com.ledble.view.BlackWiteSelectView;
import com.ledble.view.ColorTextView;
import com.ledble.view.MyColorPickerImageView4RGB;
import com.ledble.view.BlackWiteSelectView.OnSelectColor;
import com.ledble.adapter.ModelAdapter;
import com.ledble.bean.AdapterBean;
import com.ledble.bean.MyColor;
import com.kllzn.R;

public class RGBActivity extends BaseActivity {
	
	private MyColorPickerImageView4RGB imageViewPicker;
	private BlackWiteSelectView blackWiteSelectView;
	private SeekBar seekBarSpeedBar;
	private TextView textViewSpeed;
	private SeekBar seekBarSpeedBar2;
	private TextView textViewSpeed2;
	private SeekBar seekBarBrightness;
	private TextView textViewBrightness;
	private SegmentedRadioGroup changeRadioGroup;
	private ToggleButton tgbtn;
	private Button buttonConfirButton;
	private Button backButton;
	private TextView textRGB;

	
	private LinearLayout mContentView;
	private int style = 0;
	private int currentTab = 1;// 1：色环，2：模式
	private int currentSelecColorFromPicker;
	private ColorTextView actionView;
	private ArrayList<ColorTextView> colorTextViews;

	private SharedPreferences sp;
	private ModelAdapter maAdapter;

	// 隐藏部分 选色部分View
	private LinearLayout relativeTabColorCover;
	
//	private SegmentedRadioGroup srgCover;
	private TextView textRGBCover;
	private LinearLayout llRing;
	private LinearLayout llCover;
	private TextView tvCoverModel;
//	private ListView lvCover;
	private MyColorPickerImageView4RGB imageViewPicker2;
	private BlackWiteSelectView blackWiteSelectView2;

	@SuppressLint("InflateParams")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		
		initView();
	}

	@SuppressLint("InflateParams")
	@Override
	public void initView() {
		super.initView();
		setContentView(R.layout.activity_rgb);
		
		// mActivity = (MainActivity) getActivity();
	
		
		sp = getSharedPreferences(Constant.SPF_DIY, Context.MODE_PRIVATE);
		

		this.mContentView = findLinearLayout(R.id.linarLayoutColorCile);
//		this.menuView = findLinearLayout(R.id.linearLayoutColorCover);
		
		
		
		
		relativeTabColorCover = (LinearLayout) findLinearLayout(R.id.relativeTabColorCover);

		llRing = (LinearLayout) findLinearLayout(R.id.llRing);
		llCover = (LinearLayout) findLinearLayout(R.id.llCover);
		tvCoverModel = (TextView) findViewById(R.id.tvCoverModel);

		imageViewPicker2 = (MyColorPickerImageView4RGB) findViewById(R.id.imageViewPicker2);
		blackWiteSelectView2 = (BlackWiteSelectView) findViewById(R.id.blackWiteSelectView2);
		textRGB = (TextView) mContentView.findViewById(R.id.tvRGB);
		textRGBCover = (TextView) relativeTabColorCover.findViewById(R.id.tvRGBCover);
		
		// 返回按钮
		backButton = findButtonById(R.id.buttonBack);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// NetConnectBle.getInstance().configSPI(bannerType, (byte)
				// (bannerPix >> 8), (byte) bannerPix,
				// bannerSort);
				finish();
			}
		});
		
		
		seekBarSpeedBar = (SeekBar) findViewById(R.id.seekBarSpeed);
		textViewSpeed = (TextView) findViewById(R.id.textViewSpeed);
		this.seekBarSpeedBar.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					MainActivity.getMainActivity().setSpeed(1);
					textViewSpeed.setText(getResources().getString(R.string.speed_set, 1));
				} else {
					MainActivity.getMainActivity().setSpeed(progress);
					textViewSpeed.setText(getResources().getString(R.string.speed_set, progress));
				}
			}
		});
		
		seekBarSpeedBar2 = (SeekBar) findViewById(R.id.seekBarSpeed2);
		textViewSpeed2 = (TextView) findViewById(R.id.textViewSpeed2);
		this.seekBarSpeedBar2.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (progress == 0) {
					seekBar.setProgress(1);
					MainActivity.getMainActivity().setSpeed(1);
					textViewSpeed2.setText(getResources().getString(R.string.speed_set, 1));
				} else {
					MainActivity.getMainActivity().setSpeed(progress);
					textViewSpeed2.setText(getResources().getString(R.string.speed_set, progress));
				}
			}
		});

		seekBarBrightness = (SeekBar) findViewById(R.id.seekBarBrightNess);
		textViewBrightness = (TextView) findViewById(R.id.textViewBrightNess);
		this.seekBarBrightness.setOnSeekBarChangeListener(new OnSeekBarChangeListenerAdapter() {
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
				if (0 == progress) {
					seekBar.setProgress(1);
					textViewBrightness.setText(getResources().getString(R.string.brightness_set, 1));
					MainActivity.getMainActivity().setBrightNess(1);
				
				} else {
					textViewBrightness.setText(getResources().getString(R.string.brightness_set, progress));
					MainActivity.getMainActivity().setBrightNess(progress);
					
				}
				
			}
		});

		blackWiteSelectView = (BlackWiteSelectView) findViewById(R.id.blackWiteSelectView);
		this.blackWiteSelectView.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color, int progress) {
				int p = progress;

				p = (progress*32)/100;
				
				MainActivity.getMainActivity().setBrightNess(p);
//				LogUtil.i(LedBleApplication.tag, "Brightness ========================================================= " + p);
				 int[] colors = Tool.getRGB(color);
				 updateRgbText(colors);

			}
		});
		
		imageViewPicker = (MyColorPickerImageView4RGB) findViewById(R.id.imageViewPicker);
		this.imageViewPicker.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				int[] colors = Tool.getRGB(color);
				blackWiteSelectView.setStartColor(color);
				updateRgbText(colors);
			}
		});
		
		
		changeRadioGroup = (SegmentedRadioGroup) findViewById(R.id.changeButton);
		changeRadioGroup.check(R.id.changeButtonOne);
		changeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				if (R.id.changeButtonOne == checkedId) {
					style = 0;
				} else if((R.id.changeButtonTwo == checkedId)) {
					style = 1;
				} else if((R.id.changeButtonThree == checkedId)) {
					style = 2;
				}
			}
		});
		
		tgbtn = (ToggleButton) findViewById(R.id.toggleButton1);
		tgbtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				MainActivity.getMainActivity().isLightOpen = isChecked;
				if (isChecked) {
					MainActivity.getMainActivity().open();
					tgbtn.setBackgroundResource(R.drawable.on_btn);
				} else {
					MainActivity.getMainActivity().close();
					tgbtn.setBackgroundResource(R.drawable.off_btn);
				}
			}
		});
		
	
		
//		srgCover.check(R.id.rbRing);
//		srgCover.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
//			@Override
//			public void onCheckedChanged(RadioGroup group, int checkedId) {
//				if (R.id.rbRing == checkedId) {
//					currentTab = 1;
//					llRing.setVisibility(View.VISIBLE);
//					llCover.setVisibility(View.GONE);
//				} else if((R.id.rbModle == checkedId)) {
//					currentTab = 2;
//					llRing.setVisibility(View.GONE);
//					llCover.setVisibility(View.VISIBLE);
//				} 
//			}
//		});
//		
//		lvCover.setAdapter(buildModel());
//		lvCover.setOnItemClickListener(new OnItemClickListener() {
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				maAdapter.setIndex(position);
//				maAdapter.notifyDataSetChanged();
//				AdapterBean abean = ((AdapterBean) maAdapter.getItem(position));
//				String value = abean.getValue();
//				tvCoverModel.setText(mActivity.getResources().getString(R.string.current_mode_format, abean.getLabel()));
//				tvCoverModel.setTag(value);
//				currentSelecColorFromPicker = Integer.parseInt(abean.getValue());
//			}
//		});
		
		initSingColorView();
		initColorBlock();
		initColorSelecterView();
		
	}

	private void putDataBack(ArrayList<MyColor> colors) {
		MainActivity.getMainActivity().setDiy(colors, style);
	}
	
	private ArrayList<MyColor> getSelectColor() {

		ArrayList<MyColor> colorList = new ArrayList<MyColor>();
		if (!ListUtiles.isEmpty(colorTextViews)) {
			for (ColorTextView ctx : colorTextViews) {
				if (-1 != ctx.getColor()) {
					int[] rgb = Tool.getRGB(ctx.getColor());
					colorList.add(new MyColor(rgb[0], rgb[1], rgb[2]));
				}
			}
		}
		return colorList;
	}

	/**
	 * 初始化单色View的点击事件
	 */
	private void initSingColorView() {
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);

		String tagStart = "viewColor";
		View.OnClickListener click = new OnClickListener() {
			@Override
			public void onClick(View v) {
				v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.layout_scale));
				int color = (Integer) v.getTag();
				updateRgbText(Tool.getRGB(color));
				blackWiteSelectView.setStartColor(color);
				imageViewPicker.move2Ege(rmap.get(color));
			}
		};
		ArrayList<View> views = new ArrayList<View>();
		for (int i = 1; i <= 6; i++) {
			View view = mContentView.findViewWithTag(tagStart + i);
			view.setOnClickListener(click);
			view.setTag(colors[i - 1]);
			views.add(view);
		}

	}
	
	/**
	 * 初始化颜色选择block
	 */
	private void initColorBlock() {
		View blocks = mContentView.findViewById(R.id.linearLayoutViewBlocks);
		//16个自定义view
		colorTextViews = new ArrayList<ColorTextView>();
		
		
		View diy = mContentView.findViewById(R.id.linarLayoutColorCile);
		for(int i = 1; i <= 6; i++){
			final ColorTextView tv = (ColorTextView) diy.findViewWithTag((String.valueOf("diyColor") + i));
			String tag = (String) tv.getTag();
			int color = sp.getInt(tag, -1);
			
			if(color != -1){
				if (color < 128) {
					int radius = 10;
					float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
					// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
					RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null); 
					// 组合圆角矩形和ShapeDrawable
					ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape); 
					// 设置形状的颜色
					shapeDrawable.getPaint().setColor(color); 
					// 设置绘制方式为填充
					shapeDrawable.getPaint().setStyle(Paint.Style.FILL);
					// 将当前选择得颜色设置到触发颜色编辑器得View上
					tv.setBackgroundDrawable(shapeDrawable);
					tv.setColor(color);
				} else {
					Drawable image = getImage(color + "");
					tv.setBackgroundDrawable(image);
					tv.setColor(color);
				}
				tv.setText("");
			}
			
			tv.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {// 点击弹出颜色选择框
					v.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.layout_scale));
					int color = tv.getColor();
					if(color != -1) {
					   if (color < 128) {
						   updateRgbText(Tool.getRGB(color));
					   } else {
						   MainActivity.getMainActivity().setRegMode(color);
					   }
					} else {
						showColorCover((ColorTextView) v, true);
					}
				}
			});
			tv.setOnLongClickListener(new OnLongClickListener() {
				@Override
				public boolean onLongClick(View v) {
					ColorTextView cv = (ColorTextView) v;
					cv.setColor(-1);
					String tag = (String) tv.getTag();
					sp.edit().putInt(tag, -1).commit();
					// 长按删除颜色
					cv.setBackgroundDrawable(getResources().getDrawable(R.drawable.block_shap_color));
					cv.setText("+");
					return true;
				}
			});
		}
	}
	
	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 */
	private void showColorCover(ColorTextView actionView) {
		showColorCover(actionView, false);
	}
	
	/**
	 * 弹出颜色编辑器
	 * 
	 * @param actionView
	 * @param hasModel
	 */
	private void showColorCover(ColorTextView actionView, final boolean hasModel) {
		this.actionView = actionView;
		//数据初始化
		currentSelecColorFromPicker = -1;
//		srgCover.check(R.id.rbRing);
//		tvCoverModel.setText(getResources().getString(R.string.current_mode));
//		maAdapter.setIndex(-1);
//		maAdapter.notifyDataSetChanged();
		textRGBCover.setText(MainActivity.getMainActivity().getString(R.string.r_g_b, 0, 0, 0));
		
		Animation slideInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_bottom_in);
		slideInAnimation.setAnimationListener(new AnimationListenerAdapter() {
			@Override
			public void onAnimationStart(Animation animation) {
				relativeTabColorCover.setVisibility(View.VISIBLE);
				if (hasModel) {
//					srgCover.setVisibility(View.VISIBLE);
					llRing.setVisibility(View.VISIBLE);
					llCover.setVisibility(View.GONE);
				} else {
//					srgCover.setVisibility(View.INVISIBLE);
					llRing.setVisibility(View.VISIBLE);
					llCover.setVisibility(View.GONE);
				}
			}
		});
		relativeTabColorCover.startAnimation(slideInAnimation);
	}
	
	/**
	 * 隐藏
	 */
	private void hideColorCover() {
		Animation slideInAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_bottom_out);
		slideInAnimation.setAnimationListener(new AnimationListenerAdapter() {
			@Override
			public void onAnimationEnd(Animation animation) {
				relativeTabColorCover.setVisibility(View.GONE);
			}
		});
		relativeTabColorCover.startAnimation(slideInAnimation);
	}
	
	
	/**
	 * 初始化颜色编辑器
	 */
	private void initColorSelecterView() {
		imageViewPicker2.setOnTouchPixListener(new MyColorPickerImageView4RGB.OnTouchPixListener() {
			@Override
			public void onColorSelect(int color, float angle) {
				blackWiteSelectView2.setStartColor(color);
				currentSelecColorFromPicker = color;
				
				int[] colors = Tool.getRGB(color);
//				updateRgbText(colors);
				textRGBCover.setText(MainActivity.getMainActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
				MainActivity.getMainActivity().setRgb(colors[0], colors[1], colors[2]);
			}
		});

		blackWiteSelectView2.setOnSelectColor(new OnSelectColor() {
			@Override
			public void onColorSelect(int color,int progress) {
				currentSelecColorFromPicker = color;
				int p = progress;
				if (progress<=0) {
					p = 1;
				}
				p = (progress*32)/100;
				
				MainActivity.getMainActivity().setBrightNess(p);
				int[] colors = Tool.getRGB(color);
				textRGBCover.setText(MainActivity.getMainActivity().getString(R.string.r_g_b, colors[0], colors[1], colors[2]));
			}
		});

//		View viewColors = mContentView.findViewById(R.id.viewColors);
		View viewColors = findViewById(R.id.viewColors);
		ArrayList<View> viewCsArrayLis = new ArrayList<View>();
		int[] colors = { 0xffff0000, 0xffffff00, 0xff00ff00, 0xff00ffff, 0xff0000ff, 0xffff00ff };
		final HashMap<Integer, Double> rmap = new HashMap<Integer, Double>();
		rmap.put(colors[0], 0.0);
		rmap.put(colors[1], Math.PI / 3);
		rmap.put(colors[2], Math.PI * 2 / 3);
		rmap.put(colors[3], Math.PI);
		rmap.put(colors[4], Math.PI * 4 / 3);
		rmap.put(colors[5], Math.PI * 5 / 3);
		
		for (int i = 1; i <= 6; i++) {
			View vc = viewColors.findViewWithTag("viewColor" + i);
			vc.setTag(colors[i - 1]);
			vc.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int selectColor = (Integer) v.getTag();
					currentSelecColorFromPicker = selectColor;
					blackWiteSelectView2.setStartColor(selectColor);
					imageViewPicker2.move2Ege(rmap.get(selectColor));
					updateRgbText(Tool.getRGB(selectColor));
				}
			});
			viewCsArrayLis.add(vc);
		}
		
		buttonConfirButton = (Button) findViewById(R.id.buttonConfirm);
		buttonConfirButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				hideColorCover();
				if (currentSelecColorFromPicker == -1)
					return;
				if (currentTab == 1) {
					int radius = 10;
					float[] outerR = new float[] { radius, radius, radius, radius, radius, radius, radius, radius };
					// 构造一个圆角矩形,可以使用其他形状，这样ShapeDrawable
					// 就会根据形状来绘制。
					RoundRectShape roundRectShape = new RoundRectShape(outerR, null, null); 
					// 组合圆角矩形和ShapeDrawable																		
					ShapeDrawable shapeDrawable = new ShapeDrawable(roundRectShape); 
					// 设置形状的颜色
					shapeDrawable.getPaint().setColor(currentSelecColorFromPicker); 
					// 设置绘制方式为填充
					shapeDrawable.getPaint().setStyle(Paint.Style.FILL); 
					actionView.setColor(currentSelecColorFromPicker);
					// 保存颜色值
					String tag = (String) actionView.getTag();
					sp.edit().putInt(tag, currentSelecColorFromPicker).commit();
					// 将当前选择得颜色设置到触发颜色编辑器得View上
					actionView.setBackgroundDrawable(shapeDrawable);
				} else {
					actionView.setColor(currentSelecColorFromPicker);
					// 保存颜色值
					String tag = (String) actionView.getTag();
					String value = tvCoverModel.getTag().toString();
					sp.edit().putInt(tag, Integer.parseInt(value)).commit();
					Drawable image = getImage(value);
					actionView.setBackgroundDrawable(image);
				}
				actionView.setText("");
			}
		});
	}
	
	public Drawable getImage(String value) {
		int resID = getResources().getIdentifier("img_" + value, "drawable", "com.carledlight");
		return getResources().getDrawable(resID);
	}

	public void updateRgbText(int rgb[]) {
//		((TextView) mContentView.findViewById(R.id.textViewR)).setText("R:" + (rgb[0]));
//		((TextView) mContentView.findViewById(R.id.textViewG)).setText("G:" + (rgb[1]));
//		((TextView) mContentView.findViewById(R.id.textViewB)).setText("B:" + (rgb[2]));
		
		textRGB.setText(MainActivity.getMainActivity().getString(R.string.r_g_b, rgb[0], rgb[1], rgb[2]));		
		
		
		try {
			MainActivity.getMainActivity().setRgb(rgb[0], rgb[1], rgb[2]);
		} catch (Exception e) {
			e.printStackTrace();
			Tool.ToastShow(this, "错误。。。");
		}
	}

	private ModelAdapter buildModel() {
		String[] ary = getResources().getStringArray(R.array.rgb_mode);
		ArrayList<AdapterBean> abs = new ArrayList<AdapterBean>();
		for (String lable : ary) {
			String label[] = lable.split(",");
			AdapterBean abean = new AdapterBean(label[0], label[1]);
			abs.add(abean);
		}
		maAdapter = new ModelAdapter(getApplicationContext(), abs);
		return maAdapter;
	}
}
