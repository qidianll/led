package com.ledble.service;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
public class LiveService extends NotificationListenerService {
	
	public LiveService() {

    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
    	
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
    	
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_REDELIVER_INTENT;
    }

}
