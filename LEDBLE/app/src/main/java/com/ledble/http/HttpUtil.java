package com.ledble.http;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.util.Log;

public class HttpUtil {
	
	private String result = "";
	private String message = "";
	private static HttpUtil httpUtil = null;
	
	private HttpUtil() {} 
	 
	public static HttpUtil getInstance() {
		if (httpUtil == null) {
			synchronized (HttpUtil.class) {
				if (httpUtil == null) {
					httpUtil = new HttpUtil();
				}
			}
		}
		return httpUtil;
	}
	
	public interface HttpCallBack {
		void onSuccess(String result);
		void onException(String message);
	}

	/**
	 * 获取数据
	 * @param activity
	 * @param url      
	 * @param params
	 * @param httpCallBack
	 */
	public void getSourceData(final Activity activity, final String url, final Map<String, String> params, 
			final HttpCallBack httpCallBack) {
		new Thread(new Runnable() {
			
			public void run() {
				String resultStr = null;
				HttpParams httpParameters = new BasicHttpParams();
				// Set the timeout in milliseconds until a connection is established.
				int timeoutConnection = 10000;
				HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
				// Set the default socket timeout (SO_TIMEOUT) 
				// in milliseconds which is the timeout for waiting for data.
				int timeoutSocket = 30000;
				HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
			    
				SchemeRegistry schemeRegistry = new SchemeRegistry();
				try {
					KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
					keyStore.load(null, null);
					schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
		        ThreadSafeClientConnManager cm = new ThreadSafeClientConnManager(httpParameters, schemeRegistry);
		        DefaultHttpClient httpclient = new DefaultHttpClient(cm, httpParameters);
		        
				HttpProtocolParams.setUseExpectContinue(httpclient.getParams(), false);
			    HttpPost httpPost = new HttpPost(url);
		 
			    //设置参数  
	            List<NameValuePair> nvps = new ArrayList<NameValuePair>();   
	            for (Iterator<String> iter = params.keySet().iterator(); iter.hasNext();) {  
	                String name = iter.next();  
	                String value = params.get(name);  
	                nvps.add(new BasicNameValuePair(name, value));  
	            }
	 
		    	try {
					httpPost.setEntity(new UrlEncodedFormEntity(nvps,HTTP.UTF_8));
					ResponseHandler <String> res1 = new BasicResponseHandler();
					resultStr = httpclient.execute(httpPost,res1);
					Log.v("HttpUtil",resultStr);
			        
				} catch(Exception e) {
					if (httpCallBack != null && e != null) {
						message = "ErrorMsg:"+ e.getMessage();
						Log.v("HttpUtil",message);
						activity.runOnUiThread(new Runnable() {
				     
				            public void run() {
				            	httpCallBack.onException(message);
				            }
				        });
						
					}
				}
		    	httpclient.getConnectionManager().shutdown();
		    	if (httpCallBack != null && resultStr != null) {
		    		result = resultStr;
		    		activity.runOnUiThread(new Runnable() {
					     
			            public void run() {
			            	httpCallBack.onSuccess(result);
			            }
			        });
		    	}
			}
		}).start();
	 }
}
