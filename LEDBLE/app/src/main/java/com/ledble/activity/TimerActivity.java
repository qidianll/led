package com.ledble.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.format.Time;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.common.BaseActivity;
import com.common.uitl.Constant;
import com.common.uitl.NumberHelper;
import com.common.uitl.SharePersistent;
import com.common.uitl.StringUtils;
import com.ledble.R;
import com.ledble.bean.TimerModelBean;
import com.ledble.view.wheel.OnWheelChangedListener;
import com.ledble.view.wheel.WheelModelAdapter;
import com.ledble.view.wheel.WheelView;
import com.umeng.analytics.MobclickAgent;

public class TimerActivity extends BaseActivity {

	private LinearLayout linearLayoutTimerOn;;
	private LinearLayout linearLayoutTimerOff;;
	private TextView textViewOnTime;

	private TextView textViewOffTime;
	private TextView textViewModelText;
	private ToggleButton toggleButtonOn;
	private ToggleButton toggleButtonOff;
	
	private boolean isClick = false;
	private final int INT_TIMER_ON = 111;
	private final int INT_TIMER_OFF = 222;
	private MainActivity mActivity;

	public static final String TIMER_TAG = "timer_tag";
	
	private String tag = "";
	private View linearLayoutContainer;

	private int hour /*= 12*/;
	private int minite /*= 30*/;
	private int model /*= 10*/;
	
	
	private String modelText = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		initView();
	}

	@Override
	public void initView() {
		setContentView(R.layout.fragment_timer);
		
		mActivity = MainActivity.getMainActivity();		
		
		
		textViewOnTime = findTextViewById(R.id.textViewOnTime);
		textViewOffTime = findTextViewById(R.id.textViewOffTime);
		
		textViewModelText = findTextViewById(R.id.textViewModelText);
		
		toggleButtonOn = (ToggleButton)findViewById(R.id.toggleOn);
		toggleButtonOff = (ToggleButton)findViewById(R.id.toggleOff);
		
//		String ttag = getMyTag();
//		TimerModelBean tb = (TimerModelBean) SharePersistent.getObjectValue(mActivity, ttag);
//		if (null != tb) {
//			textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(tb.getTimerOnHour()) + ":"
//					+ NumberHelper.LeftPad_Tow_Zero(tb.getTimerOnMinute()));
//			toggleButtonOn.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOn"));
//
//			textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(tb.getTimerOffHour()) + ":"
//					+ NumberHelper.LeftPad_Tow_Zero(tb.getTimerOffMinute()));
//			toggleButtonOff.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOff"));
//			textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on)
//					+ (StringUtils.isEmpty(tb.getModelText()) ? "" : ":" + tb.getModelText()));
//			textViewModelText.setTag(tb.getModel());
//		}
//		String ttag = getMyTag();
//		TimerModelBean tb = (TimerModelBean) SharePersistent.getObjectValue(mActivity, ttag);
		
		for (int i = 0; i < 2; i++) {
			if (i== 0) {
				tag = "TimerOn";
				String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName()+tag);
				
				if (null != timeData && timeData.length > 3) {
//					Toast.makeText(MainActivity.getMainActivity(), "timeData[0] = "+timeData[0], Toast.LENGTH_SHORT).show();
					
					int hour = Integer.parseInt(timeData[0]);
					int minute = Integer.parseInt(timeData[1]);
					int model = Integer.parseInt(timeData[2]);
					String modelText = timeData[3];			
					
//					Toast.makeText(MainActivity.getMainActivity(), "hour = "+hour, Toast.LENGTH_SHORT).show();
					if (hour >= 0 && minute >= 0 ) {
						textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":"
								+ NumberHelper.LeftPad_Tow_Zero(minute));
					}
					if (model >= 0 ) {
						textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on)
								+ (StringUtils.isEmpty(modelText) ? "" : ":" + modelText));
					}
					
					toggleButtonOn.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOn"));					
					
////					textViewModelText.setTag(model);
				}
			}else {
				tag = "TimerOff";
				String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName()+tag);
				if (null != timeData && timeData.length > 1) {					
//
					int hour = Integer.parseInt(timeData[0]);
					int minute = Integer.parseInt(timeData[1]);
					
					if (hour >= 0 && minute >= 0) {
						textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":"
								+ NumberHelper.LeftPad_Tow_Zero(minute));
					}					
					
					toggleButtonOff.setChecked(SharePersistent.getBoolean(mActivity, "toggleButtonOff"));					
				}
			}
		}
		
		
		Button buttonBackButton = (Button)findButtonById(R.id.buttonBack);
		buttonBackButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		linearLayoutTimerOn = (LinearLayout)findViewById(R.id.linearLayoutTimerOn);
		linearLayoutTimerOn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tag = "TimerOn";
				gotoTimerSettting(v, INT_TIMER_ON);
				SharePersistent.savePerference(mActivity, "Timer", tag);
			}
		});
		linearLayoutTimerOff = (LinearLayout)findViewById(R.id.linearLayoutTimerOff);
		linearLayoutTimerOff.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				tag = "TimerOff";
				gotoTimerSettting(v, INT_TIMER_OFF);
				SharePersistent.savePerference(mActivity, "Timer", tag);
			}
		});

		
		toggleButtonOn.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//				saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), -1, -1, -1, -1, null, -1);
				SharePersistent.saveBoolean(mActivity, mActivity.getGroupName()+"toggleButtonOn", toggleButtonOn.isChecked());
				
				tag = "TimerOn";
				String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName()+tag);
				if (toggleButtonOn.isChecked()) {
					int hour = Integer.parseInt(timeData[0]);
					int minute = Integer.parseInt(timeData[1]);
					int model = Integer.parseInt(timeData[2]);
					if (hour >= 0 && minute >= 0) {
						mActivity.timerOn(hour, minute, model);
					}
					
				} else {
					mActivity.turnOnOffTimerOn(0);
				}
			}
		});
		
		toggleButtonOff.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// textViewOffTime //保存定时关的时间
//				saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), -1, -1, -1, -1, null, -1);
				SharePersistent.saveBoolean(mActivity, mActivity.getGroupName()+"toggleButtonOff", toggleButtonOff.isChecked());
//				TimerModelBean tb = (TimerModelBean) SharePersistent.getObjectValue(mActivity, TIMER_TAG);
				
				tag = "TimerOff";
				String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName()+tag);
				if (toggleButtonOff.isChecked() ) {
					int hour = Integer.parseInt(timeData[0]);
					int minute = Integer.parseInt(timeData[1]);
					mActivity.timerOff(hour, minute);
				} else {
					mActivity.turnOnOffTimerOff(0);
				}
			}
		});
	}

	private void putDataback() {

		Intent intent = new Intent();
		intent.putExtra("hour", hour);
		intent.putExtra("minite", minite);
		intent.putExtra("model", model);
		intent.putExtra("modelText", modelText);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}
	
	private void gotoTimerSettting(View v, int id) {
		isClick = true;
		Intent intent = new Intent(mActivity, TimerSettingActivity.class);
		mActivity.startActivityForResult(intent, id);
//		startActivityForResult(intent, id);
//		startActivity(new Intent(mActivity, TimerSettingActivity.class));
//		start
//		getParent().startActivityForResult(intent, id); 
		
//		Toast.makeText(mActivity, "id = " + id, Toast.LENGTH_SHORT).show();	
	}
	
//	private String getMyTag() {
//		String tag = mActivity.getGroupName();
//		if (StringUtils.isEmpty(tag) || "null".equals(tag)) {
//			tag = TimerModelBean.groupNameDefaut;
//		}
//		return tag;
//	}
	
	/**
	 * 保存设置参数
	 * 
	 * @param timerOnTurnOn
	 * @param timerOffTurnOn
	 * @param timerOnHour
	 * @param timerOnMinute
	 * @param timerOffHour
	 * @param timerOffMinute
	 */
//	private void saveState2Tag(boolean timerOnTurnOn, boolean timerOffTurnOn, int timerOnHour, int timerOnMinute, int timerOffHour,
//			int timerOffMinute, String modelText, int model) {
////		String tag = getTag();
//
//		TimerModelBean timerModelBean = (TimerModelBean) SharePersistent.getObjectValue(mActivity, TIMER_TAG);
//		if (null == timerModelBean) {
//			timerModelBean = new TimerModelBean();
//		}
//		timerModelBean.setGroupName(TIMER_TAG);
//
//		timerModelBean.setOpenTurnOn(timerOnTurnOn);
//		timerModelBean.setCloseTurnOn(timerOffTurnOn);
//
//		if (-1 != timerOnHour) {
//			timerModelBean.setTimerOnHour(timerOnHour);
//			timerModelBean.setTimerOnMinute(timerOnMinute);
//			timerModelBean.setModelText(modelText);
//			timerModelBean.setModel(model);
//		}
//		if (-1 != timerOffHour) {
//			timerModelBean.setTimerOffHour(timerOffHour);
//			timerModelBean.setTimerOffMinute(timerOffMinute);
//		}
//		SharePersistent.setObjectValue(mActivity, tag, timerModelBean);
//	}
	
//	@Override
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//					
//		if (requestCode == INT_TIMER_ON && resultCode == Activity.RESULT_OK) {		
//			
//			Toast.makeText(mActivity, "INT_TIMER_ON", Toast.LENGTH_SHORT).show();
//			
//			int hour = data.getIntExtra("hour", -1);
//			int minute = data.getIntExtra("minite", -1);
//			int model = data.getIntExtra("model", -1);
//			String modelText = data.getStringExtra("modelText");
//
//			textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
//			textViewOnTime.setTag(modelText);
//			textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on) + ":" + modelText);
//			textViewModelText.setTag(model);
//			saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), hour, minute, -1, -1, modelText, model);
//			if (toggleButtonOn.isChecked()) {
//				mActivity.timerOn(hour, minute, model);
//			}
//			return;
//		}
//		if (requestCode == INT_TIMER_OFF && resultCode == Activity.RESULT_OK) {
//			
//			Toast.makeText(mActivity, "INT_TIMER_OFF", Toast.LENGTH_SHORT).show();
//			
//			int hour = data.getIntExtra("hour", -1);
//			int minute = data.getIntExtra("minite", -1);
//			textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
//			saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), -1, -1, hour, minute, null, -1);
//			if (toggleButtonOff.isChecked()) {
//				mActivity.timerOff(hour, minute);
//			}
//			return;
//		}
//	}
	
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		MobclickAgent.onResume(this);
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		MobclickAgent.onPause(this);
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		if (isClick) {
			String[] timeData = SharePersistent.getTimerData(MainActivity.getMainActivity(), MainActivity.getMainActivity().getGroupName()+tag);
//			
			if (tag.equalsIgnoreCase("TimerOn")) {
//				
//				Toast.makeText(MainActivity.getMainActivity(), "modelText = "+timeData[3] , Toast.LENGTH_SHORT).show();
				
				int hour = Integer.parseInt(timeData[0]);
				int minute = Integer.parseInt(timeData[1]);
				int model = Integer.parseInt(timeData[2]);
				String modelText = timeData[3];

				textViewOnTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
				textViewOnTime.setTag(modelText);
				textViewModelText.setText(mActivity.getResources().getString(R.string.timer_on) + ":" + modelText);
				textViewModelText.setTag(model);
//				saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), hour, minute, -1, -1, modelText, model);
				if (toggleButtonOn.isChecked()) {
					mActivity.timerOn(hour, minute, model);
				}
				
			}
			else if (tag.equalsIgnoreCase("TimerOff")) {
				
				int hour = Integer.parseInt(timeData[0]);
				int minute = Integer.parseInt(timeData[1]);
				textViewOffTime.setText(NumberHelper.LeftPad_Tow_Zero(hour) + ":" + NumberHelper.LeftPad_Tow_Zero(minute));
//				saveState2Tag(toggleButtonOn.isChecked(), toggleButtonOff.isChecked(), -1, -1, hour, minute, null, -1);
				if (toggleButtonOff.isChecked()) {
					mActivity.timerOff(hour, minute);
				}
				
			}
		}
		
		
//		Toast.makeText(MainActivity.getMainActivity(), ""+tag, Toast.LENGTH_SHORT).show();
	}
}
