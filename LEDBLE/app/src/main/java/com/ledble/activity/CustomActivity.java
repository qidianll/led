package com.ledble.activity;

import com.ledble.R;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class CustomActivity extends Activity {
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom);
        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvContent = (TextView) findViewById(R.id.tvContent);
        
//        String title = "重要通知";
//        String description = getString(R.string.notice);
//        
//        tvTitle.setText(title);
//        tvContent.setText(description);
        
        
        tvTitle.setText(getIntent().getStringExtra("title"));
        tvContent.setText(getIntent().getStringExtra("description"));
    }
    
}
