// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class RgbFragment$$ViewBinder<T extends com.ledble.fragment.RgbFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230942, "field 'relativeTab1'");
    target.relativeTab1 = view;
    view = finder.findRequiredView(source, 2131230943, "field 'relativeTab2'");
    target.relativeTab2 = view;
    view = finder.findRequiredView(source, 2131230944, "field 'relativeTab3'");
    target.relativeTab3 = view;
    view = finder.findRequiredView(source, 2131230846, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230846, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131231061, "field 'textViewRGB'");
    target.textViewRGB = finder.castView(view, 2131231061, "field 'textViewRGB'");
    view = finder.findRequiredView(source, 2131230849, "field 'imageViewPicker'");
    target.imageViewPicker = finder.castView(view, 2131230849, "field 'imageViewPicker'");
    view = finder.findRequiredView(source, 2131230758, "field 'blackWiteSelectView'");
    target.blackWiteSelectView = finder.castView(view, 2131230758, "field 'blackWiteSelectView'");
    view = finder.findRequiredView(source, 2131231053, "field 'tvBrightness'");
    target.tvBrightness = finder.castView(view, 2131231053, "field 'tvBrightness'");
    view = finder.findRequiredView(source, 2131231040, "field 'textViewWarmCool'");
    target.textViewWarmCool = finder.castView(view, 2131231040, "field 'textViewWarmCool'");
    view = finder.findRequiredView(source, 2131230913, "field 'pikerImageView'");
    target.pikerImageView = finder.castView(view, 2131230913, "field 'pikerImageView'");
    view = finder.findRequiredView(source, 2131230969, "field 'seekBarBrightNess'");
    target.seekBarBrightNess = finder.castView(view, 2131230969, "field 'seekBarBrightNess'");
    view = finder.findRequiredView(source, 2131231015, "field 'textViewBrightNess'");
    target.textViewBrightNess = finder.castView(view, 2131231015, "field 'textViewBrightNess'");
    view = finder.findRequiredView(source, 2131230847, "field 'imageViewOnOffCT'");
    target.imageViewOnOffCT = finder.castView(view, 2131230847, "field 'imageViewOnOffCT'");
    view = finder.findRequiredView(source, 2131231017, "field 'textViewBrightNessDim'");
    target.textViewBrightNessDim = finder.castView(view, 2131231017, "field 'textViewBrightNessDim'");
    view = finder.findRequiredView(source, 2131230914, "field 'pikerImageViewDim'");
    target.pikerImageViewDim = finder.castView(view, 2131230914, "field 'pikerImageViewDim'");
    view = finder.findRequiredView(source, 2131230848, "field 'imageViewOnOffDim'");
    target.imageViewOnOffDim = finder.castView(view, 2131230848, "field 'imageViewOnOffDim'");
  }

  @Override public void unbind(T target) {
    target.relativeTab1 = null;
    target.relativeTab2 = null;
    target.relativeTab3 = null;
    target.imageViewOnOff = null;
    target.textViewRGB = null;
    target.imageViewPicker = null;
    target.blackWiteSelectView = null;
    target.tvBrightness = null;
    target.textViewWarmCool = null;
    target.pikerImageView = null;
    target.seekBarBrightNess = null;
    target.textViewBrightNess = null;
    target.imageViewOnOffCT = null;
    target.textViewBrightNessDim = null;
    target.pikerImageViewDim = null;
    target.imageViewOnOffDim = null;
  }
}
