// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class CutomFragment$$ViewBinder<T extends com.ledble.fragment.CutomFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230784, "field 'changeRadioGroup'");
    target.changeRadioGroup = finder.castView(view, 2131230784, "field 'changeRadioGroup'");
    view = finder.findRequiredView(source, 2131230846, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230846, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131230976, "field 'seekBarSpeedCustom'");
    target.seekBarSpeedCustom = finder.castView(view, 2131230976, "field 'seekBarSpeedCustom'");
    view = finder.findRequiredView(source, 2131231036, "field 'textViewSpeedCustom'");
    target.textViewSpeedCustom = finder.castView(view, 2131231036, "field 'textViewSpeedCustom'");
    view = finder.findRequiredView(source, 2131230968, "field 'seekBarBrightCustom'");
    target.seekBarBrightCustom = finder.castView(view, 2131230968, "field 'seekBarBrightCustom'");
    view = finder.findRequiredView(source, 2131231014, "field 'textViewBrightCustom'");
    target.textViewBrightCustom = finder.castView(view, 2131231014, "field 'textViewBrightCustom'");
    view = finder.findRequiredView(source, 2131230762, "field 'buttonRunButton'");
    target.buttonRunButton = finder.castView(view, 2131230762, "field 'buttonRunButton'");
  }

  @Override public void unbind(T target) {
    target.changeRadioGroup = null;
    target.imageViewOnOff = null;
    target.seekBarSpeedCustom = null;
    target.textViewSpeedCustom = null;
    target.seekBarBrightCustom = null;
    target.textViewBrightCustom = null;
    target.buttonRunButton = null;
  }
}
