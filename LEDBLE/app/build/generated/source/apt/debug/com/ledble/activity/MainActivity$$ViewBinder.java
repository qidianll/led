// Generated code from Butter Knife. Do not modify!
package com.ledble.activity;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MainActivity$$ViewBinder<T extends com.ledble.activity.MainActivity> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230978, "field 'segmentDm'");
    target.segmentDm = finder.castView(view, 2131230978, "field 'segmentDm'");
    view = finder.findRequiredView(source, 2131230977, "field 'segmentCt'");
    target.segmentCt = finder.castView(view, 2131230977, "field 'segmentCt'");
    view = finder.findRequiredView(source, 2131230980, "field 'segmentRgb'");
    target.segmentRgb = finder.castView(view, 2131230980, "field 'segmentRgb'");
    view = finder.findRequiredView(source, 2131230979, "field 'segmentMusic'");
    target.segmentMusic = finder.castView(view, 2131230979, "field 'segmentMusic'");
    view = finder.findRequiredView(source, 2131230860, "field 'ivLeftMenu'");
    target.ivLeftMenu = finder.castView(view, 2131230860, "field 'ivLeftMenu'");
    view = finder.findRequiredView(source, 2131231018, "field 'textViewConnectCount'");
    target.textViewConnectCount = finder.castView(view, 2131231018, "field 'textViewConnectCount'");
    view = finder.findRequiredView(source, 2131230862, "field 'ivRightMenu'");
    target.ivRightMenu = finder.castView(view, 2131230862, "field 'ivRightMenu'");
    view = finder.findRequiredView(source, 2131230865, "field 'ivType'");
    target.ivType = finder.castView(view, 2131230865, "field 'ivType'");
    view = finder.findRequiredView(source, 2131230947, "field 'rgBottom'");
    target.rgBottom = finder.castView(view, 2131230947, "field 'rgBottom'");
    view = finder.findRequiredView(source, 2131230924, "field 'rbFirst'");
    target.rbFirst = finder.castView(view, 2131230924, "field 'rbFirst'");
    view = finder.findRequiredView(source, 2131230933, "field 'rbSecond'");
    target.rbSecond = finder.castView(view, 2131230933, "field 'rbSecond'");
    view = finder.findRequiredView(source, 2131230934, "field 'rbThrid'");
    target.rbThrid = finder.castView(view, 2131230934, "field 'rbThrid'");
    view = finder.findRequiredView(source, 2131230925, "field 'rbFourth'");
    target.rbFourth = finder.castView(view, 2131230925, "field 'rbFourth'");
    view = finder.findRequiredView(source, 2131230923, "field 'rbFifth'");
    target.rbFifth = finder.castView(view, 2131230923, "field 'rbFifth'");
    view = finder.findRequiredView(source, 2131230895, "field 'avtivity_main'");
    target.avtivity_main = finder.castView(view, 2131230895, "field 'avtivity_main'");
    view = finder.findRequiredView(source, 2131230900, "field 'left_menu'");
    target.left_menu = finder.castView(view, 2131230900, "field 'left_menu'");
    view = finder.findRequiredView(source, 2131230951, "field 'right_menu'");
    target.right_menu = finder.castView(view, 2131230951, "field 'right_menu'");
    view = finder.findRequiredView(source, 2131230879, "field 'TopItem'");
    target.TopItem = finder.castView(view, 2131230879, "field 'TopItem'");
    view = finder.findRequiredView(source, 2131230983, "field 'shakeView'");
    target.shakeView = finder.castView(view, 2131230983, "field 'shakeView'");
  }

  @Override public void unbind(T target) {
    target.segmentDm = null;
    target.segmentCt = null;
    target.segmentRgb = null;
    target.segmentMusic = null;
    target.ivLeftMenu = null;
    target.textViewConnectCount = null;
    target.ivRightMenu = null;
    target.ivType = null;
    target.rgBottom = null;
    target.rbFirst = null;
    target.rbSecond = null;
    target.rbThrid = null;
    target.rbFourth = null;
    target.rbFifth = null;
    target.avtivity_main = null;
    target.left_menu = null;
    target.right_menu = null;
    target.TopItem = null;
    target.shakeView = null;
  }
}
