// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class ModeFragment$$ViewBinder<T extends com.ledble.fragment.ModeFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230886, "field 'listViewModel'");
    target.listViewModel = finder.castView(view, 2131230886, "field 'listViewModel'");
    view = finder.findRequiredView(source, 2131231019, "field 'textViewCurretModel'");
    target.textViewCurretModel = finder.castView(view, 2131231019, "field 'textViewCurretModel'");
    view = finder.findRequiredView(source, 2131230852, "field 'buttonPlay'");
    target.buttonPlay = finder.castView(view, 2131230852, "field 'buttonPlay'");
    view = finder.findRequiredView(source, 2131230846, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230846, "field 'imageViewOnOff'");
    view = finder.findRequiredView(source, 2131230972, "field 'seekBarMode'");
    target.seekBarMode = finder.castView(view, 2131230972, "field 'seekBarMode'");
    view = finder.findRequiredView(source, 2131231026, "field 'textViewMode'");
    target.textViewMode = finder.castView(view, 2131231026, "field 'textViewMode'");
    view = finder.findRequiredView(source, 2131230975, "field 'seekBarSpeedBar'");
    target.seekBarSpeedBar = finder.castView(view, 2131230975, "field 'seekBarSpeedBar'");
    view = finder.findRequiredView(source, 2131231035, "field 'textViewSpeed'");
    target.textViewSpeed = finder.castView(view, 2131231035, "field 'textViewSpeed'");
    view = finder.findRequiredView(source, 2131230969, "field 'seekBarBrightness'");
    target.seekBarBrightness = finder.castView(view, 2131230969, "field 'seekBarBrightness'");
    view = finder.findRequiredView(source, 2131231015, "field 'textViewBrightness'");
    target.textViewBrightness = finder.castView(view, 2131231015, "field 'textViewBrightness'");
  }

  @Override public void unbind(T target) {
    target.listViewModel = null;
    target.textViewCurretModel = null;
    target.buttonPlay = null;
    target.imageViewOnOff = null;
    target.seekBarMode = null;
    target.textViewMode = null;
    target.seekBarSpeedBar = null;
    target.textViewSpeed = null;
    target.seekBarBrightness = null;
    target.textViewBrightness = null;
  }
}
