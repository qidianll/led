// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class MusicFragment$$ViewBinder<T extends com.ledble.fragment.MusicFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230854, "field 'imageViewPre'");
    target.imageViewPre = finder.castView(view, 2131230854, "field 'imageViewPre'");
    view = finder.findRequiredView(source, 2131230851, "field 'imageViewPlay'");
    target.imageViewPlay = finder.castView(view, 2131230851, "field 'imageViewPlay'");
    view = finder.findRequiredView(source, 2131230845, "field 'imageViewNext'");
    target.imageViewNext = finder.castView(view, 2131230845, "field 'imageViewNext'");
    view = finder.findRequiredView(source, 2131231011, "field 'textViewAutoAjust'");
    target.textViewAutoAjust = finder.castView(view, 2131231011, "field 'textViewAutoAjust'");
    view = finder.findRequiredView(source, 2131231057, "field 'tvCurrentTime'");
    target.tvCurrentTime = finder.castView(view, 2131231057, "field 'tvCurrentTime'");
    view = finder.findRequiredView(source, 2131231069, "field 'tvTotalTime'");
    target.tvTotalTime = finder.castView(view, 2131231069, "field 'tvTotalTime'");
    view = finder.findRequiredView(source, 2131230894, "field 'llDecibel'");
    target.llDecibel = finder.castView(view, 2131230894, "field 'llDecibel'");
    view = finder.findRequiredView(source, 2131230971, "field 'seekBarDecibel'");
    target.seekBarDecibel = finder.castView(view, 2131230971, "field 'seekBarDecibel'");
    view = finder.findRequiredView(source, 2131231058, "field 'tvDecibelValue'");
    target.tvDecibelValue = finder.castView(view, 2131231058, "field 'tvDecibelValue'");
    view = finder.findRequiredView(source, 2131230892, "field 'llBottom'");
    target.llBottom = finder.castView(view, 2131230892, "field 'llBottom'");
    view = finder.findRequiredView(source, 2131230973, "field 'seekBarMusic'");
    target.seekBarMusic = finder.castView(view, 2131230973, "field 'seekBarMusic'");
    view = finder.findRequiredView(source, 2131230855, "field 'imageViewRotate'");
    target.imageViewRotate = finder.castView(view, 2131230855, "field 'imageViewRotate'");
    view = finder.findRequiredView(source, 2131230853, "field 'imageViewPlayType'");
    target.imageViewPlayType = finder.castView(view, 2131230853, "field 'imageViewPlayType'");
    view = finder.findRequiredView(source, 2131230771, "field 'buttonMusicLib'");
    target.buttonMusicLib = finder.castView(view, 2131230771, "field 'buttonMusicLib'");
    view = finder.findRequiredView(source, 2131230974, "field 'seekBarRhythm'");
    target.seekBarRhythm = finder.castView(view, 2131230974, "field 'seekBarRhythm'");
    view = finder.findRequiredView(source, 2131231089, "field 'volumCircleBar'");
    target.volumCircleBar = finder.castView(view, 2131231089, "field 'volumCircleBar'");
    view = finder.findRequiredView(source, 2131231062, "field 'tvRhythm'");
    target.tvRhythm = finder.castView(view, 2131231062, "field 'tvRhythm'");
    view = finder.findRequiredView(source, 2131231070, "field 'tvrhythmValue'");
    target.tvrhythmValue = finder.castView(view, 2131231070, "field 'tvrhythmValue'");
  }

  @Override public void unbind(T target) {
    target.imageViewPre = null;
    target.imageViewPlay = null;
    target.imageViewNext = null;
    target.textViewAutoAjust = null;
    target.tvCurrentTime = null;
    target.tvTotalTime = null;
    target.llDecibel = null;
    target.seekBarDecibel = null;
    target.tvDecibelValue = null;
    target.llBottom = null;
    target.seekBarMusic = null;
    target.imageViewRotate = null;
    target.imageViewPlayType = null;
    target.buttonMusicLib = null;
    target.seekBarRhythm = null;
    target.volumCircleBar = null;
    target.tvRhythm = null;
    target.tvrhythmValue = null;
  }
}
