// Generated code from Butter Knife. Do not modify!
package com.ledble.fragment;

import android.view.View;
import butterknife.ButterKnife.Finder;
import butterknife.ButterKnife.ViewBinder;

public class DmFragment$$ViewBinder<T extends com.ledble.fragment.DmFragment> implements ViewBinder<T> {
  @Override public void bind(final Finder finder, final T target, Object source) {
    View view;
    view = finder.findRequiredView(source, 2131230846, "field 'imageViewOnOff'");
    target.imageViewOnOff = finder.castView(view, 2131230846, "field 'imageViewOnOff'");
  }

  @Override public void unbind(T target) {
    target.imageViewOnOff = null;
  }
}
